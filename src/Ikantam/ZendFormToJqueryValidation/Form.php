<?php
/**
 * Author: Alex P.
 * Date: 15.08.14
 * Time: 14:26
 */

namespace Ikantam\ZendFormToJqueryValidation;

/**
 * Class Form
 * @package Ikantam\ZendFormToJqueryValidation
 */
class Form
{
    /**
     * @var FormElement
     */
    protected $elementHandler;

    /**
     * @var \Zend_Form
     */
    protected $form;

    /**
     * @var array
     */
    protected $result = array(
        'rules' => array(),
        'messages' => array()
    );

    /**
     * @param FormElement $handler
     */
    public function __construct(FormElement $handler)
    {
        $this->elementHandler = $handler;
    }

    /**
     * @param \Zend_Form $form
     * @return $this
     */
    public function setForm(\Zend_Form $form)
    {
        $this->form = $form;
        return $this;
    }

    /**
     * Result of conversion
     * @return array
     */
    public function getResult()
    {
        $this->handleForm();
        return $this->result;
    }

    /**
     * Encode result to JSON
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->getResult());
    }

    /**
     * Alias to __toString
     * @return string
     */
    public function toJSON()
    {
        return $this->__toString();
    }

    /**
     * Handles current form
     * @throws Exception if form is not set
     */
    protected function handleForm()
    {
        if (!$this->form) {
            throw new Exception('Form is not set.');
        }
        $filterCallback = function($item){
            return !empty($item);
        };
        $this->result = array('rules' => array(), 'messages' => array());

        $this->walkForm($this->form);

        $this->result['rules'] = array_filter($this->result['rules'], $filterCallback);
        $this->result['messages'] = array_filter($this->result['messages'], $filterCallback);
    }

    /**
     * Recursively walks through form and it's subforms and retrieves data for each their's element
     * @param \Zend_Form $form
     */
    protected function walkForm(\Zend_Form $form)
    {
        $result = &$this->result;

        foreach ($form->getElements() as $element) {
            $result['rules'] = array_merge($result['rules'], $this->elementHandler->getRules($element));
            $result['messages'] = array_merge($result['messages'], $this->elementHandler->getMessages($element));
        }

        foreach($form->getSubForms() as $subForm)
        {
            $this->walkForm($subForm);
        }

    }
} 