<?php
/**
 * Author: Alex P.
 * Date: 15.08.14
 * Time: 12:18
 */

namespace Ikantam\ZendFormToJqueryValidation;


/**
 * Class ValidatorsMap
 * @package Ikantam\ZendFormToJqueryValidation
 */
class ValidatorsMap
{
    /**
     * Rules and messages related to some validator classes
     * @var array
     */
    protected $map = array();

    /**
     *  Init the class
     */
    public function __construct()
    {
        $this->initDefaultMap();
    }

    /**
     * Sets map element
     * @param string $validatorClassName (Zend_Validate_... or My\ZendValidateInterface\Class)
     * @param array|\Closure $jsValidator - must results array contains jQueryValidation rule
     * @param string|array|\Closure $errorMessage - array is 'ruleName'=>'message' which
     * is preferred due to performance reasons. Resulting array could contain multiple values
     * since one Zend validator could be used for multiple jQuery validators (Zend_Validate_StringLength for instance)
     * @return $this
     */
    public function setValidatorMapElement($validatorClassName, $jsValidator, $errorMessage)
    {
        $this->map[$validatorClassName] = array(
            'twin' => $jsValidator,
            'message' => $errorMessage
        );

        return $this;
    }

    /**
     * Retrieves map element for validator
     * @param \Zend_Validate_Interface $validator
     * @return null|array
     */
    public function getMap(\Zend_Validate_Interface $validator)
    {
        $className = get_class($validator);
        return isset($this->map[$className]) ? $this->map[$className] : null;
    }

    /**
     * Retrieves rules for validator
     * @param \Zend_Validate_Interface $validator
     * @return array
     */
    public function getRules(\Zend_Validate_Interface $validator)
    {
        $map = $this->getMap($validator);
        $rules = array();
        if (isset($map['twin'])) {
            $twin = $map['twin'];
            if (is_array($twin)) {
                $rules = $twin;
            } elseif($twin instanceof \Closure) {
                $rules = $twin($validator);
            }
        }

        return $rules;
    }

    /**
     * Retrieves messages for validator
     * @param \Zend_Validate_Interface $validator
     * @return array
     * @throws Exception
     */
    public function getMessages(\Zend_Validate_Interface $validator)
    {
        $map = $this->getMap($validator);
        $messages = array();
        if (isset($map['message']))  {
            $message = $map['message'];
            if (is_array($message)) {
                $messages = $message;
            } elseif (is_string($message)) {
                $rules = $this->getRules($validator);
                foreach ($rules as $name => $param) {
                    $messages[$name] = $message;
                }
            } elseif ($message instanceof \Closure) {
                $messages = $message($validator);
                if (!is_array($messages)) {
                    throw new Exception('Function must return array for ' .
                        'error messages "' . gettype($messages). '" obtained.');
                }
            }
        }

        return $messages;
    }

    /**
     * Sets map element for Zend validators
     */
    protected function initDefaultMap()
    {
        $this->setValidatorMapElement(
            'Zend_Validate_NotEmpty',
            array('required' => true),
            array('required' => 'Value is required and can\'t be empty.')
        );

        $this->setValidatorMapElement(
            'Zend_Validate_StringLength',
            function(\Zend_Validate_StringLength $validator){
                $rules = array();
                if (null !== ($min = $validator->getMin())) {
                    $rules['minlength'] = $min;
                }
                if (null !== ($max = $validator->getMax())) {
                    $rules['maxlength'] = $max;
                }
                return $rules;
            },
            array(
                'minlength' => 'At least {0} characters required.',
                'maxlength' => 'Please enter no more than {0} characters.'
            )
        );

        $this->setValidatorMapElement(
            'Zend_Validate_LessThan',
            function(\Zend_Validate_LessThan $validator) {
                return array('max' => $validator->getMax());
            },
            array('max' => 'Value must be less than {0}.')
        );

        $this->setValidatorMapElement(
            'Zend_Validate_GreaterThan',
            function(\Zend_Validate_GreaterThan $validator) {
                return array('min' => $validator->getMin());
            },
            array('min' => 'Value must be greater than or equal to {0}.')
        );

        $this->setValidatorMapElement(
            'Zend_Validate_Digits',
            array('digits' => true),
            array('digits' => 'Field accepts only digits.')
        );

        $this->setValidatorMapElement(
            'Zend_Validate_EmailAddress',
            array('email' => true),
            array('email' => 'Invalid email address.')
        );

    }
} 