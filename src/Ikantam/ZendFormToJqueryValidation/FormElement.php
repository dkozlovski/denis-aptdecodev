<?php
/**
 * Author: Alex P.
 * Date: 15.08.14
 * Time: 14:08
 */

namespace Ikantam\ZendFormToJqueryValidation;


/**
 * Class FormElement
 * @package Ikantam\ZendFormToJqueryValidation
 */
class FormElement
{
    /**
     * Represents rules
     */
    const DATA_TYPE_RULES = 'getRules';
    /**
     * Represents messages
     */
    const DATA_TYPE_MESSAGES = 'getMessages';

    /**
     * @var ValidatorsMap
     */
    protected $map;

    /**
     * @param ValidatorsMap $map
     */
    public function __construct(ValidatorsMap $map)
    {
        $this->map = $map;
    }

    /**
     * Retrieves rules for element
     * @param \Zend_Form_Element $element
     * @return array
     */
    public function getRules(\Zend_Form_Element $element)
    {
        return $this->getElementData($element, self::DATA_TYPE_RULES);
    }

    /**
     * Retrieves error messages for element
     * @param \Zend_Form_Element $element
     * @return array
     */
    public function getMessages(\Zend_Form_Element $element)
    {
        return $this->getElementData($element, self::DATA_TYPE_MESSAGES);
    }

    /**
     * @param \Zend_Form_Element $element
     * @param $type
     * @return array
     */
    protected  function getElementData(\Zend_Form_Element $element, $type)
    {
        $data = array();
        if ($element->isRequired() && !$element->getValidator('NotEmpty')) {
            $element->addValidator('NotEmpty');
        }
        foreach ($element->getValidators() as $validator) {
            $data = array_merge($data, $this->map->{$type}($validator));
        }

        return array($element->getFullyQualifiedName() => $data);
    }
} 