<?php
/**
 * Author: Alex P.
 * Date: 22.08.14
 * Time: 11:14
 */

namespace Core\Service;


use \Core\GoogleShoppingContent\ProductConverter;

/**
 * Class GoogleShoppingContent
 * @package Core\Service
 */
class GoogleShoppingContent
{
    /**
     * @var \Core\GoogleShoppingContent\ProductConverter
     */
    protected $productConverter;

    /**
     * @var \Google_Service_ShoppingContent
     */
    protected $service;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var array
     */
    protected $errors = array();

    /**
     * @var \Zend_Validate_Interface
     */
    protected $availabilityValidator;

    /**
     * @var \Zend_Log
     */
    protected $logger = null;

    /**
     * EAV key
     */
    const REST_ID_ATTRIBUTE = 'google_rest_id';

    /**
     * Dependencies
     * @param ProductConverter $converter
     * @param \Google_Service_ShoppingContent $service
     * @param array $config
     * @param \Zend_Validate_Interface $availabilityValidator
     */
    public function __construct(
        ProductConverter $converter,
        \Google_Service_ShoppingContent $service,
        array $config,
        \Zend_Validate_Interface $availabilityValidator
    ) {
        $this->productConverter = $converter;
        $this->service = $service;
        $this->config = $config;
        $this->availabilityValidator = $availabilityValidator;
    }

    /**
     * Insert single product to shopping content
     * @param \Product_Model_Product $product
     * @return bool
     */
    public function insertProduct(\Product_Model_Product $product)
    {
        if (!$this->availabilityValidator->isValid($product)) {
            // @TODO: log validator errors
            return false;
        }
        if (!$googleProduct = $this->convertOne($product)) {
            if (isset($this->logger)) {
                $message = 'Product ID: ' . $product->getId() . " was not inserted.\n";
                $this->logger->log($message . $this->productConverter->getErrorsString("\n"), \Zend_Log::ERR);

            }
            return false;
        }

        $response = $this->service->products->insert(
            $this->config['merchantId'],
            $googleProduct,
            array('dryRun' => @$this->config['dryRun'])
        );

        $this->logWarnings($response->getWarnings(), $product->getId());

        if ($restId = $response->getId()) {
            $product->setEAVAttributeValue(self::REST_ID_ATTRIBUTE, $restId, 'varchar');
            $product->save();
            return $restId;
        }

        return false;
    }

    /**
     * Update existing product
     * @param \Product_Model_Product $product
     * @return bool
     */
    public function updateProduct(\Product_Model_Product $product)
    {
        if (!$product->getEAVAttributeValue(self::REST_ID_ATTRIBUTE)) {
            if (isset($this->logger)) {
                $this->logger->log('Can not update product ID:' . $product->getId() . '. No google id', \Zend_Log::ERR);
            }
            $this->error('notInserted', 'Product can not be updated. Can not fetch google product id');
            return false;
        }

        return (bool)$this->insertProduct($product);
    }

    /**
     * Delete single product from google shopping content
     * @param \Product_Model_Product $product
     * @return bool
     */
    public function deleteProduct(\Product_Model_Product $product)
    {
        if (!$restId = $product->getEAVAttributeValue(self::REST_ID_ATTRIBUTE)) {
            if (isset($this->logger)) {
                $this->logger->log('Can not delete product ID:' . $product->getId() . '. No google id', \Zend_Log::ERR);
            }
            $this->error('notInserted', 'Product can not be deleted. Can not fetch google product id');
            return false;
        }

        $response = $this->service->products->delete(
            $this->config['merchantId'],
            $restId, array('dryRun' => @$this->config['dryRun'])
        );

        // The response for a successful delete is empty
        if (empty($response)) {
            $this->deleteRestId($product);
            return true;
        }

        return false;
    }

    /**
     * Insert multiple products in one request
     * @param \Product_Model_Product_Collection $products
     * @param bool $isUpdateRequest
     * @return bool
     */
    public function insertProductBatch(\Product_Model_Product_Collection $products, $isUpdateRequest = false)
    {
        if (!$products->getSize()) {
            $this->error('empty', 'Empty collection.');
            if (isset($this->logger)) {
                $this->logger->log('Can not insert batch: Empty collection.', \Zend_Log::ERR);
            }
            return false;
        }

        $requestType = $isUpdateRequest ? 'update' : 'insert';

        if ($isUpdateRequest) {
            $products->getFlexFilter()
                ->id('in', $products->getColumn('id'))
                ->byEAVAttribute($this->getRestIdEAVAttribute(), 'IS_NOT_NULL')
                ->apply();

            $restIds = $products->getColumns(array(self::REST_ID_ATTRIBUTE), true);
        }

        // Only products which passed validation
        $products->itemsFilter(array($this->availabilityValidator, 'isValid'));
        // @TODO: log validator errors

        $entries = array();

        foreach ($this->convertCollection($products) as $googleProduct){
            if ($isUpdateRequest) {
                $googleProduct->setId($restIds[$googleProduct->getOfferId()][self::REST_ID_ATTRIBUTE]);
            }
            $entries[] = $this->newBatchEntry($googleProduct);
        }

        if (count($entries)) {
            $request = $this->newBatchRequest($entries, $requestType);
            $response = $this->service->products->custombatch($request, array('dryRun' => @$this->config['dryRun']));
            //$this->logResponse($response, strtoupper($requestType));
            if (!$isUpdateRequest && isset($response['entries'])) {
                foreach ($response['entries'] as $entry) {
                    /** @var \Google_Service_ShoppingContent_Product $googleProduct */
                    $googleProduct = $entry->getProduct();
                    if (($restId = $googleProduct->getId()) && ($product = $products->firstMatched(function($item) use ($googleProduct){
                                return $item->getId() == $googleProduct->getOfferId();
                            }))) {
                        /** @var \Product_Model_Product $product */
                        $product->setEAVAttributeValue(self::REST_ID_ATTRIBUTE, $restId)->save();
                    }
                }
            }

        }

        return true;
    }

    /**
     * Update multiple products in one request
     * @param \Product_Model_Product_Collection $products
     * @return bool
     */
    public function updateProductBatch(\Product_Model_Product_Collection $products)
    { 
        return $this->insertProductBatch($products, true);
    }

    /**
     * Delete multiple products in one request
     * @param \Product_Model_Product_Collection $products
     * @return bool
     */
    public function deleteProductBatch(\Product_Model_Product_Collection $products)
    {
        if (!$products->getSize()) {
            $this->error('empty', 'Empty collection.');
            if (isset($this->logger)) {
                $this->logger->log('Can not delete batch: Empty collection.', \Zend_Log::ERR);
            }
            return false;
        }

        // List of id before using filter
        $idInCollection = $products->getColumn('id');

        // Using filter to retrieve all attributes in one query
        $restIds = $products->getFlexFilter()
            ->id('in', $products->getColumn('id'))
            ->byEAVAttribute($this->getRestIdEAVAttribute(), 'IS_NOT_NULL')
            ->apply()
            ->getColumn(self::REST_ID_ATTRIBUTE);

        $notEntryId = array_diff($idInCollection, $products->getColumn('id'));
        if (isset($this->logger)) {
            $message = "Some products were not used in delete batch request due to non existing rest id\n";
            $message .= 'IDS: ' . implode(', ', $notEntryId);
            $this->logger->log($message, \Zend_Log::WARN);
        }

        $entries = array();

        foreach ($restIds as $restId) {
            $entries[] = $this->newBatchEntry($restId, 'delete');
        }

        if (count($entries)) {
            $request = $this->newBatchRequest($entries);
            $response = $this->service->products->custombatch($request, array('dryRun' => @$this->config['dryRun']));
            //$this->logResponse($response, 'DELETE');
            if (isset($response['entries'])) {

                foreach ($response['entries'] as $entry) {
                    /** @var \Google_Service_ShoppingContent_Product $restId */
                    if ($product = $products->firstMatched(function($item) use ($entry){
                                return $item->getId() == $entry->getBatchId();
                            })) {
                        /** @var \Product_Model_Product $product */
                        $this->deleteRestId($product);
                    }
                }
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Logger writes warnings and errors
     * @param \Zend_Log $logger
     * @return $this
     */
    public function setLogger(\Zend_Log $logger)
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @param $warnings
     * @param $id
     * @return $this
     */
    protected function logWarnings($warnings, $id)
    {
        if (!isset($this->logger)) {
            return $this;
        }
        /** @var \Google_Service_ShoppingContent_Error $warning */
        foreach ($warnings as $warning) {
            $message = sprintf("ID:%s [%s] %s\n", $id, $warning->getReason(), $warning->getMessage());
            $this->logger->log($message, \Zend_Log::WARN);
        }

        return $this;
    }

    /**
     * @param $response
     * @param string $operation
     * @return $this
     */
    protected function logResponse($response, $operation = '')
    {
        if (isset($this->logger)) {
            /** @var \Google_Service_ShoppingContent_ProductsCustomBatchResponseEntry $responseEntry */
            foreach ($response->getEntries() as $responseEntry) {
                /** @var \Google_Service_ShoppingContent_Product $googleProduct */
                $googleProduct = $responseEntry->getProduct();
                if ($errors = @$responseEntry->getErrors()) {
                    $message = sprintf(
                        $operation . "Product ID: %s got errors. \n%s",
                        $responseEntry->getBatchId(),
                        implode("\n", $errors)
                    );
                    $this->logger->log($message, \Zend_Log::ERR);
                } else {
                    if (is_object($googleProduct)) {
                        $this->logWarnings($googleProduct->getWarnings(), $googleProduct->getOfferId());
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @param \Product_Model_Product $product
     * @return bool|\Google_Service_ShoppingContent_Product
     */
    protected function convertOne(\Product_Model_Product $product)
    {
        $converted = $this->productConverter->convert($product);
        if ($this->productConverter->hasErrors()) {
            return false;
        }

        if (empty($converted)) {
            return false;
        }

        /** @var \Google_Service_ShoppingContent_Product $googleProduct */
        $googleProduct = array_shift($converted);
        if ($restId = $product->getEAVAttributeValue(self::REST_ID_ATTRIBUTE)) {
            // Update product
            $googleProduct->setId($restId);
        }

        return $googleProduct;
    }

    /**
     * @param \Product_Model_Product_Collection $products
     * @return array
     */
    protected function  convertCollection(\Product_Model_Product_Collection $products)
    {
        $googleProducts = $this->productConverter->convert($products);

        if ($this->productConverter->hasErrors() && isset($this->logger)) {
            foreach ($this->productConverter->getErrors() as $id => $errors) {
                $errorsString = '';
                foreach ($errors as $errList) {
                        $errorsString .= implode("\n", $errList);
                }

                $message = sprintf("Product ID: %s was not converted. \n%s", $id, $errorsString);
                $this->logger->log($message, \Zend_Log::ERR);
            }
        }

        return $googleProducts;
    }


    /**
     * @param $googleProduct
     * @param string $method
     * @return \Google_Service_ShoppingContent_ProductsCustomBatchRequestEntry
     */
    protected function newBatchEntry($googleProduct, $method = 'insert')
    {
        $entry = new \Google_Service_ShoppingContent_ProductsCustomBatchRequestEntry();

        if ($googleProduct instanceof \Google_Service_ShoppingContent_Product) {
            $entry->setProduct($googleProduct);
            $entry->setBatchId($googleProduct->getOfferId());
        } elseif (is_string($googleProduct)) {

            $length = strlen($googleProduct) - strrpos($googleProduct, ':');
            $productId = substr($googleProduct, strrpos($googleProduct, ':') + 1, $length);
            $entry->setProductId($googleProduct);
            $entry->setBatchId($productId);
        }

        $entry->setMethod($method);
        $entry->setMerchantId($this->config['merchantId']);

        return $entry;
    }

    /**
     * @param array $entries
     * @return \Google_Service_ShoppingContent_ProductsCustomBatchRequest
     */
    protected function newBatchRequest(array $entries)
    {
        $batchRequest = new \Google_Service_ShoppingContent_ProductsCustomBatchRequest();
        $batchRequest->setEntries($entries);

        return $batchRequest;
    }

    /**
     * @return \Application_Model_EAV_Attribute
     */
    protected function getRestIdEAVAttribute()
    {
        $attr = new \Application_Model_EAV_Attribute('varchar', self::REST_ID_ATTRIBUTE);
        return $attr;
    }

    /**
     * @param \Product_Model_Product $product
     * @return $this
     */
    protected function deleteRestId(\Product_Model_Product $product)
    {
        if (@!$this->config['dryRun']) {
            $eav = new \Ikantam_EAV($product, $this->getRestIdEAVAttribute(), new \Application_Model_EAV_DB_MySql());
            $eav->deleteValue();
        }
        return $this;
    }

    /**
     * @param $key
     * @param $message
     * @return $this
     */
    protected function error($key, $message)
    {
        $this->errors[$key] = $message;
        return $this;
    }
}
