<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 15.09.14
 * Time: 12:15
 */

namespace Core\Service;


class ThirdPartyWidget
{
    protected $config;

    protected $view;

    protected $user;

    protected $eavStorage;

    public  function __construct(
        array $config,
        \Zend_View $view,
        \Application_Model_User $user,
        \Ikantam_EAV_Interface_DB $eavStorage
    ) {
        $this->config = $config;
        $this->view = $view;
        $this->user = $user;
        $this->eavStorage = $eavStorage;

        $view->setBasePath($config['view.basePath']);
        $view->setScriptPath($config['view.scriptPath']);

    }

    public function getWidget($path, $assignments = array())
    {
        $cfg = &$this->config;

        if (!isset($cfg[$path])) {
            throw new \InvalidArgumentException('Path is not defined.');
        }

        $pathRoot = substr($path, 0, strpos($path, '.'));

        if ($pathRoot && isset($cfg[$pathRoot . '.config'])) {
            $this->view->assign($cfg[$pathRoot . '.config']);
        }

        $widgetCfg = $cfg[$path];
        $scriptPath = ltrim($widgetCfg['view_script'], '/..\\');

        if (
            $cfg['enabled'] === false ||
            $widgetCfg['enabled'] === false ||
            ($cfg['allowTest'] === false && $cfg['ENV'] !== 'live')
        ) {
            return '';
        } elseif (isset($widgetCfg['trackHits'])) {
            $currentHits = $this->getHits($path);
            $limitHits = isset($widgetCfg['limitHits']) ? $widgetCfg['limitHits'] : false;
            if ($limitHits && $limitHits >= $currentHits) {
                return '';
            }
            // increment hits
            $this->addHit($path, $currentHits);
            $this->view->hits = $currentHits + 1;
        }

        if (isset($widgetCfg['markShow']) && $widgetCfg['markShow'] === true) {
            if (!$currentHits = $this->getHits($path)) {
                $this->addHit($path, 0);
            }
            $this->view->isFirstHit = !$currentHits;
        }

        $this->view->assign($assignments);
        return $this->view->render($scriptPath);

    }

    protected function addHit($path, $currentHits)
    {
        if ($this->user->isExists()) {
            $eav = new \Ikantam_EAV(
                $this->user,
                $this->widgetHitsEAVAttribute($path),
                $this->eavStorage
            );
            $eav->saveValue($currentHits + 1);
        }
        return $this;
    }

    protected function getHits($path)
    {
        if (!$this->user->isExists()) {
            return 0;
        }
        $eav = new \Ikantam_EAV(
            $this->user,
            $this->widgetHitsEAVAttribute($path),
            $this->eavStorage
        );

        return $eav->getValue() ?: 0;
    }

    protected function widgetHitsEAVAttribute($path)
    {
        $name = str_replace('.', '_', $path) . '_hits';
        return new \Application_Model_EAV_Attribute('smallint', $name);
    }
}
