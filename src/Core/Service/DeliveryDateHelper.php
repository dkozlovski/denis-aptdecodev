<?php
/**
 * Author: Alex P.
 * Date: 04.08.14
 * Time: 20:10
 */

namespace Core\Service;



use \Admin_Model_DeliveryDate_FlexFilter as DateFilter;

/**
 * Class DeliveryDateHelper
 * @package Core\Service
 */
class DeliveryDateHelper
{
    /**
     * @var DateFilter $filter
     */
    protected $filter;

    /**
     * Options to configure filter
     * @var array
     */
    protected $options = array(
        'include_small' => true,
        'include_default' => true,
        'from' => 'now',
        'till' => null,
        'return_count' => false,
        'limit' => null,
    );

    /**
     * @param DateFilter $filter
     */
    public function __construct(DateFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * Sets flag value
     *
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setOption($name, $value)
    {
        if (array_key_exists($name, $this->options)) {
            $this->options[$name] = $value;
        }
        return $this;
    }

    /**
     * Retrieves flag value
     *
     * @param string $name
     * @return mixed
     */
    public function getOption($name)
    {
        if (array_key_exists($name, $this->options)) {
            return $this->options[$name];
        }
        return null;
    }

    /**
     * Set flag which indicates if need to include dates where shipping for small items is available
     *
     * @param $flag
     * @return $this
     */
    public function setIncludeSmall($flag)
    {
        $this->setOption('include_small', (bool)$flag);
        return $this;
    }

    /**
     * Set flag which indicates if need to include dates where default shipping is available
     *
     * @param bool $flag
     * @return $this
     */
    public function setIncludeDefault($flag)
    {
        $this->setOption('include_default', (bool)$flag);
        return $this;
    }

    /**
     * Set begin date
     *
     * @param int|string $stamp
     * @return $this
     */
    public function setFrom($stamp)
    {
        if ($stamp === '') {
            $stamp = time();
        }
        $this->setOption('from', $stamp);
        return $this;
    }

    /**
     * Set end date
     *
     * @param int|string $stamp
     * @return $this
     */
    public function setTill($stamp)
    {
        if ($stamp === '') {
            $stamp = null;
        }
        $this->setOption('till', $stamp);
        return $this;
    }

    /**
     * Special flag which indicates whether "dateIntersect" will return count or Collection
     *
     * @param bool $flag - if set to true, count will be returned
     * @return $this
     */
    public function setReturnCount($flag)
    {
        $this->setOption('return_count', (bool)$flag);
        return $this;
    }

    public function setLimit($limit)
    {
        $this->setOption('limit', $limit);
        return $this;
    }

    /**
     * Finds how many delivery days conforms to conditions
     *
     * @param int|string (optional) $till
     * @return \Admin_Model_DeliveryDate_Collection|int - depends on "return_count" flag
     */
    public function dateIntersect($till = null)
    {
        if (null !== $till) {
            $this->setTill($till);
        }

        $this->configureFilter();

        return $this->getOption('return_count') ?
            $this->filter->count() : $this->filter->apply($this->getOption('limit'));
    }

    /**
     * Converts specified value to Unix Timestamp
     *
     * @param $value
     * @return int
     * @throws \InvalidArgumentException
     */
    protected function toTimestamp($value)
    {
        if (is_numeric($value)) {
            return (int)$value;
        } elseif (is_string($value)) {
            if(($value = strtotime($value)) !== false) {
                return $value;
            }
        }
        throw new \InvalidArgumentException('Value can not be converted to timestamp.');
    }

    /**
     * Configures filter using options
     * @return $this
     */
    protected function configureFilter()
    {
        $filter = $this->filter;
        $filter->clear();

        $from = $this->toTimestamp($this->getOption('from'));
        $till = $this->getOption('till');
        if (null === $till) {
            $till = time();
        } else {
            $till = $this->toTimestamp($till);
        }

        $filter->date('between', $from, $till);

        if ($includeDefault = $this->getOption('include_default')) {
            $filter->startGroup()
                ->is_first_period_available('=', 1)
                ->or_is_second_period_available('=', 1)
                ->or_is_third_period_available('=',1);

        }

        if ($this->getOption('include_small')) {
            $filter->startGroup();
            if ($includeDefault) {
                $filter->or_is_first_period_for_small_available('=', 1);
            } else {
                $filter->is_first_period_for_small_available('=', 1);
            }

            $filter->or_is_second_period_for_small_available('=', 1)
                ->or_is_third_period_for_small_available('=',1);
        }

        // if both of types set as "not include" then configure filter to find days where delivery is unavailable
        if (!$includeDefault && !$this->getOption('include_small')) {
            $filter->is_first_period_available('=', 0)
                ->is_second_period_available('=', 0)
                ->is_third_period_available('=', 0)
                ->is_first_period_for_small_available('=', 0)
                ->is_second_period_for_small_available('=', 0)
                ->is_third_period_for_small_available('=', 0);
        }

        return $this;
    }
}
