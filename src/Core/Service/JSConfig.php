<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 06.08.14
 * Time: 17:04
 */

namespace Core\Service;


/**
 * Class JSConfig
 * @package Core\Service
 */
class JSConfig
{
    /**
     * Class must implement encode method
     * @var object
     */
    protected $encoder;

    /**
     * Options which will be encoded
     * @var array
     */
    protected $options = array();

    /**
     * @param object $encoder
     */
    public function __construct($encoder)
    {
        $this->setEncoder($encoder);
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setOption($name, $value)
    {
        $this->options[$name] = $value;
        return $this;
    }

    /**
     * @param $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function addOption($name, $value)
    {
        if (!array_key_exists($name, $this->options)) {
            $this->setOption($name, $value);
        }
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function removeOption($name)
    {
        unset($this->options[$name]);
        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->encoder->encode($this->options);
    }

    /**
     * @param $encoder
     * @throws \InvalidArgumentException
     */
    protected function setEncoder($encoder)
    {
        if (!is_object($encoder) || !method_exists($encoder, 'encode')) {
            throw new \InvalidArgumentException('Invalid encoder. Encoder must implement "encode" method');
        }

        $this->encoder = $encoder;
    }

} 