<?php
/**
 * Author: Alex P.
 * Date: 25.08.14
 * Time: 18:56
 */

namespace Core\GoogleShoppingContent;


/**
 * Class TokenStorage
 * Hard depended on \Application_Model_NVP
 * @package Core\GoogleShoppingContent
 */
class TokenStorage
{
    /**
     * Name with which value will be written and retrieved
     * @var string
     */
    protected $optionName = 'google_shopping_content_access_token';

    /**
     * Get token
     * @return string
     */
    public function get()
    {
        return \Application_Model_NVP::option($this->optionName);
    }

    /**
     * Save token
     * @param string $value
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function set($value)
    {
        if (!is_string($value) || empty($value)) {
            throw new \InvalidArgumentException('Token must be a string.');
        }
        return \Application_Model_NVP::option($this->optionName, $value);
    }
} 