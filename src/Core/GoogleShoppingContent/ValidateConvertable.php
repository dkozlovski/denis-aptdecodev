<?php
/**
 * Author: Alex P.
 * Date: 22.08.14
 * Time: 12:37
 */

namespace Core\GoogleShoppingContent;


use \Zend_Validate_Abstract;

class ValidateConvertable extends Zend_Validate_Abstract
{
    const DESCRIPTION = 'noDescription';
    const DESCRIPTION_URL = 'descriptionContainsUrl';
    const IMAGE = 'noImage';
    const NOT_EXISTS = 'notExistingProduct';
    const CATEGORY = 'unresolvedCategory';

    protected $urlValidator ;

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::DESCRIPTION     => 'Product does not have a description. Google only accepts products with description.',
        self::DESCRIPTION_URL    => 'Product description should not contain any url.',
        self::IMAGE => 'Product must have an image.',
        self::NOT_EXISTS => 'Product is not exists.',
        self::CATEGORY => 'Unresolved Google Merchant category:
            no relation between google category and product category.'
    );

    /**
     * List of relations between Google Merchant categories and Application categories
     * @var array
     */
    protected  $categoriesMap;

    public function __construct(array $categoriesMap)
    {
        $this->categoriesMap = $categoriesMap;
    }

    /**
     * Returns true if and only if $product meets the validation requirements
     *
     * If $product fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  \Product_Model_Product $product
     * @throws \InvalidArgumentException
     * @return boolean
     */
    public function isValid($product)
    {
        if (!$product instanceof \Product_Model_Product) {
            throw new \InvalidArgumentException('Only products might be checked with this validator.');
        }
        $this->_setValue($product);

        if (!$product->isExists()) {
            $this->_error(self::NOT_EXISTS);
            return false;
        }

        if (!isset($this->categoriesMap[$product->getCategoryId()])) {
            $this->_error(self::CATEGORY);
            return false;
        }

        if (!$description = $product->getDescription()) {
            $this->_error(self::DESCRIPTION);
            return false;
        } elseif ($this->containsUrl($description)) {
            $this->_error(self::DESCRIPTION_URL);
            return false;
        }

        if (!$product->getMainImage()->isExists()) {
            $this->_error(self::IMAGE);
            return false;
        }

        return true;
    }

    /**
     * Checks whether string contains url
     * @param string $str
     * @return bool
     */
    protected function containsUrl($str)
    {
        $pattern = '((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]' .
            '+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)';

        return (bool)preg_match($pattern, $str);

    }

}