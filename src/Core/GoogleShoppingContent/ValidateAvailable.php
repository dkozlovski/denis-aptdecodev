<?php
/**
 * Author: Alex P.
 * Date: 28.08.14
 * Time: 18:06
 */

namespace Core\GoogleShoppingContent;


use \Zend_Validate_Abstract;

class ValidateAvailable extends Zend_Validate_Abstract
{
    const EXPIRED = 'expired';
    const NOT_PUBLISHED = 'notPublished';
    const SOLD = 'sold';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::EXPIRED => 'Product is expired and can not be inserted.',
        self::NOT_PUBLISHED => 'Product is not published and can not be inserted.',
        self::SOLD => 'Product is sold.'
    );

    /**
     * Returns true if and only if $product meets the validation requirements
     *
     * If $product fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  \Product_Model_Product $product
     * @throws \InvalidArgumentException
     * @return boolean
     */
    public function isValid($product)
    {
        /** @var \Product_Model_Product $product */
        if (!$product instanceof \Product_Model_Product) {
            throw new \InvalidArgumentException('Only products might be checked with this validator.');
        }
        $this->_setValue($product);

        if ($product->getIsSold()) {
            $this->_error(self::SOLD);
            return false;
        }

        if (!$product->getIsPublished() || !$product->getIsApproved() || !$product->getIsVisible()) {
            $this->_error(self::NOT_PUBLISHED);
            return false;
        }

        if ($product->getTimeToLife() < 0) {
            $this->_error(self::EXPIRED);
            return false;
        }

        return true;

    }
}