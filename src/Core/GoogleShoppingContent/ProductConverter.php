<?php
/**
 * Author: Alex P.
 * Date: 22.08.14
 * Time: 11:15
 */

namespace Core\GoogleShoppingContent;


use Zend_Validate_Interface;

/**
 * Class ProductConverter
 * @package Core\GoogleShoppingContent
 */
class ProductConverter
{
    // The products will be sold online
    const CHANNEL = 'online';
    // The product details are provided in English
    const CONTENT_LANGUAGE = 'en';
    // The products are sold in the United States
    const TARGET_COUNTRY = 'US';

    const CURRENCY = 'USD';

    const DATE_FORMAT = \DateTime::ISO8601;

    /**
     * Describes relations
     * @var array
     */
    protected $categoriesMap;

    /**
     * Results of convert
     * @var array
     */
    protected $convertedProducts = array();

    /**
     * Used to check if product could be converted
     * @var \Zend_Validate_Interface
     */
    protected $validator;

    /**
     * Contains errors in case validator rejected product(s)
     * @var array
     */
    protected $errors = array();

    /**
     * @var \DateTime
     */
    protected $dateTime;

    /**
     * @param array $categoriesMap
     * @param Zend_Validate_Interface $validator
     */
    public function __construct(array $categoriesMap, Zend_Validate_Interface $validator)
    {
        $this->categoriesMap = $categoriesMap;
        $this->validator = $validator;
        $this->dateTime = new \DateTime;
    }

    /**
     * Converts product(s) to appropriate Google product
     * @param array|\Product_Model_Product|\Product_Model_Product_Collection $product
     * @return array
     * @throws \InvalidArgumentException
     */
    public function convert($product)
    {
        $this->convertedProducts = array();

        if ($product instanceof \Product_Model_Product) {
            $this->convertProduct($product);

        } elseif ($product instanceof \Product_Model_Product_Collection) {
            foreach($product as $item) {
                $this->convertProduct($item);
            }
        } elseif (is_array($product)) {
            foreach ($product as $element) {
                $this->convert($element);
            }
        } else {
            throw new \InvalidArgumentException('Given parameter cannot be converted.');
        }

        return $this->convertedProducts;
    }

    /**
     * Retrieves list of errors
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function getErrorsString($separator = ', ')
    {
        $errString = '';
        foreach ($this->errors as $list) {
            foreach ($list as $productErrors) {
                $errString .= implode($separator, $productErrors);
            }
        }

        return $errString;
    }

    /**
     * Indicates whether were errors while converting
     * @return bool
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }

    /**
     * Converts product
     * @param \Product_Model_Product $product
     * @return $this
     */
    protected function convertProduct(\Product_Model_Product $product)
    {
        if (!$this->validator->isValid($product)) {
            $this->errors[$product->getId('noId')][] = $this->validator->getMessages();
            return $this;
        }

        $googleProduct = $this->newGoogleProduct();

        $googleProduct->setOfferId($product->getId());
        $googleProduct->setTitle($product->getTitle());
        $googleProduct->setDescription($product->getDescription());

        $googleProduct->setLink($product->getProductUrl());
        $googleProduct->setImageLink($product->getMainImageUrl());
        $googleProduct->setContentLanguage(self::CONTENT_LANGUAGE);
        $googleProduct->setTargetCountry(self::TARGET_COUNTRY);
        $googleProduct->setChannel(self::CHANNEL);

        // Product always "in stock". Application deletes it when it sold.
        $googleProduct->setAvailability('in stock');
        $googleProduct->setCondition($product->getCondition() === 'new' ? 'new' : 'used');
        $googleProduct->setGoogleProductCategory($this->getGoogleCategoryByCategoryId($product->getCategoryId()));

        $googleProduct->setIdentifierExists(false);
        $googleProduct->setBrand($product->getManufacturer()->getTitle());

        //Expiration date
        $dates = array_filter(array($product->getAvailableTill(), $product->getExpireAt()));
        if (!empty($dates)) {
            $googleProduct->setExpirationDate($this->formatDate(min($dates)));
        }

        $googleProduct->setMaterial($product->getMaterial()->getTitle());

        $googleProduct->setColor($product->getColor()->getTitle());

        $price = $this->newGooglePrice();
        $price->setValue($this->formatPrice($product->getPrice()));
        $price->setCurrency(self::CURRENCY);

        $googleProduct->setPrice($price);

        $shippingPrice = $this->newGoogleShippingPrice();
        $shippingPrice->setValue(
            $this->formatPrice(\Shipping_Model_Rate::getFlatRate($product->getIsUnder15Pounds()))
        );
        $shippingPrice->setCurrency(self::CURRENCY);

        $shipping = $this->newGoogleShippingContent();
        $shipping->setPrice($shippingPrice);
        $shipping->setCountry(self::TARGET_COUNTRY);
        $shipping->setService('Aptdeco Moving Partner (NYC only)');

        $googleProduct->setShipping(array($shipping));
        $googleProduct->setProductType($product->getCategory()->getTitle());

        // Retrieve all visible images include main
        $images = $product->getImagesFilter()->is_visible('=', 1)->apply(10);

        // Make their urls list
        $imageLinks = array_map(function($item){
                /** @var \Product_Model_Image $item */
                return $item->getS3Url(1500, 1500, 'frame');
            }, $images->getItems());

        $googleProduct->setAdditionalImageLinks($imageLinks);

        $this->setCustomAttributes($product, $googleProduct);

        $this->convertedProducts[] = $googleProduct;

        return $this;
    }

    /**
     * Formats number, uses "." as decimals separator, avoids thousands separator, truncates for 2 signs
     * @param $price
     * @return string
     */
    protected function formatPrice($price)
    {
        return number_format($price, 2, '.', '');
    }

    /**
     * Converts UNIX timestamp to specified format (see self::DATE_FORMAT)
     * @param int|string $timestamp
     * @return string
     */
    protected function formatDate($timestamp)
    {
        return $this->dateTime->setTimestamp($timestamp)->format(self::DATE_FORMAT);
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    protected function getGoogleCategoryByCategoryId($categoryId)
    {
        return $this->categoriesMap[$categoryId]['google_category_name'];
    }

    /**
     * @return \Google_Service_ShoppingContent_Product
     */
    protected function newGoogleProduct()
    {
        return new \Google_Service_ShoppingContent_Product;
    }

    /**
     * @return \Google_Service_ShoppingContent_Price
     */
    protected function newGooglePrice()
    {
        return new \Google_Service_ShoppingContent_Price;
    }

    /**
     * @return \Google_Service_ShoppingContent_ProductShipping
     */
    protected function newGoogleShippingContent()
    {
        return new \Google_Service_ShoppingContent_ProductShipping;
    }

    /**
     * @return \Google_Service_ShoppingContent_Price
     */
    protected function newGoogleShippingPrice()
    {
        return new \Google_Service_ShoppingContent_Price;
    }

    /**
     * @param \Product_Model_Product $product
     * @param \Google_Service_ShoppingContent_Product $googleProduct
     * @return $this
     */
    protected function setCustomAttributes(
        \Product_Model_Product $product,
        \Google_Service_ShoppingContent_Product $googleProduct
    ) {
        $list = array();

        foreach (array('width', 'height', 'depth') as $size) {
            if ($val = $product->getData($size)) {
                $list[] = $this->createAttribute($size, 'float', (float)$val, 'inch');
            }
        }

        if ($age = $product->getAge()) {
            $list[] = $this->createAttribute('age', 'int', (int)$age, 'year');
        }

        $googleProduct->setCustomAttributes($list);

        return $this;
    }


    /**
     * @param string $name
     * @param string $type
     * @param mixed $value
     * @param string $unit
     * @return \Google_Service_ShoppingContent_ProductCustomAttribute
     * @throws \InvalidArgumentException
     */
    protected function createAttribute($name, $type, $value, $unit = null)
    {
        $types = array("boolean", "datetimerange", "float", "group", "int", "price", "text", "time", "url");
        if (!in_array($type, $types)) {
            throw new \InvalidArgumentException('Unknown custom attribute type.');
        }
        $attribute = new \Google_Service_ShoppingContent_ProductCustomAttribute;
        $attribute->setName($name);
        $attribute->setType($type);
        $attribute->setUnit($unit);
        $attribute->setValue($value);

        return $attribute;
    }
}