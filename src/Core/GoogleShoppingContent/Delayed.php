<?php
/**
 * Author: Alex P.
 * Date: 02.09.14
 * Time: 15:12
 */

namespace Core\GoogleShoppingContent;


/**
 * Class Delayed
 * @package Core\GoogleShoppingContent
 */
class Delayed
{
    /**
     * Used to mark product as needs to be updated
     */
    const DELAYED_UPDATE_MARKER_ATTRIBUTE = 'google_shopping_content_delayed_update';

    /**
     * @var
     */
    protected $filter;

    /**
     * @param \Product_Model_Product_FlexFilter $filter
     */
    public function __construct(\Product_Model_Product_FlexFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @param \Product_Model_Product $product
     * @return $this
     */
    public function update(\Product_Model_Product $product)
    {
        $product->setEAVAttributeValue(self::DELAYED_UPDATE_MARKER_ATTRIBUTE, true)->save();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateProducts()
    {
        return $this->filter
            ->clear()
            ->byEAVAttribute($this->getEAVAttribute(self::DELAYED_UPDATE_MARKER_ATTRIBUTE), '=', 1)
            ->apply();
    }

    /**
     * @param \Product_Model_Product_Collection $products
     * @param $type
     * @return $this
     */
    public function clear(\Product_Model_Product_Collection $products, $type)
    {
        $storage = $this->getEAVStorage();
        $attr = $this->getEAVAttribute($type);
        foreach ($products as $product) {
            $eav = new \Ikantam_EAV($product, $attr, $storage);
            $eav->deleteValue();
        }

        return $this;
    }

    /**
     * @param string $type
     * @return \Application_Model_EAV_Attribute
     */
    protected function getEAVAttribute($type = self::DELAYED_UPDATE_MARKER_ATTRIBUTE)
    {
        return new \Application_Model_EAV_Attribute('smallint', $type);
    }

    /**
     * @return \Application_Model_EAV_DB_MySql
     */
    protected function getEAVStorage()
    {
        return new \Application_Model_EAV_DB_MySql;
    }

} 