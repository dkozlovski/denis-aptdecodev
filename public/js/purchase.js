(function(){
var confirmFn = {};    
$(document).ready(function(){
var lastVal = '';
var timeout = null;
var ul = $('#suggestions-list');

//Keyup event

  $('#filter-items').keyup(function(key){

    var value = $(this).val();
    value = value.trim();
    if(value != lastVal)
    {lastVal =  value;
        if(value.length > 0){      
            
     if(timeout !== null)
     {
        clearTimeout(timeout);
     }
     timeout = setTimeout(function(){
        timeout = null;
        
        $('#suggestions-list').empty(); 
        
        
        $.ajax({
  type: "POST",
  async: false,
  url: 'http://test.loc/trunk/public/manufacturer/async/purchasebrands',
  data: {'manufacturerValue':value},
  success: function(data){            
           data = $.parseJSON(data);
           insertItems(data, value);
        },
}); 


        $.ajax({
  type: "POST",
  async: false,
  url: 'http://test.loc/trunk/public/user/purchase/suggestions',
  data: {'search':value},
  success: function(data){
         data = $.parseJSON(data);
         insertFromObject(data, value);          
        },

});

  var popup = $('.items-to-filter');      
      if(ul.find('li').length > 0)
      {
        popup.fadeIn();
      } else
        {
         popup.fadeOut();   
        }      
        
     },375);

       
    }}
  });
//-----------------------------------Confirm delivery
confirmFn = function(){

    var button = $(this);
    var confirmed = false;

    
    $.post('http://test.loc/trunk/public/user/purchase/confirmdelivery', {'confirm':button.val()}, function(data){

    data = $.parseJSON(data);
    //print_r(data);
    if(data['confirmed'] === true)
    {
        confirmed = true;
        var row = button.closest('tr');
    row.find('.in-progress').removeClass('in-progress').addClass('recived').text('Received');
    button.remove();
        if($('.replacment').text() == 'In delivery')
        {
            setTimeout(function(){
                row.remove();
                
    var answer = false;      
    var postData = $('#filterForm').serializeArray();
        postData.push({name :'page', value:$('#currentPage').text() || 1});

        $.ajax({
       async: false,
       url: 'http://test.loc/trunk/public/user/purchase/loadfrompage', 
       data: postData,
       type: "POST",
       success: function(data){

            data = $.parseJSON(data);
            answer = data;
            createProductRow(data, $('#my-items-table'));

       }  
      }); 
      
     if(answer)
     {
        $('.pagination-right').html(answer.paginationControlHTML);
     }  //AJAX
                
                
                },2000);
        }        
    }
           
    });

    if($('.replacment').text() == 'In delivery')
    {
        var el = $('.replacment').parent();
        var color = el.css('backgroundColor')

       el.animate({'backgroundColor':'#E8AF3C'},500,function(){
        $(this).animate({'backgroundColor':color},1500);        
         });
    }

}

$("[name='confirm']").click(confirmFn);
}); //doc ready

 function highlightResult(sVal, resVal)
 {
    var expr = new RegExp("^("+sVal+")","i");
    resVal = resVal.replace(expr,"<b>$1</b>");
    return resVal;
 }
 
 function insertItems(items, sVal)
 {
    var list = $('#suggestions-list');
    for(i in items)
    {
      var li = document.createElement('li');
      var a  = document.createElement('a');

    
      $(a).attr('href','http://test.loc/trunk/public/user/purchase/history/brand/'+items[i]).html(highlightResult(sVal,items[i])+' <span style="font-size:10px;">(brand)</span>');
      li.appendChild(a);
      list.append(li);
    }
 }
 
 function insertFromObject(obj, sVal)
 {
    var list = $('#suggestions-list');
    
    for(i in obj['products'])
    {
      var li = document.createElement('li');
      var a  = document.createElement('a');
      $(a).attr('href','http://test.loc/trunk/public/product/view/id/'+obj['products'][i]['id']).html(highlightResult(sVal,obj['products'][i]['title'])+' <span style="font-size:10px;">(product)</span>');
      li.appendChild(a);
      list.append(li);          
    }
    
        for(i in obj['users'])
    {
      var li = document.createElement('li');
      var a  = document.createElement('a');
      $(a).attr('href','http://test.loc/trunk/public/user/purchase/history/user/'+obj['users'][i]['id']).html(highlightResult(sVal,obj['users'][i]['name'])+' <span style="font-size:10px;">(username)</span>');
      li.appendChild(a);
      list.append(li);          
    }  
 }
 
 //--------create row

function createProductRow(data, parent)
{
    data = data.products;
    $(parent).empty();
    $(parent).html('<tr class="table-title"><th></th><th></th><th class="txt-lft">Order Date</th><th class="txt-lft">Delivery Method</th><th>Confirmation</th><th>Status</th></tr>');
    for(i in data)
    {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        var td3 = document.createElement('td');
        var td4 = document.createElement('td');
        var td5 = document.createElement('td');
        var td6 = document.createElement('td');  
              
        $(td1).addClass('cart-thumb');
        $(td2).addClass('nobrdr-it');
        $(td3).addClass('date-del-method').addClass('nobrdr-it');
        $(td4).addClass('date-del-method');
        $(td6).addClass('status').addClass('in-progress');
        
        var img = document.createElement('img');
        $(img).attr({'alt':data[i].title, 'src':data[i].imgUrl}).appendTo(td1);
        
        var h3 = document.createElement('h3');
        var a = document.createElement('a');
        $(a).attr('href','http://test.loc/trunk/public/user/purchase/order/id/'+data[i].orderId).text('Order: #'+data[i].orderId).appendTo(h3);
        $(h3).appendTo(td2);
        
        var ul = document.createElement('ul');
        var li1 = document.createElement('li');
        var li2 = document.createElement('li');
        var li3 = document.createElement('li');
        var a_product = document.createElement('a');        
        $(a_product).attr('href',data[i].linkUrl).text(data[i].title);
        $(li1).append(a_product).appendTo(ul);
        var a_user = document.createElement('a');
        $(a_user).attr('href',data[i].userUrl).text(data[i].userName);
        $(li2).append(a_user).appendTo(ul);
        $(li3).text(data[i].price).appendTo(ul);
        $(ul).appendTo(td2);
        
        var p_date = document.createElement('p');
        $(p_date).text(data[i].orderDate).appendTo(td3);
        
        var p_shipping = document.createElement('p');
        $(p_shipping).text(data[i].shipping).appendTo(td4);
        
        var button_confirm = document.createElement('button');
        $(button_confirm).attr({'class':'publish', 'name':'confirm', 'value':data[i].orderItemId, 'type':'button'})
                         .text('Confirm Delivery')
                         .click(confirmFn)
                         .appendTo(td5);
        
        var p_status = document.createElement('p');
        $(p_status).text('In progress').appendTo(td6);
        

        
       $(tr).append(td1).append(td2).append(td3).append(td4).append(td5).append(td6);

       
       $(parent).append(tr);
        
    }// for in
}
 
})();