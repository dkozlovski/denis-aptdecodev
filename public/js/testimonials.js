/*! 
*  Written for     : Testimonials slider plugin
*  Author          : @IkantamCorp - @ElmahdiMahmoud
*  Updated         : 15-10-013
* Free to use under the GPLv2 license.
*/
(function ($) {
$.fn.testimonials = function (options) { 
	var settings = $.extend({
		'speed': 600
	}, options);
	var isA = false;
	
	return this.each(function () {
	var
	userFeed   = $(this),
	sliderList = userFeed.children('ul'),
	sliderItem = sliderList.children('li'),
	buttons    = userFeed.children('button'),
	sliderItew = sliderItem.eq(0).outerWidth(),
	slides 	   = sliderItem.length,

	animateSlider = function (direction) {
		sliderList.stop(true, true).animate({
			"margin-left": direction + "=" + sliderItew
		}, settings.speed, null, function () {
		isA = false;
	}); 
	},

	isAtStart = function () {
		return parseInt(sliderList.css('margin-left'), 10) === 0;
	},

	isAtEnd = function () {
	var
	imageWidth = sliderItem.first().width(),
	imageCount = sliderItem.length,
	maxMargin  = -1 * (imageWidth * (imageCount - 1));
	return parseInt(sliderList.css('margin-left'), 10) < maxMargin;
	};

	if(slides == 1) { buttons.hide(); }

	sliderList.css({
		width: slides * sliderItew
	});

	sliderItem.css({
		width: sliderItew
	});

	$(window).resize(function() {
	var 
	calc = parseInt(sliderItem.eq(0).css('padding-left')) + parseInt(sliderItem.eq(0).css('padding-right')),
	ww = jQuery('body').width();
	sliderItew = userFeed.width() - calc;	

	sliderList.css({
		width: slides * (sliderItew + calc),
		marginLeft: 0
	});
	sliderItem.css({
		width: sliderItew
	});
	sliderItew = sliderItew + calc;
	});
	
	$(window).resize();
	
	buttons.on('click', function () {
	var
	_this = $(this),
	isBackBtn = _this.hasClass('prev-btn');

	if ((isBackBtn && isAtStart()) || (!isBackBtn && isAtEnd())) {
		return;
	}
	if (!isA) {
		isA = true;
		animateSlider((isBackBtn ? '+' : '-'));
	}
	});
});
};
})(jQuery);
