(function(){
var removedItem = null;
$(document).ready(function(){



//---------Select all click    
$('#selectAll').click(function(){

    if(!$('#selectAllCb').attr('checked'))
     {
        $("input[type='checkbox']").attr('checked', false);
        $('.check-box').removeClass('checkedBox');
     } else
        {
        $("input[type='checkbox']").attr('checked', true);
        $('.check-box').addClass('checkedBox');  
        }          
calculateSubtotal();
});

//-----------check box
//alert($(".check-box input[type='checkbox']").length)
$(".check-box").click(function(){
calculateSubtotal();
});  
 
//-----------remove item
$('.removeitem').click(function(){    
    removedItem = remove($(this));

    return false;   
    });
    
$('.restore').click(function(){
    restore(removedItem);
}); 

$('body').click(function(){


});
    
});//doc ready

//subtotal change
function calculateSubtotal()
{ 
    var prices = $(".checkedBox").find("input[name='price']").serializeArray();
    var sum = 0;
    for(i in prices)
    {
        sum += parseFloat(prices[i]['value']);        
    }
    
    if(sum == 0){
    
        $('.proceed').attr('disabled',true);
    } else
        {   
            if($('.proceed').attr('disabled') == 'disabled') 
            {
                $('.proceed').attr('disabled',false);
            }        
        }
    
    
    $('#subtotal').text('$'+number_format(sum.toFixed(2),2,'.',','));
}

//-------remove and restore
function remove(element) {

    $.post('http://test.loc/trunk/public/cart/async/remove/',{'id':element.attr('data-product')},function(answ){
        var data = $.parseJSON(answ);
        var link = document.createElement('a');
        var messBlock = $('.alert');
        
        $('#productLink').remove();              
        $('#cartCount').html('<i></i>'+data['count']);
        $(link).hide().attr({'href':data['url'],'id':'productLink'}).text(data['title']).prependTo(messBlock).fadeIn(350);
        messBlock.slideDown(350);
       
    });
    var row = element.closest('.product-row');
    var tmp = row.clone();
    row.remove();
    calculateSubtotal();    
    return tmp;
}

function restore(element){

    $(element).prependTo($('table'));
    
    element.find('.removeitem').click(function(){
    removedItem = remove($(this));
    return false;  
    });

     $.post('http://test.loc/trunk/public/cart/async/restore',
     {'id':element.find("input[type='checkbox']").attr('value')},
     function(answ){
        var data = $.parseJSON(answ);
        $('#cartCount').html('<i></i>'+data['count']);
        $('.alert').slideUp(200);
     });
     calculateSubtotal();
};



function number_format( number, decimals, dec_point, thousands_sep ) 
{  
 
    var i, j, kw, kd, km; 

    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }
 
    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
 
    if( (j = i.length) > 3 ){
        j = j % 3;
    } else{
        j = 0;
    }
 
    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
 
 
    return km + kw + kd;
}


//--------debug
function debug(text) {
  ((window.console && console.log) ||
   (window.opera && opera.postError) ||
   window.alert).call(this, text);
}
var print_r = function(variable, deep, index) {
		if (variable===undefined) {alert('undefined'); return false;}
		if (deep===undefined) {var deep = 0;}
		if (index===undefined) {var index = '';} else {index+=': ';}
		var mes = '';
		var i = 0;
		var pre = '\n';
		while (i<deep) {pre+='\t'; i++;}
		if (typeof(variable)=='object') {
			mes+=pre+index+' {';
			for (index in variable) {
				mes+=print_r(variable[index], (deep+1), index);
			}
			mes+=pre+'}';
		} else {
			mes+=pre+index+variable;
		}
		if (deep) {return mes;} else {alert(mes);}
	}
    

    })();