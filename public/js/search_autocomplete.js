(function(){
  
$('document').ready(function(){

var lines = new Lines();


$("#search-bar input").keyup(function(key){
 
    var searchVal = $(this).val();
    var ul = $('#search-suggestions ul');
    
    
    if($.trim(searchVal).length >= 3)
    {
        
        $.post('http://test.loc/trunk/public/product/search/find',{'searchField':searchVal}, function(answ){
            var data =  $.parseJSON(answ);
            lines.clear();
            ul.empty();
            for(i in data)
            {   /*IMAGES TEMPORARILY STATIC*/
                lines.addLine(data[i]['title'], data[i]['url'], data[i]['image']);//+'/images/products/'+data[i]['title']+'.png');
                ul.append(lines.getLine(i).getLiElement());
            }
            $("#search-suggestions").hide();
            if(lines.length > 0)
            {
          $("#search-suggestions").slideDown();
            }            
        });               
     

                      
 
    }}); 

});//doc ready



//autocomplit line object

function Line (title, productUrl, imagePath)
{
    this.title = title || 'noName';
    this.url = productUrl || 'noUrl';
    this.imagePath = imagePath || 'noPath';
    
    this.li = document.createElement('li');
    this.imgLink = document.createElement('a');
    this.image = document.createElement('img');
    this.titleLinkWrapper = document.createElement('p');
    this.titleLink = document.createElement('a');
    
    $(this.imgLink).attr('href','url');
    $(this.image).attr({'src':this.imagePath, 'alt':this.title}).css('width','37px');
    $(this.titleLink).attr('href', this.url).text(this.title);
    
    $(this.imgLink).append(this.image);
    $(this.titleLinkWrapper).append(this.titleLink);
    $(this.li).append(this.imgLink).append(this.titleLinkWrapper)    
    
} 

Line.prototype.getLiElement = function()
{
    return this.li;
}

Line.prototype.getImageElement = function()
{
    return this.image;
}

//autocomplit lines object
function Lines()
{
    this.lines = [];
    this.length = 0;
    this.current = -1;
    this.next = -1;
    this.previous = -1;
} 

Lines.prototype = {
    
    addLine: function(title, ProductUrl, imagePath)
        {
            var line = new Line(title, ProductUrl, imagePath);
            this.lines.push(line);
            this.setLength(this.lines.length);           
        },
        
    getLine: function(index)
        {
          return this.lines[index];  
        },
        
    setLength: function(len)
        {
            this.length = len;
        },
        
    clear: function()
        {
            this.lines.splice(0, this.lines.length);
            this.length = 0;
            this.current = -1; this.next = -1; this.previous = -1;
        },
        
    getCurrentLine: function()
        {
            if(this.length > 0)
            {
              if(this.current === -1)
                {
                    this.current = 0;
                } 
                
                return this.lines[this.current];
            }  
        },
        
    getNextLine: function()
        {
           if(this.length > 0)
           {
            if(this.current <= 0)
            {
                this.next = this.current+1;
            } else
                {
                    if(this.length-1 >= this.current+1)
                        { 
                          this.next = this.current+1;
                        } else
                            {
                                this.next = 0;
                            }
                }
            this.current = this.next;
            return this.lines[this.next];            
           }
            
        },
        
    getPreviousLine: function()
        {
          if(this.length > 0)
           {
             if(this.current <= 0)
              { 
                this.previous = this.length-1;
              }  else
                    { 
                        this.previous = this.current-1;
                    } 
              
              this.current = this.previous;
              return this.lines[this.previous];  
                
           }
        }
}


})();//EOF