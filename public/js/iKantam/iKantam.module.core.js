//bind implementation (IE 8 etc.)
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        if (typeof this !== "function") {
            // closest thing possible to the ECMAScript 5 internal IsCallable function
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP = function () {},
            fBound = function () {
                return fToBind.apply(this instanceof fNOP && oThis
                    ? this
                    : oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}
//************************BIND

/** iKantam module
 * @author Alex Poliushkin
 */
(function(global){

    global.iKantam = (function () {

        /*private variables and methods*/
        var hasOwn = Object.prototype.hasOwnProperty,
            _indexOf = function (haystack, needle) {

                var i = 0,
                    max = haystack.length;
                for(; i < max; i++) {
                    if(haystack[i] === needle) {
                        return i;
                    }
                }
                return -1;
            },
            _storage = (function(){

                var data = {},
                    alowed = {
                        number  : 1,
                        string  : 1,
                        boolean : 1
                    },

                    prefix = (Math.random() + '_').slice(2);

                return {
                    set : function (name, value) {
                        /* if(this.isDefined(name)) {
                         return false;
                         } */

                        if(!hasOwn.call(alowed, typeof value)) {
                            return false;
                        }

                        data[prefix + name] = value;
                        return true;
                    },

                    isDefined : function (name) {
                        return hasOwn.call(data, prefix + name);
                    },

                    get : function (name, default_) {

                        if(this.isDefined(name)) {
                            return data[prefix + name];
                        }

                        return default_ || null;
                    },

                    unset : function (name) {
                        if(this.isDefined(name)) {
                            return delete data[prefix + name];
                        }
                        return false;
                    }
                };

            }()), //_storage
            _modules = {},

            cloner = {

                _clone: function _clone(obj) {
                    if (obj instanceof Array) {
                        var out = [];
                        for (var i = 0, len = obj.length; i < len; i++) {
                            var value = obj[i];
                            out[i] = (value !== null && typeof value === "object") ? _clone(value) : value;
                        }
                    } else {
                        var out = {};
                        for (var key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                var value = obj[key];
                                out[key] = (value !== null && typeof value === "object") ? _clone(value) : value;
                            }
                        }
                    }
                    return out;
                },

                clone: function(it) {
                    return this._clone({
                        it: it
                    }).it;
                }
            },

            _queue = [],
            _whenQueue = [],
            _callWhenCallback = function(modules, callback) {
                var args,
                    modulesArr = [];

                if (modules.length === 1) {
                    modulesArr.push(_getModule(modules.shift()));
                } else {
                    args = _getModules.apply(this, modules);
                    var i;
                    for (i in args) {
                        if(hasOwn.call(args, i)) {
                            modulesArr.push(args[i]);
                        }
                    }
                }
                callback.apply(this, modulesArr);
            },
            _touchWhenQueue = function(list){
                var i = list.length;
                for (; i--;) {
                    if (!hasOwn.call(_modules, list[i])){
                        return false;
                    }
                }
                return true;
            },
            _afterModuleAdd = function(name, o){
                var whenQueueSize = _whenQueue.length,
                    i = 0,
                    callback,
                    whenArgs;

                for(i = 0; i < whenQueueSize; i++) {
                    if (typeof _whenQueue[i] !== 'undefined' && _touchWhenQueue(_whenQueue[i].when)) {
                        whenArgs = _whenQueue[i].when;
                        callback = _whenQueue[i].callback;
                        _whenQueue.splice(i, 1);
                        _callWhenCallback(whenArgs, callback);
                    }
                }

                return true;
            };

        function _getModules (list) {
            var args = Array.prototype.slice.call(arguments),
                mod = {},
                i = 0,
                max = args.length;
            if(!args.length || list === '*') {
                return _modules;
            } else {
                for(; i < max; i += 1) {
                    if(hasOwn.call(_modules, args[i])) {
                        mod[args[i]] = _modules[args[i]];
                    }
                }
            }
            return mod;
        }

        function _getModule (name) {

            if(hasOwn.call(_modules, name)) {
                return _modules[name];
            }

            return null;
        };

//iKantam object
        return {
            info : {
                fileName : 'iKantam.module.core.js',
                modulesDir : 'modules',
                version : '1.3.4'
            },

            /**
             * Protect namespacing. Create new objects or use existing.
             * ...
             * iKantam.namespace('my.new.object');
             * ...
             * @return object
             */

            namespace : function (ns_string) {
                var parts =  ns_string.split('.'),
                    parent = this,
                    i;

                if(parts[0] === parent) {
                    parts = parts.slice(1);
                }

                for(i = 0; i < parts.length; i++) {
                    if(typeof parent[parts[i]] === 'undefined') {
                        parent[parts[i]] = {};
                    }

                    parent = parent[parts[i]];
                }

                return parent;
            },

            /**
             * Save and protect from rewrite.
             * Only simple data can be saved.
             *
             */
            storage : {

                set : _storage.set.bind(_storage),

                get : _storage.get.bind(_storage),

                unset : _storage.unset.bind(_storage),

                isSet : _storage.isDefined.bind(_storage)
            },


            addModule : function(name, implementation,  self_binding) {

                self_binding = self_binding || false;
                var i,
                    max = _queue.length;
                if(!hasOwn.call(_modules, name)) {

                    if(typeof implementation === 'function') {
                        if(self_binding) {
                            _modules[name] = implementation;
                        } else {
                            _modules[name] = implementation.bind(this);
                        }

                        //check if this function in call queue
                        for(i = 0; i < max; i += 1) {
                            if(_queue[i].func === name) {
                                if(self_binding) {
                                    implementation.apply(implementation, _queue[i].args);
                                } else {
                                    implementation.apply(this, _queue[i].args);
                                }

                                //_queue.splice(i, 1);
                                _queue[i] = {};
                            }
                        }
                        _afterModuleAdd(name, this);
                        return true;
                    }
                    _modules[name] = implementation;
                    _afterModuleAdd(name, this);
                    return true;
                }

                throw new Error('Module with name "'+name+'" already exist');
            },

            when: function(when, callback) {
                var args = Array.prototype.slice.call(arguments),
                    callback = args.pop();
                if (_touchWhenQueue(args)) {
                    _callWhenCallback(args, callback);
                } else {
                    _whenQueue.push({when: args, callback: callback});
                }
                return this;
            },

            getModules : _getModules,

            getModule : _getModule,

            clone : cloner.clone.bind(cloner),

            checkProperty : function(object, property) {
                return hasOwn.call(object, property);
            },

            /* Queue */

            putToCallQueue : function (name, args) {
                if(typeof name === 'string'){

                    var f = this.getModule(name);
                    if(typeof f === 'function') {
                        f.apply(this, args);
                        return true;
                    }
                    _queue.push({func : name, args : args});
                    return true;
                }

                return false;
            },

            /* not ready */
            _requestModule : function (name) {
                if(!this.info.fileName) {
                    return false;
                }

                if(hasOwn.call(_modules, name)) {
                    return true;
                }

                name = name.replace('_', '.');
                var incpath = this.namespace('__include_path'),
                    scripts = document.getElementsByTagName('script'),
                    max = scripts.length,
                    i,
                    currentSource,
                    core = scripts[0];

                if(!incpath.dir) {

                    for(i = 0; i < max; i+=1) {
                        currentSource = scripts[i].src;

                        if(currentSource.indexOf(this.info.fileName) != -1) {
                            core = scripts[i];
                            incpath.dir = currentSource.substr(0, currentSource.length - this.info.fileName.length);
                            incpath.dir += (this.info.modulesDir) ? this.info.modulesDir + '/' : '';

                            break;
                        }
                    }
                }

                if(typeof incpath.dir === 'string') {
                    var module = document.createElement('script');

                    module.type = 'text/javascript';
                    module.src = incpath.dir + name + '.js';


                    core.parentNode.insertBefore(module, core);
                    return true;
                }

                return false;
            },

            objectSize : function ( object ) {
                var size = 0;
                for  (var i in object) {
                    if( object.hasOwnProperty(i) ) {
                        size++;
                    }
                }
                return size;
            },

            firstMatched : function (arr, callback, context) {
                var el;
                context = context || this;
                for (var i = 0, l = arr.length; i < l; i++) {
                    el = arr[i];
                    if (callback.call(context, el, i, arr)) {
                        return el;
                    }
                }
                return undefined;
            },

            extend : function(child, parent, other) {
                if(parent) {
                    var F = function() {};
                    F.prototype = parent.prototype;
                    child.prototype = new F();
                    child.prototype.constructor = child;
                    child.prototype.proto = function() {
                        return parent.prototype;
                    }

                } else {
                    child.prototype.proto = function() {
                        return;
                    }
                } },

            stringHash :  function(str){
                var hash = 0, i, char;
                if (str.length == 0) return hash;
                for (i = 0, l = str.length; i < l; i++) {
                    char  = str.charCodeAt(i);
                    hash  = ((hash<<5)-hash)+char;
                    hash |= 0; // Convert to 32bit integer
                }
                return hash;
            },
            preg_quote : function(str){
                return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
            },

            redirect : function (url, data){
                if(typeof data !== 'object' || this.objectSize(data) < 1){
                    window.location.href = url;
                } else {
                    if(!jQuery){
                        throw new Error('jQuery required');
                    }
                    $form = jQuery('<form method="POST" action="'+ url +'">');
                    jQuery.each(data, function(key, value){
                        $form.append(jQuery('<input type="hidden" name="'+key+'" value="'+value+'">'));
                    });
                    $form.appendTo('body').submit();
                }
            }

        };


    }());

    iKantam.addModule('array', (function(){

        var _oToStr = Object.prototype.toString,
            _slice = Array.prototype.slice,
            _isArray = function (object) {

                return _oToStr.call(object) === '[object Array]';
            },

            _indexOf = function (haystack, needle) {

                var i = 0,
                    max = haystack.length;
                for(; i < max; i++) {
                    if(haystack[i] === needle) {
                        return i;
                    }
                }
                return -1;
            },
            _inArray = function(haystack, needle){
                return _indexOf(haystack, needle) !== -1;
            };

        return {
            isArray : _isArray,
            inArray : _inArray,
            indexOf : _indexOf
        };
    }()));


})(this)