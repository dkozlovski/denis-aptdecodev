(function(){
    "use strict";
    if(!iKantam) {
        throw new Error('iKantam module core required');
    }

    var timeout = setTimeout(function(){
        throw new Error('Modules required by product.form.validation were not loaded in time.');
    }, 60000);

    iKantam.when('form_validation', 'array', 'aptdeco_overlay', function(form_validation, arr, overlay){
        clearTimeout(timeout);

        var placeError = function($errorElement, $element, top){
                var offset = $element.offset();
                top = top || -12;
                offset.top += top;
                $errorElement.appendTo('body').offset(offset);
            },
            fv = form_validation({
                form: '#submit-form',
                // rewrites default error messages
                messages: {
                    color_id: {
                        required: 'Please select color.'
                    },
                    original_price: {
                        min: 'Original price should be greater than or equal to listing price.'
                    },
                    price: {
                        min: function(minValue) {
                            var categoryName = '';
                            if($('#subcategory_id').val() == '56'){
                                categoryName = $('#subcategory_id option:selected').html();
                            } else {
                                if (!$('#category_id').val()){
                                    return 'Please select a category';
                                }
                                categoryName = $('#category_id option:selected').html();
                            }

                            return 'Minimum price for ' + categoryName + ' is $' + minValue;
                        }
                    }
                },
                rules: {
                    original_price: {
                        min: function(el) {
                            return $('#price').val() || true;
                        }
                    },
                    price: {
                        min: function(el) {
                            var $categoryId = $('#category_id');

                            if ($categoryId.val() == '42') { // 42 - Décor
                                return 1;
                            }
                            if (arr.inArray(['31', '5'], $categoryId.val())) { // 31 - Sofas, 5 - Beds
                                return 100;
                            }

                            if ($('#subcategory_id').val() == '56') { // 56 - Armoires / Wardrobes
                                return 100;
                            }
                            return 50;
                        }
                    }
                },
                defaults: {
                    ignore: '.ignore',
                    submitHandler: function(form) {
                        overlay.show();
                        var action = iKantam.storage.get('submit_form_action');

                        if (action === 'publish' && !$('#photo_container').find('img').length) {
                            overlay.hide();
                            $('#photo_required').removeClass('hidden');
                            $('html, body').animate({
                                scrollTop: $('#photo_required').offset().top - 100
                            }, 1000);
                        } else {
                            var quiz = iKantam.getModule('quiz');
                            if ($.isFunction(quiz)) {
                                overlay.hide();
                                quiz(form);
                            } else {
                                form.submit();
                            }
                        }
                    }
                },
                whenHighlight: function($element, errorClass) {
                    var result = true, // whether element was intercepted. (set to true to avoid default setting)
                        addressGroup = [
                            "pickup_address[full_name]",
                            "pickup_address[address_line1]",
                            "pickup_address[address_line2]",
                            "pickup_address[city]",
                            "pickup_address[state]",
                            "pickup_address[postal_code]",
                            "pickup_address[telephone_number]",
                            "pickup_address[country_code]"
                        ];
                    switch ($element.attr('name')) {
                        case 'condition':
                            $('#condition_select')
                                .addClass(errorClass)
                                .closest('.form-group')
                                .find('.form-label')
                                .addClass(errorClass);
                            break;

                        case 'price': // no break
                        case 'original_price':
                            $element
                                .closest('.input-prepend')
                                .addClass(errorClass)
                                .closest('.form-group')
                                .find('.form-label')
                                .addClass(errorClass);
                            break;

                        case 'qty':
                            $element
                                .closest('.qty-form')
                                .addClass(errorClass)
                                .find('.form-label')
                                .addClass(errorClass);
                            break;

                        case 'pickup_address[country_code]':
                            $('#country')
                                .addClass(errorClass)
                                .closest('.form-group')
                                .find('.form-label')
                                .addClass(errorClass);
                            break;

                        default :
                            result = false;
                            break;
                    }

                    if (arr.inArray(addressGroup, $element.attr('name'))) {
                        $('#address_fields').removeClass('hidden');
                    }

                    return result;
                },

                whenPlaceError: function($errorElement, $element){
                    var result = true; // whether element was intercepted. (set to true to avoid default setting)

                    switch ($element.attr('name')) {
                        case 'price': // no break
                        case 'original_price':
                            placeError($errorElement, $element.closest('.input-prepend'));
                            break;

                        case 'condition':
                            placeError($errorElement, $('#condition_select'));
                            break;

                        case 'subcategory_id':
                            if ($element.closest('.form-group').css('display') !== 'none') {
                                result = false;
                            }
                            break;

                        case 'color_id':
                            placeError($errorElement, $element, -16);
                            break;

                        case 'pickup_address[country_code]':
                            $('#country-error').remove();
                            $errorElement.attr('id', 'country-error');
                            placeError($errorElement, $('#country'));
                            break;

                        default :
                            result = false;
                            break;
                    }
                    return result;
                }

            });

        $('#condition_select').on('change', function(){
            if (fv.isUsed()) {
                $('#condition_hidden').valid();
            }
        });

        $('.picolor').on('click touchstart', function(){
            if (fv.isUsed()) {
                $('input[name="color_id"]:first').valid();
            }
        });

        function revalidate () {
            if (fv.isUsed()) {
                fv.form.valid();
            }
        }

        // when form elements change their positions
        $('#category_id').on('change', $.debounce(revalidate));
        $('#subcategory_id').on('change', $.debounce(revalidate));
        $(window).resize($.debounce(revalidate, 300));

        iKantam.addModule('product_form_validation', fv);
    });
})()