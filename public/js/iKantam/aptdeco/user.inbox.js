$(function(){
    if (!iKantam) {
        throw new Error('iKantam module required');
    }
    iKantam.when('aptdeco_overlay', function(ov){
        $('#message_form').on('submit', function(){
            ov.show();
        });
    });
});