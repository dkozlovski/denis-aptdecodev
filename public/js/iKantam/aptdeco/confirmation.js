/**
 * Created by Alex on 17.09.14.
 */
(function(){
    "use strict";
    if(!iKantam) {
        throw new Error('iKantam module core required');
    } else if (!jQuery) {
        throw new Error('jQuery is required');
    }

    iKantam.when('app_config', function(config){

        var baseUrl = this.BASE_URL || config.base_url || '/',

            deferred = $.ajax({
                type: 'GET',
                url:  baseUrl + 'front-helper/modal-html/template/confirmation'
            });

        iKantam.addModule('confirmation', function(options){
            var $modal,
                Confirmation;

            deferred.done(function(data){
                $modal = $(data);
                $modal.find('.apt-modal-title').text(options.message || 'Are you sure?');
                $modal.on('click', '#aptdeco_confirm_yes', function(){
                    Confirmation.yes();
                    Confirmation.hide();
                }).on('click', '#aptdeco_confirm_no', function(){
                    Confirmation.no();
                    Confirmation.hide();
                });
            });
            Confirmation = {
                yes: options.yes || function(){},
                no: options.no || function(){},
                show: function(){
                    deferred.done(function(){
                        $modal.modal('show');
                    });
                },
                hide: function(){
                    deferred.done(function(){
                        $modal.modal('hide');
                    });
                }

            };

            return Confirmation;
        });
    });

})()
