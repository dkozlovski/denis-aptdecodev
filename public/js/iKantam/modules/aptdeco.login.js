(function(){
    if(!iKantam) {
        throw new Error('iKantam module core required');
    }

    var timeout = setTimeout(function(){
        throw new Error('Modules required by apteco_login were not loaded in time.');
    }, 25000);

    iKantam.when('app_config', function(){
        clearTimeout(timeout);
        var config = iKantam.getModule('app_config');

        if (null === config) {
            throw new Error('"app_config" module is required');
        } else {
            if (typeof  config.facebook_app_id === 'undefined') {
                throw new Error('facebook_app_id is not defined.');
            }
        }

        // Facebook init method
        window.fbAsyncInit = function() {
            FB.init({
                appId      : config.facebook_app_id,
                cookie     : true,
                version    : 'v2.0'
            });

            iKantam.addModule('FB', FB);
        };
        //Facebook JS SDK
        if (typeof window.FB === 'undefined') {
            // Load the SDK asynchronously
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        }


        var AptdecoLogin;

        iKantam.addModule('aptdeco_login', function(options){
            if (typeof AptdecoLogin !== 'undefined') {
                return AptdecoLogin;
            }
            options = options || {};

            var baseUrl = this.BASE_URL || config.base_url || '/',
                $modal,
                loginUrl = options.loginUrl || baseUrl + '/user/login/async',
                locked = false,
                getData = function(){
                    return {
                        email: $modal.find('input[name="email"]').val(),
                        password: $modal.find('input[name="password"]').val()
                    };
                },
                showMessage = function(message){
                    $modal
                        .find('#login_message')
                        .hide()
                        .removeClass('hidden')
                        .find('p')
                        .text(message)
                        .closest('#login_message')
                        .slideDown();
                },
                hideMessage = function(){
                    $modal.find('#login_message').hide();
                };

            var deferred = $.ajax({
                type: 'GET',
                url:  baseUrl + 'front-helper/modal-html/template/login'
            });

            AptdecoLogin = {
                show: function (){
                    if (this.isLoggedIn()) {
                        alert('Already logged in!');
                        return false;
                    }
                    deferred.done(function(){
                        $modal.modal('show');
                    });
                },
                hide: function(){
                    deferred.done(function(){
                        $modal.modal('hide');
                    });
                },
                isLoggedIn: function(){
                    var result = false;
                    $.ajax({
                        url: options.isLoggedUrl || baseUrl + 'front-helper/is-logged',
                        async: false,
                        success: function(response) {
                            result = response.is_logged;
                        }
                    });
                    return result;
                }
            };

            deferred.done(function(data){
                $modal = $(data);
                $modal.on('hide', function(){
                    $(this).find('input').val('');
                });
                var runLogin = function(e, data){
                    if (locked) {
                        return;
                    }
                    locked = true;
                    $modal.find('#login_error').parent().hide();
                    showMessage('Please wait.')
                    $.post(loginUrl, data || getData(), function(response){
                        if (response.success) {
                            if ($.isFunction(options.success)) {
                                options.success(AptdecoLogin, response);
                            } else {
                                if (response.header_html) {
                                    $('#header').replaceWith(response.header_html);
                                }
                                setTimeout(function(){
                                    AptdecoLogin.hide();
                                }, 1500);
                            }
                        } else {
                            if ($.isFunction(options.fail)) {
                                options.fail(AptdecoLogin, response);
                            } else {
                                if (response.message) {
                                    $modal.find('#login_error')
                                        .text(response.message)
                                        .parent()
                                        .hide()
                                        .removeClass('hidden')
                                        .slideDown();
                                }
                            }
                        }
                    })
                        .always(function(response){
                            hideMessage();
                            if (response.success && response.user_name) {
                                showMessage('Welcome, ' + response.user_name + '!');
                            }
                            if ($.isFunction(options.afterAll)) {
                                options.afterAll(AptdecoLogin, response);
                            }
                            locked = false;
                        });
                };

                // native login
                $modal.find('#login_btn').on('click', runLogin);

                // facebook login
                $modal.find('#login_btn_facebook').on('click', function(e){
                    if (locked) {
                        return;
                    }
                    iKantam.when('FB', function(){
                        FB.login(function(response){
                            if (response.status === 'connected') {
                                runLogin(e, response.authResponse);
                            }
                        },{scope: 'public_profile,email'});
                    });
                });

            });

            return AptdecoLogin;

        });
    });
})();