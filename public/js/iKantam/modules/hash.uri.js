
	/** Hash uri module OBJECT
     * @method key([key], [value])
     *  if [key] specified and empty [value] - return uri value or null if nothing found
     *  if [key] and [value] specified  - set new uri value or replace existing
     *  if no prameters specified or key = '*' - return object contains all data 
     * 
     * @method segment(index, [value])
     *  if index specified and empty [value] - return segment value: uri = "firts&second&key=thirdvalue"| O.segment(3) - return "thirdvalue"
     *  if index and [value] specified - set new segment value or replace old
     *
     * @method slice(start, [end]) - uri segments slice
     * 
     * @method segmentString(index) - uri segment text: 'key=value'
     */
if(!iKantam) {
    throw new Error('iKantam module core required');
}

iKantam.addModule('hash_uri', (function(){
        
        var _hash = decodeURIComponent(window.location.hash.substr(1)),                                
            _decodedHash = (function (){ 
                var i ,
                    splited ,
                    data = _hash.split('&'),
                    max = data.length,
                    _currentKey = 0, 
                    result = {} ;
                    
                    result._segments = [];
                
                for(i = 0; i < max ; i+=1 ) {
                    splited = data[i].split('=');                    
            
                    if(splited.length == 2) {
                        var aux = {};
                            if(splited[0].substr(-2) === '[]') {
                                result[splited[0]] = result[splited[0]] || [];
                                result[splited[0]].push(splited[1]);
                            } else {
                                result[splited[0]] = splited[1];
                            }
                            aux[splited[0]] = splited [1];
                        result._segments.push(aux); 
                        
                           
                    } else {
                        if(splited.length == 1) {
                            result._segments.push(splited[0]);
                           result[_currentKey] = splited[0];
                           _currentKey++ ;
                        }
                    }
                }
                return result;
            }()), //_decodedHash
            
            _refreshLocationHash  = function () {
                var locStr = ''
                    i = 0,
                    max = _decodedHash._segments.length ;
                    
                for (i ; i < max; i++) {
                    if(typeof _decodedHash._segments[i] === 'object') {
                        for(var j in _decodedHash._segments[i]) { 
                            locStr += j + '=' + _decodedHash._segments[i][j];
                        }
                    } else {
                        locStr += _decodedHash._segments[i];
                    }
                    if(i+1 < max) {
                        if(locStr !== ''){
                            locStr += '&';
                        }
                    }
                }    
                // window.location.hash = encodeURIComponent(locStr);
                 window.location.hash = locStr;
            };
            
           return {         

                key : function (key, val){
                    
                    if(key === undefined || key === '*') {
                        var obj = {};
                        for(var x in _decodedHash) {
                            if(x !== '_segments') {
                                obj[x] = _decodedHash[x];
                            }
                        }
                        
                        return obj;
                    }
                    
                    if(key !== '_segments') {
                        
                        if(val !== undefined) {

                            if(key.substr(-2) === '[]') { 
                                _decodedHash[key] = _decodedHash[key] || [];
                                _decodedHash[key].push(val);
                            } else {
                            _decodedHash[key] = val;
                            for(var i = 0, max = _decodedHash._segments.length; i < max; i++) {
                                if(iKantam.checkProperty(_decodedHash._segments[i], key)) {
                                  _decodedHash._segments[i][key] = val;
                                  _refreshLocationHash();
                                  return this;  
                                }
                            }}
                            var aux = {};
                            aux[key] = val;
                            _decodedHash._segments.push(aux);
                            _refreshLocationHash();
                            return this;
                        }                        
                        
                        if(iKantam.checkProperty(_decodedHash, key)) {
                            return _decodedHash[key];
                        }
                    }
                },
                
                segment : function (index, val) {
                    
                    index = index - 1;
                    if(val !== undefined && index > -1 && index <= _decodedHash._segments.length) {
                        if(typeof val === 'string') {
                            val = encodeURIComponent(val);
                         }
                       if(iKantam.checkProperty(_decodedHash._segments, index)) {
                            _decodedHash._segments[index] = val;
                        } else {
                            _decodedHash._segments.push(val);
                        }
                        var i = 0,
                            max = _decodedHash._segments.length,
                            aux_segments = iKantam.clone(_decodedHash._segments),
                            currentKey = 0;
                            
                            _decodedHash = {};
                            
                        for(i; i < max; i++) {
                            
                            if(typeof aux_segments[i] === 'object') {
                               for(var j in aux_segments[i]) {
                                _decodedHash[j] = aux_segments[i][j]; 
                               }
                            } else {
                               _decodedHash[currentKey] = aux_segments[i];
                               currentKey ++ ;  
                            }                           
                         }
                         
                         _decodedHash._segments = aux_segments;
                         _refreshLocationHash();
                        return this;
                    }
                    
                    if(index < 0 || index > _decodedHash._segments.length) {
                        return null;
                    } else {
                        
                        if(iKantam.checkProperty(_decodedHash._segments, index)) {
                            return _decodedHash._segments[index];
                        }
                    }
                    return null;
                },
                
                segmentString : function (index) {
                    var segment = this.segment(index);
                    if(segment) {
                        var str ;
                        if(typeof segment === 'object') {
                            for(var i in segment) {
                                str = i + '=' + segment[i];
                                return str;
                            }
                        } else {
                            return segment;
                        }
                    }
                    
                    return null;
                },
                
                slice : function (start, end) {
                    if(!end || end > _decodedHash._segments.length) {
                        end = _decodedHash._segments.length;
                    }
                    
                    if(!start || start < 1) {
                        start = 1;
                    }
                    
                    var result = '';
                    
                    for(start; start <= end; start++) {
                        if(result !== '') {
                            result += '&';
                        }
                        result += this.segmentString(start);
                    }
                    
                    return result;
                }
           };

}()));