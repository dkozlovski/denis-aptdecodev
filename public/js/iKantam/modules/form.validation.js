(function(){
    "use strict";
    if(!iKantam) {
        throw new Error('iKantam module core required');
    }

    var timeout = setTimeout(function(){
        throw new Error('Modules required by form_validation were not loaded in time.');
    }, 25000);

    iKantam.when('jquery_validation_object', function(validation_object){
        clearTimeout(timeout);
        iKantam.addModule('form_validation', function(options){
            if (!options.form) {
                throw new Error('Cannot apply validation rules: form is not set.');
            } else if (typeof $.fn.validate !== 'function') {
                throw new Error('Cannot apply validation rules: jQuery validation plugin is not exists.');
            }

            $.extend(true, validation_object.rules, options.rules);
            $.extend(true, validation_object.messages, options.messages);

            // Replace native "min" validator (added facility to bypass validator by returning true)
            $.validator.addMethod('min', function( value, element, param ){
                return this.optional( element ) || value === true || +value >= +param;
            });

            // Replace native "max" validator (added facility to bypass validator by returning true)
            $.validator.addMethod('max', function( value, element, param ){
                return this.optional( element ) || value === true || +value <= +param;
            });

            var $form  = $(options.form),
                isUsed = false,
                defaults = {
                    highlight: function(element, errorClass, validClass){
                        element = $(element);
                        if ($.isFunction(options.whenHighlight)) {
                            if (options.whenHighlight.call(this, element, errorClass, validClass)) {
                                return ;
                            }
                        }
                        element.addClass(errorClass);
                        element.closest('.form-group').find('.form-label').addClass(errorClass);
                    },
                    unhighlight: function( element, errorClass, validClass ) {
                        if ($.isFunction(options.whenUnhighlight)) {
                            if (options.whenUnhighlight.call(this, $(element), errorClass, validClass)) {
                                return ;
                            }
                        }
                        $(element).closest('.form-group').find('.error').removeClass('error');
                        var assumedId = '#' + this.idOrName(element) + '-error';
                        assumedId = assumedId.replace(/\[|\]/g, '');
                        $(assumedId).remove();
                    },
                    errorElement: 'small',
                    errorPlacement: function ($errorElement, $element) {
                        isUsed = true;
                        $('#' + $errorElement.attr('id')).remove(); //!
                        var i,
                            elementOffset = $element.offset();
                        if ($.isPlainObject(options.errorElementAttributes)) {
                            for (i in options.errorElementAttributes) {
                                $errorElement.attr(i, options.errorElementAttributes[i]);
                            }
                        } else {
                            $errorElement.addClass('field-helper')
                                .css({position: 'absolute', color: "#ff6666 !important"});
                        }

                        if ($.isFunction(options.whenPlaceError)) {
                            if (options.whenPlaceError.call(this, $errorElement, $element)) {
                                return ;
                            }
                        }
                        elementOffset.top -= 12;
                        $errorElement.css(elementOffset);
                        $errorElement.appendTo('body');
                    }

                };

            $form.validate($.extend(validation_object, defaults, options.defaults));

            return {
                form: $form,
                isUsed: function() {
                    return isUsed;
                }
            }

        });



    });
})();