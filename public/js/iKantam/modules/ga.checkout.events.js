$(document).ready(function(){
    
try {
(function(){
    function time () {
        return (new Date).getTime();
    }
    
    function getSpendTime () {
        return (time() - iKantam.storage.get('startTime', 0)) / 1000; 
    }  

    
        
    var memory = iKantam.storage,
        steps = {
            'checkout/index'            : 'shippingMethod' ,
            'checkout/shipping-address' : 'shippingAddress' ,
            'checkout/payment'          : 'payment' ,
            'checkout/delivery'         : 'selectDates' 
        },
        currentStep = (function(){
            var location = window.location.toString(),
                step = location.match(/checkout\/[a-zA-Z0-9-]*/)[0];
                if(step === 'checkout/' || step === 'checkout/shipping-method') {
                    step = 'checkout/index';
                }
            return steps[step];
            
        }());

        _gaq = window._gaq || [];
    
    memory.set('startTime', time());
    
    if(!memory.get('stepsComplete', false)) {
        _gaq.push(['_trackEvent', 'checkout', 'stepLoaded', currentStep]);

     }else {
               _gaq.push(['_trackEvent', 'checkout', 'completeCheckout', currentStep]);
 
     }
     
     switch(currentStep) {
        case steps['checkout/index'] :
            
            (function(){
                $('.radio-btn').on('click', function(){
                        _gaq.push(['_trackEvent', 'checkout', 'selectDeliveryMethod', $(this).find('input').val()]);

                });//select delivery method
            })();
            
        break;
        
        case steps['checkout/shipping-address'] :
            (function(){
               $('.user-address .remove-address').on('click', function(){
                       _gaq.push(['_trackEvent', 'address-shipping', 'remove']);
 
               });
               
               $('.user-address a:first').on('click', function(){
                        _gaq.push(['_trackEvent', 'address-shipping', 'select']);

               });
            })();
        break;
        
        case steps['checkout/payment'] :
        
            (function(){
                $('.buttons-block .btn').on('click', function(){
                        _gaq.push(['_trackEvent', 'checkout', 'payButtonClicked']);

                }); 
                
               $('.user-address .remove-address').on('click', function(){
                       _gaq.push(['_trackEvent', 'address-billing', 'remove']);
 
               });
               
               $('.user-address a:first').on('click', function(){
                        _gaq.push(['_trackEvent', 'address-billing', 'select']);

               });                 
            })();
       
        break;
        
        case steps ['checkout/delivery'] :

            (function(){
                $('li .check-box').on('click', function(){
                        _gaq.push(['_trackEvent', 'checkout', ($(this).hasClass('checkedBox')) ? 'selectDate' : 'removeDate']);

                });
                
                $('#confirm-delivery').on('mousedown', function(){
                        _gaq.push(['_trackEvent', 'checkout', 'confirmDeliveryClicked']);

                });
            })();
       
        break;
        
        default :
            _gaq(['_trackEvent', 'checkout', 'unknownStep', location.toString()]);

        break;
     }
 
 //track errors 
 //add function to iKantam modules for remote call from anywhere   
    (function (step) {
        iKantam.addModule('trackErrors', function(){
             var err = this.namespace('checkout.err'),
                 i;
             
             if(this.getModule('array').isArray(err)) {
                var max = err.length;
 
                 for(; max-- ;) {
                _gaq.push(['_trackEvent', 'user_error', step, 'Message: "'+err[max]+'"']);
                     
                 }   
                 return;
             }
             
        for(i in err) {
            if(err.hasOwnProperty(i)) {
                _gaq.push(['_trackEvent', 'user_error', step, 'Field: "'+i+'". Message: "'+err[i]+'"']);
  
            }            
        }
        });
    })(currentStep);
   
   iKantam.getModule('trackErrors')(); // run error tracking
   
// Leave page event
$(window).on(($.browser.webkit || $.browser.mozilla) ? 'beforeunload' : 'unload', function(){
      _gaq.push(['_trackEvent', 'checkout', 'leavePage', currentStep , getSpendTime()]); 

});//leave page
               
      
})();

} catch(err) { console.info(err);
    _gaq = window._gaq || [];
    _gaq.push(['_trackEvent', 'js_error', 'ga.checkout.events.js', err.message]);
}     
    
});