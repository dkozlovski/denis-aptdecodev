if(typeof iKantam === undefined) {
    throw new Error ('iKantam module core required');
}
/*
 Required options:
 {
    baseUrl : site url,
    popupTemplate : html template
 }
*/
iKantam.addModule('newsletter_subscribe_popup', function($, options){
    var that = this, //iKantam
        core = {
            checkIfPopupShow : function () {
                var result ;
                $.ajax({
                    async : false,
                    type  : 'post',
                    dataType : 'json',
                    url : options.baseUrl + '/newsletter/index/check-subscription',
                    success : function ( response ) {
                       result =  response.remind ;
                    }
                });
                return result;
            }, //checkIfUserSubscribed
            
            getPopup : function () { 
                return $(options.popupTemplate);
            },
            
            getSuccess : function() {
                return $(options.successTemplate);  
            },
            
            showPopUp : function () { 
                $('#wrap').prepend(this.getPopup());
               // console.log($('#wrap'), this.getPopup());
                
                /* Код Махмуда из custom.js */
                		var scrollTop=parseInt($(window).scrollTop());
                		var windowHeight=$(window).height();
                		var persent=parseInt((windowHeight*25)/100);
                		if(scrollTop > persent){
                			$('.subscribe-overlay').fadeIn(600, function(){
                			$('.subscribe-window').css({'top':persent+scrollTop+'px'}).slideDown();
                		});
                		}
                		else{
                			$('.subscribe-overlay').fadeIn(600, function(){
                			$('.subscribe-window').css({'top':persent+scrollTop+'px'}).slideDown();
                			})
                		}
                		$('#wrap').on('click', '.close-butt', function(){
                			$(this).parents('.subscribe-overlay').fadeOut(500);
                		})
                		
                		$(document).bind('click', function(e) {
                			var $clicked = $(e.target);
                			if(!$clicked.parents().hasClass("subscribe-window"))
                		 { 
                			 $(".subscribe-overlay").fadeOut(500);
                		 }
                		});
            },//showPopup
            
            start : function () {
                if(this.checkIfPopupShow()) {
                    this.showPopUp()  ;
                }
                return true;
            },
            
            showSuccess : function () {
                $('.subscribe-overlay').html($(core.getSuccess()).html());
            }
            
        };
        
      return core ;
    
    
});
