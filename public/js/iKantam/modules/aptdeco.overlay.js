(function(){
    if(!iKantam) {
        throw new Error('iKantam module core required');
    }
    iKantam.when('app_config', function(config){
        var aptdeco_overlay = function(){
            var overlayApi = {
                    show: function(){},
                    hide: function(){}
                },
                pattern = '<div class="loader-overlay"></div>'+
                    '<div class="loader-container"><img src="'+ config.public_url +'images/up.gif" /></div>';
            $(function(){
                var overlay = $('.loader-overlay, .loader-container');
                if (overlay.length < 2) {
                    overlay = $(pattern).hide();
                    $('body').prepend(overlay);
                }
                overlayApi.show = function(){
                    overlay.fadeIn();
                };
                overlayApi.hide = function(){
                    overlay.fadeOut();
                };
            });

            aptdeco_overlay = overlayApi;
            return overlayApi;
        };

        iKantam.addModule('aptdeco_overlay', aptdeco_overlay());
    })

})();

