(function() {
    function showOverlay(){
        $(".loader-overlay, .loader-container").show();
    }
    function hideOverlay(){
        $(".loader-overlay, .loader-container").fadeOut('slow');
    }
    jQuery.fn.imagesUploader = function(imagesContainer, options) {

        var _container = imagesContainer;
        var _isLoading = 0;
        var _options = options;

        $(this).on('click', '.item-photo', function(){
            $('[current]').removeAttr('current');
            $(this).attr('current', '');
        });

        $('.upload-image').fileupload({
            dropZone: null,
            url: options.upload_url,
            dataType: 'json',
            formData: function(form){
                var formData = $(this.fileInput).data('form'),
                    result = [];

                $.each(formData, function (name, value) {
                    result.push({name: name, value: value});
                });

                formData.image_spec = null;

                return result;
            },
            error: function(data) {
                _container.triggerError('Something went wrong while uploading :(');
                hideOverlay();
            },
            add: function(e, data) {
                $('#photo_required').addClass('hidden');
                if (data.files[0].size !== undefined && (data.files[0].size) > 14680064) {
                    alert('Sorry. File size is too big. Only accepts files less than 14MB size.');
                } else {
                    showOverlay();
                    var formData = data.fileInput ? data.fileInput.data('form') : null;

                    _container.triggerStartUploading(formData);
                    _isLoading++;
                    data.submit();
                }
            },
            always: function() {

                _isLoading--;
                if (_isLoading === 0) {
                    hideOverlay();
                   // _loadingTemplate.addClass("hidden");
                }
            },
            done: function(e, data) {
                if (data.result && data.result.success) {

                    _container.addImage(
                        data.result.file.id,
                        data.result.file.url,
                        false,
                        false,
                        false,
                        _options,
                        _container,
                        data.fileInput.data('form')
                    );
                    if (_isLoading === 1) {//"done" is called before "always"
                        //_buttonTemplate.removeClass("hidden");
                    }

                    var formData = data.fileInput.data('form');

                    if (data.originalFiles.length > 1) {
                        formData.cell_id = undefined;
                        formData.image_spec = undefined;
                    }


                } else if (data.result && data.result.message) {
                    _container.triggerError(data.result.message, data.fileInput.data('form'));
                   // _errorTemplate.html(data.result.message).removeClass("hidden");
                   // _buttonTemplate.removeClass("hidden");
                } else {
                    _container.triggerError('Something went wrong while uploading :(', data.fileInput.data('form'));
                   // _buttonTemplate.removeClass("hidden");
                }
            },
            send: function(e, data) {
                /*$('html,body').animate({
                    scrollTop: _uploader.offset().top
                }, 800);*/
            }
        });
    };

    jQuery.fn.aptdecoImage = function(options) {

        var _input = $(this); // dom root container for image
        var _container = options.container; // imageContainer object
        var _imageId = options.image_id;
        var _imageUrl = options.image_url;
        var _isDefault = options.is_default;
        var _isLocked = options.is_locked;
        var _hasLocked = options.has_locked;
        var deleteUrl = options.delete_url;
        var defaultUrl = options.default_url;
        var rotateUrl = options.rotate_url;
        var _isLoading = false;
        var additionalPostData = options.additionalPostData || {};

        var _overlay = jQuery("<div></div>", {"class": "overlay"});
        var _actionsWrapper = jQuery("<div></div>", {"class": "uploads-action ctr-icons"});

        var _setDefaultButton = _input.find('[data-purpose="set_default"]');
        var _isLockedButton = _input.find('[data-purpose="mark_as_locked"]');
        var _isDefaultButton = _input.find('[data-purpose="mark_as_default"]');
        var _deleteImageButton = _input.find('[data-purpose="delete"]');
        var _lockButton =  _input.find('[data-purpose="lock"]');
        var _unlockButton =  _input.find('[data-purpose="unlock"]');
        //var _rotateLeftImageButton = jQuery("<button></button>", {"class": "apt-btn stick top-right rotate-left", type: "button", html: "<i class=\"apt-icon-curved-arr left\"></i>"});
        var _rotateRightImageButton = _input.find('[data-purpose="rotate_right"]');
        var _image = _input.find('img').attr('src', _imageUrl);

        _initTemplate();
        _bindEvents();

        function   _initTemplate()
        {
            _lockButton.addClass('hidden');
            _unlockButton.addClass('hidden');
            if (_isDefault) {
                _setDefaultButton.addClass("hidden");
            } else {
                _isDefaultButton.addClass("hidden");
            }

            if (_isLocked) {//if default photo is locked it can't be edited, so hide delete & rotate buttons
                _deleteImageButton.addClass("hidden");
                //_rotateLeftImageButton.addClass("hidden");
                _rotateRightImageButton.addClass("hidden");
            } else {
                _isLockedButton.addClass("hidden");
                if (_hasLocked) {//hide "set as default" button if our default photo is locked
                    _setDefaultButton.addClass("hidden");
                }
            }

            _input.attr("id", "image-" + _imageId);
        }

        function _bindEvents() {
            _deleteImageButton.on("click touchstart", function() {
                deleteImage();
            });

            /*_rotateLeftImageButton.on("click", function() {
                var currentAngle = getCurrentAngle(_rotateRightImageButton);
                rotateImage(currentAngle, 'left');
            });*/
            _rotateRightImageButton.on("click touchstart", function() {
                var currentAngle = getCurrentAngle(_rotateRightImageButton);
                rotateImage(currentAngle);
            });

            _setDefaultButton.on("click touchstart", function() {
                setDefaultImage();
            });
        }

        /**
         * get angle from image src
         * @param _rotateImageButton
         * @returns {Number}
         */
        function getCurrentAngle(_rotateImageButton) {
            var imageScr     = _rotateImageButton.closest('.item-photo').find('img').attr('src');
            var _temp        = imageScr.substr(imageScr.lastIndexOf('-') + 1);
            return parseInt(_temp.substr(0, _temp.indexOf('.')));
        }

        function setIsDefault(isDefault) {
            _isDefault = isDefault;

            if (isDefault) {
                _setDefaultButton.addClass("hidden");
                _isDefaultButton.removeClass("hidden");
            } else {
                _setDefaultButton.removeClass("hidden");
                _isDefaultButton.addClass("hidden");
            }
        }

        function beforeSend() {
            showOverlay();
            this.data += ('&' + $.param(additionalPostData));
            _isLoading = true;

            _image.addClass("hidden");
            _overlay.addClass("hidden");
        }

        function complete()
        {
            hideOverlay();
            _isLoading = false;
            _image.removeClass("hidden");
            _overlay.removeClass("hidden");
        }

        function deleteImage() {
            if (_isLoading) {
                return;
            }

            $.ajax({
                dataType: "json",
                url: deleteUrl,
                data: {image_id: _imageId},
                async: true,
                type: "POST",
                beforeSend: beforeSend,
                success: function(data) {
                    if (data.success && !data.error) {
                        _input.remove();
                    } else if (data.error) {
                        if (data.message) {
                            alert(data.message);
                        } else {
                            alert("Cannot delete the image.");
                        }
                    }
                },
                error: function() {
                    alert("Cannot delete the image.");
                },
                complete: function() {
                    complete();
                }
            });
        }

        function setDefaultImage()
        {
            if (_isLoading) {
                return;
            }

            $.ajax({
                dataType: "json",
                url: defaultUrl,
                data: {image_id: _imageId},
                async: true,
                type: "POST",
                beforeSend: beforeSend,
                success: function(data) {
                    if (data.success && !data.error) {
                        _container.unsetDefaultImage();
                        setIsDefault(true);
                    } else if (data.error) {
                        if (data.message) {
                            alert(data.message);
                        } else {
                            alert("Cannot set the image as default.");
                        }
                    }
                },
                error: function() {
                    alert("Cannot set the image as default.");
                },
                complete: function() {
                    complete();
                }
            });
        }

        function rotateImage(currentAngle, direction) {
            if (_isLoading) {
                return;
            }

            if (direction == 'left') {
                angle = currentAngle - 90;
            } else {
                angle = currentAngle + 90;
            }

            if (angle < 0) {
                angle += 360;
            }

            $.ajax({
                dataType: "json",
                url: rotateUrl,
                data: {image_id: _imageId, angle: angle},
                async: true,
                type: "POST",
                beforeSend: beforeSend,
                success: function(data) {
                    if (data.success && !data.error && data.url) {
                        _image.attr("src", data.url);
                    } else if (data.error) {
                        if (data.message) {
                            alert(data.message);
                        } else {
                            alert("Cannot rotate the image.");
                        }
                    }
                },
                error: function() {
                    alert("Cannot rotate the image.");
                },
                complete: function() {
                    complete();
                }
            });
        }

        return {
            setIsDefault: function(isDefault) {
                _isDefault = isDefault;

                if (isDefault) {
                    _setDefaultButton.addClass("hidden");
                    _isDefaultButton.removeClass("hidden");
                } else {
                    _setDefaultButton.removeClass("hidden");
                    _isDefaultButton.addClass("hidden");
                }
            }
        };
    };
})();

(function() {
    jQuery.fn.imagesContainer = function() {
        var _container = jQuery(this),
            _images = [],
        // placeholders used instead of photo blocks
            _defaultPlaceholders = [
                'front side',
                'right side',
                'left side',
                'back side',
                'wide angle (room shot)',
                'material close up',
                'damage close up (scratches, dents, tears)'
            ],
            _placeholderTemplate = $('#template_photo_placeholder').html(),
            _photoContainer = $('#template_photo').html(),
            storage = {
                defaults: {}
            },
        // puts new element to container
        _placeNewPhotoBlock = function ($block, specs){
            var replaceableElement;

            if ($.isPlainObject(specs) && $.isNumeric(specs.cell_id)) {
                replaceableElement = _container.find('.item-photo').eq(specs.cell_id);
                if (replaceableElement.length) {
                    replaceableElement.before($block).detach();
                    return $block;
                }
            }

            // first by priority is element with class .photo-placeholder marked as current (just add empty attr)
            if (_container.find('.photo-placeholder[current]').length){
                replaceableElement = _container.find('.photo-placeholder[current]');

                replaceableElement.before($block).detach(); //! exactly detach
                // second is the first element with class .photo-placeholder
            } else if (_container.find('.photo-placeholder').length) {
                replaceableElement = _container.find('.photo-placeholder:first');

                replaceableElement.before($block).detach();

            } else {
                // and finally:
                _container.find('#add_new_photo').before($block);
            }


            return $block;
        },
            _loadingTemplate = $('#template_loading_in_progress').html(),
            _errorTemplate = $('#template_error').html();

        var API = {
            addImage: function(imageId, imageUrl, isDefault, isLocked, hasLocked, options, container, specs) {

                if (typeof  specs.image_spec !== 'undefined') {
                    var cellIndex = _defaultPlaceholders.indexOf(specs.image_spec);
                    if (cellIndex !== -1) {
                        specs.cell_id = cellIndex;
                    }
                } else {
                    var $freeInput = $('input[data-form-data]:first');
                    if ($freeInput.length) {
                        specs = $freeInput.data('form');
                    }
                }

                var newImage = _placeNewPhotoBlock(jQuery(_photoContainer), specs)
                        .aptdecoImage({
                            image_id: imageId,
                            image_url: imageUrl,
                            is_default: isDefault,
                            is_locked: isLocked,
                            has_locked: hasLocked,
                            container: container,
                            delete_url: options.delete_url,
                            default_url: options.default_url,
                            rotate_url: options.rotate_url,
                            additionalPostData: options.additionalPostData || {}
                        });

                _images.push(newImage);
                storage.defaults[$(newImage).index()] = $(newImage);
            },
            addPlaceholder: function(text){
                var $placeholder = $(_placeholderTemplate);

                $placeholder.find('.photo-placeholder-text').text(text);
                _container.find('#add_new_photo').before($placeholder);

                // Adds special params to request.
                // Cell id will be response back (it need to determine block to which assigned response)
                $placeholder.find('[type="file"]').attr(
                    'data-form',
                    '{"cell_id" : "'+ $placeholder.index() +'", "image_spec" : "'+ text +'"}'
                );
                storage.defaults[$placeholder.index()] = $placeholder;
            },
            unsetDefaultImage: function() {
                for (i in _images) {
                    _images[i].setIsDefault(false);
                }
            },
            triggerStartUploading: function(specs){
                var $block = $(_loadingTemplate)
                    .attr('current', '');

               return  _placeNewPhotoBlock($block, specs)
            },
            triggerError: function(message, specs) {
                var $block = $(_errorTemplate)
                    .attr('current', '');

                setTimeout(function(){
                    $block.replaceWith(storage.defaults[$block.index()]);
                }, 4000);

                return _placeNewPhotoBlock($block, specs).find('.error-message').text(message);
            }
        };
        // fill container with default placeholders (even if photos exists)
        $(_defaultPlaceholders).each(function(i,v){
             API.addPlaceholder(v);
        });

        return API;
    };
})();