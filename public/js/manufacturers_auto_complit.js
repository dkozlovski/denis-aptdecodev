$(document).on('ready',function(){

	var hoverColor = '#E6E6E6'; //lines
	var field = $('#manufacturer');
	var lines = new Lines();

	var ignoredKeys = [0,16,17,18,20,27,34,35,36,37,39,44,91,112,113,114,115,116,117,118,119,120,121,122,123,144,145];

	//-----------------Container
	//container cordinates
	var point = field.offset();
	point.top+=field.height()+7; 
    
	var container = document.createElement('div');
	container = $(container);
	container.addClass('manufacturersLiveSearchContainer');
	container.css({
		'position':'absolute',
		'border':'1px solid #DADADA',
		'backgroundColor':'#FFF',
		'top':point.top + 2,
		'left':point.left,
		 'color': '#808080',
		'cursor': 'pointer',
		'font-size': '14px',
		'width':field.outerWidth() - 2,
		'display':'none'
	}).appendTo('body');
	//----------------------Manufacturer events
	// cancel form submiting
	field.on('keydown',function(e){
		if(e.keyCode == 13) {
			return false;
		}
	});

	//-----------------keyup

	field.on('keyup',function(key){
		if(inArray(ignoredKeys,key.keyCode)) return false;


		switch(key.keyCode)
		{
			case 38:
				lines.getCurrent().css('backgroundColor',container.css('backgroundColor'));
				lines.getPrevious().css('backgroundColor',hoverColor);
				$(this).val(lines.getCurrent().text());
				return false;
				break;
        
			case 40:
				lines.getNext().css('backgroundColor',hoverColor);
				lines.getPrevious().css('backgroundColor',container.css('backgroundColor'));        
				$(this).val(lines.getNext().text());
				return false;
				break;
        
			case 13:
				container.empty().hide();
				lines.clear();
				return false;
				break;

		}
    

		var value = $(this).val();

 
		if($.trim(value)!='')
		{
            
			var postData = $.parseJSON('{"manufacturerValue":"'+value+'"}');         
			$.post(
				'http://localhost/reham-aptdeco/public/manufacturer/async/find',
				postData,
				function(data)
				{
					data = $.parseJSON(data);  
					var dataCount = data.length;
					// clear container
					container.empty().hide();


					//add lines to container 
					lines.clear();       
					for(var i = 0; i < dataCount; i++)
					{ 
						var line = document.createElement('div');
						$(line).addClass('manufacturersLiveSearchLine');
						$(line).text(data[i]).appendTo(container);
						//-------lines events
						$(line).on('mouseenter',function(){
							$(this).css('backgroundColor',hoverColor);
						});//mouseenter
    
						$(line).on('mouseout',function(){
							$(this).css('backgroundColor',container.css('backgroundColor'))
						});//mouseout
    
						$(line).on('click',function(){
							field.val($(this).text());
							container.empty().hide();
						});//click
						//-----

						lines.addLine($(line));

					}
					container.show(200);
                
				}//post function
            
				);//ajax post
		}   
		else 
		{
			container.empty().hide(); // if empty field
		} 
    
    
    
	});//--------------keyup event end
 


 
	//-------------------Container events
	container.on('mouseleave',function(){
		container.empty().hide();
	});
//--------

    
});


function inArray(data,value)
{
	for(key in data)
	{
		if(data[key] == value) return true; 
	}
	return false;
}
//--------Lines object
function Lines(content)
{
	this.data = [];
	this.current = -1;

	if(content instanceof Array) this.data = content; 
}

Lines.prototype = 
{
	addLine: function(line) {
		this.data.push(line);
		return this;
	},
    
	clear: function(){
		this.setCurrent(-1);
		this.data.splice(0,this.data.length);
		return this;
	},
    
	setCurrent: function(x){
		this.current = x;
	},
	getNext: function(){
		if(this.data.length > 0)
		{
			if(this.current == -1) 
			{
				this.setCurrent(0);
				return this.data[0];
			}
			else
			{

				if(this.current+1 <= this.data.length-1)
				{
					this.setCurrent(this.current+1);
					return this.data[this.current];
				}
				else{
					this.setCurrent(0);
					return this.data[0];
				}
                            
			}
		}
	return false;
                    
},
       
getPrevious: function(){
          
	if(this.data.length > 0)
	{ 
		if(this.current == -1 || this.current == 0)
		{
			this.setCurrent(this.data.length-1);            
			return this.data[this.current];
                
		}
		else
		{
			this.setCurrent(this.current-1);
			return this.data[this.current];
		}
	}

	return false;
},
    
getCurrent: function()
{
	if(this.data.length > 0)
	{
		if(this.current == -1) this.setCurrent(0);
		return this.data[this.current];
	}
	return false;
},
    
getData: function()
{
	return this.data;
}
    
}//Lines prototype
//------------------------------  