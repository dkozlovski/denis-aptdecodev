
	/**
     * Required options:
     *  selector parent - parent block for control group
     *   
     * Possible options: 
     *  selector down - down button
     *  selector up - up button     
     *  integer max - max value
     *  integer min - min value
     *  bool readOnly
     *  callback onUp | function(input, val) {...}
     *  callback onDown | function(input, val) {...}
     */
(function($, window){

 $.fn.extend({
    spinIt : function(options) {
      
    if(!options.parent) {
        throw new Error('spinIt: parent element is not defined!');
    }   
    var parent = options.parent;

        
      return  this.each(function(){            

            if(isNaN($(this).val())) {
                $(this).val(0);
            }
            
            if(options.readOnly) {
                $(this).attr('readonly', true);
            }
            
            $(this).on('keypress', function (e) {
                if(e.keyCode < 48 || e.keyCode > 57) {
                    return false;
                }
            });
                       
            if(options.up) {
                 
                 var $upBtn = $(this).closest(parent).find(options.up);
                  
                 if(options.max !== undefined) {
                  $upBtn.on('click', function () {
                    if(this.val() < options.max) {
                        var newVal = parseInt(this.val()) + 1;
                        this.val(newVal);
                        if(typeof options.onUp == 'function') {
                            options.onUp(this, newVal);
                        }                        
                    }
                  }.bind($(this)));  
                 } else {
                   $upBtn.on('click', function () {
                    var newVal = parseInt(this.val()) + 1;
                    this.val(newVal);
                        if(typeof options.onUp == 'function') {
                            options.onUp(this, newVal);
                        }                    
                   }.bind($(this))); 
                 }   
                    
            }//if up
            
            
            if(options.down) {
                if(options.min !== undefined) { 
                  $(this).closest(parent).find(options.down).on('click', function () {
                    if(this.val() > options.min) {
                        var newVal = parseInt(this.val()) - 1; 
                        this.val(newVal);
                        if(typeof options.onDown == 'function') {
                            options.onDown(this, newVal);
                        }
                    }
                  }.bind($(this)));   
                } else {
                  $(this).closest(parent).find(options.down).on('click', function() {
                        var newVal = parseInt(this.val()) - 1; 
                        this.val(newVal);
                        if(typeof options.onDown == 'function') {
                            options.onDown(this, newVal);
                        }
                  }.bind($(this)) );  
                }
            }//if down
            
            if(options.down) {
                $(this).closest(parent).find(options.down).on('click', function () {
                    
                });//click down
            }//if down
            
        });
    }
 });  
    
})(jQuery, this)