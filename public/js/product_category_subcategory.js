$(document).ready(function(){
var field = $('#category_id');
var subcategorySelect = document.createElement('select');
subcategorySelect = $(subcategorySelect);

subcategorySelect.attr({'name':'subcategory_id','id':'subcategory_id'});


//category change
field.on('change',function(){

subcategory($(this),subcategorySelect);
    
    
 });//field on change


subcategory(field,subcategorySelect);
});//document ready



function subcategory(field, subcategorySelect)
{
var id = field.val(); 

var postData = {'categoryId':id};

 $.post('/trunk/public/category/async/subcategories',postData,function(data){
 data = $.parseJSON(data);
 subcategorySelect.empty();


    $.each(data, function(key, value) {   
     subcategorySelect
          .append($('<option>', { value : key })
          .text(value));
});

//append or remove subCategory
if($(data).size())
{
  field.after(subcategorySelect);  
} else
    {
    subcategorySelect.remove();
    }
 });//post
    
}




var print_r = function(variable, deep, index) {
		if (variable===undefined) {alert('undefined'); return false;}
		if (deep===undefined) {var deep = 0;}
		if (index===undefined) {var index = '';} else {index+=': ';}
		var mes = '';
		var i = 0;
		var pre = '\n';
		while (i<deep) {pre+='\t'; i++;}
		if (typeof(variable)=='object') {
			mes+=pre+index+' {';
			for (index in variable) {
				mes+=print_r(variable[index], (deep+1), index);
			}
			mes+=pre+'}';
		} else {
			mes+=pre+index+variable;
		}
		if (deep) {return mes;} else {alert(mes);}
	}