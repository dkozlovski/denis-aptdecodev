(function(){
var publishFn = {};
var unpublishFn = {};
var deleteFn = {};
$(document).ready(function(){
var lastVal = '';
var timeout = null;
var statusEl = $('#status');
var ul = $('#suggestions-list');


//console.log($('#filterForm').serializeArray());
//Keyup event

  $('#filter-items').keyup(function(key){

    var value = $(this).val();
    value = value.trim();
    if(value != lastVal)
    {lastVal =  value;
        if(value.length > 0){      
            
     if(timeout !== null)
     {
        clearTimeout(timeout);
     }
     timeout = setTimeout(function(){
        timeout = null;
        $('#suggestions-list').empty();  

        
        $.ajax({
  type: "POST",
  async: false,
  url: 'http://test.loc/trunk/public/user/sale/suggestions',
  data: {'search':value},
  success: function(data){
         data = $.parseJSON(data);
         insertFromObject(data, value);          
        },
});

  var popup = $('.items-to-filter');      
      if(ul.find('li').length > 0)
      {
        popup.fadeIn();
      } else
        {
         popup.fadeOut();   
        }      
        
     },375);

       
    }}
  });
  
/* Publish-Unpublish-Delete
var itemsPerPage = 10; //load from PHP
var currPage = $('#currentPage').text() || 1; // or load from PHP
var totalPageCount = 3; //from PHP

/*function loadFromNext(page, count)//call when load from next page.
{
    var postData = $('#filterForm').serializeArray();
        postData.push({name :'page', value:page});
        postData.push({name:'count', value:count});
    var answer  = false;
        $.ajax({
       async: false,
       url: 'http://test.loc/trunk/public/user/sale/loadfrompage', 
       data: postData,
       type: "POST",
       success: function(data){ 
            data = $.parseJSON(data);
            answer = data;
            createProductRow(data, $('#my-items-table'))
       console.info(data);
       }  
      });
     if(answer)
     { 
        this.setTotalPageCount(answer.totalPageCount);
        $('.pagination pagination-right').html(answer.paginationControlHTML);
     }     
}//<<<loadFromNext*/

//publish
publishFn = function(){
    
    if(updateProduct($(this).val(), 'publish'))
    { 
        if(statusEl.text() == 'Not published')
        {           
            removeRow($(this).closest('tr'), 2000, statusEl);
            $(this).parent().html('<p class="c-nprogress">Published</p>');
        }else
            {
                $(this).addClass('un-publish').text('Unpublish').unbind().click(unpublishFn);
            }
    }
    
}
$(".publish:not('.un-publish')").click(publishFn);

//unpublish
unpublishFn = function(){
    if(updateProduct($(this).val(), 'unpublish'))
    {
        if(statusEl.text() == 'Published')
        {           
            removeRow($(this).closest('tr'), 2000, statusEl);
            $(this).parent().html('<p class="i-nprogress">Unpublished</p>');
        } else 
            {
                $(this).removeClass('un-publish').text('Publish').unbind().click(publishFn);
            }       
    }
}
$('.un-publish').click(unpublishFn);


//delete
deleteFn = function(){
    if(updateProduct($(this).val(), 'delete'))
    {
      $(this).closest('tr').remove();      
      var sc = $('#sales_count');
      if(parseInt(sc.text()) > 0) sc.text(parseInt(sc.text()) - 1);
      refreshTable();
    }
}

$('.delitem').click(deleteFn);

}); //doc ready

 function updateProduct(id, action)//delete publish or unpublish depends on action param
 {
    var result = false;
    
    $.ajax({
       async: false,
       url: 'http://test.loc/trunk/public/user/sale/update', 
       data: {'id':id, 'updAction':action},
       type: "POST",
       success: function(data){ 
       data = $.parseJSON(data);
       result = data.result;
       }  
      });
    
    return result;
 }

 function removeRow(row, delay, element)// element to blink
 {
    element = element || false;
        setTimeout(function(){
            $(row).remove();
            refreshTable();
        },delay);
        if(element) blink(element);
 }
  function blink(element)  
  { 
        var color = element.css('backgroundColor')
       element.parent().animate({'backgroundColor':'#E8AF3C'},500,function(){
        $(this).animate({'backgroundColor':color},1500);        
         });
  }
 
 
 function highlightResult(sVal, resVal)
 {
    var expr = new RegExp("^("+sVal+")","i");
    resVal = resVal.replace(expr,"<b>$1</b>");
    return resVal;
 }
 
 
 function insertFromObject(obj, sVal)
 {
    var list = $('#suggestions-list');
    
    for(i in obj['products'])
    {
      var li = document.createElement('li');
      var a  = document.createElement('a');
      $(a).attr('href','http://test.loc/trunk/public/product/view/id/'+obj['products'][i]['id']).html(highlightResult(sVal,obj['products'][i]['title']));
      li.appendChild(a);
      list.append(li);          
    }
 
 }
 
 
 /*function adjustPaginator(itemsPerPage, currentPage, totalPageCount, itemsCount, fromNext, fromPrevious)
 {
    this.itemsPerPage = itemsPerPage;
    this.currentPage = currentPage;
    this.totalPageCount = totalPageCount;
    this.itemsCount = itemsCount;
    this.fromNext = fromNext;
    this.fromPrevious = fromPrevious;
 }
 
 adjustPaginator.prototype = {
    setCurrentPage: function (number)
    {
      this.currentPage = number;  
    },
    
    setTotalPageCount: function(number)
    {
      this.totalPageCount = number;  
    },
    
   
    setItemsCount : function(number)
    {
      this.itemsCount = number;  
    },
    
    adjust: function()
    {

      if(this.currentPage !== this.totalPageCount && this.itemsCount < this.itemsPerPage) //load from next
      {
        page = this.currentPage;
        count = this.itemsPerPage - this.itemsCount;
          return this.loadFrom(page, this.fromNext)
      }
      
      if(this.currentPage === this.totalPageCount && this.currentPage > 1) // load from previous
      {  
          return this.loadFrom(this.currentPage, this.fromPrevious)
      }
      
      return false;  
    },
    
    loadFrom: function(page, callback)
    {
      return callback.call(this, page) || true;  
    },
 }


var obj = new adjustPaginator(10, 1, 2, [1,2,3,4,5,6,7], function(pn, ec){
    console.log('FromNext called. Getting data from page '+pn+' and count of elements to get is '+ec+'.');    
},function(pn, ec){
    console.log('FromPrevious called. Getting data from page '+pn+' and count of elements to get is '+ec+'.');
});

console.info(obj.adjust());*/

//--------create row

function createProductRow(data, parent)
{
    data = data.products;
    $(parent).empty();
    for(i in data)
    {
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        var td2 = document.createElement('td');
        var td3 = document.createElement('td');
        var td4 = document.createElement('td');
        $(td1).addClass('cart-thumb');
        $(td3).addClass('act');
        $(td4).addClass('take-act');
        var img = document.createElement('img');
        $(img).attr({'alt':data[i].title, 'src':data[i].imgUrl}).appendTo(td1);
        
        var h3 = document.createElement('h3');
        var a = document.createElement('a');
        $(a).attr('href',data[i].linkUrl).text(data[i].title).appendTo(h3);
        $(h3).appendTo(td2);
        
        var ul = document.createElement('ul');
        var li1 = document.createElement('li');
        var li2 = document.createElement('li');
        var li3 = document.createElement('li');
        $(li1).text(data[i].category).appendTo(ul);
        $(li2).text(data[i].shipping).appendTo(ul);
        $(li3).text(data[i].price).appendTo(ul);
        $(ul).appendTo(td2);
        
        if(data[i].isReceived !== 'notSold')
        {
            var p = document.createElement('p');
            if(data[i].isReceived)
                {
                  $(p).text('Delivered')  
                } else
                    {
                        $(p).text('Waiting for delivery');
                    }
            $(p).appendTo(td3)
        } else
            {
                var button  = document.createElement('button');
                var editB   = document.createElement('button'); 
                var deleteB = document.createElement('button');
                
                if(data[i].isPublished)
                {
                    $(button).attr({
                        'name'   : 'unpublish',
                        'class'  : 'publish un-publish',
                        'type'   : 'button',
                        'value'  :  data[i].id,
                    }).text('Unpublish').click(unpublishFn);
                } else
                    {
                    $(button).attr({
                        'name'   : 'publish',
                        'class'  : 'publish',
                        'type'   : 'button',
                        'value'  :  data[i].id,
                    }).text('Publish').click(publishFn);                        
                        
                    }
                    
                $(button).appendTo(td3);
                
                $(editB).attr({'class':'ed-i-tem','type':'button'}).text('Edit').appendTo(td4);//add action
                $(td4).append(document.createTextNode(' '));
                $(deleteB).attr({
                    'name' :'delete',
                    'class':'delitem',
                    'value': data[i].id,
                        }).html('<i></i>').click(deleteFn).appendTo(td4);
                    
            }// for in
        
       $(tr).append(td1).append(td2).append(td3).append(td4);
      // console.info(tr);
       
       $(parent).append(tr);
        
    }
}
//-----Publish, Unpublish, Delete event function
function refreshTable()
{

    var answer = false;      
    var postData = $('#filterForm').serializeArray();
        postData.push({name :'page', value:$('#currentPage').text() || 1});

        $.ajax({
       async: false,
       url: 'http://test.loc/trunk/public/user/sale/loadfrompage', 
       data: postData,
       type: "POST",
       success: function(data){
            data = $.parseJSON(data);
            answer = data;
            createProductRow(data, $('#my-items-table'))
       }  
      }); 
      
     if(answer)
     {
        $('.pagination-right').html(answer.paginationControlHTML);
     }    
}

})();