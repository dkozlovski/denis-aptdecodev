var sortBy = 'date';
var sortDirection = 'DESC';  
  $(document).ready(function(){
var page  = 2;
    
/*===== clear =====================*/
$("a:contains('Clear'):not(.clearall):not(#priceClear)").click(function() {
    var containers = $(this).parent().next('.content');
    var checkBoxes = containers.children('ul').children('li').children('.check-box');
    var brandCB = containers.children('.parent').children('.scrollable').children('ul').children('li').children('.check-box');
    checkBoxes.removeClass('checkedBox');
    brandCB.removeClass('checkedBox');
    checkBoxes.children('input:checked').attr('checked',false);
    brandCB.children('input:checked').attr('checked',false);
    containers.find('.selectedColor').removeClass('selectedColor');    
    postFilter(true); 
    page  = 2;

    return false;
});
$('#priceClear').click(function(){
    page  = 2;
    clearPrice();
     postFilter(true); 
     return false;
     });
/*===== clear all ================*/
$('.clearall').click(function() {
    var checkedBoxes = $('.checkedBox');

    checkedBoxes.children('input:checked').attr('checked',false);
    checkedBoxes.removeClass('checkedBox');
    $('.selectedColor').removeClass('selectedColor');
    clearPrice();
    postFilter(true);
    page = 2;

});

/*=== select all when doc is ready*/
(function(){
    $('.check-box').addClass('checkedBox').children('input:checked').attr('checked',true);
    $('button[data-color]').addClass('selectedColor');
    clearPrice(); 
})();
    
/*===== AJAX events */

 $('li .check-box').click(function(){
    postFilter(true);var page  = 2;
 });//subcategory, brand, condition
 
 $('button[data-color]').click(function(){
    postFilter(true); page  = 2;
 });//color
 
$('#slider').slider().bind({
    slidestop : function() 
                { 
                    postFilter(true); 
                    page = 2;                  
                }
});//price

//-----------------Scroll down event

$(window).scroll(function () {
  if ($(window).scrollTop() + $(window).height() == $(document).height()) {
    postFilter(false,page);
    page++;
  return false;
  }
  })

//----------------Sort by price
$('#sortPrice').click(function(){
    sortBy = 'price';
        if(sortDirection == 'ASC') 
        {
            sortDirection = 'DESC';
        } else
            {
                sortDirection = 'ASC';
            }
        page = 2; 
    postFilter(true);

});//sort

//----------------test function
$('body').click((function test(){

//print_r($("#slider").slider("option","values"));

}));    

    
    
  });//doc ready  
    
    
//param bool replace: add or replace node   

    
    
    
    
    
    
    function  prepareFilterData(page) {
        var data = [];
        data['category'] = [];
        data['brand'] = [];
        data['condition'] = [];
        data['color'] = [];
        data['page'] = page || 1;


//----------------CATEGORY
        $("div[data-category]").each(function(){
            if($(this).hasClass('checkedBox'))
                {
                    data['category'].push($(this).attr('data-category'));
                }            
            });
            data['category'].push($('input[data-category]').attr('data-category'));
//----------------BRAND            
        $("div[data-brand]").each(function(){
            if($(this).hasClass('checkedBox'))
                {
                    data['brand'].push($(this).attr('data-brand'));
                }            
            });
//----------------CONDITION              
        $("div[data-condition]").each(function(){
            if($(this).hasClass('checkedBox'))
                {
                    data['condition'].push($(this).attr('data-condition'));
                }            
            });
            
//----------------COLOR

        $("button[data-color]").each(function(){
            if($(this).hasClass('selectedColor'))
                {
                    data['color'].push($(this).attr('data-color'));
                }            
            });
            
//----------------PRICE
var values = $("#slider").slider('option','values');
        data['price'] = {'min':values[0], 'max':values[1]};

        return data;
    }
    
function clearPrice()
              {
     var price = $("#slider");           
    price.slider({values:[price.slider('option','min'),price.slider('option','max')]});
    $("input.sliderValue[data-index='0']").val('$'+price.slider('option','min'));
    $("input.sliderValue[data-index='1']").val('$'+price.slider('option','max'));
                   
                }
    

    
//------------------obj    
    function Products(data) {
        this.data = [];
        this.length  = 0;
        if(data instanceof Array) 
         {
            this.data = data;
            this.length = this.data.length;
         }
    }
    
    Products.prototype = {
        
        setData: function(data)
            {
               this.data = data;
               this.length = this.data.length;
               return this; 
            } ,
        clear: function ()
            {
               this.data.splice(0,this.length);
               this.length = 0;
               return this;
            },
            
        build: function (node, clear)
        { 
            if(clear) {
                node.html('');
                  $('body,html,document').animate({
                    scrollTop: 0
                        },200);
                }
        
          for(i in this.data)
              {
                var item = document.createElement('li');
                var imgWrapper = document.createElement('div');
                var userWrapper = document.createElement('div');
                var imgP = document.createElement('img');
                var imgU = document.createElement('img');
                var p = document.createElement('p');
                var price = document.createElement('span');
                var star = document.createElement('span');
                
                $(imgWrapper).addClass('img-wrapper first').append(imgP);
                $(imgP).attr({
                    'src':this.data[i]['imagePath'],
                    'alt':this.data['title']
                    });
                $(userWrapper).addClass('user-rating').append(imgU).append(star);
                $(imgU).attr({'src':this.data[i]['sellerAvatar'],'alt':'user raiting'});
                $(star).addClass('star');
                $(price).text(this.data[i]['price']);
                $(p).append(document.createTextNode(this.data[i]['title']+' ')).append(price);
                $(item).append(imgWrapper).append(p).append(userWrapper);
                
                if(!clear) 
                    {
                      $(item).hide().fadeIn(3000);   
                    }
                             
                node.append(item);                
                
              }
             
        },

        test: function()
        {
            print_r(this.data);
        },
    }    
