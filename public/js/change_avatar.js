$(document).ready(function(){
    (function(){

var jcrop_api = false;
var coords;

var progress = $('#loadinprogress');
var err      = $('#avatarfail');
var btn      = $('.btn-file');
var preview  = $('.fileupload-preview');
var img      = $('#prevImg');

function destroyJcrop()
{
if(jcrop_api instanceof Object)
        {
        jcrop_api.destroy();
        preview.hide();
        btn.show();                                
        } 
}

$('#backToUpload').click(destroyJcrop);

$('#saveAvatar').click(function(){
    var postData = coords;
    postData.Width = img.width();
    postData.Height = img.height();
    
    $.post('http://test.loc/trunk/public/uploadimage/crop', {cropParams:postData}, function(data){
        data = $.parseJSON(data);
        if(data.error)
        {
            preview.hide();
            displayError(data.message);
            btn.show();
        } else
            {
                $('.close').click();
                $('#mainPhoto').attr('src', data.src);
                destroyJcrop();
            }
    });   
});



  
function displayError(message) {

 err.empty()
    .append(document.createElement('i'))
    .append(document.createTextNode(message))
    .show();
    
    progress.hide();
  
 } 
  
$('#fileupload').fileupload({
    dataType: 'json',
    limitMultiFileUploads: 1,
    //maxFileSize: 100000,
    //acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
   
    add: function(e, data)
                        { 
                        var allowed = /\.jpe?g|jpg|png|gif$/i;
                        var isAllowed = true;
                        var eFile = {};                          
                        
                         $.each(data.files, function(index, file){
                              if(!allowed.test(file.name))
                                    {
                                        isAllowed = false;
                                        eFile = file; 
                                        return false;                                   
                                    }                          
                         });
                         
                            if(isAllowed)
                            {
                                data.submit();
                            } else
                                {
                            displayError('File "'+eFile.name+'" can not uploaded. Image must be in "jpg, gif or png" format.');
                            btn.show();
                               
                                }                        
                        },
    
    send: function(e, data)
                          { 
                            err.empty();
                            btn.hide();
                            progress.show();                           
                                                        
                          },
                          
    fail: function(e, data)
                          {
                           displayError('Unable to upload image. Please try again later.');                           
                           btn.show();
                           
                          },
    
    done: function(e, data)
                          {                            
                            var file = data.result; 
                            
                            if(file.error)
                            {
                             var msg = file.err_msg || 'File was not accepted.';
                             displayError(msg);
                             btn.show();   
                            } else
                                {
                                progress.hide();
                                
                                
                                preview.show();                                
                                img.attr('src',file.src)                                             
                                             .Jcrop({
                                                
                                                aspectRatio : 1,
                                                bgOpacity  : .3,
                                               // bgColor :'white',
                                                setSelect: [10,10, 100, 100],                                               
                                                
                                                onSelect : function(C) {
                                                    coords = C;
                                                    },
                                             },function(){jcrop_api = this;});    
                                }
                          },
                          
                          
                          
}); //fileupload
    
})();//my obj

    
});//doc ready