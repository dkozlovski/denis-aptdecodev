CREATE TABLE IF NOT EXISTS `blog_home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_post_slug` varchar(255) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `BLOG_POST_SLUG_UNIQUE` (`blog_post_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;