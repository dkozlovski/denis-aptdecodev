ALTER TABLE `orders` ADD `shipping_address_id` INT UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `orders` ADD `billing_address_id` INT UNSIGNED NULL DEFAULT NULL;

DROP TABLE IF EXISTS `order_addresses`;

CREATE TABLE IF NOT EXISTS `order_addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_address_id` int(10) unsigned DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `address_line1` varchar(255) NOT NULL,
  `address_line2` varchar(255) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `state_code` varchar(64) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `postal_code` varchar(20) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `building_type` ENUM( 'elevator', 'walkup' ) DEFAULT NULL,
  `number_of_stairs` SMALLINT UNSIGNED DEFAULT NULL,
  `address_type` enum('billing','shipping') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ORDER_ADDRESSES_TO_USER_ADDRESSES` (`user_address_id`)
) ENGINE=InnoDB;

ALTER TABLE `order_addresses`
  ADD CONSTRAINT `FK_ORDER_ADDRESSES_TO_USER_ADDRESSES` FOREIGN KEY (`user_address_id`) REFERENCES `user_addresses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;