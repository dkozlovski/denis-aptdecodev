CREATE  TABLE `invalid_messages` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `from` INT NULL ,
  `subject` VARCHAR(255) NOT NULL ,
  `text` TEXT NOT NULL ,
  `created` INT UNSIGNED NULL ,
  `matches` TEXT NULL ,
  PRIMARY KEY (`id`) )
COMMENT = 'Messages failed validation i.e. contains email or phone number.';