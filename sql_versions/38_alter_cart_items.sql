ALTER TABLE `cart_items` ADD `price` DECIMAL(12, 5) UNSIGNED NOT NULL DEFAULT '0';

ALTER TABLE `cart_items` ADD `row_total` DECIMAL(12, 5) UNSIGNED NOT NULL DEFAULT '0';

ALTER TABLE `cart_items` ADD `is_active` SMALLINT UNSIGNED NOT NULL DEFAULT '0';

CREATE TRIGGER `cart_items_bins` BEFORE INSERT ON `cart_items`
FOR EACH ROW
SET NEW.`row_total` = NEW.`price` * NEW.`qty` ;

CREATE TRIGGER `cart_items_bupd` BEFORE UPDATE ON `cart_items`
FOR EACH ROW
SET NEW.`row_total` = NEW.`price` * NEW.`qty` ;