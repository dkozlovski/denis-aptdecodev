DROP TABLE IF EXISTS `notification_tasks_email_templates`;
DROP TABLE IF EXISTS `notification_tasks_users`;
DROP TABLE IF EXISTS `notification_tasks`;
DROP TABLE IF EXISTS `email_templates`;

CREATE TABLE `email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `view_path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `notification_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parameters` text NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `notification_tasks_email_templates` (
  `notification_task_id` int(10) unsigned NOT NULL,
  `email_template_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `notification_task_id_UNIQUE` (`notification_task_id`,`email_template_id`),
  KEY `FK_NOTIFICATIOIN_TASKS_EMAIL_TEMPLATES_TO_NOTIFICATION_TASK_idx` (`notification_task_id`),
  KEY `FK_NOTIFICATIOIN_TASKS_EMAIL_TEMPLATES_TO_EMAIL_TEMPLATES_idx` (`email_template_id`),
  CONSTRAINT `FK_NOTIFICATIOIN_TASKS_EMAIL_TEMPLATES_TO_EMAIL_TEMPLATES` FOREIGN KEY (`email_template_id`) REFERENCES `email_templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NOTIFICATIOIN_TASKS_EMAIL_TEMPLATES_TO_NOTIFICATION_TASKS` FOREIGN KEY (`notification_task_id`) REFERENCES `notification_tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `notification_tasks_users` (
  `notification_task_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `counter` smallint(5) unsigned DEFAULT NULL,
  KEY `FK_NOTIFICATION_TASKS_USERS_TO_USERS_idx` (`user_id`),
  KEY `FK_NOTIFICATION_TASKS_USERS_TO_NOTIFICATION_TASKS_idx` (`notification_task_id`),
  CONSTRAINT `FK_NOTIFICATION_TASKS_USERS_TO_NOTIFICATION_TASKS` FOREIGN KEY (`notification_task_id`) REFERENCES `notification_tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NOTIFICATION_TASKS_USERS_TO_USERS` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);