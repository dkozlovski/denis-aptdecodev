drop view `view_product_prices`;

CREATE VIEW `view_product_prices` AS
select `products`.`price` AS `price`, `products`.`category_id` AS `category_id`, `categories`.`parent_id` AS `parent_id`
from `products`
join `categories` on `products`.`category_id` = `categories`.`id`
where `products`.`is_visible` = 1 and `is_approved` = 1 and `is_published` = 1;