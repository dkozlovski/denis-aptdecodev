CREATE  TABLE `google_xml_documents` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `full_path` VARCHAR(255) NOT NULL ,
  `created` INT UNSIGNED NOT NULL ,
  `is_handled` SMALLINT UNSIGNED NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) )
COMMENT = 'Google shopping content/xml feeds';