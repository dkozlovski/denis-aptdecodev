CREATE TABLE IF NOT EXISTS `products_available_till` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '0',
  `days_left` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
