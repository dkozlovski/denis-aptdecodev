CREATE VIEW `view_seller_popularity` AS
SELECT count( * ) AS `sold_products`, `products`.`user_id` AS `user_id`
FROM `order_items`
LEFT JOIN `products` ON `products`.`id` = `order_items`.`product_id`
GROUP BY `products`.`user_id`