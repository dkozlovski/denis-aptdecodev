create table `invitation_admins`(
    `id` int(10) AUTO_INCREMENT PRIMARY KEY,
    `admin_id` int(10) NOT NULL,
    `email` varchar(100) NOT NULL,
    `code` varchar (16) NOT NULL,
    `is_used` smallint (5) NOT NULL  
    );