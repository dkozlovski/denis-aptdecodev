DROP VIEW `view_product_manufacturers`;

CREATE VIEW `view_product_manufacturers` AS SELECT 
`manufacturers`.`id` AS `id`,
`manufacturers`.`title` AS `title`,
`products`.`category_id` AS `category_id`,
`categories`.`parent_id` AS `parent_id`,
`products`.`title` AS `product_title`,
`products`.`description` AS `product_description`

 FROM `manufacturers` 
 JOIN `products` ON `manufacturers`.`id` = `products`.`manufacturer_id`
 JOIN `categories` ON `categories`.`id` = `products`.`category_id`
 
 WHERE `products`.`is_visible` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_published` = 1;
