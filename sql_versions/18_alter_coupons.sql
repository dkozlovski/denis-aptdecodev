DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `type` enum('percent','shipping','fixed') NOT NULL,
  `amount` decimal(5,2) unsigned DEFAULT NULL,
  `created_at` int(10) unsigned NOT NULL,
  `valid_till` int(10) unsigned NOT NULL,
  `coupon_limit` int(10) unsigned NOT NULL DEFAULT '1',
  `user_limit` int(10) unsigned NOT NULL DEFAULT '1',
  `is_active` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `COUPONS_CODE_UNIQUE` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `coupons_users`;
CREATE TABLE IF NOT EXISTS `coupons_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `used_at` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `coupon_id` (`coupon_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

ALTER TABLE `coupons_users`
  ADD CONSTRAINT `FK_COUPONS_USERS_TO_COUPONS` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_COUPONS_USERS_TO_USERS` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
