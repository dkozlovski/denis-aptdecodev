CREATE  TABLE `categories_google_merchant_categories` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `category_id` INT UNSIGNED NOT NULL ,
  `google_merchant_category_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_CGM_TO_CATEGORIES_idx` (`category_id` ASC) ,
  INDEX `FK_CGM_TO_GOOGLE_CATEGORIES_idx` (`google_merchant_category_id` ASC) ,
  CONSTRAINT `FK_CGM_TO_CATEGORIES`
    FOREIGN KEY (`category_id` )
    REFERENCES `categories` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_CGM_TO_GOOGLE_CATEGORIES`
    FOREIGN KEY (`google_merchant_category_id` )
    REFERENCES `google_merchant_categories` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'Defines relations between categories and google_merchant_categories';

INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('31', '105');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('1', '105');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('2', '105');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('18', '105');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('29', '108');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('11', '53');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('3', '53');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('4', '55');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('7', '28');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('21', '78');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('22', '85');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('27', '56');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('32', '53');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('47', '64');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('37', '112');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('38', '119');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('39', '113');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('40', '114');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('41', '77');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('5', '15');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('15', '19');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('16', '25');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('17', '19');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('26', '19');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('30', '108');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('36', '19');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('48', '69');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('33', '32');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('6', '121');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('8', '104');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('10', '32');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('12', '42');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('28', '34');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('34', '112');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('49', '67');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('19', '124');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('13', '127');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('14', '128');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('35', '129');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('23', '89');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('24', '95');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('25', '91');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('42', '221');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('43', '221');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('50', '222');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('51', '221');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('52', '221');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('53', '221');
INSERT INTO `categories_google_merchant_categories` (`category_id`, `google_merchant_category_id`) VALUES ('54', '221');