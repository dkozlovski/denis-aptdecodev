ALTER TABLE `categories_home` ADD COLUMN `alt` VARCHAR(45) NULL DEFAULT NULL  AFTER `image_path` ;
ALTER TABLE `manufacturers_home` ADD COLUMN `alt` VARCHAR(45) NULL DEFAULT NULL  AFTER `image_path` ;

-- CATEGORIES DATA
UPDATE `categories_home` SET `alt`='Buy used beds' WHERE `image_path`='beds.jpg';
UPDATE `categories_home` SET `alt`='Used chairs for sale' WHERE `image_path`='chairs.jpg';
UPDATE `categories_home` SET `alt`='Buy used lightings' WHERE `image_path`='lighting.jpg';
UPDATE `categories_home` SET `alt`='Buy used outdoors' WHERE `image_path`='outdoors.jpg';
UPDATE `categories_home` SET `alt`='Used sofas for sale' WHERE `image_path`='sofas.jpg';
UPDATE `categories_home` SET `alt`='Used storages for sale' WHERE `image_path`='storage.jpg';
UPDATE `categories_home` SET `alt`='Used tables for sale' WHERE `image_path`='tables.jpg';
UPDATE `categories_home` SET `alt`='Buy used home accessories' WHERE `image_path`='homeacc.jpg';

-- BRANDS DATA
UPDATE `manufacturers_home` SET `alt`='Used Pottery Barn furniture' WHERE `image_path`='pottery-barn-logo.png';
UPDATE `manufacturers_home` SET `alt`='Used West Elm furniture' WHERE `image_path`='west-elm-logo.png';
UPDATE `manufacturers_home` SET `alt`='Used Crate & Barrel furniture' WHERE `image_path`='crate-and-barrel-logo.png';
UPDATE `manufacturers_home` SET `alt`='Used Design within Reach furniture' WHERE `image_path`='design-within-reach-logo.png';
UPDATE `manufacturers_home` SET `alt`='Used CB2 furniture' WHERE `image_path`='cb2-logo.png';
UPDATE `manufacturers_home` SET `alt`='Used Natuzzi furniture' WHERE `image_path`='natuzzi-logo.png';

