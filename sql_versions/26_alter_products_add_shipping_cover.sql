ALTER TABLE `products` ADD COLUMN `shipping_cover` SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT 'Percentage that is covered by the seller'  AFTER `qty` ;

ALTER TABLE `payouts` ADD COLUMN `expenses` SMALLINT UNSIGNED NULL COMMENT 'Additional expenses.'  AFTER `summary_price` ;
