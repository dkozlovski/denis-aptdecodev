CREATE  TABLE `product_popularity` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `product_id` INT UNSIGNED NOT NULL ,
  `type` ENUM('view','cart','wishlist') NOT NULL ,
  `count` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `product_id_UNIQUE` (`product_id` ASC, `type` ASC) ,
  INDEX `FK_FROM_PRODUCT_POPULARITY_TO_PRODUCTS_idx` (`product_id` ASC) ,
  CONSTRAINT `FK_FROM_PRODUCT_POPULARITY_TO_PRODUCTS`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'Stores information about how many times product was viewed, added to cart or to wishlist.';
