﻿ALTER TABLE `options` DROP `value`;

CREATE  TABLE IF NOT EXISTS `options_values` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `option_id` INT UNSIGNED NOT NULL ,
  `condition` VARCHAR(255) NOT NULL ,
  `value` SMALLINT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_OPTIONS_VALUES_TO_OPTIONS_idx` (`option_id` ASC) ,
  UNIQUE INDEX `UNIQUE_CONDITION` (`option_id`, `condition` ASC), 
  CONSTRAINT `FK_OPTIONS_VALUES_TO_OPTIONS`
    FOREIGN KEY (`option_id` )
    REFERENCES `options` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

	CREATE  TABLE `individual_options` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `options_values_id` INT UNSIGNED NOT NULL ,
  `user_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) ,
  INDEX `FK_INDIVIDUAL_OPTIONS_TO_OPTIONS_VALUES_idx` (`options_values_id` ASC) ,
  CONSTRAINT `FK_INDIVIDUAL_OPTIONS_TO_OPTIONS_VALUES`
    FOREIGN KEY (`options_values_id` )
    REFERENCES `options_values` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_INDIVIDUAL_OPTIONS_TO_USERS`
    FOREIGN KEY (`user_id` )
    REFERENCES `users` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE); 
	
INSERT INTO `options` (`id`, `code`) VALUES
(1, 'comission_from_buyer'),
(2, 'comission_from_seller'),
(3, 'comission_individual');	
