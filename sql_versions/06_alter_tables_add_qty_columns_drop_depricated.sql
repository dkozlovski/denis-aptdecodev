ALTER TABLE `products` ADD COLUMN `qty` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 1  AFTER `is_reminded_about_expire` ;
ALTER TABLE `products` DROP COLUMN `is_sold`;
ALTER TABLE `cart_items` ADD COLUMN `qty` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 1  AFTER `is_used_on_checkout` ;
ALTER TABLE `order_items` ADD COLUMN `qty` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 1  AFTER `status` ;
ALTER TABLE `payouts` ADD COLUMN `qty` SMALLINT NOT NULL DEFAULT 1  AFTER `is_active` , ADD COLUMN `summary_price` INT NOT NULL  AFTER `qty` ;
