CREATE TABLE `distance_between_postal_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code1` varchar(7) NOT NULL,
  `code2` varchar(7) NOT NULL,
  `distance` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code1_UNIQUE` (`code1`,`code2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='For calculate distance between postal codes application use third party service. This table stores calculation results to prevent repeated calls.'