ALTER TABLE `countries` ADD `sort_order` SMALLINT UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `countries` ADD `is_active` SMALLINT UNSIGNED NOT NULL DEFAULT '1';

UPDATE `countries` SET `sort_order` = '150' WHERE `countries`.`code` = 'AT';
UPDATE `countries` SET `sort_order` = '400' WHERE `countries`.`code` = 'CA';
UPDATE `countries` SET `sort_order` = '2160' WHERE `countries`.`code` = 'CH';
UPDATE `countries` SET `sort_order` = '830' WHERE `countries`.`code` = 'DE';
UPDATE `countries` SET `sort_order` = '2360' WHERE `countries`.`code` = 'US';

INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Afghanistan", "AF", "10", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Åland Islands", "AX", "20", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Albania", "AL", "30", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Algeria", "DZ", "40", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("American Samoa", "AS", "50", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Andorra", "AD", "60", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Angola", "AO", "70", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Anguilla", "AI", "80", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Antarctica", "AQ", "90", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Antigua and Barbuda", "AG", "100", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Argentina", "AR", "110", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Armenia", "AM", "120", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Aruba", "AW", "130", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Australia", "AU", "140", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Azerbaijan", "AZ", "160", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bahamas", "BS", "170", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bahrain", "BH", "180", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bangladesh", "BD", "190", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Barbados", "BB", "200", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Belarus", "BY", "210", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Belgium", "BE", "220", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Belize", "BZ", "230", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Benin", "BJ", "240", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bermuda", "BM", "250", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bhutan", "BT", "260", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bolivia, Plurinational State of", "BO", "270", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bonaire, Sint Eustatius and Saba", "BQ", "280", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bosnia and Herzegovina", "BA", "290", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Botswana", "BW", "300", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bouvet Island", "BV", "310", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Brazil", "BR", "320", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("British Indian Ocean Territory", "IO", "330", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Brunei Darussalam", "BN", "340", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Bulgaria", "BG", "350", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Burkina Faso", "BF", "360", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Burundi", "BI", "370", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Cambodia", "KH", "380", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Cameroon", "CM", "390", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Cape Verde", "CV", "410", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Cayman Islands", "KY", "420", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Central African Republic", "CF", "430", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Chad", "TD", "440", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Chile", "CL", "450", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("China", "CN", "460", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Christmas Island", "CX", "470", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Cocos (Keeling) Islands", "CC", "480", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Colombia", "CO", "490", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Comoros", "KM", "500", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Congo", "CG", "510", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Congo, the Democratic Republic of the", "CD", "520", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Cook Islands", "CK", "530", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Costa Rica", "CR", "540", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Côte d'Ivoire", "CI", "550", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Croatia", "HR", "560", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Cuba", "CU", "570", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Curaçao", "CW", "580", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Cyprus", "CY", "590", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Czech Republic", "CZ", "600", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Denmark", "DK", "610", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Djibouti", "DJ", "620", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Dominica", "DM", "630", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Dominican Republic", "DO", "640", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Ecuador", "EC", "650", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Egypt", "EG", "660", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("El Salvador", "SV", "670", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Equatorial Guinea", "GQ", "680", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Eritrea", "ER", "690", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Estonia", "EE", "700", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Ethiopia", "ET", "710", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Falkland Islands (Malvinas)", "FK", "720", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Faroe Islands", "FO", "730", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Fiji", "FJ", "740", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Finland", "FI", "750", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("France", "FR", "760", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("French Guiana", "GF", "770", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("French Polynesia", "PF", "780", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("French Southern Territories", "TF", "790", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Gabon", "GA", "800", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Gambia", "GM", "810", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Georgia", "GE", "820", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Ghana", "GH", "840", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Gibraltar", "GI", "850", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Greece", "GR", "860", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Greenland", "GL", "870", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Grenada", "GD", "880", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Guadeloupe", "GP", "890", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Guam", "GU", "900", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Guatemala", "GT", "910", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Guernsey", "GG", "920", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Guinea", "GN", "930", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Guinea-Bissau", "GW", "940", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Guyana", "GY", "950", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Haiti", "HT", "960", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Heard Island and McDonald Islands", "HM", "970", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Holy See (Vatican City State)", "VA", "980", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Honduras", "HN", "990", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Hong Kong", "HK", "1000", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Hungary", "HU", "1010", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Iceland", "IS", "1020", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("India", "IN", "1030", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Indonesia", "ID", "1040", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Iran, Islamic Republic of", "IR", "1050", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Iraq", "IQ", "1060", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Ireland", "IE", "1070", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Isle of Man", "IM", "1080", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Israel", "IL", "1090", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Italy", "IT", "1100", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Jamaica", "JM", "1110", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Japan", "JP", "1120", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Jersey", "JE", "1130", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Jordan", "JO", "1140", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Kazakhstan", "KZ", "1150", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Kenya", "KE", "1160", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Kiribati", "KI", "1170", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Korea, Democratic People's Republic of", "KP", "1180", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Korea, Republic of", "KR", "1190", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Kuwait", "KW", "1200", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Kyrgyzstan", "KG", "1210", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Lao People's Democratic Republic", "LA", "1220", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Latvia", "LV", "1230", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Lebanon", "LB", "1240", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Lesotho", "LS", "1250", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Liberia", "LR", "1260", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Libya", "LY", "1270", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Liechtenstein", "LI", "1280", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Lithuania", "LT", "1290", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Luxembourg", "LU", "1300", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Macao", "MO", "1310", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Macedonia, the former Yugoslav Republic of", "MK", "1320", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Madagascar", "MG", "1330", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Malawi", "MW", "1340", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Malaysia", "MY", "1350", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Maldives", "MV", "1360", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Mali", "ML", "1370", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Malta", "MT", "1380", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Marshall Islands", "MH", "1390", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Martinique", "MQ", "1400", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Mauritania", "MR", "1410", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Mauritius", "MU", "1420", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Mayotte", "YT", "1430", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Mexico", "MX", "1440", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Micronesia, Federated States of", "FM", "1450", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Moldova, Republic of", "MD", "1460", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Monaco", "MC", "1470", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Mongolia", "MN", "1480", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Montenegro", "ME", "1490", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Montserrat", "MS", "1500", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Morocco", "MA", "1510", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Mozambique", "MZ", "1520", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Myanmar", "MM", "1530", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Namibia", "NA", "1540", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Nauru", "NR", "1550", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Nepal", "NP", "1560", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Netherlands", "NL", "1570", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("New Caledonia", "NC", "1580", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("New Zealand", "NZ", "1590", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Nicaragua", "NI", "1600", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Niger", "NE", "1610", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Nigeria", "NG", "1620", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Niue", "NU", "1630", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Norfolk Island", "NF", "1640", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Northern Mariana Islands", "MP", "1650", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Norway", "NO", "1660", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Oman", "OM", "1670", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Pakistan", "PK", "1680", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Palau", "PW", "1690", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Palestine, State of", "PS", "1700", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Panama", "PA", "1710", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Papua New Guinea", "PG", "1720", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Paraguay", "PY", "1730", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Peru", "PE", "1740", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Philippines", "PH", "1750", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Pitcairn", "PN", "1760", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Poland", "PL", "1770", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Portugal", "PT", "1780", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Puerto Rico", "PR", "1790", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Qatar", "QA", "1800", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Réunion", "RE", "1810", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Romania", "RO", "1820", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Russian Federation", "RU", "1830", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Rwanda", "RW", "1840", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Saint Barthélemy", "BL", "1850", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Saint Helena, Ascension and Tristan da Cunha", "SH", "1860", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Saint Kitts and Nevis", "KN", "1870", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Saint Lucia", "LC", "1880", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Saint Martin (French part)", "MF", "1890", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Saint Pierre and Miquelon", "PM", "1900", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Saint Vincent and the Grenadines", "VC", "1910", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Samoa", "WS", "1920", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("San Marino", "SM", "1930", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Sao Tome and Principe", "ST", "1940", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Saudi Arabia", "SA", "1950", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Senegal", "SN", "1960", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Serbia", "RS", "1970", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Seychelles", "SC", "1980", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Sierra Leone", "SL", "1990", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Singapore", "SG", "2000", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Sint Maarten (Dutch part)", "SX", "2010", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Slovakia", "SK", "2020", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Slovenia", "SI", "2030", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Solomon Islands", "SB", "2040", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Somalia", "SO", "2050", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("South Africa", "ZA", "2060", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("South Georgia and the South Sandwich Islands", "GS", "2070", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("South Sudan", "SS", "2080", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Spain", "ES", "2090", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Sri Lanka", "LK", "2100", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Sudan", "SD", "2110", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Suriname", "SR", "2120", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Svalbard and Jan Mayen", "SJ", "2130", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Swaziland", "SZ", "2140", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Sweden", "SE", "2150", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Syrian Arab Republic", "SY", "2170", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Taiwan, Province of China", "TW", "2180", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Tajikistan", "TJ", "2190", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Tanzania, United Republic of", "TZ", "2200", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Thailand", "TH", "2210", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Timor-Leste", "TL", "2220", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Togo", "TG", "2230", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Tokelau", "TK", "2240", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Tonga", "TO", "2250", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Trinidad and Tobago", "TT", "2260", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Tunisia", "TN", "2270", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Turkey", "TR", "2280", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Turkmenistan", "TM", "2290", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Turks and Caicos Islands", "TC", "2300", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Tuvalu", "TV", "2310", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Uganda", "UG", "2320", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Ukraine", "UA", "2330", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("United Arab Emirates", "AE", "2340", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("United Kingdom", "GB", "2350", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("United States Minor Outlying Islands", "UM", "2370", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Uruguay", "UY", "2380", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Uzbekistan", "UZ", "2390", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Vanuatu", "VU", "2400", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Venezuela, Bolivarian Republic of", "VE", "2410", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Viet Nam", "VN", "2420", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Virgin Islands, British", "VG", "2430", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Virgin Islands, U.S.", "VI", "2440", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Wallis and Futuna", "WF", "2450", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Western Sahara", "EH", "2460", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Yemen", "YE", "2470", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Zambia", "ZM", "2480", "1");
INSERT INTO `countries`(`name`,`code`,`sort_order`,`is_active`) VALUES("Zimbabwe", "ZW", "2490", "1");
