ALTER TABLE  `cart_user_notified` DROP  `is_notified1` ;

ALTER TABLE  `cart_user_notified` ADD  `last_send_date1` INT( 10 ) NULL ,
ADD  `last_send_date2` INT( 10 ) NULL ,
ADD  `last_send_date3` INT( 10 ) NULL;

UPDATE `cart_user_notified` SET `last_send_date1`= UNIX_TIMESTAMP(), `last_send_date2`= UNIX_TIMESTAMP(), `last_send_date3`= UNIX_TIMESTAMP()