ALTER TABLE `categories` ADD `is_active` SMALLINT UNSIGNED NOT NULL DEFAULT '1';

UPDATE `categories` SET `is_active`= 0 WHERE `parent_id` IS NULL AND `title` = "Outdoor";

UPDATE `categories` SET `is_active`= 0 WHERE `parent_id` IS NOT NULL AND `title` = "Outdoor Chairs";

UPDATE `categories` SET `is_active`= 0 WHERE `parent_id` IS NOT NULL AND `title` = "Outdoor Tables and Chairs";