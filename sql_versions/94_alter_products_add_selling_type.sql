ALTER TABLE `products` ADD COLUMN `selling_type` ENUM('individual_item', 'set') NULL  AFTER `qty` , ADD COLUMN `number_of_items_in_set` SMALLINT(5) UNSIGNED NULL  AFTER `selling_type` ;
