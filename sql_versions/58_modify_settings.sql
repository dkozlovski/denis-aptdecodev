RENAME TABLE  `settings` TO `settings__old` ;



SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


CREATE TABLE IF NOT EXISTS `settings` (
  `entity_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`entity_id`),
  UNIQUE KEY `key` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

INSERT INTO `settings` (`entity_id`, `name`) VALUES
(17, 'abandoned_cart1'),
(18, 'abandoned_cart2'),
(19, 'abandoned_cart3');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `settings_attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_type` varchar(16) NOT NULL,
  `attribute_name` varchar(32) NOT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `settings_attribute` (`attribute_id`, `attribute_type`, `attribute_name`) VALUES
(1, 'int', 'hours'),
(2, 'text', 'text'),
(3, 'varchar', 'subject');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `settings_int` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) NOT NULL,
  `attribute_id` int(10) NOT NULL,
  `value` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  KEY `attribute_id` (`attribute_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

INSERT INTO `settings_int` (`id`, `entity_id`, `attribute_id`, `value`) VALUES
(12, 17, 1, 3),
(13, 18, 1, 24),
(14, 19, 1, 72);

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `settings_text` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) NOT NULL,
  `attribute_id` int(10) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  KEY `attribute_id` (`attribute_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `settings_text` (`id`, `entity_id`, `attribute_id`, `value`) VALUES
(2, 17, 2, 'Hey there,\r\n \r\nDecision-making can be tough. So when we noticed you were considering buying furniture through our site, we figured we''d make your decision easier.\r\n\r\n<b>Here’s a code for $15 off when you check out on AptDeco within the next day: CR12TD. </b>\r\n\r\nA few things you should know:\r\n1. You can reserve the stuff you like & guarantee it''s yours by submitting a purchase request. Sellers have 24 hours to confirm.\r\n2. Enjoy our White Glove delivery service for a flat rate of $65. You can also choose to pick up yourself if you''re a fan of manual labor.\r\n3. We can deliver within 1-2 days once the order is confirmed.\r\n\r\nPlease let me know if you have any questions I can help you with- just reply to this email or call us at +1.347.619.2783.\r\n\r\nHoping you’ll give us a try,\r\n\r\nReham\r\nThe AptDeco Team'),
(3, 18, 2, 'Hey there,\r\n \r\nDecision-making can be tough. So when we noticed you were considering buying furniture through our site, we figured we''d make your decision easier.\r\n\r\n<b>Here’s a code for $15 off when you check out on AptDeco within the next day: CR12TD. </b>\r\n\r\nA few things you should know:\r\n1. You can reserve the stuff you like & guarantee it''s yours by submitting a purchase request. Sellers have 24 hours to confirm.\r\n2. Enjoy our White Glove delivery service for a flat rate of $65. You can also choose to pick up yourself if you''re a fan of manual labor.\r\n3. We can deliver within 1-2 days once the order is confirmed.\r\n\r\nPlease let me know if you have any questions I can help you with- just reply to this email or call us at +1.347.619.2783.\r\n\r\nHoping you’ll give us a try,\r\n\r\nReham\r\nThe AptDeco Team'),
(4, 19, 2, 'Hey there,\r\n \r\nDecision-making can be tough. So when we noticed you were considering buying furniture through our site, we figured we''d make your decision easier.\r\n\r\n<b>Here’s a code for $15 off when you check out on AptDeco within the next day: CR12TD. </b>\r\n\r\nA few things you should know:\r\n1. You can reserve the stuff you like & guarantee it''s yours by submitting a purchase request. Sellers have 24 hours to confirm.\r\n2. Enjoy our White Glove delivery service for a flat rate of $65. You can also choose to pick up yourself if you''re a fan of manual labor.\r\n3. We can deliver within 1-2 days once the order is confirmed.\r\n\r\nPlease let me know if you have any questions I can help you with- just reply to this email or call us at +1.347.619.2783.\r\n\r\nHoping you’ll give us a try,\r\n\r\nReham\r\nThe AptDeco Team');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `settings_varchar` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) NOT NULL,
  `attribute_id` int(10) NOT NULL,
  `value` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_id` (`entity_id`),
  KEY `attribute_id` (`attribute_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;


INSERT INTO `settings_varchar` (`id`, `entity_id`, `attribute_id`, `value`) VALUES
(1, 17, 3, '$15 Credit on AptDeco. 3'),
(2, 18, 3, '$15 Credit on AptDeco. 24'),
(3, 19, 3, '$15 Credit on AptDeco. 72');

-- --------------------------------------------------------

ALTER TABLE `settings_int`
  ADD CONSTRAINT `settings_int_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `settings_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `settings_int_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `settings` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `settings_text`
  ADD CONSTRAINT `settings_text_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `settings_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `settings_text_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `settings` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `settings_varchar`
  ADD CONSTRAINT `settings_varchar_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `settings_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `settings_varchar_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `settings` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;

SET FOREIGN_KEY_CHECKS=1;
