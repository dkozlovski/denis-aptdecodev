ALTER TABLE `admins` ADD `role_id` INT UNSIGNED NOT NULL , ADD INDEX ( `role_id` ) ;

CREATE TABLE IF NOT EXISTS `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `admin_roles` (`id`, `name`) VALUES
(1, 'Super Admin'),
(2, 'Product Manager'),
(3, 'Accounting');

UPDATE `admins` SET `role_id` = 1;

 ALTER TABLE `admins` ADD FOREIGN KEY ( `role_id` ) REFERENCES `admin_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;