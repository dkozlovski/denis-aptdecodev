ALTER TABLE `user_addresses` ADD `postal_code` VARCHAR( 255 ) NULL DEFAULT NULL ;
ALTER TABLE `user_addresses` ADD `telephone_number` VARCHAR( 255 ) NULL DEFAULT NULL ;

UPDATE `user_addresses` SET `postal_code` = `postcode`, `telephone_number` = `phone_number`;