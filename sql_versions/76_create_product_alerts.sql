CREATE TABLE IF NOT EXISTS `product_alerts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `emails` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `collection_id` int(10) unsigned DEFAULT NULL,
  `subcategory_ids` varchar(64) DEFAULT NULL,
  `conditions` varchar(64) DEFAULT NULL,
  `color_ids` varchar(64) DEFAULT NULL,
  `brand_ids` varchar(64) DEFAULT NULL,
  `min_price` int(5) DEFAULT NULL,
  `max_price` int(5) DEFAULT NULL,
  `delete_code` varchar(32) NOT NULL,
  `send_count` int(10) unsigned NOT NULL DEFAULT '0',
  `last_send_date` int(10) unsigned NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

ALTER TABLE `product_alerts`
  ADD CONSTRAINT `product_alerts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;