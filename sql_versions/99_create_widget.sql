CREATE TABLE IF NOT EXISTS `widget_homepage_as_seen_in` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `page_url` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `sort_order` SMALLINT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `widget_homepage_slider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('image','video') NOT NULL,
  `title` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sort_order` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;