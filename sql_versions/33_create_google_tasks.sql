CREATE TABLE `google_shopping_content_tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `operation` enum('insert','update','delete') NOT NULL,
  `status` enum('pending','closed') NOT NULL,
  `created_date` int(11) NOT NULL,
  `execution_date` int(11) DEFAULT NULL,
  `exp_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_GOOGLE_QUEUE_TO_PRODUCTS_idx` (`product_id`),
  CONSTRAINT `FK_GOOGLE_QUEUE_TO_PRODUCTS` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) 
COMMENT = 'Stores task (insert/update/delete)';