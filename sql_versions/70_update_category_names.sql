UPDATE `collections` SET `name` = "Vintage + Antiques" WHERE `name` = "Antiques";
UPDATE `categories` SET `title` = "Decor" WHERE `title` = "Home Accessories";
UPDATE `categories` SET `title` = "Décor" WHERE `title` = "Decor";