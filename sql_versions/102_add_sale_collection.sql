INSERT INTO `collections` (`id`, `name`, `description`, `image_path`, `page_url`, `is_active`) VALUES (NULL , "Sale", NULL , NULL , 'sale', '1');

set @collection_id = LAST_INSERT_ID();
INSERT INTO `collections_products` (`collection_id`, `product_id`) SELECT @collection_id, `id` FROM products WHERE price < old_price;

INSERT INTO `collections_menus` (`collection_id`, `menu_id`)
	SELECT `collections`.`id`, `menus`.`id` FROM `collections`
		JOIN `menus` ON `menus`.`name` = 'main_secondary'
	WHERE `collections`.`name` = 'Sale';