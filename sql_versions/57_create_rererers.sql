CREATE TABLE IF NOT EXISTS `referers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `referer` varchar(255) NOT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

ALTER TABLE `referers`
  ADD CONSTRAINT `referers_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;