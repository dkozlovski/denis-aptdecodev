CREATE TABLE `eav_users_varchar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_id_UNIQUE` (`entity_id`,`attribute_id`),
  KEY `FK_EAV_USERS_VARCHAR_TO_USERS_idx` (`entity_id`),
  CONSTRAINT `FK_EAV_USERS_VARCHAR_TO_USERS` FOREIGN KEY (`entity_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)