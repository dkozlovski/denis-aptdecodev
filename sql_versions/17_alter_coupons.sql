ALTER TABLE `coupons` ADD `limit` INT UNSIGNED NOT NULL DEFAULT '1' AFTER `redeem_date`;
ALTER TABLE `coupons` DROP `redeem_date`;
