update `products` left join
(SELECT `product_id`, sum(`count`) as `suma`
FROM `product_popularity`
GROUP BY `product_id`) as `pp` on (`products`.`id` = `pp`.`product_id`)
set `products`.`product_popularity` = `pp`.`suma`