DROP TABLE IF EXISTS `session`;

CREATE TABLE IF NOT EXISTS `session` (
  `id` varchar(64) NOT NULL DEFAULT '',
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
