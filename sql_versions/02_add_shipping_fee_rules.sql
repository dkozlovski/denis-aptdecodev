CREATE  TABLE  IF NOT EXISTS `shipping_fee_rules` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `operator` VARCHAR(10) NULL ,
  `sum` DOUBLE NOT NULL,
  `fee` DOUBLE NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `uniq_pair_opertor_sum` (`operator`, `sum` ASC) );