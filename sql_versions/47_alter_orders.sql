DROP TABLE IF EXISTS `orders`;

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `user_full_name` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `coupon_id` varchar(255) DEFAULT NULL,
  `billing_address_id` int(10) unsigned DEFAULT NULL,
  `shipping_address_id` int(10) unsigned DEFAULT NULL,
  `free_shipping` smallint(5) unsigned NOT NULL DEFAULT '0',
  `subtotal_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `shipping_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `discount_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `transaction_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `total_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `total` decimal(12,5) unsigned NOT NULL,
  `payment_id` int(10) unsigned DEFAULT NULL,
  `created_at` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;