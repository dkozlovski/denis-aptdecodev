CREATE TABLE `app_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ;



CREATE TABLE `users_app_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `app_setting_id` int(10) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`,`app_setting_id`),
  KEY `FK_USERS_APP_SETTINGS_TO_USERS_idx` (`user_id`),
  KEY `FK_USERS_AOO_SETTINGS_TO_APP_SETTINGS_idx` (`app_setting_id`),
  CONSTRAINT `FK_USERS_AOO_SETTINGS_TO_APP_SETTINGS` FOREIGN KEY (`app_setting_id`) REFERENCES `app_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_USERS_APP_SETTINGS_TO_USERS` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ;