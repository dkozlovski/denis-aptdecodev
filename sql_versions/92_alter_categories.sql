/* Add Dining Sets under Tables category*/
INSERT INTO `categories` (`parent_id`, `title`, `sort_order`, `page_title`, `page_url`, `is_active`)
VALUES ('37', 'Dining sets', '50', 'New and used dining sets for sale', 'dining-sets', '1');

/* Move products from Sofa Beds (under Beds) to Sofa Beds (under Sofas) */
UPDATE `products` SET `category_id` = 29 WHERE `category_id` = 30;

/* Disable Sofa Beds under Beds*/
UPDATE `categories` SET `is_active` = 0 WHERE `id` = 30;

/* Bedside Tables */
UPDATE `categories` SET `title` = "Nightstands" WHERE `id` = 6;

/* Bookcases and Shelves */
UPDATE `categories` SET `title` = "Bookcases" WHERE `id` = 8;

/* Cabinets */
UPDATE `categories` SET `title` = "Dressers" WHERE `id` = 10;

/* Chest and Drawers */
UPDATE `categories` SET `title` = "Chests / Trunks" WHERE `id` = 12;

/* Storage Tables */
UPDATE `categories` SET `is_active` = 0 WHERE `id` = 34;

/* TV Stands */
UPDATE `categories` SET `title` = "Media Storage" WHERE `id` = 49;

/* https://www.aptdeco.com/catalog/vintage-black-wicker-trunk */
UPDATE `products` SET `category_id` = 12 WHERE `id` = 26705;

/* https://www.aptdeco.com/catalog/cb2-low-dresser-coffee-table-tv-stand */
UPDATE `products` SET `category_id` = 49 WHERE `id` = 27163;

/* https://www.aptdeco.com/catalog/pottery-barn-wicker-trunk-xl */
UPDATE `products` SET `category_id` = 12 WHERE `id` = 26326;

/* https://www.aptdeco.com/catalog/bathroom-vanity-2 */
UPDATE `products` SET `category_id` = 12 WHERE `id` = 27937;

/* Move products from Storage Tables to Chests/Trunks */
UPDATE `products` SET `category_id` = 12 WHERE `category_id` = 34;

/* Add Armoires / Wardrobes under Storage category*/
INSERT INTO `categories` (`parent_id`, `title`, `sort_order`, `page_title`, `page_url`, `is_active`)
VALUES ('33', 'Armoires / Wardrobes', '50', 'New and used armoires & wardrobes for sale', 'armoires-wardrobes', '1');

/* Disable Room Accessories under Decor */
UPDATE `categories` SET `is_active` = 0 WHERE `id` = 43;

/* Disable Other Room Accessories under Decor */
UPDATE `categories` SET `is_active` = 0 WHERE `id` = 54;

/* Add Armoires / Wardrobes under Decor category*/
INSERT INTO `categories` (`parent_id`, `title`, `sort_order`, `page_title`, `page_url`, `is_active`)
VALUES ('42', 'Other Décor', '100', 'New and used decor for sale', 'other-decor', '1');

UPDATE `products` SET `category_id` = 57 WHERE `category_id` IN (43, 54);


/* Disable Desk Lamps under Lightning */
UPDATE `categories` SET `is_active` = 0 WHERE `id` = 13;

/* Move products from Desk Lamps to Table Lamps */
UPDATE `products` SET `category_id` = 35 WHERE `category_id` = 13;

/* Add Ceiling Lamps under Lightning */
INSERT INTO `categories` (`parent_id`, `title`, `sort_order`, `page_title`, `page_url`, `is_active`)
VALUES ('19', 'Ceiling Lamps', '100', 'New and used ceiling lamps for sale', 'ceiling-lamps', '1');

/* Add Wall Lamps under Lightning */
INSERT INTO `categories` (`parent_id`, `title`, `sort_order`, `page_title`, `page_url`, `is_active`)
VALUES ('19', 'Wall Lamps', '100', 'New and used wall lamps for sale', 'wall-lamps', '1');
 
/* Add Pillows under Decor */
INSERT INTO `categories` (`parent_id`, `title`, `sort_order`, `page_title`, `page_url`, `is_active`)
VALUES ('42', 'Pillows', '100', 'New and used pillows lamps for sale', 'pillows', '1');

/* Add Tabletop under Decor */
INSERT INTO `categories` (`parent_id`, `title`, `sort_order`, `page_title`, `page_url`, `is_active`)
VALUES ('42', 'Tabletop', '100', 'New and used table tops lamps for sale', 'table-tops', '1');
