ALTER TABLE  `coupons_users` ADD  `order_id` INT( 10 ) UNSIGNED NULL AFTER  `user_id` ,
ADD INDEX (  `order_id` )

ALTER TABLE  `coupons_users` ADD FOREIGN KEY (  `order_id` ) REFERENCES  `aptdeco`.`orders` (
`id`
) ON DELETE SET NULL ON UPDATE SET NULL ;