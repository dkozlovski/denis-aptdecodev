CREATE TABLE IF NOT EXISTS `wishlist_user_notified` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User Id',
  `last_send_date1` int(10) DEFAULT NULL,
  `last_send_date2` int(10) DEFAULT NULL,
  `last_send_date3` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

UPDATE `wishlist_user_notified` SET `last_send_date1`= UNIX_TIMESTAMP(), `last_send_date2`= UNIX_TIMESTAMP(), `last_send_date3`= UNIX_TIMESTAMP()