CREATE  TABLE `newsletter_emails` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(100) NOT NULL ,
  `user_id` INT UNSIGNED NULL ,
  `unsubscribe` SMALLINT NULL ,
  `unsubscribe_code` VARCHAR(32) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) ,
  INDEX `uns_code` (`unsubscribe_code` ASC) ,
  INDEX `FK_NEWSLETTER_EMAILS_TO_USERS_idx` (`user_id` ASC) ,
  CONSTRAINT `FK_NEWSLETTER_EMAILS_TO_USERS`
    FOREIGN KEY (`user_id` )
    REFERENCES `users` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE);

