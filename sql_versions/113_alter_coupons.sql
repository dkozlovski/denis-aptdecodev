ALTER TABLE  `coupons` ADD  `category_id` INT( 10 ) UNSIGNED NULL AFTER  `user_limit` ,
ADD  `postcode` VARCHAR ( 6 )  NULL AFTER  `category_id` ,
ADD  `min_price` DECIMAL( 12, 2 ) UNSIGNED NULL AFTER  `postcode` ,
ADD  `max_price` DECIMAL( 12, 2 ) UNSIGNED NULL AFTER  `min_price`
