CREATE TABLE  `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
);

CREATE TABLE `collections_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `collection_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_COLLECTIONS_MENUS_TO_COLLECTIONS_idx` (`collection_id`),
  KEY `FK_COLLECTIONS_MENUS_TO_MENUS_idx` (`menu_id`),
  CONSTRAINT `FK_COLLECTIONS_MENUS_TO_COLLECTIONS` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_COLLECTIONS_MENUS_TO_MENUS` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO `menus` (`name`, `description`) VALUES ('main_top', 'Main menu.');
INSERT INTO `menus` (`name`, `description`) VALUES ('main_secondary', 'Secondary menu. Located in header.');
INSERT INTO `menus` (`name`, `description`) VALUES ('homepage_tile', 'Located on homepage.');

INSERT INTO `collections_menus` (`collection_id`, `menu_id`) 
    VALUES (
        (SELECT `collections`.`id` FROM `collections` WHERE `collections`.`name` = 'Antiques' LIMIT 1), 
        (SELECT `menus`.`id` FROM `menus` WHERE `menus`.`name` = 'main_secondary' LIMIT 1));

INSERT INTO `collections_menus` (`collection_id`, `menu_id`) 
    VALUES (
        (SELECT `collections`.`id` FROM `collections` WHERE `collections`.`name` = "Kid's Corner" LIMIT 1), 
        (SELECT `menus`.`id` FROM `menus` WHERE `menus`.`name` = 'main_secondary' LIMIT 1));        

INSERT INTO `collections_menus` (`collection_id`, `menu_id`) 
    VALUES (
        (SELECT `collections`.`id` FROM `collections` WHERE `collections`.`name` = 'Antiques' LIMIT 1), 
        (SELECT `menus`.`id` FROM `menus` WHERE `menus`.`name` = 'homepage_tile' LIMIT 1));