ALTER TABLE `orders` ADD `user_full_name` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `user_id`;
ALTER TABLE `orders` ADD `user_email` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `user_full_name`;

ALTER TABLE `user_addresses` CHANGE `user_id` `user_id` INT( 10 ) UNSIGNED NULL DEFAULT NULL ;
ALTER TABLE `user_addresses` ADD `building_type` ENUM( 'elevator', 'walkup' ) NULL DEFAULT NULL ;
ALTER TABLE `user_addresses` ADD `number_of_stairs` SMALLINT UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `order_items` ADD `hold_uri` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `status` ;