UPDATE `settings_text` as `st`
JOIN `settings_attribute` AS `sa`  ON `st`.`attribute_id` = `sa`.`attribute_id` AND `sa`.`attribute_name` = 'text'
JOIN `settings` AS `s` ON `st`.`entity_id` = `s`.`entity_id` AND `s`.`name`='email_notification_to_seller_price_lowered'

SET `value`= '<div style="background: #f5f5f5; margin: 0 20px 30px 20px; padding: 20px; font-size: 16px; color: #666;"> Hi %first_name%, <br /><br />
      Greetings from AptDeco!  We just wanted to let you know that, per your request, we’ve begun actively managing the price of your <strong style="font-weight: bold;">%product_title% </strong> from now until your listing expires.  Your original price was <strong style="font-weight: bold;">%old_price%</strong> , and we’ve just dropped it to <strong style="font-weight: bold;">%price%</strong>. We think this could help you to complete a sale in the next few days!
      <br /><br />
      As always, please reply or give us a call at <strong style="font-weight: bold;">347-619-2783</strong> if you have any questions.  We’re happy to discuss our pricing management or anything else that might be of help.
  </div>';


UPDATE `settings_varchar` as `sv`
JOIN `settings_attribute` AS `sa`  ON `sv`.`attribute_id` = `sa`.`attribute_id` AND `sa`.`attribute_name` = 'subject'
JOIN `settings` AS `s` ON `sv`.`entity_id` = `s`.`entity_id` AND `s`.`name`='email_notification_to_seller_price_lowered'

SET `value`= 'Price Drop Alert for %product_title%';