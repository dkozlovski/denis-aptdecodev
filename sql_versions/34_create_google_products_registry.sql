CREATE  TABLE `google_products_registry` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `product_id` INT UNSIGNED NOT NULL ,
  `updated` INT UNSIGNED NOT NULL ,
  `status` ENUM('active','deleted','error') NOT NULL ,
  `exp_date` INT NULL, 
  `error_text` VARCHAR(255) NULL,
  PRIMARY KEY (`id`) ,
  INDEX `FK_GOOGLE_PRODUCTS_REGISTRY_TO_PRODUCTS_idx` (`product_id` ASC) ,
  CONSTRAINT `FK_GOOGLE_PRODUCTS_REGISTRY_TO_PRODUCTS`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
COMMENT = 'Information about state of products';