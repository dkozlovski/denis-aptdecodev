CREATE  TABLE `manufacturers_association_list` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `manufacturer_id` INT UNSIGNED NOT NULL ,
  `association` VARCHAR(60) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `association_UNIQUE` (`association` ASC) ,
  INDEX `FK_manufacturers_association_list_TO_MANUFACTURERS_idx` (`manufacturer_id` ASC) ,
  CONSTRAINT `FK_manufacturers_association_list_TO_MANUFACTURERS`
    FOREIGN KEY (`manufacturer_id` )
    REFERENCES `manufacturers` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'This table contains association list. Used to correct typos.';