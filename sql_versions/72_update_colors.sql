ALTER TABLE `colors` ADD `is_active` SMALLINT UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `colors` ADD `sort_order` SMALLINT UNSIGNED NOT NULL DEFAULT '0';

UPDATE `colors` SET `is_active` = 1;

UPDATE `colors` SET `hex` = "#000000", `sort_order` = 10 WHERE `title` = "Black";

INSERT INTO `colors` (`title`, `hex`, `element_id`, `is_active`, `sort_order`) VALUES ("Dark Grey", "#666666", "darkgrey-2", 1, 20);
INSERT INTO `colors` (`title`, `hex`, `element_id`, `is_active`, `sort_order`) VALUES ("Dark Brown", "#663300", "darkbrown-2", 1, 30);

UPDATE `colors` SET `title` = "Light Green" WHERE `element_id` = "litgreen-2";

INSERT INTO `colors` (`title`, `hex`, `element_id`, `is_active`, `sort_order`) VALUES ("Purple", "#663399", "purple-2", 1, 40);

UPDATE `colors` SET `hex` = "#003399", `sort_order` = 50 WHERE `title` = "Blue";
UPDATE `colors` SET `hex` = "#336633", `sort_order` = 60 WHERE `title` = "Green";
UPDATE `colors` SET `hex` = "#FFFF66", `sort_order` = 70 WHERE `title` = "Yellow";
UPDATE `colors` SET `hex` = "#CC6600", `sort_order` = 80 WHERE `title` = "Orange";
UPDATE `colors` SET `hex` = "#CC3333", `sort_order` = 90 WHERE `title` = "Red";

INSERT INTO `colors` (`title`, `hex`, `element_id`, `is_active`, `sort_order`) VALUES ("Natural", "#CAB796", "natural-2", 1, 100);
INSERT INTO `colors` (`title`, `hex`, `element_id`, `is_active`, `sort_order`) VALUES ("Light Grey / Silver", "#CCCCCC", "lightgrey-2", 1, 110);

UPDATE `colors` SET `sort_order` = 120 WHERE `title` = "White";
UPDATE `colors` SET `title` = "Patterned / Multi", `sort_order` = 130 WHERE `title` = "Pattern";

UPDATE `colors` SET `is_active` = 0 WHERE `title` IN ("Grey", "Brown", "Beige", "Pink", "Light Green", "Gold", "Silver");


DROP VIEW `view_product_colors`;

CREATE VIEW `view_product_colors` AS

select
`colors`.`id` AS `id`,
`colors`.`title` AS `title`,
`colors`.`hex` AS `hex`,
`colors`.`element_id` AS `element_id`,
`colors`.`is_active` AS `is_active`,
`products`.`category_id` AS `category_id`,
`categories`.`parent_id` AS `parent_id`,
`products`.`title` AS `product_title`,
`products`.`description` AS `product_description`,
`products`.`manufacturer_id` AS `manufacturer_id`

from ((`colors` join `products` on((`colors`.`id` = `products`.`color_id`))) join `categories` on((`categories`.`id` = `products`.`category_id`)))

WHERE `colors`.`is_active` = 1 ORDER BY `colors`.`sort_order`;

UPDATE `colors` SET `hex` = "#515151" WHERE `title` = "Dark Grey";