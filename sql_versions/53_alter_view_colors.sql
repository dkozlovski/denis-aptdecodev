DROP VIEW `view_product_colors`;

CREATE VIEW `view_product_colors` AS SELECT 
`colors`.`id` AS `id`,
`colors`.`title` AS `title`,
`colors`.`hex` AS `hex`,
`colors`.`element_id` AS `element_id`,
`products`.`category_id` AS `category_id`,
`categories`.`parent_id` AS `parent_id`,
`products`.`title` AS `product_title`,
`products`.`description` AS `product_description`,
`products`.`manufacturer_id` AS `manufacturer_id`
FROM `colors` 
JOIN `products` ON `colors`.`id` = `products`.`color_id`
JOIN `categories` ON `categories`.`id` = `products`.`category_id`;