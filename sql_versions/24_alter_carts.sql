ALTER TABLE `carts` ADD `user_id` INT UNSIGNED DEFAULT NUll ;
ALTER TABLE `carts` ADD `is_notified` SMALLINT UNSIGNED DEFAULT 0 ;
ALTER TABLE `cart_items` ADD `created_at` INT UNSIGNED NOT NULL ;
ALTER TABLE `users` ADD `created_at` INT UNSIGNED DEFAULT NULL ;

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hours` smallint(5) unsigned NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;