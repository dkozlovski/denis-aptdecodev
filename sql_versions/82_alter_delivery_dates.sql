ALTER TABLE  `delivery_dates` ADD  `is_first_period_for_small_available` SMALLINT( 5 ) NOT NULL DEFAULT  '0' AFTER  `is_first_period_available`;
ALTER TABLE  `delivery_dates` ADD  `is_second_period_for_small_available` SMALLINT( 5 ) NOT NULL DEFAULT  '0' AFTER  `is_second_period_available`;
ALTER TABLE  `delivery_dates` ADD  `is_third_period_for_small_available` SMALLINT( 5 ) NOT NULL DEFAULT  '0' AFTER  `is_third_period_available`
