CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('int','smallint','varchar','text','decimal','float','date','datetime','bigint','mediumtext') NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`,`type`)
);

CREATE TABLE `eav_products_int` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_id_UNIQUE` (`entity_id`,`attribute_id`),
  KEY `FK_EAV_PRODUCTS_INT_TO_PRODUCTS_idx` (`entity_id`),
  CONSTRAINT `FK_EAV_PRODUCTS_INT_TO_PRODUCTS` FOREIGN KEY (`entity_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `eav_products_smallint` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `value` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_id_UNIQUE` (`entity_id`,`attribute_id`),
  KEY `FK_EAV_PRODUCTS_SMALLINT_TO_PRODUCTS_idx` (`entity_id`),
  CONSTRAINT `FK_EAV_PRODUCTS_SMALLINT_TO_PRODUCTS` FOREIGN KEY (`entity_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `eav_products_text` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_id_UNIQUE` (`entity_id`,`attribute_id`),
  KEY `FK_EAV_PRODUCTS_TEXT_TO_PRODUCTS_idx` (`entity_id`),
  CONSTRAINT `FK_EAV_PRODUCTS_TEXT_TO_PRODUCTS` FOREIGN KEY (`entity_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `eav_products_varchar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_id_UNIQUE` (`entity_id`,`attribute_id`),
  KEY `FK_EAV_PRODUCTS_VARCHAR_TO_PRODUCTS_idx` (`entity_id`),
  CONSTRAINT `FK_EAV_PRODUCTS_VARCHAR_TO_PRODUCTS` FOREIGN KEY (`entity_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);