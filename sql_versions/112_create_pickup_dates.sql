CREATE TABLE IF NOT EXISTS `pickup_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL,
  `date_string` varchar(20) NOT NULL,
  `is_first_period_available` smallint(5) NOT NULL DEFAULT '0',
  `is_first_period_for_small_available` smallint(5) NOT NULL DEFAULT '0',
  `is_second_period_available` smallint(5) NOT NULL DEFAULT '0',
  `is_second_period_for_small_available` smallint(5) NOT NULL DEFAULT '0',
  `is_third_period_available` smallint(5) NOT NULL DEFAULT '0',
  `is_third_period_for_small_available` smallint(5) NOT NULL DEFAULT '0',
  `is_fourth_period_available` smallint(5) NOT NULL DEFAULT '0',
  `is_fourth_period_for_small_available` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `date_string_UNIQUE` (`date_string`),
  KEY `date_idx` (`date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8