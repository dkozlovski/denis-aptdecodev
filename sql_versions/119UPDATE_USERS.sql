update `users`left join (select `products`.`user_id` as `user`, sum(`oi`.`suma`) as `pop`
from `products`  left join (select `order_items`.`product_id`, sum(`order_items`.`qty`) as `suma`
from `order_items`
group by `order_items`.`product_id`) as `oi` on (`products`.`id` = `oi`.`product_id`)
group by `products`.`user_id`) as `p_oi` on (`users`.`id` = `p_oi`.user)
set `users`.`seller_popularity` = `p_oi`.`pop`