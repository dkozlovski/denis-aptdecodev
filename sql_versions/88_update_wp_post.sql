UPDATE `wp_posts` SET `post_content` ='           <div class="the-post-content mceContentBody">
                                <div class="content-wrapper">
                                    <p>
                                        AptDeco is a home grown company created right here in New York City in 2013. The idea for AptDeco was born out of pure necessity: after numerous bad experiences dealing with current services, we all thought to ourselves "there must be a way to buy or sell furniture without the scams, hassle, or creepy people going into your apartment!".
                                    </p>

                                    <p id="contact-us">
                                        From that lone statement AptDeco was born and is dedicated to creating a simple and dare we say enjoyable process for buying & selling quality pre-owned furniture. AptDeco offers a complete end-to-end process to bring back the joy of discovering amazing furniture finds for your home. With a trusted and verified community of buyers and sellers, pre-arranged pick up and delivery and a secure payment process you can finally sit back, relax and enjoy your finds while we take care of those pesky details in the background.
                                    </p>

                                </div>

                            </div>
                                <h3 class="the-post-title">Contact Us</h3>
                                <ul class="list-unstyled text-std">
                                    <li><i class="apt-i-msg top-3"></i> hello@aptdeco.com (monitored dail)</li>
                                    <li><i class="apt-icon-tel top-3"></i> (347) 619-2783 (Monday - Friday 9AM - 7PM EST)</li>
                                </ul>

                                <h3 class="the-post-title">Partners</h3>
                                <ul class="list-unstyled text-std">
                                    <li>
                                        Moving:
                                        <a href="#" class="text-primary dark">Unpakt</a>
                                    </li>
                                    <li>
                                        Interior Design:
                                        <a href="" class="text-primary dark">HomePolish/Sarah</a>
                                    </li>
                                    <li>
                                        Cleaning:
                                        <a href="#" class="text-primary dark">Jared/Handybook</a>
                                    </li>
                                    <li>
                                        Donation:
                                        <a href="#" class="text-primary dark">Habita4Humanity</a>
                                    </li>
                                    <li>
                                        Couchdoctor:
                                        <a href="#" class="text-primary dark">AFS</a>
                                    </li>
                                    <li><br />
                                        <a href="#contact-us" class="text-primary dark">Contact us</a>
                                        to become our partner
                                    </li>
                                </ul>' WHERE  `post_name` LIKE  'about-us';





/**

AptDeco is a home grown company created right here in New York City in 2013. The idea for AptDeco was born out of pure necessity: after numerous bad experiences dealing with current services, we all thought to ourselves “there must be a way to buy or sell furniture without the scams, hassle, or creepy people going into your apartment!”.

From that lone statement AptDeco was born and is dedicated to creating a simple and dare we say enjoyable process for buying &amp; selling quality pre-owned furniture. AptDeco offers a complete end-to-end process to bring back the joy of discovering amazing furniture finds for your home. With a trusted and verified community of buyers and sellers, pre-arranged pick up and delivery and a secure payment process you can finally sit back, relax and enjoy your finds while we take care of those pesky details in the background.
<h3>Contact Us</h3>
<pre><span style="line-height: 0.2; font-size: 1rem;">Email: hello@aptdeco.com (monitored daily)
</span><em id="__mceDel">Phone: (347) 619-2783 (Monday - Friday 9AM - 7PM EST)</em></pre>*/