CREATE TABLE `eav_users_smallint` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `value` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_id_UNIQUE` (`entity_id`,`attribute_id`),
  CONSTRAINT `FK_EAV_USERS_SMALLINT_TO_USERS` FOREIGN KEY (`entity_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)