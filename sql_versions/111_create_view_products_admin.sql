CREATE VIEW `view_products_admin`
AS select
`products`.`id` AS `id`,
`products`.`user_id` AS `user_id`,
`products`.`title` AS `title`,
`products`.`description` AS `description`,
`products`.`price` AS `price`,
`products`.`old_price` AS `old_price`,
`products`.`original_price` AS `original_price`,
`products`.`category_id` AS `category_id`,
`products`.`manufacturer_id` AS `manufacturer_id`,
`products`.`condition` AS `condition`,
`products`.`material_id` AS `material_id`,
`products`.`color_id` AS `color_id`,
`products`.`width` AS `width`,
`products`.`height` AS `height`,
`products`.`depth` AS `depth`,
`products`.`age` AS `age`,
`products`.`available_from` AS `available_from`,
`products`.`available_till` AS `available_till`,
`products`.`is_published` AS `is_published`,
`products`.`created_at` AS `created_at`,
`products`.`updated_at` AS `updated_at`,
`products`.`approved_at` AS `approved_at`,
`products`.`is_pick_up_available` AS `is_pick_up_available`,
`products`.`is_delivery_available` AS `is_delivery_available`,
`products`.`is_available_for_purchase` AS `is_available_for_purchase`,
`products`.`is_visible` AS `is_visible`,
`products`.`pick_up_address_id` AS `pick_up_address_id`,
`products`.`is_approved` AS `is_approved`,
`products`.`pickup_full_name` AS `pickup_full_name`,
`products`.`pickup_address_line1` AS `pickup_address_line1`,
`products`.`pickup_address_line2` AS `pickup_address_line2`,
`products`.`pickup_city` AS `pickup_city`,
`products`.`pickup_state` AS `pickup_state`,
`products`.`pickup_postcode` AS `pickup_postcode`,
`products`.`pickup_country_code` AS `pickup_country_code`,
`products`.`pickup_phone` AS `pickup_phone`,
`products`.`page_url` AS `page_url`,
`products`.`qty` AS `qty`,
`products`.`expire_at` AS `expire_at`,
`products`.`shipping_cover` AS `shipping_cover`,
`products`.`is_verified` AS `is_verified`,
`products`.`is_under_15_pounds` AS `is_under_15_pounds`,
`categories`.`parent_id` AS `parent_category_id`,
`users`.`rating` AS `seller_rating`,

(case when (`products`.`qty` > 0) then '0' else '1' end) AS `is_product_sold`,
if(((isnull(`products`.`available_till`) or (`products`.`available_till` = 0) or (`products`.`available_till` > unix_timestamp())) and ((`products`.`expire_at` = 0) or (`products`.`expire_at` > unix_timestamp()))),0,1) AS `is_expired` ,
`view_seller_popularity`.`sold_products` AS `seller_popularity`,
 sum(`count`) as `product_popularity`

from `products`
left join `categories` on `products`.`category_id` = `categories`.`id`
left join `users` on `products`.`user_id` = `users`.`id`
left join `view_seller_popularity` on `products`.`user_id` = `view_seller_popularity`.`user_id`
left join `product_popularity` on `products`.`id` = `product_popularity`.`product_id`
where `products`.`is_visible` = 1
group by `products`.`id`;