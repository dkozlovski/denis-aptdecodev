CREATE  TABLE `user_product_view_history` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `user_id` INT UNSIGNED NOT NULL ,
  `product_id` INT UNSIGNED NOT NULL ,
  `first_view` INT NOT NULL ,
  `last_view` INT NOT NULL ,
  `view_count` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_USER_PRODUCT_VIEW_HISTORY_TO_USER_idx` (`user_id` ASC) ,
  INDEX `FK_USER_PRODUCT_VIEW_HISTORY_TO_PRODUCT_idx` (`product_id` ASC) ,
  UNIQUE INDEX `user_id_UNIQUE` (`user_id`, `product_id` ASC) ,
  CONSTRAINT `FK_USER_PRODUCT_VIEW_HISTORY_TO_USER`
    FOREIGN KEY (`user_id` )
    REFERENCES `users` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_USER_PRODUCT_VIEW_HISTORY_TO_PRODUCT`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);