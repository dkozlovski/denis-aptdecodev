ALTER TABLE `conditions` CHANGE `title` `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;

INSERT INTO `conditions`(`code`, `title`, `short_title`) VALUES ("like-new", "Like New: Product has been unwrapped from box but looks new & doesn't have any scratches, blemishes, etc.", "Like New");


SELECT
`products`.`id` as `id`,
`products`.`title` as `title`,
`products`.`description` as `description`,
`products`.`price` as `price`,
`products`.`color_id` as `color_id`,
`products`.`manufacturer_id` as `manufacturer_id`,
`products`.`category_id` as `category_id`,
`products`.`condition` as `condition`    ,  
`products`.`created_at` as `created_at`,
`products`.`updated_at` as `updated_at`,
`categories`.`title` as `category_title`,
`conditions`.`short_title` as `condition_title`,
`manufacturers`.`title` as `manufacturer_title`,
`colors`.`title` as `color_title`,
`materials`.`title` as `material_title`,
`users`.`rating` as `seller_rating`

FROM `products`
LEFT JOIN `categories` ON `products`.`category_id` = `categories`.`id`
LEFT JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
LEFT JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
LEFT JOIN `colors` ON `products`.`color_id` = `colors`.`id`
LEFT JOIN `materials` ON `products`.`material_id` = `materials`.`id`
LEFT JOIN `users` ON `products`.`user_id` = `users`.`id`

WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1
ORDER BY `id` ASC;
