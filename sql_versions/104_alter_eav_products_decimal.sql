CREATE TABLE IF NOT EXISTS `eav_products_decimal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  `value` decimal(12,4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entity_id_UNIQUE` (`entity_id`,`attribute_id`),
  KEY `FK_EAV_PRODUCTS_DECIMAL_TO_PRODUCTS_idx` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;

ALTER TABLE `eav_products_decimal`
  ADD CONSTRAINT `FK_EAV_PRODUCTS_DECIMAL_TO_PRODUCTS` FOREIGN KEY (`entity_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;