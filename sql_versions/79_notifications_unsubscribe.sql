CREATE TABLE `notifications_unsubscribe` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL,
  `is_user_confirm` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `FK_NOTIFICATIONS_UNSUBSCRIBE_idx` (`user_id`),
  CONSTRAINT `FK_NOTIFICATIONS_UNSUBSCRIBE` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)