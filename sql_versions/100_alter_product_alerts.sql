ALTER TABLE  `product_alerts` CHANGE  `category_id`  `category_id` INT( 10 ) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE  `product_alerts` ADD INDEX (  `category_id` );
ALTER TABLE  `product_alerts` ADD INDEX (  `collection_id` );
ALTER TABLE  `product_alerts` ADD FOREIGN KEY (  `category_id` ) REFERENCES  `categories` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;
