ALTER TABLE `products`
CHANGE COLUMN `pickup_postcode` `pickup_postcode` VARCHAR(255) NULL  ,
CHANGE COLUMN `is_verified` `is_verified` SMALLINT(5) UNSIGNED NULL  ,
ADD COLUMN `is_manage_price_allowed` SMALLINT UNSIGNED NULL DEFAULT NULL  AFTER `is_under_15_pounds` ,
ADD COLUMN `admin_edited_price` SMALLINT UNSIGNED NULL DEFAULT NULL  AFTER `is_manage_price_allowed` ;
