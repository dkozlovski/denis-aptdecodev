CREATE  TABLE `collections` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) NOT NULL ,
  `description` VARCHAR(255) NULL ,
  `image_path` VARCHAR(255) NULL ,
  `page_url` VARCHAR(255) NOT NULL ,
  `is_active` SMALLINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (`id`) );
  
  CREATE  TABLE .`collections_products` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `collection_id` INT UNSIGNED NOT NULL ,
  `product_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_CP_TO_COLLECTIONS_idx` (`collection_id` ASC) ,
  INDEX `FK_CP_TO_PRODUCTS_idx` (`product_id` ASC) ,
  CONSTRAINT `FK_CP_TO_COLLECTIONS`
    FOREIGN KEY (`collection_id` )
    REFERENCES `collections` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_CP_TO_PRODUCTS`
    FOREIGN KEY (`product_id` )
    REFERENCES `products` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);
    
ALTER TABLE `collections_products` 
ADD UNIQUE INDEX `collection_id_UNIQUE` (`collection_id` ASC, `product_id` ASC) ;
    
