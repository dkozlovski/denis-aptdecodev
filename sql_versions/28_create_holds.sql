CREATE  TABLE IF NOT EXISTS `holds` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `order_id` INT UNSIGNED NOT NULL ,
  `item_id` INT UNSIGNED NULL DEFAULT NULL ,
  `type` ENUM('item','shipping') NOT NULL ,
  `amount` DECIMAL(12,5) UNSIGNED NOT NULL DEFAULT 0 ,
  `captured_amount` DECIMAL(12,5) UNSIGNED NOT NULL DEFAULT 0 ,
  `status` ENUM('pending','voided','captured') NOT NULL ,
  `created_at` INT UNSIGNED NOT NULL ,
  `updated_at` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_HOLDS_TO_ORDER_ITEMS_idx` (`item_id` ASC) ,
  INDEX `FK_HOLDS_TO_ORDERS_idx` (`order_id` ASC) ,
  CONSTRAINT `FK_HOLDS_TO_ORDERS`
    FOREIGN KEY (`order_id` )
    REFERENCES `orders` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_HOLDS_TO_ORDER_ITEMS`
    FOREIGN KEY (`item_id` )
    REFERENCES `order_items` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;