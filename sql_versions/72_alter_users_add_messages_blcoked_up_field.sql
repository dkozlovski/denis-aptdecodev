ALTER TABLE `users`
ADD COLUMN `messages_blocked_up` INT UNSIGNED NULL AFTER `is_user_deleted`;

ALTER TABLE `conversation_messages`
ADD COLUMN `is_spam` SMALLINT UNSIGNED NOT NULL DEFAULT 0 AFTER `created_at`;

DROP VIEW `view_inbox_messages`;
CREATE
     OR REPLACE ALGORITHM = UNDEFINED
    SQL SECURITY DEFINER
VIEW `view_inbox_messages` AS
    select
        `conversation_messages`.`id` AS `id`,
        `conversation_messages`.`conversation_id` AS `conversation_id`,
        `conversation_messages`.`author_id` AS `author_id`,
        `conversation_messages`.`text` AS `text`,
        `conversation_messages`.`created_at` AS `created_at`,
        `conversation_messages_users`.`message_id` AS `message_id`,
        `conversation_messages_users`.`user_id` AS `user_id`,
        `conversation_messages_users`.`is_new` AS `is_new`,
        `conversation_messages_users`.`is_visible` AS `is_visible`
    from
        (`conversation_messages`
        join `conversation_messages_users` ON ((`conversation_messages`.`id` = `conversation_messages_users`.`message_id`)))
    where
        ((`conversation_messages_users`.`is_visible` = 1)
            and (`conversation_messages`.`author_id` <> `conversation_messages_users`.`user_id`))
and `is_spam` = 0
    order by `conversation_messages`.`created_at` desc;


 DROP VIEW `view_outbox_messages`;
    CREATE
     OR REPLACE ALGORITHM = UNDEFINED
    SQL SECURITY DEFINER
VIEW `view_outbox_messages` AS
    select
        `conversation_messages`.`id` AS `id`,
        `conversation_messages`.`conversation_id` AS `conversation_id`,
        `conversation_messages`.`author_id` AS `author_id`,
        `conversation_messages`.`text` AS `text`,
        `conversation_messages`.`created_at` AS `created_at`,
        `conversation_messages_users`.`message_id` AS `message_id`,
        `conversation_messages_users`.`user_id` AS `user_id`,
        `conversation_messages_users`.`is_new` AS `is_new`,
        `conversation_messages_users`.`is_visible` AS `is_visible`
    from
        (`conversation_messages`
        join `conversation_messages_users` ON ((`conversation_messages`.`id` = `conversation_messages_users`.`message_id`)))
    where
        ((`conversation_messages_users`.`is_visible` = 1)
            and (`conversation_messages`.`author_id` = `conversation_messages_users`.`user_id`))
and `is_spam` = 0
    order by `conversation_messages`.`created_at` desc;
