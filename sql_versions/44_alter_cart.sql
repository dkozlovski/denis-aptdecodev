CREATE TABLE IF NOT EXISTS `shipping_methods` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `shipping_methods` (`id`, `title`) VALUES
('delivery', 'Delivery by Aptdeco partner'),
('pickup', 'Pick-up');


DROP TABLE IF EXISTS `cart_items`;
DROP TABLE IF EXISTS `carts`;

CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `coupon_id` varchar(255) DEFAULT NULL,
  `billing_address_id` int(10) unsigned DEFAULT NULL,
  `shipping_address_id` int(10) unsigned DEFAULT NULL,
  `free_shipping` smallint(5) unsigned NOT NULL DEFAULT '0',
  `subtotal_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `shipping_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `discount_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `transaction_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `total_amount` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `is_notified` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cart_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `shipping_method_id` varchar(255) DEFAULT NULL,
  `product_price` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `qty` smallint(5) unsigned NOT NULL DEFAULT '0',
  `row_total` decimal(12,5) unsigned NOT NULL DEFAULT '0.00000',
  `options` text,
  `created_at` int(10) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(10) unsigned NOT NULL DEFAULT '0',
  `is_active` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_CART_ITEMS_CARTS` (`cart_id`),
  KEY `FK_CART_ITEMS_TO_SHIPPING_METHODS` (`shipping_method_id`),
  KEY `FK_CART_ITEMS_TO_PRODUCTS_idx` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;