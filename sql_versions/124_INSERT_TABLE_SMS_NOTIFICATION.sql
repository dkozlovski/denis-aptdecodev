INSERT INTO `sms_notification` (`type`,`description`,`text`) values
('seller_user_checkout','SMS notification: The seller receives a sms when the user draws a deal',''),
('user_seller_canceled','SMS notification: The user receives a sms when the seller cancels the transaction',''),
('user_seller_approved','SMS notification: The user receives a sms when the seller confirms the deal','')
