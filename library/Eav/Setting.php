<?php

class Eav_Setting extends Eav_Eav
{
    protected $_entityFieldId      = 'entity_id'; // имя первичного ключа в таблице settings_entity
    protected $_attributeTableName = 'settings_attribute'; // имя таблицы опций(атрибутов)
    protected $_attributeFieldId   = 'attribute_id'; // имя первичного ключа в таблице опций
    protected $_attributeFieldType = 'attribute_type'; // имя для поля "type" в таблице опций
    protected $_attributeFieldName = 'attribute_name'; // имя для поля "name" в таблице опций

    public function getAttributeTableName()
    {
        return $this->_attributeTableName;
    }
}
