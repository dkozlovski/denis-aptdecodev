<?php

namespace RESTful2;

class SortExpression
{
    public $name,
        $ascending;

    public function __construct($field, $ascending = true)
    {
        $this->field = $field;
        $this->ascending = $ascending;
    }
}
