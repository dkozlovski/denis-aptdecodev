<?php

namespace RESTful2\Exceptions;

/**
 * Indicates that a query unexpectedly returned no results.
 */
class NoResultFound extends Base
{
}
