<?php

namespace RESTful2\Exceptions;

/**
 * Indicates that a query unexpectedly returned multiple results when at most
 * one was expected.
 */
class MultipleResultsFound extends Base
{
}
