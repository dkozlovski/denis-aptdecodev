<?php

namespace RESTful2\Exceptions;

/**
 * Base class for all RESTful2\Exceptions.
 */
class Base extends \Exception
{
}
