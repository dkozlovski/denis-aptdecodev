<?php

namespace RESTful2;

class Fields
{
    public function __get($name)
    {
        return new Field($name);
    }
}
