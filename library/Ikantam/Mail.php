<?php

class Ikantam_Mail extends Zend_Mail
{

    protected $_allowedEmails = array(
        'v.o.v.a@tut.by',
        'vladimir.haidysh@gmail.com',
        'rehamf@gmail.com',
        'stephylane57@gmail.com',
        'dev.artem.hawk@gmail.com',
        'lostcharacter@mail.ru',
        'amy@aptdeco.com'
    );
    protected $_sendCopy      = array(
        'active' => false,
        'emails' => array('v.o.v.a@tut.by', 'dev.artem.hawk@gmail.com'/* , 'support@aptdeco.com' */),
    );

    public function __construct($data = array(), $charset = 'utf-8', $from = 'hello@aptdeco.com')
    {
        parent::__construct($charset);

        $this->setFrom($from, 'AptDeco');
        $this->setSubject($data['subject']);
        $this->addTo($data['email']);
        $this->setBodyHtml($data['body']);
    }

    public function send($transport = null, $useTransport = false)
    {
        $recipients = $this->getRecipients();

        if (APPLICATION_ENV === 'live' || (count($recipients) == 1 && in_array(current($recipients), $this->_allowedEmails))) {
            if ($useTransport) {
                $transport = $this->_getTransport();

                try {
                    parent::send($transport);
                } catch (Exception $exc) {
                    //@TODO: log exception
                    parent::send(null);
                }
            } else {
                parent::send($transport);
            }
        }
    }

    /**
     * Use mandrill.com SMTP to send an email
     * 
     * @return \Zend_Mail_Transport_Smtp
     */
    protected function _getTransport()
    {
        $host     = 'smtp.mandrillapp.com';
        $port     = 587;
        $username = 'vladimir.haidysh@gmail.com';
        $password = 'FE1QqPY2NH4YbBMuNilzmA';

        $config = array(
            'auth'     => 'login',
            'username' => $username,
            'password' => $password,
            'port'     => $port,
            'ssl'      => 'tls',
        );

        return new Zend_Mail_Transport_Smtp($host, $config);
    }

    /** Enable send copies
     * @param array $emails 
     */
    public function sendCopy($body)
    {
        foreach ($this->_sendCopy['emails'] as $email) {

            $toSend = array(
                'email'   => $email,
                'subject' => 'Copy of email sent to ' . implode(', ', $this->getRecipients()) . '. ' . $this->getSubject(),
                'body'    => $body
            );

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            $mail->send();
        }
    }

    /** Disable send copies
     */
    public function disableSendCopy()
    {
        $this->_sendCopy['active'] = false;
    }

}