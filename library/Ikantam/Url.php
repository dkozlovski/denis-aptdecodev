<?php

class Ikantam_Url
{

    protected static $_view;
    protected static $_version;

    private static function getView()
    {
        if (!self::$_view) {
            self::$_view = new Zend_View();
        }
        return self::$_view;
    }

    public static function getUrl($path, $params = array(), $reset = true, $configPath = true)
    {
        $path = array_reverse(explode('/', trim($path, '/')));

        $params['action']     = !empty($path[0]) ? $path[0] : null;
        $params['controller'] = !empty($path[1]) ? $path[1] : null;
        $params['module']     = !empty($path[2]) ? $path[2] : null;

        $baseUrl = self::getBaseUrl(self::getView(), $configPath);
        //$baseUrl = 'http://localhost';

        return $baseUrl . self::getView()->url($params, $route = 'default', $reset);
    }

    public static function getPublicUrl($path, $configPath = false)
    {
        if (APPLICATION_ENV !== 'live' && APPLICATION_ENV !== 'testing') {
            return self::_getLocalPublicUrl($path, $configPath);
        }
        
        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->static_content_url;
        
        if (Zend_Registry::isRegistered('config')) {
            $config = Zend_Registry::get('config');
            if (isset($config->assets->version) && $config->assets->version) {
                self::$_version = '/static/v/' . $config->assets->version . '/';
            }
        }
        
        return $cloudFrontUrl . self::$_version . $path;
    }
    
    protected static function _getLocalPublicUrl($path, $configPath)
    {
        $baseUrl   = self::getBaseUrl(self::getView(), $configPath);
        $publicUrl = rtrim(Zend_Controller_Front::getInstance()->getBaseUrl(), '/') . '/';
        //$baseUrl = 'http://localhost';
        //$publicUrl = '/reham-aptdeco/public/';

        if (Zend_Registry::isRegistered('config')) {
            $config = Zend_Registry::get('config');
            if (isset($config->assets->version) && $config->assets->version) {
                self::$_version = 'static/v/' . $config->assets->version . '/';
            }
        }

        return $baseUrl . $publicUrl . self::$_version . $path;
    }

    public static function getStaticUrl($path)
    {
        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
        return $cloudFrontUrl . '/static/' . $path;

        //return self::getPublicUrl($path);
        //$baseUrl = 'https://s3-us-west-2.amazonaws.com/aptdeco/';
        //return $baseUrl . $path;
    }
    
    public static function getS3Url($path)
    {
        $cloudFrontUrl = 'https://s3-us-west-2.amazonaws.com/' . Zend_Registry::get('config')->amazon_s3->bucketName;
        return $cloudFrontUrl . '/static/' . $path;

        //return self::getPublicUrl($path);
        //$baseUrl = 'https://s3-us-west-2.amazonaws.com/aptdeco/';
        //return $baseUrl . $path;
    }

    public static function getUrlWithGaParams($url, $params = array())
    {
        // params description: https://support.google.com/analytics/answer/1033863?hl=en&ref_topic=1032998
        $defaultParams = array(
            'utm_source'   => 'email',
            'utm_medium'   => 'email',
            'utm_campaign' => 'email'
        );

        $params = array_merge($defaultParams, $params);
        $query  = http_build_query($params);
        if ($query) {
            $mark = (strpos($url, '?') === false) ? '?' : '&';
            $url .= $mark . $query;
        }
        return $url;
    }

    protected static function getBaseUrl($view, $configPath)
    {
        if (defined('BASE_ORDER_URL')) {
            return '';
        }

        $baseUrl = null;

        if ($configPath) {
            $baseUrl = Zend_Registry::get('config')->baseUrl;
        }

        return empty($baseUrl) ? $view->serverUrl() : $baseUrl;
    }

}