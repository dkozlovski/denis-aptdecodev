<?php

class Ikantam_Backend
{

	protected $_autoQuoteIdentifiers = true;
	protected $_orderField;
	

	public function delete($tableName, array $whereCondition)
	{
		$bind = array();
		$where = $this->_whereExpr($whereCondition, $bind);

		$sql = 'DELETE FROM ' . $this->quote($tableName)
				. ' WHERE ' . $where;

		$stmt = $this->query($sql, $bind);
		$result = $stmt->rowCount();
		return $result;
	}
	
	public function select($tableName, array $whereCondition)
	{
		$bind = array();
		$where = $this->_whereExpr($whereCondition, $bind);

		$sql = 'SELECT * FROM ' . $this->quote($tableName)
				. ' WHERE ' . $where;

		$stmt = $this->query($sql, $bind);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function selectC($tableName, $whereCondition, array $bind, $order, $limit)
	{
		//$bind = array();
		//$where = $this->_whereExpr($whereCondition, $bind);

		$sql = 'SELECT * FROM ' . $this->quote($tableName)
				. ' WHERE ' . implode(' AND ', $whereCondition);
		
		if ($order) {
			$sql .= $order;
		}
		
		if ($limit) {
			$sql .= $limit;
		}

		$stmt = $this->query($sql, $bind);
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function update($tableName, array $bind, array $whereCondition)
	{
		$set = array();

		foreach ($bind as $field => $value) {
			unset($bind[$field]);
			$bind[':' . $field] = $value;
			$set[] = $this->quote($field) . ' = ' . ':' . $field;
		}

		$where = $this->_whereExpr($whereCondition, $bind);

		$sql = 'UPDATE ' . $this->quote($tableName)
				. ' SET ' . implode(', ', $set)
				. ' WHERE ' . $where;

		$stmt = $this->query($sql, $bind);
		$result = $stmt->rowCount();
		return $result;
	}

	public function insert($tableName, array $bind)
	{
		$cols = array();
		$vals = array();

		foreach ($bind as $field => $value) {
			$cols[] = $this->quote($field, true);
			unset($bind[$field]);
			$bind[':' . $field] = $value;
			$vals[] = ':' . $field;
		}

		$sql = 'INSERT INTO ' . $this->quote($tableName)
				. ' (' . implode(', ', $cols) . ') '
				. 'VALUES (' . implode(', ', $vals) . ')';
try {
		$stmt = $this->query($sql, $bind);
		$result = $stmt->rowCount();
		return $result;
		} catch (PDOException $e) {
		return 0;
		}
	}

	public function query($sql, $bind)
	{
		$stmt = $this->getConnection()->prepare($sql);
		$stmt->execute($bind);
		return $stmt;
	}
	
	protected function getConnection()
	{
		$s = Application_Model_DbFactory::getFactory()->getConnection();
		return $s;
	}

	protected function _whereExpr($where, &$bind)
	{
		$wh = array('1 = 1');

		foreach ($where as $field => $value) {
			$bind[':where_' . $field] = $value;
			$wh[] = $this->quote($field) . ' = ' . ':where_' . $field;
		}

		return implode(' AND ', $wh);
	}

	public function quote($value)
	{
		$q = $this->getQuoteIdentifierSymbol();
		return ($q . str_replace("$q", "$q$q", $value) . $q);
	}

	public function getQuoteIdentifierSymbol()
	{
		return '`';
	}

}
