<?php

/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Filter
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: UpperCase.php 24593 2012-01-05 20:35:02Z matthew $
 */

/**
 * @category   Zend
 * @package    Zend_Filter
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Ikantam_ConvertImage implements Zend_Filter_Interface//Zend_Filter_File_UpperCase extends Zend_Filter_StringToUpper
{

    protected $_options = array();

    /**
     * Adds options to the filter at initiation
     *
     * @param string $options
     */
    public function __construct($options = null)
    {
        if (!empty($options)) {
            //$this->setEncoding($options);
            $this->_options = $options;
        }
    }

    /**
     * Defined by Zend_Filter_Interface
     *
     * Does a lowercase on the content of the given file
     *
     * @param  string $value Full path of file to change
     * @return string The given $value
     * @throws Zend_Filter_Exception
     */
    public function filter($value)
    {
        if (!file_exists($value)) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$value' not found");
        }

        if (!is_writable($value)) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$value' is not writable");
        }

        $filePath = $value;
        $fileName = basename($filePath);

        switch (strtolower(substr(strrchr($fileName, '.'), 1))) {
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'png':
            case false:
                if($srcImg = @imagecreatefromjpeg($filePath)){
                   break; 
                }
                if($srcImg = @imagecreatefromgif($filePath)){
                    break;
                }
                if($srcImg = @imagecreatefrompng($filePath)){
                    break;
                }             
            default:
                $srcImg = null;
        }
        
        if (!$srcImg) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$value' not found");
        }

        $imgWidth = imagesx($srcImg);
        $imgHeight = imagesy($srcImg);

        if (!$imgWidth || !$imgHeight) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$value' not found");
        }

        $newImg = @imagecreatetruecolor($imgWidth, $imgHeight);
        
        imagefill($newImg, 0, 0, imagecolorallocate($newImg, 255, 255, 255));
        imagealphablending($newImg, true);
       
        @imagecopy($newImg, $srcImg, 0, 0, 0, 0, $imgWidth, $imgHeight);
        imagejpeg($newImg, $this->_options['file_path'] . '/original-0.jpg', $this->_options['jpeg_quality']);
        
        // Free up memory (imagedestroy does not delete files):
        @imagedestroy($newImg);
        @imagedestroy($srcImg);

        unlink($filePath);
        
        return true;
    }

}
