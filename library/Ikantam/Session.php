<?php

class Ikantam_Session
{

	protected $_data;
	protected $_formData = array();
	protected $_formErrors = array();
	protected $_messages = array();
	
	public function __call($method, $args)
	{
	    $key = substr($method, 3);
        
		switch (substr($method, 0, 3)) {
			case 'get' :				
				$data = $this->_data->$key;
				return $data;
                break;
			case 'set' :
				$this->_data->$key = isset($args[0]) ? $args[0] : null;
				return $this;
                break;
            case 'uns' :
                if(isset($this->_data->$key)) unset($this->_data->$key);
                return $this;
                break;
            case 'has' :
                return isset($this->_data->{$key});
            break;    
		}
		throw new Exception("Invalid method " . get_class($this) . "::" . $method . "(" . print_r($args, 1) . ")");
	}

	public function setFlashData($key, $value)
	{
		$this->_data->$key = $value;
		$this->_data->setExpirationHops(1, $key);

		return $this;
	}

	public function getFlashData($key)
	{
		return $this->_data->$key;
	}

	public function __construct()
	{
		$this->_data = new Zend_Session_Namespace();
	}

	public function addFormData(array $data2, $includeFields = null, $excludeFields = null)
	{
		$data = $this->addFormData2($data2, $includeFields, $excludeFields);

		foreach ($data as $field => $value) {
			$this->setFormData($field, $value);
		}
		return $this;
	}

	public function addFormData2($data, $includeFields, $excludeFields)
	{
		if ($includeFields !== null) {
			$fields = array();
			if (is_array($includeFields)) {
				foreach ($includeFields as $includeField) {
					if (isset($data[$includeField])) {
						$fields[$includeField] = $data[$includeField];
					}
				}
			} else {
				if (isset($data[$includeFields])) {
					$fields[$includeFields] = $data[$includeFields];
				}
			}
			return $fields;
		} elseif ($excludeFields !== null) {
			if (is_array($excludeFields)) {
				foreach ($excludeFields as $excludeField) {
					unset($data[$excludeField]);
				}
			} else {
				unset($data[$excludeFields]);
			}
			return $data;
		}
		return $data;
	}

	public function setFormData($field, $value = null)
	{
		if (is_array($field)) {
			$this->_formData = $field;
		} else {
			$this->_formData[$field] = $value;
		}
		$this->setFlashData('form_data', $this->_formData);
		return $this;
	}

	public function getFormData($field = null)
	{
		$this->_formData = (array) $this->getFlashData('form_data');

		if ($field === null) {
			return $this->_formData;
		}

		return isset($this->_formData[$field]) ? $this->_formData[$field] : null;
	}

	public function addFormErrors(array $errors)
	{
		foreach ($errors as $field => $error) {
			$this->setFormErrors($field, $error);
		}
		return $this;
	}

	public function setFormErrors($field, $error = null)
	{
		if (is_array($field)) {
			$this->_formErrors = $field;
		} else {
			$this->_formErrors[$field] = $error;
		}
		$this->setFlashData('form_erros', $this->_formErrors);
		return $this;
	}

	public function getFormErrors($field = null)
	{
		$this->_formErrors = (array) $this->getFlashData('form_erros');

		if ($field === null) {
			return $this->_formErrors;
		}

		return isset($this->_formErrors[$field]) ? $this->_formErrors[$field] : null;
	}

	public function addMessage($type, $message)
	{
		$this->_messages[$type][] = $message;
		$this->setFlashData('messages', $this->_messages);
		return $this;
	}

	public function getMessages($type = null)
	{
		$this->_messages = (array) $this->getFlashData('messages');

		if ($type === null) {
			return $this->_messages;
		}

		return isset($this->_messages[$type]) ? $this->_messages[$type] : array();
	}

    /**
     * Works very similar like flashData but has the same lifetime as a session
     * Method might be used as getter or setter depends on parameters number
     *
     * @param $key
     * @param null $data
     * @param bool $holdData - if need to continue keep data after fetching
     * @return null
     */
    public function activeData ($key, $data = null, $holdData = false)
    {
        $key = md5('__'.$key);
        if(!is_null($data)) {
          $this->_data->{$key} = $data;
          return $data;  
        } elseif($this->{'has'.$key}())  {
            $val = $this->{'get'.$key}();
            if(!$holdData) {
               $this->{'uns'.$key}(); 
            }            
            return $val;
        }
        
        return null;    
    }
    
    public function isDefined($name)
    {
        return isset($this->_data->{$name});
    }

}
