<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 03.07.14
 * Time: 0:09
 */

interface Ikantam_EAV_Interface_Attribute
{
    /**
     * Gets attribute type
     * @return string
     */
    public function getType();

    /**
     * Sets attribute name
     * @param string $name
     * @return mixed
     */
    public function setName($name);

    /**
     * Gets attribute name
     * @return string
     */
    public function getName();

    /**
     * Retrieve attribute unique identifier
     * @return mixed
     */
     public function getAttributeId();
} 