<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 03.07.14
 * Time: 0:41
 */

interface Ikantam_EAV_Interface_DB
{
    /**
     * Saves value
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @param $value
     * @return mixed
     */
    public function saveValue(
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute,
        $value
    );

    /**
     * Retrieves value
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @return mixed
     */
    public function getValue(
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute
    );

    /**
     * Delete value
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @return mixed
     */
    public function deleteValue(
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute
    );
} 