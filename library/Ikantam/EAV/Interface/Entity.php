<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 03.07.14
 * Time: 0:04
 */

interface Ikantam_EAV_Interface_Entity
{
    /**
     * Return unique identifier for entity
     * @return mixed
     */
    public function getEntityId();

    /**
     * Return entity family (common for group)
     * @return string
     */
    public function getEntityFamily();
} 