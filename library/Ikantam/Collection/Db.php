<?php

class Ikantam_Collection_Db extends Ikantam_Collection
{

	const SORT_ORDER_ASC = 'ASC';
	const SORT_ORDER_DESC = 'DESC';

	protected $_filters = array();
	protected $_orderBy = array();
	protected $_currentPage = 1;
	protected $_totalPages = 1;
	protected $_offset = 0;
	protected $_limit = 10;
	protected $_itemCountPerPage = 10;

	public function addFilter($field, $values)
	{
		$this->_filters[$field] = $values;
		return $this;
	}

	public function getFilters()
	{
		return $this->_filters;
	}

	public function setCurrentPage($page)
	{
		$this->_currentPage = (int) $page;
	}

	protected function getCurrentPage()
	{
		if ($this->_currentPage < 1) {
			return 1;
		}

		if ($this->_currentPage > $this->getTotalPages()) {
			return $this->_totalPages;
		}

		return $this->_currentPage;
	}

	/**
	 * Set sorting order
	 *
	 * @param string|array $attribute
	 * @param string $direction
	 * @return Ikantam_Collection_Db
	 */
	public function setOrderBy($attribute, $direction = self::SORT_ORDER_ASC)
	{
		if (is_array($attribute)) {
			foreach ($attribute as $attr) {
				$this->_setOrderBy($attr, $direction);
			}
		} else {
			$this->_setOrderBy($attribute, $direction);
		}

		return $this;
	}

	/**
	 * Add ORDER BY
	 *
	 * @param string $field
	 * @param string $direction
	 * @return Ikantam_Collection_Db
	 */
	protected function _setOrderBy($field, $direction)
	{
		$direction = (strtoupper($direction) == self::SORT_ORDER_ASC) ? self::SORT_ORDER_ASC : self::SORT_ORDER_DESC;

		$this->_orderBy[$field] = $direction;

		return $this;
	}

}