<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 02.07.14
 * Time: 23:37
 */


class Ikantam_EAV
{
    /**
     * @var Ikantam_EAV_Interface_Entity
     */
    protected $entity;

    /**
     * @var Ikantam_EAV_Interface_Attribute
     */
    protected $attribute;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var Ikantam_EAV_Interface_DB
     */
    protected $db;

    /**
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @param Ikantam_EAV_Interface_DB
     */
    public function __construct(
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute,
        \Ikantam_EAV_Interface_DB $db
    ) {
        $this->entity = $entity;
        $this->attribute = $attribute;
        $this->db = $db;
    }

    /**
     * Saves value for entity attribute
     * @param $value
     * @return Ikantam_EAV
     */
     public function saveValue($value)
     {
         $this->db->saveValue($this->entity, $this->attribute, $value);
         return $this;
     }

    /**
     * Retrieves value
     * @return mixed
     */
    public function getValue()
    {
        return $this->db->getValue($this->entity, $this->attribute);
    }

    /**
     * Delete value
     * @return mixed
     */
    public function deleteValue()
    {
        return $this->db->deleteValue($this->entity, $this->attribute);
    }

} 