<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 03.07.14
 * Time: 20:53
 */

interface Ikantam_Filter_Flex_Interface_EAV
{
    /**
     * Get name of the attributes table
     * @return string
     */
    public function getAttributesTable();

    /**
     * Get map for tables where values are stored
     * @return array
     * structure:
     * 'INT' => 'eav_table_int',
     * 'TEXT' => 'eav_table_text'
     * 'X-TYPE' => 'x-table_name'...
     */
    public function getTypeTableMap();

    /**
     * Get entity table name
     * @return array
     */
    public function getEntityTable();

} 