<?php
    /**
    * Interface for Ikantam_Filter_Flex 
    */
    
interface Ikantam_Filter_Flex_Interface {
    /**
    * Returns a filter related with collection.  
    * @return object Ikantam_Filter_Flex  
    */
    public function  getFlexFilter ();

}