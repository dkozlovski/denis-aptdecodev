<?php

/**
 * @see Zend_Filter_Interface
 */
require_once 'Zend/Filter/Interface.php';

class Ikantam_Filter_Array implements Zend_Filter_Interface
{
    /**
     * Defined by Zend_Filter_Interface
     *
     * Returns (float) $value
     *
     * @param  string $value
     * @return float
     */
    public function filter($value)
    {
        return (array) $value;
    }
}
