<?php

class Ikantam_Filter_SphinxSearch
{
	protected $_sphinx;
	
	public function __construct()
	{
		$this->_sphinx = new Ikantam_Sphinx();
	}

	public function addFilter($field, $value)
	{
		$this->_sphinx->SetFilter($field, (array) $value);
	}

	public function addFilterRange($field, $minValue, $maxValue)
	{
		//if (is_float($minValue) || is_float($maxValue)) {
			$this->_sphinx->SetFilterFloatRange($field, (float) $minValue, (float) $maxValue);
		//}
		//$this->_sphinx->SetFilterRange($field, $minValue, $maxValue);
	}

	public function setLimit($offset, $limit)
	{
		$offset = (int) $offset;
		$limit = (int) $limit;
		
		if ($offset < 0) {
			$offset = 0;
		}
		
		if ($limit <= 0) {
			$limit = 1;
		}

		$this->_sphinx->SetLimits($offset, $limit);
	}

	public function setSort($field, $mode)
	{
		
	}

	public function load2($collection)
	{
		$search = $this->_sphinx->Query('', 'category_filter_delta category_filter_main');
		
		if ($search !== false && isset($search['matches'])) {
			$ids = array_keys($search['matches']);
			$this->_getBackend($collection)->getBySphinx($collection, implode(', ', $ids));
		} else {
			var_Dump($this->_sphinx->GetLastError());
		}
		if ($this->_sphinx->GetLastWarning()) {
			var_dump($this->_sphinx->GetLastWarning());
		}
	}
	
	protected function _getBackend($collection)
	{
		$backendClass = str_replace('_Collection', '_Backend', get_class($collection));
		return new $backendClass();
	}
}