<?php

class Ikantam_Filter_Ascii implements Zend_Filter_Interface
{
    public function filter($value)
    {
        //leave only printable ascii characters
        return preg_replace('/[^\x20-\x7E]/', '', $value);
    }
}
