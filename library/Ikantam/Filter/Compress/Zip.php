<?php
class Ikantam_Filter_Compress_Zip extends Zend_Filter_Compress_Zip
{
    /**
    * Return content string from archived file
    *
    * @return string 
    */
    public function getTargetContent()
    {
        $archive = $this->getArchive();
        
        $zip = new ZipArchive();
        $res = $zip->open($archive);

        $target = $this->getTarget(); 
        $result = '';
     
        if (($res = $zip->open($archive)) === true) {
            $result = $zip->getFromName($target);
            $zip->close();
        } else {
            throw new Zend_Filter_Exception($this->_errorString($res));
        }
        
        return $result;              
    }    
}