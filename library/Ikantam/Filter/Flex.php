<?php

class Ikantam_Filter_Flex extends Ikantam_Filter_Flex_Abstract
{

    /**
     * Holds used attributes
     * @var array
     */
    protected $usedEAVAttributes = array();

    /**
     * Create filter query via Zend Request object
     * @param  \Zend_Controller_Request_Http $request
     * @param  array $map - rules to explain the filter how to handle the request
     * @throws Exception
     * @return object self
     * $map structure:
     * array(
     *   'request_param_name' => array ('filter_method_name' => 'operator', ['and']),
     *   'request_param_name2' => array ('filter_method_name2' => function($request_value){...}) | must return array contains parameters
     * )
     */
    public function handleRequest(\Zend_Controller_Request_Http $request, array $map = array())
    {
        foreach ($map as $param => $rule) {
            if (($rp = $request->getParam($param, '*__undefined__*')) == '*__undefined__*') {
                continue;
            }
            if (is_array($rule)) {
                $aux = array_keys($rule);
                reset($rule);
                $def = $rule[key($rule)];
                if ($def instanceof \Closure) {
                    $result = call_user_func($def, $rp);
                    call_user_func_array(array($this, array_shift($aux)), $result);
                    continue;
                }
                $rule   = (array) $rule;
                $rule[] = $rp;
                $method = array_shift($aux);
                if (!is_string($method)) {
                    throw new Exception('Invalid map structure.');
                }
                call_user_func_array(array($this, $method), $rule);
            }
        }

        return $this;
    }

    /**
     * Add filtering condition by EAV attribute
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @param string $operator
     * @param mixed $value
     * @return $this
     * @throws Ikantam_Filter_Flex_Interface_Exception
     */
    public function byEAVAttribute(\Ikantam_EAV_Interface_Attribute $attribute, $operator, $value = null)
    {
        if (!$this->getModel() instanceof \Ikantam_Filter_Flex_Interface_EAV) {
            throw new \Ikantam_Filter_Flex_Interface_Exception(
                'To use filtering by EAV attribute model should implement Ikantam_Filter_Flex_Interface_EAV interface'
            );
        }

        $eavAlias = $this->createNewEAVRule($attribute);
        $eavAliasValue = $eavAlias . '_value';

        $eavFullField = $this->_checkField($eavAliasValue);
        $eavFullField = is_array($eavFullField) ? array_shift($eavFullField) : $eavFullField;
        $this->_select[$eavAlias] = $eavFullField. " AS `" . $attribute->getName() . "`";

        $this->{$eavAliasValue}($operator, $value);

        return $this;
    }

    public function clear()
    {
        $this->getModel()->clear();
        return parent::clear();
    }

    protected function createNewEAVRule(\Ikantam_EAV_Interface_Attribute $attribute)
    {
        $entityTable = $this->getModel()->getEntityTable();
        $typeMap = $this->getModel()->getTypeTableMap();
        $typeTable = $typeMap[$attribute->getType()];

        $alias = 'eav_' . strtolower($attribute->getType() . '_' .$attribute->getName());

        if (!in_array($attribute->getName(), $this->usedEAVAttributes)) {
            $this->_joins[$alias] = "LEFT JOIN `{$typeTable}` AS `{$alias}`
                                 ON `{$alias}`.`entity_id` = {$entityTable}.`id`
                                 AND `{$alias}`.`attribute_id` = " . $attribute->getAttributeId();

            $this->_rules[$alias . '_value'] = array($alias => "`{$alias}`.`value`");
            $this->_fields[] = $alias . '_value';
        }

        return $alias;
    }

    protected function _setModel($model)
    {
        if (is_array($model)) {
            $model = array_shift($model);
        }
        $given_class = get_class($model);
        if ($this->_acceptClass !== $given_class) {
            throw new Exception(' Classes missmatch. Only accept instance of "' . $this->_acceptClass .
            '" class, instance of "' . $given_class . '" class given.');
        }

        $this->_model = $model;
    }

    protected function _getDefaultFields()
    {
        $dfields = $this->getModel()->getDescribedFields();
        $fields  = array_map(function($item) {
                    return $item['Field'];
                }, $dfields);

        return $fields;
    }

    protected function _getTable()
    {
        return $this->getModel()->getTable();
    }

    protected function _query($sql, $binds)
    {
        return $this->getModel()->runQuery($sql, $binds);
    }

    protected function _count($sql, $binds)
    {
        $result = $this->getModel()->rawQuery($sql, $binds);
        $result = array_shift($result);
        return $result['total_rows'];
    }


}