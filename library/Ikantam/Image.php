<?php

class Ikantam_Image
{
    protected $_directoriesCount = 10000;
    
    public function _getRandomDirName()
    {
        return time() . md5(Ikantam_Math_Rand::getString(16, Ikantam_Math_Rand::ALPHA_NUMERIC));
    }

    public function rotateOriginalImage($sourcepath, $destPath, $angle)
    {
        $srcImg = imagecreatefromjpeg($sourcepath . '/original-0.jpg');

        $rotate = imagerotate($srcImg, 360 - $angle, 0);

        $bn = $destPath . '/original-' . $angle . '.jpg';

        imagejpeg($rotate, $bn, 75);
        imagedestroy($rotate);
        imagedestroy($srcImg);
    }
    
    protected function _getProductUploadDir($baseUploadPath, $productId)
    {
        $th = (int) (($productId - 1) / $this->_directoriesCount);
        $folder = ($th * $this->_directoriesCount + 1) . '-' . ($th * $this->_directoriesCount + $this->_directoriesCount);

        $firstLevelDir = $baseUploadPath . DIRECTORY_SEPARATOR . $folder;
        
        if (!file_exists($firstLevelDir)) {//one directory per X product IDs
            mkdir($firstLevelDir, 0777);
        }

        $productDir = $firstLevelDir . DIRECTORY_SEPARATOR . $productId;

        if (!file_exists($productDir)) {//one directory per product ID
            mkdir($productDir, 0777);
        }

        return $folder . DIRECTORY_SEPARATOR . $productId;
    }
    
    protected function _getAvatarUploadDir($baseUploadPath, $userId)
    {
        $th = (int) (($userId - 1) / $this->_directoriesCount);
        $folder = ($th * $this->_directoriesCount + 1) . '-' . ($th * $this->_directoriesCount + $this->_directoriesCount);

        $firstLevelDir = $baseUploadPath . DIRECTORY_SEPARATOR . $folder;
        
        if (!file_exists($firstLevelDir)) {//one directory per X product IDs
            mkdir($firstLevelDir, 0777);
        }

        $productDir = $firstLevelDir . DIRECTORY_SEPARATOR . $userId;

        if (!file_exists($productDir)) {//one directory per product ID
            mkdir($productDir, 0777);
        }

        return $folder . DIRECTORY_SEPARATOR . $userId;
    }
    
    public function getAvatarUploadDir($userId)
    {
        $th = (int) (($userId - 1) / $this->_directoriesCount);
        $folder = ($th * $this->_directoriesCount + 1) . '-' . ($th * $this->_directoriesCount + $this->_directoriesCount);

        return $folder . DIRECTORY_SEPARATOR . $userId;
    }
    
    protected function _getImageUploadDir($productUploadPath)
    {
        $imageKey = $this->_getRandomDirName();
        $baseUploadDir = $productUploadPath . DIRECTORY_SEPARATOR . $imageKey;
        if (!file_exists($baseUploadDir)) {
            mkdir($baseUploadDir, 0777);//one directory per product image
        }

        return $imageKey;
    }

    public function uploadImage($uploadPath, $productId)
    {
        $productPath = $this->_getProductUploadDir($uploadPath, $productId);
        $productDir = $uploadPath . DIRECTORY_SEPARATOR . $productPath;

        $imagePath = $this->_getImageUploadDir($productDir);
        $imageDir = $productDir . DIRECTORY_SEPARATOR . $imagePath;

        $params = array('file_path' => $imageDir, 'jpeg_quality' => 90);

        $adapter = new Zend_File_Transfer_Adapter_Http();

        $adapter->setDestination($productDir);
        $adapter->addValidator('Size', false, array('min' => '1kB', 'max' => '14MB'));
        //$adapter->addValidator('Extension', false, array('jpg', 'jpeg', 'png', 'gif'));
        //$adapter->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '14MB'));
        $adapter->addValidator('IsImage', false);
        $adapter->addFilter(new Ikantam_ConvertImage($params));

        if (!$adapter->receive()) {
            $messages = $adapter->getMessages();
            echo implode("\n", $messages);
            die();
        }

        return $productPath . DIRECTORY_SEPARATOR . $imagePath;
    }
    
    public function downloadAvatar($src, $userId)
    {
        $uploadPath = APPLICATION_PATH . '/../public/upload/avatars';
        
        $productPath = $this->_getProductUploadDir($uploadPath, $userId);

        $newImageName = $this->_getRandomDirName() . '.jpg';

        $this->uploadAvatarToS3($src, $productPath . '/' . $newImageName);
            
        return $productPath . '/' . $newImageName;
    }
    
    public function uploadAvatar($uploadPath, $userId)
    {
        $productPath = $this->_getProductUploadDir($uploadPath, $userId);
        $productDir = $uploadPath . DIRECTORY_SEPARATOR . $productPath;

        $newImageName = $this->_getRandomDirName() . '.jpg';
        $imageDir = $productDir . DIRECTORY_SEPARATOR . $newImageName;

        $params = array('file_path' => $imageDir, 'jpeg_quality' => 90);

        $adapter = new Zend_File_Transfer_Adapter_Http();

        $adapter->setDestination($productDir);
        $adapter->addValidator('Size', true, array('min' => '1kB', 'max' => '14MB'));
        $adapter->addValidator('Extension', true, array('jpg', 'jpeg', 'png', 'gif'));
        $adapter->addValidator('FilesSize', true, array('min' => '1kB', 'max' => '14MB'));
        $adapter->addValidator('IsImage', true);
        $adapter->addFilter(new Ikantam_ConvertAvatar($params));

        if (!$adapter->receive()) {
            $messages = $adapter->getMessages();
            throw new Exception(implode("\n", $messages));
            //echo implode("\n", $messages);
            //die();
        }

        $this->uploadAvatarToS3($imageDir, $productPath . '/' . $newImageName);
        unlink($imageDir);
            
            
        return $productPath . '/' . $newImageName;
    }

    public function createScaledImages($uploadDir, $imagePath, $productId, $angle = 0)
    {
        //$uploadDir = "/var/www/reham-aptdeco/application/../product-images"
        //$imagePath = "1-10000/162/136862760127e621f49657ab47d13fe5fe0c880587"
 
        $versions = array(
            array('width' => 1500, 'height' => 1500, 'keepFrame' => true),
            //array('width' => 1500, 'height' => 1500, 'keepFrame' => false),
            //array('width' => 500, 'height' => 500, 'keepFrame' => true),
            array('width' => 500, 'height' => 500, 'keepFrame' => false),
            //array('width' => 200, 'height' => 200, 'keepFrame' => true),
            array('width' => 200, 'height' => 200, 'keepFrame' => false),
            array('width' => 360, 'height' => 300, 'keepFrame' => true),
                //array('width' => 360, 'height' => 300, 'keepFrame' => false),
        );

        $fileName = 'original-' . $angle . '.jpg';
        $file_path = $uploadDir . DIRECTORY_SEPARATOR . $imagePath . DIRECTORY_SEPARATOR . $fileName;

        $srcImg = @imagecreatefromjpeg($file_path);

        $img_width = imagesx($srcImg);
        $img_height = imagesy($srcImg);

        if (!$img_width || !$img_height) {
            return false;
        }

        foreach ($versions as $version) {

            if ($version['keepFrame']) {
                $scale = min($version['width'] / $img_width, $version['height'] / $img_height);
            } else {
                $scale = max($version['width'] / $img_width, $version['height'] / $img_height);
            }

            $new_width = $version['width'];
            $new_height = $version['height'];

            $scaledWidth = $img_width * $scale;
            $scaledHeight = $img_height * $scale;

            $startX = (int) (($new_width - $scaledWidth) / 2);
            $startY = (int) (($new_height - $scaledHeight) / 2);

            $newImg = @imagecreatetruecolor($new_width, $new_height);
            $whiteColor = imagecolorallocate($newImg, 255, 255, 255);
            imagefill($newImg, 0, 0, $whiteColor);

            $newFileName = $version['width'] . '-' . $version['height'] . '-' . (($version['keepFrame']) ? 'frame' : 'crop') . '-' . $angle . '.jpg';

            $new_file_path = $uploadDir . DIRECTORY_SEPARATOR . $imagePath . DIRECTORY_SEPARATOR . $newFileName;

            if (!$srcImg) {
                //error
                die();
            }

            //@imagecopyresampled(
            @imagecopyresized(
                            $newImg, $srcImg, $startX, $startY, 0, 0, $scaledWidth, $scaledHeight, $img_width, $img_height);
            imagejpeg($newImg, $new_file_path, 75);
            @imagedestroy($newImg);

            $this->uploadToS3($new_file_path, $imagePath . '/' . $newFileName);
            unlink($new_file_path);
        }
        $ophath = $uploadDir . DIRECTORY_SEPARATOR . $imagePath . DIRECTORY_SEPARATOR . 'original-' . $angle . '.jpg';
        $impht = $imagePath . '/' . 'original-' . $angle . '.jpg';
        
        $this->uploadToS3($ophath, $impht);
        unlink($ophath);

        @imagedestroy($srcImg);
        //}
        //echo "\nscale finish: " . microtime(true);
    }

    protected function uploadToS3($srcFile, $dstPath)
    {
        $awsKey = Zend_Registry::get('config')->amazon_s3->awsKey;
        $secretKey = Zend_Registry::get('config')->amazon_s3->secretKey;
        $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;

        $s3 = new Zend_Service_Amazon_S3($awsKey, $secretKey);

        $meta = array(Zend_Service_Amazon_S3::S3_ACL_HEADER => Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ);

        $res = false;
        do {
            try {
                $res = $s3->putObject($bucketName . '/product-images/' . $dstPath, file_get_contents($srcFile), $meta);
                //var_Dump($res);
            } catch (Exception $e) {
                //var_dump($e->getMessage());
                //die();
                //@TODO: handle error
            }
        } while ($res !== true);
    }
    
    public function uploadAvatarToS3($srcFile, $dstPath)
    {
        $awsKey = Zend_Registry::get('config')->amazon_s3->awsKey;
        $secretKey = Zend_Registry::get('config')->amazon_s3->secretKey;
        $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;

        $s3 = new Zend_Service_Amazon_S3($awsKey, $secretKey);

        $meta = array(Zend_Service_Amazon_S3::S3_ACL_HEADER => Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ);

        $res = false;
        do {
            try {
                $res = $s3->putObject($bucketName . '/user-images/avatars/' . $dstPath, file_get_contents($srcFile), $meta);
                //var_Dump($res);
            } catch (Exception $e) {
                //var_dump($e->getMessage());
                //die();
                //@TODO: handle error
            }
        } while ($res !== true);
    }

    //////////////////
    //////////////////
    ///////////////////
    protected $_keepAspectRatio = true;

    protected function getUploadPath()
    {
        return APPLICATION_PATH . '/../public/upload/products';
    }

    protected function getImagePath($originalPath, $width, $height, $keepAspectRatio, $keepFrame, $constrainOnly)
    {
        $path = $this->_getImagePath($width, $height, $keepAspectRatio, $keepFrame, $constrainOnly);

        return $this->getUploadPath() . '/' . $path . '/' . $originalPath;
    }

    protected function _getImagePath($width, $height, $keepAspectRatio, $keepFrame, $constrainOnly)
    {
        return (int) $width . '_' . (int) $height . '_' . (int) $keepAspectRatio . '_' . (int) $keepFrame . '_' . (int) $constrainOnly;
    }

    public function getPath($originalPath, $width = null, $height = null, $keepAspectRatio = true, $keepFrame = true, $constrainOnly = false)
    {
        if ($width === null) {
            return $originalPath;
        }

        if ($height === null) {
            $height = $width;
        }

        $imagePath = $this->getImagePath($originalPath, $width, $height, $keepAspectRatio, $keepFrame, $constrainOnly);

        if (!file_exists($imagePath)) {
            $this->resize($originalPath, $width, $height, $keepAspectRatio, $keepFrame, $constrainOnly);
        }

        $path = $this->_getImagePath($width, $height, $keepAspectRatio, $keepFrame, $constrainOnly);

        $originalPath = preg_replace('/\.png$/', '.jpg', $originalPath);
        $originalPath = preg_replace('/\.gif$/', '.jpg', $originalPath);

        // var_dump();die($originalPath);
        return $path . '/' . $originalPath;
    }

    protected function resize($originalPath, $width, $height, $keepAspectRatio, $keepFrame, $constrainOnly)
    {
        $fullPath = $this->getUploadPath() . '/' . $originalPath;
        $newFilePath = $this->getImagePath($originalPath, $width, $height, $keepAspectRatio, $keepFrame, $constrainOnly);

        $newFilePath = preg_replace('/\.png$/', '.jpg', $newFilePath);
        $newFilePath = preg_replace('/\.gif$/', '.jpg', $newFilePath);

        $path = $this->_getImagePath($width, $height, $keepAspectRatio, $keepFrame, $constrainOnly);
        //var_dump(!file_exists($this->getUploadPath() . '/' . $path));die();    

        if (!file_exists($this->getUploadPath() . '/' . $path)) {
            mkdir($this->getUploadPath() . '/' . $path, 0777);
        }

        $write_image = 'imagejpeg';
        $image_quality = 90;

        switch (strtolower(substr(strrchr($originalPath, '.'), 1))) {
            case 'jpg':
            case 'jpeg':
                $src_img = @imagecreatefromjpeg($fullPath);
                break;
            case 'gif':
                $src_img = @imagecreatefromgif($fullPath);
                break;
            case 'png':
                $src_img = @imagecreatefrompng($fullPath);
                break;
            default:
                $src_img = null;
        }

        list($imgWidth, $imgHeight) = @getimagesize($fullPath);

        if ($imgWidth < $width && $imgHeight < $height) {
            if ($imgWidth > $imgHeight) {
                $newWidth = $imgWidth;
                $newHeight = $imgWidth;
            } else {
                $newWidth = $imgHeight;
                $newHeight = $imgHeight;
            }

            if ($newWidth < 500 && $newHeight < 500) {
                $newWidth = $newHeight = 500;
            }
        } else {
            $newWidth = $width;
            $newHeight = $height;
        }

        $scaleX = $imgWidth / $newWidth;
        $scaleY = $imgHeight / $newHeight;

        $scale = max($scaleX, $scaleY);

        $startX = ($newWidth - ($imgWidth / $scale)) / 2; //    0;//abs($newSize - $imgHeight) / 2;
        $startY = ($newHeight - ($imgHeight / $scale)) / 2; //abs($newSize - $imgWidth) / 2;

        $newImg = @imagecreatetruecolor($newWidth, $newHeight);
        $whiteColor = imagecolorallocate($newImg, 255, 255, 255);
        imagefill($newImg, 0, 0, $whiteColor);

        $success = $src_img && @imagecopyresampled(
                        $newImg, $src_img,
                        //0, 0, 0, 0,
                        $startX, $startY, 0, 0, $imgWidth / $scale, $imgHeight / $scale, $imgWidth, $imgHeight
                        //$img_width,
                        //$img_height
                ) && $write_image($newImg, $newFilePath, $image_quality);

        // Free up memory (imagedestroy does not delete files):
        @imagedestroy($src_img);
        @imagedestroy($newImg);
    }

}
