<?php
/**
 * Author: Alex P.
 * Date: 10.06.14
 * Time: 18:30
 */

/**
 * Calculates similarity of the strings using the simplified "shingles" algorithm
 */

class Ikantam_Text_Comparator_SimpleShingles implements Ikantam_Text_Comparator_Interface
{
    /**
     * Compare text by simplified "shingles" algorithm. Return similarity percentage.
     *
     * @param string $text1
     * @param string $text2
     * @return float - similarity percentage
     */
    public function compare($text1, $text2)
    {
        return $this->check($text1, $text2);
    }

    /**
     * Convert text to shingles
     *
     * @param string $text
     * @param int $n - words amount in one shingle
     * @return array
     */
    protected function getShingle($text, $n=3)
    {
        $shingles = array();
        $text = $this->cleanText($text);
        $elements = explode(" ",$text);
        for ($i=0;$i<(count($elements)-$n+1);$i++) {
            $shingle = '';
            for ($j=0;$j<$n;$j++){
                $shingle .= mb_strtolower(trim($elements[$i+$j]), 'UTF-8')." ";
            }
            if(strlen(trim($shingle)))
                $shingles[$i] = trim($shingle, ' -');
        }
        return $shingles;
    }

    /**
     * Canonicalizes text
     * @param string $text
     * @return string
     */
    protected function cleanText($text)
    {
        $new_text = preg_replace("[\,|\.|\'|\"|\\|\/i]","",$text);
        $new_text = preg_replace("/[\n|\t]/i"," ",$new_text);
        $new_text = preg_replace('/(\s\s+)/', ' ', trim($new_text));
        return $new_text;
    }

    /**
     * Calculate intersections percentage
     * @param $text1
     * @param $text2
     * @return float|int
     */
    protected function check($text1, $text2)
    {
        if (!$text1 || !$text2) {
            return 0;
        }

        $first_shingles = array_unique($this->getShingle($text1, 1));
        $second_shingles = array_unique($this->getShingle($text2, 1));

        $intersect = array_intersect($first_shingles, $second_shingles);

        $merge = array_unique(array_merge($first_shingles,$second_shingles));

        $diff = (count($intersect)/count($merge))/0.01;

        return $diff;

    }

}