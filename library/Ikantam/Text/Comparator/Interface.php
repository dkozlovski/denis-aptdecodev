<?php
/**
 * Author: Alex P.
 * Date: 10.06.14
 * Time: 18:26
 */

interface Ikantam_Text_Comparator_Interface
{
    /**
     * Compare 2 strings
     *
     * @param string $text1
     * @param string $text2
     * @return mixed
     */
    public function compare($text1, $text2);
} 