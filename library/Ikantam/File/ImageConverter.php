<?php

class Ikantam_File_ImageConverter
{
    protected $_fileName;
    
    public function convert($fullPath, $dstPath, $quality)
    {
        if (!file_exists($fullPath)) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$fullPath' not found");
        }

        if (!is_writable($fullPath)) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$fullPath' is not writable");
        }

        $filePath = $fullPath;
        $fileName = basename($filePath);

        switch (strtolower(substr(strrchr($fileName, '.'), 1))) {
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'png':
            case false:
                if($srcImg = @imagecreatefromjpeg($filePath)){
                   break; 
                }
                if($srcImg = @imagecreatefromgif($filePath)){
                    break;
                }
                if($srcImg = @imagecreatefrompng($filePath)){
                    break;
                }             
            default:
                $srcImg = null;
        }
        
        if (!$srcImg) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$fullPath' not found");
        }

        $imgWidth = imagesx($srcImg);
        $imgHeight = imagesy($srcImg);

        if (!$imgWidth || !$imgHeight) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$fullPath' not found");
        }

        $newImg = @imagecreatetruecolor($imgWidth, $imgHeight);
        
        imagefill($newImg, 0, 0, imagecolorallocate($newImg, 255, 255, 255));
        imagealphablending($newImg, true);
       
        @imagecopy($newImg, $srcImg, 0, 0, 0, 0, $imgWidth, $imgHeight);
        imagejpeg($newImg, $dstPath, $quality);
        
        // Free up memory (imagedestroy does not delete files):
        @imagedestroy($newImg);
        @imagedestroy($srcImg);
        
        return true;
    }
    
    
    
    
    public function createScaledImages($srcImagePath, $dstDirectoryPath, $angle = 0)
    {
        
        $versions = array(
            array('width' => 1500, 'height' => 1500, 'keepFrame' => true),
            //array('width' => 1500, 'height' => 1500, 'keepFrame' => false),
            //array('width' => 500, 'height' => 500, 'keepFrame' => true),
            array('width' => 500, 'height' => 500, 'keepFrame' => false),
            //array('width' => 200, 'height' => 200, 'keepFrame' => true),
            array('width' => 200, 'height' => 200, 'keepFrame' => false),
            array('width' => 360, 'height' => 300, 'keepFrame' => true),
                //array('width' => 360, 'height' => 300, 'keepFrame' => false),
        );

        $srcImg = @imagecreatefromjpeg($srcImagePath);
        
        $img_width = imagesx($srcImg);
        $img_height = imagesy($srcImg);

        if (!$srcImg || !$img_width || !$img_height) {
            return false;
        }

        foreach ($versions as $version) {

            if ($version['keepFrame']) {
                $scale = min($version['width'] / $img_width, $version['height'] / $img_height);
            } else {
                $scale = max($version['width'] / $img_width, $version['height'] / $img_height);
            }

            $new_width = $version['width'];
            $new_height = $version['height'];

            $scaledWidth = $img_width * $scale;
            $scaledHeight = $img_height * $scale;

            $startX = (int) (($new_width - $scaledWidth) / 2);
            $startY = (int) (($new_height - $scaledHeight) / 2);

            $newImg = @imagecreatetruecolor($new_width, $new_height);
            $whiteColor = imagecolorallocate($newImg, 255, 255, 255);
            imagefill($newImg, 0, 0, $whiteColor);

            $newFileName = $version['width'] . '-' . $version['height'] . '-' . (($version['keepFrame']) ? 'frame' : 'crop') . '-' . $angle . '.jpg';

            $new_file_path = $dstDirectoryPath . DIRECTORY_SEPARATOR . $newFileName;

            

            //@imagecopyresampled(
            @imagecopyresized($newImg, $srcImg, $startX, $startY, 0, 0, $scaledWidth, $scaledHeight, $img_width, $img_height);
            imagejpeg($newImg, $new_file_path, 75);
            @imagedestroy($newImg);
            $this->_fileName[] = $new_file_path;
        }

        @imagedestroy($srcImg);
        return true;
    }
    
    public function getFileName()
    {
        if (!is_array($this->_fileName)) {
            return $this->_fileName;
        }
        
        if (count($this->_fileName) < 2) {
            return current($this->_fileName);
        }
        
        return $this->_fileName;
    }
    
    
}