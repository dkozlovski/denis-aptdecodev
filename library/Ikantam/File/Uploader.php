<?php

class Ikantam_File_Uploader
{

    const ERROR_FILE_SIZE_MIN  = 'File size should be greater than 1KB';
    const ERROR_FILE_SIZE_MAX  = 'File size should be less than 14MB';
    const ERROR_FILE_EXTENSION = 'File extension should be .png, .gif or .jpg/jpeg';
    const ERROR_FILE_TYPE      = 'File should be an image';

    protected $_uploadPath = '';
    protected $_adapter    = null;
    protected $_params     = array();

    /**
     * 
     * @param Zend_File_Transfer_Adapter_Abstract $adapter
     * @return \Ikantam_File_Uploader
     */
    public function setAdapter(Zend_File_Transfer_Adapter_Abstract $adapter)
    {
        $this->_adapter = $adapter;
        return $this;
    }

    /**
     * 
     * @return \Zend_File_Transfer_Adapter_Abstract
     */
    public function getAdapter()
    {
        return $this->_adapter;
    }

    public function setUploadPath($uploadPath)
    {
        $this->_uploadPath = $uploadPath;
        return $this;
    }

    public function getUploadPath()
    {
        return $this->_uploadPath;
    }

    public function setParams(array $params)
    {
        $this->_params = $params;
        return $this;
    }

    public function getParams()
    {
        return $this->_params;
    }

    protected function addDefaultValidators()
    {
        $size = new Zend_Validate_File_Size(array('min' => '1kB', 'max' => '14MB'));
        $size->setMessages(array(
            Zend_Validate_File_Size::TOO_BIG => self::ERROR_FILE_SIZE_MAX,
            Zend_Validate_File_Size::TOO_SMALL => self::ERROR_FILE_SIZE_MIN,
        ));

        $extension = new Zend_Validate_File_Extension(array('jpg', 'jpeg', 'png', 'gif'));
        $extension->setMessages(array(
            Zend_Validate_File_Extension::FALSE_EXTENSION => self::ERROR_FILE_EXTENSION,
        ));

        $isImage = new Zend_Validate_File_IsImage();
        $isImage->setMessages(array(
            Zend_Validate_File_IsImage::FALSE_TYPE => self::ERROR_FILE_TYPE,
            Zend_Validate_File_IsImage::NOT_DETECTED => self::ERROR_FILE_TYPE,
        ));

        $this->getAdapter()
                ->addValidator($size, true)
                //->addValidator($extension, true)
                ->addValidator($isImage, true);
    }
    
    protected function addDefaultFilters()
    {
        $params  = $this->getParams();
        $this->getAdapter()
                ->addFilter(new Ikantam_ConvertImage($params));
    }

    public function upload()
    {
        $adapter = $this->getAdapter();
        $path    = $this->getUploadPath();

        $adapter->setDestination($path);

        $this->addDefaultValidators();

        if (!$adapter->receive()) {
            return false;
        }

        return true;
    }
    
    public function uploadToS3($srcFile, $dstPath)
    {
        $awsKey = Zend_Registry::get('config')->amazon_s3->awsKey;
        $secretKey = Zend_Registry::get('config')->amazon_s3->secretKey;

        $s3 = new Zend_Service_Amazon_S3($awsKey, $secretKey);

        $meta = array(Zend_Service_Amazon_S3::S3_ACL_HEADER => Zend_Service_Amazon_S3::S3_ACL_PUBLIC_READ);

        $res = false;
        do {
            try {

                $res = $s3->putObject($dstPath, file_get_contents($srcFile), $meta);

            } catch (Exception $e) {
                //var_dump($e->getMessage());
                //die();
                //@TODO: handle error
            }
        } while ($res !== true);
    }
    
    public function getFileName()
    {
        return $this->getAdapter()->getFileName();
    }

    public function getErrorMessages()
    {
        return $this->getAdapter()->getMessages();
    }

    public function uploadImage($uploadPath, $productId)
    {
        $productPath = $this->_getProductUploadDir($uploadPath, $productId);
        $productDir  = $uploadPath . DIRECTORY_SEPARATOR . $productPath;

        $imagePath = $this->_getImageUploadDir($productDir);
        $imageDir  = $productDir . DIRECTORY_SEPARATOR . $imagePath;

        $params = array('file_path' => $imageDir, 'jpeg_quality' => 90);

        $adapter = new Zend_File_Transfer_Adapter_Http();

        $adapter->setDestination($productDir);
        $adapter->addValidator('Size', false, array('min' => '1kB', 'max' => '32MB'));
        $adapter->addValidator('Extension', false, array('jpg', 'jpeg', 'png', 'gif'));
        //$adapter->addValidator('FilesSize', false, array('min' => '1kB', 'max' => '32MB'));
        $adapter->addValidator('IsImage', false);
        $adapter->addFilter(new Ikantam_ConvertImage($params));

        if (!$adapter->receive()) {
            $messages = $adapter->getMessages();
            echo implode("\n", $messages);
            die();
        }

        return $productPath . DIRECTORY_SEPARATOR . $imagePath;
    }

}