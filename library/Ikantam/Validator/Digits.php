<?php
    class Ikantam_Validator_Digits extends Zend_Validate_Digits
        {
            public function isValid ($var)
            {
                if(is_array($var))
                    {
                        foreach($var as $value)
                        {
                          if(!parent::isValid($value)) return false;  
                        }
                        return true;
                    } else
                        {
                            return parent::isValid($var);
                        }
            
            }
            
        }