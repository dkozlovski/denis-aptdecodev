<?php

class Ikantam_PdfTemplate
{
    protected $writer;
    protected $_defaultFontSize = 8; //pt
    protected $_smallFontSize = 4.5; //pt
    
    
    public function __construct (array $data)
    {
       $this->writer = new Ikantam_PdfPageWriter();
       $this->createTemplate($data);
    }
    
    protected function toStd ($d)    {

		if (is_array($d)) {

			return (object) array_map(array($this,'toStd'), $d);
		}
		else {
			return $d;
        }
	
    
    }
    
    protected function prepareStrings($args)
    {
        $strings = func_get_args();
        $result = '';
        foreach($strings as $string)
        {
            $result.="\n".$string;
        }
        return $result;
    }
    

    
    protected function createTemplate($data)
    {
     $data = $this->toStd($data); 
     $shop = &$data->shop;
     $bank = &$shop->bank;
     $contacts = &$data->contacts;
     $customer = &$data->customer;
     $deliv = &$customer->delivery;
     $invoice = &$data->invoice;
     $this->writer->drawRectangle(360, 156, 561, 220);// header right column
//HEADER  
     $this->writer->write($shop->name.', '.$shop->street.', '.$shop->zip.' '.$shop->city, $this->_smallFontSize, 62, 156) // Shop adress
                  ->write('Rechnung', $this->_defaultFontSize + 0.5, 364, 164, 'Times-Bold')// Header of right column rectangle  
                  ->writeStrings($this->prepareStrings( // Customer info
                                                        'An', 
                                                        $customer->name, 
                                                        $customer->street, 
                                                        $customer->zip.' '.$customer->city), $this->_defaultFontSize, 62, 172)
                   ->writeStrings($this->prepareStrings( // Right column rectangle. Invoice info.
                                                         'Rechnungsdatum: '.$invoice->actualDate,
                                                         'Liefertermin: '.$invoice->actualDate1,
                                                         ($invoice->customerNo) ? 'Kundenummer: '.$invoice->customerNo : null,
                                                         'Bestellnummer: '.$invoice->numberOfOrder,
                                                         'Rechnungsnummer: '.$invoice->numberOfInvoice,
                                                         'Bei der Bezahlung bitte immer angeben'
                                                         ), $this->_defaultFontSize - 0.5, 364, 174)
                   ->write('Auftraggeber:  '.$customer->name.', '.$customer->street.', '.$customer->zip.' '.$customer->city, $this->_defaultFontSize + 2, 62, 270)
                   ->write('Versandanschrift:  '.$deliv->name.', '.$deliv->street.', '.$deliv->zip.' '.$deliv->city, $this->_defaultFontSize + 2, 62, 285);
//PRODUCTS TABLE
    
    $lineStyle = new Zend_Pdf_Style();
    $lineStyle->setLineColor(new Zend_Pdf_Color_Html('#4a7ebb'));
    $lineStyle->setLineWidth(0.6);
    
    $align = new stdClass();
    $align->c1 = 62;
    $align->c2 = 62 + $this->writer->widthForStringUsingFontSize('Pos       ', $this->writer->getFont(), $this->_defaultFontSize + 3);
    $align->c3 = 561 - $this->writer->widthForStringUsingFontSize('Menge     Einzelpreis     Gesamtpreis', $this->writer->getFont(), $this->_defaultFontSize + 3);
    $align->c4 = $align->c3 + $this->writer->widthForStringUsingFontSize('Menge     ', $this->writer->getFont(), $this->_defaultFontSize + 3);
    $align->c5 = $align->c4 + $this->writer->widthForStringUsingFontSize('Einzelpreis     ', $this->writer->getFont(), $this->_defaultFontSize + 3);
    $align->right->c4 = $align->c4 + $this->writer->widthForStringUsingFontSize('Einzelpreis', $this->writer->getFont(), $this->_defaultFontSize + 3);
    $align->right->c5 = $align->c5 + $this->writer->widthForStringUsingFontSize('Gesamtpreis', $this->writer->getFont(), $this->_defaultFontSize + 3);      
              
    $this->writer->write('Pos       Articel', $this->_defaultFontSize + 3, 62, 355)
                 ->write('Menge     Einzelpreis     Gesamtpreis',
                  $this->_defaultFontSize + 3,
                   $align->c3, 355);
    $this->writer->drawLine(62, 362, 561, 362, $lineStyle);
    
    $products = (array)$invoice->products;
    $top = 0;
    $ratio = ceil($this->_defaultFontSize/3) + $this->_defaultFontSize;
    $fillStyle = new Zend_Pdf_Style();
    $fillColor = new Zend_Pdf_Color_GrayScale(0.75);
    $fillStyle->setFillColor($fillColor);
    $fillStyle->setLineColor($fillColor);
    for($i = 0; $i < count($products); $i++)
    {
      if($i % 2 == 1)
      {
        $this->writer->drawRectangle(62, 365 + $top, 561, 365 + $ratio + $top, Zend_Pdf_Page::SHAPE_DRAW_FILL, $fillStyle);
      }
      $this->writer->write($i+1, $this->_defaultFontSize, $align->c1, 372 + $top) //Pos
                   ->write($products[$i]->name, $this->_defaultFontSize, $align->c2, 372 + $top) // product name
                   ->write($products[$i]->quantity,$this->_defaultFontSize, $align->c3, 372 + $top) // order quantity
                   ->write($products[$i]->price,$this->_defaultFontSize, $align->right->c4 - $this->writer->widthForStringUsingFontSize($products[$i]->price, $this->writer->getFont(), $this->_defaultFontSize), 372 + $top) //product price
                   ->write($products[$i]->sum,$this->_defaultFontSize, $align->right->c5 - $this->writer->widthForStringUsingFontSize($products[$i]->sum, $this->writer->getFont(), $this->_defaultFontSize), 372 + $top); //sum
                   
     $top += $ratio;                   
      
    }
    
    $top+= 370;
    $this->writer->drawLine(62,$top, 561, $top, $lineStyle);
    $this->writer->write('Summe', $this->_defaultFontSize, 62, $top  + 10, 'Times-Bold')
                 ->write('Versandkosten', $this->_defaultFontSize, 62, $top + 15 + $this->_defaultFontSize, 'Times-Bold')
                 ->write($invoice->orderQt, $this->_defaultFontSize, $align->right->c5 - $this->writer->widthForStringUsingFontSize($invoice->orderQt, $this->writer->getFont(), $this->_defaultFontSize), $top  + 10, 'Times-Bold')
                 ->write($invoice->shippingCost, $this->_defaultFontSize, $align->right->c5 - $this->writer->widthForStringUsingFontSize($invoice->shippingCost, $this->writer->getFont(), $this->_defaultFontSize), $top  + 15 + $this->_defaultFontSize, 'Times-Bold');
    $top+= 27;
    
    $this->writer->drawLine(62, $top, 561, $top);
    
    $top+= 10;
    
    $this->writer->write('Summe',$this->_defaultFontSize, 62, $top, 'Times-Bold')
         ->write($invoice->total, $this->_defaultFontSize, $align->right->c5 - $this->writer->widthForStringUsingFontSize($invoice->total, $this->writer->getFont(), $this->_defaultFontSize), $top, 'Times-Bold');  
 
//BELOW PRODUCTS TABLE   
    $top+=20;
    $this->writer->write($invoice->tax, $this->_defaultFontSize, 62, $top);
    
    $top+=40;
    $this->writer->writeStrings($invoice->payInfo, $this->_defaultFontSize, 62, $top);
//FOOTER 
    $top = 841 - (($this->_smallFontSize * 3) + 40);
    $this->writer->writeStrings($this->prepareStrings($shop->name, $shop->street, $shop->zip.' '.$shop->city),$this->_smallFontSize +  2, 62, $top)
                 ->writeStrings($this->prepareStrings('Tel.: '.$contacts->phone, 'email: '.$contacts->email, 'Web: '.$contacts->site),$this->_smallFontSize + 2, 177, $top)
                 ->writeStrings($this->prepareStrings('Geschäftsführer: '.$shop->owner, 'HG: '.$shop->regNo, 'US-ID: '.$shop->taxNo),$this->_smallFontSize + 2, 320, $top)
                 ->writeStrings($this->prepareStrings('Bankverbindung: '.$bank->name,'Hypovereinsbank'. $bank->hypovereinsbank , $bank->iban),$this->_smallFontSize + 2, 561 - $this->writer->widthForStringUsingFontSize($bank->iban, $this->writer->getFont(), $this->_smallFontSize + 2), $top);
    
                         
    


                
    }
    
    public function getWriter ()
    {
        return $this->writer;
    }
    
    
}