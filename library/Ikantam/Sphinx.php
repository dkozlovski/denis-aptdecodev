<?php

require_once 'sphinxapi.php';

class Ikantam_Sphinx extends SphinxClient
{
	public function __construct()
	{
		$config = Zend_Registry::get('config')->resources->db->params;
		$this->SetServer($config->host, $config->username, $config->password, 3312);
		$this->SetConnectTimeout(1);
		//$sphinx->SetArrayResult(true);
		$this->SetMatchMode(SPH_MATCH_BOOLEAN);
		parent::__construct();
	}
}
