<?php

abstract class Ikantam_Search_Abstract
{
    protected $_items = array();
    protected $_keyWords = array();
    protected $_SearchParams;
    protected $_htmlTag;
    protected $_tagAttr;
    
    protected function KeyWordsToArray($keys)
    {
        if(is_array($keys)){
            
            $keys = array_unique($keys);
            usort($keys, function($a,$b){
                if(strlen($a) == strlen($b)){
                    return 0;
                }
                return (strlen($a) > strlen($b)) ? -1 : 1;
            });
            $this->_keyWords = $keys;
            
        }elseif(is_string($keys)){
            
            $keys = preg_replace('/\\s+/', ' ', $keys);
            $keys = trim($keys);
            $keys = explode(' ',$keys);
            $keys = array_unique($keys);
            usort($keys, function($a,$b){
                if(strlen($a) == strlen($b)){
                    return 0;
                }
                return (strlen($a) > strlen($b)) ? -1 : 1;
            });
            
        }
        
        return $keys;
    }
    
    protected function setStyleKeyWords($content)
    {
        if(!empty($this->_items) && !empty($this->_keyWords)){
            
            if(isset($this->_items[0]->$content) && is_string($this->_items[0]->$content)){
                
                foreach ($this->_items as $item){
                        
                        $item->$content = $this->setStyleKeyWordsItem($item->$content);
 
                }
                
            }elseif(isset($this->_items[0][$content]) && is_string($this->_items[0][$content])){
                
                foreach ($this->_items as &$item){

                        $item[$content] = $this->setStyleKeyWordsItem($item[$content]);

                }
                
            }elseif(is_string($this->_items[0])){
                
                foreach ($this->_items as &$item){

                        $item = $this->setStyleKeyWordsItem($item);

                }
                
            }
            
        }
        
        return $this;
    }
    
    private function setStyleKeyWordsItem($text)
    {   
        $arKeyWords = $this->KeyWordsToArray($this->_keyWords);
        foreach($arKeyWords as $key){
            
           $text =  preg_replace("/(".preg_quote($key).")/i", "<{$this->_htmlTag} {$this->_tagAttr}>$0</{$this->_htmlTag}>", $text);
            
        }
        return $text;
    }
    
    public function setStyleParams($htmlTag = "span", $tagAttr = "")
    {
        $this->_htmlTag = $htmlTag;
        $this->_tagAttr = $tagAttr;
        
        return $this;
    }
    
    public function search($keys, $SearchParams = null)
    {
        $this->_keyWords = $keys;
        $this->_SearchParams = $SearchParams;
        $this->FindItems();

        return $this->_items;
    }
    
    abstract protected function FindItems();
}

