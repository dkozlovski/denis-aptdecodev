<?php

class Ikantam_Search_Blogs extends Ikantam_Search_Abstract
{
    
    protected function FindItems() {
        
        
        $client = new Elasticsearch\Client();
        $resDoc = $client->search(
                array(
                    'index' => 'blog',
                    'type' => 'posts',
                    'body' => array(
                        'query' => array(
                            'match'=> array(
                                '_all'=>array(
                                    'query' => $this->_keyWords,
                                    'operator' => 'or'
                                )                            
                            )
                        )
                    )
                ));

        $arID = array();
        foreach($resDoc["hits"]["hits"] as $row){
            
            $arID[] = $row["_id"];
            
        }
       
        $wp_db_connector = new Application_Model_Static();
        $this->_items = $wp_db_connector->getFindBlogPosts($arID);
        
        if($this->_htmlTag){
        $this->setStyleKeyWords("post_content");
        $this->setStyleKeyWords("post_title");
        $this->setStyleKeyWords("term_name");
        }
    }
}

