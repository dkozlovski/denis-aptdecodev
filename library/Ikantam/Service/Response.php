<?php
abstract class Ikantam_Service_Response
{
    /**
     * Contains response array
     * @type string 
     */    
    protected $_data;
    
    /**
     * Contains raw response data
     * @type string 
     */
    protected $_rawResponseData ;
    
    public function __get($name)
    {
        if(isset($this->_data[$name])){
            return $this->_data[$name];
        }
        
        trigger_error('Undefined property "'.$name.'"', E_USER_WARNING);
    }
    
    /**
     * Walk recursive through all response elements and return first value from first the matched element name
     * 
     * @param string $name
     * @param array $value
     * @return mixed 
     */
    public function find ($name)
    {
        return $this->_find($name, $this->_data);    
    }
    
    /**
     * Walk recursive through all response elements and return first value from first the matched element name
     * 
     * @param string $name
     * @param array $haystack
     * @return mixed 
     */
    protected function _find($name, $haystack)
    {
        foreach($haystack as $key => $value) {
            if($name === $key) {
                return $value;
            } elseif(is_array($value)) {
                 $result = $this->_find($name, $value);
                 if(false !== $result) {
                    return $result;
                 }
            }
        }
        return false;    
    }
    
    public function __construct ($rawResponse)
    {
        $this->_setResponseString($rawResponse);
        $this->_data = $this->toArray();
    }
    
    /**
     * Set raw data property
     * 
     * @param string $data  
     * @return object self 
     */
    protected function _setResponseString ($data)
    {
        $this->_rawResponseData = $data;
        return $this;
    }
    
    /**
     * Return raw response string
     *
     * @return string 
     */
    public function getResponseString ()
    {
        return $this->_rawResponseData;
    }
    
    /**
     * Converts response data to array
     *
     * @return array 
     */
    abstract public function toArray ();
    
    /**
     * Converts response data to stdClass object
     *
     * @return object stdClass 
     */
    public function toObject ()
    {
        return (object) $this->toArray();
    }
    
    /**
     * Checks if response is empty
     * 
     * @return bool 
     */
    public function isEmpty ()
    {
        return empty($this->_rawResponseData);
    }
 
}