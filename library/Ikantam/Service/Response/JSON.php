<?php
abstract class Ikantam_Service_Response_JSON extends Ikantam_Service_Response
{   
    /**
     * implementation of Ikantam_Service_Response::toArray
     * @return array
     */
    public function toArray ()
    {
        return Zend_Json::decode($this->getResponseString());
    }
}