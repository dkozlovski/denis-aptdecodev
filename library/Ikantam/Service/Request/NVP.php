<?php
//Original source code was taken from PayPal SDK's PayPal\Core\PPMessage
abstract class Ikantam_Service_Request_NVP extends Ikantam_Service_Request
{
	/**
	 * @param string $prefix
	 * @return string
	 */
	public function toNVPString($prefix = '')
	{
		$nvp = array();
		foreach (get_object_vars($this) as $property => $defaultValue) {
			
			if (($propertyValue = $this->{$property}) === NULL || $propertyValue == NULL) {
				continue;
			}

			if (is_object($propertyValue)) {
				$nvp[] = $propertyValue->toNVPString($prefix . $property . '.'); // prefix

			} elseif (is_array($defaultValue) || is_array($propertyValue)) {
				foreach (array_values($propertyValue) as $i => $item) {
					if (!is_object($item)){
                        $nvp[] = $prefix . $property . "($i)" . '=' . urlencode($item);
					}else{
                        $nvp[] = $item->toNVPString($prefix . $property . "($i).");
                    }
				}

			} else {
				// Handle classes with attributes
				if($property == 'value' && ($anno = Ikantam_Service_Utils::propertyAnnotations($this, $property)) != NULL && isset($anno['value']) ) {
					$nvpKey = substr($prefix, 0, -1); // Remove the ending '.'
				} else {
					$nvpKey = $prefix . $property ;
				}
				$nvp[] = $nvpKey . '=' . urlencode($propertyValue);
			}
		}

		return implode('&', $nvp);
	}

    
    public function __toString()
    {
        return $this->toNVPString();
    }

}