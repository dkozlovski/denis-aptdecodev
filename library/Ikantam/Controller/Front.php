<?php

/**
 * Class Ikantam_Controller_Front
 * @method User_Model_Session getSession()
 */
class Ikantam_Controller_Front extends Ikantam_Controller_Abstract
{

    protected $_sessionModel = 'User_Model_Session';

    /**
     * @var \Core\Service\JSConfig
     */
    protected $jsConfig;

    public function __construct(\Zend_Controller_Request_Abstract $request, \Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        parent::__construct($request, $response, $invokeArgs);

        $this->jsConfig = $this->getContainer()
            ->get('core.service.JSConfig');
        
        $this->_setDefaultCssJs();        
        $this->_initJsCss();
        $this->_insertDescribedScripts();
        $this->_initJsConfig();

        $this->view->jsConfig__ = $this->jsConfig;
    }

    /**
     * @return DI\ContainerInterface
     */
    public static function getContainer()
    {
        return Zend_Controller_Front::getInstance()
            ->getDispatcher()
            ->getContainer();
    }

    private function _setDefaultCssJs()
    {

//CSS
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/core.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/bootstrap.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/bootstrap-responsive.css'));
        //$this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/static.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/additional.css'));
//$this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/editor-style.css'));
//Scripts
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery-1.8.3.min.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/iKantam.module.core.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/aptdeco.overlay.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery.cookie.js'));
//$this->view->headScript()->appendFile('//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/addons.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/input.spin.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery-ui-1.9.2.min.js'));
//$this->view->headScript()->appendFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js');

        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/custom.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/bootstrap.min.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jQuery.dPassword.js'));

//$this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/bootstrap-fileupload.min.js'));

        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jcarousellite.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/cloud-zoom.1.0.2.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/zoom.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/maskedinput.min.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery.tinyscrollbar.min.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery.ui.touch-punch.min.js'));



    }

    protected function _initJsConfig()
    {
        $this->jsConfig->setOptions(array(
                'base_url' => Ikantam_Url::getUrl(''),
                'public_url' => Ikantam_Url::getPublicUrl(''),
            ));
    }

    protected function _initJsCss()
    {
        
    }

    protected function isAjax()
    {
        return $this->getRequest()->isXmlHttpRequest();
    }
}
