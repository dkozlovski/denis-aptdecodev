<?php

class Ikantam_Controller_CatalogAbstract extends Ikantam_Controller_Front
{

    protected $_productsPerPage = 6;

    public function init()
    {
        parent::init();
    }
    
    protected function applyQueryFilter($products)
    {
        $query = $this->getRequest()->getParam('q');

        if ($query) {
            $query = '%' . str_replace('%', '', $query) . '%';
            $products->addFilter('title', $query, 'like');
        }
    }

    protected function applyOrder($products)
    {
        $products->addOrder('is_sold', 'ASC');

        $sortBy = $this->getRequest()->getParam('sort');

        if ($sortBy == 'price' || $sortBy == 'rating') {
            $dir = ($this->getRequest()->getParam('dir') == 'desc') ? 'DESC' : 'ASC';
            $products->addOrder($sortBy, $dir);
        }

        $products->addOrder('created_at', 'DESC');
    }

    protected function applyLimit($products)
    {
        $page = (int) $this->getRequest()->getParam('page');
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $this->_productsPerPage;
        $products->setOffset($offset)->setLimit($this->_productsPerPage);
    }

    protected function applyMiscFilter($products)
    {
        $products->addFilter('is_visible', 1, 'eq')
                ->addFilter('is_published', 1, 'eq')
                ->addFilter('is_sold', 0, 'eq');
                //->addFilter('user_id', $this->getSession()->getUserId(), 'neq');
    }

    protected function applyConditionsFilter($products)
    {
        $conditions = (array) $this->getRequest()->getParam('conditions');

        if (count($conditions) > 0) {
            $products->addFilter('condition', $conditions, 'in');
        }
    }

    protected function applyColorsFilter($products)
    {
        $colors = (array) $this->getRequest()->getParam('colors');

        if (count($colors) > 0) {
            $products->addFilter('color_id', $colors, 'in');
        }
    }

    protected function applyBrandsFilter($products)
    {
        $brands = (array) $this->getRequest()->getParam('brands');

        if (count($brands) > 0) {
            $products->addFilter('manufacturer_id', $brands, 'in');
        }
    }

    protected function applyPricesFilter($products)
    {
        $minPrice = (int) $this->getRequest()->getParam('min_price');
        $maxPrice = (int) $this->getRequest()->getParam('max_price');

        if ($minPrice > 0) {
            $products->addFilter('price', $minPrice, 'gteq');
        }
        if ($maxPrice > 0) {
            $products->addFilter('price', $maxPrice, 'lteq');
        }
    }

    protected function applyCategoriesFilter($productsCollection)
    {
        $categoryId = $this->getRequest()->getParam('category');

        if ($categoryId) {
            $category = new Catalog_Model_Category($categoryId);

            if ($category->getId() && !$category->getParentId()) {
                $subcategories = $category->getSubCategories()->toArray();
                $productsCollection->addFilter('category_id', $category->getSubCategories(), 'in');
            } else {
                $productsCollection->addFilter('category_id', $category->getId(), 'eq');
                return;
            }
        }
        $subcategories = (array) $this->getRequest()->getParam('categories');

        if (count($subcategories) > 0) {
            $productsCollection->addFilter('category_id', $subcategories, 'in');
        }
    }

}