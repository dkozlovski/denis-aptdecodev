<?php

abstract class Ikantam_Controller_Abstract extends Zend_Controller_Action
{

	protected $_sessionModel;
	protected $_session;
    protected $_configPath ;

	public function __construct(\Zend_Controller_Request_Abstract $request, \Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
	{
		$this->setSession(new $this->_sessionModel());
        $this->_configPath = APPLICATION_PATH.'/configs/';
		parent::__construct($request, $response, $invokeArgs);
	}

	/**
	 * 
	 * @param \Ikantam_Session $session
	 * @return \Ikantam_Controller_Abstract
	 */
	protected function setSession(\Ikantam_Session $session)
	{
		$this->_session = $session;
		return $this;
	}

	/**
	 * 
	 * @return \Ikantam_Session
	 */
	protected function getSession()
	{
		return $this->_session;
	}

	protected function addError($errorMessage)
	{
		$this->getSession()->addError($errorMessage);
	}

	protected function addErrors(array $errorMessages)
	{
		$this->getSession()->addErrors($errorMessages);
	}

	/**
	 * Log Exception to file
	 * 
	 * @param Exception $exception
	 * @param boolean $logParams
	 */
	protected function logException(Exception $exception, $logParams = true)
	{
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/log/exception.log');
		$logger->addWriter($writer);
		$logger->log($exception, Zend_Log::INFO); //@TODO: priority?
		if ($logParams) {
			$logger->log(var_export($this->getRequest()->getParams(), true), Zend_Log::INFO);
		}
	}
    
	/** 
     * @param string $config - file name
     * @param string $section - section name, optional
     * @param array $options
     * 
     * @return Zend_Config_Xml 
     */    
    protected function getXmlConfig($config, $section = null, $options = false)
    {
        if(substr($config, -4) != '.xml') {
            $config .= '.xml';
        }
        $cfg = new Zend_Config_Xml($this->_configPath.$config, $section, $options);
        return $cfg;         
    }
    
    protected function _insertDescribedScripts()
    {
        if (isset($this->_js) && is_array($this->_js)) {
            foreach ($this->_js as $js) {
                $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl($js), 'text/javascript');
            }
        }

        if (isset($this->_css) && is_array($this->_css)) {
            foreach ($this->_css as $css) {
                $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl($css));
            }
        }
    }

    /**
     * Converts data to JSON format and set it to response object with appropriate headers
     * @param mixed $data
     * @return \Zend_Controller_Response_Abstract
     */
    protected function responseJSON($data)
    {
        return $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->appendBody(Zend_Json::encode($data));
    }

}
