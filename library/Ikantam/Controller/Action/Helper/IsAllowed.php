<?php

 
class Ikantam_Controller_Action_Helper_IsAllowed extends Zend_Controller_Action_Helper_Abstract {

    public function getAcl ()
    {
        return Application_Model_Acl::getInstance();
    }
    
    public function getCurrentUser ()
    {
        if(Zend_Controller_Front::getInstance()->getRequest()->module === 'admin') {
             $role = new Admin_Model_Roles_Backend();
            return $role->getRole(Admin_Model_Session::instance()->getAdmin()->getData('role_id'));
        } 
        return User_Model_Session::instance()->getUser();
    }
    
    public function direct ($resource = null, $privilege = null)
    { 
        
        return $this->getAcl()->isAllowed($this->getCurrentUser(), $resource, $privilege);
    }
}