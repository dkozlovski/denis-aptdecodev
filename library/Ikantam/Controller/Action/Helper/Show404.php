<?php
/**
 * @author Aleksei Poliushkin
 * @copyright iKantam 2013/8/1   
 */
 
class Ikantam_Controller_Action_Helper_Show404 extends Zend_Controller_Action_Helper_Abstract {

    protected function _init ($message, $code)
    {
        throw new Zend_Controller_Action_Exception($message, $code);
    }
    
    public function direct ($message = 'Page not found', $code = 404)
    {
        $this->_init($message, $code);
    }    
}   