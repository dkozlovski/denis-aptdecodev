<?php
/**
 * @author Aleksei Poliushkin
 * @copyright iKantam 2013/7/30   
 */
 
class Ikantam_Controller_Action_Helper_RedirectAfter extends Zend_Controller_Action_Helper_Abstract {
  
	/**      
     * Run redirect.
     */    
    public function initRedirect ()
    {
        $_redirect = User_Model_Session::instance()->activeData('_*^%$*^468f_redirect');
        $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');

        if(is_array($_redirect)) {
            $redirector->gotoRoute($_redirect);
        } elseif (is_string($_redirect)) {
            if (!preg_match('/^https?:\/\//', $_redirect)) {
                 $_redirect = Ikantam_Url::getUrl($_redirect);
            }

            $redirector->gotoUrl($_redirect);
        }
    }
	/**
     * Save the request params for redirect
     * @param array $params - request params
     */    
    public function saveRedirectParams ($params = null)
    {
        if(!$params) {
            $params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
        }
        User_Model_Session::instance()->activeData('_*^%$*^468f_redirect', $params);
    }
    
    public function hasRedirect ()
    {
        return (bool) User_Model_Session::instance()->activeData('_*^%$*^468f_redirect', null, true);
    }
    
    public function getRedirectParams ()
    {
        return User_Model_Session::instance()->activeData('_*^%$*^468f_redirect', null, true);
    }
    
    public function direct ($params = null)
    {
        $this->saveRedirectParams($params);
    }
}