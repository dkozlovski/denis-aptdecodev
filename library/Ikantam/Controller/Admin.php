<?php

class Ikantam_Controller_Admin extends Ikantam_Controller_Abstract
{

    protected $_sessionModel = 'Admin_Model_Session';
    private   $_testMode = true;

    /**
     * @var \Core\Service\JSConfig
     */
    protected $jsConfig;

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        parent::__construct($request, $response, $invokeArgs);
        if(!$this->_testMode) {
            $params =  $this->getRequest()->getParams();
            if( !$this->getSession()->isLoggedIn() && ($params['controller'] != 'login' && $params['action'] != 'index'))
            {
                $this->_redirect('admin/login/index');
            }
        }

        $this->jsConfig = Zend_Controller_Front::getInstance()
            ->getDispatcher()
            ->getContainer()
            ->get('core.service.JSConfig');

        $this->_setDefaultCssJs();
        $this->_initJsCss();
        $this->_insertDescribedScripts();
        $this->_initJsConfig();

        $this->view->jsConfig__ = $this->jsConfig;
    }

    private function _setDefaultCssJs() {

//CSS
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/core.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/bootstrap.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/bootstrap-responsive.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/additional.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/static.css'));
//$this->view->headLink()->appendStylesheet('https://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css');

//$this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/editor-style.css'));
//Scripts
        $this->view->headScript()->prependFile(Ikantam_Url::getPublicUrl('js/iKantam/iKantam.module.core.js'));
        $this->view->headScript()->prependFile(Ikantam_Url::getPublicUrl('js/custom.js'));
        $this->view->headScript()->prependFile(Ikantam_Url::getPublicUrl('js/jquery/jquery-1.8.3.min.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/addons.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery-ui.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/bootstrap.min.js'));
//$this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/bootstrap-fileupload.min.js'));

        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jcarousellite.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jcarousellite.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/cloud-zoom.1.0.2.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/zoom.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/maskedinput.min.js'));

    }

    protected function _initJsConfig()
    {
        $this->jsConfig->setOptions(array(
                'base_url' => Ikantam_Url::getUrl(''),
                'public_url' => Ikantam_Url::getPublicUrl(''),
            ));
    }

    protected function _initJsCss()
    {}

}