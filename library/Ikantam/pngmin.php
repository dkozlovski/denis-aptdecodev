<?php

class pngmin
{
    public static function compress_png($path_to_png_file, $pathapp ,$max_quality = 90)
    {
        $compressed_png_content = "";
        if(file_exists($pathapp."pngquant")){
            if (file_exists($path_to_png_file)) {
                $min_quality = 60;
                $compressed_png_content = shell_exec($pathapp."pngquant --quality=$min_quality-$max_quality - < ".escapeshellarg($path_to_png_file));
            }
        }
        return $compressed_png_content;
    } 
}

