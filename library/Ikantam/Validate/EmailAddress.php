<?php

class Ikantam_Validate_EmailAddress extends Zend_Validate_EmailAddress
{

    protected $_singleErrorMessage = "Email address is invalid.";

    public function isValid($value)
    {
        $valid = parent::isValid($value);

        if (!$valid) {
            $this->_messages = array($this->getSingleErrorMessage());
        }

        return $valid;
    }

    public function getSingleErrorMessage()
    {
        return $this->_singleErrorMessage;
    }

    public function setSingleErrorMessage($singleErrorMessage)
    {
        $this->_singleErrorMessage = $singleErrorMessage;
        return $this;
    }

}