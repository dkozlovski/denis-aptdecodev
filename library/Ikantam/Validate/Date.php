<?php

class Ikantam_Validate_Date extends Zend_Validate_Abstract
{
    
    const DATE_FALSE_FORMAT  = 'falseFormat';
    const DATE_INVALID_TYPE = 'invalidType';
    const DATE_EMPTY = 'emptyDate';
    const DATE_INVALID    = 'invalidDate';
    const FORMAT_INVALID = 'Wrong format given';
    
        protected $_messageTemplates = array(
        self::DATE_INVALID_TYPE => "Invalid type given. String expected.",
        self::DATE_EMPTY => "Empty string given. Date string expected.",
        self::DATE_INVALID   => "'%value%' does not appear to be a valid date.",
        self::DATE_FALSE_FORMAT    => "'%value%' does not fit the date format '%format%'.",

    );

        
    protected $_parsedDate = array();    
    
    protected $_format = 'yyyy-mm-dd';
    protected $_value = null;
        
    public function __construct ($format = null)
    {     if(!empty($format)) 
        $this->setFormat($format);
    }
    
    	/** 
         * Specifies sequence       
         * @param string $format (ymd, dmy, yyyy-mm-dd, mm-dd-yyyy, YMD, MDY etc.)          
         */
    
    public function setFormat ($format)
    {     
        $this->_format = $format;        
        if(!$this->checkFormat())
        {
            throw new Exception(self::FORMAT_INVALID." '{$format}'");
        }
    }
    
    public function setValue ($value)
    {
        $filter = new Zend_Filter_StringTrim();
        $this->_value = $filter->filter($value);
    }
    
    	/**
         * Validate format
         * @return bool
         */
    
    private function checkFormat ()
    { 
        $empty  = true;    

        if(stripos($this->_format,'y') !== false) 
        {       
            $empty = false; 
            
if((strripos($this->_format,'y') - stripos($this->_format,'y')) > 3 || !$this->isValidEntry($this->_format,'y'))
                {
                     return false;
                }             
        }
        
        if(stripos($this->_format,'m') !== false)
        {
            $empty = false;
if((strripos($this->_format,'m') - stripos($this->_format,'m')) > 1 || !$this->isValidEntry($this->_format,'m'))
                {
                     return false;
                }             
        }
        
        if(stripos($this->_format,'d') !== false)
        {
            $empty = false;
if((strripos($this->_format,'d') - stripos($this->_format,'d')) > 1 || !$this->isValidEntry($this->_format,'d'))
                {
                     return false;
                }             
        }
        
        
        if($empty) {return false;}     

        
        return true;
           
    }
    
    	/**
         * Validate entry
         * @param string $str
         * @param string $char 
         * @return bool
         */
    
    private function isValidEntry($str, $char)
    {
        if(strlen($char) < 1 || !is_string($char)) return false;
        if($char === '') return false;
        $str = strtolower($str);
        for($i = strpos($str, $char); $i < strrpos($str, $char); $i++)
        {
         if($str[$i] !== $char)
         {
            return false;
         }   
        }
        return true;
    }
    
    	/**
         * @param string $date
         * @param string $format 
         * @return array
         */
    private function parseDate($date, $format)
    {
        $result = array();
        $date = preg_split('/\D+/', $date);
        
        $position = array();
        $position['year']  = stripos($format,'y');
        $position['month'] = stripos($format, 'm');
        $position['day']   = stripos($format, 'd');
        
        asort($position);
        $i = 0;
        foreach($position as $key => $val)
        {
            if(isset($date[$i]))
            {
                $result[$key] = (int)$date[$i];
            }
            $i++;
        }
        
        return $result;             
        
    }
    
    public function getParsedDate ()
    {
        return $this->_parsedDate;
    }
    
    public function isValid ($value)
    {
        $this->setValue($value);
        
        if(!is_string($this->_value))
        {
            $this->_error(self::DATE_INVALID_TYPE);
            return false;
        }
        
        if($this->_value === '')
        {
            $this->_error(self::DATE_EMPTY);
            return false;
        }
        
        
        $this->_parsedDate = $this->parseDate($this->_value, $this->_format);
        
        foreach($this->_parsedDate as $val)
        {
            if($val == 0)
            {
                $this->_error(self::DATE_INVALID);
                return false;
            }
        }
        
        if(!preg_match('/^\d{1,4}\D*\d{1,4}\D*\d{1,4}$/u', $this->_value))
        {
            $this->_error(self::DATE_FALSE_FORMAT);
            return false;
        }
        
        if(!checkdate($this->_parsedDate['month'], $this->_parsedDate['day'], $this->_parsedDate['year']))
        {
            $this->_error(self::DATE_INVALID);
            return false;
        }               
      
      $this->_value =  $this->_parsedDate['month'].'-'.$this->_parsedDate['day'].'-'.$this->_parsedDate['year'];
      
      return true;       
    }
    
    
}    