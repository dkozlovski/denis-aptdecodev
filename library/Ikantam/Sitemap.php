<?php

class Ikantam_Sitemap
{

    protected $_sitemap = "";
    protected $_baseUrl;
    protected $_basePath;

    public function __construct($baseUrl)
    {
        $this->_baseUrl = rtrim($baseUrl, '/');
        $this->_sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                . "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n"
                . "    <url>\n"
                . "        <loc>$baseUrl</loc>\n"
                . "    </url>\n"
                . "%content%"
                . "</urlset>"
        ;

        $this->_basePath = APPLICATION_PATH . '/../public/';
    }

    public function createSitemap($fileName = 'sitemap.xml')
    {
        file_put_contents($this->_basePath . $fileName, $this->getSitemap());
    }

    public function getSitemap()
    {
        return $this->_sitemap;
    }

    public function addUrls($urls)
    {
        $this->_sitemap = str_replace('%content%', $this->_addUrls($urls), $this->_sitemap);
    }

    protected function _addUrls($urls, $slice = "")
    {
        $template = "    <url>\n        <loc>%s</loc>\n    </url>\n";
        $out      = '';

        if (is_string($urls)) {
            $out .= sprintf($template, $this->_baseUrl . $slice . "/" . $urls);
        } elseif (is_array($urls)) {

            foreach ($urls as $k => $url) {
                if (is_string($k)) {
                    $out .= $this->_addUrls($url, $slice . "/" . $k);
                    $out .= sprintf($template, $this->_baseUrl . $slice . "/" . $k);
                } else {
                    $out .= $this->_addUrls($url, $slice);
                }
            }
        }

        return $out;
    }

}