<?php
class Ikantam_Maps_Geocoding_Request_Mapquest_NVP extends Ikantam_Service_Request_NVP
{
    /**
     * API key
     * This parameter is required.
     * @type string
     */
    public $key;
    
    /**
     * Specifies the format of the request. If this parameter is not supplied, 
     * the input format is assumed to be JSON-formatted text. The JSON-formatted 
     * input text must be supplied as either the "json" parameter of an HTTP GET, 
     * or as the BODY of an HTTP POST. If this parameter is "xml", 
     * the XML-formatted input text must be supplied as either the "xml" 
     * parameter of an HTTP GET, or as the BODY of an HTTP POST.  
     * Defaults to "json" when using POST; otherwise it determines which to use 
     * based on parameters given.
     */
    public $inFormat; 
    
    /**
     * This parameter, if present, should contain the JSON-formatted text of the request. 
     * Use this parameter if you want to submit your request in JSON format, but do not want 
     * to use an HTTP POST to submit body text. 
     */
     public $json;
     
    /**
     * This parameter, if present, should contain the XML-formatted text of the request. 
     * Use this parameter if you want to submit your request in XML format, but do not 
     * want to use an HTTP POST to submit body text. 
     */
     public $xml;
     
    /**
     * Specifies the format of the response. Must be one of the following, if supplied:
     *   json
     *   xml
     *   csv (character delimited)
     */
     public $outFormat = 'json';      

    /**
     * The number of results to limit the response to in the case of an ambiguous address. (-1 indicates no limit) 
     * 
     */
     public $maxResults;
     

    /**
     * This parameter tells the service whether it should return a URL to a static map thumbnail image for a location being geocoded.
     *   true - The response will include a URL to a static map image of the geocoded location.
     *   false - The response will not include a static map image URL. 
     */
     public $thumbMaps;
     

    /**
     * When using batch geocoding or when ambiguous results are returned, any results within the provided bounding box 
     * will be moved to the top of the results list. Bounding box format is: upper left latitude, upper left longitude, 
     * lower right latitude, lower right longitude. 
     *
     * Refer to the Geocode Options Sample below for examples of how to format this parameter. 
     * http://www.mapquestapi.com/geocoding/#optionssample
     */
     public $boundingBox;
     

    /**
     * This option tells the service whether it should fail when given a latitude/longitude pair in an address or batch geocode call, or if it should ignore that and try and geocode what it can.
     *   true - The geocoder will ignore the LatLng specified in the location, and use the address info to perform geocode
     *   false - The geocoder will return a geoaddress object containing the LatLng passed in and write a warning message to Info block
     *   Default = false (Applies to address and batch calls) 
     */
     public $ignoreLatLngInput; 
     

    /**
     * A delimiter is used only when outFormat=csv. The delimiter is the single character used to separate the fields of a character delimited file. The delimiter defaults to a comma(,). The valid choices are:
     * Comma (,) - All fields are quoted when comma is used.
     *  Pipe (|) - The fields are not quoted when pipes are used.
     *  Colon (:) - All fields are quoted when colon is used.
     *  Semicolon (;) - All fields are quoted when semicolon is used. 
     */
     public $delimiter;                    
   
                                   
}