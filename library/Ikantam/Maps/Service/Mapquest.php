<?php
class Ikantam_Maps_Service_Mapquest implements Ikantam_Maps_Service_Interface
{
    
    protected $_endpoints = array(
        'Geocoding' => 'http://www.mapquestapi.com/geocoding/v1/', // see http://www.mapquestapi.com/geocoding/
        'Directions' => 'http://www.mapquestapi.com/directions/v2/', // see http://www.mapquestapi.com/directions/
    );
    
    protected $_operations = array(
        'Geocoding' => array(
            'address',
            'reverse',
            'batch',            
        ),
        
        'Directions' => array(
            'route',
            'optimizedroute',
            'routematrix',
            'findlinkid',
        ), 
    );
    
    protected $_errors;
    
    protected $_config;
    
    public function __construct()
    {
        $this->_config = new Zend_Config_Xml(APPLICATION_PATH.'/configs/additional_settings.xml', 'mapquest');
    }
    
    /**
     * Last response
     * @type object Ikantam_Service_Response
     */
    protected $_response ;
    
    protected $_cache;
    
    public function __call ($method, $args)
    {
        $request = array_shift($args);
        if($request instanceof Ikantam_Service_Request){
            return $this->call($request, $method);
        }        
        throw new Exception("Invalid method " . get_class($this) . "::" . $method . "(" . print_r($args, 1) . ")");       
    }
        
    /**
     * Implementation of Ikantam_Maps_Service_Interface
     * @param object Ikantam_Service_Request $request
     * @param string $operation 
     * @return object - response
     */
    public function call (\Ikantam_Service_Request $request, $operation)
    {
        if(!$key = $this->_getRequestKey($request)) {
            throw new Ikantam_Maps_Service_Mapquest_Exception('Endpoint not found for this type of request');
        } elseif(!in_array($operation, $this->_operations[$key])) {
            throw new Ikantam_Maps_Service_Mapquest_Exception('Unknown operation "'.$key.'.'.$operation.'"'); 
        } 
        $requestUrl = $this->_endpoints[$key] . $operation . '?'; 
        
        $response = $this->_makeRequestFactory($requestUrl, $request);
         
         if($response->isEmpty()) {
             $this->_errors = array('Service unavailable.', 'Response is empty.');
             $this->_logError(9015, implode(' | ', $this->_errors)); 
                        
         } elseif($response->info['statuscode'] != 0){
             $this->_errors = $response->info['messages'];
             $this->_logError($response->info['statuscode'], implode(' | ', $this->_errors), $request);
         }
        
        return $response;       
    }
    
    /**
     * Returns last response
     *
     * @return object Ikantam_Service_Response 
     */
    public function getResponse ()
    {
        return $this->_response;
    }
    
    /**
     * Calls request method based on request type and wrap response into response object based on out format 
     * 
     * @param  $url
     * @param  Ikantam_Service_Request $request
     * @return object Ikantam_Service_Response
     */
    protected function _makeRequestFactory($url, \Ikantam_Service_Request $request)
    {
        $requsetType = $this->_getRequestType($request);
        $responseType = strtoupper($request->outFormat);
        $requestMethod = '_request'.$requsetType;
        
        if(!method_exists($this, $requestMethod)){
            throw new Ikantam_Maps_Service_Mapquest_Exception('Request method not found for type "'.$requsetType.'"');  
        } else{ 
             $this->_authorizeRequest($request);
             $responseClass = 'Ikantam_Maps_'.$this->_getRequestKey($request).'_Response_Mapquest_'.$responseType;
             $response = new $responseClass($this->{$requestMethod}($request, $url));
        }
        
        return $response;    
    }
    
    /**
     * Add credentials to request object
     * @param object Ikantam_Service_Request $request  
     * @return object Ikantam_Service_Request
     */
    protected function _authorizeRequest(\Ikantam_Service_Request $request)
    {   
        if(!isset($request->key)) {
            $request->key = urldecode($this->_config->appKey);
        } 
        return $request; 
    }
    
    /**
     * Determine request key based on class name
     * 
     * @param  object Ikantam_Service_Request
     * @return mixed string | bool(false if not found)
     */
    protected function _getRequestKey(\Ikantam_Service_Request $request)
    {
        if(isset($this->_cache[__method__])){
            return $this->_cache[__method__];
        }
        $classNameParts = explode('_', get_class($request));
        $endpointsKeys = array_keys($this->_endpoints); 
        foreach($classNameParts as $part) {
            if(in_array($part, $endpointsKeys)) {
                $this->_cache[__method__] = $part;
                return $part;
            }
        }
        return false;
    }
    
    /**
     * Return request type based on class name
     * 
     * @param object Ikantam_Service_Request  
     * @return mixed string
     */
    protected function _getRequestType(\Ikantam_Service_Request $request)
    {
        $classNameParts = explode('_', get_class($request));
        return array_pop($classNameParts);        
    }
    
    /**
     * Make request from NVP request object
     * @param  \Ikantam_Service_Request $request
     * @param  string $url
     * @return string response
     */
    protected function _requestNVP(\Ikantam_Service_Request $request, $url)
    {
        return @file_get_contents($url.$request);    
    }      
    
	/**
	 * Log sirvice errors
	 * 
	 * @param string $code  - service response error code
     * @param string $message - error message
	 * @param object self
	 */
	protected function _logError($code, $message, $request = null)
	{
	   $title = ($code == 9015) ? 'Unable to call service ' : 'Mapquest response with error code ';
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/log/mapquest.log');
		$logger->addWriter($writer);
		$logger->log($title.$code.': '.$message, Zend_Log::ERR);
        if($request) {
            unset($request->key);
            $logger->log(print_r(array_filter((array)$request), true), Zend_Log::INFO);
        }
        return $this;
	}    
}