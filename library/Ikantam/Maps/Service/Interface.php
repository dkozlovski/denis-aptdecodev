<?php
interface Ikantam_Maps_Service_Interface
{
    /**
     * Do request to service API
     * @param  object Ikantam_Service_Request $requestObject
     * @param  string $operation
     * @return object Ikantam_Service_Response
     */
    public function call (\Ikantam_Service_Request $requestObject, $operation);

}