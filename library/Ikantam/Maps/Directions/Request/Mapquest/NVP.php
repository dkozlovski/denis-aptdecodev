<?php
class Ikantam_Maps_Directions_Request_Mapquest_NVP extends Ikantam_Service_Request_NVP
{
    /**
     * API key
     * This parameter is required.
     * @type string
     */
    public $key;
    
    /**
     * Specifies the format of the request. If this parameter is not supplied, 
     * the input format is assumed to be JSON-formatted text. The JSON-formatted 
     * input text must be supplied as either the "json" parameter of an HTTP GET, 
     * or as the BODY of an HTTP POST. If this parameter is "xml", 
     * the XML-formatted input text must be supplied as either the "xml" 
     * parameter of an HTTP GET, or as the BODY of an HTTP POST.  
     * Defaults to "json" when using POST; otherwise it determines which to use 
     * based on parameters given.
     */
    public $inFormat; 
    
    /**
     * This parameter, if present, should contain the JSON-formatted text of the request. 
     * Use this parameter if you want to submit your request in JSON format, but do not want 
     * to use an HTTP POST to submit body text. 
     */
     public $json;
     
    /**
     * This parameter, if present, should contain the XML-formatted text of the request. 
     * Use this parameter if you want to submit your request in XML format, but do not 
     * want to use an HTTP POST to submit body text. 
     */
     public $xml;
     
    /**
     * Specifies the format of the response. Must be one of the following, if supplied:
     *   json
     *   xml
     *   csv (character delimited)
     */
     public $outFormat = 'json';      

    /**
     * When the input format is Key/Value pairs, the starting location of a Route Request. 
     * Exactly one "from" parameter is allowed. This is used for single-line addresses only.
     * http://www.mapquestapi.com/common/locations.html#singlelinelocations
     */ 
    public $from;
    
    /**
     * When the input format is Key/Value pairs, the ending location(s) of a Route Request. 
     * More than one "to" parameter may be supplied. This is used for single-line addresses only.
     * http://www.mapquestapi.com/common/locations.html#singlelinelocations
     */ 
    public $to;                    

    /**
     * Specifies the type of units to use when calculating distance. Acceptable values are:
     *  m - Miles
     *  k - Kilometers
     */ 
    public  $unit = 'm';
    
    /**
     *   Specifies the type of route wanted. Acceptable values are:
     *   fastest - Quickest drive time route.
     *   shortest - Shortest driving distance route.
     *   pedestrian - Walking route; Avoids limited access roads; Ignores turn restrictions.
     *   multimodal - Combination of walking and (if available) Public Transit.
     *   bicycle - Will only use roads on which bicycling is appropriate.
     */ 
    public $routeType = 'fastest';
    
    /**
     * Avoid Timed Conditions Option. Avoids using timed conditions such as Timed Turn Restrictions, Timed Access Roads, HOV Roads, and Seasonal Closures. Please Note: This flag should NOT be used if the Date/Time Routing Options are used.
     *   false - No penalties will be set and the timed conditions can be utilized when generating a route.
     *   true - Penalties will be set and the timed conditions will be avoided when generating a route
     */ 
    public  $avoidTimedConditions = 'false';
    
    /**
     * Specifies the type of narrative to generate.
     *   none - No narrative is generated
     *   text - Standard text narrative
     *   html - Adds some HTML tags to the standard text
     *   microformat - Uses HTML span tags with class attributes to allow parts of the narrative to be easily styled via CSS. 
     *    Explanation http://www.mapquestapi.com/directions/narrative_microformat.html
     * Default = 'text'
     */ 
    public $narrativeType;
    
    /**
     * Enhanced Narrative Option. This option will generate an enhanced narrative for Route & Alternate Route Services. 
     * This will encompass Intersection Counts, Previous Intersection, and Next Intersection/Gone Too Far advice.
     *   false - No intersection counts, previous intersection, or Next Intersection/Gone Too Far advice will be displayed.
     *   true - Intersection counts, previous intersection, and Next Intersection/Gone Too Far advice can be displayed when available.
     *   Default = 'false'        
     *   Note: In order to view the proper maneuver note styles, the narrativeType needs to be set to 'microformat'. 
     *  Microformat uses HTML span tags with class attributes to allow parts of the narrative to be easily styled via CSS. 
     *      Read more http://www.mapquestapi.com/directions/narrative_microformat.html
     */ 
    public $enhancedNarrative;
    
    /**
     * The maximum number of Link IDs to return for each maneuver. If zero, no Link ID data is returned.
     * Default - 0
     */ 
    public $maxLinkId;
    
    /**
     * Examples of commonly used locale parameter values. Input can be any supported ISO 639-1 code.
     *
     *   English(US) = en_US(default)
     *   English(Great Britain) = en_GB
     *   French(Canada) = fr_CA
     *   French(France) = fr_FR
     *   Germany(Germany) = de_DE
     *   Spanish(Spain) = es_ES
     *   Spanish(Mexico) = es_MX
     *   Russian(Russia) = ru_RU
     */ 
    public $locale = 'en_US';        
    
    /**
     * Attribute flags of roads to try to avoid. The available attribute flags depend on the data set. This does not guarantee roads with these attributes will be avoided if alternate route paths are too lengthy or not possible or roads that contain these attributes are very short.
     *   
     *   Available choices:
     *   Limited Access - Highways
     *   Toll Road
     *   Ferry
     *   Unpaved
     *   Seasonal Closure - Approximate. Seasonal roads may not be selected with 100% accuracy.
     *  Country Crossing
     */ 
    public $avoids;
    
    /**
     * Link IDs of roads to absolutely avoid. May cause some routes to fail. 
     * Multiple Link IDs should be comma-separated.
     */ 
    public $mustAvoidLinkIds;
    
    /**
     * Link IDs of roads to try to avoid during route calculation. 
     * Does not guarantee these roads will be avoided if alternate route paths are too lengthy or not possible. 
     * Multiple Link IDs should be comma-separated.
     */ 
    public $tryAvoidLinkIds;
    
    /**
     * State boundary display option.
     *   true - State boundary crossings will be displayed in narrative.
     *   false - State boundary crossings will not be displayed in narrative.
     */ 
    public $stateBoundaryDisplay;  
    
    /**
     * Country boundary display option
     *   true - Country boundary crossings are displayed in narrative.
     *   false - Country boundary crossings are not displayed in narrative.
     */ 
    public $countryBoundaryDisplay;  
    
    /**
     * Side of street display option
     *   true - Side of street is displayed in narrative.
     *   false - Side of street is not displayed in narrativ
     */ 
    public $sideOfStreetDisplay; 
    
    /**
     * The "End at" destination maneuver display option.
     *   true - the "End at" destination maneuver is displayed in narrative.
     *   false - the "End at" destination maneuver is not displayed in narrative.
     */ 
    public $destinationManeuverDisplay;
    
    /**
     * To return a route shape without a mapState. If the generalize parameter is also set to 0, then all shapePoints will be returned. 
     *  If generalize parameter is set to > 0, then fewer shapePoints will be returned. Please also see the generalize description below. Note that if the request is for a long route, 
     *  you should use a compressed shapeFormat to decrease the response size.
     *  true - Shape points will be displayed.
     *  false - Requires a mapState in the request.
     * 
     *  Default = false
     */ 
    public $fullShape; 
    
    /**
     * This option applies to both input and output (raw, cmp, cmp6) and overrides inShapeFormat and outShapeFormat. 
     *   Shape format options.
     *   raw - shape is represented as float pairs.
     *   cmp - shape is represented as a compressed path string with 5 digits of precision.
     *   cmp6 - Same as for cmp, but uses 6 digits of precision.
     *   See the Compressed Lat/Lng description page(http://www.mapquestapi.com/common/encodedecode.html) 
     *      for more detail, including sample source code and an interactive encoder/decoder.
     */ 
    public $shapeFormat;
    
    /**
     * Input shape format options.
     *   raw - shape is represented as float pairs.
     *   cmp - shape is represented as a compressed path string with 5 digits of precision.
     *   cmp6 - Same as for cmp, but uses 6 digits of precision.
     *   See the Compressed Lat/Lng description page(http://www.mapquestapi.com/common/encodedecode.html) 
     *    for more detail, including sample source code and an interactive encoder/decoder.
     */ 
    public $inShapeFormat;
    
    /**
     * Output shape format options.
     *   raw - shape is represented as float pairs.
     *   cmp - shape is represented as a compressed path string with 5 digits of precision.
     *   cmp6 - Same as for cmp, but uses 6 digits of precision.
     *   See the Compressed Lat/Lng description page(http://www.mapquestapi.com/common/encodedecode.html) 
     *    for more detail, including sample source code and an interactive encoder/decoder.
     */ 
    public $outShapeFormat; 
    
    /**
     * If mapState is provided, this option will be ignored. 
     *  If no mapState is provided, this parameter will be used to reduce the number of points returned in the shapePoints object.
     * If the generalize parameter is 0, then no shape simplification will be done and all shape points will be returned.        
     * If the generalize parameter is > 0, it will be used as the tolerance distance (in meters) 
     * in the Douglas-Peucker Algorithm (http://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm)
     *  for line simplification.        
     * Higher values of generalize will result in fewer points in the final route shape.
     */ 
    public $generalize; 
    
    /**
     * Sets the cycling road favoring factor. A value of < 1 favors cycling on non-bike lane roads. 
     * Values are clamped to the range 0.1 to 100.0.
     *  
     * Default = 1.0
     */ 
    public $cyclingRoadFactor;
    
    /**
     * Specifies the road grade avoidance strategies to be used for each leg. This parameter is only for bicycle routes.
     *  DEFAULT_STRATEGY - No road grade strategy will be used.
     *  AVOID_UP_HILL - Avoid up hill road grades.
     *  AVOID_DOWN_HILL - Avoid down hill road grades.
     *  AVOID_ALL_HILLS - Avoid all hill road grades.
     *  FAVOR_UP_HILL - Favor up hill road grades.
     *  FAVOR_DOWN_HILL - Favor down hill road grades.
     *  FAVOR_ALL_HILLS - Favor all hill road grades.
     *  Default = 'DEFAULT_STRATEGY'
     */ 
    public $roadGradeStrategy; 
    
    /**
     * Driving style to be used when calculating fuel usage.
     *  1 or cautious - Assume a cautious driving style.
     *  2 or normal - Assume a normal driving style. This is the default.
     *  3 or aggressive - Assume an aggressive driving style.
     */ 
    public $drivingStyle = '2'; 
    
    /**
     * Maneuver maps display option.
     *  true - A small staticmap is displayed per maneuver with the route shape of that maneuver. The route response will return a mapUrl.
     *  See mapUrl in the Route Response section for a detailed description.
     *  false - A small staticmap is not displayed per maneuver.     * 
     */ 
    public $manMaps = 'false';
    
    /**
     * This is the speed (in miles per hour as default and it does adhere to the unit specifier) allowed for pedestrian routeTypes 
     * (or walking portions of a multimodal route). This is used for computing expected route times for walking routes. It is also used when computing multimodal routes. Since multimodal routes use date/time and schedule based departures and arrivals from transit stops, the speed which one walks can alter the suggested route. 
     * The default is 2.5 miles per hour.
     */ 
    public $walkingSpeed;                      
                                   
}