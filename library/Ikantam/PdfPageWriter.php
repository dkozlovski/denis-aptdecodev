<?php

class Ikantam_PdfPageWriter
{

    protected $_page;
    protected $_font;
    protected $_defaultFont = 'Times-Roman';
    
    public function __construct ($page = null)    
    {
        if($pdf instanceof Zend_Pdf_Page)
        {
            $this->_page = $page;
        } else
            {
                $this->_page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);  
            }    


     $this->setFont();     
    }
    
    	/**
         * @param string $name 
         * @return self
         */
    public function setFont ($name = null)
    {
      $fn = ($name)? $name : $this->_defaultFont;
      $this->_font = Zend_Pdf_Font::fontWithName($fn);
      return $this;
    }
    
    public function getFont ()
    {
        return $this->_font;
    }
    
    public function getDefaultFont ()
    {
        return $this->_defaultFont;
    }
    
    	/**
         * @param string $text
         * @param float $fontSize - size in points
         * @param float $x
         * @param float $y
         * @param string $fontName - optional font name 
         * @return self
         */
    public function write ($text, $fontSize, $x, $y, $fontName = null, $style = null) 
    {
        if(!$style instanceof Zend_Pdf_Style)
        {
        $style = new Zend_Pdf_Style();
        $style->setFillColor(new Zend_Pdf_Color_Html('#000000'));          
        }
      if($fontName)
      {
        $this->setFont($fontName);
      } 
      $this->_page->setStyle($style); 
      $this->_page->setFont($this->_font, $fontSize)
                  ->drawText($text, $x, $this->convertY($y), 'UTF-8');
     return $this;

    }
    
    	/**
         * This method write multi strings text to pdf page
         * strings must be separated by \n symbol
         * @param string $text
         * @param float $fontSize
         * @param float $x
         * @param float $y
         * @param string $fontName - optional
         * @return self
         */
    public function writeStrings($text, $fontSize, $x, $y, $fontName = null, $style = null)
    {
        $top = 0;
        $strings = array_filter(explode("\n", $text));
        foreach($strings as $string)
        {
            $this->write($string, $fontSize, $x, $y + $top, $fontName, $style);
            $top+= ceil($fontSize/10) + $fontSize;
        } 
        return $this;
    }
    
    	/**
         * Calculate string width
         * @param string $string
         * @param Zend_Pdf_Font $font
         * @param float $fontSize
         * @return int
         */    
         
    public function widthForStringUsingFontSize($string, $font, $fontSize)
    {
        $drawingString = '"libiconv"' == ICONV_IMPL ?
            iconv('UTF-8', 'UTF-16BE//IGNORE', $string) :
            @iconv('UTF-8', 'UTF-16BE', $string);

        $characters = array();
        for ($i = 0; $i < strlen($drawingString); $i++) {
            $characters[] = (ord($drawingString[$i++]) << 8) | ord($drawingString[$i]);
        }
        $glyphs = $font->glyphNumbersForCharacters($characters);
        $widths = $font->widthsForGlyphs($glyphs);
        $stringWidth = (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
        return $stringWidth;

    }
     	/**
          * @param float $x
          * @param float $y
          * @param float $x1
          * @param float $y1
          * @param Zend_Pdf_Style $style
          * @return self;
          */
    public function drawRectangle ($x, $y, $x2, $y2, $fillType = Zend_Pdf_Page::FILL_METHOD_NON_ZERO_WINDING, $style = null)
    {
        if(!$style instanceof Zend_Pdf_Style)
        {
        $style = new Zend_Pdf_Style();
        $style->setLineColor(new Zend_Pdf_Color_Html('#000000'));
        $style->setLineWidth(0.5);           
        }
        $this->_page->setStyle($style)
             ->drawRectangle($x, $this->convertY($y), $x2, $this->convertY($y2), $fillType);   
        return $this;
    }
    
     	/**
          * @param float $x
          * @param float $y
          * @param float $x1
          * @param float $y1
          * @param Zend_Pdf_Style $style
          * @return self;
          */
    public function drawLine ($x, $y, $x2, $y2, $style = null, $htmlColor = null)
    {
        if(!$style instanceof Zend_Pdf_Style)
        {
        $style = new Zend_Pdf_Style();
        $style->setLineWidth(0.5);                    
        }
        
        if($htmlColor)
        {
            $style->setLineColor(new Zend_Pdf_Color_Html($htmlColor));
        }
        
        $this->_page->setStyle($style)
             ->drawLine($x, $this->convertY($y), $x2, $this->convertY($y2));   
        return $this;        
    
    }
    
    protected function convertY($y) // from post script geometry to std
    {

        if(!$this->_page){
            throw new Exception('Page not defined.');
        }
        return $this->_page->getHeight() - $y;     
        
    }
    
    public function getPage ()
    {
        return $this->_page;
    }
    
    public function attach ($pdf)
    {
        if($pdf instanceof Zend_Pdf)
        {
            $pdf->pages[] = $this->_page;
        } else
            {
                throw new Exception('Page can be attached only to Zend_Pdf instance.');
            }
     return $this;   
    }
}