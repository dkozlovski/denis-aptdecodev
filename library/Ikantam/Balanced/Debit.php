<?php

class Ikantam_Balanced_Debit extends Ikantam_Object
{

	protected function getClient($buyer)
	{
		$client = new Zend_Http_Client();
		$client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
		$client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $buyer->getDebitsUri());
		return $client;
	}

	public function __construct($data, $buyer, $marketplace)
	{

		$client = $this->getClient($buyer);

		foreach ($data as $key => $value) {
			$client->setParameterPost($key, $value);
		}

		$response = $client->request('POST');

		if ($response->isError()) {
			$body = Zend_Json_Decoder::decode($response->getBody());

			$error = 'Your card is declined.';
			if (isset($body['category_code'])) {
				if ($body['category_code'] == 'card-declined') {
					$error = 'Your card has insufficient funds or is cancelled.';
				}
			}

			throw new Ikantam_Balanced_Exception($error);
		} elseif ($response->isSuccessful()) {
			$body = Zend_Json_Decoder::decode($response->getBody());
			$this->addData($body);
		}
	}

}