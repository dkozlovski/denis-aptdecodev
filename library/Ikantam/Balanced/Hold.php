<?php

class Ikantam_Balanced_Hold extends Ikantam_Object
{

    public function capture($description = null, $appearsOnStatementAs = null, $onBehalfOfUri = null, $sourceUri = null)
    {
        $client = $this->getClient($this->getAccount()->getDebitsUrl());

        $client->setParameterPost('hold_uri', $this->getUri());
        $client->setParameterPost('description', $description);
        $client->setParameterPost('appears_on_statement_as', $appearsOnStatementAs);
        $client->setParameterPost('on_behalf_of_uri', $onBehalfOfUri);
        $client->setParameterPost('source_uri', $sourceUri); //optional string. URI of a specific bank account or card to be debited.

        try {
            $response = $client->request('POST');

            if ($response->isError()) {
                //var_Dump($response);
                die();
            } elseif ($response->isSuccessful()) {
                $data = Zend_Json_Decoder::decode($response->getBody());
                $debit = new Ikantam_Balanced_Debit();
                return $debit->addData($data);
            }
        } catch (Exception $e) {
            //var_Dump($e);
            die();
        }
    }

    public function void($appearsOnStatementAs = null, $isVoid = true)
    {
        $client = $this->getClient($this->getUri());

        $client->setParameterPost('is_void', $isVoid);
        $client->setParameterPost('appears_on_statement_as', $appearsOnStatementAs);

        try {
            $response = $client->request('PUT');

            if ($response->isError()) {
                //var_Dump($response);
                die();
            } elseif ($response->isSuccessful()) {
                $data = Zend_Json_Decoder::decode($response->getBody());
                return $this->addData($data);
            }
        } catch (Exception $e) {
            //var_Dump($e);
            die();
        }
    }

    protected function getAccount()
    {
        return new Ikantam_Balanced_Account();
    }

}