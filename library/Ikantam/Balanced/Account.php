<?php

class Ikantam_Balanced_Account extends Ikantam_Balanced_Abstract
{

    protected function getClient($uri)
    {
        $client = new Zend_Http_Client();
        $client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $uri);
        return $client;
    }

    public function createdHold($amount, $description = null, $appearsOnStatementAs = null)
    {
        $client = $this->getClient($this->getHoldsUrl());

        $client->setParameterPost('amount', $amount);
        $client->setParameterPost('description', $description);
        $client->setParameterPost('appears_on_statement_as', $appearsOnStatementAs);

        try {
            $response = $client->request('POST');

            if ($response->isError()) {
                $this->_logResponse($response, $this->getHoldsUrl());
                //var_Dump($response);
                throw new Ikantam_Balanced_Exception('Cannot process you payment.');
                //die();
            } elseif ($response->isSuccessful()) {
                $data = Zend_Json_Decoder::decode($response->getBody());
                $hold = new Ikantam_Balanced_Hold();
                return $hold->addData($data);
            }
        } catch (Exception $e) {
             $this->logException($e);
            //var_Dump($e);
            throw new Ikantam_Balanced_Exception('Cannot process you payment.');
                //die();
        }
    }

}
