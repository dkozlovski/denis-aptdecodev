<?php

class Ikantam_Exception_Redirect extends Exception{}
class Ikantam_Exception_DuplicateEmail extends Exception{}

class Ikantam_Balanced_Merchant extends Ikantam_Object
{

    protected function getClient($marketplace)
    {
        $client = new Zend_Http_Client();
        $client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $marketplace->getAccountsUri());
        return $client;
    }

    protected function isSuccessful($code)
    {
        $restype = floor($code / 100);
        if ($restype == 2 || $restype == 1) { // Shouldn't 3xx count as success as well ???
            return true;
        }

        return false;
    }

    public function isError($code)
    {
        $restype = floor($code / 100);
        if ($restype == 4 || $restype == 5) {
            return true;
        }

        return false;
    }

    public function isRedirect($code)
    {
        $restype = floor($code / 100);
        if ($restype == 3) {
            return true;
        }

        return false;
    }

    public function __construct($data, $marketplace)
    {
        $url = Ikantam_Balanced_Settings::getApiEndpoint() . $marketplace->getAccountsUri();
        $login = Ikantam_Balanced_Settings::getApiKey();

        $ch = curl_init();

        // установка URL и других необходимых параметров
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $login . ':');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // завершение сеанса и освобождение ресурсов
        curl_close($ch);

        if ($this->isSuccessful($httpCode)) {
            $body = Zend_Json::decode($res);
            $this->addData($body);
        } elseif ($this->isError($httpCode)) {
            $body = Zend_Json::decode($res);
            if (isset($body['category_code'], $body['status']) && $body['category_code'] == 'duplicate-email-address' && $body['status'] = 'Conflict') {
                throw new Ikantam_Exception_DuplicateEmail('Account with this email address already exists.');
            }
            //var_dump($res);
            //die();
        } elseif ($this->isRedirect($httpCode)) {
            $body = Zend_Json::decode($res);
            
            if (isset($body['redirect_uri'])) {
                throw new Ikantam_Exception_Redirect($body['redirect_uri']);
            }
        }

        /*
          $client = $this->getClient($marketplace);

          foreach ($data as $key => $value) {
          $client->setParameterPost($key, $value);
          }

          $response = $client->request('POST');

          if ($response->isError()) {
          //echo 'create merchant';
          //var_Dump($response);
          //die();
          } elseif ($response->isSuccessful()) {
          $body = Zend_Json_Decoder::decode($response->getBody());
          $this->addData($body);
          }
         */
    }

    public function createDebit($data, $marketplace)
    {
        $debit = new Ikantam_Balanced_Debit($data, $this, $marketplace);
        return $debit;
    }

}