<?php

/**
 * @method int getInEscrow() Description
 * @method array getOwnerAccount() Description
 * @method string getCallbacksUri() Description
 * @method type getDomainUrl() Description
 * @method type getName() Description
 * @method type getTransactionsUri() Description
 * @method type getSupportEmailAddress() Description
 * @method type getUri() Description
 * @method type getEventsUri() Description
 * @method type getAccountsUri() Description
 * @method type getSupportPhoneNumber() Description
 * @method type getRefundsUri() Description
 * @method array getMeta() Description
 * @method type getDebitsUri() Description
 * @method type getHoldsUri() Description
 * @method type getBankAccountsUri() Description
 * @method type getId() Description
 * @method type getCreditsUri() Description
 * @method type getCardsUri() Description
 */
class Ikantam_Balanced_Marketplace extends Ikantam_Balanced_Abstract
{

    public function __construct()
    {
        $this->getOne();
    }

    protected function getClient()
    {
        $client = new Zend_Http_Client();
        $client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . '/v1/marketplaces?limit=2&offset=0');

        return $client;
    }

    public function getOne()
    {
        $client = $this->getClient();

        try {
            $response = $client->request('get');

            if ($response->isSuccessful()) {
                $body = Zend_Json_Decoder::decode($response->getBody());
                if (isset($body['items']) && is_array($body['items'])) {
                    $this->setData(current($body['items']));
                } else {
                    //@TODO: error
                }
            } elseif ($response->isRedirect()) {
                //@TODO: error
            } elseif ($response->isError()) {
                //@TODO: error
            } else {
                //@TODO: error
            }
        } catch (Exception $e) {
            //@TODO: error
        }
    }

    public function createCard($data)
    {
        $card = new Ikantam_Balanced_Card($data, $this);

        return $card;
    }

    public function createBuyer($data, $cardUri)
    {
        $buyer = new Ikantam_Balanced_Buyer($data, $cardUri, $this);

        return $buyer;
    }

    public function createMerchant($data)
    {
        $buyer = new Ikantam_Balanced_Merchant($data, $this);
        return $buyer;
    }

    public function createBankAccount($data)
    {
        $buyer = new Ikantam_Balanced_BankAccount($data, $this);
        return $buyer;
    }

    public function getBankAccount($uri)
    {
        $bankAccount = new Ikantam_Balanced_BankAccount();
        return $bankAccount->getOne($uri, $this);
    }

    public function getOneBankAccount($basUri)
    {
        $client = new Zend_Http_Client();
        $client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $basUri);

        $response = $client->request('GET');

        if ($response->isError()) {
            //error
        } elseif ($response->isSuccessful()) {
            $body = Zend_Json_Decoder::decode($response->getBody());
            return $body['items'][0];
        }
    }

    public function addBankAccount($merchantUri, $bankAccountUri)
    {
        $client = $this->getClient();
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $merchantUri);

        $client->setParameterPost('bank_account_uri', $bankAccountUri);
        $response = $client->request('PUT');

        if ($response->isError()) {
            //error
        } elseif ($response->isSuccessful()) {
            $body = Zend_Json_Decoder::decode($response->getBody());
            //$this->addData($body);
        }

        return $this;
    }

    public function createCredit($baUri, $amount)
    {
        return new Ikantam_Balanced_Credit($baUri, $amount);
    }

    public function getCredit($creditUri)
    {
        $client = new Zend_Http_Client();
        $client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);

        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $creditUri);

        $credit = new Ikantam_Object();

        $response = $client->request('GET');

        if ($response->isError()) {
            $body = Zend_Json_Decoder::decode($response->getBody());
            var_dump($body);
            die();
            //throw new Ikantam_Balanced_Exception('Could not retrieve this credit');
        } elseif ($response->isSuccessful()) {
            $body = Zend_Json_Decoder::decode($response->getBody());
            $credit->addData($body);
        }
        return $credit;
    }

    public function captureHold($holdUri, $debitsUri)
    {
        $client = $this->_getClient($debitsUri);

        $client->setParameterPost('hold_uri', $holdUri);

        return $this->_doRequest($client, 'POST');
    }

    public function voidHold($holdUri)
    {
        $client = $this->_getClient($holdUri);

        $client->setParameterPost('is_void', true);

        return $this->_doRequest($client, 'PUT');
    }

    protected function _getClient($uri)
    {
        $client = new Zend_Http_Client();
        $client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $uri);
        return $client;
    }

    protected function _doRequest(Zend_Http_Client $client, $method)
    {
        try {
            $response = $client->request($method);

            if ($response->isError()) {
                $this->_logResponse($response, $client->getUri(true));
                return false;
            } elseif ($response->isSuccessful()) {
                return true;
            }
        } catch (Exception $e) {
            $this->_logException($e);
            return false;
        }
    }

    protected function _logException(Exception $exception)
    {
        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/log/balanced.log');
        $logger = new Zend_Log($writer);
        $logger->log($exception, Zend_Log::CRIT);
    }

}
