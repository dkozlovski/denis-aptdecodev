<?php

class Ikantam_Balanced_Settings
{
	protected static $_apiEndpoint = 'https://api.balancedpayments.com';
	protected static $_apiKey = 'd1b7d472913211e2b57c026ba7cd33d0';
    protected static $_ownerUri = '/v1/bank_accounts/BA6outRgrIVrzItl1zBkFfm0';
    protected static $_marketplaceUri = '/v1/marketplaces/MP2LFT0iONeCOatZtqI0cSVk';

	public static function getApiEndpoint()
	{
        return Zend_Registry::get('config')->balancedPayments->params->apiEndpoint;
	}
	
	public static function getApiKey()
	{
		return Zend_Registry::get('config')->balancedPayments->params->apiKey;
	}
    
    public static function getOwnerUri()
	{
		return Zend_Registry::get('config')->balancedPayments->params->ownerUri;
	}
    
    public static function getMarketplaceUri()
	{
		return Zend_Registry::get('config')->balancedPayments->params->marketplaceUri;
	}
    
    public static function getOnBehalfOfUri()
	{
		return Zend_Registry::get('config')->balancedPayments->params->onBehalfOfUri;
	}
    
}


