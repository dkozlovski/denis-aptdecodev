<?php

class Ikantam_Balanced_Buyer extends Ikantam_Balanced_Abstract
{
	protected function getClient($marketplace)
	{
		$client = new Zend_Http_Client();
		$client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
		$client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $marketplace->getAccountsUri());
		return $client;
	}
	
	
	public function __construct($data, $cardUri, $marketplace)
	{

			$client = $this->getClient($marketplace);

			foreach ($data as $key => $value) {
				$client->setParameterPost($key, $value);
			}
			
			$client->setParameterPost('card_uri', $cardUri);
			
			$response = $client->request('POST');

			if ($response->isError()) {
                
                $this->_logResponse($response, $this->getHoldsUrl());
				//var_Dump($response);
				throw new Ikantam_Balanced_Exception('Cannot process you payment.');
                //die();
			} elseif ($response->isSuccessful()) {
				$body = Zend_Json_Decoder::decode($response->getBody());
				$this->addData($body);
			}

	}
	
	
	public function createDebit($data, $marketplace)
	{
		$debit = new Ikantam_Balanced_Debit($data, $this, $marketplace);
		return $debit;
	}
    
    public function createdHold($amount, $description = null, $appearsOnStatementAs = null)
    {
        $client = new Zend_Http_Client();
		$client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
		$client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $this->getHoldsUri());

        $client->setParameterPost('amount', $amount);
        $client->setParameterPost('description', $description);
        $client->setParameterPost('appears_on_statement_as', $appearsOnStatementAs);

        try {
            $response = $client->request('POST');

            if ($response->isError()) {
                $this->_logResponse($response, $this->getHoldsUrl());
                //var_Dump($response);
                throw new Ikantam_Balanced_Exception('Opps - looks like your card is declined. Please try a different card or contact your bank for more information.');
                //die();
            } elseif ($response->isSuccessful()) {
                $data = Zend_Json_Decoder::decode($response->getBody());
                $hold = new Ikantam_Balanced_Hold();
                return $hold->addData($data);
            }
        } catch (Exception $e) {
            $this->logException($e);
            //var_Dump($e);
            throw new Ikantam_Balanced_Exception('Opps - looks like your card is declined. Please try a different card or contact your bank for more information.');
            //die();
        }
    }
	
}
