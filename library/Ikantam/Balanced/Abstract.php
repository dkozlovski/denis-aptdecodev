<?php

class Ikantam_Balanced_Abstract extends Ikantam_Object
{

	public function logException(Exception $exception)
	{
		$error = 'Application error: ' . $exception->getMessage();

		$log = $this->getLog();

		if ($log) {
			$log->crit($error);
			$log->debug($exception);
		}
	}

	protected function getLog()
	{
		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');

		if (!$bootstrap->hasResource('Log')) {
			return false;
		}

		$log = $bootstrap->getResource('Log');

		return $log;
	}
    
     protected function _logResponse(Zend_Http_Response $response, $uri)
    {
        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/log/balanced.log');
        $logger = new Zend_Log($writer);
        $logger->log($response, Zend_Log::ERR);
        $logger->log($uri, Zend_Log::ERR);
    }

}