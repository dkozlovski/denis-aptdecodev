<?php

class Ikantam_Balanced_BankAccount extends Ikantam_Object
{

    protected function getClient($marketplace)
    {
        $client = new Zend_Http_Client();
        $client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $marketplace->getBankAccountsUri());
        return $client;
    }

    public function getOne($uri, $marketplace)
    {
        $client = $this->getClient($marketplace);
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $uri);

        
		$response = $client->request('GET');

		if ($response->isError()) {
			//error
		} elseif ($response->isSuccessful()) {
			$body = Zend_Json_Decoder::decode($response->getBody());
			$this->addData($body);
		}

		return $this;
    }

    public function __construct($data = null, $marketplace = null)
    {
        if ($data && $marketplace) {
            $client = $this->getClient($marketplace);

            foreach ($data as $key => $value) {
                $client->setParameterPost($key, $value);
            }

            $response = $client->request('POST');

            if ($response->isError()) {
                //echo 'create bank account';
                //var_Dump($response);
                //die();
            } elseif ($response->isSuccessful()) {
                $body = Zend_Json_Decoder::decode($response->getBody());
                $this->addData($body);
            }
        }
    }

}