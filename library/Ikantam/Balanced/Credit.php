<?php

class Ikantam_Balanced_Credit extends Ikantam_Object
{

	protected function getClient()
	{
		$client = new Zend_Http_Client();
		$client->setAuth(Ikantam_Balanced_Settings::getApiKey(), '', Zend_Http_Client::AUTH_BASIC);
		return $client;
	}

	public function __construct($baUri, $amount)
	{

		$client = $this->getClient();
        $client->setUri(Ikantam_Balanced_Settings::getApiEndpoint() . $baUri . '/credits');
        $client->setParameterPost('amount', (int) $amount);
	

		$response = $client->request('POST');

		if ($response->isError()) {
			$body = Zend_Json_Decoder::decode($response->getBody());
            //var_dump($body);
            
			throw new Ikantam_Balanced_Exception('Could not credit a bank account');
		} elseif ($response->isSuccessful()) {
			$body = Zend_Json_Decoder::decode($response->getBody());
			$this->addData($body);
		}
	}

}