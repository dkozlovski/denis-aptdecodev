<?php

class Ikantam_Form extends Zend_Form
{
    /**
     * Retrieve error messages from elements failing validations
     *
     * @param  string $name
     * @param  bool $suppressArrayNotation
     * @return array
     */
    public function getFlatMessages($name = null, $suppressArrayNotation = false)
    {
        if (null !== $name) {
            if (isset($this->_elements[$name])) {
                return $this->getElement($name)->getMessages();
            } else if (isset($this->_subForms[$name])) {
                return $this->getSubForm($name)->getMessages(null, true);
            }
            foreach ($this->getSubForms() as $key => $subForm) {
                if ($subForm->isArray()) {
                    $belongTo = $subForm->getElementsBelongTo();
                    if ($name == $this->_getArrayName($belongTo)) {
                        return $subForm->getMessages(null, true);
                    }
                }
            }
        }

        $customMessages = $this->_getErrorMessages();
        if ($this->isErrors() && !empty($customMessages)) {
            return $customMessages;
        }

        $messages = array();

        foreach ($this->getElements() as $name => $element) {
            $eMessages = $element->getMessages();
            if (!empty($eMessages)) {
                $messages[$name] = reset($eMessages);
            }
        }

        foreach ($this->getSubForms() as $key => $subForm) {
            $merge = $subForm->getMessages(null, true);
            if (!empty($merge)) {
                if (!$subForm->isArray()) {
                    $merge = array($key => $merge);
                } else {
                    $merge = $this->_attachToArray($merge,
                                                   $subForm->getElementsBelongTo());
                }
                $messages = $this->_array_replace_recursive($messages, $merge);
            }
        }

        if (!$suppressArrayNotation &&
            $this->isArray() &&
            !$this->_getIsRendered()) {
            $messages = $this->_attachToArray($messages, $this->getElementsBelongTo());
        }

        return $messages;
    }

    /**
     * Wraps name into "belongsTo" brackets if needed
     *
     * @param string $name
     * @return string
     */
    public function createFullyQualifiedName($name)
    {
        if (null !== ($belongsTo = $this->getElementsBelongTo())) {
            $name = $belongsTo . '[' . $name . ']';
        }

        return $name;
    }

    /**
     * Recursively walks through all form elements and its subform elements
     *
     * @param callable $callable - 1 param form element
     * @param bool $walkSubforms - whether need to walk through subforms elements
     * @return $this
     */
    public function elementsWalk(\Closure $callable, $walkSubforms = true)
    {
        foreach ($this->getElements() as $element){
                $callable($element);
        }

        if ($walkSubforms) {
            foreach ($this->getSubForms() as $form) {
                if ($form instanceof Ikantam_Form) {
                    $form->elementsWalk($callable);
                }
            }
        }

        return $this;
    }

    /**
     * Creates HtmlTag decorator
     * @param array $options
     * @param string $content (optional) - internal content for tag
     * @return Ikantam_Form_Decorator_HtmlTag
     */
    protected function getHtmlTagDecorator($options = array(), $content = null)
    {
        if (is_string($options)) {
            $options = array('tag' => $options);
        }
        if (is_string($content)) {
            $options['content'] = $content;
        }
        return new Ikantam_Form_Decorator_HtmlTag($options);
    }
    
    
}