<?php

class Ikantam_Crop
{

	protected $_options;
	protected $_img;
	protected $_cimg; //cropped
	protected $_error;
	protected $_info;
	protected $_tmp;

	const NOT_EXIST = 'notExist';
	const FILE_TYPE = 'fileType';
	const NO_AREA = 'noArea';
	const REQUIREMENTS = 'requirements';
	const ORIGINAL = -1;

	protected $_messagesTemplate = array(
		self::NOT_EXIST => 'File %s not exist.',
		self::FILE_TYPE => 'File %s has unsupported type and can not be cropped.',
		self::NO_AREA => 'There is nothing to crop. Width or height can not be null.',
		self::REQUIREMENTS => 'Error: %s',
	);

	public function __construct($options = null)
	{
		$this->_options = array(
			'src_dir' => '', //source directory 
			'dst_dir' => '', //destiantion directory
			'quality' => 90, //quality 10-low, 100-high. Use for png and jpg format.
			'interlace' => false, //progressive JPEG
			'output_format' => self::ORIGINAL, //IMG_JPG|IMG_GIF|IMG_PNG
			'start_point' => array(0, 0), //top left point(x,y) to crop start
			'width' => 0, //cropping with
			'height' => 0, //cropping height
			'save_original' => true, // delete original file if set to false
			'name' => '', //new name for cropped image
			'random_name' => false, //name for cropped image generates randomly
			'square' => false, // width/height = 1
			'prefix' => 'img_',
			'min_height' => null,
			'min_width' => null,
			'max_height' => null,
			'max_width' => null,
			'max_file_size' => null,
			'create_thumbnail' => false,
			'thumbnail' => array(
				'width' => null,
				'height' => null,
				'prefix' => 'thumbnail_',
				'dir' => null,
			),
		);

		if ($options) {
			$this->setOptions($options);
		}
	}

	public function setOptions($options)
	{
		if (is_array($options)) {

			$this->_options = array_merge($this->_options, $options);
		}

		if (!empty($this->_options['src_dir'])) {
			$this->convertPath($this->_options['src_dir']);
		}

		if (!empty($this->_options['dst_dir'])) {
			$this->convertPath($this->_options['dst_dir']);
		}

		if (!empty($this->_options['thumbnail']['dir'])) {
			$this->convertPath($this->_options['thumbnail']['dir']);
		}



		return $this;
	}

	protected function isValidRequirements()
	{
		if (!$this->_info)
			return false;



		$options = &$this->_options;
		if (isset($options['min_width'])) {
			if ($this->_info[0] < $options['min_width']) {
				$this->addError(self::REQUIREMENTS, 'minimum available width is "' . $options['min_width'] . '" pixels.');
				return false;
			}
		}

		if (isset($options['max_width'])) {
			if ($this->_info[0] > $options['max_width']) {
				$this->addError(self::REQUIREMENTS, 'maximum available width is "' . $options['max_width'] . '" pixels.');
				return false;
			}
		}

		if (isset($options['min_height'])) {
			if ($this->_info[1] < $options['min_height']) {
				$this->addError(self::REQUIREMENTS, 'minimum available height is "' . $options['min_height'] . '" pixels.');
				return false;
			}
		}

		if (isset($options['max_height'])) {
			if ($this->_info[1] > $options['max_height']) {
				$this->addError(self::REQUIREMENTS, 'maximum available height is "' . $options['max_height'] . '" pixels.');
				return false;
			}
		}

		if ($options['square'] && $options['width'] != $options['height']) {
			$this->addError(self::REQUIREMENTS, 'the chosen area must be square.');
			return false;
		}

		if (isset($options['max_file_size'])) {
			if (!$this->_info['size']) {
				throw new Exception(__METHOD__ . ' file size is undefined.');
			}

			if ($this->_info['size'] > $options['max_file_size']) {
				$this->addError(self::REQUIREMENTS, 'maximum available file size is "' . $options['max_file_size'] . '" bytes.');
				return false;
			}
		}
	}

	protected function getExtension($format)
	{
		switch ($format) {
			case IMG_JPG:
				return '.jpg';
				break;

			case IMG_GIF:
				return '.gif';
				break;

			case IMG_PNG:
				return '.png';
				break;
		}
	}

	protected function convertPath(&$path)
	{
		$path = str_replace('/', DIRECTORY_SEPARATOR, $path);
		$path = str_replace('\\', DIRECTORY_SEPARATOR, $path);

		if ($path[strlen($path) - 1] != DIRECTORY_SEPARATOR)
			$path.= DIRECTORY_SEPARATOR;
	}

	protected function createDestination($dir, $name)
	{
		$this->_tmp['final_name'] = $this->_options['prefix'] . $name;
		$result = $dir . $this->_tmp['final_name'];

		if ($this->_options['save_original'] && file_exists($result . $this->_tmp['ext'])) {
			if (empty($this->_options['prefix']))
				$this->_options['prefix'] = '_';
			$result = $this->createDestination($dir, $this->_options['prefix'] . $name);
		}

		return $result;
	}

	protected function addError($key, $value = '')
	{
		$this->_error[] = array(
			'type' => $key,
			'message' => sprintf($this->_messagesTemplate[$key], $value));
	}

	public function getErrorMessages()
	{
		return $this->_error ? $this->_error : false;
	}

	/**
	 * Set image that will bew cropped. 
	 *  
	 * @param mixed $image 
	 * @return self
	 * 
	 * $image could be GD resource or string(file name or full path)
	 * If $image is GD resource than validation(width,heigth etc.) will be skiped.   
	 */
	public function setImage($image)
	{
		if (@get_resource_type($image) === 'gd') {
			$this->_img = $image;
			if (empty($this->_options['name'])) {
				if ($this->_options['random_name']) {
					$this->_tmp['name'] = uniqid();
				} else {
					throw new Exception(__METHOD__ . ' you must provide image name or enable "random_name" mode to generate it automatically.');
				}
			}
            $this->_tmp['ext'] = $this->getExtension($this->_options['output_format']);
            $this->_tmp['source'] = null;
            $this->_info = array(imagesx($image), imagesy($image));
			return $this;
		}

		if (is_string($image)) {
			if (strpos($image, '\\') || strpos($image, '/')) {
				$this->convertPath($image);

				$parts = explode(DIRECTORY_SEPARATOR, $image);
				$parts = array_filter($parts);
				$name = $parts[count($parts) - 1];
				unset($parts[count($parts) - 1]);
				$dir = implode(DIRECTORY_SEPARATOR, $parts);
				$this->_tmp['name'] = $name;
				$image = $name;
				$this->_options['src_dir'].=$dir . DIRECTORY_SEPARATOR;
			} else {
				$this->_tmp['name'] = $image;
			}

			if ($this->_options['random_name']) {
				$this->_tmp['name'] = uniqid();
			}

			$image = $this->_options['src_dir'] . $image;


			if (!file_exists($image)) {
				$this->addError(self::NOT_EXIST, $image);
				//throw new Exception(__METHOD__.' Image '. $image .'not exist.');
				return $this;
			}

			$this->_info = getimagesize($image);

			if (!$this->_info) {
				$this->addError(self::FILE_TYPE, $image);
				return $this;
			}
			$this->_info['size'] = @filesize($image);
			$this->_tmp['source'] = $image;
			$originalFormat;
			switch ($this->_info[2]) {
				case 1:
					$this->_img = imagecreatefromgif($image);
					$originalFormat = IMG_GIF;
					break;

				case 2:
					$this->_img = imagecreatefromjpeg($image);
					$originalFormat = IMG_JPEG;
					break;

				case 3:
					$this->_img = imagecreatefrompng($image);
					$originalFormat = IMG_PNG;
					break;

				default:
					$this->addError(self::FILE_TYPE, $image);
					break;
			}

			if ($this->_options['output_format'] == self::ORIGINAL) {
				$this->_options['output_format'] = $originalFormat;
			}
			$this->_tmp['ext'] = $this->getExtension($this->_options['output_format']);
		} else {
			throw new Exception(__METHOD__ . ' Invalid parameter given, expected string or image resource.');
		}

		$this->isValidRequirements();
		return $this;
	}

	public function crop($x = null, $y = null, $w = null, $h = null)
	{
		if (!$x) {
			$x = $this->_options['start_point'][0];
		}

		if (!$y) {
			$y = $this->_options['start_point'][1];
		}

		if (!$w) {
			$w = $this->_options['width'];
		}

		if (!$h) {
			$h = $this->_options['height'];
		}

		if (!$w || !$h) {
			$this->addError(self::NO_AREA);
			return false;
		}

		$this->_info['cropped'] = array($w, $h);

		if (!$this->getErrorMessages()) {
			$name = '';
			if ($this->_options['random_name']) {
				$name = $this->_tmp['name'];
			} elseif (!empty($this->_options['name'])) {
				$name = $this->_options['name'];
			} elseif ($this->_tmp['name']) {
				$name = $this->_tmp['name'];
			} else {
				throw new Exception('Unable to save file with no name.');
			}

			if (!empty($this->_options['dst_dir'])) {
				$this->_tmp['destination'] = $this->createDestination($this->_options['dst_dir'], $name);
			} else {
				$this->_tmp['destination'] = $this->createDestination($this->_options['src_dir'], $name);
			}

			if (!preg_match('/\.jpe?g|jpg|png|gif$/ui', $this->_tmp['destination'])) {
				$this->_tmp['destination'].= $this->_tmp['ext'];
			}

            if(!(int)$w || !(int)$h) throw new Exception(__METHOD__.'. Width or height can not be 0.');
			$img = imagecreatetruecolor((int)$w, (int)$h);
			$this->_cimg = $img;
			//imagecopyresampled($img, $this->_img, 0, 0, $x, $y, $w, $h, $w, $h);
			imagecopy($img, $this->_img, 0, 0, $x, $y, $w, $h);


			$callback = null;
			switch ($this->_options['output_format']) {
				case IMG_JPEG:
					if ($this->_options['interlace'])
						imageinterlace($img, true);
					imagejpeg($img, $this->_tmp['destination'], $this->_options['quality']);
					$callback = 'imagejpeg';
					break;

				case IMG_GIF:
					imagegif($img, $this->_tmp['destination']);
					$callback = 'imagegif';
					break;

				case IMG_PNG:
					$quality = $this->_options['quality'];
					if ($quality > 9) {
						$quality = ceil($this->_options['quality'] / 10) - 1;
						$this->_options['quality'] = $quality;
					}
					imagepng($img, $this->_tmp['destination'], $quality);
					$callback = 'imagepng';
					break;

				default:
					return false;
					break;
			}


			if ($this->_options['create_thumbnail']) {
				$this->saveThumbnail($callback, $this->_tmp['final_name'] . $this->_tmp['ext']);
			}

			if ($this->_options['save_original'] === false) {
				if (file_exists($this->_tmp['source'])) {
					unlink($this->_tmp['source']);
				}
			}
		} else {
			return false;
		}
		return true;
	}

	protected function saveThumbnail($callback, $name)
	{
		$w = $this->_options['thumbnail']['width'];
		$h = $this->_options['thumbnail']['height'];

		if (!$w || !$h) {
			throw new Exception(__METHOD__ . ' can not save thumbnail: expects 2 parameters(width and height)');
		}

		$directory = $this->_options['thumbnail']['dir'];
		if (!$directory)
			$directory = $this->_options['src_dir'];

		if ($this->_cimg && $this->_info['cropped']) {
			$cropped = $this->_info['cropped'];  //array (width, height)

			$thumbnail = imagecreatetruecolor($w, $h);
			imagecopyresampled($thumbnail, $this->_cimg, 0, 0, 0, 0, $w, $h, $cropped[0], $cropped[1]);

			/*if (empty($this->_options['thumbnail']['prefix'])) {
				$this->_options['thumbnail']['prefix'] = 'thumbnail_';
			}*/
            $prefix = '';
            if(isset($this->_options['thumbnail']['prefix'])) $prefix = $this->_options['thumbnail']['prefix'];
			$destination = $directory ? $directory . DIRECTORY_SEPARATOR . $prefix . $name : $prefix . $name;


			if (!file_exists($destination)) {
				if ($callback === 'imagejpeg' || $callback === 'imagepng') {
					$callback($thumbnail, $destination, $this->_options['quality']);
				} else {
					$callback($thumbnail, $destination);
				}

				return true;
			}
		}
		return false;
	}

	public function getImage()
	{
		return $this->_img();
	}

	public function getCroppedImage()
	{
		return $this->_cimg;
	}

	public function getInfo()
	{
		return $this->_info;
	}

	public function getName()
	{
		return $this->_tmp['final_name'] . $this->_tmp['ext'];
	}

}