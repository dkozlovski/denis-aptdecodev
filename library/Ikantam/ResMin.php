<?php

require_once "jsmin.php";
require_once "cssmin.php";
require_once "pngmin.php";

class Ikantam_ResMin
{
    protected $_dir ="";
    protected $_newDir="";
    protected $_pathapp="";
    
    public function minify()
    {   
        if(is_dir($this->_dir))
        {

            if(!is_dir($this->_newDir))
            {
                mkdir($this->_newDir);
            }
      
            $this->createTreeDirs($this->_dir);
            $this->miniFiles($this->_dir);   
        }

       return $this;
    }
    public function setPathApp($path) {
        
        $this->_pathapp = $path;
        
    }
    public function setDir($dir)
    {
        $this->_dir = $dir;
        $this->_newDir = $dir;
        return $this;
    }
    
    public function setNewDir($dir)
    {
        $this->_newDir = $dir;
        return $this;
    }
    
    protected function miniFiles($dir)
    {
        
        $files = glob($dir."/*");
        
        foreach($files as $file){
            
           if(is_dir($file))
           {
               $this->miniFiles($file);
           }else{
               $path_info = pathinfo($file);
               if("js" == strtolower($path_info['extension'])){
                   
                   $code = file_get_contents($file);
                   $file = str_replace($this->_dir, $this->_newDir, $file);
                   $gzcode = gzencode(jsmin::minify($code),9);
                   $path = pathinfo($file);
                   $file = substr($file,0,-strlen($path["extension"]))."mini.".$path["extension"];
                   file_put_contents($file, jsmin::minify($code));
                   file_put_contents($file.".gz", $gzcode);
                   
               }elseif("css" == strtolower($path_info['extension'])){
                   
                   $code = file_get_contents($file);
                   $file = str_replace($this->_dir, $this->_newDir, $file);              
                   $gzcode = gzencode(CssMin::minify($code),9);
                   $path = pathinfo($file);
                   $file = substr($file,0,-strlen($path["extension"]))."mini.".$path["extension"];
                   file_put_contents($file, Cssmin::minify($code));
                   file_put_contents($file.".gz", $gzcode);
                   
               }elseif("png" == strtolower($path_info['extension'])){
                   
                   $code = pngmin::compress_png($file, $this->_pathapp);
                   if($code){
                        $file = str_replace($this->_dir, $this->_newDir, $file);
                        file_put_contents($file, $code);
                   }
                   
               }else{              
                    copy($file, str_replace($this->_dir, $this->_newDir, $file));
               }
           }
            
        }
        
    }
    
    protected function createTreeDirs($dir)
    {
       $catalogs = glob($dir."/*");
       
       foreach($catalogs as $catalog ){
           
           if(is_dir($catalog)){

               $newCatalog = str_replace($this->_dir, $this->_newDir, $catalog);
               if(!is_dir($newCatalog))
               {
                   mkdir($newCatalog);                   
               }
               
               $this->createTreeDirs($catalog);
               
           }
       }
    }
    
}

