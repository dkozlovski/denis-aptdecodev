<?php

    class Ikantam_Log 
    {
       protected static $logger;
       protected static $logfile = 'my_log.log';       

        
        public static function log  ($message, $priority = 6)
        { 
           if(is_bool($message)) {
            $message = ($message) ? 'TRUE' : 'FALSE';
           } 
           if(!self::$logger)
           { 
            self::$logger = new Zend_Log(new Zend_Log_Writer_Stream(APPLICATION_PATH.'/'.self::$logfile));
           }
           
           if(is_array($message) || is_object($message))
           {
            $message = print_r($message, 1);
           }
           self::$logger->log($message, $priority); 
        }
        
        
    }