<?php
/**
* Adapter for Zend_Paginator to Ikantam_Collection
* Ikantam_Collection must implements Ikantam_Filter_Flex_Interface
*/ 
class Ikantam_Paginator_Flex_Adapter implements Zend_Paginator_Adapter_Interface
{
    protected $_collection ;
    protected $_filter ;

    /**
     * @param mixed $filter \Ikantam_Filter_Flex | \Ikantam_Filter_Flex_Interface
     * @throws \InvalidArgumentException
     */
    public function __construct ($filter) {
        if ($filter instanceof \Ikantam_Filter_Flex) {
            $this->_collection = $filter->getModel();
            $this->_filter = $filter;
        } elseif (!$filter instanceof Ikantam_Filter_Flex_Interface) {
            throw new \InvalidArgumentException('Collection "'.get_class($filter).
                '" must implements Ikantam_Filter_Flex_Interface');
        } else {
            $this->_collection = $filter;
            $this->_filter = $this->_collection->getFlexFilter();
        }
    }

    /**
     * @return \Ikantam_Filter_Flex
     */
    public function getFlexFilter()
    {
        return $this->_filter;
    }
    
    public function getItems ($offset, $itemCountPerPage)
    {
        $this->_filter->apply($itemCountPerPage, $offset);
        return $this->_collection->getItems();
    }
    
    public function count ()
    {
        return $this->_filter->count();
    }
}