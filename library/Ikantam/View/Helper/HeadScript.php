<?php

class Ikantam_View_Helper_HeadScript extends Zend_View_Helper_HeadScript
{

    protected $_version;

    public function __construct()
    {
        parent::__construct();

        if (Zend_Registry::isRegistered('config')) {
            $config = Zend_Registry::get('config');
            if (isset($config->assets->js->version)) {
                $this->_version = $config->assets->js->version;
            }
        }
    }

    public function itemToString($item, $indent, $escapeStart, $escapeEnd)
    {
        if (!empty($item->attributes['src']) && $this->_version) {
            $item->attributes['src'] .= '?v=' . $this->_version;
        }

        return parent::itemToString($item, $indent, $escapeStart, $escapeEnd);
    }

}
