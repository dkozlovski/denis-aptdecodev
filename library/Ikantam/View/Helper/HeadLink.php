<?php

class Ikantam_View_Helper_HeadLink extends Zend_View_Helper_HeadLink
{

    protected $_version;

    public function __construct()
    {
        parent::__construct();

        if (Zend_Registry::isRegistered('config')) {
            $config = Zend_Registry::get('config');
            if (isset($config->assets->css->version)) {
                $this->_version = $config->assets->css->version;
            }
        }
    }

    public function itemToString(stdClass $item)
    {
        if (!empty($item->href) && $this->_version) {
            $item->href .= '?v=' . $this->_version;
        }

        return parent::itemToString($item);
    }

}
