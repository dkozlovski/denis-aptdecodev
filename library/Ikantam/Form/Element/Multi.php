<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 09.07.14
 * Time: 16:14
 */

abstract class Ikantam_Form_Element_Multi extends Zend_Form_Element_Multi
{
    protected $multiDecorators = array();

    /**
     * Add a decorator for rendering the element
     *
     * @param  Zend_Form_Decorator_Interface $decorator

     * @return Ikantam_Form_Element_Multi
     */
    public function addMultiDecorator(Zend_Form_Decorator_Interface $decorator)
    {
        $this->multiDecorators[] = $decorator;
        return $this;
    }

} 