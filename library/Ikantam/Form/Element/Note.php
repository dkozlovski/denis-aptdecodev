<?php

class Ikantam_Form_Element_Note extends Zend_Form_Element_Xhtml
{
    public $helper = 'formNote';

    protected $_ignore = true;

    public function setValue($value)
    {
        if (null === $this->_value) {
            parent::setValue($value);
        }
        return $this;
    }
}