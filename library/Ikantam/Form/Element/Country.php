<?php

require_once 'Zend/Form/Element/Select.php';

class Ikantam_Form_Element_Country extends Zend_Form_Element_Select
{
    /**
     * Use formSelect view helper by default
     * @var string
     */
    public $helper = 'formCountry';
}
