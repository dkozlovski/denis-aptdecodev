<?php
/**
 * Author: Alex P.
 * Date: 08.07.14
 * Time: 11:25
 */

class Ikantam_Form_Decorator_HtmlTag extends Zend_Form_Decorator_HtmlTag
{
    /**
     * Render content wrapped in an HTML tag
     *
     * @param  string $content
     * @return string
     */
    public function render($content)
    {
        $tag       = $this->getTag();
        $placement = $this->getPlacement();
        $noAttribs = $this->getOption('noAttribs');
        $openOnly  = $this->getOption('openOnly');
        $closeOnly = $this->getOption('closeOnly');
        $tagContent = $this->getOption('content');
        if ($tagContent instanceof Closure) {
            $tagContent = $tagContent($this);
        }

        $this->removeOption('noAttribs');
        $this->removeOption('openOnly');
        $this->removeOption('closeOnly');
        // don't remove content option

        $attribs = null;
        if (!$noAttribs) {
            $attribs = $this->getOptions();
        }

        if (is_array($attribs)) {
            foreach ($attribs as $name => $value) {
                if (strpos($name, '__') === 0) {
                    unset($attribs[$name]);
                }
            }
            unset($attribs['content']); // !
        }

        switch ($placement) {
            case self::APPEND:
                if ($closeOnly) {
                    return $content . $this->_getCloseTag($tag);
                }
                if ($openOnly) {
                    return $content . $this->_getOpenTag($tag, $attribs) . $tagContent;
                }
                return $content
                . $this->_getOpenTag($tag, $attribs)
                . $tagContent
                . $this->_getCloseTag($tag);
            case self::PREPEND:
                if ($closeOnly) {
                    return $this->_getCloseTag($tag) . $content;
                }
                if ($openOnly) {
                    return $this->_getOpenTag($tag, $attribs) . $content . $tagContent;
                }
                return $this->_getOpenTag($tag, $attribs)
                . $tagContent
                . $this->_getCloseTag($tag)
                . $content;
            default:
                return (($openOnly || !$closeOnly) ? $this->_getOpenTag($tag, $attribs) : '')
                . $tagContent
                . $content
                . (($closeOnly || !$openOnly) ? $this->_getCloseTag($tag) : '');
        }
    }
} 