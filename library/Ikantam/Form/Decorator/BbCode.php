<?php
/**
 * Author: Alex P.
 * Date: 21.07.14
 * Time: 17:00
 */



class Ikantam_Form_Decorator_BbCode extends Zend_Form_Decorator_HtmlTag
{
    /**
     * Get the formatted open tag
     *
     * @param  string $tag
     * @param  array $attribs
     * @return string
     */
    protected function _getOpenTag($tag, array $attribs = null)
    {
        $html = '[' . $tag;
        if (null !== $attribs) {
            $html .= $this->_htmlAttribs($attribs);
        }
        $html .= ']';
        return $html;
    }

    /**
     * Get formatted closing tag
     *
     * @param  string $tag
     * @return string
     */
    protected function _getCloseTag($tag)
    {
        return '[/' . $tag . ']';
    }

} 