<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class NoFundingDestination extends Error
{
    public static $codes = array('no-funding-destination');
}
