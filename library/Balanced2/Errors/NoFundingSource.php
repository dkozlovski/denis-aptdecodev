<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class NoFundingSource extends Error
{
    public static $codes = array('no-funding-source');
}
