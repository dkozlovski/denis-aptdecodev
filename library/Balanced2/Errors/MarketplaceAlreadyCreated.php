<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class MarketplaceAlreadyCreated extends Error
{
    public static $codes = array('marketplace-already-created');
}
