<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class CannotRefund extends Error
{
    public static $codes = array('funding-source-not-refundable');
}
