<?php

namespace Balanced2\Errors;

use \Exception;

class FundingInstrumentNotCreditable extends Exception {}
