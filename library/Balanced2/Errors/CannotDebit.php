<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class CannotDebit extends Error
{
    public static $codes = array('funding-source-not-debitable');
}
