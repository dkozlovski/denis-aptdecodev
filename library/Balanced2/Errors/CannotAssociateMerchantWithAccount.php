<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class CannotAssociateMerchantWithAccount extends Error
{
    public static $codes = array('cannot-associate-merchant-with-account');
}