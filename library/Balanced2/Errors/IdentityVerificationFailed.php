<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class IdentityVerificationFailed extends Error
{
    public static $codes = array('identity-verification-error', 'business-principal-kyc', 'business-kyc', 'person-kyc');
}
