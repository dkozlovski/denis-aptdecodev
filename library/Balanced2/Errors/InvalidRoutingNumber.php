<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class InvalidRoutingNumber extends Error
{
    public static $codes = array('invalid-routing-number');
}
