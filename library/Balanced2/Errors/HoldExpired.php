<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class HoldExpired extends Error
{
    public static $codes = array('authorization-expired');
}
