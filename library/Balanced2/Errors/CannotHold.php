<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class CannotHold extends Error
{
    public static $codes = array('funding-source-not-hold');
}
