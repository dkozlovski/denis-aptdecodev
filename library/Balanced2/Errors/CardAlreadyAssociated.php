<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class CardAlreadyAssociated extends Error
{
    public static $codes = array('card-already-funding-src');
}
