<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class AddressVerificationFailed extends Error
{
    public static $codes = array('address-verification-failed');
}
