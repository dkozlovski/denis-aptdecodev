<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class Declined extends Error
{
    public static $codes = array('funding-destination-declined', 'authorization-failed', 'card-declined');
}
