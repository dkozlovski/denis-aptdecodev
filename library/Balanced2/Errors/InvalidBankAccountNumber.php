<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class InvalidBankAccountNumber extends Error
{
    public static $codes = array('invalid-bank-account-number');
}
