<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class InsufficientFunds extends Error
{
    public static $codes = array('insufficient-funds');
}
