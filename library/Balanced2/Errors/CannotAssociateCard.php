<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class CannotAssociateCard extends Error
{
    public static $codes = array('cannot-associate-card');
}
