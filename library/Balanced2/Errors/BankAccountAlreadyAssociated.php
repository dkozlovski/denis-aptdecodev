<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class BankAccountAlreadyAssociated extends Error
{
    public static $codes = array('bank-account-already-associated');
}
