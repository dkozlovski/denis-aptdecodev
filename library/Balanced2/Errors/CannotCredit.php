<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class CannotCredit extends Error
{
    public static $codes = array('funding-destination-not-creditable');
}
