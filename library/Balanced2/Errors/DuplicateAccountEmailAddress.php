<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class DuplicateAccountEmailAddress extends Error
{
    public static $codes = array('duplicate-email-address');
}
