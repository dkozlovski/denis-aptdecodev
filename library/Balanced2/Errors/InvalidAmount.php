<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class InvalidAmount extends Error
{
    public static $codes = array('invalid-amount');
}
