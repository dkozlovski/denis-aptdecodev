<?php

namespace Balanced2\Errors;

use Balanced2\Errors\Error;

class AccountIsAlreadyAMerchant extends Error
{
    public static $codes = array('account-already-merchant');
}
