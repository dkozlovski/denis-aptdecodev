<?php

namespace Balanced2;

/**
 * Configurable settings.
 *
 *  You can either set these settings individually:
 *
 *  <code>
 *  \Balanced2\Settings::$api_key = 'my-api-key-secret';
 *  </code>
 *
 *  or all at once:
 *
 *  <code>
 *  \Balanced2\Settngs::configure(
 *      'https://api.balancedpayments.com',
 *      'my-api-key-secret'
 *      );
 *  </code>
 */
class Settings
{

    const VERSION = '1.2.0';

    public static $url_root = 'https://api.balancedpayments.com';
    public static $api_key  = null;
    public static $agent    = 'balanced-php';
    public static $version  = Settings::VERSION;
    public static $accept   = 'application/vnd.balancedpayments+json; version=1.1, application/vnd.api+json';

    public static function init()
    {
        $config = \Zend_Registry::get('config')->balancedPayments->params;
        self::configure($config->apiEndpoint, $config->apiKey);
    }
    
    /**
     * Configure all settings.
     *
     * @param string url_root The root (schema://hostname[:port]) to use when constructing api URLs.
     * @param string api_key The api key secret to use for authenticating when talking to the api. If null then api usage is limited to uauthenticated endpoints.
     */
    public static function configure($url_root, $api_key)
    {
        self::$url_root = $url_root;
        self::$api_key = $api_key;
    }

}