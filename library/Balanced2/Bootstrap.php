<?php

namespace Balanced2;

/**
 * Bootstrapper for Balanced does autoloading and resource initialization.
 */
class Bootstrap
{
    const DIR_SEPARATOR = DIRECTORY_SEPARATOR;
    const NAMESPACE_SEPARATOR = '\\';

    public static $initialized = false;


    public static function init()
    {
        spl_autoload_register(array('\Balanced2\Bootstrap', 'autoload'));
        self::initializeResources();
    }

    public static function autoload($classname)
    {
        self::_autoload(dirname(dirname(__FILE__)), $classname);
    }

    public static function pharInit()
    {
        spl_autoload_register(array('\Balanced2\Bootstrap', 'pharAutoload'));
        self::initializeResources();
    }

    public static function pharAutoload($classname)
    {
        self::_autoload('phar://balanced.phar', $classname);
    }

    private static function _autoload($base, $classname)
    {
        $parts = explode(self::NAMESPACE_SEPARATOR, $classname);
        $path = $base . self::DIR_SEPARATOR. implode(self::DIR_SEPARATOR, $parts) . '.php';
        if (file_exists($path)) {
            require_once($path);
        }
    }

    /**
     * Initializes resources (i.e. registers them with Resource::_registry). Note
     * that if you add a Resource then you must initialize it here.
     *
     * @internal
     */
    private static function initializeResources()
    {
        if (self::$initialized)
            return;

        \Balanced2\Resource::init();

        \Balanced2\APIKey::init();
        \Balanced2\Marketplace::init();
        \Balanced2\Credit::init();
        \Balanced2\Debit::init();
        \Balanced2\Refund::init();
        \Balanced2\Reversal::init();
        \Balanced2\Card::init();
        \Balanced2\BankAccount::init();
        \Balanced2\BankAccountVerification::init();
        \Balanced2\CardHold::init();
        \Balanced2\Callback::init();
        \Balanced2\Event::init();
        \Balanced2\Customer::init();
        \Balanced2\Order::init();
        \Balanced2\Dispute::init();

        self::$initialized = true;
    }
}
