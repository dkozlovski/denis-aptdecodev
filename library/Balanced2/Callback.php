<?php

namespace Balanced2;

use Balanced2\Resource;
use \RESTful2\URISpec;

/**
 * A Callback is a publicly accessible location that can receive JSON
 * payloads whenever an Event occurs.
 *
 * <code>
 * $callback = new Balanced2\Callback(array(
 *   "url" => "http://www.example.com/callback"
 * ));
 * $callback->save();
 * </code>
 */
class Callback extends Resource
{
    protected static $_uri_spec = null;

    public static function init()
    {
        self::$_uri_spec = new URISpec('callbacks', 'id', '/');
        self::$_registry->add(get_called_class());
    }
}
