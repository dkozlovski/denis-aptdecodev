<?php

class Zend_View_Helper_TimeElapsedString extends Zend_View_Helper_Abstract
{

	public function timeElapsedString($datetime, $maxDates = null)
	{
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );

        $maxDates = $maxDates !== null ? $maxDates : count($string);
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        $string = array_slice($string, 0, $maxDates);

        return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

}
