<?php

class Zend_View_Helper_IsLoggedIn extends Zend_View_Helper_Abstract
{

	public function isLoggedIn()
	{
		return $this->getSession()->isLoggedIn();
	}

	protected function getSession()
	{
		return new User_Model_Session();
	}

}
