<?php

class Zend_View_Helper_Cart extends Zend_View_Helper_Abstract
{

	public function cart()
	{
      $session = new User_Model_Session();       
      return $session->getCart(); 
	}

}
