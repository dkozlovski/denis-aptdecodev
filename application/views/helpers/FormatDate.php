<?php

class Zend_View_Helper_FormatDate extends Zend_View_Helper_Abstract
{

    public function formatDate($dateString, $type = 'full')
    {
        if (null === $dateString || $dateString == 'other' || $dateString == 'not-selected') {
            return 'Not selected';
        }

        $date = strtotime(substr($dateString, 0, 10));
        $time = trim(substr($dateString, 10));

        $find = array(
            '8-12',
            '12-16',
            '16-20',
            '9-13',
            '17-21',
            '7-12'
        );

        $replace = array(
            '8AM &ndash; 12PM',
            '12PM &ndash; 4PM',
            '4PM &ndash; 8PM',
            '9AM &ndash; 1PM',
            '5PM &ndash; 9PM',
            '7AM &ndash; 12PM'
        );
        
        if ($type == 'short') {
            return str_replace($find, $replace, $time);
        }
        return date('l, F j', $date) . ', ' . str_replace($find, $replace, $time);
    }

}