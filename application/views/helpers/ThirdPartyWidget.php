<?php
/**
 * Author: Alex P.
 * Date: 09.09.14
 * Time: 18:05
 */


class Zend_View_Helper_ThirdPartyWidget extends Zend_View_Helper_Abstract
{


    /**
     * Build widget result with params
     * @param string $path
     * @param array (optional) $params
     * @return mixed
     */
    public function thirdPartyWidget($path, array $params = array())
    {
        return Ikantam_Controller_Front::getContainer()->get('core.service.thirdPartyWidget')
            ->getWidget($path, $params);
    }
} 