<?php

class Zend_View_Helper_GetUrlWithGaParams extends Zend_View_Helper_Abstract
{

    /**
     * copy Ikantam_Url::getUrlWithGaParams
     * @param $url
     * @param array $params
     * @return string
     */
    public function getUrlWithGaParams($url, $params = array())
    {
        // params description: https://support.google.com/analytics/answer/1033863?hl=en&ref_topic=1032998
        $defaultParams = array(
            'utm_source'   => 'email',
            'utm_medium'   => 'email',
            'utm_campaign' => 'email'
        );

        $params = array_merge($defaultParams, $params);
        $query = http_build_query($params);
        if ($query) {
            $mark = (strpos($url, '?') === false) ? '?' : '&';
            $url .= $mark . $query;
        }
        return $url;
    }
}
