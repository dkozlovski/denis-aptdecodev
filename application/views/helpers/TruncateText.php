<?php
class Zend_View_Helper_TruncateText extends Zend_View_Helper_Abstract {

	/**
     * Truncate text
     * @param string $text - source
     * @param int $wordsLimit - optional. If not specified, text will be truncated till first dot.
     * @param bool $htmlMode - html truncate | cares about tag closing 
     * @return string
     */    
    public function truncateText ($text, $wordsLimit = null, $htmlMode = false)
    {
      $text = preg_replace('/\s{2,}/', ' ', $text);
      if($htmlMode) {
        return Ikantam_DOM_TruncateHTML::truncateWords($text, $wordsLimit);
      }  
       
      
      if(!$wordsLimit) {
        $stripTags = new Zend_Filter_StripTags();
        return substr($text, 0, strpos($text, '. '));  
      } 
        
      $words = explode(' ', $text, $wordsLimit + 1);
      unset($words[count($words) - 1]);
      return implode(' ', $words);
          
    }
}