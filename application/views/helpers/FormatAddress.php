<?php

class Zend_View_Helper_FormatAddress extends Zend_View_Helper_Abstract
{

    public function formatAddress($address, Zend_Form_Decorator_Interface $lineDecorator = null)
    {
        $lines = array();
        if ($address instanceof Product_Model_Product) {            
            $lines[] = $address->getPickupFullName();
            $lines[] = sprintf('%s%s', $address->getPickupAddressLine1(), ' ' . $address->getPickupAddressLine2());
            $lines[] = sprintf('%s %s %s', $address->getPickupCity(), $address->getPickupState(), $address->getPickupPostcode());
            //$lines[] = $address->getPickupCountryCode();
            $lines[] = 'Phone: ' . $address->getPickupPhone();
        } elseif ($address instanceof Order_Model_Address) {
            $lines[] = $address->getFullName();
            $lines[] = sprintf('%s%s', $address->getAddressLine1(), ' ' . $address->getAddressLine2());
            $lines[] = sprintf('%s %s %s', $address->getCity(), $address->getStateCode(), $address->getPostalCode());
            //$lines[] = $address->getCountryCode();
            $lines[] = 'Phone: ' . $address->getTelephone();
        } else {
            if ($address->getAddressLine2()) {
                $lines[] = sprintf('%s, %s, %s, %s %s %s', $address->getAddressLine1(), $address->getAddressLine2(), $address->getCity(), $address->getState(), $address->getPostcode(), $address->getCountryCode());
            } else {
                $lines[] = sprintf('%s, %s, %s %s %s', $address->getAddressLine1(), $address->getCity(), $address->getState(), $address->getPostcode(), $address->getCountryCode());
            }
        }

        if (!$lineDecorator) {
            $adr = implode("\n", $lines);
        } else {
            $adr = implode('', array_map(function($line) use ($lineDecorator){
                        return $lineDecorator->render($line);
                    }, $lines));
        }

        return $adr;
    }

}
