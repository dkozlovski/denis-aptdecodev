<?php

class Zend_View_Helper_Wishlist extends Zend_View_Helper_Abstract
{

	public function wishlist()
	{
		$session = new User_Model_Session();
		return $session->getUser()->getWishlist();
	}

}