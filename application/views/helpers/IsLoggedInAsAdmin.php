<?php

class Zend_View_Helper_IsLoggedInAsAdmin extends Zend_View_Helper_Abstract
{

	public function isLoggedInAsAdmin()
	{
		return $this->getSession()->isLoggedIn();
	}

	protected function getSession()
	{
		return new Admin_Model_Session();
	}

}
