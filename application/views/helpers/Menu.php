<?php

class Zend_View_Helper_Menu extends Zend_View_Helper_Abstract
{

    private $_navigation = null;
    private $_categories = null;

    public function __construct()
    {
        $this->_navigation = new Application_Model_Navigation();
        $this->_categories = new Category_Model_Category_Collection();
    }

    public function menu()
    {
        return $this;
    }

    public function parentCategories()
    {
        $data       = array();
        $attributes = array('active' => array('class' => 'current'));
        foreach ($this->_categories->getParents()->getItems() as $category) {
            $data[] = array(
                'module'     => 'category',
                'controller' => 'index', // 'view',
                'action'     => 'index',
                'label'      => $category->getTitle(),
                'params'     => array(
                    'id' => $category->getId(),
                )
            );
        }
        return $this->_navigation->createMenu($data, $attributes);
    }

    public function listCategories()
    {
        $html = '';
        foreach ($this->_categories->getParents()->getItems() as $parent) {
            if (!$parent->getSubCategories()->getItems())
                continue;
            $data = array(array('attributes' => array('class' => 'category-menu'), 'label' => $parent->getTitle(), 'tag' => 'a', 'module' => 'category', 'controller' => 'index', 'action' => 'index', 'params' => array('id' => $parent->getId())));
            foreach ($parent->getSubCategories() as $category) {
                $data[] = array(
                    'module'     => 'category',
                    'controller' => 'index', //'view',
                    'action'     => 'index',
                    'label'      => $category->getTitle(),
                    'tag'        => 'a',
                    'params'     => array(
                        'id' => $category->getId(),
                    )
                );
            }//----<<<foreach
            $html.= $this->_navigation->createMenu($data);
        }//----<<<foreach 

        return $html;
    }

    /**
     * Return string html code for active item inside sidebar if current 
     * module, controller and action equals to parameters
     * 
     * @param string module
     * @param string controller
     * @param string action 
     * @return string - html
     */
    public function userProfileSidebarCheckActive($module, $controller = null, $action = null)
    {
        $active = 'class="activeg"';
        $params = $this->_navigation->getRequest()->getParams();

        if ($module == $params['module']) {
            if (is_null($controller))
                return $active;
            if ($controller == $params['controller']) {
                if (is_null($action))
                    return $active;
                if ($action == $params['action'])
                    return $active;
            }
        }
        return false;
    }

}