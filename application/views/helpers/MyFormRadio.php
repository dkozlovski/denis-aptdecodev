<?php

require_once 'Zend/View/Helper/FormRadio.php';

class Zend_View_Helper_MyFormRadio extends Zend_View_Helper_FormRadio
{

    public function myFormRadio($name, $value = null, $attribs = null, $options = null, $listsep = "<br />\n")
    {

        $info          = $this->_getInfo($name, $value, $attribs, $options, $listsep);
        extract($info); // name, value, attribs, options, listsep, disable
        // retrieve attributes for labels (prefixed with 'label_' or 'label')
        $label_attribs = array();
        $value_attribs = array();
        foreach ($attribs as $key => $val) {
            $tmp    = false;
            $keyLen = strlen($key);
            if ((6 < $keyLen) && (substr($key, 0, 6) == 'label_')) {
                $tmp = substr($key, 6);
            } elseif ((5 < $keyLen) && (substr($key, 0, 5) == 'label')) {
                $tmp = substr($key, 5);
            }

            $tmp2 = false;

            if ((6 < $keyLen) && (substr($key, 0, 6) == 'value_')) {
                $tmp2 = substr($key, 6);
            } elseif ((5 < $keyLen) && (substr($key, 0, 5) == 'value')) {
                $tmp2 = substr($key, 5);
            }

            if ($tmp) {
                // make sure first char is lowercase
                $tmp[0]              = strtolower($tmp[0]);
                $label_attribs[$tmp] = $val;
                unset($attribs[$key]);
            }

            if ($tmp2) {
                // make sure first char is lowercase
                $tmp2[0]              = strtolower($tmp2[0]);
                $value_attribs[$tmp2] = $val;
                unset($attribs[$key]);
            }
        }

        $labelPlacement = 'append';
        foreach ($label_attribs as $key => $val) {
            switch (strtolower($key)) {
                case 'placement':
                    unset($label_attribs[$key]);
                    $val = strtolower($val);
                    if (in_array($val, array('prepend', 'append'))) {
                        $labelPlacement = $val;
                    }
                    break;
            }
        }

        $valueClass = '';
        foreach ($value_attribs as $key => $val) {
            switch (strtolower($key)) {
                case 'class':
                    unset($value_attribs[$key]);
                    $val        = strtolower($val);
                    $valueClass = ' class="' . $val . '"';
                    break;
            }
        }

        // the radio button values and labels
        $options = (array) $options;

        // build the element
        $xhtml = '';
        $list  = array();

        // should the name affect an array collection?
        $name = $this->view->escape($name);
        if ($this->_isArray && ('[]' != substr($name, -2))) {
            $name .= '[]';
        }

        // ensure value is an array to allow matching multiple times
        $value = (array) $value;

        // Set up the filter - Alnum + hyphen + underscore
        require_once 'Zend/Filter/PregReplace.php';
        $pattern = @preg_match('/\pL/u', 'a') ? '/[^\p{L}\p{N}\-\_]/u'    // Unicode
                : '/[^a-zA-Z0-9\-\_]/';     // No Unicode
        $filter  = new Zend_Filter_PregReplace($pattern, "");

        // add radio buttons to the list.
        foreach ($options as $opt_value => $opt_label) {

            // Should the label be escaped?
            if ($escape) {
                $opt_label = $this->view->escape($opt_label);
            }
            $opt_label = '<span' . $valueClass . '>' . $opt_label . '</span>';
            // is it disabled?
            $disabled  = '';
            if (true === $disable) {
                $disabled = ' disabled="disabled"';
            } elseif (is_array($disable) && in_array($opt_value, $disable)) {
                $disabled = ' disabled="disabled"';
            }

            // is it checked?
            $checked    = '';
            $divChecked = 'radio-btn';
            if (in_array($opt_value, $value)) {
                $checked    = ' checked="checked"';
                $divChecked = 'radio-btn checkedRadio';
            }

            // generate ID
            $optId = $id . '-' . $filter->filter($opt_value);

            // Wrap the radios in labels
            $radio = '<div class="' . $divChecked . '"><input type="' . $this->_inputType . '"'
                    . ' name="' . $name . '"'
                    . ' id="' . $optId . '"'
                    . ' value="' . $this->view->escape($opt_value) . '"'
                    . $checked
                    . $disabled
                    . $this->_htmlAttribs($attribs)
                    . $this->getClosingBracket() . '</div>'
                    . (('append' == $labelPlacement) ? $opt_label : '');

            // add to the array of radio buttons
            $list[] = $radio;
        }

        // XHTML or HTML for standard list separator?
        if (!$this->_isXhtml() && false !== strpos($listsep, '<br />')) {
            $listsep = str_replace('<br />', '<br>', $listsep);
        }

        // done!
        $xhtml .= implode($listsep, $list);

        return $xhtml;
    }

}