<?php

class Zend_View_Helper_GetUrl extends Zend_View_Helper_Abstract
{

	public function getUrl($path, $params = array(), $reset = true, $configPath = true)
	{
		$path = array_reverse(explode('/', trim($path, '/')));

		$params['action'] = !empty($path[0]) ? $path[0] : null;
		$params['controller'] = !empty($path[1]) ? $path[1] : null;
		$params['module'] = !empty($path[2]) ? $path[2] : null;

		$view = new Zend_View();

		return $this->getBaseUrl($view, $configPath) . $view->url($params, $route = 'default', $reset);
	}

	protected function getBaseUrl($view, $configPath)
	{
		$baseUrl = null;

		if ($configPath) {
			$baseUrl = Zend_Registry::get('config')->baseUrl;
		}

		return empty($baseUrl) ? $view->serverUrl() : $baseUrl;
	}

}
