<?php

class Zend_View_Helper_GetPublicUrl extends Zend_View_Helper_Abstract
{

	public function GetPublicUrl($path, $configPath = false)
	{
		$view = new Zend_View();

		return $this->getBaseUrl($view, $configPath) . rtrim(Zend_Controller_Front::getInstance()->getBaseUrl(), '/') . '/' . $path;
	}

	protected function getBaseUrl($view, $configPath)
	{
		$baseUrl = null;

		if ($configPath) {
			$baseUrl = Zend_Registry::get('config')->baseUrl;
		}

		return empty($baseUrl) ? $view->serverUrl() : $baseUrl;
	}

}
