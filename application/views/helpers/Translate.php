<?php

class Zend_View_Helper_Translate extends Zend_View_Helper_Abstract
{

	public function translate($string)
	{
		$translate = Zend_Registry::get('Zend_Translate');

		return $translate->_($string);
	}

}
