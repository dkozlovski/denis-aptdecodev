<?php

class Zend_View_Helper_CollectionMenu extends Zend_View_Helper_Abstract
{

	public function collectionMenu($menuName, $limit = null)
	{
        return $this->_getActiveCollectionByMenuName($menuName, $limit);   
	}
    
    protected function _getActiveCollectionByMenuName($menuName, $limit)
    {
        $collections = new Application_Model_Set_Collection;
        $filter = $collections->getFlexFilter();
        $filter->is_active('=', 1)
                ->menu_name('=', $menuName);
                
        return $filter->apply($limit);                
    }

}
