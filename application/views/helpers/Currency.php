<?php

class Zend_View_Helper_Currency extends Zend_View_Helper_Abstract
{

	public function currency($value)
	{
		$currency = new Zend_Currency(array('display' => Zend_Currency::USE_SYMBOL), 'en_US');
		$currency->setValue($value)->setLocale('en_US');

		return $currency;
	}

}
