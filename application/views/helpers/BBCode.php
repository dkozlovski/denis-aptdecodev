<?php
/**
 * Author: Alex P.
 * Date: 21.07.14
 * Time: 17:38
 */

use Golonka\BBCode\BBCodeParser;


class Zend_View_Helper_BBCode extends Zend_View_Helper_Abstract
{
    protected $parser ;

    public function __construct()
    {
        $this->parser = new  BBCodeParser();
        $this->parser->setParser('paragraph', '/\[p\](.*?)\[\/p\]/s', '<p>$1</p>');
    }

    public function bBCode($string = null)
    {
        if (is_string($string)) {
            return $this->parser->parse($string);
        } elseif (null === $string) {
            return $this->parser;
        }

        throw new \InvalidArgumentException('Parameter should be string or empty.');
    }
} 