<?php

class Zend_View_Helper_DatePickerInputValue extends Zend_View_Helper_Abstract
{

    public function datePickerInputValue($date)
    {
        if (is_int($date)) {
            $date = date('Y-m-d', $date);
        }
        $format = 'M d, Y';
        if ($date === 'other' || $date === 'not-selected' || is_null($date)) {
            return date($format);
        }
        return date($format, strtotime(substr($date, 0, 10)));
    }

}