<?php

class Zend_View_Helper_IsAllowed extends Zend_View_Helper_Abstract
{

    public function isAllowed($resource = null, $privilege = null)
    {
        return $this->getAcl()->isAllowed($this->getCurrentUser(), $resource, $privilege);
    }

    public function getAcl()
    {
        return Application_Model_Acl::getInstance();
    }

    public function getCurrentUser()
    {
        if (Zend_Controller_Front::getInstance()->getRequest()->module === 'admin') {
            $role = new Admin_Model_Roles_Backend();
            return $role->getRole(Admin_Model_Session::instance()->getAdmin()->getData('role_id'));
        }
        
        return User_Model_Session::instance()->getUser();
    }

}