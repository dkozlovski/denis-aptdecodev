<?php

class Zend_View_Helper_User extends Zend_View_Helper_Abstract
{

	public function user()
	{
		$session = new User_Model_Session();
		return $session->getUser();
	}

}