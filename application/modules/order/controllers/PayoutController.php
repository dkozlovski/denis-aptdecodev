<?php

class Order_PayoutController extends Ikantam_Controller_Front
{
    protected $_ownerUri = '/v1/bank_accounts/BA6outRgrIVrzItl1zBkFfm0';
    
    
    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
        parent::init();
    }
    
    public function indexAction()
    {
        
        $payout = new Order_Model_Payout(2);
        
        if (!$payout->getId() || $payout->getIsProcessed()) {
            return;//error
        }
        
        $user = new User_Model_User($payout->getUserId());
        $bankAccountUri = $user->getMerchant()->getBankAccountUri();
        
        $amount = $payout->getTotalPaid() * 100;//amount in cents
        
        $marketplace = new Ikantam_Balanced_Marketplace();
        $credit = $marketplace->createCredit($bankAccountUri, $amount);
        
        $payout->setStatus($credit->getStatus())
                ->setCreatedAt(strtotime($credit->getCreatedAt()))
                ->setCreditUri($credit->getUri())
                ->setIsProcessed(1)
                ->setTotalPaid($credit->getAmount() / 100)
                ->save();

        
        $amount = ($payout->getProductPrice() - $payout->getTotalPaid()) * 100;
        
        $marketplace = new Ikantam_Balanced_Marketplace();
        $credit = $marketplace->createCredit($this->_ownerUri, $amount);

        
        echo 'done';
        die();
    }
}