<?php

class Order_Model_Payment2 extends Application_Model_Abstract
{

    protected $_creditCard;
    protected $_customer;
    protected $_order;
    protected $_holds = array();

    public function __construct()
    {
        $this->_getHolds();
    }

    protected function _getHolds()
    {
        $holdTypes = \Zend_Registry::get('config')->payment->hold->types;

        foreach ($holdTypes as $holdType) {
            $this->_holds[] = new $holdType;
        }
    }

    /**
     * Get credit card associated with payment
     * 
     * @return \Application_Model_CreditCard
     */
    public function getCreditCard()
    {
        return $this->_creditCard;
    }

    /**
     * Set credit card associated with payment
     * 
     * @param \Application_Model_CreditCard $card
     * @return \Order_Model_Payment
     */
    public function setCreditCard(\Application_Model_CreditCard $card)
    {
        $this->_creditCard = $card;
        return $this;
    }

    /**
     * Get customer associated with payment
     * 
     * @return \Application_Model_User
     */
    public function getCustomer()
    {
        return $this->_customer;
    }

    /**
     * Set customer associated with payment
     * 
     * @param \Application_Model_User $customer
     * @return \Order_Model_Payment
     */
    public function setCustomer(\Application_Model_User $customer)
    {
        $this->_customer = $customer;
        return $this;
    }

    /**
     * Get order associated with payment
     * 
     * @return \Order_Model_Order
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * Set order associated with payment
     * 
     * @param \Order_Model_Order $order
     * @return \Order_Model_Payment
     */
    public function setOrder(\Order_Model_Order $order)
    {
        $this->_order = $order;
        return $this;
    }

    public function createPayment($cart)
    {
        try {
            $buyer = $this->getCustomer()->getBalancedAccount();

            foreach ($cart->getAllDeliveredItems() as $item) {
                //$itemTotal      = $item->getTotals(); //incl shipping, transaction, discounts & qty, return formatted price
                $shippingPrice  = $item->getShippingPrice();
                $transactionFee = $item->getTransactionFee();
                
                foreach ($this->_holds as $hold) {
                    $hold->setItem($item)
                            ->setOrder($this->getOrder())
                            ->create();
                }

                //$hold1 = $buyer->createHold($itemTotal - $shippingPrice);
                //$hold2 = $buyer->createHold($shippingPrice);
                //$this->getOrder()->addShippingHold($hold2)->save();

                $orderItem = new Order_Model_Item();
                $_product  = $item->getProduct();

                $orderItem->setOrderId($this->getOrder()->getId())
                        ->setProductId($_product->getId())
                        ->setTitle($_product->getTitle())
                        ->setPrice($_product->getPrice())
                        ->setShippingMethod($item->getShipping())
                        ->setShippingAddressId($cart->getShippingAddressId())
                        ->setShippingPrice($shippingPrice)
                        ->setTransactionFee($transactionFee)
                     //   ->setHoldUri($hold1->getUri())
                        ->setDebitsUri($buyer->getDebitsUri())
                        ->setIsDelivered(0)
                        ->setIsReceived(0)
                        ->setOptions($item->getOptions())
                        ->setStatus(Order_Model_Order::ORDER_STATUS_ON_HOLD)
                        ->setQty($item->getQty(1))
                        ->save();

                //see product::setIsSold($qty) 
                $_product->setIsSold($item->getQty(1))->save();
                $item->delete();
            }

            $this->setUserId($this->getCustomer()->getId())
                    ->setCardNumber($this->getCreditCard()->getLastFour())
                    ->setAmount($cart->getTotals())
                    ->setCreatedAt(time())
                    ->save();
        } catch (Ikantam_Balanced_Exception $exc) {
            //var_dump($exc->getMessage());die();
            $this->logException($exc);
            throw $exc;
        } catch (Exception $exc) {
            //var_dump($exc->getMessage());die();
            $this->logException($exc);
            throw new Ikantam_Balanced_Exception("Cannot process your payment. Please, try again later.");
        }

        return $this;
    }

}
