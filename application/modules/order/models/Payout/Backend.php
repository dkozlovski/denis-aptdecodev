<?php

class Order_Model_Payout_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'payouts';

    public function getByItemId($object, $orderItemId)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `item_id` = :item_id AND `is_active` = 1';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':item_id', $orderItemId);
        $stmt->execute();
        $row  = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            $object->addData($row);
        }
    }

    public function getOneForCronJob($object)
    {
        $sql = 'SELECT `payouts`.* FROM `' . $this->_getTable() . '` 
            JOIN `merchants` ON `payouts`.`merchant_id` = `merchants`.`id`

            WHERE
            `payouts`.`is_processed` = 0
            AND `payouts`.`is_active` = 1 
            AND `payouts`.`processing_time` < :processing_time
            AND `payouts`.`is_paid_by_check` = 0
            AND (`payouts`.`dispute_status` IS NULL OR `payouts`.`dispute_status` = "dispute_seller")
            AND `merchants`.`account_uri` IS NOT NULL 
            AND `merchants`.`bank_account_uri` IS NOT NULL

            ORDER BY `payouts`.`id` ASC LIMIT 1';


        /* $sql = 'SELECT * FROM `' . $this->_getTable() . '` 
          WHERE `is_processed` = 0 AND `is_active` = 1 AND `processing_time` < :processing_time
          ORDER BY `id` ASC LIMIT 1'; */

        $processingTime = time();

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':processing_time', $processingTime);
        $stmt->execute();
        $row  = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            $object->addData($row);
        }
    }

    public function getForStatusCron($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '`
            WHERE 
            `is_processed` = 1 AND `is_active` = 1
            AND `credit_uri` IS NOT NULL AND `status` != "failed"
            AND `created_at` > :time';

        $processingTime = time() - 5 * 24 * 3600; //-5 days

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':time', $processingTime);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($rows) {
            foreach ($rows as $row) {
                $item = new Order_Model_Payout();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `' . $this->_getTable() . '` 
            (`id`, `merchant_id`, `user_id`, `product_price`, `shipping_price`, `comission`,
            `total_paid`, `created_at`, `item_id`, `product_id`, `credit_uri`,
            `status`, `is_processed`, `processing_time`, `is_active`, `qty`, `summary_price`, `expenses`, `is_paid_by_check`, `dispute_status`) 
            VALUES (NULL , :merchant_id, :user_id, :product_price, :shipping_price, :comission,
            :total_paid, :created_at, :item_id, :product_id, :credit_uri,
            :status, :is_processed, :processing_time, :is_active, :qty, :summary_price, :expenses, :is_paid_by_check, :dispute_status);';

        $stmt = $this->_getConnection()->prepare($sql);

        $merchantId     = $object->getMerchantId();
        $userId         = $object->getUserId();
        $productPrice   = $object->getProductPrice();
        $shippingPrice  = $object->getShippingPrice();
        $comission      = $object->getComission();
        $totalPaid      = $object->getTotalPaid();
        $createdAt      = $object->getCreatedAt();
        $itemId         = $object->getItemId();
        $productId      = $object->getProductId();
        $creditUri      = $object->getCreditUri();
        $status         = $object->getStatus();
        $isProcessed    = $object->getIsProcessed();
        $processingTime = $object->getProcessingTime();
        $isActive       = $object->getIsActive();
        $qty            = $object->getQty(1);
        $summary        = $object->getSummaryPrice();
        $expenses       = $object->getExpenses();
        $isPaidByCheck  = $object->getIsPaidByCheck();
        $disputeStatus  = $object->getDisputeStatus();

        $stmt->bindParam(':merchant_id', $merchantId);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':product_price', $productPrice);
        $stmt->bindParam(':shipping_price', $shippingPrice);
        $stmt->bindParam(':comission', $comission);
        $stmt->bindParam(':total_paid', $totalPaid);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':item_id', $itemId);
        $stmt->bindParam(':product_id', $productId);
        $stmt->bindParam(':credit_uri', $creditUri);
        $stmt->bindParam(':status', $status);
        $stmt->bindParam(':is_processed', $isProcessed);
        $stmt->bindParam(':processing_time', $processingTime);
        $stmt->bindParam(':is_active', $isActive);
        $stmt->bindParam(':qty', $qty);
        $stmt->bindParam(':summary_price', $summary);
        $stmt->bindParam(':expenses', $expenses);
        $stmt->bindParam(':is_paid_by_check', $isPaidByCheck);
        $stmt->bindParam(':dispute_status', $disputeStatus);

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = 'UPDATE `' . $this->_getTable() . '` 
            set `merchant_id` = :merchant_id,
            `user_id` = :user_id, `product_price` = :product_price, `shipping_price` = :shipping_price,
            `comission` = :comission, `total_paid` = :total_paid,
            `created_at` = :created_at, `item_id` = :item_id,
            `product_id` = :product_id, `credit_uri` = :credit_uri,
            `status` = :status, `is_processed` = :is_processed, `processing_time` = :processing_time,
            `is_active` = :is_active, `qty` = :qty, `summary_price` = :summary_price, `expenses` = :expenses,
            `is_paid_by_check` = :is_paid_by_check, `dispute_status` = :dispute_status
            WHERE `id` = :id';

        $stmt = $this->_getConnection()->prepare($sql);

        $id             = $object->getId();
        $merchantId     = $object->getMerchantId();
        $userId         = $object->getUserId();
        $productPrice   = $object->getProductPrice();
        $shippingPrice  = $object->getShippingPrice();
        $comission      = $object->getComission();
        $totalPaid      = $object->getTotalPaid();
        $createdAt      = $object->getCreatedAt();
        $itemId         = $object->getItemId();
        $productId      = $object->getProductId();
        $creditUri      = $object->getCreditUri();
        $status         = $object->getStatus();
        $isProcessed    = $object->getIsProcessed();
        $processingTime = $object->getProcessingTime();
        $isActive       = $object->getIsActive();
        $qty            = $object->getQty(1);
        $summary        = $object->getSummaryPrice();
        $expenses       = $object->getExpenses();
        $isPaidByCheck  = $object->getIsPaidByCheck();
        $disputeStatus  = $object->getDisputeStatus();

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':merchant_id', $merchantId);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':product_price', $productPrice);
        $stmt->bindParam(':shipping_price', $shippingPrice);
        $stmt->bindParam(':comission', $comission);
        $stmt->bindParam(':total_paid', $totalPaid);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':item_id', $itemId);
        $stmt->bindParam(':product_id', $productId);
        $stmt->bindParam(':credit_uri', $creditUri);
        $stmt->bindParam(':status', $status);
        $stmt->bindParam(':is_processed', $isProcessed);
        $stmt->bindParam(':processing_time', $processingTime);
        $stmt->bindParam(':is_active', $isActive);
        $stmt->bindParam(':qty', $qty);
        $stmt->bindParam(':summary_price', $summary);
        $stmt->bindParam(':expenses', $expenses);
        $stmt->bindParam(':is_paid_by_check', $isPaidByCheck);
        $stmt->bindParam(':dispute_status', $disputeStatus);

        $stmt->execute();
    }

}