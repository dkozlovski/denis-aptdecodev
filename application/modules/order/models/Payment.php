<?php

class Order_Model_Payment extends Application_Model_Abstract
{

    const ERROR_PRODUCT_NOT_AVAILABLE = 'Some of your products are no longer available for purchase.';

    protected $_marketplace;
    protected $_card;
    protected $_buyer;
    protected $_debit;

    public function logException(Exception $exception)
    {
        $error = 'Application error: ' . $exception->getMessage();

        $log = $this->getLog();

        if ($log) {
            $log->crit($error);
            $log->debug($exception);
        }
    }

    protected function getLog()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');

        if (!$bootstrap->hasResource('Log')) {
            return false;
        }

        $log = $bootstrap->getResource('Log');

        return $log;
    }

    public function createPayment($data, $cart, $user, $transactionFee, $order, $shippingFee, $coupon)
    {

        /*
          $transactionFee - transacation fee percent
          $shippingFee - shipping price
         */

        try {
            $options     = new Application_Model_Options();
            $marketplace = new Ikantam_Balanced_Marketplace();

            $data2 = array(
                'meta'          => null,
                'name'          => $user->getFullName(),
                'email_address' => null//$user->getEmail(),
            );

            $buyer = $marketplace->createBuyer($data2, $data['card_uri']);

            $cartTotal = 0;

            if ($shippingFee) {
                $count_ = 0;
                foreach ($cart->getAllDeliveredItems() as $item) {
                    if ($item->getShipping() == 'delivery') {
                        $count_++;
                    }
                }


                if ($count_ > 0) {
                    $shippingFee = $shippingFee / $count_;
                }

                //discount coupon
                if ($coupon->isValid($user->getId()) && $coupon->getType() == 'shipping') {
                    $shippingFee = 0;
                }
            }

            foreach ($cart->getAllDeliveredItems() as $item) {

                $_product = $item->getProduct();
                $_price   = $_product->getPrice();

                //if stored parameter with name transaction_fee_from_sellers is 1 and shipping method is  pick-up
                //add additional condition to handle fee
                //see Application_Model_Option::registerAdditionalCondition and handler method Application_Model_Option::_ACHandlerShippingFee
                if ($item->getShipping() == 'pick-up' && (bool) Application_Model_NVP::option('transaction_fee_from_sellers')) {
                    $options->registerAdditionalCondition('ShippingMethod', array('pick-up'));
                }

                $comission = $options->getIndividualFeeOrDefault($_product->getUserId(), $options::SELLER, $_price * $item->getQty());
                $cartTotal += $_price * $item->getQty(1);

                $amount_in_cents = ($_price * 100) * $item->getQty(1);



                //discount coupon
                if ($coupon->isValid($user->getId())) {
                    if ($coupon->getType() == 'percent') {
                        $amount_in_cents = $amount_in_cents - ($amount_in_cents * $coupon->getAmount() / 100);
                    } elseif ($coupon->getType() == 'fixed') {
                        $amount_in_cents = $amount_in_cents - ($coupon->getAmount() * 100);
                    }
                }

                if ($item->getShipping() == 'delivery') {
                    $amount_in_cents += $shippingFee * 100;
                }


                $transactionFeeInCents = $amount_in_cents / 100 * $transactionFee;
                $amount_in_cents += $transactionFeeInCents;
                $cartTotal += $transactionFeeInCents / 100;

                $data3 = array(
                    'appears_on_statement_as' => 'AptDeco',
                    'amount'                  => $amount_in_cents, //$amount_in_cents include transaction and shipping fees
                    'description'             => 'Description',
                    'on_behalf_of_uri'        => Ikantam_Balanced_Settings::getOnBehalfOfUri()
                );


                //$this->_debit = $buyer->createDebit($data3, $marketplace);
                $amount_in_cents = floor($amount_in_cents);
                $hold            = $buyer->createdHold((int) round($amount_in_cents, 0));

                $orderItem = new Order_Model_Item();
                $orderItem->setOrderId($order->getId())
                        ->setProductId($item->getProduct()->getId())
                        ->setTitle($item->getProduct()->getTitle())
                        ->setPrice($item->getProduct()->getPrice())
                        ->setShippingMethod($item->getShipping())
                        ->setShippingAddressId($cart->getShippingAddressId())
                        ->setShippingPrice($shippingPrice)
                        ->setHoldUri($hold->getUri())
                        ->setDebitsUri($buyer->getDebitsUri())
                        ->setIsDelivered(0)
                        ->setIsReceived(0)
                        ->setOptions($item->getOptions())
                        ->setStatus(Order_Model_Order::ORDER_STATUS_ON_HOLD)
                        ->setQty($item->getQty(1))
                        ->save();



                //see product::setIsSold($qty) 
                $item->getProduct()->setIsSold($item->getQty(1))->save();
                //product sold event
                $item->getProduct()->selfTriggerEvent('sold', array('from' => __method__));

                if (!$item->getProduct()->getSeller()->getMerchant()->getId()) {
                    $sellerMer = new User_Model_Merchant();
                    $sellerMer->setUserId($item->getProduct()->getSeller()->getId())->save();
                    if ($sellerMer->getId()) {
                        $item->getProduct()->getSeller()->getMerchant()->setId($sellerMer->getId());
                    }
                }



                $summaryPrice = $_price * $item->getQty(1);

                $totalPaid = (int) ($summaryPrice * (100 - $comission));

                //discount coupon
                if ($coupon->isValid($user->getId())) {
                    if ($coupon->getType() == 'percent') {
                        $summaryPrice = $summaryPrice - ($summaryPrice * $coupon->getAmount() / 100);
                    } elseif ($coupon->getType() == 'fixed') {
                        $summaryPrice = $summaryPrice - $coupon->getAmount();
                        $coupon->setData(array()); // fixed amount apply only 1 time 
                    }
                }

                if ($summaryPrice < 0) {
                    $summaryPrice = 0;
                }

                $expenses = null;
                if ($item->getShipping() == 'delivery') {
                    //seller absorbing delivery fee
                    if ($absorbingBySellerPercent = $item->getProduct()->getShippingCover()) {
                        if ($absorbingBySellerPercent < 0 || $absorbingBySellerPercent > 100) {
                            $absorbingBySellerPercent = 0;
                        }
                        $expenses = Shipping_Model_Rate::getMaximumValueSellerAbsorb() * $absorbingBySellerPercent / 100;
                        if ($expenses < 0) {
                            $expenses = 0;
                        }
                    }
                }

                $payout = new Order_Model_Payout();
                $payout->setMerchantId($item->getProduct()->getSeller()->getMerchant()->getId())
                        ->setUserId($item->getProduct()->getSeller()->getId())
                        ->setProductPrice($item->getProduct()->getPrice())
                        ->setComission($comission)
                        ->setTotalPaid($totalPaid / 100 - $expenses)
                        ->setShippingPrice($shippingPrice)
                        ->setCreatedAt(null)
                        ->setItemId($orderItem->getId())
                        ->setProductId($item->getProduct()->getId())
                        ->setCreditUri(null)
                        ->setIsProcessed(null)
                        ->setIsActive(1)
                        ->setQty($item->getQty(1))
                        ->setSummaryPrice($summaryPrice)
                        ->setExpenses($expenses)
                        ->setIsPaidByCheck(0)
                        ->setIsDisputed(0)
                        ->save();


                $item->delete();
            }

            $cartTotal += $shippingFee;
            $this->setUserId($user->getId())
                    ->setCardNumber($data['last_four'])
                    ->setAmount($cartTotal) // CART TOTAL OR $summaryPrice(with applied coupon discount)?
                    ->setCreatedAt(time())
                    ->save();
        } catch (Ikantam_Balanced_Exception $exc) {
            //var_dump($exc->getMessage());die();
            $this->logException($exc);
            throw $exc;
        } catch (Exception $exc) {
            //var_dump($exc->getMessage());die();
            $this->logException($exc);
            throw new Ikantam_Balanced_Exception("Cannot process your payment. Please, try again later.");
        }

        return $this;
    }

    public function createPayment2($data, $cart, $user, $transactionFee, $order, $shippingFee, $coupon, $bpCustomer, $userId)
    {
        Httpful\Bootstrap::init();
        RESTful\Bootstrap::init();
        Balanced\Bootstrap::init();
        Balanced\Settings::init();

        /*
          $transactionFee - transaction fee percent
          $shippingFee - shipping price
         */

        try {

            $options     = new Application_Model_Options();
            $marketplace = Balanced\Marketplace::mine();

            if (!$bpCustomer) {
                $data2 = array('name' => $user->getFullName());
                $buyer = new Balanced\Customer($data2);
                $buyer->save();

                if (!$user->getId() && $userId) {
                    $existingUser = new User_Model_User($userId);

                    if ($existingUser->getId()) {
                        $existingUser->setBpCustomerUri($buyer->uri)->save();
                    }
                } elseif ($user->getId()) {
                    $user->setBpCustomerUri($buyer->uri)->save();
                }
            } else {
                $buyer = $bpCustomer;
            }

            $card = $marketplace->createCard(null, null, null, null, null, $data['credit_card']['card_number'], $data['credit_card']['cvv'], $data['credit_card']['exp_month'], $data['credit_card']['exp_year']);

            $buyer->addCard($card);

            $cartTotal     = 0;
            $isCouponValid = $coupon->isValid($user->getId());

            foreach ($cart->getItemsForCheckout() as $item) {
                /** @var \Cart_Model_Item $item */
                /** @var \Product_Model_Product $_product */
                $_product = $item->getProduct();
                $_price   = $_product->getPrice();


                //if stored parameter with name transaction_fee_from_sellers is 1 and shipping method is  pick-up
                //add additional condition to handle fee
                //see Application_Model_Option::registerAdditionalCondition and handler method Application_Model_Option::_ACHandlerShippingFee
                if ($item->getShippingMethodId() == 'pickup' && (bool) Application_Model_NVP::option('transaction_fee_from_sellers')) {
                    $options->registerAdditionalCondition('ShippingMethod', array('pick-up'));
                }

                $comission = $options->getIndividualFeeOrDefault($_product->getUserId(), $options::SELLER, $_price * $item->getQty());
                $cartTotal += $_price * $item->getQty(1);

                $amount_in_cents = ($_price * 100) * $item->getQty(1);

                $discountAmount = 0;

                //discount coupon
                if ($isCouponValid) {
                    if ($coupon->getType() == Cart_Model_Coupon::TYPE_PERCENT) {
                        if ($coupon->getCategoryId()) { // for category items
                            $subcategoriesIds = $coupon->getSubcategoriesIds();
                            if (in_array($item->getProduct()->getCategoryId(), $subcategoriesIds)) {
                                $discountAmount  = $amount_in_cents * $coupon->getAmount() / 100;
                                $amount_in_cents = $amount_in_cents - $discountAmount;
                            }
                        } else { // for all items
                            $discountAmount  = $amount_in_cents * $coupon->getAmount() / 100;
                            $amount_in_cents = $amount_in_cents - $discountAmount;
                        }
                    } elseif ($coupon->getType() == Cart_Model_Coupon::TYPE_FIXED) {
                        $discountAmount  = $coupon->getAmount() * 100;
                        $amount_in_cents = $amount_in_cents - $discountAmount;
                    }
                }

                if ($item->getShippingMethodId() == 'delivery') {
                    $shippingAmount = $item->getShippingAmount();
                } else {
                    $shippingAmount = 0;
                }

                $amount_in_cents += $shippingAmount * 100;
                $transactionFeeInCents = $amount_in_cents / 100 * $transactionFee;
                $amount_in_cents += $transactionFeeInCents;
                $cartTotal += $transactionFeeInCents / 100;

                $hold = $marketplace->holds->create(array(
                    "amount"     => (int) floor($amount_in_cents),
                    //"description" => "Some descriptive text for the debit in the dashboard",
                    "source_uri" => $card->uri
                ));

                $orderItem = new Order_Model_Item();
                $orderItem->setOrderId($order->getId())
                        ->setProductId($item->getProduct()->getId())
                        ->setTitle($item->getProduct()->getTitle())
                        ->setPrice($item->getProduct()->getPrice())
                        ->setProductPrice($item->getProduct()->getPrice())
                        ->setProductTitle($item->getProduct()->getTitle())
                        ->setShippingMethodId($item->getShippingMethodId())
                        ->setShippingMethod($item->getShipping())
                        ->setShippingAddressId($cart->getShippingAddressId())
                        ->setShippingPrice(round($shippingAmount, 2))
                        ->setShippingAmount(round($shippingAmount, 2))
                        ->setDiscountAmount($discountAmount / 100)
                        ->setHoldUri($hold->uri)
                        ->setIsDelivered(0)
                        ->setIsReceived(0)
                        ->setIsNotified(0)
                        ->setIsProductAvailable(null)
                        ->setOptions($item->getOptions())
                        ->setStatus(Order_Model_Order::ORDER_STATUS_ON_HOLD)
                        ->setQty($item->getQty(1))
                        ->save();

                //see product::setIsSold($qty) 
                if ($item->getProduct()->getQty() < $item->getQty(1)) {
                    throw new Exception(self::ERROR_PRODUCT_NOT_AVAILABLE);
                }

                $item->getProduct()->setIsSold($item->getQty(1))->save();
                //product sold event
                $item->getProduct()->selfTriggerEvent('sold', array('from' => __method__));

                if (!$item->getProduct()->getSeller()->getMerchant()->getId()) {
                    $sellerMer = new User_Model_Merchant();
                    $sellerMer->setUserId($item->getProduct()->getSeller()->getId())->save();
                    if ($sellerMer->getId()) {
                        $item->getProduct()->getSeller()->getMerchant()->setId($sellerMer->getId());
                    }
                }



                $summaryPrice = $item->getProduct()->getPrice() * $item->getQty(1);

                $totalPaid = (int) ($summaryPrice * (100 - $comission));

                //discount coupon
                if ($isCouponValid) {
                    if ($coupon->getType() == 'percent') {
                        $summaryPrice = $summaryPrice - ($summaryPrice * $coupon->getAmount() / 100);
                    } elseif ($coupon->getType() == 'fixed') {
                        $summaryPrice = $summaryPrice - $coupon->getAmount();
                        $coupon->setData(array()); // fixed amount apply only 1 time 
                    }
                }

                if ($summaryPrice < 0) {
                    $summaryPrice = 0;
                }

                $expenses = null;
                if ($item->getShippingMethodId() == 'delivery') {
                    //seller absorbing delivery fee
                    $expenses = $item->getSellerExpenses();
                }

                $payout = new Order_Model_Payout();
                $payout->setMerchantId($item->getProduct()->getSeller()->getMerchant()->getId())
                        ->setUserId($item->getProduct()->getSeller()->getId())
                        ->setProductPrice($item->getProduct()->getPrice())
                        ->setComission($comission)
                        ->setTotalPaid($totalPaid / 100 - $expenses)
                        ->setShippingPrice(round($shippingFee, 2))
                        ->setCreatedAt(null)
                        ->setItemId($orderItem->getId())
                        ->setProductId($item->getProduct()->getId())
                        ->setCreditUri(null)
                        ->setIsProcessed(null)
                        ->setIsActive(1)
                        ->setQty($item->getQty(1))
                        ->setSummaryPrice($summaryPrice)
                        ->setExpenses($expenses)
                        ->setIsPaidByCheck(0)
                        ->setIsDisputed(0)
                        ->save();


                $item->delete();
            }

            $cartTotal += $shippingFee;
            $this->setUserId($user->getId())
                    ->setCardNumber($card->last_four)
                    ->setAmount($cartTotal) // CART TOTAL OR $summaryPrice(with applied coupon discount)?
                    ->setCreatedAt(time())
                    ->save();
        } catch (Ikantam_Balanced_Exception $exc) {
            //var_dump($exc->getMessage());die();
            $this->logException($exc);
            throw $exc;
        } catch (Exception $exc) {
            //var_dump($exc->getMessage());die();
            if ($exc->getMessage() == self::ERROR_PRODUCT_NOT_AVAILABLE) {
                throw new Exception($exc->getMessage());
            }
            $this->logException($exc);
            throw new Ikantam_Balanced_Exception("Cannot process your payment. Please, try again later.");
        }

        return $this;
    }

}