<?php

/**
 * Class Order_Model_Item
 * @method Order_Model_Item_Backend _getBackend()
 * @method int getOrderId()
 * @method int getProductId()
 * @method string getOptions()
 * @method int getShippingMethodId()
 * @method int getPickUpAddressId()
 * @method int getQty()
 * @method float getRowTotal()
 * @method int getIsReceived()
 * @method int getIsDelivered()
 * @method string getStatus()
 *
 * @method Order_Model_Item setOptions(string $serializedOptions)
 * @method Order_Model_Item setProductId(int $value)
 * @method Order_Model_Item setProductTitle(string $value)
 * @method Order_Model_Item setProductPrice($value)
 * @method Order_Model_Item setShippingMethodId(int $value)
 * @method Order_Model_Item setPickUpAddressId(int $value)
 * @method Order_Model_Item setQty($value)
 * @method Order_Model_Item setRowTotal($value)
 * @method Order_Model_Item setIsReceived(int $value)
 * @method Order_Model_Item setIsDelivered(int $value)
 * @method Order_Model_Item setStatus($value)
 *
 */
class Order_Model_Item extends Order_Model_Item_Abstract
    implements Application_Model_Event_Dispatcher_AwareInterface
{
    protected $_eventDispatcher;
    
    public function getByProductId($id)
    {
        $this->_getBackend()->getByProductId($id, $this);
        return $this;
    }

    public function getLastWeekByProductId($id)
    {
        $this->_getBackend()->getLastWeekByProductId($id, $this);
        return $this;
    }

    protected function _beforeSave()
    {
        $rowTotal = $this->getData('qty') * $this->getData('product_price');

        $this->setData('row_total', $rowTotal);
    }

    public function fromCartItem(Cart_Model_Item $cartItem)
    {
        $this->setProductId($cartItem->getProductId())
                ->setProductTitle($cartItem->getProduct()->getTitle())
                ->setProductPrice($cartItem->getProductPrice())
                ->setShippingMethodId($cartItem->getShippingMethodId())
                ->setPickUpAddressId($cartItem->getProduct()->getPickUpAddressId())
                ->setOptions($cartItem->getOptions())
                ->setQty($cartItem->getQty())
                ->setRowTotal($cartItem->getRoeTotal());

        return $this;
    }

    /*
      public function getBuyerDateBySellerDate($sellerDate)
      {
      $deliveryDate = new Checkout_Model_DeliveryDate();
      $pos = strpos($sellerDate, ' ');
      $date = substr($sellerDate, 0, $pos);
      $time = substr($sellerDate, $pos + 1);
      $buyerTimes = $deliveryDate->getDeliveryTimesForBuyer($this->getProduct()->getPickupPostcode(), $this->getOrder()->getShippingAddress()->getPostalCode());

      $newTime = $time;
      foreach ($buyerTimes as $_time => $__sellerTimes) {

      foreach ($__sellerTimes as $__time) {
      if ($__time == $time) {
      $newTime = $_time;
      }
      }
      }

      return $date . ' ' . $newTime;
      }
     */




    /**
     * Confirm delivery on behalf of a buyer
     */
    public function confirmDelivery()
    {
        if ($this->getId() && !$this->getIsReceived() && !$this->getIsDelivered()) {

            $this->setIsReceived(1)
                    ->setIsDelivered(1)
                    ->setStatus(Order_Model_Order::ORDER_STATUS_COMPLETE)
                    ->save();

            $payout = new Order_Model_Payout();
            $payout->getByItemId($this->getId());

            if ($payout->getId() && is_null($payout->getIsProcessed()) && !$payout->getIsPaidByCheck()) {

                if ($this->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
                    $procTime = time() + 86400; //set 24-hour processing time
                } else {
                    $procTime = time(); //set immediate processing time
                }

                $payout->setIsProcessed(0)//set 0 for further processing by cron
                        ->setProcessingTime($procTime)
                        ->save();

                $seller = $this->getProduct()->getSeller();

                if (!$seller->getMerchant()->getBankAccountUri() || !$seller->getMerchant()->getAccountUri()) {
                    //seller does not have payout account
                    $mailer = new Notification_Model_User_PayoutInfoMissing();
                    $mailer->sendEmails($seller);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Get event dispatcher instance
     *
     * @return Application_Model_Event_Dispatcher
     */
    public function getEventDispatcher()
    {
        if (!$this->_eventDispatcher) {
            $this->_eventDispatcher = new Application_Model_Event_Dispatcher();
        }
        return $this->_eventDispatcher;
    }

    /**
     * Set event dispatcher instance
     *
     * @param Zend_EventManager_EventCollection $eventDispatcher
     * @return Product_Model_Product
     */
    public function setEventDispatcher(Zend_EventManager_EventCollection $eventDispatcher)
    {
        $this->_eventDispatcher = $eventDispatcher;
        return $this;
    }
}