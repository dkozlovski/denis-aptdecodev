<?php

class Order_Model_Address_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'order_addresses';

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `' . $this->_getTable() . '` 
            (`user_address_id`,
            `full_name`,
            `address_line1`,
            `address_line2`, 
            `city`, 
            `state_code`, 
            `state_name`,
            `postal_code`,
            `country_code`,
            `email`, 
            `telephone`,
            `building_type`,
            `number_of_stairs`,
            `address_type`)
            
            VALUES
            (:user_address_id,
            :full_name,
            :address_line1,
            :address_line2, 
            :city,
            :state_code,
            :state_name,
            :postal_code, 
            :country_code,
            :email,
            :telephone,
            :building_type,
            :number_of_stairs,
            :address_type);';

        $stmt = $this->_getConnection()->prepare($sql);

        $userAddressId  = $object->getUserAddressId();
        $fullName       = $object->getFullName();
        $addressLine1   = $object->getAddressLine1();
        $addressLine2   = $object->getAddressLine2();
        $city           = $object->getCity();
        $stateCode      = $object->getStateCode();
        $stateName      = $object->getStateName();
        $postalCode     = $object->getPostalCode();
        $countryCode    = $object->getCountryCode();
        $email          = $object->getEmail();
        $telephone      = $object->getTelephone();
        $buildingType   = $object->getBuildingType();
        $numberOfStairs = $object->getNumberOfStairs();
        $addressType    = $object->getAddressType();

        $stmt->bindParam(':user_address_id', $userAddressId);
        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':address_line1', $addressLine1);
        $stmt->bindParam(':address_line2', $addressLine2);
        $stmt->bindParam(':city', $city);
        $stmt->bindParam(':state_code', $stateCode);
        $stmt->bindParam(':state_name', $stateName);
        $stmt->bindParam(':postal_code', $postalCode);
        $stmt->bindParam(':country_code', $countryCode);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':telephone', $telephone);
        $stmt->bindParam(':building_type', $buildingType);
        $stmt->bindParam(':number_of_stairs', $numberOfStairs);
        $stmt->bindParam(':address_type', $addressType);

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = 'UPDATE `' . $this->_getTable() . '` SET
            `user_address_id` = :user_address_id,
            `full_name` = :full_name,
            `address_line1` = :address_line1,
            `address_line2` = :address_line2,
            `city` = :city,
            `state_code` = :state_code,
            `state_name` = :state_name,
            `postal_code` = :postal_code,
            `country_code` = :country_code,
            `email` = :email,
            `telephone` = :telephone,
            `building_type` = :building_type,
            `number_of_stairs` = :number_of_stairs,
            `address_type` = :address_type
            WHERE `id` = :id;';

        $stmt = $this->_getConnection()->prepare($sql);

        $id             = $object->getId();
        $userAddressId  = $object->getUserAddressId();
        $fullName       = $object->getFullName();
        $addressLine1   = $object->getAddressLine1();
        $addressLine2   = $object->getAddressLine2();
        $city           = $object->getCity();
        $stateCode      = $object->getStateCode();
        $stateName      = $object->getStateName();
        $postalCode     = $object->getPostalCode();
        $countryCode    = $object->getCountryCode();
        $email          = $object->getEmail();
        $telephone      = $object->getTelephone();
        $buildingType   = $object->getBuildingType();
        $numberOfStairs = $object->getNumberOfStairs();
        $addressType    = $object->getAddressType();

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':user_address_id', $userAddressId);
        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':address_line1', $addressLine1);
        $stmt->bindParam(':address_line2', $addressLine2);
        $stmt->bindParam(':city', $city);
        $stmt->bindParam(':state_code', $stateCode);
        $stmt->bindParam(':state_name', $stateName);
        $stmt->bindParam(':postal_code', $postalCode);
        $stmt->bindParam(':country_code', $countryCode);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':telephone', $telephone);
        $stmt->bindParam(':building_type', $buildingType);
        $stmt->bindParam(':number_of_stairs', $numberOfStairs);
        $stmt->bindParam(':address_type', $addressType);

        $stmt->execute();
    }

}
