<?php

class Order_Model_Payout extends Application_Model_Abstract
{

    const PAYOUT_DISPUTE_INITIATED = 'dispute_initiated';
    const PAYOUT_DISPUTE_SELLER    = 'dispute_seller';
    const PAYOUT_DISPUTE_BUYER     = 'dispute_buyer';

    protected $_marketplace;
    protected $_card;
    protected $_buyer;
    protected $_debit;

    public function getByItemId($orderItemId)
    {
        $this->_getBackend()->getByItemId($this, $orderItemId);
        return $this;
    }

    public function getOneForCronJob()
    {
        $this->_getBackend()->getOneForCronJob($this);
        return $this;
    }

    public function logException(Exception $exception)
    {
        $error = 'Application error: ' . $exception->getMessage();

        $log = $this->getLog();

        if ($log) {
            $log->crit($error);
            $log->debug($exception);
        }
    }

    protected function getLog()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');

        if (!$bootstrap->hasResource('Log')) {
            return false;
        }

        $log = $bootstrap->getResource('Log');

        return $log;
    }

    public function createOut($data, $cart, $user)
    {
        try {

            $marketplace = new Ikantam_Balanced_Marketplace();

            $data1 = array(
                'expiration_month' => $data['cc_expiry_month'],
                'security_code'    => $data['cc_code'],
                'card_number'      => $data['cc_number'],
                'expiration_year'  => $data['cc_expiry_year']
            );

            $card = $marketplace->createCard($data1);

            $data2 = array(
                'email_address' => null,
                'meta'          => null,
                'name'          => $data['ba_full_name']
            );

            $buyer = $marketplace->createBuyer($data2, $card);


            $cartTotal = 0;
            foreach ($cart->getAllDeliveredItems() as $item) {
                $_product        = $item->getProduct();
                $cartTotal += $_product->getPrice();
                $amount_in_cents = $_product->getPrice() * 100;

                $data3 = array(
                    'appears_on_statement_as' => 'AptDeco',
                    'amount'                  => $amount_in_cents,
                    'description'             => 'Description',
                    'on_behalf_of_uri'        => Ikantam_Balanced_Settings::getOnBehalfOfUri()
                );

                $this->_debit = $buyer->createDebit($data3, $marketplace);
            }

            $this->setUserId($user->getId())
                    ->setCardNumber($card->getLastFour())
                    ->setAmount($cartTotal)
                    ->setCreatedAt(time())
                    ->save();
        } catch (Ikantam_Balanced_Exception $exc) {
            $this->logException($exc);
            throw $exc;
        } catch (Exception $exc) {
            $this->logException($exc);
            throw new Ikantam_Balanced_Exception("Cannot process your payment. Please, try again later.");
        }

        return $this;
    }

}