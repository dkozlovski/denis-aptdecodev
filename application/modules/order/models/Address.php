<?php
/**
 * Class Order_Model_Address
 * @method Order_Model_Address_Backend _getbackend()
 *
 * @method int|null getOrderId()
 * @method int|null getUserId()
 * @method int|null getUserAddressId()
 * @method string getCountryCode()
 * @method string getState()
 * @method string getCity()
 * @method string getPostcode()
 * @method string getFullName()
 * @method string getAddressLine1()
 * @method string|null getAddressLine2()
 * @method string|null getEmail()
 * @method string|null getTelephone()
 * @method string|null getAddressType()
 * @method string getStateCode()
 * @method string getStateName()
 * @method string getPostalCode()
 * @method string|null getBuildingType()
 * @method int|null getNumberOfStairs()
 *
 * @method Order_Model_Address setOrderId(int $value)
 * @method Order_Model_Address setUserId(int $value)
 * @method Order_Model_Address setUserAddressId(int $value)
 * @method Order_Model_Address setCountryCode(string $value)
 * @method Order_Model_Address setState(string $value)
 * @method Order_Model_Address setCity(string $value)
 * @method Order_Model_Address setPostcode(string $value)
 * @method Order_Model_Address setFullName(string $value)
 * @method Order_Model_Address setAddressLine1(string $value)
 * @method Order_Model_Address setAddressLine2(string $value)
 * @method Order_Model_Address setEmail(string $value)
 * @method Order_Model_Address setTelephone(string $value)
 * @method Order_Model_Address setAddressType(string $value)
 * @method Order_Model_Address setStateCode(string $value)
 * @method Order_Model_Address setStateName(string $value)
 * @method Order_Model_Address setPostalCode(string $value)
 * @method Order_Model_Address setBuildingType(string $value)
 * @method Order_Model_Address setNumberOfStairs(int $value)
 */
class Order_Model_Address extends Application_Model_Abstract
{

    protected function _beforeSave()
    {
        if (!$this->getId() && $this->getUserAddressId()) {
            $address = new User_Model_Address($this->getUserAddressId());
            
            $state = $address->getData('state');
            
            if ($address->getData('state_code') && !$address->getData('state')) {
                $state = $address->getData('state_code');
            }
            
            if ($address->getData('state') && !$address->getData('state_code')) {
                $state = $address->getData('state');
                $address->setData('state_code', $state);
            }
            
            $address->setData('state', $state);

            $this->setCountryCode($address->getData('country_code'))
                    ->setStateCode($address->getData('state_code'))
                    ->setStateName($address->getData('state'))
                    ->setCity($address->getData('city'))
                    ->setPostalCode($address->getData('postcode'))
                    ->setFullName($address->getData('full_name'))
                    ->setAddressLine1($address->getData('address_line1'))
                    ->setAddressLine2($address->getData('address_line2'))
                    ->setBuildingType($address->getData('building_type'))
                    ->setTelephone($address->getData('phone_number'))
                    ->setNumberOfStairs($address->getData('number_of_stairs'));
        }
    }

}
