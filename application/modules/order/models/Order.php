<?php
/**
 * Class Order_Model_Order
 * @method Order_Model_Order_Backend _getbackend()
 *
 * @method int|null getUserId()
 * @method int getCreatedAt()
 * @method float getTotal()
 * @method int|null getPaymentId()
 * @method string|null getHowYouHeard()
 * @method string|null getUserFullName()
 * @method string|null getUserEmail()
 * @method string|null getCouponId()
 * @method int|null getBillingAddressId()
 * @method int|null getShippingAddressId()
 * @method int getFreeShipping()
 * @method float getSubtotalAmount()
 * @method float getShippingAmount()
 * @method float getDiscountAmount()
 * @method float getTransactionAmount()
 * @method float getTotalAmount()
 * @method int getUpdatedAt()
 *
 * @method Order_Model_Order setUserId(int $value)
 * @method Order_Model_Order setCreatedAt(int $value)
 * @method Order_Model_Order setTotal(float $value)
 * @method Order_Model_Order setPaymentId(int $value)
 * @method Order_Model_Order setHowYouHeard(string $value)
 * @method Order_Model_Order setUserFullName(string $value)
 * @method Order_Model_Order setUserEmail(string $value)
 * @method Order_Model_Order setCouponId(string $value)
 * @method Order_Model_Order setBillingAddressId(int $value)
 * @method Order_Model_Order setShippingAddressId(int $value)
 * @method Order_Model_Order setFreeShipping(int $value)
 * @method Order_Model_Order setSubtotalAmount(float $value)
 * @method Order_Model_Order setShippingAmount(float $value)
 * @method Order_Model_Order setDiscountAmount(float $value)
 * @method Order_Model_Order setTransactionAmount(float $value)
 * @method Order_Model_Order setTotalAmount(float $value)
 * @method Order_Model_Order setUpdatedAt(int $value)
 */
class Order_Model_Order extends Application_Model_Abstract
{

    const ORDER_STATUS_ON_HOLD           = 'holded';
    const ORDER_STATUS_COMPLETE          = 'complete';
    const ORDER_STATUS_CANCELED          = 'canceled';
    const ORDER_STATUS_PENDING           = 'pending';
    const ORDER_STATUS_CLOSED            = 'closed';
    const ORDER_SHIPPING_METHOD_PICK_UP  = 'pickup';
    const ORDER_SHIPPING_METHOD_DELIVERY = 'delivery';

    protected $_itemsForDelivery;
    protected $_itemsForPickUp;
    protected $_user;
    protected $_items = array();
    protected $shippingAddress;
    protected $billingAddress;
    protected $_eventDispatcher;
    
    public function __construct($id = null)
    {
        parent::__construct($id);

        /* $holds = \Zend_Registry::get('config')->order->holds;

          foreach ($holds as $hold) {
          $this->addHoldCreator(new $hold);
          } */
    }

    public function getAllItems()
    {
        $items = new Order_Model_Item_Collection();
        $items->getByOrderId($this->getId());
        return $items;
    }

    public function getItemsForDelivery()
    {
        if (!$this->_itemsForDelivery) {
            $this->_itemsForDelivery = new Order_Model_Item_Collection();
            $this->_itemsForDelivery->getDeliveryByOrderId($this->getId());
        }

        return $this->_itemsForDelivery;
    }

    public function getItemsForPickUp()
    {
        if (!$this->_itemsForPickUp) {
            $this->_itemsForPickUp = new Order_Model_Item_Collection();
            $this->_itemsForPickUp->getPickUpByOrderId($this->getId());
        }

        return $this->_itemsForPickUp;
    }

    public function getUser()
    {
        if (!$this->_user) {
            if ($this->getUserId()) {
                $this->_user = new User_Model_User($this->getUserId());
            } else {
                $this->_user = new User_Model_User();
                $this->_user->setFullName($this->getUserFullName())
                        ->setEmail($this->getUserEmail());
            }
        }

        return $this->_user;
    }

    public function createHolds($customer)
    {
        foreach ($this->getHoldCreators() as $hold) {
            $hold->setCustomer($customer)
                    ->setOrder($this)
                    ->create();
        }

        return $this;
    }
    
    public function getShippingAddress()
    {
        if (!$this->shippingAddress) {
            $this->shippingAddress = new Order_Model_Address($this->getShippingAddressId());
        }
        return $this->shippingAddress;
    }
    
    public function getBillingAddress()
    {
        if (!$this->billingAddress) {
            $this->billingAddress = new Order_Model_Address($this->getBillingAddressId());
        }
        return $this->billingAddress;
    }

    public function createFromCart(Cart_Model_Cart $cart)
    {
        $this->setFreeShipping($cart->getFreeShipping())
                ->setSubtotalAmount($cart->getSubtotalAmount())
                ->setShippingAmount($cart->getShippingAmount())
                ->setDiscountAmount($cart->getDiscountAmount())
                ->setTransactionAmount($cart->getTransactionAmount())
                ->setTotalAmount($cart->getTotalAmount())
                ->setCouponId($cart->getCouponId());

        if ($cart->getShippingAddressId()) {
            $shippingAddress = new Order_Model_Address();
            
            $shippingAddress->setUserAddressId($cart->getShippingAddressId())
                    ->setAddressType('shipping')
                    ->save();
            
            $this->setShippingAddressId($shippingAddress->getId());
        }

        if ($cart->getBillingAddressId()) {
            $billingAddress = new Order_Model_Address();
            
            $billingAddress->setUserAddressId($cart->getBillingAddressId())
                    ->setAddressType('billing')
                    ->save();
            
            $this->setBillingAddressId($billingAddress->getId());
        }

        /* foreach ($cart->getItemsForCheckout() as $item) {
          $orderItem = new Order_Model_Item();

          $orderItem->fromCartItem($item)
          ->setShippingAmount(0)//@TODO
          ->setIsReceived(false)
          ->setIsProductAvailable(false)
          ->setStatus(Order_Model_Order::ORDER_STATUS_ON_HOLD)
          ->setIsNotified(false);

          $this->addItem($orderItem);
          } */

        return $this;
    }

    public function getItems()
    {
        return $this->_items;
    }

    public function addItem($item)
    {
        $this->_items[] = $item;
        return $this;
    }

    /* protected function _afterSave()
      {
      foreach ($this->getItems() as $item) {
      $item->setOrderId($this->getId())->save();
      }
      } */

    public function getHoldCreators()
    {
        return $this->_holdCreators;
    }

    public function setHoldCreators(array $holds)
    {
        $this->_holdCreators = $holds;
        return $this;
    }

    public function addHoldCreator($hold)
    {
        $this->_holdCreators[] = $hold;
        return $this;
    }

    public function getIsFreeShipping()
    {
        return $this->_isFreeShipping;
    }

    public function setIsFreeShipping($isFreeShipping)
    {
        $this->_isFreeShipping = $isFreeShipping;
        return $this;
    }
    
    public function getProduct()
    {
        return $this->_getbackend()->getProduct($this->getData('id'));
    }
    /**
     * Get event dispatcher instance
     *
     * @return Application_Model_Event_Dispatcher
     */
    public function getEventDispatcher()
    {
        if (!$this->_eventDispatcher) {
            $this->_eventDispatcher = new Application_Model_Event_Dispatcher();
        }
        return $this->_eventDispatcher;
    }

    /**
     * Set event dispatcher instance
     *
     * @param Zend_EventManager_EventCollection $eventDispatcher
     * @return Product_Model_Product
     */
    public function setEventDispatcher(Zend_EventManager_EventCollection $eventDispatcher)
    {
        $this->_eventDispatcher = $eventDispatcher;
        return $this;
    }

}
