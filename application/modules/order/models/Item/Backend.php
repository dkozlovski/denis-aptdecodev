<?php

class Order_Model_Item_Backend extends Application_Model_Abstract_Backend
{

    protected $_table   = 'order_items';
    protected $_primary = 'id';
    protected $_fields  = array(
        'order_id',
        'product_id',
        'product_title',
        'product_price',
        'qty',
        'row_total',
        'shipping_method_id',
        'shipping_amount',
        'discount_amount',
        'options',
        'is_received',
        'is_delivered',
        'is_available',
        'is_product_available',
        'status',
        'hold_uri',
        'debits_uri',
        'additional_hold_uri',
        'additional_debits_uri',
        'is_notified',
        'access_code',
        'is_carrier_sent_email',
        'carrier_work_number',
        'shipping_status',
        'follow_up',
        'notes'
    );

    protected function _beforeInsert($object)
    {
        $object->setCreatedAt(time());
        $object->setUpdatedAt(time());

        if ($object->getIsCarrierSentEmail() === null) {
            $object->setIsCarrierSentEmail(0);
        }

        if ($object->getShippingStatus() === null) {
            $object->setShippingStatus('');
        }
    }

    protected function _beforeUpdate($object)
    {
        $object->setUpdatedAt(time());

        if ($object->getIsCarrierSentEmail() === null) {
            $object->setIsCarrierSentEmail(0);
        }

        if ($object->getShippingStatus() === null) {
            $object->setShippingStatus('');
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $this->_beforeInsert($object);

        $template = 'INSERT INTO `%s` (%s) VALUES (%s)';
        $bind     = array();
        $fields   = '`' . implode('`, `', $this->_fields) . '`';
        $values   = ':' . implode(', :', $this->_fields);

        foreach ($this->_fields as $field) {
            $bind[':' . $field] = $object->getData($field);
        }

        $sql  = sprintf($template, $this->_getTable(), $fields, $values);
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $this->_beforeUpdate($object);

        $bind = array();

        $template = 'UPDATE `%s` SET %s WHERE %s';

        $set = array();
        foreach ($this->_fields as $field) {
            $set[]              = sprintf('`%s` = :%s', $field, $field);
            $bind[':' . $field] = $object->getData($field);
        }

        $where                       = sprintf('`%s` = :%s', $this->_primary, $this->_primary);
        $bind[':' . $this->_primary] = $object->getData($this->_primary);
        $sql                         = sprintf($template, $this->_getTable(), implode(', ', $set), $where);

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
    }

    public function getByProductId($id, $object)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `product_id` = :product_id LIMIT 1;';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':product_id', $id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function getLastWeekByProductId($id, $object)
    {
        $sql = 'SELECT * FROM `order_items`
                JOIN `orders` ON `order_items`.`order_id` = `orders`.`id`
                WHERE `order_items`.`product_id` = :product_id and `orders`.`created_at` > :time
                ORDER BY `orders`.`created_at` DESC LIMIT 1';

        $stmt = $this->_getConnection()->prepare($sql);

        $time = time() - 604800 * 2; //7*2 = 14 days

        $stmt->bindParam(':product_id', $id);
        $stmt->bindParam(':time', $time);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function getByOrderId($orderId, $collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `order_id` = :order_id;';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':order_id', $orderId);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new $this->_itemClass();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getDeliveryByOrderId($orderId, $collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `order_id` = :order_id AND `shipping_method_id` = "delivery";';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':order_id', $orderId);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new $this->_itemClass();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getPickUpByOrderId($orderId, $collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `order_id` = :order_id AND `shipping_method_id` = "pickup";';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':order_id', $orderId);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new $this->_itemClass();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getForAvailabilityCron($collection)
    {
        $sql = 'SELECT `order_items`.* FROM `order_items`
            JOIN `orders` ON `order_items`.`order_id` = `orders`.`id` 
            WHERE `order_items`.`is_available` IS NULL AND `orders`.`created_at` < :created_at
            LIMIT 5';

        $stmt     = $this->_getConnection()->prepare($sql);
        $createAt = time() - 24 * 3600;
        $stmt->bindParam('created_at', $createAt);
        $stmt->execute();
        $rows     = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Order_Model_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getForBeforeDeliveryCron($collection)
    {
        $sql = 'SELECT `order_items`.* FROM `order_items`
            WHERE `order_items`.`is_available` = 1 AND `is_delivered` = 0
            AND `is_received` = 0 AND `is_notified` = 0 AND `shipping_method_id` = "delivery"';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Order_Model_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getForAfterDeliveryCron($collection)
    {
        $sql = 'SELECT * FROM `order_items`
            WHERE `is_product_available` = 1 AND `is_delivered` = 0
            AND `is_received` = 0 AND `is_notified` = 1 AND `shipping_method_id` = "delivery"';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Order_Model_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function hasUserPurchasedProduct($userId, $productId)
    {
        $sql = "SELECT  `oi`.`id` AS `id`
            FROM `order_items` AS `oi`
            JOIN `orders` AS `o` ON `oi`.`order_id` = `o`.`id`
            WHERE `o`.`user_id` = :user_id AND  `oi`.`product_id` = :product_id
            LIMIT  1";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam('user_id', $userId);
        $stmt->bindParam('product_id', $productId);

        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_COLUMN, 'id');
        return count($res) > 0;
    }
}