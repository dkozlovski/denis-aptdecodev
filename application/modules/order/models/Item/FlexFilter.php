<?php


/**
 * @method Order_Model_Item_FlexFilter user_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_user_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter order_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_order_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter product_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_product_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter title (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_title (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter product_title (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_product_title (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter price (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_price (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter product_price (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_product_price (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter qty (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_qty (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter row_total (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_row_total (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter shipping_method_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_shipping_method_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter shipping_method (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_shipping_method (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter shipping_address_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_shipping_address_id (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter shipping_amount (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_shipping_amount (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter discount_amount (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_discount_amount (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter shipping_price (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_shipping_price (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter options (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_options (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter is_received (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_is_received (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter is_delivered (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_is_delivered (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter is_product_available (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_is_product_available (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter is_available (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_is_available (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter status (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_status (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter hold_uri (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_hold_uri (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter debits_uri (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_debits_uri (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter is_notified (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_is_notified (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter created_at (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_created_at (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter updated_at (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_updated_at (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter access_code (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_access_code (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter is_carrier_sent_email (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_is_carrier_sent_email (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter carrier_work_number (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_carrier_work_number (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter follow_up (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_follow_up (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter shipping_status (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_shipping_status (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter notes (string $operator, mixed $value)
 * @method Order_Model_Item_FlexFilter or_notes (string $operator, mixed $value)
 * @method Order_Model_Item_Collection apply(int $limit = null, int $offset = null)
 */

class Order_Model_Item_FlexFilter extends Ikantam_Filter_Flex
{
    protected $_acceptClass = 'Order_Model_Item_Collection';
    protected $_joins       = array(
        'product' => 'INNER JOIN `products` `product` ON `product`.`id` = `order_items`.`product_id`',
        'user' => array('product' => 'INNER JOIN `users` `user` ON `product`.`user_id` = `user`.`id`')
    );
    protected $_select      = array(
        'user' => '`user`.`id` AS `user_id`'
    );
    protected $_rules       = array(
        'user_id' => array('user' => '`user`.`id`')
    );

}