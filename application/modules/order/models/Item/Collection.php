<?php

class Order_Model_Item_Collection extends Application_Model_Abstract_Collection
    implements Ikantam_Filter_Flex_Interface
{

    public function getForAvailabilityCron()
    {
        $this->_getBackend()->getForAvailabilityCron($this);
		return $this;
    }
    
    public function getForBeforeDeliveryCron()
    {
        $this->_getBackend()->getForBeforeDeliveryCron($this);
		return $this;
    }
    
    public function getForAfterDeliveryCron()
    {
        $this->_getBackend()->getForAfterDeliveryCron($this);
		return $this;
    }

	public function getByOrderId($orderId)
	{
		$this->_getBackend()->getByOrderId($orderId, $this);
		return $this;
	}
	
	public function getDeliveryByOrderId($orderId)
	{
		$this->_getBackend()->getDeliveryByOrderId($orderId, $this);
		return $this;
	}
	
	public function getPickUpByOrderId($orderId)
	{
		$this->_getBackend()->getPickUpByOrderId($orderId, $this);
		return $this;
	}

    /**
     * Returns a filter related with collection.
     * @return Order_Model_Item_FlexFilter
     */
    public function  getFlexFilter()
    {
        return new Order_Model_Item_FlexFilter($this);
    }

    /**
     * @return Order_Model_Item_FlexFilter
     */
    public static function flexFilter()
    {
        $inst = new static;
        return $inst->getFlexFilter();
    }
}

