<?php
/**
 * Class Order_Model_Item_Abstract
 * @method Order_Model_Item_Backend _getbackend()
 *
 * @method int getOrderId()
 * @method int|null getProductId()
 * @method string getTitle()
 * @method float getPrice()
 * @method string getShippingMethod()
 * @method int|null getShippingAddressId()
 * @method float|null getShippingPrice()
 * @method string|null getOptions()
 * @method string|null getHoldUri()
 * @method string|null getDebitsUri()
 * @method string|null getAdditionalHoldUri()
 * @method string|null getAdditionalDebitsUri()
 * @method int getIsDelivered()
 * @method int getIsReceived()
 * @method int|null getIsAvailable()
 * @method string|null getStatus()
 * @method int getQty()
 * @method int getIsNotified()
 * @method float getRowTotal()
 * @method string|null getShippingMethodId()
 * @method float getShippingAmount()
 * @method float getDiscountAmount()
 * @method int|null getIsProductAvailable()
 * @method int getCreatedAt()
 * @method int getUpdatedAt()
 * @method |null getAccessCode()
 * @method int getIsCarrierSentEmail()
 * @method string|null getCarrierWorkNumber()
 * @method string|null getFollowUp()
 * @method string getShippingStatus()
 * @method string|null getNotes()
 *
 * @method Order_Model_Item setOrderId(int $value)
 * @method Order_Model_Item setProductId(int $value)
 * @method Order_Model_Item setTitle(string $value)
 * @method Order_Model_Item setPrice(float $value)
 * @method Order_Model_Item setShippingMethod(string $value)
 * @method Order_Model_Item setShippingAddressId(int $value)
 * @method Order_Model_Item setShippingPrice(float $value)
 * @method Order_Model_Item setOptions(string $value)
 * @method Order_Model_Item setHoldUri(string $value)
 * @method Order_Model_Item setDebitsUri(string $value)
 * @method Order_Model_Item setAdditionalHoldUri(string $value)
 * @method Order_Model_Item setAdditionalDebitsUri(string $value)
 * @method Order_Model_Item setIsDelivered(int $value)
 * @method Order_Model_Item setIsReceived(int $value)
 * @method Order_Model_Item setIsAvailable(int $value)
 * @method Order_Model_Item setStatus(string $value)
 * @method Order_Model_Item setQty(int $value)
 * @method Order_Model_Item setIsNotified(int $value)
 * @method Order_Model_Item setProductTitle(string $value)
 * @method Order_Model_Item setProductPrice(float $value)
 * @method Order_Model_Item setRowTotal(float $value)
 * @method Order_Model_Item setShippingMethodId(string $value)
 * @method Order_Model_Item setShippingAmount(float $value)
 * @method Order_Model_Item setDiscountAmount(float $value)
 * @method Order_Model_Item setIsProductAvailable(int $value)
 * @method Order_Model_Item setCreatedAt(int $value)
 * @method Order_Model_Item setUpdatedAt(int $value)
 * @method Order_Model_Item setAccessCode( $value)
 * @method Order_Model_Item setIsCarrierSentEmail(int $value)
 * @method Order_Model_Item setCarrierWorkNumber(string $value)
 * @method Order_Model_Item setFollowUp(string $value)
 * @method Order_Model_Item setShippingStatus(string $value)
 * @method Order_Model_Item setNotes(string $value)
 */
class Order_Model_Item_Abstract extends Application_Model_Abstract
{
    protected $_product = null;
    protected $_payout = null;
    protected $_order = null;

    public function getProductTitle()
    {
        if ($this->getProduct()->getId()) {
            return $this->getProduct()->getTitle();
        }

        return $this->getData('product_title');
    }

    public function getProductPrice()
    {
        if ($this->getProduct()->getId()) {
            return $this->getProduct()->getPrice();
        }

        return $this->getData('product_price');
    }

    public function getBuyerDate()
    {
        $options = unserialize($this->getOptions());
        return isset($options['buyer_date']) ? $options['buyer_date'] : $this->getSellerDates();
    }

    public function setBuyerDate($date)
    {
        $options               = unserialize($this->getOptions());
        $options['buyer_date'] = $date;
        $this->setOptions(serialize($options));
        return $this;
    }

    public function getBuyerDatesForSeller()
    {
        $dates = $this->getBuyerDates();

        $deliveryDate = new Checkout_Model_DeliveryDate();

        if (!is_array($dates)) {
            return $dates;
        }

        $isMultipleItems =  count($this->getOrder()->getItemsForDelivery()) > 1;

        $newFullDates = array();
        foreach ($dates as $_fullDate) {
            $pos = strpos($_fullDate, ' ');
            $_date = substr($_fullDate, 0, $pos);
            $_time = substr($_fullDate, $pos + 1);

            // correct already non-existent time
            if ($_time == '9-13') {
                $_time = '7-12';
            } elseif ($_time == '17-21') {
                $_time = '16-20';
            }

            /* @see https://docs.google.com/spreadsheets/d/1X6WAHGWVECBrjAMqrDdcjGSy_d8G-vgLRkokEybpxLk/edit#gid=738547738 */
            if ($isMultipleItems) {
                $_newTimes = $deliveryDate->getDeliveryTimesForSellerForMultipleItems($this->getPickupPostcode(), $this->getDeliveryPostcode(), $_time);
            } else {
                $_newTimes = $deliveryDate->getDeliveryTimesForSellerForOnceItem($this->getPickupPostcode(), $this->getDeliveryPostcode(), $_time);
            }
            foreach ($_newTimes as $_newTime) {
                $_newFullDate = $_date . ' '. $_newTime;
                if (array_search($_newFullDate, $newFullDates) === false) {
                    $newFullDates[$_fullDate . '|' . $_newFullDate] = $_newFullDate;
                }
            }
        }

        if (!$newFullDates) {
            // for old orders
            foreach ($dates as $_fullDate) {
                $newFullDates[$_fullDate . '|' . $_fullDate] = $_fullDate;
            }
        }

        foreach ($newFullDates as $_newFullDate => $_title) {
            $_arr =  explode('|', $_newFullDate);
            $_sellerDateTime = $_arr[1];

            $_sellerDate = substr($_sellerDateTime, 0, 10);
            $_sellerTime = trim(substr($_sellerDateTime, 10));

            $pickupDate = new  Admin_Model_PickupDate();
            $pickupDate->getByDate(strtotime($_sellerDate));

            if (count($this->getOrder()->getItems()) == 1 && $this->getQty() == 1 && $this->getProduct()->getData('is_under_15_pounds')) {

                if ($_sellerTime == '7-12' && !$pickupDate->getIsFirstPeriodForSmallAvailable()) {
                    unset ($newFullDates[$_newFullDate]);
                } elseif ($_sellerTime == '12-16' && !$pickupDate->getIsSecondPeriodForSmallAvailable()) {
                    unset ($newFullDates[$_newFullDate]);
                } elseif ($_sellerTime == '16-20' && !$pickupDate->getIsThirdPeriodForSmallAvailable()) {
                    unset ($newFullDates[$_newFullDate]);
                }
            } else {
                if ($_sellerTime == '7-12' && !$pickupDate->getIsFirstPeriodAvailable()) {
                    unset ($newFullDates[$_newFullDate]);
                } elseif ($_sellerTime == '12-16' && !$pickupDate->getIsSecondPeriodAvailable()) {
                    unset ($newFullDates[$_newFullDate]);
                } elseif ($_sellerTime == '16-20' && !$pickupDate->getIsThirdPeriodAvailable()) {
                    unset ($newFullDates[$_newFullDate]);
                }
            }

        }

        return $newFullDates;
    }

    public function getBuyerDates()
    {
        $options = unserialize($this->getOptions());
        return isset($options['buyer_dates']) ? $options['buyer_dates'] : null;
    }

    public function setBuyerDates($dates)
    {
        $options                = unserialize($this->getOptions());
        $options['buyer_dates'] = $dates;
        $this->setOptions(serialize($options));
        return $this;
    }

    public function getSellerDates()
    {
        $options = unserialize($this->getOptions());
        return isset($options['seller_dates']) ? $options['seller_dates'] : null;
    }

    public function setSellerDates($dates)
    {
        $options                 = unserialize($this->getOptions());
        $options['seller_dates'] = $dates;
        $this->setOptions(serialize($options));
        return $this;
    }

    public function getDeliveryOptions()
    {
        $options = unserialize($this->getOptions());
        return isset($options['delivery_options']) ? $options['delivery_options'] : null;
    }

    public function setDeliveryOptions($deliveryOptions)
    {
        $options                     = unserialize($this->getOptions());
        $options['delivery_options'] = $deliveryOptions;
        $this->setOptions(serialize($options));
        return $this;
    }

    public function getSellerDeliveryOptions()
    {
        $options = unserialize($this->getOptions());
        return isset($options['seller_delivery_options']) ? $options['seller_delivery_options'] : null;
    }

    public function setSellerDeliveryOptions($deliveryOptions)
    {
        $options                            = unserialize($this->getOptions());
        $options['seller_delivery_options'] = $deliveryOptions;
        $this->setOptions(serialize($options));
        return $this;
    }

    /**
     * @return Product_Model_Product
     */
    public function getProduct()
    {
        if ($this->_product === null) {
            $this->_product = new Product_Model_Product($this->getProductId());
        }
        return $this->_product;
    }

    public function getPayout()
    {
        if ($this->_payout === null) {
            $this->_payout = new Order_Model_Payout();
            $this->_payout->getByItemId($this->getId());
        }
        return $this->_payout;
    }

    public function getOrder()
    {
        if ($this->_order === null) {
            $this->_order = new Order_Model_Order($this->getOrderId());
        }
        return $this->_order;
    }

    public function getPickupPostcode()
    {
        return $this->getProduct()->getPickupPostcode();
    }

    public function getDeliveryPostcode()
    {
        return $this->getOrder()->getShippingAddress()->getPostalCode();
    }


    public function getPickupBoroughName()
    {
        $deliveryDate = new Checkout_Model_DeliveryDate();
        return $deliveryDate->getBoroughNameByPostcode($this->getPickupPostcode());
    }

    public function getDeliveryBoroughName()
    {
        $deliveryDate = new Checkout_Model_DeliveryDate();
        return $deliveryDate->getBoroughNameByPostcode($this->getDeliveryPostcode());
    }

    public function hasUserPurchasedProduct($userId, $productId)
    {
        return $this->_getBackend()->hasUserPurchasedProduct($userId, $productId);
    }
}