<?php

class Order_Model_Hold_Item extends Application_Model_Hold_Abstract
{

    public function getType()
    {
        return parent::HOLD_TYPE_ITEM;
    }

    public function create()
    {
        $subtotalRules    = new Cart_Model_Totals_Rule_Subtotal();
        $transactionRules = new Cart_Model_Totals_Rule_Transaction();
        $coupon           = $this->getOrder()->getCoupon();
        $items            = $this->getOrder()->getItems();

        $subtotals    = $subtotalRules->setItems($items)->getFees();
        $transactions = $transactionRules->setItems($items)->getFees();

        $discount = 0;

        if ($coupon->getId() && $coupon->getType() == 'percent') {
            $discount = $coupon->getAmount();
        } elseif ($coupon->getId() && $coupon->getType() == 'fixed') {
            //@TODO
        }

        foreach ($items as $item) {
            $subtotal    = $subtotals[$item->getId()];
            $transaction = $transactions[$item->getId()];
            $amount      = $subtotal - $subtotal * $discount + $transaction;

            $this->_createHold($amount);
        }
    }

    protected function _createHold($amount)
    {
        $buyer = $this->getCustomer()->getBalancedAccount();
        $hold  = $buyer->createHold($amount);

        $hold->setOrder($this->getOrder())
                ->setType($this->getType())
                ->save();
    }

}