<?php

class Order_Model_Hold_Shipping extends Application_Model_Hold_Abstract
{

    public function getType()
    {
        return parent::HOLD_TYPE_SHIPPING;
    }

    public function create()
    {
        //shipping coupon applied
        if ($this->getOrder()->getFreeShipping()) {
            return true;
        }

        $items  = $this->getOrder()->getItems();
        $coupon = $this->getOrder()->getCoupon();

        $rules = new Order_Model_Totals_Rule_Shipping();

        $fees = $rules->setItems($items)->getFees();

        $discount = 0;

        if ($coupon->getId() && $coupon->getType() == 'percent') {
            $discount = $coupon->getAmount();
        }

        foreach ($fees as $fee) {
            $amount = $fee - $fee * $discount;
            $this->_createHold($amount);
        }

        return true;
    }

    protected function _createHold($amount)
    {
        $buyer = $this->getCustomer()->getBalancedAccount();
        $hold  = $buyer->createHold($amount);

        $hold->setOrder($this->getOrder())
                ->setType($this->getType())
                ->save();
    }

}