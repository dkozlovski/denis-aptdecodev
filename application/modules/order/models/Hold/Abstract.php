<?php

abstract class Application_Model_Hold_Abstract
{

    const HOLD_TYPE_SHIPPING = 'shipping';
    const HOLD_TYPE_ITEM     = 'item';

    protected $_tableName = 'holds';
    protected $_primary   = 'id';
    protected $_dbTable;
    protected $_order;
    protected $_items;

    public function setOrder($order)
    {
        $this->_order = $order;
        return $this;
    }

    public function getOrder()
    {
        return $this->_order;
    }

    public function setItems($items)
    {
        $this->_items = $items;
        return $this;
    }

    public function getItems()
    {
        return $this->_items;
    }

    /**
     * Get DB table instance
     * 
     * @return Zend_Db_Table_Abstract
     */
    public function getDbTable()
    {
        if (!$this->_dbTable) {
            $this->_dbTable = new Zend_Db_Table();
            $this->_dbTable->setOptions(array(
                'name'    => $this->getTableName(),
                'primary' => $this->getPrimary()
            ));
        }

        return $this->_dbTable;
    }

    public function getFields()
    {
        return $this->_fields;
    }

    public function getTableName()
    {
        return $this->_tableName;
    }

    public function getPrimary()
    {
        return $this->_primary;
    }

    abstract public function getType();

    public function save()
    {
        if ($this->getData('is_deleted')) {
            $this->_delete();
        } elseif ($this->getData('id')) {
            $this->_update();
        } else {
            $this->_insert();
        }
    }

    protected function _update()
    {
        $this->_table->update($this->getData());
    }

    protected function _insert()
    {
        $id = $this->_table->insert($this->getData());
        $this->setData('id', $id);
    }

}