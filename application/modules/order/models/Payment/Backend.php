<?php

class Order_Model_Payment_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'payments';
	
	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `' . $this->_getTable() . '` (`user_id` , `card_number`, `amount`, `created_at`) VALUES (:user_id , :card_number, :amount, :created_at);';
		
		$stmt = $this->_getConnection()->prepare($sql);
		
		$userId = $object->getUserId();
		$cardNumber = $object->getCardNumber();
		$amount = $object->getAmount();
		$createdAt = $object->getCreatedAt();
		
		
		$stmt->bindParam(':user_id', $userId);
		$stmt->bindParam(':card_number', $cardNumber);
		$stmt->bindParam(':amount', $amount);
		$stmt->bindParam(':created_at', $createdAt);
		
		
		$stmt->execute();
		
		$object->setId($this->_getConnection()->lastInsertId());
		
	}
	
	protected function _update(\Application_Model_Abstract $object)
	{
		//$sql = 'UPDATE `' . $this->_getTable() . '` SET `user_id` = :user_id, `created_at` = :created_at, `total` = : :total WHERE `id` = :id;';
		
		$stmt = $this->_getConnection()->prepare($sql);
		
		$id = $object->getId();
		$userId = $object->getUserId();
		$createdAt = $object->getCreatedAt();
		$total = $object->getTotal();
		
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':user_id', $userId);
		$stmt->bindParam(':created_at', $createdAt);
		$stmt->bindParam(':total', $total);
		
		$stmt->execute();
	}

}

