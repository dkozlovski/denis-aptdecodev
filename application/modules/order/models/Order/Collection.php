<?php

class Order_Model_Order_Collection extends Application_Model_Abstract_Collection
    implements Ikantam_Filter_Flex_Interface
{
    /**
     * Returns a filter related with collection.
     * @return object Ikantam_Filter_Flex
     */
    public function  getFlexFilter()
    {
        return new Order_Model_Order_FlexFilter($this);
    }
}

