<?php

class Order_Model_Order_Backend extends Application_Model_Abstract_Backend
{

    protected $_table   = 'orders';
    protected $_primary = 'id';
    protected $_fields  = array(
        'user_id',
        'user_full_name',
        'user_email',
        'coupon_id',
        'billing_address_id',
        'shipping_address_id',
        'free_shipping',
        'subtotal_amount',
        'shipping_amount',
        'discount_amount',
        'transaction_amount',
        'total_amount',
        'created_at',
        'updated_at',
        'how_you_heard',
    );

    protected function _beforeInsert($object)
    {
        $object->setCreatedAt(time());
        $object->setUpdatedAt(time());
    }

    protected function _beforeUpdate($object)
    {
        $object->setUpdatedAt(time());
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $this->_beforeInsert($object);

        $template = 'INSERT INTO `%s` (%s) VALUES (%s)';
        $bind     = array();
        $fields   = '`' . implode('`, `', $this->_fields) . '`';
        $values   = ':' . implode(', :', $this->_fields);

        foreach ($this->_fields as $field) {
            $bind[':' . $field] = $object->getData($field);
        }

        $sql = sprintf($template, $this->_getTable(), $fields, $values);

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bind);

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $this->_beforeUpdate($object);

        $bind = array();

        $template = 'UPDATE `%s` SET %s WHERE %s';

        $set = array();
        foreach ($this->_fields as $field) {
            $set[]              = sprintf('`%s` = :%s', $field, $field);
            $bind[':' . $field] = $object->getData($field);
        }

        $where                       = sprintf('`%s` = :%s', $this->_primary, $this->_primary);
        $bind[':' . $this->_primary] = $object->getData($this->_primary);
        $sql                         = sprintf($template, $this->_getTable(), implode(', ', $set), $where);

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
    }
    
    public function getProduct($order_id){
        
        $sql = 'SELECT * FROM `order_items` WHERE `order_id` = :order_id';
        
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':order_id', $order_id);
        $stmt->execute();
        $row = $stmt->fetch();

        return new Product_Model_Product($row['product_id']);
        
    }

}
