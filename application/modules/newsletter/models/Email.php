<?php

/**
 * @author Aleksei Poliushkin
 * @copyright iKantam 2013/8/2
 */
class Newsletter_Model_Email extends Application_Model_Abstract {
  
  protected $_backendClass = 'Newsletter_Model_Backend_Email';
  protected static $_instance ; 
  
  /**
   * Checks if current user have subscription
   *
   * @return bool 
   */
  public static function isUserSubscribed()
  {
    if(!self::$_instance) {
        self::$_instance = new self;
    }
    $inst = self::$_instance;
    if(null === $inst->getIsUserSubscribed()) {
        $sess = User_Model_Session::instance();
        
        if(!$sess->isLoggedIn()){
            $inst->setIsUserSubscribed(false);
        } else {
           $inst->getBy_user_id($sess->getUser()->getId());
           $inst->setIsUserSubscribed($inst->isExists());    
        }
    }
    
    return $inst->getIsUserSubscribed();
  }
  
  public function __toString () {
    return (string) $this->getEmail('');
  }
 
}