<?php

/**
 * @author Aleksei Poliushkin
 * @copyright iKantam 2013/8/2
 */
 
class Newsletter_Model_Backend_Email extends Application_Model_Abstract_Backend {
    
    protected $_table = 'newsletter_emails';
    
    protected function _insert(\Application_Model_Abstract $object)
    {
        $stmt = $this->_prepareSql(
        "INSERT INTO `". $this->_getTable() ."` (`id`, `user_id`, `email`, `unsubscribe_code`) VALUES(NULL, :uid, :email, :code)");
        
        $email = $object->getEmail();        
        $uid = $object->getUserId();
        $code = $object->getUnsubscribeCode();
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':uid', $uid);
        $stmt->bindParam(':code', $code);
        $stmt->execute();
        
      /*  try {
            $stmt->execute();
            } catch(PDOException $ex) {
                if($ex->errorInfo[1] == 1062) {
                    User_Model_Session::instance()->setFormErrors('email', 'Email already taken.');
                } else {
                    throw $ex;
                }
            } */ 
    }
    
    protected function _update(\Application_Model_Abstract $object)
    {
        $stmt = $this->_prepareSql("UPDATE `". $this->_getTable() ."` SET `email` = :email, `unsubscribe` = :uns WHERE `id` = :id");
        
        $email = $object->getEmail();
        $uns = $object->getUnsubscribe();
        $id = $object->getId();
        
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':uns', $uns);
        $stmt->bindParam(':id', $id);
        
        $stmt->execute();

    }
}