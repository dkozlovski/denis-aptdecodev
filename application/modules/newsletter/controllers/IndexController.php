<?php

class Newsletter_IndexController extends Ikantam_Controller_Front {
    
    protected $_reminderTime = 7200; // 2 hours
    
    public function checkSubscriptionAction ()
    {
        if(!$this->isAjax()) {
            $this->_helper->show404();
        } 
        
        $data = $this->getSession()->getNewsletterSubscriptionData();
        
        if(!$data) {
            if(!$this->getSession()->isLoggedIn()) {
                $data = array('remind' => true, 'last_remind' => time());
            } else {
                $nm = new Newsletter_Model_Email();
                $nm->getBy_user_id($this->getSession()->getUserId());
                
                $data = array ('remind' => (!$nm->isExists() && !$nm->getUnsubscribe()), 'last_remind' => time());                

            }
            
            $this->getSession()->setNewsletterSubscriptionData($data);
        } else {
            if(isset($data['subscribed'])) {
                $this->_helper->json(array('remind' => false), 1);  
            }
            if($this->getSession()->isLoggedIn()) {
                $nm = new Newsletter_Model_Email();
                $nm->getBy_user_id($this->getSession()->getUserId());
                
                $data = array ('remind' => (!$nm->isExists() && !$nm->getUnsubscribe())); 
                $this->_helper->json($data);                  
            }
            $data['remind'] = !$data['remind'] || $data['last_remind'] < time() - $this->_reminderTime ;
            if($data['remind']) {
                $data['last_remind'] = time();
                $this->getSession()->setNewsletterSubscriptionData($data);
            }
        }
       /* echo "<pre>";
        var_dump($data);
        echo "</pre>";*/
        
        $data = new Ikantam_Object($data);
        
       // print_d($data); exit;        
        $this->_helper->json($data->toArray(array('remind')), true);         
    }
    
    
    public function subscribeAction ()
    {
        if(!$this->isAjax()) {
            $this->_helper->show404();
        }

        $email = $this->getParam('email', false);
        $fStringTrim = new Zend_Filter_StringTrim();
        $email = $fStringTrim->filter($email);
        $vEmail = new Zend_Validate_EmailAddress();

        if(!$email || !$vEmail->isValid($email)) {
            $this->_helper->json(array('success' => false, 'message' => 'Invalid or empty email.'), true);
        }        

        
        try { 
            $nm = new Newsletter_Model_Email();
            
            $nm->getBy_email($email);
            if($nm->isExists()) {
                if($nm->getUnsubscsribe())
                {
                    $nm->setUnsubscribe(0)
                       ->save();
                    $this->_helper->json(array('success' => true), true);                       
                } else {
                    //$this->_helper->json(array('success' => false, 'message' => 'Email "'. $email .'" is already taken.'), 1);
                    $this->_helper->json(array('success' => true), true);
                }
   
            }
        
            $nm->setUserId($this->getSession()->getUserId())
               ->setEmail($email)
               ->save();

            $this->getSession()->setNewsletterSubscriptionData(array('subscribed' => true));
            
               $this->_helper->json(array('success' => true), 1);
               } catch (PDOException $e) { 
                    if(!$e->errorInfo[1] == 1062) { 
                        throw $e;
                    } 
                    $this->_helper->json(array('success' => false, 'message' => 'Email "'. $email .'" is already taken.'), 1);
               } 
        exit;                
    }
}