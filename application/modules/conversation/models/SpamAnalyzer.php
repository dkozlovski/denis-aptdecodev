<?php
/**
 * Author: Alex P.
 * Date: 11.06.14
 * Time: 13:39
 */

class Conversation_Model_SpamAnalyzer
{
    /**
     * Checks if message is spam
     *
     * @param string $message
     * @return bool
     */
    public function isSpam($message)
    {
        $user = User_Model_Session::user();
        $filter = $this->getMessagesFilter();

        $filter->author_id('=', $user->getId())
            ->created_at('>', strtotime('-2 minutes'))
            ->order('id desc');

        if (($messagesCount = $filter->count()) >= 20) {
            // if user sent 20 or more message for last 2 minutes
            return true;
        } elseif ($messagesCount < 4) {
            return false;
        }

        $similarity = $this->getSimilarityLevel($filter->apply(), $message);
        return $similarity >= 80;
    }

    /**
     * Create messages collection filter
     * @return Conversation_Model_Conversation_Message_FlexFilter
     */
    protected function getMessagesFilter()
    {
        $collection = new Conversation_Model_Conversation_Message_Collection;
        return $collection->getFlexFilter();
    }

    /**
     * Calculate average similarity level for messages
     * (All messages are compared with current message)
     * @param Conversation_Model_Conversation_Message_Collection $messages
     * @param string $currentMessage
     * @return float
     */
    public function getSimilarityLevel(Conversation_Model_Conversation_Message_Collection $messages, $currentMessage)
    {
        Zend_Registry::set('checkedForSpamMessagesCollection', $messages);

        $comparator = new Ikantam_Text_Comparator_SimpleShingles;

        $levels = array_map(
            function($message) use ($currentMessage, $comparator){

                return $comparator->compare($message, $currentMessage);
            }, $messages->getColumn('text')
        );

        return array_sum($levels) / count($levels);
    }

} 