<?php

/**
 * @method mixed getId()
 * @method mixed getSubject()
 * @method Conversation_Model_Conversation setSubject(string $subject)
 */
class Conversation_Model_Conversation extends Application_Model_Abstract
{

	protected $_users;
	protected $_messages;

	/**
	 * Get all participants of conversation
	 * 
	 * @return User_Model_User_Collection
	 */
	public function getUsers()
	{
		if (!$this->_users) {
			$this->_users = new User_Model_User_Collection();
			$this->_users->getByConversationId($this->getId());
		}
		return $this->_users;
	}

	/**
	 * Get all messages in conversation
	 * 
	 * @return Conversation_Model_Conversation_Message_Collection
	 */
	public function getMessages()
	{
		if (!$this->_messages) {
			$this->_messages = new Conversation_Model_Conversation_Message_Collection();
			$this->_messages->getByConversationId($this->getId());
		}
		return $this->_messages;
	}
	
	/**
	 * Get all messages in conversation
	 * 
	 * @return Conversation_Model_Conversation_Message_Collection
	 */
	public function getMessagesByUserId($userId)
	{
		if (!$this->_messages) {
			$this->_messages = new Conversation_Model_Conversation_Message_Collection();
			$this->_messages->getMessagesByUserId($this->getId(), $userId);
		}
		return $this->_messages;
	}

	/**
	 * Add new user to conversation
	 * 
	 * @param User_Model_User $user
	 * @return Conversation_Model_Conversation
	 */
	public function addUser(User_Model_User $user)
	{
		$this->getUsers()->addItem($user);
		return $this;
	}

	/**
	 * Add new message to conversation
	 * 
	 * @param Conversation_Model_Conversation_Message $message
	 * @return Conversation_Model_Conversation
	 */
	public function addMessage(Conversation_Model_Conversation_Message $message)
	{
	   	$msc = new Conversation_Model_MessageCheck($message, $this);        
        
        if(!$msc->checkFor('email', 'phoneNumber')) {

            if ($this->getSpamAnalyzer()->isSpam($message->getText())){
                User_Model_Session::user()->markAsSpammer();

                Zend_Registry::get('checkedForSpamMessagesCollection')
                    ->each(function($item){
                            $item->setIsSpam(1)->save();
                        });

                $this->setErrors(array(
                        'spam' => true
                    ));
            } else {
                $this->getMessages()->addItem($message);
            }

        } else {
            $this->setErrors($msc->getErrorMessages());
        }
		return $this;
	}

	/**
	 * Save object
	 * 
	 * @return Conversation_Model_Conversation
	 */
	public function save()
	{
	   if($this->getErrors()) {
	       return false;
	   }
       
		$this->_beforeSave();
		$this->_getBackend()->save($this);
		$this->_afterSave();
		return $this;
	}

	protected function _beforeSave()
	{
		return $this;
	}

	protected function _afterSave()
	{
		$this->saveParticipants();
		$this->saveMessages();
		return $this;
	}

	/**
	 * Save participants of conversation
	 * 
	 * @return Conversation_Model_Conversation
	 */
	protected function saveParticipants()
	{
		//@TODO: insert ignore
		foreach ($this->getUsers() as $user) {
			$conversationUser = new Conversation_Model_Conversation_User();
			$conversationUser->setConversationId($this->getId())
					->setUserId($user->getId())
					->save();
		}
		return $this;
	}

	/**
	 * Save messages of conversation
	 * 
	 * @return Conversation_Model_Conversation
	 */
	protected function saveMessages()
	{
		foreach ($this->getMessages() as $message) {
			if (!$message->getId()) {
				$message->setCreatedAt(time());
			}
			
			$message->addRecipients($this->getUsers())
					->setConversationId($this->getId())
					->save();
		}
		return $this;
	}

    /**
     * @return Conversation_Model_SpamAnalyzer
     */
    protected function getSpamAnalyzer()
    {
        return new Conversation_Model_SpamAnalyzer;
    }


}
