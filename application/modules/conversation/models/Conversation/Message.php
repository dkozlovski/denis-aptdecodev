<?php

/**
 * @method string getId()
 * @method string getConversationId()
 * @method string getAuthorId()
 * @method string getText()
 * @method string getCreatedAt()
 * @method Conversation_Model_Conversation_Message setId(int $id)
 * @method Conversation_Model_Conversation_Message setConversationId(int $conversationId)
 * @method Conversation_Model_Conversation_Message setAuthorId(int $authorId)
 * @method Conversation_Model_Conversation_Message setText(string $text)
 * @method Conversation_Model_Conversation_Message setCreatedAt(int $createdAt)
 */
class Conversation_Model_Conversation_Message extends Application_Model_Abstract
implements Application_Model_Event_Dispatcher_AwareInterface
{
        protected $_eventDispatcher;
        protected $_author;
	protected $_conversation;
	protected $_recipients;

	/**
	 * Set user as author of the message
	 * 
	 * @param User_Model_User $author
	 * @return Conversation_Model_Conversation_Message
	 */
	public function setAuthor(User_Model_User $author)
	{
		$this->_author = $author;
		$this->setAuthorId($this->_author->getId());
		return $this;
	}

	/**
	 * Get conversation the message belongs to
	 * 
	 * @return Conversation_Model_Conversation
	 */
	public function getConversation()
	{
		if (!$this->_conversation) {
			$this->_conversation = new Conversation_Model_Conversation($this->getConversationId());
		}
		return $this->_conversation;
	}

	/**
	 * Get author of the message
	 * 
	 * @return User_Model_User
	 */
	public function getAuthor()
	{
		if (!$this->_author) {
			$this->_author = new User_Model_User($this->getAuthorId());
		}
		return $this->_author;
	}

	/**
	 * Get all recipients of the message
	 * 
	 * @return User_Model_User_Collection
	 */
	public function getRecipients()
	{
		if (!$this->_recipients) {
			$this->_recipients = new User_Model_User_Collection();
			$this->_recipients->getByConversationId($this->getConversationId());
		}
		return $this->_recipients;
	}

	/**
	 * Add recipient of the message
	 * 
	 * @param User_Model_User $recipient
	 * @return Conversation_Model_Conversation_Message
	 */
	public function addRecipient(User_Model_User $recipient)
	{
		if ($recipient->getId() != $this->getAuthorId()) {
			$this->getRecipients()->addItem($recipient);
		}
		return $this;
	}

	/**
	 * Add recipients of the message
	 * 
	 * @param User_Model_User_Collection $users
	 * @return Conversation_Model_Conversation_Message
	 */
	public function addRecipients(User_Model_User_Collection $users)
	{
		foreach ($users as $user) {
			$this->addRecipient($user);
		}
		return $this;
	}

	public function save()
	{
		$this->_beforeSave();
		$this->getBackend()->save($this);
		$this->_afterSave();
	}

	protected function _beforeSave()
	{ 
	
	}

	protected function _afterSave()
	{
		//@TODO: insert ignore
		foreach ($this->getRecipients() as $recipient) {
			$messageUser = new Conversation_Model_Conversation_Message_User();
			$messageUser->setMessageId($this->getId())
					->setUserId($recipient->getId())
					->setIsNew(1)
					->setIsVisible(1)
					->save();
		}

		$messageUser = new Conversation_Model_Conversation_Message_User();
		$messageUser->setMessageId($this->getId())
				->setUserId($this->getAuthor()->getId())
				->setIsNew(0)
				->setIsVisible(1)
				->save();
	}

	public function setAsReadedFor(User_Model_User $user)
	{
		$this->getBackend()->setAsReadedFor($this, $user);
		return $this;
	}

	public function setAsDeletedFor(User_Model_User $user)
	{
		$this->getBackend()->setAsDeletedFor($this, $user);
		return $this;
	}

	protected function getBackend()
	{
		return new Conversation_Model_Conversation_Message_Backend();
	}
        
        public function getEventDispatcher()
        {
            if (!$this->_eventDispatcher) {
                $this->_eventDispatcher = new Application_Model_Event_Dispatcher();
            }
            return $this->_eventDispatcher;
        }
        
        public function setEventDispatcher(Zend_EventManager_EventCollection $eventDispatcher)
        {
            $this->_eventDispatcher = $eventDispatcher;
            return $this;
        }
}