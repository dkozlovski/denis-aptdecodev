<?php

class Conversation_Model_Conversation_User_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'conversations_users';


	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT IGNORE
			INTO `'.$this->_getTable().'` (`conversation_id`, `user_id`)
			VALUES (:conversation_id, :user_id)';
		
		$stmt = $this->_getConnection()->prepare($sql);

		$conversationId = $object->getConversationId();
		$userId= $object->getUserId();

		$stmt->bindParam(':conversation_id', $conversationId);
		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();
	}
	
	protected function _update(\Application_Model_Abstract $object)
	{
		
	}
}