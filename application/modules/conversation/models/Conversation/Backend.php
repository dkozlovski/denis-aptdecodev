<?php


class Conversation_Model_Conversation_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'conversations';
	
	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `'.$this->_getTable().'` (`subject`) VALUES (:subject)';

		$stmt = $this->_getConnection()->prepare($sql);

		$subject = $object->getSubject();

		$stmt->bindParam(':subject', $subject);

		$stmt->execute();
		
		$object->setId($this->_getConnection()->lastInsertId());
	}
	
	protected function _update(\Application_Model_Abstract $object)
	{
		$sql = 'UPDATE `'.$this->_getTable().'` SET `subject` = :subject WHERE `id` = :id';

		$stmt = $this->_getConnection()->prepare($sql);

		$id = $object->getId();
		$subject = $object->getSubject();

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':subject', $subject);

		$stmt->execute();
	}
}