<?php

class Conversation_Model_Conversation_Message_User_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'conversation_messages_users';
		
	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT IGNORE
			INTO `'.$this->_getTable().'` (`message_id`, `user_id`, `is_new`, `is_visible`)
			VALUES (:message_id, :user_id, :is_new, :is_visible)';
		
		$stmt = $this->_getConnection()->prepare($sql);

		$messageId = $object->getMessageId();
		$userId = $object->getUserId();
		$isNew = $object->getIsNew();
		$isVisible = $object->getIsVisible();

		$stmt->bindParam(':message_id', $messageId);
		$stmt->bindParam(':user_id', $userId);
		$stmt->bindParam(':is_new', $isNew);
		$stmt->bindParam(':is_visible', $isVisible);

		$stmt->execute();
		
	}
	
	protected function _update(\Application_Model_Abstract $object)
	{
		
	}
}