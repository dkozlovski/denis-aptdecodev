<?php

class Conversation_Model_Conversation_Message_Invalid_Collection 
    extends Application_Model_Abstract_Collection implements Ikantam_Filter_Flex_Interface
{
    protected $_filter;
    public function getFlexFilter ()
    {
        if(!$this->_filter) {
            $this->_filter = new Conversation_Model_Conversation_Message_Invalid_FlexFilter($this);
        }
        return  $this->_filter;
    }      
}    