<?php

class Conversation_Model_Conversation_Message_Invalid_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'invalid_messages';
    protected $_fields_ = array('id', 'from', 'subject', 'text', 'created', 'matches');
    
    protected function _insert(\Application_Model_Abstract  $object)
    { 
        $this->runStandartInsert(array('from', 'subject', 'text', 'created', 'matches'), $object);
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $this->runStandartUpdate(array('from', 'subject', 'text', 'created', 'matches'), $object);
    }

	

}