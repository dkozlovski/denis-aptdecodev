<?php
class Conversation_Model_Conversation_Message_Invalid_Exception extends Zend_Exception 
{
    protected $_invalid_message;
    
    public function setObject ($message)
    {
        $this->_invalid_message = $message;
    }
    
    public function getObject ()
    {
        return $this->_invalid_message;
    }
    
}