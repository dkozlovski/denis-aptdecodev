<?php

class Conversation_Model_Conversation_Message_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'conversation_messages';

	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `' . $this->_getTable() . '` 
			(`conversation_id`, `author_id`, `text`, `created_at`)
			VALUES (:conversation_id, :author_id, :text, :created_at)';

		$stmt = $this->_getConnection()->prepare($sql);

		$conversationId = $object->getConversationId();
		$authorId = $object->getAuthorId();
		$text = $object->getText();
		$createdAt = $object->getCreatedAt();

		$stmt->bindParam(':conversation_id', $conversationId);
		$stmt->bindParam(':author_id', $authorId);
		$stmt->bindParam(':text', $text);
		$stmt->bindParam(':created_at', $createdAt);

		$stmt->execute();
		
		$object->setId($this->_getConnection()->lastInsertId());
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		$sql = 'UPDATE `' . $this->_getTable() . '` SET
			`conversation_id` = :conversation_id,
			`author_id` = :author_id,
			`text` = :text,
			`created_at` = :created_at,
			`is_spam` = :is_spam
			WHERE `id` = :id';

		$stmt = $this->_getConnection()->prepare($sql);

		$id = $object->getId();
		$conversationId = $object->getConversationId();
		$authorId = $object->getAuthorId();
		$text = $object->getText();
		$createdAt = $object->getCreatedAt();
        $isSpam = $object->getIsSpam(0);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':conversation_id', $conversationId);
		$stmt->bindParam(':author_id', $authorId);
		$stmt->bindParam(':text', $text);
		$stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':is_spam', $isSpam);

		$stmt->execute();
	}

	public function getByConversationId($collection, $conversationId)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `conversation_id` = :conversation_id
			ORDER BY `created_at` DESC';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':conversation_id', $conversationId);
		$stmt->execute();

		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if ($rows) {
			foreach ($rows as $row) {
				$item = new Conversation_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}
	
	public function getOutboxByUserId($collection, $userId, $offset, $limit)
	{
		$sql = 'SELECT * FROM `view_outbox_messages` WHERE `user_id` = :user_id';
		
		if ($offset && $limit) {
			$sql .= ' LIMIT ' . $offset . ', ' .$limit;
		}

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);
		$stmt->execute();

		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if ($rows) {
			foreach ($rows as $row) {
				$item = new Conversation_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}
	
	public function getByUserId($collection, $userId, $offset, $limit)
	{
		/*$sql = 'SELECT * FROM `' . $this->_getTable() . '` 
			JOIN `conversation_messages_users` 
			ON `' . $this->_getTable() . '`.`id` = `conversation_messages_users`.`message_id`
			WHERE `user_id` = :user_id AND `is_visible` = 1 AND `author_id` <> `user_id`
			ORDER BY `created_at` DESC';*/
		
		$sql = 'SELECT * FROM `view_inbox_messages` WHERE `user_id` = :user_id';
		
		if ($offset && $limit) {
			$sql .= ' LIMIT ' . $offset . ', ' .$limit;
		}

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);
		$stmt->execute();

		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if ($rows) {
			foreach ($rows as $row) {
				$item = new Conversation_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getNewByUserId($collection, $userId, $offset, $limit)
	{
		/*$sql = 'SELECT * FROM `' . $this->_getTable() . '` 
			JOIN `conversation_messages_users` 
			ON `' . $this->_getTable() . '`.`id` = `conversation_messages_users`.`message_id`
			WHERE `user_id` = :user_id AND `is_visible` = 1 AND `is_new` = 1 AND `author_id` <> `user_id`
			ORDER BY `created_at` DESC';*/
		
		$sql = 'SELECT * FROM `view_inbox_messages` WHERE `is_new` = 1 AND `user_id` = :user_id';

		if ($offset && $limit) {
			$sql .= ' LIMIT ' . $offset . ', ' .$limit;
		}
		
		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);
		$stmt->execute();

		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if ($rows) {
			foreach ($rows as $row) {
				$item = new Conversation_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function setAsReadedFor(Conversation_Model_Conversation_Message $message, User_Model_User $user)
	{
		$sql = 'UPDATE `conversation_messages_users` 
			SET `is_new` = 0 
			WHERE `message_id` = :message_id AND `user_id` = :user_id';

		$stmt = $this->_getConnection()->prepare($sql);

		$messageId = $message->getId();
		$userId = $user->getId();

		$stmt->bindParam(':message_id', $messageId);
		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();
	}

	public function setAsDeletedFor(Conversation_Model_Conversation_Message $message, User_Model_User $user)
	{
		$sql = 'UPDATE `conversation_messages_users` 
			SET `is_visible` = 0 
			WHERE `message_id` = :message_id AND `user_id` = :user_id';

		$stmt = $this->_getConnection()->prepare($sql);

		$messageId = $message->getId();
		$userId = $user->getId();

		$stmt->bindParam(':message_id', $messageId);
		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();
	}
	
	
	
	
	
	public function getMessagesByUserId($collection, $conversationId, $userId)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` 
			JOIN `conversation_messages_users` 
			ON `' . $this->_getTable() . '`.`id` = `conversation_messages_users`.`message_id`
			WHERE `user_id` = :user_id AND `is_visible` = 1 AND `conversation_id` = :conversation_id
			ORDER BY `created_at` ASC';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);
		$stmt->bindParam(':conversation_id', $conversationId);
		$stmt->execute();

		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if ($rows) {
			foreach ($rows as $row) {
				$item = new Conversation_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}
	
	public function getTimeLastMessage($userID, $ConversationID)
        {
            $sql = 'SELECT * FROM '.$this->_getTable().
                   ' WHERE `author_id` = :userID and `conversation_id` = :ConversationID'.
                   ' ORDER BY `created_at` DESC';
            
            $stmt = $this->_getConnection()->prepare($sql);
            $stmt->bindParam(':userID', $userID);
            $stmt->bindParam(':ConversationID', $ConversationID);
            $stmt->execute();
            $row = $stmt->fetch();
            
            return $row;
        }
        
        public function countResponseMessage($AuthorID, $ConversationID, $timeLastMessage)
        {
            $sql = 'SELECT count(*) AS `count` FROM '.$this->_getTable().
                   ' WHERE `author_id` = :AuthorID and `conversation_id` = :ConversationID and `created_at` > :timeLastMessage';
            
            $stmt = $this->_getConnection()->prepare($sql);
            $stmt->bindParam(':AuthorID', $AuthorID);
            $stmt->bindParam(':ConversationID', $ConversationID);
            $stmt->bindParam(':timeLastMessage', $timeLastMessage);
            $stmt->execute();
            $row = $stmt->fetch();
            
            return $row['count'];
        }
	

}