<?php

class Conversation_Model_Conversation_Message_User extends Application_Model_Abstract
{
	public function getByUserId($userId)
	{
		$this->getBackend()->getByUserId($this, $userId);
		return $this;
	}
	
	public function getAllMessages($offset = null, $limit = null)
	{
		$collection = new Conversation_Model_Conversation_Message_Collection();
		return $collection->getByParticipantId($this->getId(), $offset, $limit);
	}
	
	public function getNewMessages($offset = null, $limit = null)
	{
		$collection = new Conversation_Model_Conversation_Message_Collection();
		return $collection->getNewByParticipantId($this->getId(), $offset, $limit);
	}
}