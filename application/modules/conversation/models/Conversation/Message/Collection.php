<?php


class Conversation_Model_Conversation_Message_Collection extends Application_Model_Abstract_Collection
    implements Ikantam_Filter_Flex_Interface
{

	public function getByConversationId($conversationId)
	{
		$this->_getBackend()->getByConversationId($this, $conversationId);
		return $this;
	}
	
	public function getByUserId($userId, $offset, $limit)
	{
		$this->_getBackend()->getByUserId($this, $userId, $offset, $limit);
		return $this;
	}
	
	public function getOutboxByUserId($userId, $offset, $limit)
	{
		$this->_getBackend()->getOutboxByUserId($this, $userId, $offset, $limit);
		return $this;
	}
	
	public function getNewByUserId($userId, $offset, $limit)
	{
		$this->_getBackend()->getNewByUserId($this, $userId, $offset, $limit);
		return $this;
	}
	
	public function getMessagesByUserId($conversationId, $userId)
	{
		$this->_getBackend()->getMessagesByUserId($this, $conversationId, $userId);
		return $this;
	}

    /**
     * Returns a filter related with collection.
     * @return object Ikantam_Filter_Flex
     */
    public function  getFlexFilter()
    {
        return new Conversation_Model_Conversation_Message_FlexFilter($this);
    }
    
    public function getTimeLastMessage($userID, $ConversationID)
    {
        return $this->_getBackend()->getTimeLastMessage($userID, $ConversationID);
    }
    
    public function countResponseMessage($AuthorID, $ConversationID, $timeLastMessage)
    {
        return $this->_getBackend()->countResponseMessage($AuthorID, $ConversationID, $timeLastMessage);
    }
}