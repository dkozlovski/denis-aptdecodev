<?php

class Conversation_Model_Conversation_Message_Invalid extends Application_Model_Abstract
{
    public function setAndSave (array $mathces, \Conversation_Model_Conversation_Message $message, $conversastion)
    { 
        $this->setFrom($message->getAuthor()->getId())
             ->setSubject($conversastion->getSubject())
             ->setText($message->getText())
             ->setMatches($mathces)
             ->setCreated(time());
             
        $this->save();     
    }
    
    public function setMatches ($matches)
    {
        if(is_array($matches)) {
            $matches = serialize($matches);
        }
        
        parent::setMatches($matches);
        return $this;
    }
    
    public function getMatches ($unserialize = false)
    {
        $matches = $this->getData('matches');
        if(is_string($matches) && $unserialize) {
            $matches = @unserialize($matches);
        }
        
        return $matches;
    }
    
    public function getBbCoded ($type)
    {
        $matches = $this->getMatches(true);
        $method = 'get'.ucfirst($type);
        $text = $this->{$method}();
        foreach($matches as $match) { 
            if(isset($match[0][0])) {
                $target = $match[0][0];
                $text = str_replace($target, '[[*'.$target.'*]]', $text);
            }           
        }

        return $text;
    }

    public static function highlightBbCoded($text)
    {
        return str_replace('*]]', '</span>', str_replace('[[*', '<span style="background-color:#E2FFC6;">', $text));
    }
}