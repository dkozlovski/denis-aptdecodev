<?php

class Conversation_Model_MessageCheck
{

    protected $_mathes                  = array();
    protected $_patterns                = array(
        'email'       => '#([a-z0-9][-a-z0-9_\+\.]*[a-z0-9]).{0,3}@.{0,3}([a-z0-9][-a-z0-9\.]*[a-z0-9].{0,3}(\.|dot).{0,3}(arpa|root|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\.{3}[0-9]{1,3}))#',
        'phoneNumber' => "#(\d+([\s*\.\-\(\)])*?){7,}#",
    );
    protected $_error_messages_template = array(
        'email'       => 'Forbidden share e-mail addresses.',
        'phoneNumber' => 'Forbidden share phone numbers.',
    );
    protected $_errors                  = array();
    protected $_message;
    protected $_conversation;

    public function __construct(\Conversation_Model_Conversation_Message $message, $conversation)
    {
        $this->_message      = $message;
        $this->_conversation = $conversation;
    }

    public function email()
    {
        $found = $this->_byRegex('email', $this->_message->getText(), 'email.text') ||
                $this->_byRegex('email', $this->_conversation->getSubject(), 'email.subject');
        if ($found) {
            $this->_error('email');
        }

        return $found;
    }

    public function phoneNumber()
    {
        $found = $this->_byRegex('phoneNumber', $this->_message->getText(), 'phoneNumber.text') ||
                $this->_byRegex('phoneNumber', $this->_conversation->getSubject(), 'phoneNumber.subject');
        if ($found) {
            $this->_error('phoneNumber');
        }
        return $found;
    }

    protected function _error($name)
    {
        $this->_errors[$name] = $this->_error_messages_template[$name];
    }

    public function checkFor($methods)
    {
        if (!is_array($methods)) {
            $methods = func_get_args();
        }

        $found = false;
        foreach ((array) $methods as $method) {
            if (method_exists($this, $method)) {
                if ($this->{$method}()) {
                    $found = true;
                }
            }
        }

        if ($found) {
            $this->_log($this->_mathes, $this->_message, $this->_conversation);
        }

        return $found;
    }

    public function getMatches($name = null)
    {
        if ($name && isset($this->_mathes[$name])) {
            return $this->_mathes[$name];
        } elseif (is_null($name)) {
            return $this->_mathes;
        }

        return false;
    }

    public function getErrorMessages()
    {
        return $this->_errors;
    }

    public function _log($matches, $message, $conversation)
    {
        $inv = new Conversation_Model_Conversation_Message_Invalid;
        $inv->setAndSave($matches, $message, $conversation);
    }

    protected function _byRegex($patern_name, $text, $matches_name)
    {
        $this->_mathes[$matches_name] = array();
        return preg_match_all($this->_patterns[$patern_name], $text, $this->_mathes[$matches_name]);
    }

}