<?php
class Google_IndexController extends Ikantam_Controller_Front
{    
    /**
     * If not handled document is exist then force to download it
     * Or response with code 204(No content)
     */
    public function downloadXmlAction ()
    {
         $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/google.ini', 'merchant');                    
         if($this->getParam('secret') !== $config->secret) {
            $this->_helper->show404();
         }
         
         $this->_handleTask(); // create new feed
              
         $this->view->layout()->disableLayout();
         $this->_helper->viewRenderer->setNoRender();
         
         $doc = $this->_getScheduledDocument();

         if(!$doc->isExists()) {
            $this->getResponse()->setHttpResponseCode(204);  //No content
         } else {
            $doc->forceDownload(); //fill response
            $doc->setIsHandled(true)// mark as handled
                ->save($doc::SAVE_DB); // just update db record(not content in file)
         }
                
    }
    
    /**
     * Create document object and load scheduled active(not handled) from DB
     *
     * @return object Google_Model_Xml_Document 
     */
    protected function _getScheduledDocument()
    {
        $doc = new Google_Model_Xml_Document;
        return $doc->loadScheduled();
    }
    
    /**
     * Handle scheduled task to create new xml document
     */
    protected function _handleTask()
    {
        $gp = new Google_Model_Products;
        $gp->createScheduledXmlDocument();   
    }
}
