<?php
class Google_Model_Task extends Application_Model_Abstract
{
    const STATUS_NEW = 'pending';
    const STATUS_CLOSED = 'closed';
    protected $_product;
    
    public function getProduct ()
    {
        if(!$this->_product){
            $this->_product = new Product_Model_Product($this->getProductId());
        }
        return $this->_product;
    }
    
    
}