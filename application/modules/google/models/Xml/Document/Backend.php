<?php
class Google_Model_Xml_Document_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'google_xml_documents';
    protected $_fields_ = array('id', 'full_path', 'created', 'is_handled');
    
    protected function _insert(\Application_Model_Abstract $object) 
    {
        $object->setCreated(time());
        $this->runStandartInsert(array('full_path', 'created'), $object);
    }
    
    protected function _update(\Application_Model_Abstract $object)
    {
        $this->runStandartUpdate(array('full_path', 'is_handled'), $object);
    }
    
    /**
     * Load the Scheduled active(not handled) document
     * @return object Google_Model_Xml_Document
     */
    public function loadScheduled (\Google_Model_Xml_Document $docObject)
    {
        $stmt = $this->_prepareSql("SELECT * FROM `".$this->_getTable()."` 
            WHERE `is_handled` = 0 ORDER BY `created` ASC LIMIT 1");
        $stmt->execute();            
 
        $docObject->setData($stmt->fetch(PDO::FETCH_ASSOC));
        
        return $docObject;    
    }
}