<?php
class Google_Model_Xml_Document extends Application_Model_Abstract
{
    const SAVE_DB = 1;
    const SAVE_FILE = 2;
    const SAVE_BOTH = 3;
    protected $_compressor ;
    
    public function __construct ($id = null)
    {
        $this->_compressor = new Zend_Filter_Compress(new Ikantam_Filter_Compress_Zip);
        $this->_config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/google.ini', 'xml');
      
        parent::__construct($id);
    }
    
   /**
    * Compress text to zip archive
    *
    * @param string $content
    * @return object self
    */
    protected function _compress($content)
    {
        $this->_compressor->setArchive($this->getCompressedPath());
        $this->_compressor->setTarget($this->getFileName());
        $this->_compressor->compress($content);
        return $this;        
    }
    
   /**
    * Decompress saved file
    *
    * @return string 
    */
    protected function _decompress()
    { 
        if(!file_exists($archive = $this->getCompressedPath())) {
            throw new Google_Model_Xml_Document_Exception('Archive not found.');
        }
        
        $this->_compressor->setArchive($this->getCompressedPath());
        $this->_compressor->setTarget($this->getFileName());
        
        return $this->_compressor->getTargetContent();
    }
    
   /**
    * Retrieves file name from full path
    *
    * @return string file name 
    */
    public function getFileName ()
    {
        return pathinfo($this->getFullPath(), PATHINFO_BASENAME);
    } 
    
   /**
    * Sets document name
    * 
    * @param  string $name
    * @return object self
    */
    public function setFileName ($name)
    {
        $this->setFullPath($this->_config->destination.$name);
        return $this;
    }
    
   /**
    * Sets document content
    * 
    * @param string $content 
    * @return object self
    */
    public function setContent ($content)
    {
        return parent::setContent($content);
    }
    
    public function getContent ()
    {
        if(!$content = parent::getContent()){
            $content = $this->getDecompressed();
        }
        
        return $content;
    }
    
   /**
    * Return decompressed content
    *
    * @return string
    */
    public function getDecompressed ()
    { 
        return $this->setContent($this->_decompress())
                    ->getContent();
    }
    
   /**
    * Return full path to compressed file
    *
    * @return string 
    */
    public function getCompressedPath ()
    {
        return $this->getFullPath().'.zip';   
    }
    
   /**
    * Save file and run standart save
    *
    * @param $mode - save mode
    * @return object self 
    * If need to create new document use BOTH mode it will save record in DB and create new file with compressed content
    * To update content:
    * $doc->setContent('new content')->save($doc::SAVE_DB)...
    * To update DB $doc->setFileName('new.file.xml')->save($doc::SAVE_FILE)
    */
    public function save ($mode = self::SAVE_BOTH)
    {
        if($mode & self::SAVE_FILE)
        {
            $content = $this->getContent();
            if(empty($content) || !is_string($content)){
                throw new Google_Model_Xml_Document_Exception('No content to save.');
            }
            if(file_exists($this->getCompressedPath())){
                unlink($this->getCompressedPath());
            }
            
            $this->_compress($content);            
        }
        
        if($mode & self::SAVE_DB)
        {
            parent::save();   
        }
        
         
        return $this; 
    }
    
    /**
     * Load Scheduled active document
     * 
     * @return object self
     */
    public function loadScheduled ()
    {
        $this->_getbackend()->loadScheduled($this);
        return $this; 
    }
    
   /**
    * Fill up the response object with all necessary headers and set body content(file content)
    *
    * @return object (Zend response)
    */
    public function forceDownload ()
    { 
        $response = Zend_Controller_Front::getInstance()->getResponse();
        $content = $this->getContent();
        $response->setHeader('Content-type', 'text/xml', true);
        $response->setHeader('Content-length', strlen($content));
        $response->setHeader('Content-Disposition','attachment;filename="'.$this->getFileName().'"', true);
        $response->setBody($content);
        return $response;
    }  
}