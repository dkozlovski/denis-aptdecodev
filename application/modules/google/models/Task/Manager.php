<?php
class Google_Model_Task_Manager
{
    protected $_operations = array('insert', 'delete', 'update');
    
    public function __construct ()
    {
        $this->_collection = new Google_Model_Task_Collection;
        $this->_collection->getFlexFilter()->save('empty');
    }
    
    /**
    * Add a new task for product
    * @param object Product_Model_Product $product
    * @param string $operation - operation type (insert/update/delete)
    * @param bool $overwriteExisting - overwrite existing task if true or do nothing if false
    * @return bool - result
    */
    public function newTask (\Product_Model_Product $product, $operation, $overwriteExisting = true)
    { 
        if(!in_array($operation, $this->_operations)) {
            throw new Google_Model_Task_Exception('Unknown operation type "'.$operation.'"');
        }


        switch(Google_Model_Products_Registry::productStatus($product)) {
            case Google_Model_Products_Registry::STATUS_NOT_EXIST:
                if($operation == 'delete') {
                    $this->cancelInsertUpdateTask($product);
                    return false;
                }elseif($operation == 'update') {
                    $operation  = 'insert';
                }
            break;
            case Google_Model_Products_Registry::STATUS_ACTIVE:
                if($operation == 'insert') {
                    $operation = 'update';
                }
            break;
            
            case Google_Model_Products_Registry::STATUS_DELETED:
                return false;
            break;
            
            default:
            break;    
        }
        
        if($operation == 'insert' && $product->getExpireAt() <= time()) {
            $this->cancelInsertUpdateTask($product); 
            return false;
        }         
        
        $task = new Google_Model_Task($this->isActiveTaskExist($product));

        if($operation == 'delete') {
        // search for latest expired task for this product
        // if such task found - force to close it and also close current task if it exist            
            $exp_task = $this->findTask(
                array(
                    'product_id' => array('=', $product->getId()),
                    'exp_date' => array('<=', time()),
                    'order' => array('created_date desc'),
                )
            ); 
            if($exp_task->isExists()) {
                $this->close($exp_task, 'Expired');
                if($task->isExists()) {
                    $this->close($task, 'Expired'); // instead deletion just close, google delete expired product
                }
                return false;
            }     
        }
      
        if($task->isExists() && !$overwriteExisting) {
            return false;
        }
        
        $task->setProductId($product->getId())
             ->setOperation($operation)
             ->setExpDate($product->getExpireAt())
             ->save();
    
        return true;                             
    }
    
    /**
     * Delete opened task having operation 'insert/update' for given product
     * 
     * @param  object $product
     * @return object self
     *
     */
    public function cancelInsertUpdateTask (\Product_Model_Product $product)
    {
        $this->findTask(
            array(
            'product_id' => array('=', $product->getId()),
            'status' => array('=', Google_Model_Task::STATUS_NEW),
            'operation' => array('in', 'insert', 'update')
            )
        )->delete();
        
        return $this;    
    }
    
    /**
     * Return first matched criteries task (filter uses)
     * 
     * @param  array $options - key = method, value = array with arguments
     * @return object Google_Model_Task
     */
    public function findTask (array $options)
    {
        $filter = $this->_newTaskFilter();
        foreach($options as $method => $params) {
            call_user_func_array(array($filter, $method), $params);
        }
        
        return $filter->apply(1)->getFirstItem();
    }    
    
    /**
    * Update task as closed(executed)
    * @param  object $task Google_Model_Task
    * @param  string $error - Error text. Task closed with error (marked as error in registry)
    * @return bool - result
    */
    public function close (\Google_Model_Task $task, $error = null)
    { 
        if($this->isActiveTaskExist($task->getProduct())){
            $task->setStatus(Google_Model_Task::STATUS_CLOSED)
                 ->setExecutionDate(time())
                 ->save();
                 
            $task->getProduct()
                ->selfTriggerEvent('google.merchants.task.closed', array($task->getOperation(), $error));
            
            return true;     
        }
        
        return false;    
    }
    
    /**
    * Check if task exists and have pending status
    * @param object Product_Model_Product $product
    * @return int - 0 if not exist | task id if found
    */
    public function isActiveTaskExist (\Product_Model_Product $product)
    {
        if(!$product->isExists()) {
            return false;
        }
                     
        $filter = $this->_newTaskFilter();
        
        return  $filter->product_id('=', $product->getId())
                   ->status('=', Google_Model_Task::STATUS_NEW)
                   ->apply(1)
                   ->getFirstItem()
                   ->getId(0);
                                                
    }
    
    /**
    * Create multiple tasks for products. If no products passed then not queued products will be used.
    * @param object Product_Model_Product_Collection $products
    * @param string $operation
    * @return object self
    */
    public function createPack(\Product_Model_Product_Collection $products = null, $operation = 'insert')
    {
        if(null === $products){
            $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/google.ini', 'xml');
            $products = new Product_Model_Product_Collection;            
        }
        
        $products->getForGoogleTask($config->limit);
        
        foreach($products as $product) {
            $this->newTask($product, $operation);
        }
        
        return $this;
    }
    
    /**
    * Retrieves not processed tasks
    * @param  int $limit
    * @return object Google_Model_Task_Collection
    */
    public function getTasks ($limit)
    {
        $filter = $this->_newTaskFilter();
        
       return $filter->status('=', Google_Model_Task::STATUS_NEW)
                     ->order('created_date')
                     ->apply($limit); 
    }
    
    /**
    * Create new collection filter
    * @return object Google_Model_Task_FlexFilter
    */
    protected function _newTaskFilter()
    {
        return new Google_Model_Task_FlexFilter(new Google_Model_Task_Collection);
    }  
}