<?php
class Google_Model_Task_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'google_shopping_content_tasks';
    protected $_fields_ = array('id', 'product_id', 'operation', 'status', 'created_date', 'execution_date', 'exp_date');
    
    protected function _insert(\Application_Model_Abstract $object)
    {
       $object->setCreatedDate(time())
              ->setStatus('pending');
       
       $this->runStandartInsert(array('product_id', 'operation', 'status', 'created_date', 'exp_date'), $object);     
    }
    
    protected function _update(\Application_Model_Abstract $object)
    { 
        $this->runStandartUpdate(array('status', 'operation', 'execution_date', 'exp_date'), $object);
    } 
}