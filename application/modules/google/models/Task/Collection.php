<?php
class Google_Model_Task_Collection extends Application_Model_Abstract_Collection 
    implements Ikantam_Filter_Flex_Interface
{
    protected $_backendClass = 'Google_Model_Task_Backend';
    protected $_itemObjectClass = 'Google_Model_Task';
    protected $_filter ;
    
    public function getFlexFilter ()
    {
        if(!$this->_filter){
            $this->_filter = new Google_Model_Task_FlexFilter($this);
        }        
        return $this->_filter;
    }
    
    /**
    * Retrieves products from loaded task items
    * @return object Product_Mode_Product_Collection
    */
    public function getProducts ()
    {
        $products = new Product_Model_Product_Collection;
        $products->getFlexFilter()
                 ->id('in', $this->getColumn('product_id'))
                 ->apply();
                 
        return $products;         
    }
}