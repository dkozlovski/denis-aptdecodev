<?php

class Google_Model_Products 
{
    const FORMAT_API_SHOPPING = 'GSC';
    const FORMAT_MANUAL  = 'manual';
    
    protected $_config ;

    protected $_batch_products ;
    protected $_products ;
    protected $_google_relations ;
    protected $_color_relations ;
    
    protected $_task_manager ;
    
    private $_error_buffer = array();    
    private $_condition_map = array (
        'new' => 'new',
        'like-new' => 'used',
        'good' => 'used',
        'satisfactory' => 'used',
        'age-worn' => 'used',
    ); 
    
    public function __construct($config = null)
    {
        if(!class_exists('GSC_Client'))
        {
            Zend_Loader::loadFile('GShoppingContent.php', APPLICATION_PATH.'/../library/Google', true);
        }
        
        if(null === $config) {
          $this->_config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/google.ini', 'merchant');  
        } else {
            $this->_config = $config;
        }

        
        $this->_batch_products = new GSC_ProductList;
        
        $categories = new Category_Model_Category_Collection;
        $this->_google_relations =  $categories->getFlexFilter()
                   ->alwaysJoin('google_category_name')
                   ->apply()
                   ->getColumns(array('id', 'google_category_name'), true);
                   
        $colors = new Application_Model_Color_Collection;
        $this->_color_relations = $colors->getAll()->getColumns(array('id', 'title'), true);   
        $this->_task_manager = new Google_Model_Task_Manager;                 
    }    

    
   /**
    * Convert our product model to google product
    * @param  object Product_Model_Product $product
    * @return object GSC_Product
    */
    public function fromProduct(\Product_Model_Product $product)
    {
        $g_product = new GSC_Product;
        
        if(($result = $this->_canBePublished($product)) === true) {
            $g_product->setTitle($product->getTitle());
            $g_product->setDescription($product->getDescription());
            $g_product->setProductLink($product->getProductUrl());
            $g_product->setSku($product->getId());
            $g_product->setImageLink($product->getMainImage()->getS3Url(1500, 1500, 'frame'));

           // $g_product->setTargetCountry($this->_config->target_country);
           // $g_product->setContentLanguage($this->_config->content_language);
           // In merchant account
            $g_product->setCondition($this->_condition_map[$product->getCondition()]);
            $g_product->setColor($this->_color($product));
            $g_product->setProductType($product->getCategory()->getTitle());
            $g_product->setGoogleProductCategory($this->_googleCategory($product));
            $g_product->setAdult('false');
            $g_product->setIdentifierExists('false');
            $g_product->setBrand($product->getManufacturer()->getTitle());
            //Shipping and Tax settings in merchant account
            

            
            $availability = !$product->getIsSold() ? 'in stock' : 'out of stock';
            
            if($availability == 'in stock')  {
                $g_product->setQuantity($product->getQty());
                
                /*if($product->getExpireAt() > time()){
                    $g_product->setExpirationDate(date('Y.m.d', $product->getExpireAt()));
                }*/
            }
            
            $g_product->setAvailability($availability);
            $g_product->setPrice($product->getPrice(), 'usd');
            
        } else { 
                //Google_Model_Products_Registry::write($product, 'error', implode('|', $result));
                $this->_error($result); // set errors in buffer
            }              
             
      return $g_product;    
    }
    
   /**
    * Create google product from task object and set batch operation
    * @param  object Google_Model_Task $task
    * @return object GSC_Product
    */
    public function fromTask (\Google_Model_Task $task)
    {
      $g_product = $this->fromProduct($product = $task->getProduct());  
      $g_product->setBatchOperation(strtoupper($task->getOperation()));
      if($task->getExpDate() > time()) { // task exp date update when related product update
            $date = new Zend_Date($product->getExpireAt()); 
            $g_product->setExpirationDate($date->toString(Zend_Date::ISO_8601));
      }
            
      return $g_product;
    }
    
   /**
    * Creates new xml document
    * 
    * @param $type -  
    * @return 
    */
    public function createScheduledXmlDocument ($format = self::FORMAT_MANUAL)
    {
        $xmlConf = new Zend_Config_Ini(APPLICATION_PATH.'/configs/google.ini', 'xml');
        $tasks = $this->_task_manager->getTasks($xmlConf->limit);
        $productCount = 0; //insert
        
        foreach($tasks as $task) {
            $g_product = $this->fromTask($task);
            if($g_product->getSKU()) {
                $this->_batch_products->addEntry($g_product);
                $this->_task_manager->close($task);
                $productCount++;
             } else {
                $this->_task_manager->close($task, implode('|', $this->_error()));  //close task with error                
             }
         }
          
         if($productCount){
             $xml = "<?xml version='1.0' encoding='UTF-8'?>"
                           .$this->toXml();
             // Для отправки документа через API используются scp:* и sc:* теги, а для загрузки вручную или со стороны гугла вмето них g:
             // Костыль ниже :(                             
             if($format === self::FORMAT_MANUAL) {
                $xml = str_replace('scp:', 'g:', $xml);
                $xml = str_replace('sc:', 'g:', $xml);
                $xml = str_replace('<content type="text">', '<g:description>', $xml);
                $xml = str_replace('</content>', '</g:description>', $xml);
                $xml = preg_replace('/(<link rel="alternate".*href=(".*")).*link>/', '<link href=$2/>', $xml);                  
             }                           
             
             $fileName = time().'_'.$productCount.'_feed.xml';                          
             $doc = new Google_Model_Xml_Document;
             $doc->setContent($xml)
                 ->setFileName($fileName)
                 ->save();            
         }
    }
    
   /**
    * Converts prepared products to xml
    * @return string xml
    */
    public function toXml ()
    { 
        $xml = $this->_batch_products->toXML();
      
        return $xml; 
    }
    
    protected function _googleCategory($product)
    { 
        return isset($this->_google_relations[$product->getCategoryId()]['google_category_name']) ?
            $this->_google_relations[$product->getCategoryId()]['google_category_name'] : null;
    }
    
   /**
    * Return color name
    * @param  object $product
    * @return string
    */
    protected function _color($product)
    {
        return isset($this->_color_relations[$product->getColorId()]) ? 
            strtolower($this->_color_relations[$product->getColorId()]['title']) : '';
    }
   /**
    * Check if product can be published at google products
    * @param  object $product
    * @return mixed true on success | array of error strings on failure
    */
    protected function _canBePublished($product)
    {
        $errors = array();
        
        if(!$product->getDescription())
        {
            $errors['empty_description'] = 'Description is required.';
        } else {
            $pattern = '((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)';
              $urlValidator = new Zend_Validate_Regex($pattern);
              
              if($urlValidator->isValid($product->getDescription())) {
                $errors['url_in_description'] = 'Description contains url';
              }   
          }
        if(!$this->_googleCategory($product))
        {
            $errors['no_related_category'] = 'No related google category found.';
        }
        if(!$product->getMainImage()->getS3Path())
        {
            $errors['no_image'] = 'Image is required.';
        }
        
        return (!count($errors)) ?: $errors;
    }
    
   /**
    * Get\set errors
    * @param  optional array - contains error strings to set or null to get
    * @return void|array
    */
    protected function _error(array $errors = null)
    {   
        if(is_array($errors)) {
            $this->_error_buffer = $errors;   
        } elseif(null === $errors) { 
            $errors = $this->_error_buffer;
            $this->_error_buffer = array();
            return $errors;
        }
    }
        
}