<?php
class Google_Model_Products_Registry extends Application_Model_Abstract
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';
    const STATUS_ERROR = 'error';
    const STATUS_NOT_EXIST = 'not_exist';
    
    protected static $_instance ;
    protected $_statuses ;
    
    public static function getInstance ()
    {
        if(!self::$_instance){
            self::$_instance = new self();
            self::$_instance->_statuses = array(
                self::STATUS_ACTIVE,
                self::STATUS_DELETED,
                self::STATUS_NOT_EXIST,
                self::STATUS_ERROR,
            );
        }    
        self::$_instance->setData(array());    
        return self::$_instance;
    }
    
    /**
    * Return product status in registry or special(not exist) status
    * @param  object Product_Model_Product $product
    * @return string - status
    */
    public static function productStatus (\Product_Model_Product $product)
    {
        if(!$product->isExists()) {
            return self::STATUS_NOT_EXIST;
        }
                
        $inst = self::getInstance();        
        $inst->getBy_product_id($product->getId());
        
        return $inst->getStatus(self::STATUS_NOT_EXIST);
    }
    
    /**
    * Write or rewrite status for product
    * @param  object Product_Model_Product $product
    * @param  string status (self::STATUS_...)
    * @param  string $error - error text if error status provided
    * @return object self
    */
    public static function write (\Product_Model_Product $product, $status, $error = null)
    {
        if(!$product->isExists()) {
            return $this;
        }
        
        $inst = self::getInstance();
        if(!in_array($status, $inst->getStatuses())) {
            throw new Google_Model_Product_Registry_Exception(
                'Unknown status given "'.$status.'". Possible statuses: '.implode(', ', $inst->getStatuses()));
        }
        
        if($status == self::STATUS_ERROR) {
            $inst->setErrorText($error);
        }
        
        $inst->getBy_product_id($product->getId());
        $inst->setProductId($product->getId())
            ->setStatus($status)
            ->save();
            
         return $inst;
    }
    
    /**
    * Return all statuses
    * @return array
    */
    public function getStatuses ()
    {
        return $this->_statuses;
    }
}