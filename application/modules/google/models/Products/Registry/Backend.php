<?php
class Google_Model_Products_Registry_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'google_products_registry';
    protected $_fields_ = array('id', 'product_id', 'updated', 'status', 'exp_date', 'error_text');
    
    protected function _insert(\Application_Model_Abstract $object)
    {
        $object->setUpdated(time());
        $this->runStandartInsert(array('product_id', 'updated', 'status', 'exp_date', 'error_text'), $object);
    }
    
    protected function _update(\Application_Model_Abstract $object)
    {
        $object->setUpdated(time());
        $this->runStandartUpdate(array('product_id', 'updated', 'status', 'exp_date', 'error_text'), $object);        
    }
    
}