<?php

class Notification_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initAutoload()
    {
        $resourceLoader = $this->getResourceLoader();
        $resourceLoader->addResourceType('interfaces', 'interfaces/', 'Interface');
    }
}
