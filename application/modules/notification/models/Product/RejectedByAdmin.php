<?php

class Notification_Model_Product_RejectedByAdmin
{

    protected $reasons = array(
        'Product condition'              => 'Product condition or quality',
        'Duplicate listing'              => 'Duplicate listing',
        'Mattress\electronics'           => 'Item category not supported by AptDeco (e.g. Mattresses, appliances,  electronics)',
        'Incomplete listing information' => 'Incomplete listing information',
        'Stock photos'                   => 'Inadequate photos of the item',
    );

    public function sendEmails($item, $reason = null)
    {
        $this->_toSeller($item, $reason);
    }

    /**
     * 
     * @param Product_Model_Product $product
     * @param string|null $reason
     */
    protected function _toSeller($product, $reason)
    {
        $seller = $product->getSeller();
        $reason = isset($this->reasons[$reason]) ? $this->reasons[$reason] : $reason;

        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        $view->product  = $product;
        $view->seller   = $seller;
        $view->decision = 'rejected';
        $view->reason   = $reason;

        if ($seller->getMainEmail()) {
            $output = $view->render('product_rejected_by_admin/to_seller.phtml');

            $toSend = array(
                'subject' => 'Your AptDeco listing has been declined',
                'email'   => $seller->getMainEmail(),
                'body'    => $output,
            );

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            //$mail->sendCopy($output);
            $mail->send(null, true);
        }
    }

}