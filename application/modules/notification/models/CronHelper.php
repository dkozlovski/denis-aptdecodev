<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 04.07.14
 * Time: 16:24
 */
// https://app.asana.com/0/6707107011362/12276730727349
class Notification_Model_CronHelper
{
    /**
     * @var Notification_Model_Dispatcher
     */
    protected $dispatcher;

    /**
     * Send notifications
     * @param int $limit - limit for each group (15% and 30%)
     */
    public function sendSuggestionNotificationToSellersAboutPriceReduction($limit = 5)
    {
        $products15Percent = $this->getProductCollection()
            ->getConfiguredFilterForPriceReduction15PercentSuggestion()
            ->apply($limit);

        $products30Percent = $this->getProductCollection()
            ->getConfiguredFilterForPriceReduction30PercentSuggestion()
            ->apply($limit);

        $this->sendNotificationToSellersAboutPriceReduction(
            $products15Percent,
            'to_seller/reduce_price_15percent_suggestion.phtml'
        );

        $this->sendNotificationToSellersAboutPriceReduction(
            $products30Percent,
            'to_seller/reduce_price_30percent_suggestion.phtml'
        );
    }

    /**
     * @param Product_Model_Product_Collection $products
     * @param $templatePath
     */
    protected function sendNotificationToSellersAboutPriceReduction(
        \Product_Model_Product_Collection $products,
        $templatePath
    ) {
        $settingName = 'email_notification_to_seller_price_lowering_suggestion';
        $subject = new \Application_Model_ValueExtractor('adminEAVSetting|'.$settingName.'|subject');
        $text  = new \Application_Model_ValueExtractor('adminEAVSetting|'.$settingName.'|text');

        /**
         * @var Product_Model_Product $product
         */
        foreach($products as $product) {
            $this->getDispatcher()->sendNotificationByTask(
                \Notification_Model_NotificationTask::instantTask(
                    $product->getSeller(),
                    $templatePath,
                    \Application_Model_ObjectStorage::factory(
                        array(
                            'product' => $product,
                            'subject' => $subject,
                            'text' => $text,
                            'user' => $product->getSeller(),
                        )
                    )
                )
            );

            $product->setEAVAttributeValue('notification_manage_pricing_to_seller_sent', 1)->save();
        }
    }

    /**
     * @return Product_Model_Product_Collection
     */
    protected function getProductCollection()
    {
        return new \Product_Model_Product_Collection;
    }

    /**
     * @return Notification_Model_Dispatcher
     */
    protected function getDispatcher()
    {
        if (empty($this->dispatcher)) {
            $this->dispatcher = new \Notification_Model_Dispatcher();
        }

        return $this->dispatcher;
    }
} 