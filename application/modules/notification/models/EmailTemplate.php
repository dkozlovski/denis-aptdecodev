<?php
/**
 * Author: Alex P.
 * Date: 16.06.14
 * Time: 18:14
 */
/**
 * @method Notification_Model_EmailTemplate setViewPath(string $path)
 * @method string getViewPath()
 * @method string id getId()
 * @method Notification_Model_EmailTemplate setId(string $id)
 */
class Notification_Model_EmailTemplate extends Application_Model_Abstract
{

} 