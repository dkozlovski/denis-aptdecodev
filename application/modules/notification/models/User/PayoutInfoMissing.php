<?php

class Notification_Model_User_PayoutInfoMissing
{

    public function sendEmails($seller)
    {
        $this->_toSeller($seller);
    }

    protected function _toSeller($seller)
    {
        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        $view->seller = $seller;

        if ($seller->getMainEmail()) {
            $output = $view->render('payout_info_missing/to_seller.phtml');

            $toSend = array(
                'email'   => $seller->getMainEmail(),
                'subject' => 'Action Required: Your recent sale payout is pending',
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            //$mail->sendCopy($output);
            $mail->send(null, true);
        }
    }

}