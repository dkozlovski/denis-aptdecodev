<?php

/**
 * Author: Alex P.
 * Date: 16.06.14
 * Time: 13:26
 */
//TODO: refactor: retrieve task parameters before using it on any methods. Use these parameter instead of using view as a buffer for them
/**
 * Class Notification_Model_Dispatcher
 */
class Notification_Model_Dispatcher
{

    /**
     * Applies to task filter
     * @var array
     */
    protected $defaultConditions = array(
        'counter' => array('IS_NULL'), // after sending the notification counter is incremented (Important)
    );

    /**
     * @var \Zend_View
     */
    protected $view;

    /**
     * Creates new task
     * @param Application_Model_User |Application_Model_User_Collection $user
     * @param $templatePath
     * @param Application_Model_ObjectStorage $parameters (optional)
     * @param $note - special mark for task (optional)
     */
    public static function newTask(
        $user,
        $templatePath,
        \Application_Model_ObjectStorage $parameters = null,
        $note = null
    ) {
        $template = new Notification_Model_EmailTemplate;
        $template->getBy_view_path($templatePath);
        if (!$template->isExists()) {
            $template->setViewPath($templatePath)->save();
        }
        $task = new \Notification_Model_NotificationTask;
        $task->setNote($note);
        $task->newTask($user, $template, $parameters);
    }

    /**
     * Retrieves scheduled tasks using filter conditions and initiates message sending for each task
     * @param null $limit
     * @param array $conditions
     */
    public function sendScheduledNotifications($limit = null, $conditions = array())
    {
        $conditions = array_merge($this->defaultConditions, $conditions);
        $tasks      = $this->getTasks($conditions, $limit);
        foreach ($tasks as $task) {
            $this->sendNotificationByTask($task);
        }
    }

    /**
     * Sends one message via the task
     * @param Notification_Model_NotificationTask $task
     */
    public function sendNotificationByTask(\Notification_Model_NotificationTask $task)
    {
        //Important to call getMessageBody before getting the product or subject
        $body = $this->getMessageBody($task);

        $user    = $task->getUser();
        $product = $this->getProduct();
        $subject = $this->getSubject() ? : '';

        $subject = $this->replaceShortcodes($subject, $product);
        $subject = $this->replaceShortcodes($subject, $user);

        $mail = $this->getMailLib(
                array(
                    'email'   => $user->getMainEmail(),
                    'subject' => $subject,
                    'body'    => $body,
                )
        );

        $mail->sendCopy($body);
        $mail->send();
        $this->afterSend($task);
    }

    /**
     * Runs after the message sent. Performs final actions for task.
     * @param Notification_Model_NotificationTask $task
     */
    protected function afterSend(\Notification_Model_NotificationTask $task)
    {
        $task->incrementCounter();
    }

    /**
     * Retrieves tasks collection
     * @param $conditions
     * @param $limit
     * @return Notification_Model_NotificationTask_Collection
     */
    protected function getTasks($conditions, $limit)
    {
        return \Notification_Model_NotificationTask::getFullyJoinedTasks($conditions, $limit);
    }

    /**
     * Create instance of mail library
     * @param $data
     * @return Ikantam_Mail
     */
    protected function getMailLib($data)
    {
        return new Ikantam_Mail($data);
    }

    /**
     * Renders view to get message content
     * @param Notification_Model_NotificationTask $task
     * @return string
     */
    protected function getMessageBody(\Notification_Model_NotificationTask $task)
    {
        return $this->getFilledView($task)->render($task->getEmailTemplate()->getViewPath());
    }

    /**
     * Fills view with necessary parameters
     * @param Notification_Model_NotificationTask $task
     * @return Zend_View
     * @throws Exception
     */
    protected function getFilledView(\Notification_Model_NotificationTask $task)
    {
        $view       = $this->getView();
        $view->user = $task->getUser();
        foreach ($task->getParametersObject() as $parameter) {
            if (!$parameter instanceof \Application_Interface_ObjectStorage_SymbolTable) {
                throw new \Exception('Object storage contains invalid item.');
            }
            $field = $parameter->getSymbolTableName();
            $value = null;

            if ($parameter instanceof \Application_Model_ValueExtractor) {
                $value = $parameter->extract();
            } else {
                $value = $parameter;
            }

            $view->{$field} = $value;
        }

        if (isset($view->text)) {
            if (isset($view->product)) {
                $view->text = $this->replaceShortcodes($view->text, $view->product);
            }
            if (isset($view->user)) {
                $view->text = $this->replaceShortcodes($view->text, $view->user);
            }
        }

        $unsubscribe = new \Notification_Model_Unsubscribe;
        $unsubscribe->createCodeForUser($task->getUser());

        $url = \Ikantam_Url::getUrl('unsubscribe/notifications/', array('code' => $unsubscribe->getCode()));

        $view->unsubscribeUrl = $url;

        return $view;
    }

    /**
     * Returns view
     * @return Zend_View
     */
    protected function getView()
    {
        if (!$this->view) {
            $view       = new \Zend_View();
            $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));
            $this->view = $view;
        } else {
            $this->view->clearVars();
        }

        return $this->view;
    }

    /**
     * Retrieve related product if exists
     * @return Product_Model_Product | null
     */
    protected function getProduct()
    {
        return $this->retrieveViewParam('product');
    }

    /**
     * Retrieve related subject if exists
     * @return string | null
     */
    protected function getSubject()
    {
        return $this->retrieveViewParam('subject');
    }

    /**
     * Checks if parameter assigned to view and returns it
     * @param $name
     * @return null
     */
    protected function retrieveViewParam($name)
    {
        if ($this->view && isset($this->view->{$name})) {
            return $this->view->{$name};
        }
        return null;
    }

    /**
     * Replaces special substring with value from passed object
     * @param $text
     * @param $object
     * @return mixed
     */
    protected function replaceShortcodes($text, $object)
    {
        $currency = new \Zend_View_Helper_Currency('en_US');
        if ($object instanceof \Product_Model_Product) {
            $text = str_replace('%price%', $currency->currency($object->getPrice()) . '', $text);
            $text = str_replace('%product_title%', $object->getTitle(), $text);
            $text = str_replace('%available_from%', date('M d Y', $object->getAvailableFrom()), $text);
            $text = str_replace('%available_till%', date('M d Y', $object->getAvailableTill()), $text);
            if (isset($this->view->oldPrice)) {
                $text = str_replace('%old_price%', $currency->currency($this->view->oldPrice) . '', $text);
            }
        } elseif ($object instanceof \Application_Model_User) {
            $text = str_replace('%first_name%', $object->getFirstName(), $text);
            $text = str_replace('%full_name%', $object->getFullName(), $text);
        }

        return $text;
    }

}