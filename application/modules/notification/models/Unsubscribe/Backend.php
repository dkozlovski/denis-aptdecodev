<?php
/**
 * Author: Alex P.
 * Date: 19.06.14
 * Time: 12:55
 */


class Notification_Model_Unsubscribe_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'notifications_unsubscribe';
    protected function _init()
    {
        $this->setSettings(array('db_insert' => array('on_duplicate_entry' => 'ignore')));
    }
} 