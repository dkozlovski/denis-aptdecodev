<?php
/**
 * Author: Alex P.
 * Date: 16.06.14
 * Time: 13:11
 */

class Notification_Model_NotificationTask_Collection extends \Application_Model_Abstract_Collection implements
    \Ikantam_Filter_Flex_Interface
{
    protected $_itemObjectClass = 'Notification_Model_NotificationTask';

    /**
     * Returns a filter related with collection.
     * @return Ikantam_Filter_Flex
     */
    public function  getFlexFilter()
    {
        return new Notification_Model_NotificationTask_FlexFilter($this);
    }
}