<?php
/**
 * Author: Alex P.
 * Date: 16.06.14
 * Time: 12:31
 */

class Notification_Model_NotificationTask_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'notification_tasks';

    public function boundUser(\Application_Model_User $user, \Notification_Model_NotificationTask $task)
    {
        $sql = "INSERT INTO `notification_tasks_users` (`notification_task_id`, `user_id`)" .
            " VALUES (:task_id, :user_id)";

        $stmt = $this->_prepareSql($sql);
        $stmt->bindValue(':task_id', $task->getId(), PDO::PARAM_INT);
        $stmt->bindValue(':user_id', $user->getId(), PDO::PARAM_INT);

        $stmt->execute();
    }

    public function boundTemplate(
        \Notification_Model_EmailTemplate $template,
        \Notification_Model_NotificationTask $task
    ) {
        $sql = "INSERT INTO `notification_tasks_email_templates` (`notification_task_id`, `email_template_id`)" .
            " VALUES (:task_id, :template_id)";

        $stmt = $this->_prepareSql($sql);
        $stmt->bindValue(':task_id', $task->getId(), PDO::PARAM_INT);
        $stmt->bindValue(':template_id', $template->getId(), PDO::PARAM_INT);

        $stmt->execute();
    }

    public function incrementCounter(\Notification_Model_NotificationTask $task, $userId)
    {
        $sql = "UPDATE `notification_tasks_users` SET `counter` = COALESCE(`counter`, 0) + 1 " .
            "WHERE `notification_task_id` = :task_id AND `user_id` = :user_id LIMIT 1;";

        $stmt = $this->_prepareSql($sql);
        $stmt->bindValue(':task_id', $task->getId(), PDO::PARAM_INT);
        $stmt->bindValue(':user_id', $userId, PDO::PARAM_INT);

        $stmt->execute();

        // Important to delete duplicates because only the first counter incremented
        $sql = "DELETE FROM `notification_tasks_users` " .
               "WHERE `notification_task_id` = :task_id AND `user_id` = :user_id " .
               "AND `counter` IS NULL LIMIT 1;";

        $stmt = $this->_prepareSql($sql);
        $stmt->bindValue(':task_id', $task->getId(), PDO::PARAM_INT);
        $stmt->bindValue(':user_id', $userId, PDO::PARAM_INT);

        $stmt->execute();
    }
} 