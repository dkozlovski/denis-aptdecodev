<?php
/**
 * Author: Alex P.
 * Date: 16.06.14
 * Time: 13:17
 */

class Notification_Model_NotificationTask_FlexFilter extends Ikantam_Filter_Flex
{
    protected $_acceptClass = 'Notification_Model_NotificationTask_Collection';

    protected $_joins = array (
        '_email_template' => 'INNER JOIN `notification_tasks_email_templates`  ON `notification_tasks_email_templates`.`notification_task_id` = `notification_tasks`.`id`',
        'email_template' =>  array('_email_template' => 'INNER JOIN `email_templates` ON `notification_tasks_email_templates`.`email_template_id` = `email_templates`.`id`'),
        '_users' => 'INNER JOIN `notification_tasks_users` ON `notification_tasks_users`.`notification_task_id` = `notification_tasks`.`id`',
        'users' => array('_users' => 'INNER JOIN `users` ON `notification_tasks_users`.`user_id` = `users`.`id`'),
    );

    protected $_rules = array(
        'user_id' => array('users' => '`users`.`id`'),
        'template_id' => array('email_template' => '`email_templates`.`id`'),
        'counter' => array('_users' => '`notification_tasks_users`.`counter`')
    );

    protected $_select = array(
        'users' => '`users`.`id` AS `user_id`, `notification_tasks_users`.`counter`',
        'email_template' => '`notification_tasks_email_templates`.`email_template_id`'
    );
} 