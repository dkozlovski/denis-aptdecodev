<?php
/**
 * Author: Alex P.
 * Date: 16.06.14
 * Time: 12:26
 */

class Notification_Model_NotificationTask extends \Application_Model_Abstract
{
    /**
     * @var array - id => \Notification_Model_EmailTemplate
     */
    protected static $emailTemplate  = array();

    /**
     * Restored parameters
     * @var array - id => \Application_Model_ObjectStorage
     */
    protected static $parametersStorage = array();

    /**
     * @var \Application_Model_User
     */
    protected $user;

    /**
     * Creates new task or adds user to recently created task.
     *
     * @param mixed $user - Pass collection to add multiple users at one time or single user object.
     * @param Notification_Model_EmailTemplate $emailTemplate - Template to bind
     * @param Application_Model_ObjectStorage $parameters (optional) - Necessary parameters
     * @return $this
     */
    public function newTask(
        $user,
        \Notification_Model_EmailTemplate $emailTemplate,
        \Application_Model_ObjectStorage $parameters = null
    ) {
        $parametersString = '';
        $filter = $this->getCollection()->getFlexFilter();
        $filter->template_id('=', $emailTemplate->getId());

        if ($parameters) {
            $parametersString = $parameters->serialize();
            $filter->parameters('=', $parametersString);
        }

        $this->setParameters($parametersString);

        $task = $filter->apply(1)->getFirstItem();
        if ($task->isExists()) {
            $this->setData($task->getData());
        } else {
            $this->save();
            $this->boundTemplate($emailTemplate);
        }

        if ($user instanceof \Application_Model_User_Collection) {
            foreach ($user as $userItem) {
                $this->boundUser($userItem);
            }
        } elseif ($user instanceof \Application_Model_User) {
            $this->boundUser($user);
        }

        return $this;
    }

    public static function instantTask
    (
        \Application_Model_User $user,
        $templatePath,
        \Application_Model_ObjectStorage $parameters = null
    ) {
        $task = new self;
        $task->setId(uniqid());
        if (!empty($parameters)) {
            self::$parametersStorage[$task->getId()] = $parameters;
        }
        $template = new Notification_Model_EmailTemplate();
        $template->setViewPath($templatePath);

        self::$emailTemplate[$task->getId()] = $template;

        $task->user = $user;

        return $task;
    }

    /**
     * Joins all related tables
     * @param array $additionalConditions
     * @param null $limit
     * @return \Notification_Model_NotificationTask_Collection
     */
    public static function getFullyJoinedTasks(array $additionalConditions = null, $limit = null)
    {
        $task = new self;
        $filter = $task->getCollection()->getFlexFilter();
        $filter->alwaysJoin('users')
            ->alwaysJoin('email_template');

        if ($additionalConditions) {
            foreach($additionalConditions as $method => $parameters) {
                call_user_func_array(array($filter, $method), $parameters);
            }
        }

        return $filter->apply($limit);
    }

    /**
     * Restore stored parameters.
     * @return \Application_Model_ObjectStorage
     */
    public function getParametersObject()
    {
        if (!isset(self::$parametersStorage[$this->getId()])) {
            $storage = new \Application_Model_ObjectStorage;
            if ($parameters = $this->getParameters()) {
                $storage->unserialize($parameters);
            }
            self::$parametersStorage[$this->getId()] = $storage;
        }

        return self::$parametersStorage[$this->getId()];
    }

    /**
     * Retrieves users related with task
     * @param array $additionalConditions (optional) - Conditions for filter: ['counter' => ['=', 1]]...
     * @param null $limit
     * @return \Application_Model_User_Collection
     */
    public function getUsers(array $additionalConditions = null, $limit = null)
    {
        $filter = $this->getCollection()->getFlexFilter();
        $filter->alwaysJoin('users');
        $filter->id('=', $this->getId());
        if ($additionalConditions) {
            foreach($additionalConditions as $method => $parameters) {
                call_user_func_array(array($filter, $method), $parameters);
            }
        }

        $users = new \Application_Model_User_Collection();
        return $users->getFlexFilter()->id('in', $filter->apply($limit)->getColumn('user_id'))->apply();
    }

    /**
     * @return User_Model_User
     * @throws Exception
     */
    public function getUser()
    {
        if (($userId = $this->getUserId()) && !$this->user) {
             $this->user = new \User_Model_User($userId);
        }

        if ($this->user) {
            return $this->user;
        }

        throw new \Exception('Cannot retrieve the user because task has not been fully joined.');
    }

    /**
     * Return related template
     * @return Notification_Model_EmailTemplate
     */
    public function getEmailTemplate()
    {
        if (!isset(self::$emailTemplate[$this->getId()])) {
            $task = $this->getCollection()
                ->getFlexFilter()
                ->alwaysJoin('email_template')
                ->id('=', $this->getId())
                ->apply(1)
                ->getFirstItem();

            self::$emailTemplate[$this->getId()] = new Notification_Model_EmailTemplate($task->getEmailTemplateId());
        }

        return self::$emailTemplate[$this->getId()];
    }

    /**
     * Increments counter for user
     * @return $this
     */
    public function incrementCounter()
    {
        if ($userId = $this->getUserId()) {
            $this->_getbackend()->incrementCounter($this, $userId);
        }
        return $this;
    }

    /**
     * Binds template with current task
     *
     * @param Notification_Model_EmailTemplate $template
     * @return $this
     */
    protected function boundTemplate(\Notification_Model_EmailTemplate $template)
    {
        $this->_getbackend()->boundTemplate($template, $this);
        return $this;
    }

    /**
     * Creates empty tasks collection object
     *
     * @return Notification_Model_NotificationTask_Collection
     */
    protected function getCollection()
    {
        return new Notification_Model_NotificationTask_Collection();
    }

    /**
     * Binds user with current task
     *
     * @param Application_Model_User $user
     * @return $this
     */
    protected function boundUser(\Application_Model_User $user)
    {
        $this->_getbackend()->boundUser($user, $this);
        return $this;
    }
}