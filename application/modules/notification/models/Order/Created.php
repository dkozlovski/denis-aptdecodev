<?php

class Notification_Model_Order_Created
{

    public function sendEmails($order)
    {
        $this->_toBuyer($order);
        $this->_toSeller($order);
    }

    protected function _toBuyer($order)
    {
        $buyer       = $order->getUser();
        $view        = new Zend_View();
        $view->buyer = $buyer;
        $view->order = $order;

        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));


        foreach ($order->getItemsForPickUp() as $item) {
            if ($item->getIsReceived() || $item->getIsDelivered()) {
                continue;
            }

            $view->item    = $item;
            $view->product = $item->getProduct();
            $view->seller  = $item->getProduct()->getSeller();

            if ($buyer->getMainEmail()) {
                $output2 = $view->render('order_created/order_notification_pickup_to_buyer.phtml');

                $toSend2 = array(
                    'email'   => $buyer->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output2);

                $mail2 = new Ikantam_Mail($toSend2, 'utf-8', 'hello@aptdeco.com');
                //$mail2->sendCopy($output2);
                $mail2->send(null, true);
            }
        }

        foreach ($order->getItemsForDelivery() as $item) {
            if ($item->getIsReceived() || $item->getIsDelivered()) {
                continue;
            }

            $view->item    = $item;
            $view->product = $item->getProduct();
            $view->seller  = $item->getProduct()->getSeller();

            if ($buyer->getMainEmail()) {
                $output2 = $view->render('order_created/order_notification_delivery_to_buyer.phtml');

                $toSend2 = array(
                    'email'   => $buyer->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output2);

                $mail2 = new Ikantam_Mail($toSend2, 'utf-8', 'hello@aptdeco.com');
                //$mail2->sendCopy($output2);
                $mail2->send(null, true);
            }
        }
    }

    protected function _toSeller($order)
    {
        $view        = new Zend_View();
        $view->order = $order;
        $view->buyer = $order->getUser();

        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        foreach ($order->getItemsForPickUp() as $item) {
            if ($item->getIsReceived() || $item->getIsDelivered()) {
                continue;
            }

            $seller = $item->getProduct()->getSeller();

            $payout = new Order_Model_Payout();
            $payout->getByItemId($item->getId());

            $view->product = $item->getProduct();
            $view->seller  = $seller;
            $view->item    = $item;
            $view->payout  = $payout;

            if ($seller->getMainEmail()) {
                $output = $view->render('order_created/order_notification_pickup_to_seller.phtml');

                $toSend = array(
                    'email'   => $seller->getMainEmail(),
                    'subject' => 'Purchase Request for ' . $item->getProduct()->getTitle(),
                    'body'    => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                //$mail->sendCopy($output);
                $mail->send(null, true);
            }
        }

        foreach ($order->getItemsForDelivery() as $item) {
            if ($item->getIsReceived() || $item->getIsDelivered()) {
                continue;
            }

            $seller = $item->getProduct()->getSeller();

            $payout = new Order_Model_Payout();
            $payout->getByItemId($item->getId());

            $view->product = $item->getProduct();
            $view->item    = $item;
            $view->seller  = $seller;
            $view->payout  = $payout;

            if ($seller->getMainEmail()) {
                $output = $view->render('order_created/order_notification_delivery_to_seller.phtml');

                $toSend = array(
                    'email'   => $seller->getMainEmail(),
                    'subject' => 'Purchase Request for ' . $item->getProduct()->getTitle(),
                    'body'    => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                //$mail->sendCopy($output);
                $mail->send(null, true);
            }
        }
    }

}