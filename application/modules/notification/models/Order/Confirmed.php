<?php

class Notification_Model_Order_Confirmed
{
    protected $view;

    public function __construct()
    {
        $this->view = new Zend_View();
        $this->view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $this->view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));
    }

    public function sendEmails($item)
    {
        $this->toBuyer($item);
        $this->toSeller($item);
        $this->toCarrier($item);
    }

    public function toBuyer($item)
    {
        $buyer  = $item->getOrder()->getUser();
        $seller = $item->getProduct()->getSeller();

        $view = $this->_getView();

        $view->product = $product = $item->getProduct();
        $view->item    = $item;
        $view->buyer   = $buyer;
        $view->seller  = $seller;

        $view->pickupAddressStr = implode(', ', array_filter(array(
                    $product->getPickupAddressLine1(),
                    $product->getPickupAddressLine2(),
                    $product->getPickupCity(),
                    $product->getPickupState(),
                )));

        $view->pickupPhone = $product->getPickupPhone();

        if ($buyer->getMainEmail()) {
            if ($item->getShippingMethodId() === 'delivery') {
                $output = $view->render('order_confirmed/to_buyer_delivery.phtml');
            } else {
                $output = $view->render('order_confirmed/to_buyer_pickup.phtml');
            }


            $toSend = array(
                'email'   => $buyer->getMainEmail(),
                'subject' => 'Purchase Request Confirmed - ' . $item->getProduct()->getTitle(),
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            //$mail->sendCopy($output);
            $mail->send(null, true);

            //send receipt
            $output2 = $view->render('order_confirmed/receipt_to_buyer.phtml');

            $toSend2 = array(
                'email'   => $buyer->getMainEmail(),
                'subject' => 'Your Billing Receipt - Order ' . $item->getId(),
                'body'    => $output2);

            $mail2 = new Ikantam_Mail($toSend2, 'utf-8', 'hello@aptdeco.com');
            //$mail2->sendCopy($output2);
            $mail2->send(null, true);
        }
    }

    public function toSeller($item)
    {
        $buyer  = $item->getOrder()->getUser();
        $seller = $item->getProduct()->getSeller();

        $view = $this->_getView();

        $payout = new Order_Model_Payout();
        $payout->getByItemId($item->getId());

        $view->product = $item->getProduct();
        $view->item    = $item;
        $view->buyer   = $buyer;
        $view->seller  = $seller;
        $view->payout  = $payout;
        
        $address = $item->getOrder()->getShippingAddress();

        $view->shippingAddressStr = implode(', ', array_filter(array(
                    $address->getAddressLine1(),
                    $address->getAddressLine2(),
                    $address->getCity(),
                    $address->getStateCode(),
                    $address->getPostalCode(),
                )));

        $view->shippingPhone = $address->getTelephone();

        if ($seller->getMainEmail()) {

            if ($item->getShippingMethodId() === 'delivery') {
                $output = $view->render('order_confirmed/to_seller_delivery.phtml');
            } else {
                $output = $view->render('order_confirmed/to_seller_pickup.phtml');
            }

            $toSend = array(
                'email'   => $seller->getMainEmail(),
                'subject' => 'Sale Confirmed - ' . $item->getProduct()->getTitle(),
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            //$mail->sendCopy($output);
            $mail->send(null, true);
        }
    }

    public  function toCarrier($item)
    {
        /* @var $item Admin_Model_Order_Item */
        $order = $item->getOrder();
        $settings = new Admin_Model_Settings();

        $view = $this->_getView();

        $email   = $settings->getValue('carrier_email', 'email');
        $text    = nl2br($settings->getValue('carrier_email', 'text'));
        $subject = $settings->getValue('carrier_email', 'subject');

        $validator = new Zend_Validate_EmailAddress();

        if (!$validator->isValid($email)) {
            return false;
        }

        /* Seller info */
        $seller = $item->getProduct()->getSeller();
        $sellerName = $seller->getFullName();
        $sellerEmail = $seller->getMainEmail();
        $pickUpAddress = nl2br($view->formatAddress($item->getProduct()));

        $sellerDeliveryOptions = $item->getSellerDeliveryOptions();

        $pickUpBuildingTypeText = '';
        if (!empty($sellerDeliveryOptions['building_type'])) {
            $pickUpBuildingType = $sellerDeliveryOptions['building_type'];
            if ($pickUpBuildingType == 'walkup') {
                $pickUpBuildingTypeText =  'Walk-up, flight #'. $view->escape($sellerDeliveryOptions['flights_of_stairs']);
            } elseif ($pickUpBuildingType == 'elevator') {
                $pickUpBuildingTypeText = 'Elevator';
            }
        }

        if ($item->getSellerDates()) {
            $pickUpDate = $view->formatDate($item->getSellerDates());
        } else {
            $pickUpDate = 'not specified';
        }

        /* Buyer info */
        $buyer = $item->getOrder()->getUser();
        $buyerName = $buyer->getFullName();
        $buyerEmail = $seller->getMainEmail();
        $deliveryAddress = nl2br($view->formatAddress($order->getShippingAddress()));

        $deliveryBuildingTypeText = '';
        if ($order->getShippingAddress()->getBuildingType() == 'walkup') {
            $deliveryBuildingTypeText = 'Walk-up, flight #' . $view->escape($order->getShippingAddress()->getNumberOfStairs());
        } elseif ($order->getShippingAddress()->getBuildingType() == 'elevator') {
            $deliveryBuildingTypeText = 'Elevator';
        }

        $deliveryDate = $view->formatDate($item->getBuyerDate());

        $replaces = array(
            '%product_title%',
            '%order_id%',

            '%seller_name%',
            '%seller_email%',
            '%pick_up_address%',
            '%pick_up_date%',
            '%pick_up_building_type%',

            '%buyer_name%',
            '%buyer_email%',
            '%delivery_address%',
            '%delivery_date%',
            '%delivery_building_type%'
        );
        $replacements = array(
            $item->getProductTitle(),
            $item->getOrderId(),

            $sellerName,
            $sellerEmail,
            $pickUpAddress,
            $pickUpDate,
            $pickUpBuildingTypeText,

            $buyerName,
            $buyerEmail,
            $deliveryAddress,
            $deliveryDate,
            $deliveryBuildingTypeText

        );

        $view->content = str_replace($replaces, $replacements, $text);
        $subject = str_replace($replaces, $replacements, $subject);
        $output = $view->render('order_confirmed/to_carrier.phtml');

        $toSend = array(
            'email'   => $email,
            'subject' => $subject,
            'body'    => $output);

        $mail = new Ikantam_Mail($toSend);

        try {
            //$mail->sendCopy($output);
            $mail->send(null, true);

            $item->setIsCarrierSentEmail(1)
                ->save();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function sendEmailToCarrier($item)
    {
        return $this->toCarrier($item);
    }

    protected function _getView()
    {
        return $this->view;
    }
}