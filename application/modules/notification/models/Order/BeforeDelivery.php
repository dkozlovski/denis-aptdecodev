<?php

class Notification_Model_Order_BeforeDelivery
{

    public function sendEmails($item)
    {
        $this->_toBuyer($item);
        $this->_toSeller($item);
    }

    protected function _toBuyer($item)
    {
        $buyer  = $item->getOrder()->getUser();
        $seller = $item->getProduct()->getSeller();

        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        $view->product = $item->getProduct();
        $view->item    = $item;
        $view->buyer   = $buyer;
        $view->seller  = $seller;

        if ($buyer->getMainEmail()) {
            $output = $view->render('before_delivery/to_buyer.phtml');

            $toSend = array(
                'email'   => $buyer->getMainEmail(),
                'subject' => 'Reminder: Your Upcoming Delivery - ' . $item->getProduct()->getTitle(),
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            $mail->sendCopy($output);
            $mail->send();
        }
    }

    protected function _toSeller($item)
    {
        $buyer  = $item->getOrder()->getUser();
        $seller = $item->getProduct()->getSeller();

        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        $payout = new Order_Model_Payout();
        $payout->getByItemId($item->getId());

        $view->product = $product = $item->getProduct();
        $view->item    = $item;
        $view->buyer   = $buyer;
        $view->seller  = $seller;
        $view->payout  = $payout;

        $view->pickupAddressStr = implode(', ', array_filter(array(
                    $product->getPickupAddressLine1(),
                    $product->getPickupAddressLine2(),
                    $product->getPickupCity(),
                    $product->getPickupState(),
                )));

        $view->pickupPhone = $product->getPickupPhone();


        if ($seller->getMainEmail()) {
            if ($item->getShippingMethodId() === 'delivery') {
                $output = $view->render('before_delivery/to_seller_delivery.phtml');
            } else {
                $output = $view->render('before_delivery/to_seller_pickup.phtml');
            }


            $toSend = array(
                'email'   => $seller->getMainEmail(),
                'subject' => 'Reminder: Your Upcoming Pick up - ' . $item->getProduct()->getTitle() . ' - Please Read Carefully',
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            $mail->sendCopy($output);
            $mail->send();
        }
    }

}