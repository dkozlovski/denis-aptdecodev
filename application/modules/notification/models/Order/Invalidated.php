<?php

class Notification_Model_Order_Invalidated
{

    public function sendEmails($item)
    {
        $this->_toBuyer($item);
        $this->_toSeller($item);
    }

    protected function _toBuyer($item)
    {
        $buyer  = $item->getOrder()->getUser();
        $seller = $item->getProduct()->getSeller();

        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        $view->product = $item->getProduct();
        $view->item    = $item;
        $view->buyer   = $buyer;
        $view->seller  = $seller;

        if ($buyer->getMainEmail()) {
            $output = $view->render('order_invalidated/to_buyer.phtml');

            $toSend = array(
                'email'   => $buyer->getMainEmail(),
                'subject' => 'Purchase Request Cancelled - ' . $item->getProduct()->getTitle(),
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            //$mail->sendCopy($output);
            $mail->send(null, true);
        }
    }

    protected function _toSeller($item)
    {
        return $item;
    }

}