<?php

class Notification_Model_Order_PaymentSent
{

    public function sendEmails($item)
    {
        $this->_toSeller($item);
    }

    protected function _toSeller($item)
    {
        $buyer  = $item->getOrder()->getUser();
        $seller = $item->getProduct()->getSeller();

        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        $payout = new Order_Model_Payout();
        $payout->getByItemId($item->getId());

        $view->product = $item->getProduct();
        $view->item    = $item;
        $view->buyer   = $buyer;
        $view->seller  = $seller;
        $view->payout  = $payout;

        if ($seller->getMainEmail()) {
            $output = $view->render('payment_sent/to_seller.phtml');

            $toSend = array(
                'email'   => $seller->getMainEmail(),
                'subject' => 'Payment sent - ' . $item->getProduct()->getTitle(),
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            //$mail->sendCopy($output);
            $mail->send(null, true);
        }
    }

}