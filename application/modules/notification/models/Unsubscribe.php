<?php
/**
 * Author: Alex P.
 * Date: 19.06.14
 * Time: 12:53
 */

class Notification_Model_Unsubscribe extends Application_Model_Abstract
{
    /**
     * Creates code for user
     * @param Application_Model_User $user
     * @return $this
     */
    public function createCodeForUser(\Application_Model_User $user)
    {
        $this->getBy_user_id($user->getId());
        if (!$this->isExists()) {
            $code = trim(str_replace(rand(0, 9),'_', hash('sha256', $user->getId() . $user->getFullName() . time())), '_');

            $this->setCode($code)
                ->setUserId($user->getId())
                ->setIsUserConfirm(0)
                ->save();
        }

        return $this;
    }

    /**
     * Marks user as unsubscribed using code
     * @param $code
     */
    public function unsubscribe($code)
    {
        $this->getBy_code($code);
        if ($this->isExists()) {
            $this->setIsUserConfirm(1)->save();
        }
    }
} 