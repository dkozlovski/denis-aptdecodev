<?php

class Material_Form_Material extends Zend_Form
{

    public function init()
    {  
        $title = new Zend_Form_Element_Text('title');
        
        $title->setLabel('Enter new material*')
              ->setRequired()
              ->addValidator('Db_NoRecordExists',false ,array('table'=>'materials','field'=>'title'));
              
        $submit = new Zend_Form_Element_Submit('submit');
        
        $submit->setLabel('Save');
        
        $this->addElements(array($title, $submit));        
	
    }
    
    public function throwException ()
    {
    $msg = '';
    foreach($this->getMessages() as $field => $messages)
    {
    $msg.='<br /><b>Field "'.$field.'":</b><br />';
    foreach($messages as $message)
    {
      $msg.=$message.'<br />';
    }
   }
    if($msg!='')
    {
         throw new Exception($msg); 
    }
    return false;
    }


}

