<?php

class Material_AddController extends Ikantam_Controller_Front
{

	private $_materialForm;

	public function init()
	{


		$this->_setMaterialForm(new Material_Form_Material());
		parent::init();
	}


    public function indexAction() 
    {
        $this->view->form = $this->_getMaterialForm();

    }
    
    public function postAction ()
    {
    $form = $this->_getMaterialForm();
    $postData = $this->getRequest()->getPost();
        
            if($form->isValid($postData))
            {
                $material = new Application_Model_Material();
                $material->setTitle($form->getValue('title'))->save();
            } else
                {
                   $form->throwException();
                }
            
    $this->_helper->redirector(array('module'=>'material', 'controller'=>'add', 'action'=>'index'));

           
    }
    
    
	protected function _setMaterialForm(\Zend_Form $form)
	{
	   $form->setAction(Ikantam_Url::getUrl('material/add/post'));
		$this->_materialForm = $form;
		return $this;
	}

	protected function _getMaterialForm()
	{
		return $this->_materialForm;
	}
}
