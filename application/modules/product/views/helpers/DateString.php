<?php

class Zend_View_Helper_DateString extends Zend_View_Helper_Abstract
{

	public function dateString($value)
	{
		if(is_numeric($value)){
		      $value = date('M d, Y', $value);
		}
		return $value;
	}

}
