<?php

class Product_AddController extends Ikantam_Controller_Front
{

    const ERROR_MESSAGE_DATA_REQUIRED       = 'Opps, looks like some of the fields have
                    errors or were left blank. Please update all fields highlighted
                    in red and publish again';
    const ERROR_MESSAGE_IMAGE_REQUIRED      = 'Image required';
    const SUCCESS_MESSAGE_PRODUCT_PUBLISHED = 'Your changes have been saved.';
    const SUCCESS_PRODUCT_ID = 'success_product_id';

    private $_productForm;

    /**
     * @Inject
     * @var Ikantam\ZendFormToJqueryValidation\Form
     */
    protected $formConverter;

    public function init()
    {
        $addresses = $this->_getAddresses();

        $this->_setProductForm(
            new Product_Form_NewItem(
                array('product' => $this->_getProduct(), 'addresses' => $addresses)
            )
        );

    }

    public function successAction()
    {
        $this->view->session = $this->getSession();

        $product = new Product_Model_Product($this->getSession()->activeData(self::SUCCESS_PRODUCT_ID));
        if (!$product->getId()) {
            $this->redirect('product/add/new');
        }

        $this->view->product = $product;
    }

    public function indexAction()
    {
        $this->_forward('new');
    }


    public function newAction()
    {
        $this->view->headTitle('Sell my used furniture');
        $this->view->headMeta()->appendName('description', 'Post your used furniture items for sale on AptDeco.');
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/validate/jquery.validate.min.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jQuery-File-Upload-master/js/vendor/jquery.ui.widget.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jQuery-File-Upload-master/js/jquery.iframe-transport.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jQuery-File-Upload-master/js/jquery.fileupload.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/input.spin.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/aptdeco_image.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/plugins/iCheck/icheck.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/aptdeco.login.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/form.validation.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/aptdeco/product.form.validation.defaults.js'));
        //$this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/jquery-ui.css'));
        //quiz popup

        $this->view->isNeedToShowQuiz = $this->_isNeedToShowQuiz();

        $request              = $this->getRequest();
        $form                 = $this->_getProductForm();
        $product              = $this->_getProduct();
        $categoryCollection   = new Category_Model_Category_Collection();
        $this->view->messages = array();

        $form->setProduct($product);
        $form->populateByProduct($product);

        if ($request->isPost()) {

            foreach ($product->getExtraAllImages() as $image) {
                $image->setIsVisible(1)->save();
            }

            if ($request->getPost('action_type') === 'save') {

                $form->_setNotRequired();

                if ($form->isValid($request->getPost())) {

                    $productData = $product->convertProductData($form);

                    $product->setIsPublished(0)
                        ->setIsApproved(0)
                        ->setIsVisible(1)
                        ->setCreatedAt(time())
                        ->setUserId($this->getSession()->getUser()->getId())
                        ->setPickUpAddressId($this->saveAddress()->getId())
                        ->setEAVAttributeValue('initial_price', $form->getValue('price'))
                        ->save();

                    //Antique collection?
                    if ($this->getParam('is_antiques', false)) {
                        $this->_addToAntiqueSet($product);
                    }
                    //Kid's corner collection?
                    if ($this->getParam('is_kids_corner', false)) {
                        $this->_addToKidsCornerSet($product);
                    }

                    $this->getSession()->setProductId(null);

                    if (!$this->getSession()->isLoggedIn()) {
                        $this->getSession()->activeData('publishProductRedirect', 'Save');
                        $this->getSession()->activeData('_product_data', array('id' => $product->getId(), 'is_published' => 0));
                        $this->redirect('registration/last-step');
                    } else {
                        $this->_helper->redirector('list', 'sale', 'user');
                    }
                } else {
                    $form->populate($request->getPost());
                }
            } else {
                $mainImage = $product->getMainImage();

                if ($form->isValid($request->getPost()) && ($mainImage->getId() || (APPLICATION_ENV !== 'live' && APPLICATION_ENV !== 'testing'))) {
                    $productData = $product->convertProductData($form);

                    $product->setIsVisible(1)
                        ->setIsApproved(0)
                        ->setCreatedAt(time())
                        ->setUserId($this->getSession()->getUser()->getId())
                        ->setPickUpAddressId($this->saveAddress()->getId())
                        ->setEAVAttributeValue('initial_price', $productData['price'])
                        ->save();

                    //Antique collection?
                    if ($this->getParam('is_antiques', false)) {
                        $this->_addToAntiqueSet($product);
                    }
                    //Kid's corner collection?
                    if ($this->getParam('is_kids_corner', false)) {
                        $this->_addToKidsCornerSet($product);
                    }

                    $this->getSession()->setProductId(null);
                    $this->getSession()->activeData(self::SUCCESS_PRODUCT_ID, $product->getId());

                    //$message = self::SUCCESS_MESSAGE_PRODUCT_PUBLISHED;

                    if (!$this->getSession()->isLoggedIn()) {
                        $this->getSession()->activeData('publishProductRedirect', true);
                        $this->getSession()->activeData('_product_data', array('id' => $product->getId(), 'is_published' => 1));

                        $this->redirect('registration/last-step');
                    }


                    $product->setIsPublished(1)->save();

                    $this->_helper->redirector('success', 'add', 'product');
                } else {
                    $form->getSubform('additional_details')->getElement('available_from')->removeFilter('Zend_Filter_LocalizedToNormalized');
                    $form->getSubform('additional_details')->getElement('available_till')->removeFilter('Zend_Filter_LocalizedToNormalized');

                    $form->populate($request->getPost());

                    if (!$form->isValid($request->getPost())) {
                        if (APPLICATION_ENV === 'live') {
                            file_put_contents(APPLICATION_PATH . '/log/products.log', serialize($form->getMessages()), FILE_APPEND);
                        }

                        $dimensionsError = false;

                        foreach ($form->getMessages() as $field => $messages) {
                            if (in_array($field, array('width', 'depth', 'height'))) {
                                $dimensionsError = true;
                            }
                        }

                        $this->view->messages[] = self::ERROR_MESSAGE_DATA_REQUIRED;

                    }

                    if (!$mainImage->getId()) {
                        $this->view->messages[] = self::ERROR_MESSAGE_IMAGE_REQUIRED;
                    }
                }
            }
        }

        $this->view->form           = $form;
        $this->view->catsConfig     = Zend_Json::encode($categoryCollection->getJsonConfig2());
        $this->view->session        = $this->getSession();
        $this->view->product        = $product;
        $this->view->productImages  = $product->getExtraAllImages();
        $this->view->hasLockedImage = 0;
        $this->view->imageEndpoints = Product_Model_Image::getEndpoints();
        // This option is necessary to use with facebook JS SDK (popup login)
        //$facebook = $this->getXmlConfig('additional_settings', 'facebook');
        $facebook = Zend_Registry::get('config')->facebook;
        $this->jsConfig->addOption('facebook_app_id', $facebook->appId);

        // Used to client side form validation
        $this->jsConfig->addOption('jquery_validation_json', $this->formConverter->setForm($form)->toJSON());


        $addresses = $form->getAddresses();
        $options   = $addresses->getJsonConfigByUserId();

        $this->view->pickupAddresses = Zend_Json::encode($options);
    }

    public function saveAddress()
    {
        $form              = new Product_Form_Product2();
        $pickupAddressForm = $form->getSubform('pickup_address');
        $pickupAddressData = $this->getRequest()->getPost('pickup_address');
        $address           = new User_Model_Address();

        if (!$pickupAddressForm->isValid($pickupAddressData)) {
            return $address;
        }

        $addressData                 = $pickupAddressForm->getValues();
        $addressData                 = $addressData['pickup_address'];
        $addressData['postcode']     = $addressData['postal_code'];
        $addressData['phone_number'] = $addressData['telephone_number'];
        $addressForm                 = new User_Form_Address();

        if ($addressForm->isValid($addressData)) {

            $address->addData($addressForm->getValues())
                ->setUserId($this->getSession()->getUserId())
                ->setIsPrimary(0);

            if ($this->getSession()->isLoggedIn()) {
                $address->save();
            } else {
                $this->getSession()->activeData('_guest_shipping_address', serialize($address));
            }
        }

        return $address;
    }

    /**
     * This action used for obtaining necessary user data after he has logged via AJAX
     */
    public function userInfoAction()
    {
        if (!$this->isAjax()) {
            $this->_helper->show404();
        }
        $result = array(
            'show_quiz' => $this->_isNeedToShowQuiz(),
            'addresses_html' => $this->_getProductForm()->getAddresses()->toHtml(),
            'addresses' => $this->_getProductForm()->getAddresses()->getJsonConfigByUserId(),
        );


        $this->_helper->json($result);
    }

    protected function _setProductForm(\Zend_Form $form)
    {
        $this->_productForm = $form;
        return $this;
    }

    /**
     *
     * @return Ikantam_Form
     */
    protected function _getProductForm()
    {
        return $this->_productForm;
    }

    /**
     * Get product from session
     *
     * @return \Product_Model_Product
     */
    protected function _getProduct()
    {
        if ($this->getSession()->getProductId()) {
            $productId = $this->getSession()->getProductId();
            $product   = new Product_Model_Product($productId);

            if ($product->getId()) {
                return $product;
            }
        }

        $product = new Product_Model_Product();
        $product->setIsPublished(0)
            ->setIsVisible(0)
            ->setUserId($this->getSession()->getUserId())
            ->setCreatedAt(time())
            ->save();

        $this->getSession()->setProductId($product->getId());

        return $product;
    }

    public function uploadImageAction()
    { /*echo Zend_Json::encode([
            'success' => true,
            'message' => time() . ' test error message',
            'file' => [
                'url' => "https://d21fnw4i5ddu1p.cloudfront.net/product-images/10001-20000/15214/14071542597c88de230e037641187471461e89953d/500-500-crop-0.jpg",
                'id' => 123
            ]
        ]); exit;*/
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $response = array(
            'cell_id' => $this->getParam('cell_id'),
        );

        $productId = $this->getRequest()->getParam('product_id');
        $product   = new Product_Model_Product($productId);

        if (!$product->getId() || $product->getId() != $this->getSession()->getProductId()) {

            if (!$this->getSession()->isLoggedIn() || !$product->isBelongsToUser($this->getSession()->getUser())) {
                //@TODO:return error message
                die();
            }
        }

        $basePath = Product_Model_Image::getBaseTmpDir();
        $path     = Product_Model_Image::getTmpUploadDir($product);


        $uploader = new Ikantam_File_Uploader();

        $uploader->setAdapter(new Zend_File_Transfer_Adapter_Http)
            ->setUploadPath($basePath . DIRECTORY_SEPARATOR . $path);

        if (!$uploader->upload()) {
            $errors = $uploader->getErrorMessages();

            $response['success'] = false;
            $response['error']   = $response['message'] = implode("\n", $errors);
            $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(json_encode($response));
        } else {
            $fileName = $uploader->getFileName();

            $iConverter = new Ikantam_File_ImageConverter();

            $newFileName = $basePath . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . 'original-0.jpg';
            $iConverter->convert($fileName, $newFileName, 90);

            $res = $iConverter->createScaledImages($newFileName, $basePath . DIRECTORY_SEPARATOR . $path);

            if ($res) {
                $fileNames   = $iConverter->getFileName();
                $fileNames[] = $newFileName;
            } else {
                //@TODO: return error;
            }

            foreach ($fileNames as $file) {
                $e = Product_Model_Image::getTmpS3Path($file);
                $uploader->uploadToS3($file, $e);
                unlink($file);
            }

            unlink($fileName);

            $productImage = new Product_Model_Image();

            if ($description = $this->getParam('image_spec') and $description !== 'null') {
                $descriptionFilter = new Zend_Filter;
                $descriptionFilter->addFilter(new Zend_Filter_StripTags())
                    ->addFilter(new Zend_Filter_HtmlEntities())
                    ->addFilter(new Zend_Filter_StringTrim());

                $productImage->setDescription($descriptionFilter->filter($description));
            }


            $productImage->setProductId($product->getId())
                ->setPath('original-0.jpg')
                ->setS3Path($path)
                ->setIsVisible(0)
                ->setIsMain(0)
                ->setIsLocked(0)
                ->setIsUploaded(0)
                ->save();


            $response['success']     = true;
            $response['file']['id']  = $productImage->getId();
            //$response['file']['name'] = $path . '/original-0.jpg';
            $response['file']['url'] = $productImage->getS3Url(500, 500, 'crop');
        }



        $this->getResponse()
             // text/plain because Safari 4.x and old IE try to download if header is application/json
            ->setHeader('Content-type', 'text/plain')
            ->setBody(Zend_Json::encode($response));
    }

    public function rotateImageAction()
    {
        //sleep(2);

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot rotate this image',
            'redirect' => false
        );

        $productId = $this->getRequest()->getParam('product_id');
        $imageId   = $this->getRequest()->getParam('image_id');
        $angle     = (int) ($this->getRequest()->getParam('angle') % 360);
        $product   = new Product_Model_Product($productId);

        /* if ($angle == 0) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/11368/1387148527ad03ee0b10af477a6918bd2735229acf/360-300-frame-0.jpg';
          }
          if ($angle == 90) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/12783/1389904967c793875ba1c326b544d0fefa3d1e5315/360-300-frame-0.jpg';
          }
          if ($angle == 180) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/1-10000/8967/13815191586f1b800b9c52989e796a9fb22d936fa5/360-300-frame-0.jpg';
          }
          if ($angle == 270) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/12753/138989328144e8c146a01131b527561d7406a7aac8/360-300-frame-0.jpg';
          }

          $response['success'] = true;
          $response['error']   = false;

          $this->_helper->json($response); */

        if (!in_array($angle, array(0, 90, 180, 270), true)) {
            $this->_helper->json($response);
        }

        if (!$product->getId() || $product->getId() !== $this->getSession()->getProductId()) {
            $this->_helper->json($response);
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $image->setDefaultAngle($angle)->save();

        if ($angle == 0 || ($angle == 90 && $image->get90Exists()) || ($angle == 180 && $image->get180Exists()) || ($angle == 270 && $image->get270Exists())) {
            $response['success'] = true;
            $response['error']   = false;
            $response['url']     = $image->getS3Url(500, 500, 'crop');

            $this->_helper->json($response);
        }

        if ($angle == 90 && !$image->get90Exists()) {
            $image->set90Exists(1)->save();
        }

        if ($angle == 180 && !$image->get180Exists()) {
            $image->set180Exists(1)->save();
        }

        if ($angle == 270 && $image->get270Exists()) {
            $image->set270Exists(1)->save();
        }

        $uploadDir    = APPLICATION_PATH . '/../tmp/product-images';
        $oldUploadDir = APPLICATION_PATH . '/../product-images';
        $imagePath    = $image->getS3Path();

        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;

        $oi = $cloudFrontUrl . '/product-images/' . $imagePath;

        $parts = explode('/', $imagePath);

        $tPath = $uploadDir;

        foreach ($parts as $part) {
            $tPath = $tPath . DIRECTORY_SEPARATOR . $part;
            if (!file_exists($tPath)) {
                mkdir($tPath, 0777);
            }
        }

        $file_path = $uploadDir . DIRECTORY_SEPARATOR . $imagePath;
        //КОСТЫЛЬ!
        if (!file_exists($file_path)) {
            $file_path = $oldUploadDir . DIRECTORY_SEPARATOR . $imagePath;
        }

        $img = new Ikantam_Image();
        $img->rotateOriginalImage($oi, $file_path, $angle);

        $img->createScaledImages($uploadDir, $imagePath, $productId, $angle);

        $response['success'] = true;
        $response['error']   = false;
        $response['url']     = $image->getS3Url(500, 500, 'crop');

        $this->_helper->json($response);
    }

    public function removeImageAction()
    {
        //sleep(2);

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot delete this image',
            'redirect' => false
        );

        /* $response['success'] = true;
          $response['error']   = false;
          $response['message'] = null;

          $this->_helper->json($response); */

        $productId = $this->getRequest()->getParam('product_id');
        $imageId   = $this->getRequest()->getParam('image_id');
        $product   = new Product_Model_Product($productId);

        if (!$product->getId() || $product->getId() !== $this->getSession()->getProductId()) {
            $this->_helper->json($response);
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $image->delete();

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

    public function setDefaultImageAction()
    {
        //sleep(2);

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot set this image as default',
            'redirect' => false
        );

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);

        $productId = $this->getRequest()->getParam('product_id');
        $imageId   = $this->getRequest()->getParam('image_id');
        $product   = new Product_Model_Product($productId);

        if (!$product->getId() || $product->getId() !== $this->getSession()->getProductId()) {
            $this->_helper->json($response);
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $image->setDefaultImage($product->getId(), $image->getId());

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

    /**
     * Add product to antiques
     *
     * @param Product_Model_Product $product
     * @return self
     */
    protected function _addToAntiqueSet(\Product_Model_Product $product)
    {
        $set = new Application_Model_Set;
        $set->getBy_name('Vintage + Antiques');
        if ($set->isExists()) {
            $set->addProducts($product);
        }

        return $this;
    }

    /**
     * Add product to kid's corner
     *
     * @param Product_Model_Product $product
     * @return self
     */
    protected function _addToKidsCornerSet(\Product_Model_Product $product)
    {
        $set = new Application_Model_Set;
        $set->getBy_name('Kid\'s Corner');
        if ($set->isExists()) {
            $set->addProducts($product);
        }

        return $this;
    }

    /**
     * Determine whether need to show quiz form before product will be submitted
     * @return bool
     */
    protected function _isNeedToShowQuiz()
    {
        $isNeedToShowQuiz = true;
        if ($this->getSession()->isLoggedIn()) {
            $pc   = new Product_Model_Product_Collection;
            $user = $this->getSession()->getUser();

            //if no products
            $isNeedToShowQuiz = ($pc->getFlexFilter()->user_id('=', $user->getId())->is_published('=', 1)->count() < 1);

            $isNeedToShowQuiz = $isNeedToShowQuiz && ($user->personalSettings('selling_reason') === null);
        }

        return $isNeedToShowQuiz;
    }

    protected function _getAddresses($limit = 3)
    {
        $addresses = new User_Model_Address_Collection();
        return $addresses->getLastAddressesByUserId($this->getSession()->getUser()->getId(), 3);
    }

    /**
     * Validate form via ajax request.
     *
     */
    public function isValidAction()
    {
        if (!$this->isAjax()) {
            throw new Zend_Controller_Action_Exception('Page not found.', 404);
        }

        $data = $this->getRequest()->getPost();

        $this->_helper->json(array('valid' => $this->_getProductForm()->isValid($data)));
    }

    public function getRevenueAction()
    {
        $floatFormat = new Zend_Filter_LocalizedToNormalized(array('locale' => 'en_US', 'precision' => 2));

        $price = (float) $floatFormat->filter($this->getParam('price'));

        if ($price <= 0) {
            $this->_helper->json->sendJson(array(
                    'success' => false
                ));
        }

        $options = new Application_Model_Options();

        $comission = $options->getIndividualFeeOrDefault($this->getSession()->getUserId(), Application_Model_Options::SELLER, $price);

        $revenue = $price - $price * $comission / 100;

        $this->_helper->json->sendJson(array(
                'success'  => true,
                'revenue'  => (string) $this->view->currency($revenue),
            ));
    }


}
