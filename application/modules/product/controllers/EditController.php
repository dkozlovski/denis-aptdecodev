<?php

class Product_EditController extends Ikantam_Controller_Front
{

    const ERROR_MESSAGE_DATA_REQUIRED       = 'Opps, looks like some of the fields have errors or were left blank. Please update all fields highlighted in red and publish again';
    const ERROR_MESSAGE_IMAGE_REQUIRED      = 'Image required';
    const SUCCESS_MESSAGE_PRODUCT_PUBLISHED = 'Your changes have been saved.';

    private $_productForm;
    private $_product;
    protected $_relatedObjects = array();
    protected $_js             = array(
        'js/jQuery-File-Upload-master/js/vendor/jquery.ui.widget.js',
        'js/jQuery-File-Upload-master/js/jquery.iframe-transport.js',
        'js/jQuery-File-Upload-master/js/jquery.fileupload.js',
        'js/jquery/validate/jquery.validate.min.js',
        'js/input.spin.js',
        'js/aptdeco_image.js',
        'js/plugins/iCheck/icheck.js',
        'js/iKantam/modules/form.validation.js',
        'js/iKantam/aptdeco/product.form.validation.defaults.js'

    );

    /**
     * @Inject
     * @var Ikantam\ZendFormToJqueryValidation\Form
     */
    protected $formConverter;

    public function indexAction()
    {
        $product = $this->_getProduct();
        $form    = $this->_getProductForm($this->_getFormData());

        $this->view->form           = $form;
        $this->view->product        = $product;
        $this->view->productImages  = $product->getExtraAllImages();
        $this->view->hasLockedImage = (int) $product->hasLockedImage($this->view->productImages);
        $this->view->popup          = $this->_getParam('popup');

        // Used to client side form validation
        $this->jsConfig->addOption('jquery_validation_json', $this->formConverter->setForm($form)->toJSON());
        $this->jsConfig->addOption('product_published', (bool)$product->getIsPublished());
        $this->jsConfig->addOption('validation_show_required', (bool)$this->_getParam('publish'));

        $this->_setAdditionalViewData();

        $request = $this->getRequest();
        // $request->getParam('referer'): if user click "publish" then redirect(post) occurs with additional params
        // {publish : true, referer : 'list'} - publish: to determine whether need to publish product
        // referer: list - to determine if post was sent from list or from edit page
        // See: user/views/scripts/sale.list.js.phtml:80
        if ($request->isPost() && !$request->getParam('referer') == 'list') {
            $form    = $this->_getProductForm();
            $product = $this->_getProduct();
            $titleBackup = $product->getTitle();

            if (!$product->getIsPublished() && !$request->getParam('publish')) {
                $form->_setNotRequired();
            }

            foreach ($product->getExtraAllImages() as $image) {
                $image->setIsVisible(1)->save();
            }

            $mainImage = $product->getMainImage();

            if ($form->isValid($request->getPost()) && ($mainImage->getId() || (APPLICATION_ENV !== 'live' && APPLICATION_ENV !== 'testing'))) {
                $productData = $product->convertProductData($form);

                $pickupAddress = $this->_saveAddress();

                $product->addData($productData)
                    ->setIsVisible(1)
                    ->setPickUpAddressId($pickupAddress->getId())
                    ->setPickupAddressLine1($pickupAddress->getAddressLine1())
                    ->setPickupAddressLine2($pickupAddress->getAddressLine2())
                    ->setPickupCity($pickupAddress->getCity())
                    ->setPickupState($pickupAddress->getState())
                    ->setPickupCountryCode($pickupAddress->getCountryCode())
                    ->setPickupPostalCode($pickupAddress->getPostalCode())
                    ->setPickupPostcode($pickupAddress->getPostcode())
                    ->setPickupPhone($pickupAddress->getPhoneNumber())
                    ->setPickupFullName($pickupAddress->getFullName());

                // user can't change title for approved product
                if ($product->getIsApproved() == 1) {
                    $product->setTitle($titleBackup);
                }

                // disable product expiration date on if period of availability was changed        
                $changes = $product->getExistChanges();
                if (isset($changes['available_till']) || isset($changes['available_from'])) {
                    $product->setExpireAt(0);
                }

                if ($this->getParam('publish')) {
                    $product->setIsPublished(1);
                }

                $initialPrice = $product->getEAVAttributeValue('initial_price');
                if ($initialPrice === null) {
                    $initialPrice = max($product->getPrice(), $product->getOldPrice());
                    $product->setEAVAttributeValue('initial_price', $initialPrice);
                }

                $product->save();

                //Antique collection?
                if ($this->getParam('is_antiques', false)) {
                    $this->_getRelatedObject('AntiqueSet')->addProducts($product);
                } else {
                    $this->_getRelatedObject('AntiqueSet')->removeProducts($product);
                }
                //Kid's corner collection?
                if ($this->getParam('is_kids_corner', false)) {
                    $this->_getRelatedObject('KidsCornerSet')->addProducts($product);
                } else {
                    $this->_getRelatedObject('KidsCornerSet')->removeProducts($product);
                }

                $message = self::SUCCESS_MESSAGE_PRODUCT_PUBLISHED;

                $this->getSession()->addMessage('success', $message);

                $this->_helper->redirector('list', 'sale', 'user');
            } else {
                $form->getSubform('additional_details')->getElement('available_from')->removeFilter('Zend_Filter_LocalizedToNormalized');
                $form->getSubform('additional_details')->getElement('available_till')->removeFilter('Zend_Filter_LocalizedToNormalized');

                $form->populate($request->getPost());

                if (!$form->isValid($request->getPost())) {
                    $this->view->messages[] = self::ERROR_MESSAGE_DATA_REQUIRED;
                }

                if (!$mainImage->getId()) {
                    $this->view->messages[] = self::ERROR_MESSAGE_IMAGE_REQUIRED;
                }
            }
        }
    }

    public function changeAvailablePeriodPostAction()
    {
        $form    = new Product_Form_AvailablePeriod();
        $product = $this->_getProduct();

        if ($form->isValid($this->getRequest()->getPost())) {
            $productData   = array();
            $dateTime      = new DateTime();
            if ($availableFrom = $form->getValue('available_from')) {

                if (is_array($availableFrom)) {
                    $year                          = $availableFrom['year'];
                    $month                         = $availableFrom['month'];
                    $day                           = $availableFrom['day'];
                    $productData['available_from'] = $dateTime->setDate($year, $month, $day)->setTime(0, 0, 0)->getTimestamp();
                } elseif (is_string($availableFrom)) {
                    $productData['available_from'] = strtotime($availableFrom);
                }
            }

            if ($availableTill = $form->getValue('available_till')) {
                if (is_array($availableTill)) {
                    $year                          = $availableTill['year'];
                    $month                         = $availableTill['month'];
                    $day                           = $availableTill['day'];
                    $productData['available_till'] = $dateTime->setDate($year, $month, $day)->setTime(23, 59, 59)->getTimestamp();
                } elseif (is_string($availableTill)) {
                    $productData['available_till'] = strtotime($availableTill);
                }
            }
            $product->addData($productData)
                    ->save();

            $this->getSession()->addMessage('success', self::SUCCESS_MESSAGE_PRODUCT_PUBLISHED);
            $this->_helper->redirector('list', 'sale', 'user');
        }

        $this->getSession()->addMessage('error', self::ERROR_MESSAGE_DATA_REQUIRED);
        $this->redirect('product/edit/index', array('id' => $product->getId()));
    }

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $addresses = new User_Model_Address_Collection();
        $addresses->getLastAddressesByUserId($this->getSession()->getUser()->getId(), 3);
        $product = $this->_getProduct();

        //We show 3 last user addresses by default, but product could has address out of that range (3 last)
        //So it is need to be added
        if ($addressId = $product->getPickUpAddressId()) {
            //Exactly collection ;)
            $productAddress = new User_Model_Address_Collection();
            $productAddress->getFlexFilter()->id('=', $addressId)->apply(1);
            // Merging exclude duplicates
            $addresses = $addresses->merge($productAddress);
        }

        $form = new Product_Form_NewItem(
            array('addresses' => $addresses, 'product' => $product)
        );

        $form->setAction(Ikantam_Url::getUrl('product/edit/' . $this->_getProduct()->getId()));
        if ($this->getParam('publish')) {
            $publish = new Zend_Form_Element_Hidden('publish');
            $form->addElement($publish);
        }

        $this->_setProductForm($form);
    }

    /**
     * Reduces the product price by id & hash signature
     */
    public function reducePriceAction()
    {
        $product = new Product_Model_Product($this->getParam('id'));
        $isValidSignature = $product->getHashSignature() === $this->getParam('signature');

        if ($product->isBelongsToUser($this->getSession()->getUser()) && $isValidSignature) {
            $percent = $this->getParam('percent');
            if (in_array($percent, array(15, 30))) {

                if (!$product->getEAVAttributeValue('price_lowered_by_suggestion')) {
                    $product->setPrice($product->getPercentOfPrice(100 - $percent))
                        ->setEAVAttributeValue('price_lowered_by_suggestion', true)->save();
                    $this->getSession()->setFlashData(
                        'modal_message',
                        array('', 'Price for your item has been changed.', '', 5000)
                    );
                } else {
                    $this->getSession()->setFlashData(
                        'modal_message',
                        array('', 'Price has been already changed.', '', 5000)
                    );
                }
                $this->redirect('/');
            }
        }

        $this->show404();
    }

    protected function _setProductForm(\Zend_Form $form)
    {
        $this->_productForm = $form;
        return $this;
    }

    /**
     * Get product form
     *
     * @param array $data (Optional) - populate form with data if passed
     * @return Ikantam_Form
     */
    protected function _getProductForm(array $data = null)
    {
        if ($data) {
            if ($this->getParam('publish')) {
                $this->_productForm->isValid($data);
            } else {
                $this->_productForm->populate($data);
            }
        }
        return $this->_productForm;
    }

    /**
     * Create and validate requested product
     *
     * @return Product_Model_Product
     */
    protected function _getProduct()
    {
        if (!$this->_product) {
            $productId = (int) $this->getParam('id');
            $product   = new Product_Model_Product($productId);
            if (!$product->isBelongsToUser($this->getSession()->getUser())) {
                $this->_helper->show404();
            }
            $this->_product = $product;
        }

        return $this->_product;
    }

    /**
     * Retrieves and filter product data
     *
     * @return array
     */
    protected function _getFormData()
    {
        $product                = $this->_getProduct();
        $data                   = $product->getData();
        $data['price']          = (float) $product->getPrice();
        $data['original_price'] = (float) $product->getOriginalPrice();
        $data['width']          = (float) $product->getWidth();
        $data['height']         = (float) $product->getHeight();
        $data['depth']          = (float) $product->getDepth();

        $data['manufacturer']         = $this->_getRelatedObject('Manufacturer')->getTitle();
        $data['subcategory_id']       = $this->_getRelatedObject('SubCategory')->getId();
        $data['category_id']          = $this->_getRelatedObject('RootCategory')->getId();
        $data['is_antiques']          = (int) $this->_getRelatedObject('AntiqueSet')->hasProduct($product);
        $data['is_kids_corner']       = (int) $this->_getRelatedObject('KidsCornerSet')->hasProduct($product);
        $data['has_scratches']        = $product->getEAVAttributeValue('has_scratches');
        $data['cat_in_home']          = (int) $product->getEAVAttributeValue('exposed_cat');
        $data['dog_in_home']          = (int) $product->getEAVAttributeValue('exposed_dog');
        $data['is_pick_up_available'] = $product->getIsPickUpAvailable(0);
        $data['is_smoke']             = (int)!$product->getIsSmokeFree(0);

        $address                         = $this->_getRelatedObject('PickUpAddress');
        $addressData                     = $address->getData();
        $addressData['telephone_number'] = $address->getPhoneNumber($address->getTelephoneNumber());
        $addressData['postal_code']      = $address->getPostcode($address->getPostalCode());

        $addressData['selling_type'] = $product->getSellingType();

        $data['pickup_address'] = $addressData;

        $data['pickup_address']['absorb_shipping'] = (bool) $product->getShippingCover();
        $data['pickup_address']['amount']    = (int) $product->getShippingCover();
        $data['pickup_address']['is_pick_up_available'] = (int) $product->getIsPickUpAvailable();
        $data['pickup_address']['address_id'] = $address->getId();

        if ($product->getSellingType() === \Product_Model_Product::SELLING_TYPE_SET) {
            $data['qty'] = $product->getNumberOfItemsInSet(1);
        }

        if ($this->getParam('publish')) {
            $data['publish'] = true;
        }

        if ($data['available_from']) {
            $data['available_from'] = date('M d, Y', $data['available_from']);
        }

        if ($data['available_till']) {
            $data['available_till'] = date('M d, Y', $data['available_till']);
        }

        return $data;
    }

    /**
     * Pass to view necessary options
     *
     * @return void
     */
    protected function _setAdditionalViewData()
    {
        $addresses                   = new User_Model_Address_Collection();
        $options                     = $addresses->getJsonConfigByUserId($this->getSession()->getUserId());
        $this->view->pickupAddresses = Zend_Json::encode($options);

        $categoryCollection                         = new Category_Model_Category_Collection();
        $this->view->catsConfig                     = Zend_Json::encode($categoryCollection->getJsonConfig2());
        $this->view->session                        = $this->getSession();
        $this->view->additionalImageUploaderOptions = Zend_Json::encode(array(
                    'edit_mode'  => 1,
                    'product_id' => $this->_getProduct()->getId()
        ));

        $this->view->messages       = array();
        $this->view->imageEndpoints = Product_Model_Image::getEndpoints();
    }

    /**
     * Create manufacturer object for current product
     *
     * @return Application_Model_Manufacturer
     */
    protected function _getManufacturer()
    {
        return new Application_Model_Manufacturer($this->_getProduct()->getManufacturerId());
    }

    /**
     * Create category object for current product
     *
     * @return Category_Model_Category
     */
    protected function _getSubCategory()
    {
        return $this->_getProduct()->getCategory();
    }

    /**
     * Retrieves parent category for current product
     *
     * @return  Category_Model_Category
     */
    protected function _getRootCategory()
    {
        return $this->_getRelatedObject('SubCategory')->getParent();
    }

    /**
     * Creates antique set model
     *
     * @return Application_Model_Set
     */
    protected function _getAntiqueSet()
    {
        $set = new Application_Model_Set();
        $set->getBy_name('Vintage + Antiques');

        return $set;
    }

    /**
     * Creates kid's corner set model
     *
     * @return Application_Model_Set
     */
    protected function _getKidsCornerSet()
    {
        $set = new Application_Model_Set();
        $set->getBy_name('Kid\'s Corner');

        return $set;
    }

    /**
     * Retrieves related pickup address model
     *
     * @return User_Model_Address
     */
    protected function _getPickUpAddress()
    {
        return $this->_getProduct()->getPickUpAddress();
    }

    /**
     * Cache for related objects
     *
     * @param  $name - method name without _get prefix: _getRootCategory = RootCategory
     * @return mixed object
     */
    protected function _getRelatedObject($name)
    {
        if (isset($this->_relatedObjects[$name])) {
            return $this->_relatedObjects[$name];
        } elseif (!method_exists($this, ($method = '_get' . $name))) {
            throw new Exception('Unable to get related object "' . $name . '". Because appropriate method not exist.');
        }

        $this->_relatedObjects[$name] = $this->{$method}();

        return $this->_relatedObjects[$name];
    }

    /**
     * Save new address
     *
     * @return \User_Model_Address
     */
    protected function _saveAddress()
    {
        $pickupAddressForm = new Product_Form_AddressSubform;
        $pickupAddressData = $this->getRequest()->getPost('pickup_address');
        $address           = new User_Model_Address();

        if (!$pickupAddressForm->isValid($pickupAddressData)) {
            return $address;
        }

        $addressData                 = $pickupAddressForm->getValues(true);
        $addressData                 = $addressData['pickup_address'];
        $addressData['postcode']     = $addressData['postal_code'];
        $addressData['phone_number'] = $addressData['telephone_number'];


        $address->addData($addressData)
            ->setUserId($this->getSession()->getUserId())
            ->setIsPrimary(0);

        if ($this->getSession()->isLoggedIn()) {
            $address->save();
        } else {
            $this->getSession()->activeData('_guest_shipping_address', serialize($address));
        }

        return $address;
    }

}