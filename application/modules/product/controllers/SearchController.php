<?php

class Product_SearchController extends Ikantam_Controller_Front
{
    protected $_filters = array();
    protected $_value = null;  //filtred search value from input
    protected $_page = 1;
    protected $_products =  null;  //collection

	public function init()
	{        
        $this->_filters[] = new Zend_Filter_StringTrim();
        $this->_filters[] = new Zend_Filter_StringToLower();
        
        $this->_value = $this->getRequest()->getParam('searchField');
        $this->filter($this->_value);
        $page = $this->getRequest()->getParam('page');        
        $this->_page = ($page) ? $page  : $this->_page;
        
        $this->_products = new Product_Model_Product_Collection();
        

	}
    
    protected function filter(&$value)
    {
        foreach($this->_filters as $filter)
            {
                if($filter instanceof Zend_Filter_Interface)
                    {
                     $value = $filter->filter($value);   
                    } 
            }
    }


    
    public function findAction () //search autocomplete (AJAX)
    {
        $result = array();
        
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $this->_products->searchProducts($this->_value, $this->_page, 5);        
        
            foreach($this->_products->getItems() as $product)
                {
                    $result[] = array(
                                      'title' => $product->getTitle(),
                                      'url'   => $product->getProductUrl(),
                                      'image' => Ikantam_Url::getPublicUrl($product->getMainImage()->getPath()),
                                      );
                }
            echo Zend_Json::encode($result);
    }

}



