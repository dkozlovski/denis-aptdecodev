<?php

class Product_AddNewController extends Ikantam_Controller_Front
{

    const ERROR_MESSAGE_DATA_REQUIRED       = 'Opps, looks like some of the fields have 
                    errors or were left blank. Please update all fields highlighted
                    in red and publish again';
    const ERROR_MESSAGE_IMAGE_REQUIRED      = 'Image required';
    const SUCCESS_MESSAGE_PRODUCT_PUBLISHED = 'Your changes have been saved.';

    private $_productForm;

    public function init()
    {
        $addresses = new User_Model_Address_Collection();
        $addresses->getByUserId($this->getSession()->getUserId());

        $this->_setProductForm(new Product_Form_Product2(array('addresses' => $addresses)));
    }

    public function successAction()
    {
        $this->view->session = $this->getSession();
    }

    public function indexAction()
    {
        $this->_forward('new');
    }

    public function newAction()
    {


        $form                 = new Product_Form_AddNewProduct_MainForm();
        $product              = $this->_getProduct();
        $categoryCollection   = new Category_Model_Category_Collection();
        $this->view->messages = array();


        $this->view->form           = $form;
        $this->view->catsConfig     = Zend_Json::encode($categoryCollection->getJsonConfig());
        $this->view->session        = $this->getSession();
        $this->view->product        = $product;
        $this->view->productImages  = $product->getExtraAllImages();
        $this->view->hasLockedImage = 0;
        $this->view->imageEndpoints = Product_Model_Image::getEndpoints();


        $addresses = new User_Model_Address_Collection();
        $options   = $addresses->getJsonConfigByUserId($this->getSession()->getUserId());

        $this->view->pickupAddresses = Zend_Json::encode($options);
    }

    public function saveAddress()
    {
        $form              = new Product_Form_Product2();
        $pickupAddressForm = $form->getSubform('pickup_address');
        $pickupAddressData = $this->getRequest()->getPost('pickup_address');
        $address           = new User_Model_Address();

        if (!$pickupAddressForm->isValid($pickupAddressData)) {
            return $address;
        }

        $addressData                 = $pickupAddressForm->getValues();
        $addressData                 = $addressData['pickup_address'];
        $addressData['postcode']     = $addressData['postal_code'];
        $addressData['phone_number'] = $addressData['telephone_number'];
        $addressForm                 = new User_Form_Address();

        if ($addressForm->isValid($addressData)) {

            $address->addData($addressForm->getValues())
                    ->setUserId($this->getSession()->getUserId())
                    ->setIsPrimary(0);

            if ($this->getSession()->isLoggedIn()) {
                $address->save();
            } else {
                $this->getSession()->activeData('_guest_shipping_address', serialize($address));
            }
        }

        return $address;
    }

    protected function _setProductForm(\Zend_Form $form)
    {
        $this->_productForm = $form;
        return $this;
    }

    /**
     * 
     * @return Ikantam_Form
     */
    protected function _getProductForm()
    {
        return $this->_productForm;
    }

    /**
     * Get product from session
     * 
     * @return \Product_Model_Product
     */
    protected function _getProduct()
    {
        if ($this->getSession()->getProductId()) {
            $productId = $this->getSession()->getProductId();
            $product   = new Product_Model_Product($productId);

            if ($product->getId()) {
                return $product;
            }
        }

        $product = new Product_Model_Product();
        $product->setIsPublished(0)
                ->setIsVisible(0)
                ->setUserId($this->getSession()->getUserId())
                ->setCreatedAt(time())
                ->save();

        $this->getSession()->setProductId($product->getId());

        return $product;
    }

    public function uploadImageAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $response = array();

        $productId = $this->getRequest()->getParam('product_id');
        $product   = new Product_Model_Product($productId);

        if (!$product->getId() || $product->getId() != $this->getSession()->getProductId()) {
            //@TODO:return error message
            die();
        }
        //////
        $basePath = Product_Model_Image::getBaseTmpDir();
        $path     = Product_Model_Image::getTmpUploadDir($product);


        $uploader = new Ikantam_File_Uploader();

        $uploader->setAdapter(new Zend_File_Transfer_Adapter_Http)
                ->setUploadPath($basePath . DIRECTORY_SEPARATOR . $path);

        if (!$uploader->upload()) {
            $errors = $uploader->getErrorMessages();

            $response['success'] = false;
            $response['error']   = $response['message'] = implode("\n", $errors);
            $this->getResponse()
                    ->setHeader('Content-type', 'text/plain')
                    ->setBody(json_encode($response));
        } else {
            $fileName = $uploader->getFileName();

            $iConverter = new Ikantam_File_ImageConverter();

            $newFileName = $basePath . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . 'original-0.jpg';
            $iConverter->convert($fileName, $newFileName, 90);

            $res = $iConverter->createScaledImages($newFileName, $basePath . DIRECTORY_SEPARATOR . $path);

            if ($res) {
                $fileNames   = $iConverter->getFileName();
                $fileNames[] = $newFileName;
            } else {
                //@TODO: return error;
            }

            foreach ($fileNames as $file) {
                $e = Product_Model_Image::getTmpS3Path($file);
                $uploader->uploadToS3($file, $e);
                unlink($file);
            }

            unlink($fileName);

            $productImage = new Product_Model_Image();

            $productImage->setProductId($product->getId())
                    ->setPath('original-0.jpg')
                    ->setS3Path($path)
                    ->setIsVisible(0)
                    ->setIsMain(0)
                    ->setIsLocked(0)
                    ->setIsUploaded(0)
                    ->save();


            $response['success']     = true;
            $response['file']['id']  = $productImage->getId();
            //$response['file']['name'] = $path . '/original-0.jpg';
            $response['file']['url'] = $productImage->getS3Url(500, 500, 'crop');
        }



        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(json_encode($response));
    }

    public function rotateImageAction()
    {
        //sleep(2);

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot rotate this image',
            'redirect' => false
        );

        $productId = $this->getRequest()->getParam('product_id');
        $imageId   = $this->getRequest()->getParam('image_id');
        $angle     = (int) ($this->getRequest()->getParam('angle') % 360);
        $product   = new Product_Model_Product($productId);

        /* if ($angle == 0) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/11368/1387148527ad03ee0b10af477a6918bd2735229acf/360-300-frame-0.jpg';
          }
          if ($angle == 90) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/12783/1389904967c793875ba1c326b544d0fefa3d1e5315/360-300-frame-0.jpg';
          }
          if ($angle == 180) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/1-10000/8967/13815191586f1b800b9c52989e796a9fb22d936fa5/360-300-frame-0.jpg';
          }
          if ($angle == 270) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/12753/138989328144e8c146a01131b527561d7406a7aac8/360-300-frame-0.jpg';
          }

          $response['success'] = true;
          $response['error']   = false;

          $this->_helper->json($response); */

        if (!in_array($angle, array(0, 90, 180, 270), true)) {
            $this->_helper->json($response);
        }

        if (!$product->getId() || $product->getId() !== $this->getSession()->getProductId()) {
            $this->_helper->json($response);
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $image->setDefaultAngle($angle)->save();

        if ($angle == 0 || ($angle == 90 && $image->get90Exists()) || ($angle == 180 && $image->get180Exists()) || ($angle == 270 && $image->get270Exists())) {
            $response['success'] = true;
            $response['error']   = false;
            $response['url']     = $image->getS3Url(500, 500, 'crop');

            $this->_helper->json($response);
        }

        if ($angle == 90 && !$image->get90Exists()) {
            $image->set90Exists(1)->save();
        }

        if ($angle == 180 && !$image->get180Exists()) {
            $image->set180Exists(1)->save();
        }

        if ($angle == 270 && $image->get270Exists()) {
            $image->set270Exists(1)->save();
        }

        $uploadDir    = APPLICATION_PATH . '/../tmp/product-images';
        $oldUploadDir = APPLICATION_PATH . '/../product-images';
        $imagePath    = $image->getS3Path();

        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;

        $oi = $cloudFrontUrl . '/product-images/' . $imagePath;

        $parts = explode('/', $imagePath);

        $tPath = $uploadDir;

        foreach ($parts as $part) {
            $tPath = $tPath . DIRECTORY_SEPARATOR . $part;
            if (!file_exists($tPath)) {
                mkdir($tPath, 0777);
            }
        }

        $file_path = $uploadDir . DIRECTORY_SEPARATOR . $imagePath;
        //КОСТЫЛЬ!
        if (!file_exists($file_path)) {
            $file_path = $oldUploadDir . DIRECTORY_SEPARATOR . $imagePath;
        }

        $img = new Ikantam_Image();
        $img->rotateOriginalImage($oi, $file_path, $angle);

        $img->createScaledImages($uploadDir, $imagePath, $productId, $angle);

        $response['success'] = true;
        $response['error']   = false;
        $response['url']     = $image->getS3Url(500, 500, 'crop');

        $this->_helper->json($response);
    }

    public function removeImageAction()
    {
        //sleep(2);

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot delete this image',
            'redirect' => false
        );

        /* $response['success'] = true;
          $response['error']   = false;
          $response['message'] = null;

          $this->_helper->json($response); */

        $productId = $this->getRequest()->getParam('product_id');
        $imageId   = $this->getRequest()->getParam('image_id');
        $product   = new Product_Model_Product($productId);

        if (!$product->getId() || $product->getId() !== $this->getSession()->getProductId()) {
            $this->_helper->json($response);
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $image->delete();

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

    public function setDefaultImageAction()
    {
        //sleep(2);

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot set this image as default',
            'redirect' => false
        );

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);

        $productId = $this->getRequest()->getParam('product_id');
        $imageId   = $this->getRequest()->getParam('image_id');
        $product   = new Product_Model_Product($productId);

        if (!$product->getId() || $product->getId() !== $this->getSession()->getProductId()) {
            $this->_helper->json($response);
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $image->setDefaultImage($product->getId(), $image->getId());

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

    /**
     * Add product to antiques
     * 
     * @param Product_Model_Product $product  
     * @return self
     */
    protected function _addToAntiqueSet(\Product_Model_Product $product)
    {
        $set = new Application_Model_Set;
        $set->getBy_name('Vintage + Antiques');
        if ($set->isExists()) {
            $set->addProducts($product);
        }

        return $this;
    }

    /**
     * Add product to kid's corner
     * 
     * @param Product_Model_Product $product  
     * @return self
     */
    protected function _addToKidsCornerSet(\Product_Model_Product $product)
    {
        $set = new Application_Model_Set;
        $set->getBy_name('Kid\'s Corner');
        if ($set->isExists()) {
            $set->addProducts($product);
        }

        return $this;
    }

    /**
     * Validate form via ajax request.
     * 
     */
    public function isValidAction()
    {
        if (!$this->isAjax()) {
            throw new Zend_Controller_Action_Exception('Page not found.', 404);
        }

        $data = $this->getRequest()->getPost();

        $this->_helper->json(array('valid' => $this->_getProductForm()->isValid($data)));
    }

    public function getRevenueAction()
    {
        $floatFormat = new Zend_Filter_LocalizedToNormalized(array('locale' => 'en_US', 'precision' => 2));

        $price = $floatFormat->filter($this->getRequest()->getPost('price'));

        if ($price <= 0) {
            $this->_helper->json->sendJson(array(
                'success' => false
            ));
        }

        $options = new Application_Model_Options();

        $comission = $options->getIndividualFeeOrDefault($this->getSession()->getUserId(), Application_Model_Options::SELLER, $price);

        $revenue = $price - $price * $comission / 100;

        $this->_helper->json->sendJson(array(
            'success' => true, 'revenue' => (string) $this->view->currency($revenue)
        ));
    }

}