<?php
class Product_AlertController extends Ikantam_Controller_Front
{
    public function init()
    {
        $this->view->session = $this->getSession();
    }

    protected function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery.autocomplete.js'));
    }

    public function indexAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }
        $collection = new Product_Model_Alert_Collection();
        $collection->getByUserId((int)$this->getSession()->getUserId());
        $this->view->alerts = $collection;
        $this->view->activeMenu   = 'alerts';
        $this->view->headTitle('Alerts');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }


    public function popupAction()
    {
        $response = array(
            'success' => false,
            'html'    => '',
            'message' => ''
        );
        $error = false;

        $id = (int)$this->getParam('id');
        $alert = new Product_Model_Alert();
        if ($id) {
            $alert->getById($id);
            if ($this->getSession()->getUserId() != $alert->getUserId()) {
                $error = 'Please check your login details';
            }
        } else {
            $alert->setDataFromRequest($this->getRequest());
        }

        if (!$error) {
            $this->view->alert = $alert;
            $this->_prepareOptions();
            $response['success'] = true;
            $response['html']    = $this->view->render('alert/popup.phtml');
        } else {
            $response['success'] = false;
            $response['message'] = $error;
        }

        $this->responseJSON($response)
            ->sendResponse();
        exit;

    }

    public function newAction()
    {
        $alert = new Product_Model_Alert();
        $alert->setDataFromRequest($this->getRequest());
        $this->view->alert = $alert;

        $this->_helper->viewRenderer('edit');
        $this->_prepareOptions();
    }

    public function editAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }
        $id = (int)$this->getParam('id');
        $alert = new Product_Model_Alert();
        if ($id) {
            $alert->getById($id);
            if ($this->getSession()->getUserId() != $alert->getUserId()) {
                $this->getSession()->addMessage('error', 'Please check your login details');
                $this->redirect('product/alert/index');
            }
        }

        $this->view->alert = $alert;
        $this->_prepareOptions();
    }

    public function saveAction()
    {
        $id = (int)$this->getParam('id');
        $reload = $this->getParam('id') || $this->getParam('reload');
        try {
            $alert = new Product_Model_Alert();
            if ($id) {
                $alert->getById($id);
                if ($this->getSession()->getUserId() != $alert->getUserId()) {
                    $this->responseJSON(array('success' => false, 'message' => 'Error. Please check your login details'))
                        ->sendResponse();
                    exit;
                }
            }

            $form = new Product_Form_ProductAlert();

            if ($form->isValid($this->getRequest()->getPost())
                && ($form->getValue('category_id') || $form->getValue('collection_id'))
            ) {

                $alert->setUserId($this->getSession()->getUserId());
                $alert->setDataFromForm($form);
                $alert->save();

                $this->responseJSON(array(
                    'success' => true,
                    'message' => 'Your alert was successfully saved!',
                    'reload'  => $reload

                ))
                    ->sendResponse();
                exit;

            } else {
                $messages = array();
                foreach ($form->getMessages() as  $_m) {
                    foreach ($_m as $_name => $__m) {
                        foreach ($__m as  $_message) {
                            if ($_name == 'emails') { // for subform
                                foreach ($_message as  $__message) {
                                    $messages[] = $__message;
                                }
                            } else {
                                $messages[] = $_message;
                            }
                        }
                    }
                }

                if (!($form->getValue('category_id') || $form->getValue('collection_id'))) {
                    $messages[] = 'Please select a category';
                }

                $this->responseJSON(array('success' => false, 'message' => implode('<br />', $messages)))
                    ->sendResponse();
                exit;
            }
        } catch (Exception $e) {
            $this->responseJSON(array('success' => false, 'message' => 'Error. Please try again later'))
                ->sendResponse();
            exit;
        }

    }

    public function deleteByCodeAction()
    {
        $alert = new Product_Model_Alert();
        $code = $this->getParam('code');

        $alert->getBy_delete_code($code);
        if ($alert->getId()) {
            $alert->setIsDelete(true)
                ->save();
            $this->getSession()->addMessage('success', 'Alert has been deleted. <a href="' . $alert->getRestoreByCodeUrl() . '" >Restore</a>');
        } else {
            $this->getSession()->addMessage('error', 'Your link is wrong');
        }

        if ($this->getSession()->getUserId()) {
            $this->redirect('product/alert/index');
        } else {
            $this->redirect('product/alert/delete-success');
        }
    }

    public function  deleteSuccessAction()
    {
        $this->_helper->viewRenderer('default-message');
    }

    public function restoreByCodeAction()
    {
        $alert = new Product_Model_Alert();
        $code = $this->getParam('code');

        $alert->getBy_delete_code($code);
        if ($alert->getId()) {
            $alert->setIsDelete(false)
                ->save();

            $this->getSession()->addMessage('success', 'Alert has been restored');
        } else {
            $this->getSession()->addMessage('error', 'Your link is wrong');
        }

        if ($this->getSession()->getUserId()) {
            $this->redirect('product/alert/index');
        } else {
            $this->redirect('product/alert/restore-success');
        }
    }

    public function  restoreSuccessAction()
    {
        $this->_helper->viewRenderer('default-message');
    }

    protected function _prepareOptions()
    {
        $categoryCollection = new Category_Model_Category_Collection();
        $colorCollection    = new Application_Model_Color_Collection();

        $this->view->categories   = $categoryCollection->getOptions3();
        $this->view->catsConfig   = Zend_Json::encode($categoryCollection->getJsonConfig());
        $this->view->colorsIds    = $colorCollection->getAsOptions();
        $this->view->show_sidebar = $this->getParam('show_sidebar');
        $this->view->reload       = $this->getParam('reload');
        $this->view->activeMenu   = 'profile';
    }
}