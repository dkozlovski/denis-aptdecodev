<?php

class Product_ImageController extends Ikantam_Controller_Front
{

    /**
     * Get product from session
     * 
     * @return \Product_Model_Product
     */
    protected function _getProduct()
    {
        if($this->getParam('edit_mode', false)){
            $product = new Product_Model_Product($this->getParam('product_id'));
            if($product->isBelongsToUser($this->getSession()->getUser())){
                return $product;
            }
        }
        unset($product);
        
        if ($this->getSession()->getProductId()) {
            $productId = $this->getSession()->getProductId();
            $product   = new Product_Model_Product($productId);

            if ($product->getId()) {
                return $product;
            }
        }

        $product = new Product_Model_Product();
        $product->setIsPublished(0)
                ->setIsVisible(0)
                ->setUserId($this->getSession()->getUserId())
                ->setCreatedAt(time())
                ->save();

        $this->getSession()->setProductId($product->getId());

        return $product;
    }

    public function rotateAction()
    {
        //sleep(2);

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot rotate this image',
            'redirect' => false
        );

        $product = $this->_getProduct();
        $imageId = $this->getRequest()->getParam('image_id');
        $angle   = (int) ($this->getRequest()->getParam('angle') % 360);

        /* if ($angle == 0) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/11368/1387148527ad03ee0b10af477a6918bd2735229acf/360-300-frame-0.jpg';
          }
          if ($angle == 90) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/12783/1389904967c793875ba1c326b544d0fefa3d1e5315/360-300-frame-0.jpg';
          }
          if ($angle == 180) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/1-10000/8967/13815191586f1b800b9c52989e796a9fb22d936fa5/360-300-frame-0.jpg';
          }
          if ($angle == 270) {
          $response['url'] = 'https://d6qwfb5pdou4u.cloudfront.net/product-images/10001-20000/12753/138989328144e8c146a01131b527561d7406a7aac8/360-300-frame-0.jpg';
          }

          $response['success'] = true;
          $response['error']   = false;

          $this->_helper->json($response); */

        if (!in_array($angle, array(0, 90, 180, 270), true)) {
            $this->_helper->json($response);
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }
        
        if ($image->getIsLocked()) {
            $response['message'] = 'Default photo is locked. You can\'t edit it.';
            $this->_helper->json($response);
        }

        $image->setDefaultAngle($angle)->save();

        if ($angle == 0 || ($angle == 90 && $image->get90Exists()) || ($angle == 180 && $image->get180Exists()) || ($angle == 270 && $image->get270Exists())) {
            $response['success'] = true;
            $response['error']   = false;
            $response['url']     = $image->getS3Url(500, 500, 'crop');

            $this->_helper->json($response);
        }

        if ($angle == 90 && !$image->get90Exists()) {
            $image->set90Exists(1)->save();
        }

        if ($angle == 180 && !$image->get180Exists()) {
            $image->set180Exists(1)->save();
        }

        if ($angle == 270 && $image->get270Exists()) {
            $image->set270Exists(1)->save();
        }

        $uploadDir    = APPLICATION_PATH . '/../tmp/product-images';
        $oldUploadDir = APPLICATION_PATH . '/../product-images';
        $imagePath    = $image->getS3Path();

        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;

        $oi = $cloudFrontUrl . '/product-images/' . $imagePath;

        $parts = explode('/', $imagePath);

        $tPath = $uploadDir;

        foreach ($parts as $part) {
            $tPath = $tPath . DIRECTORY_SEPARATOR . $part;
            if (!file_exists($tPath)) {
                mkdir($tPath, 0777);
            }
        }

        $file_path = $uploadDir . DIRECTORY_SEPARATOR . $imagePath;
        //КОСТЫЛЬ!
        if (!file_exists($file_path)) {
            $file_path = $oldUploadDir . DIRECTORY_SEPARATOR . $imagePath;
        }

        $img = new Ikantam_Image();
        $img->rotateOriginalImage($oi, $file_path, $angle);

        $img->createScaledImages($uploadDir, $imagePath, $product->getId(), $angle);

        $response['success'] = true;
        $response['error']   = false;
        $response['url']     = $image->getS3Url(500, 500, 'crop');

        $this->_helper->json($response);
    }

    public function deleteAction()
    {
        //sleep(2);

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot delete this image',
            'redirect' => false
        );

        /* $response['success'] = true;
          $response['error']   = false;
          $response['message'] = null;
          $this->_helper->json($response); */

        $product = $this->_getProduct();
        $imageId = $this->getRequest()->getParam('image_id');
        $image   = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }
        
        if ($image->getIsLocked()) {
            $response['message'] = 'Default photo is locked. You can\'t edit it.';
            $this->_helper->json($response);
        }

        $image->delete();

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

    public function setAsDefaultAction()
    {
        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot set this image as default',
            'redirect' => false
        );

        /* $response['success'] = true;
          $response['error']   = false;
          $response['message'] = null;
          $this->_helper->json($response); */

        $product = $this->_getProduct();
        $imageId = $this->getRequest()->getParam('image_id');
        $image   = new Product_Model_Image($imageId);
        
        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }
        
        if ($product->hasLockedImage() && !$image->getIsLocked()) {
            $response['message'] = 'Default photo is locked. You can\'t edit it.';
            $this->_helper->json($response);
        }

        $image->setDefaultImage($product->getId(), $image->getId());

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

}
