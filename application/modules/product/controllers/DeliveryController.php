<?php
/**
 * Author: Alex P.
 * Date: 05.08.14
 * Time: 13:39
 */

use \Core\Service\DeliveryDateHelper;

class Product_DeliveryController extends Ikantam_Controller_Front
{
    /**
     * @Inject("core.service.deliveryDateHelper")
     * @var \Core\Service\DeliveryDateHelper
     */
    protected $dateHelper;

    /**
     * Calculate days that fall within delivery days
     * request parameters: from, till, sizing(small|default)
     * responds with JSON: {"count" : X}
     */
    public function daysFallWithinDeliveryAction()
    {
        $this->dateHelper->setFrom($this->getParam('from'))
            ->setTill($this->getParam('till'));

        if ($this->getParam('sizing') == 'small') {
            $this->dateHelper->setIncludeDefault(false);
        } else {
            $this->dateHelper->setIncludeSmall(false);
        }

        try {
            $count = $this->dateHelper->setReturnCount(true)->dateIntersect();
        } catch (\InvalidArgumentException $exception) {
            $count = 0;
        }

        $this->_helper->json(array(
                'count' => $count
            ));
    }
} 