<?php

class Product_ViewController extends Ikantam_Controller_Front
{

    //protected $_js = array('js/iKantam/modules/newsletter.subscribe.popup.js');

    public function init()
    {
        if ($this->getParam('share', false) && !$this->getSession()->isLoggedIn()) {
            $product = new Product_Model_Product((int) $this->getRequest()->getParam('id'));
            $this->_helper->redirectAfter($product->getProductUrl() . '?share=1');
            $this->redirect('user/login/index');
        }
    }

    public function indexAction()
    {
        $productId = (int) $this->getRequest()->getParam('id');

        $product = new Product_Model_Product();
        $product->getById($productId);

        if($categoryId=$product->getCategoryId()){
            $category = new Category_Model_Category($categoryId);
            $pageUrl=($category->getPageUrl())?($category->getPageUrl()):('index');
        }else{
            $pageUrl='index';
        }

        //product does not exist or is not saved
        if (!$product->getId() || !$product->getIsVisible()) {
            //throw new Zend_Controller_Action_Exception('This product does not exist', 404);
            $this->_helper->redirector->setCode(301);
            $this->_helper->redirector('index', $pageUrl, 'catalog');
        }

        $userId = $this->getSession()->getUserId();

        //show unpublished or unapproved product only to its owner or user has purchased this product
        if (!($product->getIsPublished() && $product->getIsApproved()) && $userId != $product->getUserId() && !$product->hasUserPurchasedProduct($userId) ) {
            //throw new Zend_Controller_Action_Exception('This product does not exist', 404);
            $this->_helper->redirector->setCode(301);
            $this->_helper->redirector('index', $pageUrl, 'catalog');
        }

        //save product view event
        if ($userId && $userId != $product->getUserId()) {
            new User_Model_ProductView($productId, $userId);
        }

        $product->selfTriggerEvent('catalog.view'); // see Application_Model_Events_Listeners_Product

        $products = new Product_Model_Product_Collection();
        $products->getByUser($product->getSeller()->getId());

        $this->view->product     = $product;
        $this->view->products    = $products;
        $this->view->cart        = $this->getSession()->getCart();
        $this->view->currentUser = $this->getSession()->getUser();
        $this->view->isShowSharePopup = $this->getParam('share');

        $this->view->headTitle(sprintf('Used %s for sale in NYC',$product->getTitle()));
        $this->view->headMeta()->appendName('description', 'Buy used furniture and arrange for delivery through AptDeco.');
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/input.spin.js'));
        $this->view->session = $this->getSession();
        /*
          //Subscribe popup
          $fStripNewLines = new Zend_Filter_StripNewlines();
          $this->view->popupTemplate = $fStripNewLines->filter($this->view->partial('popup-template.phtml', 'newsletter', array('email' => $this->getSession()->getUser()->getPrimaryEmail()->getEmail())));
          $this->view->successTemplate = str_replace("'", "\'", $fStripNewLines->filter($this->view->partial('popup-success-template.phtml', 'newsletter')));
         */
    }

    // ajax
    public function estimateDeliveryAction()
    {
        $response = array(
            'success' => false,
            'message' => ''
        );

        $product = new Product_Model_Product((int)$this->getParam('product_id'));

        $postcode = $this->getParam('postcode');
        $validator = new Zend_Validate_PostCode('en_US');

        if ($validator->isValid($postcode)) {
            $this->getSession()->setEstimatedPostcode($postcode);
            $total = $product->estimateDelivery(array('postcode' => $postcode));
            $response['success'] = true;
            $response['message'] = '<div class="aptd-alert success">Estimated delivery is <strong>' . $this->view->currency($total) . '</strong></div>';
            $response['formatted_price'] = (string)$this->view->currency($total);
            $response['postcode'] = $postcode;
        } else {
            $response['success'] = false;
            $response['message'] = '<p class="invalid">Wrong zip code. Please enter only numerical values.</p>';
        }

        $this->responseJSON($response)
            ->sendResponse();
        exit;
    }

}