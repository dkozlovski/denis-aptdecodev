<?php
class Product_Model_Popularity extends Application_Model_Abstract
{
    const TYPE_VIEW = 'view'; // how many times page was loaded
    const TYPE_CART = 'cart'; // how many times has been added to cart
    const TYPE_WISHLIST = 'wishlist'; // how many times has been added to wishlist
    
    protected $_types = array('view', 'cart', 'wishlist');
        
    /**
    * Increase popularity property.
    * @param  string $type 
    * @return object Product_Model_Popularity
    */ 
    public function increase ()
    {
        $this->setCount((int)$this->getCount(0) + 1)
             ->save();
        return $this;      
    }
    
    /**
    * Retrieves popularity for product from db or returns empty object, if no match found.
    * @param  int $id - product id
    * @param  string $type - one of available types (see self::TYPE_...)
    * @return object self
    */
    public function getIfExist ($product_id, $type)
    {  
        $this->setType($type)
            ->_getbackend()->getIfExist($this, $product_id, $type);
            
        return $this;
    }
    
    //protect from direct modifications
    //use increase method instead
    protected function setCount($value)
    {
        parent::setCount($value);
        return $this;
    }
    
    protected function setType($value) 
    {
        if(!in_array($value, $this->_types)) {
            throw new Exception('Wrong type given "'.$value.'" Available types is: '.implode(', ', $this->_types));
        }        
        parent::setType($value);
        return $this;    
    }    
}