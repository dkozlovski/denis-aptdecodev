<?php

class Product_Model_Home_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'products_home';


    
    public function addProduct (\Product_Model_Product $object)
    {
        
        if($this->isExists())
        {
            $this->_update($object);
        } else
            {
                $this->_insert($object);
            }
    }
    

    
    public function delete (\Product_Model_Product $object)
    {
        $sql = "DELETE FROM `".$this->_getTable()."` WHERE `product_id` = :pid LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $pid =  $object->getId();
        $stmt->bindParam(':pid',$pid);
        $stmt->execute();
    }
    
    public function removeBackground ()
    {
        $sql = "UPDATE `".$this->_getTable()."` SET `image_path` = NULL";
        $this->_getConnection()->prepare($sql)->execute();
    }
  
  	  /**
       * @param string $imageName
       * @param Product_Model_HomeBackground $object - optional   
       */  
    public function changeBackground ($imageName, $object = null)
    {
        $update = "UPDATE `".$this->_getTable()."` SET `image_path` = :img";
        $insert = "INSERT INTO `".$this->_getTable()."` (`product_id`, `image_path`) VALUES (NULL, :img)";
        $sql = ($this->isExists()) ? $update  : $insert;
        $stmt = $this->_getConnection()->prepare($sql);
        
        
        
        $stmt->bindParam(':img', $imageName);
        
        $execute_result = $stmt->execute();
                
         if($object instanceof Product_Model_HomeBackground)
         {
              $object->setInsertedImage($imageName);
         } 
         
     }
     
     public function getBackgroundName ()
     {
        $sql = "SELECT `image_path` FROM `".$this->_getTable()."` WHERE 1 LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_COLUMN);       
     
     }
     
     protected function isExists()
     {
        $sql = "SELECT `id` FROM `".$this->_getTable()."` WHERE 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        
        return (bool)$stmt->rowCount();        
     }  


    	/**
         * !IMPORTANT!
         * @param  Product_Model_Product $object
         */
	protected function _update(\Application_Model_Abstract $object)
	{
	    $dbh = $this->_getConnection();
        $sql = "UPDATE `".$this->_getTable()."` SET `product_id` = :pid";
        
        $stmt = $dbh->prepare($sql);
        $pid = $object->getId();
        $stmt->bindParam(':pid', $pid);    

        $stmt->execute();    
   
	}

    	/**
         * !IMPORTANT!
         * @param  Product_Model_Product $object
         */
	protected function _insert(\Application_Model_Abstract $object)
	{

	    $dbh = $this->_getConnection();
        $sql = "INSERT INTO `".$this->_getTable()."` (`id`, `product_id`) VALUES (NULL, :pid)";
        
        $stmt = $dbh->prepare($sql);
        $pid = $object->getId();
        $stmt->bindParam(':pid', $pid);    

        $stmt->execute();
        
        $object->setId($dbh->lastInsertId());       
        
	}
    



}
