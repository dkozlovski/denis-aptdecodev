<?php

/**
 * Class Product_Model_Product_Collection
 * @method \Product_Model_Product_Backend _getBackend()
 */
class Product_Model_Product_Collection extends Application_Model_Abstract_Collection
    implements Ikantam_Filter_Flex_Interface, Ikantam_Filter_Flex_Interface_EAV
{

    protected $_backendClass       = 'Product_Model_Product_Backend';
    protected $_itemObjectClass    = 'Product_Model_Product';
    protected $_loaded             = false;
    protected $_paginator;
    private $_lastSearchPagesCount = null;

    const SORT_BY_NAME                 = 'title';
    const SORT_BY_PRICE                = 'price';
    const SORT_BY_DATE                 = 'created_at';
    const SORT_ASC                     = 'ASC';
    const SORT_DESC                    = 'DESC';
    const FILTER_BY_PRICE              = 'price';
    const FILTER_BY_CONDITION          = 'condition';
    const FILTER_BY_BRAND              = 'manufacturers';
    const FILTER_BY_COLOR              = 'color';
    //---sales and purchase, admin manage
    const FILTER_TYPE_ID               = 'id';
    const FILTER_TYPE_STATUS           = 'status';
    const FILTER_TYPE_ORDER            = 'order'; // Filter by order id
    const FILTER_TYPE_TITLE            = 'title';
    const FILTER_TYPE_USER             = 'user';
    const FILTER_TYPE_DATE             = 'date';
    const FILTER_TYPE_CATEGORY         = 'category';
    const FILTER_TYPE_BRAND            = 'manufacturers';
    const FILTER_TYPE_SHIPPING         = 'shipping';
    const FILTER_PUBLISHED             = 'published'; //status
    const FILTER_NOT_PUBLISHED         = 'notPublished'; //status
    const FILTER_SOLD                  = 'sold'; //status
    const FILTER_NOT_SOLD              = 'notSold'; //status
    const FILTER_RECEIVED              = 'received'; //status
    const FILTER_IN_DELIVERY           = 'inDelivery'; //status
    const FILTER_NEED_TO_APPROVE       = 'needToApprove'; //status
    const FILTER_WEEK                  = 'thisWeek'; //date
    const FILTER_MONTH                 = 'thisMonth'; //date
    const FILTER_YEAR                  = 'thisYear'; //date
    const FILTER_TYPE_BETWEEN_DATES    = 'betweenDates'; //date
    const GROUP_BY_PRODUCT             = 'groupByProduct';
    //shipping
    const FILTER_DELIVERY              = 'delivery';
    const FILTER_PICKUP                = 'pickUp';
    const FILTER_BOTH_SHIPPING_METHODS = 'both';
    //admin statuses
    const FILTER_REJECTED              = 'rejected';
    const FILTER_APPROVED              = 'approved';
    const FILTER_NOT_PROCESSED         = 'notProcessed';

    protected function _setIsLoaded($flag = true)
    {
        $this->_loaded = $flag;
    }

    public function setPagesCount($count)
    {
        $this->_lastSearchPagesCount = $count;
    }

    public function getPagesCount()
    {
        return $this->_lastSearchPagesCount;
    }

    /**
     * @param  integer $id = category id
     * @return Product_Model_Product_Collection 
     */
    public function getByCategory($id, $onlyPublished = false)
    {
        if (!is_int($id)) {
            throw new Exception('Invalid Id. Id must be an integer value.');
        }

        $this->_getBackEnd()->getByCategory($this, $id, $onlyPublished);
        return $this;
    }

    protected function removeItem($key)
    {
        unset($this->_items[$key]);
        return $this;
    }

    public function getAll()
    {
        $this->clear();
        $this->_getBackEnd()->getAll($this);
        $this->_setIsLoaded();
        return $this;
    }

    /**
     * @param  const   string $sortBy
     * @param  const   string $sortDirection 
     * @param  array   $categoryId
     * @param  integer $page
     * @param  integer $itemsPerPage
     * @param  array   $filters 
     * @return Product_Model_Product_Collection
     */
    public function loadSortedBy($sortBy = self::SORT_BY_DATE, $direction = self::SORT_DESC, $categoryId = null, $page = null, $itemsPerPage = 10, $filters = null, $onlyPublished = true)
    {
        $this->clear();
        $this->_getBackEnd()->sortBy($this, $sortBy, $direction, $categoryId, $page, $itemsPerPage, $filters, $onlyPublished);
        return $this;
    }

    /**
     * 
     * @param  const string $filterType
     * @param  array $params          
     */
    public function filterBy($filterType, $params = array())
    {
        switch ($filterType) {
            case self::FILTER_BY_PRICE:
                if (!empty($params)) {
                    $min = (float) $params['min'];
                    $max = (float) $params['max'];
                    if ($min == 0 && $max == 0) {
                        $this->clear();
                        return $this;
                    }

                    if ($max > 0) {
                        foreach ($this->getItems() as $key => $item) {
                            $price = $item->getPrice();
                            if ($price < $min || $price > $max) {
                                $this->removeItem($key);
                            }
                        }
                    } else {
                        foreach ($this->getItems() as $key => $item) {
                            $price = $item->getPrice();
                            if ($price < $min) {
                                $this->removeItem($key);
                            }
                        }
                    }
                }
                return $this;
                break;

            case self::FILTER_BY_CONDITION:
                $condition = null;
                //-------------- many params     
                if (is_array($params['condition'])) {
                    foreach ($this->getItems() as $key => $item) {
                        $remove        = true;
                        $currCondition = $item->getCondition();
                        foreach ($params['condition'] as $condition) {
                            if ($condition == $currCondition)
                                $remove = false;
                        }
                        if ($remove)
                            $this->removeItem($key);
                    }

                    return $this;
                }
                //-----------------
                if (is_string($params['condition']))
                    $condition = $params['condition'];
                if (is_string($params))
                    $condition = $params;
                if (!$condition)
                    throw new Exception(get_class($this) . ' can\'t filter with empty params.');
                foreach ($this->getItems() as $key => $item) {
                    if ($item->getCondition() != $condition) {
                        $this->removeItem($key);
                    }
                }

                return $this;
                break;

            case self::FILTER_BY_BRAND:

                if (is_array($params)) {
                    if (isset($params['title'])) {
                        if (is_array($params['title'])) {
                            $title           = null;
                            $manufacturersId = array();
                            foreach ($this->getItems() as $key => $item) {
                                $remove = true;
                                foreach ($params['title'] as $paramTitle) {
                                    $manufacturer = new Application_Model_Manufacturer();
                                    $manufacturer->getByTitle($paramTitle);

                                    if ($item->getManufacturerId() == $manufacturer->getId())
                                        $remove = false;
                                }
                                if ($remove)
                                    $this->removeItem($key);
                            }
                            return $this;
                        }// array title
                        if (is_string($params['title'])) {
                            $manufacturer = new Application_Model_Manufacturer();
                            $manufacturer->getByTitle($params['title']);
                            $id           = $manufacturer->getId();
                            foreach ($this->getItems() as $key => $item) {
                                if ($item->getManufacturerId() != $id)
                                    $this->removeItem($key);
                            }
                            return $this;
                        }
                    }

                    if (isset($params['id'])) {

                        if (is_array($params['id'])) {
                            foreach ($this->getItems() as $key => $item) {
                                $remove = true;
                                foreach ($params['id'] as $id) {
                                    if ($item->getManufacturerId() == $id)
                                        $remove = false;
                                }
                                if ($remove)
                                    $this->removeItem($key);
                            }
                            return $this;
                        }

                        if (is_string($params['id']) || is_int($params['id'])) {
                            foreach ($this->getItems() as $key => $item) {
                                if ($item->getManufacturerId() != (int) $params['id'])
                                    $this->removeItem($key);
                            }
                            return $this;
                        }
                    }
                }

                throw new Exception(get_class($this) . ' can\'t filter by brand with invalid parameters.');
                break;

            case self::FILTER_BY_COLOR:

                if (is_array($params)) {
                    if (isset($params['id'])) {
                        if (is_array($params['id'])) {

                            foreach ($this->getItems() as $key => $item) {
                                $remove    = true;
                                $currColor = $item->getColorId();
                                foreach ($params['id'] as $id) {
                                    if ($currColor == $id)
                                        $remove = false;
                                }
                                if ($remove)
                                    $this->removeItem($key);
                            }
                            return $this;
                        }

                        foreach ($this->getItems() as $key => $item) {
                            if ($item->getColorId() != $params['id'])
                                $this->removeItem($key);
                        }
                        return $this;
                    }


                    if (isset($params['title'])) {
                        if (is_array($params['title'])) {
                            foreach ($this->getItems() as $key => $item) {
                                $remove = true;
                                foreach ($params['title'] as $title) {
                                    $color  = new Application_Model_Color();
                                    $id     = $color->getByTitle($title)->getId();
                                    if ($item->getColorId() == $id)
                                        $remove = false;
                                }

                                if ($remove)
                                    $this->removeItem($key);
                            }
                            return $this;
                        }

                        $color = new Application_Model_Color();
                        $id    = $color->getByTitle($params['title'])->getId();
                        foreach ($this->getItems() as $key => $item) {
                            if ($item->getColorId() != $id)
                                $this->removeItem($key);
                        }
                        return $this;
                    }
                }


                throw new Exception(get_class($this) . ' can\'t filter by color with invalid parameters.');
                break;
        }//switch

        return $this;
    }

// filterBy

    /**
     * @param  string  $searchValue
     * @param  integer $currentPage 
     * @param  integer $itemsPerPage
     * @param  bool    $onlyPublished
     * @param  integer $minLengh (minimal alowed length of search value string)
     * @return Product_Model_Product_Collection
     */
    public function searchProducts($searchValue, $page = null, $itemsPerPage = 10, $onlyPublished = true, $minLength = 3)
    {
        $this->_getBackend()->searchProducts($this, $searchValue, $page, $itemsPerPage, $onlyPublished, $minLength);
        return $this;
    }

    /**
     * 
     * @return Product_Model_Product_Collection
     */
    public function getProductsInCart(\Cart_Model_Cart $cart)//$cart
    {
        foreach ($cart->getItemCollection() as $cartItem) {
            $this->addItem(new Product_Model_Product($cartItem->getProductId()));
        }
        return $this;
    }

    /**
     * Retrieves array from collection items
     * @return array
     */
    public function toArray()
    {
        $result = array();

        foreach ($this->getItems() as $item) {
            $result[] = $item->getData();
        }
        return $result;
    }

    /**
     * Return all categories id from items
     * @return array
     */
    public function getCategoriesId()
    {
        $result = array();
        foreach ($this->getItems() as $item) {
            $id = $item->getCategoryId();
            if (!in_array($id, $result)) {
                $result[] = $id;
            }
        }
        sort($result);
        return $result;
    }

    public function test()
    {
        $this->_getBackend()->sortByPage($this, self::SORT_BY_PRICE, self::SORT_ASC, array(2, 10), 2, 1, false);
        return $this;
    }

    public function searchProductPagesHeader($query, $offset, $limit, $filters, $order = array())
    {
        $this->_getBackend()->searchProductPagesHeader($this, $query, $offset, $limit, $filters, $order);
        return $this;
    }

    public function searchAndFilter($data, $offset, $limit)
    {
        $this->_getBackend()->searchAndFilter($this, $data, $offset, $limit);
        return $this;
    }

    public function getByIds($ids)
    {
        $this->_getBackend()->getByIds($this, $ids);
        return $this;
    }

    public function filterAndFilter($data, $offset, $limit)
    {
        $this->_getBackend()->filterAndFilter($this, $data, $offset, $limit);
        return $this;
    }

    public function getMinPriceByQuery($query)
    {
        return $this->_getBackend()->getMinPriceByQuery($query);
    }

    public function getMaxPriceByQuery($query)
    {
        return $this->_getBackend()->getMaxPriceByQuery($query);
    }

    public function getMinPriceByCategory($query)
    {
        return $this->_getBackend()->getMinPriceByCategory($query);
    }

    public function getMaxPriceByCategory($query)
    {
        return $this->_getBackend()->getMaxPriceByCategory($query);
    }

    public function getMinPriceByManufacturer($query)
    {
        return $this->_getBackend()->getMinPriceByManufacturer($query);
    }

    public function getMaxPriceByManufacturer($query)
    {
        return $this->_getBackend()->getMaxPriceByManufacturer($query);
    }

    public function countSearchAndFilterResults($query)
    {
        return $this->_getBackend()->countSearchAndFilterResults($query);
    }

    public function getByUser($userId)
    {
        $this->_getBackend()->getByUser($this, $userId);
        return $this;
    }

    public function getAllByUser($userId)
    {
        $this->_getBackend()->getAllByUser($this, $userId);
        return $this;
    }

    public function getByUserAndCategory($userId, $categoryId)
    {
        $this->_getBackend()->getByUserAndCategory($this, $userId, $categoryId);
        return $this;
    }

    /**
     * Retrieves products for sale.
     * @param integer $userId
     * @param array $options - filter and sort options 
     * @param integer $itemsPerPage
     * @param integer $currentPageNumber
     * @param string  $searchValue 
     * @return Product_Model_Product_Collection
     * @example 
     * $options = array(
     * 'date'=>Product_Model_Product_Collection::FILTER_MONTH,
     * 'sort'=>array('by'=>'title','direction'=>'ASC'),
     * 'category' => 7,
     * );
     * $obj->getProductsForSale($id,$options...);
     * @example
     * $options = array(
     * 'betweenDates'=>array($timestamp1, $timestamp2),
     * );
     */
    public function getProductsForSale($userId, $options = null, $currentPageNumber = 1, $itemsPerPage = 10, $searchValue = null)
    {
        $orderList = $this->getOrderList($userId, null, 1, -1)->getItemsWithConvertedKey();
        $this->clear();
        $this->_getBackend()->getForSales($this, $userId, $options, $this->_paginator, $currentPageNumber, $itemsPerPage, $searchValue);
        $forSale   = $this->getItemsWithConvertedKey();

        foreach ($forSale as $key => $itemForSale) {
            if (key_exists($key, $orderList)) {
                $forSale[$key] = $orderList[$key];
            }
        }

        $this->_items = array_values($forSale);

        return $this;
    }

    public function getPurchaseHistory($userId, $options = null, $currentPageNumber = 1, $itemsPerPage = 10, $searchValue = null)
    {
        $this->_getBackend()->getPurchase($this, $userId, $options, $this->_paginator, $currentPageNumber, $itemsPerPage, $searchValue);
        return $this;
    }

    public function getOrderList($userId, $options = null, $currentPageNumber = 1, $itemsPerPage = 10, $searchValue = null)
    {
        $this->_getBackend()->getOrders($this, $userId, $options, $this->_paginator, $currentPageNumber, $itemsPerPage, $searchValue);
        return $this;
    }

    public function getForAdmin($options = null, $currentPageNumber = 1, $itemsPerPage = 10, $searchValue = null)
    {
        $this->_getBackend()->getForAdmin($this, $options, $this->_paginator, $currentPageNumber, $itemsPerPage, $searchValue);
        return $this;
    }

    public function getForAdminList($options = null, $offset = null, $limit = null)
    {
        $this->_getBackend()->getForAdminList($this, $options, $offset, $limit);
        return $this;
    }

    public function getCountForAdminList($options = null)
    {
        return $this->_getBackend()->getCountForAdminList($options);
    }

    /**
     * @return Zend_Paginator 
     */
    public function getPaginator()
    {
        if ($this->_paginator instanceof Zend_Paginator) {
            return $this->_paginator;
        }
        return false;
    }

    /**
     * Replace key in array by id
     * @return array 
     */
    public function getItemsWithConvertedKey()
    {
        $result = array();
        foreach ($this->getItems() as $item) {
            $result[$item->getId()] = $item;
        }


        return $result;
    }

    /**
     * Retrieves products with publication period which expires after 1 and 3 days 
     * @return 
     */
    public function getAboutExpireTomorrow($limit = 10)
    {
        $this->_getBackend()->getAboutExpireTomorrow($this, $limit);
        return $this;
    }

    public function getAboutExpireThreeDays($limit = 10)
    {
        $this->_getBackend()->getAboutExpireThreeDays($this, $limit);
        return $this;
    }

    public function getExpired()
    {
        $this->_getBackend()->getExpired($this);
        return $this;
    }

    public function unpublishExpired()
    {
        $this->_getBackend()->unpublishExpired();
        return $this;
    }

    public function markAsReminded($marker = 1)
    {
        $this->_getBackend()->markAsReminded($this, $marker);
        return $this;
    }

    /** Retreives seller products with defined order status
     * @param  int $userId
     * @param  string $status | pending, complete, holded, closed, canceled
     * @return self
     */
    public function getByUserAndOrderStatus($userId, $status)
    {
        $this->_getBackend()->getProductsByUserAndStatus($this, $userId, $status);
        return $this;
    }

    public function getLatestNotSoldNotInCart($limit = 5, array $exclude = array())
    {
        $this->_getbackend()->getLatestNotSoldNotInCart($this, $limit, $exclude);
        return $this;
    }

    /** Products for suggestion box in cart
     * 1. Wishlist products(available for sale)
     * 2. Viewed products
     * 3. Latest products
     * @param int $limit 
     * @return self
     */
    public function getSuggestionProducts($limit = 5)
    {
        //if guest than return latest product
        if (!User_Model_Session::instance()->isLoggedIn()) {
            return $this->getLatestNotSoldNotInCart($limit);
        }

        $currentUser    = User_Model_Session::user();
        $productsRemain = $limit;
        $wishListItems  = $currentUser->getWishList()->getAllItems();
        $sgProducts     = new self();

        /* PRODUCTS FROM WISHLIST */
        foreach ($wishListItems as $wlItem) {
            $wlProduct = $wlItem->getProduct();
            /* If at least one of filds 0 then ignore this product */
            if (array_product($wlProduct->toArray(array('qty', 'is_available_for_purchase', 'is_published', 'is_visible', 'is_delivery_available')))) {
                $sgProducts->addItem($wlProduct);
                $productsRemain--;
            }

            if (!$productsRemain) {
                return $sgProducts;
            }
        }
        //***************************


        /* VIEWED PRODUCTS */
        $productViewHistory = new User_Model_ProductView_Collection();
        $productViewHistory->getUserViewHistoryWithLiveProducts($productsRemain);

        foreach ($productViewHistory as $productView) {
            //with delivery only   
            if ($productView->getIsDeliveryAvailable()) {
                $sgProducts->addItem($productView->getProduct());
            }
        }
        //****************************

        /* ADDED PRODUCTS */
        $sgProducts->removeDuplicateId();
        $productsRemain = $limit - $sgProducts->getSize();
        if ($productsRemain > 0) {
            $sgProducts = $sgProducts->merge($this->getLatestNotSoldNotInCart($productsRemain, $sgProducts->getColumn('id')));
        }

        $this->_items = $sgProducts->getItems();
        return $this;
    }

    public function getFlexFilter()
    {
        return new Product_Model_Product_FlexFilter($this);
    }

    /**
     * Get products which not queued in task manager
     * @param  int $limit
     * @return object self
     */
    public function getForGoogleTask($limit = 100)
    {
        $this->_getBackend()->getForGoogleTask($this, $limit);
        return $this;
    }

    public function getPublishedSoldProduct()
    {
        $this->_getBackend()->getPublishedSoldProduct($this);
        return $this;
    }

    /**
     * Configures FlexFilter with necessary parameters which defines products whose price should be lowered.
     * @return \Product_Model_Product_FlexFilter
     */
    public function getConfiguredFilterForManagePricing($params = array())
    {
        $flexFilter = $this->getFlexFilter();
        $flexFilter->qty('>', 0)
            ->is_approved('=', 1)
            ->is_published('=', 1)
            ->is_manage_price_allowed('=', 1)
            ->admin_edited_price('IS_NULL')
            ->admin_edited_price('!=', 1)
            // publication period will end in a week or less
            ->available_till('between', time(), strtotime('+1 week'))
            ->order('available_till', 'asc');

        if (isset($params['category'])) {
            $flexFilter->category_id('=', $params['category']);
        }

        if (isset($params['date'])) {
            switch ($params['date']) {
                case 'week':
                    $begin = strtotime('last Monday');
                    $flexFilter->created_at('between', $begin, $begin + 604800);
                    break;
                case 'month':
                    $begin = mktime(0, 0, 0, date('n'), 1, date('Y'));
                    $end   = mktime(23, 59, 59, date('n'), date('t'), date('Y'));
                    $flexFilter->created_at('between', $begin, $end);
                    break;
            }
        }

        if (isset($params['product'])) {
            $flexFilter->id('=', $params['product']);
        }

        if (isset($params['search'])) {
            $flexFilter->title('like', '%' . $params['search'] . '%');
        }

        return $flexFilter;
    }

    /**
     * Set filter in state which allows to retrieve products that sellers should be suggested
     * for 15% price reduction
     * @return \Product_Model_Product_FlexFilter
     */
    public function getConfiguredFilterForPriceReduction15PercentSuggestion()
    {
        return $this->getConfiguredFilterForPriceReductionSuggestion()
                        ->group('id')
                        // Publication period of a product must be at least 8 days
                        ->simpleExpressionCondition('available_till - coalesce(available_from, 0)', '>', 86400 * 8)
                        ->simpleExpressionCondition(
                                "(popularity_type = 'cart' AND popularity_count > 1)
                    OR (popularity_type = 'view' AND popularity_count > 10)", null, null
        );
    }

    /**
     * Set filter in state which allows to retrieve products that sellers should be suggested
     * for 30% price reduction
     * @return \Product_Model_Product_FlexFilter
     */
    public function getConfiguredFilterForPriceReduction30PercentSuggestion()
    {
        return $this->getConfiguredFilterForPriceReductionSuggestion()
                        ->group('id')
                        // Publication period of a product must be at least 8 days
                        ->simpleExpressionCondition('available_till - coalesce(available_from, 0)', '>', 86400 * 8)
                        ->simpleExpressionCondition(
                                "(popularity_type = 'cart' AND popularity_count IS NULL)
                    OR (popularity_type = 'view' AND popularity_count < 10)", null, null
        );
    }

    /**
     * Configures FlexFilter to retrieve products
     * @return \Product_Model_Product_FlexFilter
     */
    protected function getConfiguredFilterForPriceReductionSuggestion()
    {
        $attrIsNotified = new \Application_Model_EAV_Attribute(
                'smallint', 'notification_manage_pricing_to_seller_sent'
        );

        return $this->getFlexFilter()->qty('>', 0)
                        ->is_approved('=', 1)
                        ->is_published('=', 1)
                        // ->is_manage_price_allowed('IS_NULL')
                        // ->is_manage_price_allowed('=', 0)
                        ->available_till('between', time(), strtotime('+1 week'))
                        ->byEAVAttribute($attrIsNotified, 'IS_NULL')
                        ->byEAVAttribute($attrIsNotified, '=', 0)
                        ->alwaysJoin('popularity');
    }

    /**
     * Counts products whose price should be lowered
     * @return int
     */
    public static function countManagePricingProducts()
    {
        $inst = new self;
        return $inst->getConfiguredFilterForManagePricing()->count();
    }

    /**
     * Returns filter. Some sugar.
     * @return Product_Model_Product_FlexFilter
     */
    public static function flexFilter()
    {
        $inst = new self;
        return $inst->getFlexFilter();
    }

    /**
     * Get name of the attributes table
     * @return string
     */
    public function getAttributesTable()
    {
        return 'attributes';
    }

    /**
     * Get map for tables where values are stored
     * @return array
     * structure:
     * 'INT' => 'eav_table_int',
     * 'TEXT' => 'eav_table_text'
     * 'X-TYPE' => 'x-table_name'...
     */
    public function getTypeTableMap()
    {
        return array(
            'int'      => 'eav_products_int',
            'varchar'  => 'eav_products_varchar',
            'text'     => 'eav_products_text',
            'smallint' => 'eav_products_smallint',
            'decimal'  => 'eav_products_decimal'
        );
    }

    /**
     * Get entity table name
     * @return array
     */
    public function getEntityTable()
    {
        return 'products';
    }

    public function getFeatured()
    {
        $this->_getBackend()->getFeatured($this);
        return $this;
    }

    public function saveFeatured($ids)
    {
        $this->_getBackend()->saveFeatured($ids);
        return $this;
    }
    
    public function getCartSuggestions($existingItems)
    {
        return $this->_getBackend()->getCartSuggestions($existingItems);
    }

    /**
     * Unpublish all products in collection
     * @return bool
     */
    public function unpublishCollection()
    {
        if ($this->getSize()) {
            return $this->_getBackend()->unpublishProducts($this->getColumn('id'));
        }
        return false;
    }
    
    public function productRatingUP($productID, $rating)
    {
        $this->_getBackend()->productRatingUP($productID, $rating);
    }

}