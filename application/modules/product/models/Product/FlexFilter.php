<?php

/**
 * @method Product_Model_Product_FlexFilter manufacturer_name (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_manufacturer_name (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter id_of_set (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_id_of_set (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter slug_of_set (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_slug_of_set (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter color_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_color_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter user_rating (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_user_rating (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter google_task_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_google_task_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter popularity_type (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_popularity_type (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter popularity_count (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_popularity_count (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter order_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_order_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter order_item_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_order_item_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter user_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_user_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter title (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_title (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter description (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_description (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter price (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_price (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter original_price (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_original_price (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter old_price (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_old_price (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter category_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_category_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter manufacturer_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_manufacturer_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter condition (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_condition (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter material_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_material_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter width (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_width (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter height (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_height (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter depth (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_depth (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter age (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_age (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter available_from (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_available_from (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter available_till (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_available_till (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_published (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_published (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter created_at (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_created_at (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter updated_at (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_updated_at (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_pick_up_available (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_pick_up_available (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_delivery_available (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_delivery_available (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_available_for_purchase (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_available_for_purchase (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_visible (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_visible (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pick_up_address_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pick_up_address_id (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_approved (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_approved (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pickup_full_name (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pickup_full_name (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pickup_address_line1 (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pickup_address_line1 (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pickup_address_line2 (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pickup_address_line2 (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pickup_city (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pickup_city (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pickup_state (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pickup_state (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pickup_postcode (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pickup_postcode (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pickup_country_code (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pickup_country_code (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pickup_phone (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pickup_phone (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter page_url (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_page_url (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter expire_at (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_expire_at (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_reminded_about_expire (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_reminded_about_expire (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter qty (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_qty (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter selling_type (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_selling_type (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter number_of_items_in_set (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_number_of_items_in_set (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter shipping_cover (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_shipping_cover (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter pet_in_home (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_pet_in_home (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_smoke_free (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_smoke_free (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_verified (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_verified (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_under_15_pounds (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_under_15_pounds (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter is_manage_price_allowed (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_is_manage_price_allowed (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter admin_edited_price (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_admin_edited_price (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter approved_at (string $operator, mixed $value)
 * @method Product_Model_Product_FlexFilter or_approved_at (string $operator, mixed $value)
 * @method Product_Model_Product_Collection apply(int $limit = null, int $offset = null)
 */

class Product_Model_Product_FlexFilter extends Ikantam_Filter_Flex
{

    protected $_acceptClass = 'Product_Model_Product_Collection';
    protected $_joins       = array(
        'manufacturer'         => 'INNER JOIN `manufacturers` AS `manufacturer` ON `products`.`manufacturer_id` = `manufacturer`.`id`',
        'category'             => 'INNER JOIN `categories` AS `category` ON `category`.`id` = `products`.`category_id`',
        'material'             => 'INNER JOIN `materials` AS `material`  ON `material`.`id` = `products`.`material_id`',
        'color'                => 'INNER JOIN `colors` AS `color` ON `color`.`id` = `products`.`color_id`',
        'collections_products' => 'INNER JOIN `collections_products`ON `collections_products`.`product_id` = `products`.`id`',
        'collections'          => array('collections_products' => 'INNER JOIN `collections` ON `collections`.`id` = `collections_products`.`collection_id`'),
        'user'                 => 'INNER JOIN `users` AS `user` ON `products`.`user_id` = `user`.`id`',
        'google_task'          => 'LEFT JOIN `google_shopping_content_tasks` AS `google_task` ON `google_task`.`product_id` = `products`.`id`',
        'popularity'           => 'LEFT JOIN `product_popularity` ON `products`.`id` = `product_popularity`.`product_id`',
        'order_item'           => 'INNER JOIN `order_items` AS `oi` ON `oi`.`product_id` = `products`.`id`',
        'order'                => array('order_item' => 'INNER JOIN `orders` ON `orders`.`id` = `oi`.`order_id`')

    );
    protected $_select      = array(
        'order' => '`orders`.`id` AS `order_id`, `oi`.`id` AS `order_item_id`',
        'order_item' => '`oi`.`id` as `order_item_id`',
    );
    protected $_rules       = array(
        'manufacturer_name' => array('manufacturer' => '`manufacturer`.`title`'),
        'id_of_set'         => array('collections' => '`collections`.`id`'),
        'slug_of_set'       => array('collections' => '`collections`.`page_url`'),
        'color_id'          => array('color' => '`color`.`id`'),
        'user_rating'       => array('user' => '`user`.`rating`'),
        'google_task_id'    => array('google_task' => '`google_task`.`id`'),
        'popularity_type'   => array('popularity' => '`product_popularity`.`type`'),
        'popularity_count'  => array('popularity' => '`product_popularity`.`count`'),
        'order_id'          => array('order' => '`orders`.`id`'),
        'order_item_id'     => array('order_item' => '`oi`.`id`')
    );

}