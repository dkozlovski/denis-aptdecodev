<?php

class Product_Model_Product_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'products';

    protected function _delete(\Application_Model_Abstract $object)
    {
        $id = $object->getId();

        if (!$id) {
            return;
        }

        //do not actually delete products
        $sql  = "UPDATE `" . $this->_getTable() . "` SET `is_visible` = 0 where `id` = :id;";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

    public function getByCategory($object, $id, $onlyPublished)
    {
        if (!$onlyPublished) {
            $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `category_id` = :id';
        } else {
            $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `category_id` = :id AND `is_published` = 1';
        }

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $product = new Product_Model_Product();
            $product->addData($row);

            $object->addItem($product);
        }
    }

    public function getAll($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

        $stmt = $this->_getConnection()->prepare($sql);
        //  $stmt->execute();
        // $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->fillCollection($stmt, $collection);
        /*  foreach ($result as $row) {
          $object = new Product_Model_Product();
          $object->addData($row);

          $collection->addItem($object);
          } */
    }

    public function sortBy($collection, $sortBy, $direction, $categoryId, $page, $itemsPerPage, $filters, $onlyPublished)
    {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $userBE = new Application_Model_User_Backend();
        $select = new Zend_Db_Select($db);
        $select->from($this->_getTable());

        if (!is_array($categoryId) && (int) $categoryId > 0) {
            if ($onlyPublished) {
                $select->where('category_id = ?', $categoryId)
                        ->where('is_published = 1')
                        ->order($sortBy . ' ' . $direction);
            } else {
                $select->where('category_id = ?', $categoryId)
                        ->order($sortBy . ' ' . $direction);
            }
        } else {
            if ($onlyPublished) {
                $select->where('category_id IN (?)', $categoryId)
                        ->where('is_published = 1')
                        ->order($sortBy . ' ' . $direction);
            } else {
                $select->where('category_id IN (?)', $categoryId)
                        ->order($sortBy . ' ' . $direction);
            }
        }

//----------------------------|FILTERS
        if (is_array($filters)) {
            foreach ($filters as $filter) {
                $filter->attach($select);
            }
        }

//****************************|FILTERS

        $paginator = Zend_Paginator::factory($select);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($itemsPerPage);

        $collection->setPagesCount($paginator->count());

        foreach ($paginator as $item) {
            $product = new Product_Model_Product();
            $product->addData($item);
            $collection->addItem($product);
        }
    }

    public function searchProducts(Product_Model_Product_Collection $collection, $searchValue, $page, $itemsPerPage, $onlyPublished, $minLength)
    {
        $strlen = new Zend_Validate_StringLength(array('min' => $minLength));
        if ($strlen->isValid($searchValue)) {
            $db     = Zend_Db_Table::getDefaultAdapter();
            $userBE = new Application_Model_User_Backend();
            $select = new Zend_Db_Select($db);
            $select->from(
                            array('p' => $this->_getTable()))
                    ->join(array('u' => $userBE->_getTable()), 'p.user_id = u.id', array('full_name as user_name'));

            if ($onlyPublished) {
                $select->where('is_published = ?', '1');
            }
            $select->where('title LIKE ?', "{$searchValue}%")
                    ->orWhere('u.full_name LIKE ?', "{$searchValue}%")
                    ->orWhere('description LIKE ?', "%{$searchValue}%");


            $paginator = Zend_Paginator::factory($select);

            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($itemsPerPage);

            $collection->setPagesCount($paginator->count());

            foreach ($paginator as $item) {
                $product = new Product_Model_Product();
                $product->addData($item);
                $collection->addItem($product);
            }
        }
    }

    /**
     * Attach filter and sort conditions.
     */
    private function updateSelect($select, $options)
    {
        //-----options

        $select->order('created_at DESC');

        if (is_array($options)) {
            $vDigits = new Zend_Validate_Digits();
            //----status
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_STATUS])) {
                switch ($options[Product_Model_Product_Collection::FILTER_TYPE_STATUS]) {

                    case Product_Model_Product_Collection::FILTER_PUBLISHED:
                        $select->where('p.is_published = 1')
                                ->where('p.is_approved = 1')
                                ->where('p.qty > 0');
                        break;

                    case Product_Model_Product_Collection::FILTER_NOT_PUBLISHED:
                        $select->where('p.is_published = 0');
                        break;

                    case Product_Model_Product_Collection::FILTER_SOLD:
                        $select->where('p.qty = 0');
                        break;

                    case Product_Model_Product_Collection::FILTER_NOT_SOLD:
                        $select->where('p.qty > 0');
                        break;

                    case Product_Model_Product_Collection::FILTER_IN_DELIVERY:
                        $select->where('oi.is_received = 0');
                        if (!key_exists('oi', $select->getPart('from'))) {
                            $select->join(array('oi' => 'order_items'), 'p.id = oi.product_id', array('is_received'));
                        }

                        break;

                    case Product_Model_Product_Collection::FILTER_RECEIVED:
                        $select->where('oi.is_received = 1');
                        if (!key_exists('oi', $select->getPart('from'))) {
                            $select->join(array('oi' => 'order_items'), 'p.id = oi.product_id', array('is_received'));
                        }

                        break;
                    //ADMIN STATUSES
                    case Product_Model_Product_Collection::FILTER_REJECTED:
                        $select->where('p.is_approved = -1');
                        break;

                    case Product_Model_Product_Collection::FILTER_APPROVED:
                        $select->where('p.is_approved = 1');
                        $select->where('oi.is_received IS NULL');
                        break;

                    case Product_Model_Product_Collection::FILTER_NEED_TO_APPROVE:
                        $select->where('p.is_approved = 0')
                                ->where('p.is_published = 1');
                        break;

                    case Product_Model_Product_Collection::FILTER_NOT_PROCESSED:
                        $select->where('p.is_approved = 0');
                        break;
                }
            }
            //****status
            //shipping
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_SHIPPING])) {
                switch ($options[Product_Model_Product_Collection::FILTER_TYPE_SHIPPING]) {

                    case Product_Model_Product_Collection::FILTER_PICKUP:
                        $select->where('p.is_pick_up_available = 1');
                        break;

                    case Product_Model_Product_Collection::FILTER_DELIVERY:
                        $select->where('is_delivery_available = 1');
                        break;

                    case Product_Model_Product_Collection::FILTER_BOTH_SHIPPING_METHODS:
                        $select->where('p.is_pick_up_available = 1')
                                ->where('is_delivery_available = 1');
                        break;
                }
            }//<<<shipping

            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_DATE])) {
                $table = isset($options['dateByProduct']) ? 'p' : 'o'; //column `created_at` in WHERE clause p-products o-orders

                switch ($options[Product_Model_Product_Collection::FILTER_TYPE_DATE]) {
                    case Product_Model_Product_Collection::FILTER_WEEK:
                        //Timestamps for begin and end of current month
                        $begin = strtotime('last Monday');
                        $end   = $begin + 604800; // 604800 - 7 days
                        $select->where($table . '.created_at BETWEEN ' . $begin . ' AND ' . $end);
                        break;

                    case Product_Model_Product_Collection::FILTER_MONTH:
                        //Timestamps for begin and end of current month
                        $begin = mktime(0, 0, 0, date('n'), 1, date('Y'));
                        $end   = mktime(23, 59, 59, date('n'), date('t'), date('Y'));
                        $select->where($table . '.created_at BETWEEN ' . $begin . ' AND ' . $end);
                        break;

                    case Product_Model_Product_Collection::FILTER_YEAR:
                        //Timestamps for begin and end of current year
                        $begin = mktime(0, 0, 0, 1, 1, date('Y'));
                        //$day = (date('L'))?366:365;
                        $end   = mktime(23, 59, 59, 1, date('z') + 1, date('Y'));
                        $select->where($table . '.created_at BETWEEN ' . $begin . ' AND ' . $end);
                        break;
                }
            }
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_BETWEEN_DATES])) {
                $table      = isset($options['dateByProduct']) ? 'p' : 'o'; //column `created_at` in WHERE clause p-products o-orders
                $timestamps = $options[Product_Model_Product_Collection::FILTER_TYPE_BETWEEN_DATES];

                $begin = (isset($timestamps[0])) ? (int) $timestamps[0] : false;
                $end   = (isset($timestamps[1])) ? (int) $timestamps[1] + 86399 : false; //86399 of 86400 seconds in day

                if ($begin !== false) {
                    $select->where($table . '.created_at >= ?', $begin);
                }

                if ($end !== false) {
                    $select->where($table . '.created_at <= ?', $end);
                }
            }
            //------Category
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_CATEGORY])) {
                $category = &$options[Product_Model_Product_Collection::FILTER_TYPE_CATEGORY];
                if (is_string($category) || is_int($category)) {
                    $select->where('p.category_id = ?', (int) $category);
                }

                if (is_array($category)) {
                    $select->where('p.category_id IN (?)', $category);
                }
            }

            if (isset($options['sort']) && is_array($options['sort'])) {
                $select->order($options['sort']['by'] . ' ' . $options['sort']['direction']);
            }

            //-------BRAND
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]) && is_string($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND])) {
                $select->join(array('m' => 'manufacturers'), 'm.id = p.manufacturer_id', array())
                        ->where('m.title = ?', $options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]);
            } elseif (isset($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]) && is_array($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND])) {
                if ($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]['isId']) {
                    $select->join(array('m' => 'manufacturers'), 'm.id = p.manufacturer_id', array())
                            ->where('m.id = (?)', $options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]['id']);
                } else {
                    $select->join(array('m' => 'manufacturers'), 'm.id = p.manufacturer_id', array())
                            ->where('m.title IN (?)', $options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]);
                }
            }

            //----Id
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_ID]) && is_string($options[Product_Model_Product_Collection::FILTER_TYPE_ID])) {
                $select->where('p.id = ?', $options[Product_Model_Product_Collection::FILTER_TYPE_ID]);
            } elseif (isset($options[Product_Model_Product_Collection::FILTER_TYPE_ID]) && is_array($options[Product_Model_Product_Collection::FILTER_TYPE_ID])) {
                $select->where('p.id IN (?)', $options[Product_Model_Product_Collection::FILTER_TYPE_ID]);
            }

            //----TITLE
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_TITLE]) && is_string($options[Product_Model_Product_Collection::FILTER_TYPE_TITLE])) {
                $select->where('p.title LIKE ?', '%' . $options[Product_Model_Product_Collection::FILTER_TYPE_TITLE] . '%');
            } elseif (isset($options[Product_Model_Product_Collection::FILTER_TYPE_TITLE]) && is_array($options[Product_Model_Product_Collection::FILTER_TYPE_TITLE])) {
                $select->where('p.title IN (?)', $options[Product_Model_Product_Collection::FILTER_TYPE_TITLE]);
            }
            //----User
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_USER])) {
                $select->join(array('u' => 'users'), 'u.id = p.user_id', array('full_name'));
                if ($vDigits->isValid($options[Product_Model_Product_Collection::FILTER_TYPE_USER])) {
                    $select->where('u.id = ?', $options[Product_Model_Product_Collection::FILTER_TYPE_USER]);
                } elseif (is_string($options[Product_Model_Product_Collection::FILTER_TYPE_USER])) {
                    $select->where('u.full_name LIKE ?', $options[Product_Model_Product_Collection::FILTER_TYPE_USER] . '%');
                }
            }


            //----Order#
            if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_ORDER])) {
                $select->where('o.id = ?', $options[Product_Model_Product_Collection::FILTER_TYPE_ORDER]);
            }
            if (isset($options[Product_Model_Product_Collection::GROUP_BY_PRODUCT])) {
                $select->group('p.title');
            }
        }//****is_array options
//die($select);
    }

    public function getForSales(\Application_Model_Abstract_Collection $collection, $userId, $options, &$paginator, $currentPageNumber, $itemsPerPage, $searchValue)
    {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = new Zend_Db_Select($db);

        $select->from(array('p' => $this->_getTable()))
                ->where('p.user_id = ?', $userId)
                ->where('p.is_visible = ?', 1);

        //-----search
        if (is_string($searchValue) && strlen($searchValue) > 0) {
            $select->where('p.title LIKE ?', "{$searchValue}%");
        }

        $this->updateSelect($select, $options);

        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage($itemsPerPage)
                ->setCurrentPageNumber($currentPageNumber);

        $select->reset(Zend_Db_Select::ORDER)
                ->order('updated_at DESC')
                ->order('created_at DESC');


        foreach ($paginator as $row) {
            $product = new Product_Model_Product();
            $product->addData($row);

            $collection->addItem($product);
        }
    }

// EO getForSales

    public function getPurchase(\Application_Model_Abstract_Collection $collection, $userId, $options, &$paginator, $currentPageNumber, $itemsPerPage, $searchValue)
    {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = new Zend_Db_Select($db);

        $select->from(array('p' => $this->_getTable()))
                ->join(array('o' => 'orders'), null, array('created_at as order_date'))
                ->join(array('oi' => 'order_items'), 'o.id = oi.order_id AND p.id = oi.product_id', array('order_id', 'is_received', 'shipping_method_id', 'id as order_item_id', 'is_product_available', 'options as order_item_options', 'qty as order_item_qty'))
                ->where('o.user_id = ?', $userId)
                ->where('p.user_id != ?', $userId)
                ->order('o.created_at DESC');

        if (is_string($searchValue) && strlen($searchValue) > 0) {
            $select->where('p.title LIKE ?', "{$searchValue}%");
        }

        $this->updateSelect($select, $options);

        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage($itemsPerPage)
                ->setCurrentPageNumber($currentPageNumber);

        foreach ($paginator as $row) {
            $product = new Product_Model_Product();
            $product->addData($row);

            $collection->addItem($product);
        }
    }

    public function getOrders(\Application_Model_Abstract_Collection $collection, $userId, $options, &$paginator, $currentPageNumber, $itemsPerPage, $searchValue)
    {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = new Zend_Db_Select($db);

        $select->from(array('p' => $this->_getTable()))
                ->join(array('o' => 'orders'), null, array('created_at as order_date'))
                ->join(array('oi' => 'order_items'), 'o.id = oi.order_id AND p.id = oi.product_id', array('order_id', 'is_received', 'shipping_method_id', 'id as order_item_id', 'is_product_available', 'qty as order_item_qty', 'shipping_method_id'))
                ->where('o.user_id != ? OR o.user_id IS NULL', $userId)
                ->where('p.user_id  = ?', $userId)
                ->order('o.created_at desc');

        if (is_string($searchValue) && strlen($searchValue) > 0) {
            $select->where('p.title LIKE ?', "{$searchValue}%");
        }

        $this->updateSelect($select, $options);

        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage($itemsPerPage)
                ->setCurrentPageNumber($currentPageNumber);

        foreach ($paginator as $row) {
            $product = new Product_Model_Product();
            $product->addData($row);

            $collection->addItem($product);
        }
    }

    public function getForAdmin(\Application_Model_Abstract_Collection $collection, $options, &$paginator, $currentPageNumber, $itemsPerPage, $searchValue)
    {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = new Zend_Db_Select($db);

        $select->from(array('p' => 'view_products' /* $this->_getTable() */))
                ->joinLeft(array('oi' => 'order_items'), 'p.id  = oi.product_id', 'is_received')
                ->joinLeft(array('us' => 'users'), 'p.user_id  = us.id', 'full_name')
                ->joinLeft(array('mn' => 'manufacturers'), 'p.manufacturer_id  = mn.id', 'title as manufacturer_title');

        if (isset($options['limit']) && (int) $options['limit'] > 0) {
            $select->limit((int) $options['limit']);
        }

        if (is_string($searchValue) && strlen($searchValue) > 0) {
            $select->where('p.title LIKE ?', "%{$searchValue}%");
            $select->orWhere('us.full_name LIKE ?', "%{$searchValue}%", 'or');
            $select->orWhere('mn.title LIKE ?', "%{$searchValue}%", 'or');
        }

        $this->updateSelect($select, $options);

        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage($itemsPerPage)
                ->setCurrentPageNumber($currentPageNumber);

        $select->reset(Zend_Db_Select::ORDER)
                ->order('updated_at DESC')
                ->order('created_at DESC');

        foreach ($paginator as $row) {
            $product = new Product_Model_Product();
            $product->addData($row);

            $collection->addItem($product);
        }
    }

    public function getForAdminList($collection, $options, $offset, $limit)
    {
        $sql = 'SELECT distinct(`p`.`id`), `p`.*, `oi`.`is_received`, `us`.`full_name`, `us`.`email` as `email`, `mn`.`title` as `manufacturer_title` FROM `view_products_admin` AS `p`
            LEFT JOIN `order_items` AS `oi` ON `p`.`id` = `oi`.`product_id`
            LEFT JOIN `users` AS `us` ON `p`.`user_id` = `us`.`id`
            LEFT JOIN `manufacturers` AS `mn` ON `p`.`manufacturer_id` = `mn`.`id`';

        $where = array();
        $binds = array();

        if ($options['search']) {
            $search  = '%' . str_replace('%', '', $options['search']) . '%';
            $where[] = '(`p`.`title` LIKE :title OR `mn`.`title` LIKE :manufacturer OR `us`.`full_name` LIKE :full_name OR `us`.`email` LIKE :email)';
            $binds[] = $search;
            $binds[] = $search;
            $binds[] = $search;
            $binds[] = $search;
        }

        if ($options['status']) {
            switch ($options['status']) {
                case 'all':
                    break;
                case 'not-processed' :
                    $where[] = '`p`.`is_approved` = 0';
                    break;
                case 'approved' :
                    $where[] = '`p`.`is_approved` = 1';
                    break;
                case 'rejected' :
                    $where[] = '`p`.`is_approved` = -1';
                    break;
                case 'in-progress':
                    $where[] = '`oi`.`is_received` = 0';
                    break;
                case 'received':
                    $where[] = '`oi`.`is_received` = 1';
                    break;
                case 'published' :
                    $where[] = '`p`.`is_published` = 1';
                    break;
                case 'unpublished' :
                    $where[] = '`p`.`is_published` = 0';
                    break;
                case 'sold' :
                    $where[] = '`p`.`qty` < 1';
                    break;
                case 'notsold' :
                    $where[] = '`p`.`qty` > 0';
                    break;
                default:
                    break;
            }
        }

        if ($options['category_id']) {
            $where[] = '(`p`.`category_id` = :category_id OR `p`.`parent_category_id` = :parent_category_id)';
            $binds[] = $options['category_id'];
            $binds[] = $options['category_id'];
        }

        if ($options['start_date'] && strtotime($options['start_date'])) {
            $where[] = '`p`.`created_at` >= :created_after';
            $binds[] = strtotime($options['start_date']);
        }

        if ($options['end_date'] && strtotime($options['end_date'])) {
            $where[] = '`p`.`created_at` <= :created_before';
            $binds[] = strtotime($options['end_date']) + 24 * 3600;
        }

        if (count($where)) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }

        $sql .= ' ORDER BY `created_at` DESC';

        if (!is_null($offset) && !is_null($limit)) {
            $sql .= sprintf(' LIMIT %d, %d', $offset, $limit);
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($binds);

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $product = new Product_Model_Product();
                $product->addData($row);
                $collection->addItem($product);
            }
        }
    }

    public function getCountForAdminList($options)
    {
        $sql = 'SELECT count(distinct(`p`.`id`)) as `count` FROM `view_products_admin` AS `p`
            LEFT JOIN `order_items` AS `oi` ON `p`.`id` = `oi`.`product_id`
            LEFT JOIN `users` AS `us` ON `p`.`user_id` = `us`.`id`
            LEFT JOIN `manufacturers` AS `mn` ON `p`.`manufacturer_id` = `mn`.`id`';

        $where = array();
        $binds = array();

        if ($options['search']) {
            $search  = '%' . str_replace('%', '', $options['search']) . '%';
            $where[] = '(`p`.`title` LIKE :title OR `mn`.`title` LIKE :manufacturer OR `us`.`full_name` LIKE :full_name OR `us`.`email` LIKE :email)';
            $binds[] = $search;
            $binds[] = $search;
            $binds[] = $search;
            $binds[] = $search;
        }

        if ($options['status']) {
            switch ($options['status']) {
                case 'all':
                    break;
                case 'not-processed' :
                    $where[] = '`p`.`is_approved` = 0';
                    break;
                case 'approved' :
                    $where[] = '`p`.`is_approved` = 1';
                    break;
                case 'rejected' :
                    $where[] = '`p`.`is_approved` = -1';
                    break;
                case 'in-progress':
                    $where[] = '`oi`.`is_received` = 0';
                    break;
                case 'received':
                    $where[] = '`oi`.`is_received` = 1';
                    break;
                case 'published' :
                    $where[] = '`p`.`is_published` = 1';
                    break;
                case 'unpublished' :
                    $where[] = '`p`.`is_published` = 0';
                    break;
                case 'sold' :
                    $where[] = '`p`.`qty` < 1';
                    break;
                case 'notsold' :
                    $where[] = '`p`.`qty` > 0';
                    break;
                default:
                    break;
            }
        }

        if ($options['category_id']) {
            $where[] = '(`p`.`category_id` = :category_id OR `p`.`parent_category_id` = :parent_category_id)';
            $binds[] = $options['category_id'];
            $binds[] = $options['category_id'];
        }

        if ($options['start_date'] && strtotime($options['start_date'])) {
            $where[] = '`p`.`created_at` >= :created_after';
            $binds[] = strtotime($options['start_date']);
        }

        if ($options['end_date'] && strtotime($options['end_date'])) {
            $where[] = '`p`.`created_at` <= :created_before';
            $binds[] = strtotime($options['end_date']) + 24 * 3600;
        }

        if (count($where)) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($binds);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row && isset($row['count'])) {
            return (int) $row['count'];
        }

        return 0;
    }

    public function getSalesCount($userId)
    {
        /* $sql = "SELECT COUNT(*) as `sales_count` FROM `".$this->_getTable()."` as `p`
          JOIN `order_items` as `oi` ON `oi`.`product_id` = `p`.`id` WHERE `p`.`user_id` = :u_id"; */
        $sql  = "SELECT COUNT(*) as `sales_count` FROM `" . $this->_getTable() . "` as `p` WHERE `p`.`user_id` = :u_id";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':u_id', $userId);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return (int) $result['sales_count'];
    }

    public function getPurchaseCount($userId)
    {
        $sql = "SELECT COUNT(*) as `purchase_count` FROM `" . $this->_getTable() . "` as `p` JOIN `orders` as `o`
                 JOIN `order_items` as `oi` ON `o`.`id` = `oi`.`order_id` AND `p`.`id` = `oi`.`product_id`
                 WHERE `o`.`user_id` = :u_id AND `p`.`user_id` != :u_id1";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':u_id', $userId);
        $stmt->bindParam(':u_id1', $userId);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return (int) $result['purchase_count'];
    }

    protected function getPageUrl($productTitle, $productId)
    {
        $url     = strtolower($productTitle);
        $url     = preg_replace('/[\s]/', '-', $url);
        $url     = preg_replace('/[^-a-z0-9]/', '', $url);
        $url     = preg_replace('/[-]+/', '-', $url);
        $i       = 2;
        $baseUrl = $url;
        do {
            $catCount = 0;
            $sql1     = 'SELECT count(`page_url`) as `count` FROM `categories` WHERE `page_url` = :page_url';
            $stmt1    = $this->_getConnection()->prepare($sql1);
            $stmt1->bindParam(':page_url', $url);
            $stmt1->execute();
            $result1  = $stmt1->fetch(PDO::FETCH_ASSOC);
            if ($result1 && isset($result1['count'])) {
                $catCount = (int) $result1['count'];
            }
            ///////
            $prodCount = 0;
            $sql2      = 'SELECT count(`page_url`) as `count` FROM `products` WHERE `page_url` = :page_url AND `id` <> :id';
            $stmt2     = $this->_getConnection()->prepare($sql2);
            $stmt2->bindParam(':page_url', $url);
            $stmt2->bindParam(':id', $productId);
            $stmt2->execute();
            $result2   = $stmt2->fetch(PDO::FETCH_ASSOC);
            if ($result2 && isset($result2['count'])) {
                $prodCount = (int) $result2['count'];
            }
            if ($catCount !== 0 || $prodCount !== 0) {
                $url = $baseUrl . '-' . $i++;
            }
        } while ($catCount != 0 || $prodCount != 0);

        return $url;
    }

//---------------------------------------------------------------------------------------------------
    protected function _insert(\Application_Model_Abstract $object)
    {

        $sql  = 'INSERT INTO `' . $this->_getTable() . '`
       (
        `user_id`,
        `title`,
        `description`,
        `price`,
        `original_price`,
        `category_id`,
        `manufacturer_id`,
        `condition`,
        `material_id`,
        `color_id`,
        `width`,
        `height`,
        `depth`,
        `age`,
        `is_pick_up_available`,
        `is_delivery_available`,
        `available_from`,
        `available_till`,
        `is_published`,
        `created_at`,
		`is_available_for_purchase`,
		`is_visible`,
		`pick_up_address_id`,
        `is_approved`,
        `pickup_full_name`,
        `pickup_address_line1`,
        `pickup_address_line2`,
        `pickup_city`,
        `pickup_state`,
        `pickup_postcode`,
        `pickup_country_code`,
        `pickup_phone`,
        `page_url`,
        `expire_at`,
        `is_reminded_about_expire`,
        `qty`,
        `shipping_cover`,
        `pet_in_home`,
        `is_smoke_free`,
        `is_verified`,
        `is_under_15_pounds`,
        `is_manage_price_allowed`,
        `selling_type`,
        `number_of_items_in_set`,
        `approved_at`
        )
                  VALUES (
                            :user,
                            :title,
                            :description,
                            :price,
                            :original_price,
                            :category_id,
                            :manufacturer_id,
                            :condition,
                            :material_id,
                            :color_id,
                            :width,
                            :height,
                            :depth,
                            :age,
                            :pick_up,
                            :delivery,
                            :available_form,
                            :available_till,
                            :is_published,
                            :created_at,
							1,
							:is_visible,
							:pick_up_address_id,
                            0,
                            :pickup_full_name,
        :pickup_address_line1,
        :pickup_address_line2,
        :pickup_city,
        :pickup_state,
        :pickup_postcode,
        :pickup_country_code,
        :pickup_phone,
        :page_url,
        0,
        0,
        1,
        :shipping_cover,
        :pet_in_home,
        :is_smoke_free,
        :is_verified,
        :is_under_15_pounds,
        :is_manage_price_allowed,
        :selling_type,
        :number_of_items_in_set,
        :approved_at
                             )';
        $stmt = $this->_getConnection()->prepare($sql);

        $userId               = $object->getUserId();
        $title                = $object->getTitle();
        $description          = $object->getDescription();
        $price                = $object->getPrice();
        $originalPrice        = $object->getOriginalPrice();
        $categoryId           = $object->getCategoryId();
        $manufacturerId       = $object->getManufacturerId();
        $condition            = $object->getCondition();
        $materialId           = $object->getMaterialId();
        $colorId              = $object->getColorId();
        $width                = $object->getWidth();
        $height               = $object->getHeight();
        $depth                = $object->getDepth();
        $age                  = $object->getAge();
        $isPickUpAvailable    = $object->getIsPickUpAvailable();
        $isDeliveryAvailable  = $object->getIsDeliveryAvailable();
        $availableFrom        = $object->getAvailableFrom();
        $availableTill        = $object->getAvailableTill();
        $isPublished          = $object->getIsPublished(0);
        $createdAt            = $object->getCreatedAt();
        $isVisible            = $object->getIsVisible();
        $pickUpAddressId      = $object->getPickUpAddressId();
        $pickupFullName       = $object->getPickupFullName();
        $pickupAddressLine1   = $object->getPickupAddressLine1();
        $pickupAddressLine2   = $object->getPickupAddressLine2();
        $pickupCity           = $object->getPickupCity();
        $pickupState          = $object->getPickupState();
        $pickupPostcode       = $object->getPickupPostcode();
        $pickupCountryCode    = $object->getPickupCountryCode();
        $pickupPhone          = $object->getPickupPhone();
        $pageUrl              = $this->getPageUrl($object->getTitle(), $object->getId());
        $shipping_cover       = $object->getShippingCover();
        $petInHome            = $object->getPetInHome();
        $isSmokeFree          = $object->getIsSmokeFree();
        $isVerified           = $object->getIsVerified();
        $is_under_15_pounds   = $object->getIsUnder15Pounds();
        $isManagePriceAllowed = $object->getIsManagePriceAllowed();
        $sellingType          = $object->getSellingType();
        $numberOfItemsInSet   = $object->getNumberOfItemsInSet();
        $approvedAt           = $object->getApprovedAt();

        if (!$object->getCategoryId() || $object->getCategory()->getTitle() == 'Sofas' || $object->getCategory()->getTitle() == 'Beds') {
            $is_under_15_pounds = 0;
        }

        if ($object->getCategory()->getParent() && ($object->getCategory()->getParent()->getTitle() == 'Sofas' || $object->getCategory()->getParent()->getTitle() == 'Beds')) {
            $is_under_15_pounds = 0;
        }

        $stmt->bindParam(':user', $userId);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':original_price', $originalPrice);
        $stmt->bindParam(':category_id', $categoryId);
        $stmt->bindParam(':manufacturer_id', $manufacturerId);
        $stmt->bindParam(':condition', $condition);
        $stmt->bindParam(':material_id', $materialId);
        $stmt->bindParam(':color_id', $colorId);
        $stmt->bindParam(':width', $width);
        $stmt->bindParam(':height', $height);
        $stmt->bindParam(':depth', $depth);
        $stmt->bindParam(':age', $age);
        $stmt->bindParam(':pick_up', $isPickUpAvailable);
        $stmt->bindParam(':delivery', $isDeliveryAvailable);
        $stmt->bindParam(':available_form', $availableFrom);
        $stmt->bindParam(':available_till', $availableTill);
        $stmt->bindParam(':is_published', $isPublished);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':is_visible', $isVisible);
        $stmt->bindParam(':pick_up_address_id', $pickUpAddressId);

        $stmt->bindParam(':pickup_full_name', $pickupFullName);
        $stmt->bindParam(':pickup_address_line1', $pickupAddressLine1);
        $stmt->bindParam(':pickup_address_line2', $pickupAddressLine2);
        $stmt->bindParam(':pickup_city', $pickupCity);
        $stmt->bindParam(':pickup_state', $pickupState);
        $stmt->bindParam(':pickup_postcode', $pickupPostcode);
        $stmt->bindParam(':pickup_country_code', $pickupCountryCode);
        $stmt->bindParam(':pickup_phone', $pickupPhone);
        $stmt->bindParam(':page_url', $pageUrl);
        $stmt->bindParam(':shipping_cover', $shipping_cover);

        $stmt->bindParam(':pet_in_home', $petInHome);
        $stmt->bindParam(':is_smoke_free', $isSmokeFree);
        $stmt->bindParam(':is_verified', $isVerified);
        $stmt->bindParam(':is_under_15_pounds', $is_under_15_pounds);
        $stmt->bindParam(':is_manage_price_allowed', $isManagePriceAllowed);
        $stmt->bindParam(':selling_type', $sellingType);
        $stmt->bindParam(':number_of_items_in_set', $numberOfItemsInSet);
        $stmt->bindParam(':approved_at', $approvedAt);

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function beforeUpdate(\Application_Model_Abstract $object)
    {
        $sql       = 'UPDATE `cart_items` SET `product_price` = :price, `row_total` = (`qty` * `product_price`) WHERE `product_id` = :product_id';
        $stmt      = $this->_getConnection()->prepare($sql);
        $productId = $object->getId();
        $price     = $object->getPrice();

        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':product_id', $productId);
        $stmt->execute();
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $this->beforeUpdate($object);

        $sql = "UPDATE `" . $this->_getTable() . "`
  set
   `user_id` = :user,
   `title` = :title,
   `description` = :description,
   `price` = :price,
   `original_price` = :original_price,
   `old_price` = :old_price,
   `category_id` = :category_id,
   `manufacturer_id` = :manufacturer_id,
   `condition` = :condition,
   `material_id` = :material_id,
   `color_id` = :color_id,
   `width` = :width,
   `height` = :height,
   `depth` = :depth,
   `age` = :age,
   `is_pick_up_available`  = :pickup,
   `is_delivery_available` = :delivery,
   `available_from` = :available_from,
   `available_till` = :available_till,
   `is_published` = :is_published,
   `is_available_for_purchase` = 1,
   `is_visible` = :is_visible,
   `created_at` = :created_at,
		`pick_up_address_id` = :pick_up_address_id,
        `is_approved` = :is_approved,
        `pickup_full_name` = :pickup_full_name,
        `pickup_address_line1` = :pickup_address_line1,
        `pickup_address_line2` = :pickup_address_line2,
        `pickup_city` = :pickup_city,
        `pickup_state` = :pickup_state,
        `pickup_postcode` = :pickup_postcode,
        `pickup_country_code` = :pickup_country_code,
        `pickup_phone` = :pickup_phone,
        `page_url` = :page_url,
        `expire_at` = :expire_at,
        `is_reminded_about_expire` = :is_reminded,
        `qty` = :qty,
        `shipping_cover` = :shipping_cover,
        `updated_at` = " . time() . ",
        `pet_in_home` = :pet_in_home,
        `is_smoke_free` = :is_smoke_free,
        `is_verified` = :is_verified,
        `is_under_15_pounds` = :is_under_15_pounds,
        `is_manage_price_allowed` = :is_manage_price_allowed,
        `admin_edited_price` = :admin_edited_price,
        `selling_type` = :selling_type,
        `number_of_items_in_set` = :number_of_items_in_set,
        `approved_at` = :approved_at
		WHERE `id` = :id";

        $stmt = $this->_getConnection()->prepare($sql);

        $id             = $object->getId();
        $userId         = $object->getUserId();
        $title          = $object->getTitle();
        $description    = $object->getDescription();
        $price          = $object->getPrice();
        $originalPrice  = $object->getOriginalPrice();
        $oldPrice       = $object->getOldPrice();
        $categoryId     = $object->getCategoryId();
        $manufacturerId = $object->getManufacturerId();
        $condition      = $object->getCondition();
        $materialId     = $object->getMaterialId();
        $colorId        = $object->getColorId();
        $width          = $object->getWidth();
        $height         = $object->getHeight();
        $depth          = $object->getDepth();
        $isApproved     = $object->getIsApproved();

        $age                 = $object->getAge();
        $isPickUpAvailable   = $object->getIsPickUpAvailable();
        $isDeliveryAvailable = $object->getIsDeliveryAvailable();
        $availableFrom       = $object->getAvailableFrom();

        $availableTill = $object->getAvailableTill();
        $isPublished   = $object->getIsPublished();
        $createdAt     = $object->getCreatedAt();

        $isVisible       = $object->getIsVisible();
        $pickUpAddressId = $object->getPickUpAddressId();

        $pickupFullName       = $object->getPickupFullName();
        $pickupAddressLine1   = $object->getPickupAddressLine1();
        $pickupAddressLine2   = $object->getPickupAddressLine2();
        $pickupCity           = $object->getPickupCity();
        $pickupState          = $object->getPickupState();
        $pickupPostcode       = $object->getPickupPostcode();
        $pickupCountryCode    = $object->getPickupCountryCode();
        $pickupPhone          = $object->getPickupPhone();
        $pageUrl              = $this->getPageUrl($object->getTitle(), $object->getId());
        $exireAt              = $object->getExpireAt(0);
        $isReminded           = $object->getIsRemindedAboutExpire(0);
        $qty                  = $object->getQty();
        $shipping_cover       = $object->getShippingCover();
        $petInHome            = $object->getPetInHome();
        $isSmokeFree          = $object->getIsSmokeFree();
        $isVerified           = $object->getIsVerified();
        $is_under_15_pounds   = $object->getIsUnder15Pounds();
        $adminEditedPrice     = $object->getAdminEditedPrice();
        $isManagePriceAllowed = $object->getIsManagePriceAllowed();
        $sellingType          = $object->getSellingType();
        $numberOfItemsInSet   = $object->getNumberOfItemsInSet();
        $approvedAt           = $object->getApprovedAt();

        if (!$object->getCategoryId() || $object->getCategory()->getTitle() == 'Sofas' || $object->getCategory()->getTitle() == 'Beds') {
            $is_under_15_pounds = 0;
        }

        if ($object->getCategory()->getParent() && ($object->getCategory()->getParent()->getTitle() == 'Sofas' || $object->getCategory()->getParent()->getTitle() == 'Beds')) {
            $is_under_15_pounds = 0;
        }

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':user', $userId);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':original_price', $originalPrice);
        $stmt->bindParam(':old_price', $oldPrice);
        $stmt->bindParam(':category_id', $categoryId);
        $stmt->bindParam(':manufacturer_id', $manufacturerId);
        $stmt->bindParam(':condition', $condition);
        $stmt->bindParam(':material_id', $materialId);
        $stmt->bindParam(':color_id', $colorId);
        $stmt->bindParam(':width', $width);
        $stmt->bindParam(':height', $height);
        $stmt->bindParam(':depth', $depth);
        $stmt->bindParam(':age', $age);
        $stmt->bindParam(':pickup', $isPickUpAvailable);
        $stmt->bindParam(':delivery', $isDeliveryAvailable);
        $stmt->bindParam(':available_from', $availableFrom);
        $stmt->bindParam(':available_till', $availableTill);
        $stmt->bindParam(':is_published', $isPublished);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':is_visible', $isVisible);
        $stmt->bindParam(':pick_up_address_id', $pickUpAddressId);
        $stmt->bindParam(':is_approved', $isApproved);

        $stmt->bindParam(':pickup_full_name', $pickupFullName);
        $stmt->bindParam(':pickup_address_line1', $pickupAddressLine1);
        $stmt->bindParam(':pickup_address_line2', $pickupAddressLine2);
        $stmt->bindParam(':pickup_city', $pickupCity);
        $stmt->bindParam(':pickup_state', $pickupState);
        $stmt->bindParam(':pickup_postcode', $pickupPostcode);
        $stmt->bindParam(':pickup_country_code', $pickupCountryCode);
        $stmt->bindParam(':pickup_phone', $pickupPhone);
        $stmt->bindParam(':page_url', $pageUrl);
        $stmt->bindParam(':expire_at', $exireAt);
        $stmt->bindParam(':is_reminded', $isReminded);
        $stmt->bindParam(':qty', $qty);
        $stmt->bindParam(':shipping_cover', $shipping_cover);

        $stmt->bindParam(':pet_in_home', $petInHome);
        $stmt->bindParam(':is_smoke_free', $isSmokeFree);
        $stmt->bindParam(':is_verified', $isVerified);
        $stmt->bindParam(':is_under_15_pounds', $is_under_15_pounds);
        $stmt->bindParam(':is_manage_price_allowed', $isManagePriceAllowed);
        $stmt->bindParam(':admin_edited_price', $adminEditedPrice);
        $stmt->bindParam(':selling_type', $sellingType);
        $stmt->bindParam(':number_of_items_in_set', $numberOfItemsInSet);
        $stmt->bindParam(':approved_at', $approvedAt);
        $stmt->execute();

        if (APPLICATION_ENV === 'live') {
            $this->_logUpdate($object);
        }
    }

    protected function prepareProduct($product)
    {
        $str = date('Y-m-d H:i:s') . " Product Data:\n";
        $str .= 'id: ' . (string) $product->getData('id') . "\n";
        $str .= 'user_id: ' . (string) $product->getData('user_id') . "\n";
        $str .= 'title: ' . (string) $product->getData('title') . "\n";
        $str .= 'is_published: ' . (string) $product->getData('is_published') . "\n";
        $str .= 'isPublished: ' . (int) $product->getIsPublished() . "\n";
        return $str;
    }

    protected function _logUpdate($product)
    {
        $fileName = APPLICATION_PATH . '/log/save-product.log';

        $data = $this->prepareProduct($product) . "\n*******************************************\n\n";

        file_put_contents($fileName, $data, FILE_APPEND);
    }

    protected function _getFullMatch($query, $filters, $order = array())
    {
        $query_like = '%' . str_replace('%', '', strtolower(trim($query))) . '%';

        $where = '';
        $bind  = array();

        if (isset($filters['min_price']) && (int) $filters['min_price'] >= 0) {
            $where .= ' AND `products`.`price` >= :min_price';
            $bind[] = $filters['min_price'];
        }
        if (isset($filters['max_price']) && (int) $filters['max_price'] > 0) {
            $where .= ' AND `products`.`price` <= :max_price';
            $bind[] = $filters['max_price'];
        }
        if (isset($filters['colors'])) {
            $i         = 1;
            $condition = array();
            foreach ($filters['colors'] as $color) {
                $condition[] = ':color' . $i++;
                $bind[]      = $color;
            }
            $where .= sprintf(' AND `products`.`color_id` IN (%s)', implode(', ', $condition));
        }
        if (isset($filters['brands'])) {
            $i         = 1;
            $condition = array();
            foreach ($filters['brands'] as $color) {
                $condition[] = ':brand' . $i++;
                $bind[]      = $color;
            }
            $where .= sprintf(' AND `products`.`manufacturer_id` IN (%s)', implode(', ', $condition));
        }
        if (isset($filters['categories'])) {
            $i         = 1;
            $condition = array();
            foreach ($filters['categories'] as $color) {
                $condition[] = ':category' . $i++;
                $bind[]      = $color;
            }
            $where .= sprintf(' AND `products`.`category_id` IN (%s)', implode(', ', $condition));
        }
        if (isset($filters['conditions'])) {
            $i         = 1;
            $condition = array();
            foreach ($filters['conditions'] as $color) {
                $condition[] = ':condition' . $i++;
                $bind[]      = $color;
            }
            $where .= sprintf(' AND `products`.`condition` IN (%s)', implode(', ', $condition));
        }

        $sql = 'SELECT `products`.*
            FROM `products`
            JOIN `categories` ON `products`.`category_id` = `categories`.`id`
            JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
            JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
            JOIN `colors` ON `products`.`color_id` = `colors`.`id`
            JOIN `materials` ON `products`.`material_id` = `materials`.`id`
            JOIN `users` ON `products`.`user_id` = `users`.`id`
            WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1 ' . $where . '
            AND
            (`products`.`title` LIKE :query1
            OR `products`.`description` LIKE :query2
            OR `categories`.`title` LIKE :query3
            OR `conditions`.`short_title` LIKE :query4
            OR `manufacturers`.`title` LIKE :query5
            OR `colors`.`title` LIKE :query6
            OR `materials`.`title` LIKE :query7)';

        $orderField     = isset($order['field']) ? $order['field'] : '`products`.`created_at`';
        $orderDirection = (isset($order['direction'])) ? $order['direction'] : 'desc';
        $orderDirection = (strtolower($orderDirection) == 'desc') ? 'DESC' : 'ASC';
        $sql .= ' ORDER BY ' . $orderField . ' ' . $orderDirection;

        $stmt = $this->_getConnection()->prepare($sql);

        $bind[] = $query_like;
        $bind[] = $query_like;
        $bind[] = $query_like;
        $bind[] = $query_like;
        $bind[] = $query_like;
        $bind[] = $query_like;
        $bind[] = $query_like;

        $stmt->execute($bind);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    protected function _getPartialMatch($query, $filters, $order = array())
    {
        $q = explode(' ', trim($query));

        foreach ($q as $word) {
            $query_like[] = '%' . str_replace('%', '', strtolower(trim($word))) . '%';
        }

        $where = '';
        $bind  = array();

        if (isset($filters['min_price']) && (int) $filters['min_price'] >= 0) {
            $where .= ' AND `products`.`price` >= :min_price';
            $bind[] = $filters['min_price'];
        }
        if (isset($filters['max_price']) && (int) $filters['max_price'] > 0) {
            $where .= ' AND `products`.`price` <= :max_price';
            $bind[] = $filters['max_price'];
        }
        if (isset($filters['colors'])) {
            $i         = 1;
            $condition = array();
            foreach ($filters['colors'] as $color) {
                $condition[] = ':color' . $i++;
                $bind[]      = $color;
            }
            $where .= sprintf(' AND `products`.`color_id` IN (%s)', implode(', ', $condition));
        }
        if (isset($filters['brands'])) {
            $i         = 1;
            $condition = array();
            foreach ($filters['brands'] as $color) {
                $condition[] = ':brand' . $i++;
                $bind[]      = $color;
            }
            $where .= sprintf(' AND `products`.`manufacturer_id` IN (%s)', implode(', ', $condition));
        }
        if (isset($filters['categories'])) {
            $i         = 1;
            $condition = array();
            foreach ($filters['categories'] as $color) {
                $condition[] = ':category' . $i++;
                $bind[]      = $color;
            }
            $where .= sprintf(' AND `products`.`category_id` IN (%s)', implode(', ', $condition));
        }
        if (isset($filters['conditions'])) {
            $i         = 1;
            $condition = array();
            foreach ($filters['conditions'] as $color) {
                $condition[] = ':condition' . $i++;
                $bind[]      = $color;
            }
            $where .= sprintf(' AND `products`.`condition` IN (%s)', implode(', ', $condition));
        }

        $sql = 'SELECT `products`.*
            FROM `products`
            JOIN `categories` ON `products`.`category_id` = `categories`.`id`
            JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
            JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
            JOIN `colors` ON `products`.`color_id` = `colors`.`id`
            JOIN `materials` ON `products`.`material_id` = `materials`.`id`
            JOIN `users` ON `products`.`user_id` = `users`.`id`
            WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1' . $where;

        $c = 1;

        foreach ($query_like as $word) {
            $sql .= "\n" . 'AND
            (`products`.`title` LIKE :query' . $c++ . /* '
                      OR `products`.`description` LIKE :query' . $c++ . */ '
            OR `categories`.`title` LIKE :query' . $c++ . '
            OR `conditions`.`short_title` LIKE :query' . $c++ . '
            OR `manufacturers`.`title` LIKE :query' . $c++ . '
            OR `colors`.`title` LIKE :query' . $c++ . '
            OR `materials`.`title` LIKE :query' . $c++ . ')';

            //$bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
        }

        $orderField     = isset($order['field']) ? $order['field'] : '`products`.`created_at`';
        $orderDirection = (isset($order['direction'])) ? $order['direction'] : 'desc';
        $orderDirection = (strtolower($orderDirection) == 'desc') ? 'DESC' : 'ASC';
        $sql .= ' ORDER BY ' . $orderField . ' ' . $orderDirection;

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bind);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function searchProductPagesHeader(Application_Model_Abstract_Collection $collection, $query, $offset, $limit, $filters, $order = array())
    {
        $out     = array();
        $active  = array();
        $expired = array();
        $sold    = array();

        $res1 = $this->_getFullMatch($query, $filters, $order);

        if ($res1) {
            foreach ($res1 as $product) {
                if ($product['qty'] > 0) {
                    if ((!$product['available_till'] || $product['available_till'] > time()) && (!$product['expire_at'] || $product['expire_at'] > time())) {
                        $active[$product['id']] = $product;
                    } else {
                        $expired[$product['id']] = $product;
                    }
                } else {
                    $sold[$product['id']] = $product;
                }
            }
        }

        $res2 = $this->_getPartialMatch($query, $filters, $order);

        if ($res2) {
            foreach ($res2 as $product) {
                if ($product['qty'] > 0) {
                    if ((!$product['available_till'] || $product['available_till'] > time()) && (!$product['expire_at'] || $product['expire_at'] > time())) {
                        $active[$product['id']] = $product;
                    } else {
                        $expired[$product['id']] = $product;
                    }
                } else {
                    $sold[$product['id']] = $product;
                }
            }
        }

        $out = array_merge($active, $expired, $sold);

        $c = 0;
        foreach ($out as $row) {
            $c++;

            if ($c <= $offset) {
                continue;
            }

            $item = new Product_Model_Product();
            $item->addData($row);
            $collection->addItem($item);

            if ($c >= $offset + $limit) {
                break;
            }
        }
    }

    protected function getIdProduct($object, $data)
    {
        if (!is_array($data)) {
            $object = new $object;
            return $object->getByTitle($data)->getId();
        }
        foreach ($data as $getId) {
            $object = new $object;
            $id     = $object->getByTitle($getId)->getId();
            if (!empty($id)) {
                $masId[] = $id;
            }
        }
        if (isset($masId)) {
            return implode(',', $masId);
        } else {
            return false;
        }
    }

    public function searchAndFilter(Application_Model_Abstract_Collection $collection, $data, $offset, $limit)
    {
        $query = '%' . str_replace('%', '', strtolower($data['query'])) . '%';

        $sql = 'SELECT * FROM `products` WHERE'
                . ' `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1'
                . ' AND `products`.`qty` > 0'
                . ' AND (`products`.`available_till` = 0 OR `products`.`available_till` IS NULL OR `products`.`available_till` > ' . time() . ')'
                . ' AND (`products`.`expire_at` = 0 OR `products`.`expire_at` > ' . time() . ')'
                . ' AND (`title` LIKE :query1 OR `description` LIKE :query2)';

        if (isset($data['style']) && is_array($data['style']) && count($data['style']) > 0) {
            $sql .= ' AND `category_id` IN (' . implode(',', $data['style']) . ')';
        }

        if (isset($data['color']) && is_array($data['color']) && count($data['color']) > 0) {
            $sql .= ' AND `color_id` IN (' . implode(',', $data['color']) . ')';
        }

        if (isset($data['brand']) && is_array($data['brand']) && count($data['brand']) > 0) {
            $sql .= ' AND `manufacturer_id` IN (' . implode(',', $data['brand']) . ')';
        }

        if (isset($data['condition']) && is_array($data['condition']) && count($data['condition']) > 0) {
            $sql .= ' AND `condition` IN ("' . implode('","', $data['condition']) . '")';
        }

        if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
            $sql .= ' AND `price` >= :min_price';
        }

        if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
            $sql .= ' AND `price` <= :max_price';
        }

        if (isset($data['user_id'])) {
            $sql .= ' AND `user_id` != :user_id';
        }

        $sql .= ' AND `is_visible` = 1';

        $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':query1', $query);
        $stmt->bindParam(':query2', $query);

        if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
            $stmt->bindParam(':min_price', $data['min_price']);
        }

        if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
            $stmt->bindParam(':max_price', $data['max_price']);
        }

        if (isset($data['user_id'])) {
            $stmt->bindParam(':user_id', $data['user_id']);
        }

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Product_Model_Product();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getByIds(Application_Model_Abstract_Collection $collection, $ids)
    {
        $sql = 'SELECT * FROM `products` WHERE `id` IN (' . implode(', ', $ids) . ')';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Product_Model_Product();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function filterAndFilter(Application_Model_Abstract_Collection $collection, $data, $offset, $limit)
    {
        $query = $data['query'];

        $sql = 'SELECT `products`.* FROM `products` JOIN `categories`
			ON `products`.`category_id` = `categories`.`id`
			WHERE `products`.`is_published` = 1 AND (`categories`.`parent_id` = :query OR `categories`.`id` = :query2)';

        if (isset($data['style']) && is_array($data['style']) && count($data['style']) > 0) {
            $sql .= ' AND `products`.`category_id` IN (' . implode(',', $data['style']) . ')';
        }

        if (isset($data['color']) && is_array($data['color']) && count($data['color']) > 0) {
            $sql .= ' AND `products`.`color_id` IN (' . implode(',', $data['color']) . ')';
        }

        if (isset($data['brand']) && is_array($data['brand']) && count($data['brand']) > 0) {
            $sql .= ' AND `products`.`manufacturer_id` IN (' . implode(',', $data['brand']) . ')';
        }

        if (isset($data['condition']) && is_array($data['condition']) && count($data['condition']) > 0) {
            $sql .= ' AND `products`.`condition` IN ("' . implode('","', $data['condition']) . '")';
        }

        if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
            $sql .= ' AND `products`.`price` >= :min_price';
        }

        if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
            $sql .= ' AND `products`.`price` <= :max_price';
        }

        $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':query', $query);
        $stmt->bindParam(':query2', $query);

        if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
            $stmt->bindParam(':min_price', $data['min_price']);
        }

        if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
            $stmt->bindParam(':max_price', $data['max_price']);
        }

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Product_Model_Product();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function countSearchAndFilterResults($data)
    {
        $query = '%' . str_replace('%', '', strtolower($data['query'])) . '%';

        $sql = 'SELECT COUNT(*) as `count` FROM `products` WHERE `products`.`is_published` = 1 AND (`title` LIKE :query1 OR `description` LIKE :query2)';

        if (isset($data['category']) && is_array($data['category']) && count($data['category']) > 0) {
            $sql .= ' AND `category_id` IN (' . implode(',', $data['category']) . ')';
        }

        if (isset($data['color']) && is_array($data['color']) && count($data['color']) > 0) {
            $sql .= ' AND `color_id` IN (' . implode(',', $data['color']) . ')';
        }

        if (isset($data['manufacturer']) && is_array($data['manufacturer']) && count($data['manufacturer']) > 0) {
            $sql .= ' AND `manufacturer_id` IN (' . implode(',', $data['manufacturer']) . ')';
        }

        if (isset($data['condition']) && is_array($data['condition']) && count($data['condition']) > 0) {
            $sql .= ' AND `condition` IN (' . implode(',', $data['condition']) . ')';
        }

        if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
            $sql .= ' AND `price` >= :min_price';
        }

        if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
            $sql .= ' AND `price` <= :max_price';
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':query1', $query);
        $stmt->bindParam(':query2', $query);

        if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
            $stmt->bindParam(':min_price', $data['min_price']);
        }

        if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
            $stmt->bindParam(':max_price', $data['max_price']);
        }

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return (int) $row['count'];
        }
        return 0;
    }

    public function getMinPriceByQuery($query)
    {
        /* $query = '%' . str_replace('%', '', strtolower($query)) . '%';

          $sql = 'SELECT MIN(`price`) as `min_price` FROM `products` WHERE `products`.`is_published` = 1 AND (`title` LIKE :query1 OR `description` LIKE :query2)';

          $stmt = $this->_getConnection()->prepare($sql);

          $stmt->bindParam(':query1', $query);
          $stmt->bindParam(':query2', $query);

          $stmt->execute();

          $row = $stmt->fetch(PDO::FETCH_ASSOC); */

        $q = explode(' ', trim($query));

        foreach ($q as $word) {
            $query_like[] = '%' . str_replace('%', '', strtolower(trim($word))) . '%';
        }

        $sql = 'SELECT MIN(`products`.`price`) as `min_price`
            FROM `products`
            JOIN `categories` ON `products`.`category_id` = `categories`.`id`
            JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
            JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
            JOIN `colors` ON `products`.`color_id` = `colors`.`id`
            WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1';

        $c    = 1;
        $bind = array();

        foreach ($query_like as $word) {
            $sql .= "\n" . 'AND
            (`products`.`title` LIKE :query' . $c++ . '
            OR `products`.`description` LIKE :query' . $c++ . '
            OR `categories`.`title` LIKE :query' . $c++ . '
            OR `conditions`.`short_title` LIKE :query' . $c++ . '
            OR `manufacturers`.`title` LIKE :query' . $c++ . '
            OR `colors`.`title` LIKE :query' . $c++ . ')';

            $bind = array_pad($bind, $c - 1, $word);
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bind);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return $row['min_price'];
        }

        return 0;
    }

    public function getMinPriceByCategory($query)
    {
        $sql = 'SELECT MIN(`price`) as `min_price` FROM `view_product_prices`
			WHERE `category_id` = :category_id1 OR `parent_id` = :category_id2';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':category_id1', $query);
        $stmt->bindParam(':category_id2', $query);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return $row['min_price'];
        }

        return 0;
    }

    public function getMinPriceByManufacturer($manufacturer_id)
    {
        $sql = 'SELECT MIN(`price`) as `min_price` FROM `products`
			WHERE `manufacturer_id` = :manufacturer_id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':manufacturer_id', $manufacturer_id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return $row['min_price'];
        }

        return 0;
    }

    public function getMaxPriceByManufacturer($manufacturer_id)
    {
        $sql = 'SELECT MAX(`price`) as `max_price` FROM `products`
			WHERE `manufacturer_id` = :manufacturer_id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':manufacturer_id', $manufacturer_id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return $row['max_price'];
        }

        return 0;
    }

    public function getMaxPriceByQuery($query)
    {
        /* $query = '%' . str_replace('%', '', strtolower($query)) . '%';

          $sql = 'SELECT MAX(`price`) as `max_price` FROM `products` WHERE `products`.`is_published` = 1 AND (`title` LIKE :query1 OR `description` LIKE :query2)';

          $stmt = $this->_getConnection()->prepare($sql);

          $stmt->bindParam(':query1', $query);
          $stmt->bindParam(':query2', $query);

          $stmt->execute();

          $row = $stmt->fetch(PDO::FETCH_ASSOC); */

        $q = explode(' ', trim($query));

        foreach ($q as $word) {
            $query_like[] = '%' . str_replace('%', '', strtolower(trim($word))) . '%';
        }

        $sql = 'SELECT MAX(`products`.`price`) as `max_price`
            FROM `products`
            JOIN `categories` ON `products`.`category_id` = `categories`.`id`
            JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
            JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
            JOIN `colors` ON `products`.`color_id` = `colors`.`id`
            WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1';

        $c    = 1;
        $bind = array();

        foreach ($query_like as $word) {
            $sql .= "\n" . 'AND
            (`products`.`title` LIKE :query' . $c++ . '
            OR `products`.`description` LIKE :query' . $c++ . '
            OR `categories`.`title` LIKE :query' . $c++ . '
            OR `conditions`.`short_title` LIKE :query' . $c++ . '
            OR `manufacturers`.`title` LIKE :query' . $c++ . '
            OR `colors`.`title` LIKE :query' . $c++ . ')';

            $bind = array_pad($bind, $c - 1, $word);
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bind);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return $row['max_price'];
        }

        return 999999;
    }

    public function getMaxPriceByCategory($query)
    {
        $sql = 'SELECT MAX(`price`) as `max_price` FROM `view_product_prices`
			WHERE `category_id` = :category_id1 OR `parent_id` = :category_id2';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':category_id1', $query);
        $stmt->bindParam(':category_id2', $query);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return $row['max_price'];
        }

        return 999999;
    }

    public function getByUser($collection, $userId)
    {
        $sql = 'SELECT * FROM `products` WHERE `user_id` = :user_id AND `is_published` = 1 AND `is_approved` = 1;';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $userId);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Product_Model_Product();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getAllByUser($collection, $userId)
    {
        $sql = 'SELECT * FROM `products` WHERE `user_id` = :user_id;';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $userId);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Product_Model_Product();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getByUserAndCategory($collection, $userId, $categoryId)
    {
        $sql = 'SELECT `products`.*
			FROM `products`
			JOIN `categories` ON `products`.`category_id` = `categories`.`id`
			WHERE `products`.`user_id` = :user_id  AND `products`.`is_published` = 1 AND
			(`categories`.`parent_id` = :category_id1 OR `categories`.`id` = :category_id2)';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':category_id1', $categoryId);
        $stmt->bindParam(':category_id2', $categoryId);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Product_Model_Product();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getFixedProduct(\Product_Model_Product $object)
    {
        $sql = "SELECT `p`.* FROM `" . $this->_getTable() . "` as `p` INNER JOIN `products_home` as `ph` ON `ph`.`product_id` = `p`.`id`";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            $object->setData($result);
        }
    }

    public function getByPageUrl(\Product_Model_Product $object, $pageUrl)
    {
        $sql  = "SELECT * FROM `" . $this->_getTable() . "` WHERE `page_url` = :page_url LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':page_url', $pageUrl);

        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            $object->setData($result);
        }
    }

    public function getAboutExpireTomorrow(\Product_Model_Product_Collection $collection, $limit)
    {

        $now = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        $tommorow = array($now + 86399, $now + 86400 * 2);
        //$threeDaysLater = array($now + (86400 * 3), $now + (86400 * 3) + 86400);

        $sql = "SELECT * FROM `" . $this->_getTable() . "`
                WHERE `expire_at` BETWEEN " . $tommorow[0] . " AND " . $tommorow[1] . "
                AND `is_published` = 1
                AND `is_reminded_about_expire` = 1
                AND `qty` > 0
                LIMIT {$limit}";

        $stmt = $this->_getConnection()->prepare($sql);
        $this->fillCollection($stmt, $collection);
    }

    public function getAboutExpireThreeDays(\Product_Model_Product_Collection $collection, $limit)
    {
        $now            = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $threeDaysLater = array($now + (86400 * 3) - 1, $now + (86400 * 3) - 1 + 86399);

        $sql = "SELECT * FROM `" . $this->_getTable() . "`
                WHERE `expire_at` BETWEEN " . $threeDaysLater[0] . " AND " . $threeDaysLater[1] . "
                AND `is_published` = 1
                AND `is_reminded_about_expire` = 0
                AND `qty` > 0
                LIMIT {$limit}";

        $stmt = $this->_getConnection()->prepare($sql);
        $this->fillCollection($stmt, $collection);
    }

    public function markAsReminded(\Product_Model_Product_Collection $collection, $marker = 1)
    {
        $in   = implode(',', $collection->getColumn('id'));
        if ($marker != 0 && $marker != 1 && $marker != 2)
            throw new Exception(__METHOD__ . '  incorrect marker "' . $marker . '". It should be 0,1 or 2.');
        if (empty($in))
            return;
        $sql  = "UPDATE `" . $this->_getTable() . "` SET `is_reminded_about_expire` = {$marker} WHERE `id` IN ({$in})";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
    }

    public function getExpired(\Product_Model_Product_Collection $collection)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `expire_at` != 0 AND `is_published` = 1 AND `expire_at` < " . time();

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            foreach ($result as $item) {
                $product = new Product_Model_Product();
                $product->setData($item);
                $collection->addItem($product);
            }
        }
    }

    public function unpublishExpired()
    {
        if (APPLICATION_ENV !== 'live') {
            $select = 'SELECT `id` FROM `' . $this->_getTable() . '` WHERE `expire_at` != 0 AND `is_published` = 1 AND `expire_at` < ' . time();
            $stmt0  = $this->_getConnection()->prepare($select);

            $stmt0->execute();
            $rows = $stmt0->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                $catalogSearch = new Catalog_Model_CatalogSearch();
                foreach ($rows as $row) {
                    if (isset($row['id'])) {
                        $catalogSearch->deleteProductIndexById($row['id']);
                    }
                }
            }
        }

        $sql = "UPDATE `" . $this->_getTable() . "`
            SET `is_published` = 0
            WHERE `expire_at` != 0 AND `is_published` = 1 AND `expire_at` < " . time();

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
    }

    public function getProductsByUserAndStatus(\Product_Model_Product_Collection $collection, $userId, $status)
    {
        $sql = "SELECT `p`.*, `status`, `o`.`id` AS 'item_id' FROM `" . $this->_getTable() . "` AS `p`
                INNER JOIN `order_items` AS `o` ON `o`.`product_id` = `p`.`id` AND `o`.`status` = :status
                WHERE `p`.`user_id` = :user_id AND `p`.`qty` = 0";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':status', $status);
        $stmt->bindParam(':user_id', $userId);

        $this->fillCollection($stmt, $collection);
    }

    public function getLatestNotSoldNotInCart(\Application_Model_Abstract_Collection $collection, $limit, $exclude)
    {
        $userId              = User_Model_Session::user()->getId(0);
        $cartItemsCollection = User_Model_Session::instance()->getCart()->getItemsCollection();

        $in = implode(',', array_merge($cartItemsCollection->getColumn('product_id'), $exclude));
        if ($in) {
            $in = 'NOT IN (' . $in . ') ';
        }

        $sql = "SELECT `p`.* FROM`" . $this->_getTable() . "` AS `p` WHERE `qty` > 0 AND `is_approved` = 1 AND `is_published` = 1 AND `is_delivery_available` = 1
                AND `p`.`user_id` != :user_id AND `id` {$in}
                ORDER BY `created_at` DESC LIMIT :limit";


        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':limit', $limit);

        $this->fillCollection($stmt, $collection);
    }

    public function deliveryDateAvailable($product)
    {
        $sql = 'SELECT COUNT(*) as `count` FROM `delivery_dates`
            WHERE `date` > :now AND `date` < :prodAvailability
            AND (`is_first_period_available` = 1 OR `is_second_period_available` = 1 OR `is_third_period_available` = 1)';

        $stmt = $this->_getConnection()->prepare($sql);

        $now = time() + 24 * 3600;

        if ($product->getAvailableTill()) {
            $pA = (int) $product->getAvailableTill() + 24 * 3600;
        } else {
            $pA = time() + 70 * 24 * 3600;
        }

        $stmt->bindParam(':now', $now);
        $stmt->bindParam(':prodAvailability', $pA);

        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result && isset($result['count'])) {
            return (int) $result['count'];
        }
        return 0;
    }

    /**
     * Retrieves products which not queued in task manager
     * @param  object Application_Model_Abstract_Collection $collection
     * @param  int $limit
     * @return
     */
    public function getForGoogleTask(\Application_Model_Abstract_Collection $collection, $limit)
    {
        $sql = "SELECT `p`.* FROM `" . $this->_getTable() . "` AS `p` LEFT JOIN `google_shopping_content_tasks` AS `gt`
                ON `gt`.`product_id` = `p`.`id`
                WHERE
                    `p`.`available_till` > " . time() . " AND
                    `p`.`expire_at` > " . time() . " AND
                    `p`.`qty` > 0 AND
                    `p`.`is_available_for_purchase` = 1 AND
                    `p`.`is_published` = 1 AND
                    `p`.`is_visible` = 1 AND
                    `gt`.`id` IS NULL
                GROUP BY `p`.`id` LIMIT :limit";

        $stmt = $this->_prepareSql($sql);
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);

        $this->fillCollection($stmt, $collection);

        return $collection;
    }

    public function getPublishedSoldProduct($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `is_published` = 1 AND `is_visible` = 1 AND `qty` < 1';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Product_Model_Product();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getFeatured($collection)
    {
        $sql = 'SELECT * FROM `featured_products` WHERE `is_active` = 1';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Product_Model_Product($row['product_id']);
                $collection->addItem($item);
            }
        }
    }

    public function saveFeatured($productIds)
    {
        $this->_getConnection()->query('DELETE FROM `featured_products`');

        $sql = 'INSERT INTO `featured_products` (`product_id`, `is_active`) VALUES (:product_id, 1)';

        foreach ($productIds as $productId) {
            $stmt = $this->_getConnection()->prepare($sql);
            $stmt->bindParam(':product_id', $productId);
            $stmt->execute();
        }
    }

    public function getCartSuggestions($existingItems)
    {
        $out = array();

        $sql = 'SELECT * FROM `products` '
                . 'WHERE `user_id` = :user_id '
                . 'AND `id` NOT IN (:productIds) '
                . 'AND `is_visible` = 1 '
                . 'AND `is_published` = 1 '
                . 'AND `is_approved` = 1 '
                . 'AND `qty` > 0 '
                . 'AND (`available_till` = 0 OR `available_till` IS NULL OR `available_till` > ' . time() . ') '
                . 'AND (`expire_at` = 0 OR `expire_at` > ' . time() . ')';

        foreach ($existingItems as $userId => $productIds) {
            $productIds = implode(', ', $productIds);

            $stmt = $this->_getConnection()->prepare($sql);

            $stmt->bindParam('user_id', $userId);
            $stmt->bindParam('productIds', $productIds);

            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if ($rows) {
                foreach ($rows as $row) {
                    $item = new Product_Model_Product();
                    $item->setData($row);

                    $out[$userId][] = $item;
                }
            }

            $stmt->closeCursor();
        }

        return $out;
    }

    /**
     * Set is_published to 0 on products listed in given ids
     * @param array $ids
     * @return bool
     */
    public function unpublishProducts(array $ids)
    {
        if (!empty($ids)) {
            $sql = 'UPDATE `products` set `is_published` = 0 WHERE `id` IN (';
            $sql .= str_pad('', 2 * count($ids) - 1, '?,') . ')';

            $stmt = $this->_prepareSql($sql);
            return $stmt->execute($ids);
        }

        return false;
    }
    
    public function productRatingUP($productID, $rating)
    {      
        $sql = 'UPDATE `products` SET `product_popularity` = :rating WHERE id = :productID';
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':rating', $rating);
        $stmt->bindParam(':productID', $productID);
        $stmt->execute();
    }

}