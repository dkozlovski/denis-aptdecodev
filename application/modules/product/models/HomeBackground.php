<?php

class Product_Model_HomeBackground extends Application_Model_Abstract
{

   protected $_backgroundDir = '/public/upload/homepage/background';
   protected $_backgroundUri = 'upload/homepage/background';
   
   public function __construct($id = null)
   {
    parent::__construct($id);
    $this->_backendClass = 'Product_Model_Home_Backend';
    $this->_backgroundDir = realpath(APPLICATION_PATH.$this->_backgroundDir).DIRECTORY_SEPARATOR;  
   }
   
   public function change($imageName)
   {
     $this->_getbackend()->changeBackground($imageName, $this);
     return $this->getInsertedImageUrl();
   }
   
   public function remove()
   {
    $this->_getbackend()->removeBackground();
   }
   
   public function getImageName ()
   {
        return $this->_getbackend()->getBackgroundName();
   }
   
   public static function url($imgName = null)
   {
        $obj = new self();
    if(is_null($imgName))
    {
    $url = $obj->_getbackend()->getBackgroundName();
    } else
        {
            $url = $imgName;
        }
        if($url)
        {
            $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
            $url = $cloudFrontUrl . '/homepage/' . $url;
        }
        
     return $url; 
   }
   
   protected function getInsertedImageUrl ()
   { 
        if($url = $this->getInsertedImage())
        {
            $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
            $url = $cloudFrontUrl . '/homepage/' . $url;
        }
        
     return $url;   
   }
   
   public function save(){}

}