<?php

class Product_Model_Product2_Collection extends Ikantam_Collection2
{

    protected $_table        = 'view_products';
    protected $_backendClass = 'Product_Model_Product2_Backend';

    public function getProducts($params)
    {
        $b = new $this->_backendClass();
        $b->getProducts($this, $params);
        return $this;
    }

    /**
     * Get all products added by the user
     * 
     * @param string $userId
     * @param string $categoryId
     * @param integer $offset
     * @param integer $limit
     * @return Product_Model_Product2_Collection
     */
    public function getByUser($userId, $categoryId = null, $offset = null, $limit = null)
    {
        $b = new $this->_backendClass();
        $b->getByUser($this, $userId, $categoryId, $offset, $limit);
        return $this;
    }

    /**
     * Get all salable products added by the user
     * 
     * @param string $userId
     * @param string $categoryId
     * @param integer $offset
     * @param integer $limit
     * @return Product_Model_Product2_Collection
     */
    public function getSalableByUser($userId, $categoryId = null, $offset = null, $limit = null)
    {
        $b = new $this->_backendClass();
        $b->getSalableByUser($this, $userId, $categoryId, $offset, $limit);
        return $this;
    }

    public function getFields()
    {
        //$data = require_once('users_array.php');
        //return array_keys($data);

        $fields = array('id', 'user_id', 'title',
            'description', 'price', 'original_price',
            'category_id', 'manufacturer_id', 'condition',
            'material_id', 'color_id', 'width', 'height',
            'depth', 'age', 'available_from', 'available_till',
            'is_published', 'created_at', 'is_pick_up_available',
            'is_delivery_available', 'is_available_for_purchase',
            'is_visible', 'pick_up_address_id', 'qty', 'is_approved',
            'parent_category_id', 'expire_at');

        return $fields;
    }

    public function setFilters(array $filters)
    {
        if (isset($filters['query'])) {
            $query = '%' . str_replace('%', '', $filters['query']) . '%';
            $this->addFilter('title', $query, 'like');
        }
        if (isset($filters['parent_category_id'])) {
            $this->addFilter('parent_category_id', $filters['parent_category_id'], 'eq');
        }
        if (isset($filters['categories'])) {
            $this->addFilter('category_id', $filters['categories'], 'in');
        }
        if (isset($filters['min_price'])) {
            $this->addFilter('price', $filters['min_price'], 'gteq');
        }
        if (isset($filters['max_price'])) {
            $this->addFilter('price', $filters['max_price'], 'lteq');
        }
        if (isset($filters['conditions'])) {
            $this->addFilter('condition', $filters['conditions'], 'in');
        }
        if (isset($filters['colors'])) {
            $this->addFilter('color_id', $filters['colors'], 'in');
        }
        if (isset($filters['brands'])) {
            $this->addFilter('manufacturer_id', $filters['brands'], 'in');
        }
        if (isset($filters['is_approved'])) {
            $this->addFilter('is_approved', $filters['is_approved'], 'eq');
        }
        if (isset($filters['user_id'])) {
            $this->addFilter('user_id', $filters['user_id'], 'neq');
        }
        if (isset($filters['only_available']) && $filters['only_available'] == true) {

            $filter = array(
                array('qty', 0, 'gt'),
                array('or' => array(
                        array('available_till', 0, 'eq'),
                        array('available_till', null, 'isnull'),
                        array('available_till', time(), 'gteq')
                    )),
                array('or' => array(
                        array('expire_at', 0, 'eq'),
                        array('expire_at', time(), 'gteq')
                    )),
            );


            $this->addCompoundFilters($filter, 'and');
        }

        $this->addFilter('is_published', 1, 'eq');

        return $this;
    }

}