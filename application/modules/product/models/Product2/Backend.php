<?php

class Product_Model_Product2_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'products';

	public function getByUser($collection, $userId, $categoryId, $offset, $limit)
	{
		$sql = 'SELECT `products`.* FROM `products`';

		if (!is_null($categoryId)) {
			$sql .= ' JOIN `categories` ON `products`.`category_id` = `categories`.`id`';
		}

		$sql .= ' WHERE `user_id` = :user_id AND `is_visible` = 1';

		if (!is_null($categoryId)) {
			$sql .= ' AND (`category_id` = :category_id OR `parent_id` = :parent_id)';
		}

		if (!is_null($offset) && !is_null($limit)) {
			$sql .= ' LIMIT ' . (int) $offset . ', ' . (int) $limit;
		}

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);

		if (!is_null($categoryId)) {
			$stmt->bindParam(':category_id', $categoryId);
			$stmt->bindParam(':parent_id', $categoryId);
		}

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new Product_Model_Product();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getSalableByUser($collection, $userId, $categoryId, $offset, $limit)
	{
		$sql = 'SELECT `products`.* FROM `products`';

		if (!is_null($categoryId)) {
			$sql .= ' JOIN `categories` ON `products`.`category_id` = `categories`.`id`';
		}

		//@TODO:change table structure
		//$sql .= ' WHERE `user_id` = :user_id AND `is_published` = 1 AND `is_visible` = 1 AND `is_sold` = 0';
		$sql .= ' WHERE `user_id` = :user_id AND `is_published` = 1 AND `is_visible` = 1 AND `is_approved` = 1';

		if (!is_null($categoryId)) {
			$sql .= ' AND (`category_id` = :category_id OR `parent_id` = :parent_id)';
		}

		if (!is_null($offset) && !is_null($limit)) {
			$sql .= ' LIMIT ' . (int) $offset . ', ' . (int) $limit;
		}

        $sql .= ' ORDER BY `qty` DESC';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);

		if (!is_null($categoryId)) {
			$stmt->bindParam(':category_id', $categoryId);
			$stmt->bindParam(':parent_id', $categoryId);
		}

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new Product_Model_Product();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getOrders(\Application_Model_Abstract_Collection $collection, $userId, $options, $paginator, $currentPageNumber, $itemsPerPage, $searchValue)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = new Zend_Db_Select($db);

		$select->from(array('p' => $this->_getTable()))
				->join(array('o' => 'orders'), null, array('created_at as order_date'))
				->join(array('oi' => 'order_items'), 'o.id = oi.order_id AND p.id = oi.product_id', array('order_id', 'is_received', 'shipping_method_id', 'id as order_item_id'))
				->where('o.user_id != ?', $userId)
				->where('p.user_id  = ?', $userId);

		if (is_string($searchValue) && strlen($searchValue) > 0) {
			$select->where('p.title LIKE ?', "{$searchValue}%");
		}

		$this->updateSelect($select, $options);

		$paginator = Zend_Paginator::factory($select);
		$paginator->setItemCountPerPage($itemsPerPage)
				->setCurrentPageNumber($currentPageNumber);

		foreach ($paginator as $row) {
			$product = new Product_Model_Product();
			$product->addData($row);

			$collection->addItem($product);
		}
	}

// EO getForSales

	public function getForSales(\Application_Model_Abstract_Collection $collection, $userId, $options, $paginator, $currentPageNumber, $itemsPerPage, $searchValue)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = new Zend_Db_Select($db);

		$select->from(array('p' => $this->_getTable()))
				->where('p.user_id = ?', $userId);

		//-----search
		if (is_string($searchValue) && strlen($searchValue) > 0) {
			$select->where('p.title LIKE ?', "{$searchValue}%");
		}

		$this->updateSelect($select, $options);

		$paginator = Zend_Paginator::factory($select);
		$paginator->setItemCountPerPage($itemsPerPage)
				->setCurrentPageNumber($currentPageNumber);

		foreach ($paginator as $row) {
			$product = new Product_Model_Product();
			$product->addData($row);

			$collection->addItem($product);
		}
	}

// EO getForSales

	public function getByCategory($object, $id, $onlyPublished)
	{
		if (!$onlyPublished) {
			$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `category_id` = :id';
		} else {
			$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `category_id` = :id AND `is_published` = 1';
		}


		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($result as $row) {
			$product = new Product_Model_Product();
			$product->addData($row);
			$object->addItem($product);
		}
	}

	public function getAll($collection)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($result as $row) {
			$object = new Product_Model_Product();
			$object->addData($row);

			$collection->addItem($object);
		}
	}

	/* public function sortBy (Application_Model_Abstract_Collection $collection, $sortBy, $direction, $categoryId, $onlyPublished)
	  {
	  if($categoryId){
	  if(!is_array($categoryId) && (int)$categoryId > 0)
	  {
	  if(!$onlyPublished)
	  {
	  $sql = "SELECT * FROM `".$this->_getTable()."` WHERE `category_id` = :cId ORDER BY `{$sortBy}` {$direction}";
	  } else
	  {
	  $sql = "SELECT * FROM `".$this->_getTable()."` WHERE `category_id` = :cId AND `is_published` = 1 ORDER BY `{$sortBy}` {$direction}";
	  }
	  } else
	  {
	  $in = '';

	  foreach($categoryId as $id)
	  {
	  if((int)$id  > 0)
	  {
	  $in.=$id;
	  if(end($categoryId) != $id)
	  {
	  $in.=',';
	  }
	  }
	  }
	  if(empty($in)) throw new Exception(get_class($this).' invalid data for IN(id)');
	  if(!$onlyPublished){
	  $sql = "SELECT * FROM `".$this->_getTable()."` WHERE `category_id` IN({$in}) ORDER BY `{$sortBy}` {$direction}";
	  } else
	  {
	  $sql = "SELECT * FROM `".$this->_getTable()."` WHERE `category_id` IN({$in}) AND `is_published` = 1 ORDER BY `{$sortBy}` {$direction}";
	  }
	  } }else
	  {
	  return;
	  }
	  $stmt = $this->_getConnection()->prepare($sql);

	  if(!is_array($categoryId) && (int)$categoryId > 0) $stmt->bindParam(':cId',  $categoryId);

	  $stmt->execute();
	  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

	  foreach($result as $row)
	  {
	  $object = new Category_Model_Category();
	  $object->addData($row);

	  $collection->addItem($object);

	  } } */

	public function sortBy($collection, $sortBy, $direction, $categoryId, $page, $itemsPerPage, $filters, $onlyPublished)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$userBE = new Application_Model_User_Backend();
		$select = new Zend_Db_Select($db);
		$select->from($this->_getTable());

		if (!is_array($categoryId) && (int) $categoryId > 0) {
			if ($onlyPublished) {
				$select->where('category_id = ?', $categoryId)
						->where('is_published = 1')
						->order($sortBy . ' ' . $direction);
			} else {
				$select->where('category_id = ?', $categoryId)
						->order($sortBy . ' ' . $direction);
			}
		} else {
			if ($onlyPublished) {
				$select->where('category_id IN (?)', $categoryId)
						->where('is_published = 1')
						->order($sortBy . ' ' . $direction);
			} else {
				$select->where('category_id IN (?)', $categoryId)
						->order($sortBy . ' ' . $direction);
			}
		}

//----------------------------|FILTERS
		if (is_array($filters)) {
			foreach ($filters as $filter) {
				$filter->attach($select);
			}
		}

//****************************|FILTERS

		$paginator = Zend_Paginator::factory($select);

		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage($itemsPerPage);

		$collection->setPagesCount($paginator->count());

		foreach ($paginator as $item) {
			$product = new Product_Model_Product();
			$product->addData($item);
			$collection->addItem($product);
		}
	}

	public function searchProducts(Product_Model_Product_Collection $collection, $searchValue, $page, $itemsPerPage, $onlyPublished, $minLength)
	{
		$strlen = new Zend_Validate_StringLength(array('min' => $minLength));
		if ($strlen->isValid($searchValue)) {
			$db = Zend_Db_Table::getDefaultAdapter();
			$userBE = new Application_Model_User_Backend();
			$select = new Zend_Db_Select($db);
			$select->from(
							array('p' => $this->_getTable()))
					->join(array('u' => $userBE->_getTable()), 'p.user_id = u.id', array('full_name as user_name'));

			if ($onlyPublished) {
				$select->where('is_published = ?', '1');
			}
			$select->where('title LIKE ?', "{$searchValue}%")
					->orWhere('u.full_name LIKE ?', "{$searchValue}%")
					->orWhere('description LIKE ?', "%{$searchValue}%");


			$paginator = Zend_Paginator::factory($select);

			$paginator->setCurrentPageNumber($page);
			$paginator->setItemCountPerPage($itemsPerPage);

			$collection->setPagesCount($paginator->count());

			foreach ($paginator as $item) {
				$product = new Product_Model_Product();
				$product->addData($item);
				$collection->addItem($product);
			}
		}
	}

//---------------------------------------------------------------------------------------------------
	protected function _insert(\Application_Model_Abstract $object)
	{

		$sql = 'INSERT INTO `' . $this->_getTable() . '` 
       (
        `user_id`,
        `title`, 
        `description`,
        `price`, 
        `original_price`,
        `category_id`,
        `manufacturer_id`,
        `condition`,
        `material_id`,
        `color_id`,
        `width`,
        `height`,
        `depth`,
        `age`,
        `is_pick_up_available`,
        `is_delivery_available`,
        `available_from`,
        `available_till`,
        `is_published`,
        `created_at`,
		`is_available_for_purchase`,
		`is_visible`,
		`pick_up_address_id`
        )
                  VALUES ( 
                            :user,
                            :title,
                            :description,
                            :price,
                            :original_price,
                            :category_id,
                            :manufacturer_id,
                            :condition,
                            :material_id,
                            :color_id,
                            :width,
                            :height,
                            :depth,
                            :age,
                            :pick_up,
                            :delivery,
                            :available_form,
                            :available_till,  
                            :is_published,
                            :created_at,
							1,
							:is_visible,
							:pick_up_address_id
                             )';
		$stmt = $this->_getConnection()->prepare($sql);

		$userId = $object->getUserId();
		$title = $object->getTitle();
		$description = $object->getDescription();
		$price = $object->getPrice();
		$originalPrice = $object->getOriginalPrice();
		$categoryId = $object->getCategoryId();
		$manufacturerId = $object->getManufacturerId();
		$condition = $object->getCondition();
		$materialId = $object->getMaterialId();
		$colorId = $object->getColorId();
		$width = $object->getWidth();
		$height = $object->getHeight();
		$depth = $object->getDepth();


		$age = $object->getAge();
		$isPickUpAvailable = $object->getIsPickUpAvailable();
		$isDeliveryAvailable = $object->getIsDeliveryAvailable();
		$availableFrom = $object->getAvailableFrom();

		$availableTill = $object->getAvailableTill();
		$isPublished = $object->getIsPublished();
		$createdAt = $object->getCreatedAt();

		$isVisible = $object->getIsVisible();
		$pickUpAddressId = $object->getPickUpAddressId();


		$stmt->bindParam(':user', $userId);
		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':description', $description);
		$stmt->bindParam(':price', $price);
		$stmt->bindParam(':original_price', $originalPrice);
		$stmt->bindParam(':category_id', $categoryId);
		$stmt->bindParam(':manufacturer_id', $manufacturerId);
		$stmt->bindParam(':condition', $condition);
		$stmt->bindParam(':material_id', $materialId);
		$stmt->bindParam(':color_id', $colorId);
		$stmt->bindParam(':width', $width);
		$stmt->bindParam(':height', $height);
		$stmt->bindParam(':depth', $depth);
		$stmt->bindParam(':age', $age);
		$stmt->bindParam(':pick_up', $isPickUpAvailable);
		$stmt->bindParam(':delivery', $isDeliveryAvailable);
		$stmt->bindParam(':available_form', $availableFrom);
		$stmt->bindParam(':available_till', $availableTill);
		$stmt->bindParam(':is_published', $isPublished);
		$stmt->bindParam(':created_at', $createdAt);
		$stmt->bindParam(':is_visible', $isVisible);
		$stmt->bindParam(':pick_up_address_id', $pickUpAddressId);

		$stmt->execute();

		$object->setId($this->_getConnection()->lastInsertId());
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		$sql = "UPDATE `" . $this->_getTable() . "` 
  set
   `user_id` = :user, 
   `title` = :title,
   `description` = :description,
   `price` = :price,
   `original_price` = :original_price,
   `category_id` = :category_id,
   `manufacturer_id` = :manufacturer_id,
   `condition` = :condition,
   `material_id` = :material_id,
   `color_id` = :color_id,
   `width` = :width,
   `height` = :height,
   `depth` = :depth,
   `age` = :age,
   `is_pick_up_available`  = :pickup,
   `is_delivery_available` = :delivery,
   `available_from` = :available_from,
   `available_till` = :available_till,
   `is_published` = :is_published,
   `is_available_for_purchase` = 1,
   `is_visible` = :is_visible,
   `created_at` = :created_at,
		`pick_up_address_id` = :pick_up_address_id
		WHERE `id` = :id";

		$stmt = $this->_getConnection()->prepare($sql);

		$id = $object->getId();
		$userId = $object->getUserId();
		$title = $object->getTitle();
		$description = $object->getDescription();
		$price = $object->getPrice();
		$originalPrice = $object->getOriginalPrice();
		$categoryId = $object->getCategoryId();
		$manufacturerId = $object->getManufacturerId();
		$condition = $object->getCondition();
		$materialId = $object->getMaterialId();
		$colorId = $object->getColorId();
		$width = $object->getWidth();
		$height = $object->getHeight();
		$depth = $object->getDepth();


		$age = $object->getAge();
		$isPickUpAvailable = $object->getIsPickUpAvailable();
		$isDeliveryAvailable = $object->getIsDeliveryAvailable();
		$availableFrom = $object->getAvailableFrom();

		$availableTill = $object->getAvailableTill();
		$isPublished = $object->getIsPublished();
		$createdAt = $object->getCreatedAt();

		$isVisible = $object->getIsVisible();
		$pickUpAddressId = $object->getPickUpAddressId();


		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':user', $userId);
		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':description', $description);
		$stmt->bindParam(':price', $price);
		$stmt->bindParam(':original_price', $originalPrice);
		$stmt->bindParam(':category_id', $categoryId);
		$stmt->bindParam(':manufacturer_id', $manufacturerId);
		$stmt->bindParam(':condition', $condition);
		$stmt->bindParam(':material_id', $materialId);
		$stmt->bindParam(':color_id', $colorId);
		$stmt->bindParam(':width', $width);
		$stmt->bindParam(':height', $height);
		$stmt->bindParam(':depth', $depth);
		$stmt->bindParam(':age', $age);
		$stmt->bindParam(':pickup', $isPickUpAvailable);
		$stmt->bindParam(':delivery', $isDeliveryAvailable);
		$stmt->bindParam(':available_from', $availableFrom);
		$stmt->bindParam(':available_till', $availableTill);
		$stmt->bindParam(':is_published', $isPublished);
		$stmt->bindParam(':created_at', $createdAt);
		$stmt->bindParam(':is_visible', $isVisible);
		$stmt->bindParam(':pick_up_address_id', $pickUpAddressId);
		$stmt->execute();
        
        if (APPLICATION_ENV === 'live') {
            $this->_logUpdate($object);
        }
	}
    
    protected function prepareProduct($product)
    {
        $str = '2 -- ' . date('Y-m-d H:i:s') . " Product Data:\n";
        $str .= 'id: ' . (string) $product->getData('id') . "\n";
        $str .= 'user_id: ' . (string) $product->getData('user_id') . "\n";
        $str .= 'title: ' . (string) $product->getData('title') . "\n";
        $str .= 'is_published: ' . (string) $product->getData('is_published') . "\n";
        $str .= 'isPublished: ' . (int) $product->getIsPublished() . "\n";
        return $str;
    }

    protected function _logUpdate($product)
    {
        $fileName = APPLICATION_PATH . '/log/save-product.log';

        $data = $this->prepareProduct($product) . "\n*******************************************\n\n";

        file_put_contents($fileName, $data, FILE_APPEND);
    }

	public function searchAndFilter(Application_Model_Abstract_Collection $collection, $data, $offset, $limit)
	{
		$query = '%' . str_replace('%', '', strtolower($data['query'])) . '%';

		$sql = 'SELECT * FROM `products` WHERE `products`.`is_published` = 1 AND (`title` LIKE :query1 OR `description` LIKE :query2)';

		if (isset($data['style']) && is_array($data['style']) && count($data['style']) > 0) {
			$sql .= ' AND `category_id` IN (' . implode(',', $data['style']) . ')';
		}

		if (isset($data['color']) && is_array($data['color']) && count($data['color']) > 0) {
			$sql .= ' AND `color_id` IN (' . implode(',', $data['color']) . ')';
		}

		if (isset($data['brand']) && is_array($data['brand']) && count($data['brand']) > 0) {
			$sql .= ' AND `manufacturer_id` IN (' . implode(',', $data['brand']) . ')';
		}

		if (isset($data['condition']) && is_array($data['condition']) && count($data['condition']) > 0) {
			$sql .= ' AND `condition` IN ("' . implode('","', $data['condition']) . '")';
		}

		if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
			$sql .= ' AND `price` >= :min_price';
		}

		if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
			$sql .= ' AND `price` <= :max_price';
		}

		$sql .= ' LIMIT ' . $offset . ', ' . $limit;

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':query1', $query);
		$stmt->bindParam(':query2', $query);

		if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
			$stmt->bindParam(':min_price', $data['min_price']);
		}

		if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
			$stmt->bindParam(':max_price', $data['max_price']);
		}

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new Product_Model_Product();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function filterAndFilter(Application_Model_Abstract_Collection $collection, $data, $offset, $limit)
	{
		$query = $data['query'];

		$sql = 'SELECT `products`.* FROM `products` JOIN `categories`
			ON `products`.`category_id` = `categories`.`id`
			WHERE `products`.`is_published` = 1 AND (`categories`.`parent_id` = :query OR `categories`.`id` = :query2)';

		if (isset($data['style']) && is_array($data['style']) && count($data['style']) > 0) {
			$sql .= ' AND `products`.`category_id` IN (' . implode(',', $data['style']) . ')';
		}

		if (isset($data['color']) && is_array($data['color']) && count($data['color']) > 0) {
			$sql .= ' AND `products`.`color_id` IN (' . implode(',', $data['color']) . ')';
		}

		if (isset($data['brand']) && is_array($data['brand']) && count($data['brand']) > 0) {
			$sql .= ' AND `products`.`manufacturer_id` IN (' . implode(',', $data['brand']) . ')';
		}

		if (isset($data['condition']) && is_array($data['condition']) && count($data['condition']) > 0) {
			$sql .= ' AND `products`.`condition` IN ("' . implode('","', $data['condition']) . '")';
		}

		if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
			$sql .= ' AND `products`.`price` >= :min_price';
		}

		if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
			$sql .= ' AND `products`.`price` <= :max_price';
		}

		$sql .= ' LIMIT ' . $offset . ', ' . $limit;

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':query', $query);
		$stmt->bindParam(':query2', $query);

		if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
			$stmt->bindParam(':min_price', $data['min_price']);
		}

		if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
			$stmt->bindParam(':max_price', $data['max_price']);
		}

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new Product_Model_Product();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function countSearchAndFilterResults($data)
	{
		$query = '%' . str_replace('%', '', strtolower($data['query'])) . '%';

		$sql = 'SELECT COUNT(*) as `count` FROM `products` WHERE `products`.`is_published` = 1 AND (`title` LIKE :query1 OR `description` LIKE :query2)';

		if (isset($data['category']) && is_array($data['category']) && count($data['category']) > 0) {
			$sql .= ' AND `category_id` IN (' . implode(',', $data['category']) . ')';
		}

		if (isset($data['color']) && is_array($data['color']) && count($data['color']) > 0) {
			$sql .= ' AND `color_id` IN (' . implode(',', $data['color']) . ')';
		}

		if (isset($data['manufacturer']) && is_array($data['manufacturer']) && count($data['manufacturer']) > 0) {
			$sql .= ' AND `manufacturer_id` IN (' . implode(',', $data['manufacturer']) . ')';
		}

		if (isset($data['condition']) && is_array($data['condition']) && count($data['condition']) > 0) {
			$sql .= ' AND `condition` IN (' . implode(',', $data['condition']) . ')';
		}

		if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
			$sql .= ' AND `price` >= :min_price';
		}

		if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
			$sql .= ' AND `price` <= :max_price';
		}

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':query1', $query);
		$stmt->bindParam(':query2', $query);

		if (isset($data['min_price']) && (int) $data['min_price'] >= 0) {
			$stmt->bindParam(':min_price', $data['min_price']);
		}

		if (isset($data['max_price']) && (int) $data['max_price'] > 0) {
			$stmt->bindParam(':max_price', $data['max_price']);
		}

		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row) {
			return (int) $row['count'];
		}
		return 0;
	}

	public function getMinPriceByQuery($query)
	{
		$query = '%' . str_replace('%', '', strtolower($query)) . '%';

		$sql = 'SELECT MIN(`price`) as `min_price` FROM `products` WHERE `products`.`is_published` = 1 AND (`title` LIKE :query1 OR `description` LIKE :query2)';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':query1', $query);
		$stmt->bindParam(':query2', $query);

		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row) {
			return $row['min_price'];
		}

		return 0;
	}

	public function getMinPriceByCategory($query)
	{
		/* $sql = 'SELECT MIN( `products`.`price` ) AS `min_price`
		  FROM `products`
		  JOIN `categories` ON `products`.`category_id` = `categories`.`id`
		  WHERE `products`.`category_id` = :category_id1
		  OR `categories`.`parent_id` = :category_id2'; */

		$sql = 'SELECT MIN(`price`) as `min_price` FROM `view_product_prices`
			WHERE `category_id` = :category_id1 OR `parent_id` = :category_id2';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':category_id1', $query);
		$stmt->bindParam(':category_id2', $query);

		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row) {
			return $row['min_price'];
		}

		return 0;
	}

	public function getMaxPriceByQuery($query)
	{
		$query = '%' . str_replace('%', '', strtolower($query)) . '%';

		$sql = 'SELECT MAX(`price`) as `max_price` FROM `products` WHERE `products`.`is_published` = 1 AND (`title` LIKE :query1 OR `description` LIKE :query2)';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':query1', $query);
		$stmt->bindParam(':query2', $query);

		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row) {
			return $row['max_price'];
		}

		return 999999;
	}

	public function getMaxPriceByCategory($query)
	{
		$sql = 'SELECT MAX(`price`) as `max_price` FROM `view_product_prices`
			WHERE `category_id` = :category_id1 OR `parent_id` = :category_id2';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':category_id1', $query);
		$stmt->bindParam(':category_id2', $query);

		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row) {
			return $row['max_price'];
		}

		return 999999;
	}

	public function getAllByUser($collection, $userId)
	{
		$sql = 'SELECT * FROM `products` WHERE `user_id` = :user_id;';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new Product_Model_Product();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getByUserAndCategory($collection, $userId, $categoryId)
	{
		$sql = 'SELECT `products`.*
			FROM `products`
			JOIN `categories` ON `products`.`category_id` = `categories`.`id`
			WHERE `products`.`user_id` = :user_id  AND `products`.`is_published` = 1 AND
			(`categories`.`parent_id` = :category_id1 OR `categories`.`id` = :category_id2)';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);
		$stmt->bindParam(':category_id1', $categoryId);
		$stmt->bindParam(':category_id2', $categoryId);

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new Product_Model_Product();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getPurchase(\Application_Model_Abstract_Collection $collection, $userId, $options, $paginator, $currentPageNumber, $itemsPerPage, $searchValue)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = new Zend_Db_Select($db);

		$select->from(array('p' => $this->_getTable()))
				->join(array('o' => 'orders'), null, array('created_at as order_date'))
				->join(array('oi' => 'order_items'), 'o.id = oi.order_id AND p.id = oi.product_id', array('order_id', 'is_received', 'shipping_method_id', 'id as order_item_id'))
				->where('o.user_id = ?', $userId)
				->where('p.user_id != ?', $userId);

		if (is_string($searchValue) && strlen($searchValue) > 0) {
			$select->where('p.title LIKE ?', "{$searchValue}%");
		}

		$this->updateSelect($select, $options);

		$paginator = Zend_Paginator::factory($select);
		$paginator->setItemCountPerPage($itemsPerPage)
				->setCurrentPageNumber($currentPageNumber);

		foreach ($paginator as $row) {
			$product = new Product_Model_Product();
			$product->addData($row);

			$collection->addItem($product);
		}
	}

	public function getSalesCount($userId)
	{
		/* $sql = "SELECT COUNT(*) as `sales_count` FROM `".$this->_getTable()."` as `p`  
		  JOIN `order_items` as `oi` ON `oi`.`product_id` = `p`.`id` WHERE `p`.`user_id` = :u_id"; */
		$sql = "SELECT COUNT(*) as `sales_count` FROM `" . $this->_getTable() . "` as `p` WHERE `p`.`user_id` = :u_id";
		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':u_id', $userId);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return (int) $result['sales_count'];
	}

	public function getPurchaseCount($userId)
	{
		$sql = "SELECT COUNT(*) as `purchase_count` FROM `" . $this->_getTable() . "` as `p` JOIN `orders` as `o`
                 JOIN `order_items` as `oi` ON `o`.`id` = `oi`.`order_id` AND `p`.`id` = `oi`.`product_id`
                 WHERE `o`.`user_id` = :u_id AND `p`.`user_id` != :u_id1";

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':u_id', $userId);
		$stmt->bindParam(':u_id1', $userId);
		$stmt->execute();

		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return (int) $result['purchase_count'];
	}

	/**
	 * Attach filter and sort conditions. 
	 */
	private function updateSelect($select, $options)
	{
		//-----options 

		if (is_array($options)) {
			$vDigits = new Zend_Validate_Digits();
			//----status
			if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_STATUS])) {
				switch ($options[Product_Model_Product_Collection::FILTER_TYPE_STATUS]) {

					case Product_Model_Product_Collection::FILTER_PUBLISHED:
						$select->where('p.is_published = 1')
								->where('p.is_sold = 0');
						break;

					case Product_Model_Product_Collection::FILTER_NOT_PUBLISHED:
						$select->where('p.is_published = 0')
								->where('p.is_sold = 0');
						break;

					case Product_Model_Product_Collection::FILTER_SOLD:
						$select->where('p.is_sold = 1');
						break;

					case Product_Model_Product_Collection::FILTER_NOT_SOLD:
						$select->where('p.is_sold = 0');
						break;

					case Product_Model_Product_Collection::FILTER_IN_DELIVERY:
						$select->where('oi.is_received = 0');
						if (!key_exists('oi', $select->getPart('from'))) {
							$select->join(array('oi' => 'order_items'), 'p.id = oi.product_id', array('is_received'));
						}

						break;

					case Product_Model_Product_Collection::FILTER_RECEIVED:
						$select->where('oi.is_received = 1');
						if (!key_exists('oi', $select->getPart('from'))) {
							$select->join(array('oi' => 'order_items'), 'p.id = oi.product_id', array('is_received'));
						}

						break;
				}
			}
			//****status

			if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_DATE])) {
				switch ($options[Product_Model_Product_Collection::FILTER_TYPE_DATE]) {
					case Product_Model_Product_Collection::FILTER_MONTH:
						//Timestamps for begin and end of current month                     
						$begin = mktime(0, 0, 0, date('n'), 1, date('Y'));
						$end = mktime(23, 59, 59, date('n'), date('t'), date('Y'));
						$select->where('o.created_at BETWEEN ' . $begin . ' AND ' . $end);
						break;

					case Product_Model_Product_Collection::FILTER_YEAR:
						//Timestamps for begin and end of current year                     
						$begin = mktime(0, 0, 0, 1, 1, date('Y'));
						//$day = (date('L'))?366:365;
						$end = mktime(23, 59, 59, 1, date('z') + 1, date('Y'));
						$select->where('o.created_at BETWEEN ' . $begin . ' AND ' . $end);
						break;
				}
			}
			if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_BETWEEN_DATES])) {
				$timestamps = $options[Product_Model_Product_Collection::FILTER_TYPE_BETWEEN_DATES];

				$begin = (isset($timestamps[0])) ? (int) $timestamps[0] : false;
				$end = (isset($timestamps[1])) ? (int) $timestamps[1] + 86399 : false; //86399 of 86400 seconds in day

				if ($begin !== false) {
					$select->where('o.created_at >= ?', $begin);
				}

				if ($end !== false) {
					$select->where('o.created_at <= ?', $end);
				}
			}
			//------Category
			if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_CATEGORY])) {
				$ctaegoryId = (int) $options[Product_Model_Product_Collection::FILTER_TYPE_CATEGORY];
				$select->where('p.category_id = ?', $ctaegoryId);
			}

			if (isset($options['sort']) && is_array($options['sort'])) {
				$select->order($options['sort']['by'] . ' ' . $options['sort']['direction']);
			}

			//-------BRAND
			if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]) && is_string($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND])) {
				$select->join(array('m' => 'manufacturers'), 'm.id = p.manufacturer_id', array())
						->where('m.title = ?', $options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]);
			} elseif (isset($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]) && is_array($options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]))
				$select->join(array('m' => 'manufacturers'), 'm.id = p.manufacturer_id', array())
						->where('m.title IN (?)', $options[Product_Model_Product_Collection::FILTER_TYPE_BRAND]); {
				
			}


			//----TITLE
			if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_TITLE]) && is_string($options[Product_Model_Product_Collection::FILTER_TYPE_TITLE])) {
				$select->where('p.title LIKE ?', $options[Product_Model_Product_Collection::FILTER_TYPE_TITLE] . '%');
			} elseif (isset($options[Product_Model_Product_Collection::FILTER_TYPE_TITLE]) && is_array($options[Product_Model_Product_Collection::FILTER_TYPE_TITLE])) {
				$select->where('p.title IN (?)', $options[Product_Model_Product_Collection::FILTER_TYPE_TITLE]);
			}
			//----User
			if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_USER])) {
				$select->join(array('u' => 'users'), 'u.id = p.user_id', array('full_name'));
				if ($vDigits->isValid($options[Product_Model_Product_Collection::FILTER_TYPE_USER])) {
					$select->where('u.id = ?', $options[Product_Model_Product_Collection::FILTER_TYPE_USER]);
				} elseif (is_string($options[Product_Model_Product_Collection::FILTER_TYPE_USER])) {
					$select->where('u.full_name LIKE ?', $options[Product_Model_Product_Collection::FILTER_TYPE_USER] . '%');
				}
			}


			//----Order#
			if (isset($options[Product_Model_Product_Collection::FILTER_TYPE_ORDER])) {
				$select->where('o.id = ?', $options[Product_Model_Product_Collection::FILTER_TYPE_ORDER]);
			}
		}//****is_array options
//die($select);
	}

	public function getProducts($collection, $params)
	{
		$bindParams = array();
		$where = array();

		$sql = 'SELECT * FROM `view_products` ';

		if (isset($params['title'])) {
			$where[] = '(`title` LIKE :title)';
			$bindParams[':title'] = '%' . str_replace('%', '', $params['title']) . '%';
		}
		
		if (isset($params['user_id'])) {
			$where[] = '(`user_id` = :user_id)';
			$bindParams[':user_id'] = $params['user_id'];
		}
		
		if (isset($params['is_published'])) {
			$where[] = '(`is_published` = :is_published)';
			$bindParams[':is_published'] = $params['is_published'];
		}
        
        if (isset($params['is_approved'])) {
			$where[] = '(`is_approved` = :is_approved)';
			$bindParams[':is_approved'] = $params['is_approved'];
		}
		
		if (isset($params['category_id'])) {
			$where[] = '(`category_id` = :category_id OR `parent_category_id` = :parent_category_id)';
			$bindParams[':category_id'] = $params['category_id'];
			$bindParams[':parent_category_id'] = $params['category_id'];
		}
		
		if (isset($params['subcategory_id'])) {
			$where[] = '(`category_id` = :subcategory_id)';
			$bindParams[':subcategory_id'] = $params['subcategory_id'];
		}
		
		if (isset($params['subcategories'])) {
			$subcategories = array();
			foreach ($params['subcategories'] as $key => $value) {
				$key = ':subcategory_' . $key;
				$subcategories[] = $key;
				$bindParams[$key] = $value;
			}
			$where[] = sprintf('(`category_id` IN (%s))', implode(', ', $subcategories));
		}

		if (isset($params['min_price'])) {
			$where[] = '(`price` >= :min_price)';
			$bindParams[':min_price'] = $params['min_price'];
		}

		if (isset($params['max_price'])) {
			$where[] = '(`price` <= :max_price)';
			$bindParams[':max_price'] = $params['max_price'];
		}

		if (isset($params['conditions'])) {
			$conditions = array();
			foreach ($params['conditions'] as $key => $value) {
				$key = ':condition_' . $key;
				$conditions[] = $key;
				$bindParams[$key] = $value;
			}
			$where[] = sprintf('(`condition` IN (%s))', implode(', ', $conditions));
		}
		
		if (isset($params['colors'])) {
			$colors = array();
			foreach ($params['colors'] as $key => $value) {
				$key = ':color_' . $key;
				$colors[] = $key;
				$bindParams[$key] = $value;
			}
			$where[] = sprintf('(`color_id` IN (%s))', implode(', ', $colors));
		}
		
		if (isset($params['manufacturers'])) {
			$manufacturers = array();
			foreach ($params['manufacturers'] as $key => $value) {
				$key = ':manufacturer_' . $key;
				$manufacturers[] = $key;
				$bindParams[$key] = $value;
			}
			$where[] = sprintf('(`manufacturer_id` IN (%s))', implode(', ', $manufacturers));
		}
		
		if (count($where)) {
			$sql .= 'WHERE ' . implode(' AND ', $where);
		}

		if (isset($params['offset'], $params['limit'])) {
			$sql .= ' LIMIT :offset, :limit';
			$bindParams[':offset'] = $params['offset'];
			$bindParams[':limit'] = $params['limit'];
		}

		$stmt = $this->_getConnection()->prepare($sql);
		
		$stmt->execute($bindParams);
		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if ($rows) {
			foreach ($rows as $row) {
				$item = new Product_Model_Product2();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

}
