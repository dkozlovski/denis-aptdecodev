<?php

/**
 * Class Product_Model_Alert
 * @method Product_Model_Alert_Backend _getbackend()
 * @method int getUserId()
 * @method string getEmails()
 * @method int getCategoryId()
 * @method int getCollectionId()
 * @method string getSubcategoryIds()
 * @method string getConditions()
 * @method string getColorIds()
 * @method string getBrandIds()
 * @method float getMinPrice()
 * @method float getMaxPrice()
 * @method string getDeleteCode()
 * @method int getSendCount()
 * @method int getCreatedAt()
 * @method int getLastSendDate()
 * @method int getIsDelete()
 *
 * @method array getEmailsAsArray();
 * @method array getSubcategoryIdsAsArray()
 * @method array getConditionsAsArray()
 * @method array getColorIdsAsArray()
 * @method array getBrandIdsAsArray()
 *
 * @method Product_Model_Alert setUserId(int $userId)
 * @method Product_Model_Alert setEmails(array $value)
 * @method Product_Model_Alert setCategoryId(string $value)
 * @method Product_Model_Alert setCollectionId(string $value)
 * @method Product_Model_Alert setSubcategoryIds(string $value)
 * @method Product_Model_Alert setConditions(string $value)
 * @method Product_Model_Alert setColorIds(string $value)
 * @method Product_Model_Alert setBrandIds(array $value)
 * @method Product_Model_Alert setMinPrice(int $value)
 * @method Product_Model_Alert setMaxPrice(int $value)
 * @method Product_Model_Alert setDeleteCode(string $value)
 * @method Product_Model_Alert setSendCount(int $value)
 * @method Product_Model_Alert setCreatedAt(int $value)
 * @method Product_Model_Alert setLastSendDate(int $value)
 * @method Product_Model_Alert setIsDelete(int $value)
 *
 * @method Product_Model_Alert setEmailsAsArray(array $value)
 * @method Product_Model_Alert setSubcategoryIdsAsArray(array $value)
 * @method Product_Model_Alert setConditionsAsArray(array $value)
 * @method Product_Model_Alert setColorIdsAsArray(array $value)
 * @method Product_Model_Alert setBrandIdsAsArray(array $value)

 */
class Product_Model_Alert extends Application_Model_Abstract
{

    protected $_backendClass   = 'Product_Model_Alert_Backend';
    protected $_category       = null;
    protected $_collection     = null;
    protected $_user           = null;
    protected $_selectedBrands = array();

    protected function _beforeSave()
    {
        if (!$this->getId()) {
            $this->setDeleteCode(md5(uniqid('alert') . time()))
                    ->setCreatedAt(time())
                    ->setSendCount(0)
                    ->setIsDelete(0);
        }
    }

    public function __call($method, $args)
    {
        if (substr($method, -7, 7) == 'AsArray') {
            $method = substr($method, 0, -7);
            if (substr($method, 0, 3) == 'set' && isset($args[0]) && is_array($args[0])) {
                $args[0] = implode(',', $args[0]);
            }

            $return = parent::__call($method, $args);

            if (substr($method, 0, 3) == 'get') {
                $return = $return ? explode(',', $return) : array();
            }
            return $return;
        }
        return parent::__call($method, $args);
    }

    public function getCategory()
    {
        if ($this->_category === null) {
            $this->_category = new Category_Model_Category($this->getCategoryId());
        }
        return $this->_category;
    }

    public function getCollection()
    {
        if ($this->_collection === null) {
            $this->_collection = new Application_Model_Set($this->getCollectionId());
        }
        return $this->_collection;
    }

    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = new User_Model_User($this->getUserId());
        }
        return $this->_user;
    }

    public function getDeleteByCodeUrl()
    {
        return Ikantam_Url::getUrl('product/alert/delete-by-code', array('code' => $this->getDeleteCode()));
    }

    public function getRestoreByCodeUrl()
    {
        return Ikantam_Url::getUrl('product/alert/restore-by-code', array('code' => $this->getDeleteCode()));
    }

    public function getSelectedBrandsCollection()
    {
        if ($this->_selectedBrands === array()) {
            if ($this->getBrandIds()) {
                $this->_selectedBrands = new Application_Model_Manufacturer_Collection();
                $this->_selectedBrands->findByIds($this->getBrandIdsAsArray());
            }
        }
        return $this->_selectedBrands;
    }

    public function setDataFromRequest(\Zend_Controller_Request_Abstract $request)
    {

        $this->setCategoryId($request->getParam('category_id') > 0 ? (int) $request->getParam('category_id') : null)
                ->setCollectionId($request->getParam('collection_id') > 0 ? (int) $request->getParam('collection_id') : null)
                ->setEmailsAsArray($request->getParam('emails'))
                ->setSubcategoryIdsAsArray($request->getParam('subcategory_ids'))
                ->setConditionsAsArray($request->getParam('conditions'))
                ->setColorIdsAsArray($request->getParam('color_ids'))
                ->setBrandIdsAsArray($request->getParam('brand_ids'));


        if (strlen($request->getParam('min_price')) && $request->getParam('min_price') >= 0) {
            $this->setMinPrice((int) $request->getParam('min_price'));
        } else {
            $this->setMinPrice(null);
        }

        if ($request->getParam('max_price') && $request->getParam('max_price') >= 0) {
            $this->setMaxPrice((int) $request->getParam('max_price'));
        } else {
            $this->setMaxPrice(null);
        }

        return $this;
    }

    public function setDataFromForm(\Zend_Form $form)
    {
        $this->setCategoryId(($form->getValue('category_id') > 0) ? (int) $form->getValue('category_id') : null)
                ->setCollectionId(($form->getValue('collection_id') > 0) ? (int) $form->getValue('collection_id') : null)
                ->setSubcategoryIdsAsArray($form->getValue('subcategory_ids') ? $form->getValue('subcategory_ids') : null)
                ->setConditionsAsArray($form->getValue('conditions') ? $form->getValue('conditions') : null)
                ->setColorIdsAsArray($form->getValue('color_ids') ? $form->getValue('color_ids') : null)
                ->setBrandIdsAsArray($form->getValue('brand_ids') ? $form->getValue('brand_ids') : null);

        $emails = array();
        foreach ($form->getValue('emails')as $_email) {
            if ($_email) {
                $emails[] = $_email;
            }
        }
        $this->setEmailsAsArray($emails);


        if (strlen($form->getValue('min_price')) && $form->getValue('min_price') >= 0) {
            $this->setMinPrice((int) $form->getValue('min_price'));
        } else {
            $this->setMinPrice(null);
        }

        if ($form->getValue('max_price') && $form->getValue('max_price') >= 0) {
            $this->setMaxPrice((int) $form->getValue('max_price'));
        } else {
            $this->setMaxPrice(null);
        }

        return $this;
    }

    public function getProductsCollection()
    {
        $collection = new Product_Model_Product_Collection();
        if ($this->getProductIds()) {
            $collection->getByIds($this->getProductIdsAsArray());
        }
        return $collection;
    }

    public function sendEmails($debug = false)
    {

        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/product/views/scripts'));

        $view->alert = $this;

        $output = $view->render('email_product_alert.phtml');

        if ($this->getCategoryId()) {
            $name = $this->getCategory()->getTitle();
        } else {
            $name = $this->getCollection()->getName();
        }

        $toSend = array(
            'email'   => $this->getEmailsAsArray(),
            'subject' => 'We have more ' . $name . ' for you!',
            'body'    => $output);

        $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');

        try {
            $mail->sendCopy($output);
            $mail->send();

            $this->setSendCount($this->getSendCount() + 1);
            if (!$debug) {
                $this->setLastSendDate(time());
            }
            $this->save();
            $this->saveSentItems();
        } catch (Exception $e) {
            
        }
    }

    public static function getParamsFromRequest(\Zend_Controller_Request_Abstract $request)
    {
        return array(
            'category_id'     => $request->getParam('query') > 0 ? (int) $request->getParam('query') : null,
            'subcategory_ids' => $request->getParam('categories'),
            'min_price'       => $request->getParam('min_price'),
            'max_price'       => $request->getParam('max_price'),
            'conditions'      => $request->getParam('conditions'),
            'brand_ids'       => $request->getParam('brands'),
            'color_ids'       => $request->getParam('colors')
        );
    }

    public static function getShortConditionsLabel()
    {
        return array(
            'new'          => 'New',
            'like-new'     => 'Like New',
            'good'         => 'Good',
            'satisfactory' => 'Satisfactory',
            'age-worn'     => 'Age-worn'
        );
    }

    public function renderCriteria()
    {

        $htmlArray[] = $this->getCategoryTitle() ? $this->getCategoryTitle() : $this->getCollectionTitle();

        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));


        if ($this->getSubcategoryTitles()) {
            $htmlArray[] = 'Styles: <span style="color: #999">' . $this->getSubcategoryTitles() . '</span>';
        }

        if ($this->getMinPrice() !== null || $this->getMaxPrice()) {
            if ($this->getMinPrice() !== null && $this->getMaxPrice()) {
                $htmlArray[] = 'Price: <span style="color: #999">'
                        . $view->currency($this->getMinPrice())
                        . ' - '
                        . $view->currency($this->getMaxPrice())
                        . '</span>';
            } elseif ($this->getMinPrice() !== null) {
                $htmlArray[] = 'Price: <span style="color: #999">From ' . $view->currency($this->getMinPrice()) . '</span>';
            } elseif ($this->getMaxPrice()) {
                $htmlArray[] = 'Price: <span style="color: #999">To ' . $view->currency($this->getMaxPrice()) . '</span>';
            }
        }

        if ($this->getColorTitles()) {
            $htmlArray[] = 'Color: <span  style="color: #999">' . $this->getColorTitles() . '</span>';
        }

        if ($this->getBrandTitles()) {
            $htmlArray[] = 'Brand: <span  style="color: #999">' . $this->getBrandTitles() . '</span>';
        }

        if ($this->getConditions()) {
            $conditions     = self::getShortConditionsLabel();
            $conditionTitle = array();
            foreach ($this->getConditionsAsArray() as $_condition) {
                if (isset($conditions[$_condition])) {
                    $conditionTitle[] = $conditions[$_condition];
                }
            }
            $htmlArray[] = 'Condition: <span  style="color: #999">' . implode(', ', $conditionTitle) . '</span>';
        }
        return implode(', ', $htmlArray);
    }

    public function saveSentItems()
    {
        $this->_getbackend()->saveSentItems($this);
        return $this;
    }

}