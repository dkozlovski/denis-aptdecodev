<?php

class Product_Model_Image_Collection extends Application_Model_Abstract_Collection
    implements Ikantam_Filter_Flex_Interface
{
	protected $_backendClass = 'Product_Model_Image_Backend';
    protected $_itemObjectClass = 'Product_Model_Image';
	
	public function getByProductId($id)
	{
		$this->_getBackend()->getByProductId($this, $id);
		return $this;
	}
    
    public function getExtraByProductId($id)
	{
		$this->_getBackend()->getExtraByProductId($this, $id);
		return $this;
	}
	
	public function getAll()
	{
		$this->_getBackend()->getAll($this);
		return $this;
	}
	
	protected function _getBackend()
	{
		return new $this->_backendClass();
	}

    /**
     * Returns a filter related with collection.
     * @return  Product_Model_Image_FlexFilter
     */
    public function  getFlexFilter()
    {
        return new \Product_Model_Image_FlexFilter($this);
    }
}