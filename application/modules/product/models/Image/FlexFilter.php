<?php
/**
 * Author: Alex P.
 * Date: 22.08.14
 * Time: 16:59
 */

/**
 * @method Product_Model_Image_FlexFilter id (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_id (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter product_id (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_product_id (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter path (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_path (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter is_main (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_is_main (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter is_locked (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_is_locked (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter is_visible (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_is_visible (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter s3_path (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_s3_path (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter default_angle (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_default_angle (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_90_exists (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_180_exists (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_270_exists (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter is_uploaded (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_is_uploaded (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter description (string $operator, mixed $value)
 * @method Product_Model_Image_FlexFilter or_description (string $operator, mixed $value)
 * @method Product_Model_Image_Collection apply(int $limit = null, int $offset = null)
 */
class Product_Model_Image_FlexFilter extends Ikantam_Filter_Flex
{
    protected $_acceptClass = 'Product_Model_Image_Collection';

    protected $_joins = array(
    );

    protected $_select = array(
    );

    protected $_rules = array(
    );
} 