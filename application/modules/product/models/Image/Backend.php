<?php

class Product_Model_Image_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'product_images';
	protected $_itemClass = 'Product_Model_Image';

	public function getByProductId($collection, $id)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `product_id` = :id AND `is_main` = 0 AND `is_visible` = 1;';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (count($rows)) {
			foreach ($rows as $row) {
				$item = new $this->_itemClass();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}
    
    public function getExtraByProductId($collection, $id)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `product_id` = :id';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (count($rows)) {
			foreach ($rows as $row) {
				$item = new $this->_itemClass();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getMainByProductId($image, $id)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` 
			WHERE `product_id` = :id AND `is_main` = 1 AND `is_visible` = 1;';
		
		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			$image->addData($row);
		}
	}

	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = "INSERT INTO `" . $this->_getTable() . "` 
			(`product_id`, `path`, `is_main`, `is_locked`, `is_visible`, `s3_path`, `is_uploaded`, `description`)
			VALUES (:product_id, :path, :is_main, :is_locked, :is_visible, :s3_path, :is_uploaded, :description)";
		
		$stmt = $this->_getConnection()->prepare($sql);
		
		$productId = $object->getProductId();
		$path = $object->getPath();
		$isMain = $object->getIsMain();
        $isLocked = $object->getIsLocked();
		$isVisible = $object->getIsVisible();
        $s3Path = $object->getS3Path();
        $isUploaded = $object->getIsUploaded();
        $description = $object->getDescription();
		
		$stmt->bindParam(':product_id', $productId);
		$stmt->bindParam(':path', $path);
		$stmt->bindParam(':is_main', $isMain);
        $stmt->bindParam(':is_locked', $isLocked);
		$stmt->bindParam(':is_visible', $isVisible);
        $stmt->bindParam(':s3_path', $s3Path);
        $stmt->bindParam(':is_uploaded', $isUploaded);
        $stmt->bindParam(':description', $description);
		
		$stmt->execute();
		
		$object->setId($this->_getConnection()->lastInsertId());
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		$sql = "UPDATE `" . $this->_getTable() . "` 
			SET `product_id` = :product_id, `path` = :path, `is_main` = :is_main, `is_locked` = :is_locked,
            `is_visible` = :is_visible, `s3_path` = :s3_path, `is_uploaded` = :is_uploaded,
            `90_exists` = :90_exists, `180_exists` = :180_exists, `270_exists` = :270_exists,
            `default_angle` = :default_angle, `description` = :description
			WHERE `id` = :id";
		
		$stmt = $this->_getConnection()->prepare($sql);
		
		$id = $object->getId();
		$productId = $object->getProductId();
		$path = $object->getPath();
		$isMain = $object->getIsMain();
        $isLocked = $object->getIsLocked();
		$isVisible = $object->getIsVisible();
        $s3Path = $object->getS3Path();
		$isUploaded = $object->getIsUploaded();
        $_90Exists = $object->get90Exists();
        $_180Exists = $object->get180Exists();
        $_270Exists = $object->get270Exists();
        $defaultAngle = $object->getDefaultAngle();
        $description = $object->getDescription();
        
		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':product_id', $productId);
		$stmt->bindParam(':path', $path);
		$stmt->bindParam(':is_main', $isMain);
        $stmt->bindParam(':is_locked', $isLocked);
		$stmt->bindParam(':is_visible', $isVisible);
        $stmt->bindParam(':s3_path', $s3Path);
        $stmt->bindParam(':is_uploaded', $isUploaded);
        $stmt->bindParam(':90_exists', $_90Exists);
        $stmt->bindParam(':180_exists', $_180Exists);
        $stmt->bindParam(':270_exists', $_270Exists);
        $stmt->bindParam(':default_angle', $defaultAngle);
        $stmt->bindParam(':description', $description);
		
		$stmt->execute();
	}
	
	public function setDefaultImage($productId, $imageId)
	{
		$sql = "UPDATE `" . $this->_getTable() . "` 
			SET `is_main` = 0 WHERE `product_id` = :product_id";
		
		$stmt = $this->_getConnection()->prepare($sql);
		
		$stmt->bindParam(':product_id', $productId);
		$stmt->execute();
		
		/////
		
		$sql2 = "UPDATE `" . $this->_getTable() . "` 
			SET `is_main` = 1 WHERE `product_id` = :product_id AND `id` = :id";

		$stmt2 = $this->_getConnection()->prepare($sql2);
		$stmt2->bindParam(':product_id', $productId);
		$stmt2->bindParam(':id', $imageId);

		$stmt2->execute();
	}

}