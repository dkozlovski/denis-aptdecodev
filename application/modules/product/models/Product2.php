<?php

/**
 * Product model
 * 
 * @method int getUserId()
 * @method string getTitle()
 * @method string getDescription()
 * @method float getPrice()
 * @method float getOriginalPrice()
 * @method int getCategoryId()
 * @method int getManufacturerId()
 * @method int getConditionId()
 * @method int getMaterialId()
 * @method int getColorId()
 * @method float getWidth()
 * @method float geHeight()
 * @method float getDepth()
 * @method int getAge()
 * @method int getAvailableFrom()
 * @method int getAvailableTill()
 * @method int getCreatedAt()
 * @method boolean getIsPublished()
 * @method boolean getIsSold()
 * @method boolean getIsVisible()
 */
class Product_Model_Product2 extends Application_Model_Abstract
{

	protected $_seller;
	protected $_color;
	protected $_material;
	protected $_manufacturer;
	protected $_category;

	/**
	 * Get product's seller
	 * 
	 * @return Application_Model_User
	 */
	public function getSeller()
	{
		if (!$this->_seller) {
			$this->_seller = new Application_Model_User($this->getUserId());
		}
		return $this->_seller;
	}

	/**
	 * Set product's seller
	 * 
	 * @param Application_Model_User $user
	 * @return Product_Model_Product2
	 */
	public function setSeller(Application_Model_User $user)
	{
		$this->_seller = $user;
		$this->setUserId($user->getId());
		return $this;
	}

	/**
	 * Get product's color
	 * 
	 * @return Application_Model_Color
	 */
	public function getColor()
	{
		if (!$this->_color) {
			$this->_color = new Application_Model_Color($this->getColorId());
		}
		return $this->_color;
	}

	/**
	 * Set product's color
	 * 
	 * @param Application_Model_Color $color
	 * @return Product_Model_Product2
	 */
	public function setColor(Application_Model_Color $color)
	{
		$this->_color = $color;
		$this->setColorId($color->getId());
		return $this;
	}

	/**
	 * Get products manufacturer
	 * 
	 * @return Application_Model_Manufacturer
	 */
	public function getManufacturer()
	{
		if (!$this->_manufacturer) {
			$this->_manufacturer = new Application_Model_Manufacturer($this->getManufacturerId());
		}
		return $this->_manufacturer;
	}

	/**
	 * Set products manufacturer
	 * 
	 * @param Application_Model_Manufacturer $manufacturer
	 * @return Product_Model_Product2
	 */
	public function setManufacturer(Application_Model_Manufacturer $manufacturer)
	{
		$this->_manufacturer = $manufacturer;
		$this->setManufacturerId($manufacturer->getId());
		return $this;
	}

	/**
	 * Get product's material
	 * 
	 * @return Application_Model_Material
	 */
	public function getMaterial()
	{
		if (!$this->_material) {
			$this->_material = new Application_Model_Material($this->getMaterialId());
		}
		return $this->_material;
	}

	/**
	 * set product's material
	 * 
	 * @param Application_Model_Material $material
	 * @return Product_Model_Product2
	 */
	public function setMaterial(Application_Model_Material $material)
	{
		$this->_material = $material;
		$this->setMaterialId($material->getId());
		return $this;
	}

	/**
	 * Get product's category
	 * 
	 * @return Category_Model_Category
	 */
	public function getCategory()
	{
		if (!$this->_category) {
			$this->_category = new Category_Model_Category($this->getCategoryId());
		}
		return $this->_category;
	}

	/**
	 * Set product's category
	 * 
	 * @param Category_Model_Category $category
	 * @return Product_Model_Product2
	 */
	public function setCategory(Category_Model_Category $category)
	{
		$this->_category = $category;
		$this->setCategoryId($category->getId());
		return $this;
	}

	public function getPickUpAddress()
	{
		return new User_Model_Address($this->getPickUpAddressId());
	}

	/**
	 * Get product's page URL
	 * 
	 * @return string
	 */
	public function getProductUrl()
	{
        if ($this->getPageUrl()) {
            return Ikantam_Url::getUrl('catalog/' . $this->getPageUrl());
        }
        
		return Ikantam_Url::getUrl('product/view/index', array('id' => $this->getId()));
	}

	public function getEditUrl()
	{
		return Ikantam_Url::getUrl('product/edit/' . $this->getId());
	}

	public function getAddToCartUrl()
	{
		return Ikantam_Url::getUrl('cart/index/add', array('id' => $this->getId()));
	}

	public function getAddToCartUrlAjax()
	{
		return Ikantam_Url::getUrl('cart/index/ajax-add', array('id' => $this->getId()));
	}

	public function getRemoveFromCartUrl()
	{
		return Ikantam_Url::getUrl('cart/index/remove', array('id' => $this->getId()));
	}

	public function getAddToWishlistUrl()
	{
		return Ikantam_Url::getUrl('wishlist/index/add', array('id' => $this->getId()));
	}

	public function getRemoveFromWishlistUrl()
	{
		return Ikantam_Url::getUrl('wishlist/index/remove', array('id' => $this->getId()));
	}

	public function getProductLink()
	{
		return '<a href="' . $this->getProductUrl() . '">' . $this->getTitle() . '</a>';
	}

	public function getUserLink()
	{
		$user = new Application_Model_User($this->getUserId());
		return $user->getViewProfileLink();
	}

	public function getConditionLabel()
	{
		switch ($this->getCondition()) {
			case 'new':
				return 'New';
				break;

			case 'good':
				return 'Good';
				break;

			case 'satisfactory':
				return 'Satisfactory';
				break;

			case 'age-worn':
				return 'Age-worn';
				break;

			default:
				return null;
				break;
		}
	}

	public function getAgeLabel()
	{
		switch ($this->getAge()) {
			case 0:
				return '<1';
				break;
			case 1:
				return '1';
				break;
			case 2:
				return '2';
				break;
			case 3:
				return '3';
				break;
			case 4:
				return '4';
				break;
			case 5:
				return '5+';
				break;
			default:
				return null;
				break;
		}
	}

	/**
	 * 
	 * @return \Product_Model_Image
	 */
	public function getMainImage($visible = false)
	{
		$image = new Product_Model_Image();
		$image->getMainByProductId($this->getId());

		if (!$image->getId()) {

			if ($this->getAllImages($visible)->getSize() > 0) {
				$image = $this->getAllImages($visible)->getFirstItem();
				$image->setIsMain(1)->save();
			}

			return $image;
		}
		return $image;
	}

	/**
	 * 
	 * @return \Product_Model_Image_Collection
	 */
	public function getAllImages($visible = false)
	{
		$collection = new Product_Model_Image_Collection();
		$collection->getByProductId($this->getId(), $visible);
		return $collection;
	}

	public function getMainImageUrl($type = null)
	{
		if ($type == 'medium') {
			return Ikantam_Url::getPublicUrl('upload/products/medium/' . $this->getMainImage()->getPath());
		}
		if ($type == 'squared') {
			return Ikantam_Url::getPublicUrl('upload/products/squared/' . $this->getMainImage()->getPath());
		}
		return Ikantam_Url::getPublicUrl('upload/products/' . $this->getMainImage()->getPath());
	}

}
