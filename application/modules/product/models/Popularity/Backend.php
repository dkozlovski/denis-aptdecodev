<?php
class Product_Model_Popularity_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'product_popularity';
    protected $_fields_ = array('id', 'product_id', 'type', 'count');
    
    public function getIfExist (\Product_Model_Popularity $object, $product_id, $type)
    {
        $stmt = $this->_prepareSql("SELECT * FROM `".$this->_getTable()."` WHERE `product_id` = :product_id AND `type` = :type LIMIT 1");
        $stmt->bindValue(':product_id', (int)$product_id, PDO::PARAM_INT);
        $stmt->bindValue(':type', $type, PDO::PARAM_STR);
        $stmt->execute();
        
        $object->setData($stmt->fetch(PDO::FETCH_ASSOC));
        
        return $object;
    }
    
    protected function _insert(\Application_Model_Abstract  $object)
    { 
        $this->runStandartInsert(array('product_id', 'type', 'count'), $object);
    }
    protected function _update(\Application_Model_Abstract $object)
    {
        $this->runStandartUpdate(array('product_id', 'type', 'count'), $object);
    }
    
    
     
}