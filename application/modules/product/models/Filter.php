<?php
    
    class Product_Model_Filter
{
    
        const FILTER_BY_PRICE     = 'price';
        const FILTER_BY_CONDITION = 'condition';
        const FILTER_BY_BRAND     = 'manufacturer_id';
        const FILTER_BY_COLOR     = 'color_id';
        
        const ERR_TYPE  = 'Invalid filter type.';
        const ERR_PARAM = 'Mismatch parameters with filter type.';  

    protected $_type = null;
    protected $_data = array(
                            'min'=>null,
                            'max'=>null,
                            'conditions'=>null,
                            'brands'=>null,
                            'colors'=>null,
                            );//data
    
    public function __construct ($type, $params = null)
    {
        switch($type)
         {
            case self::FILTER_BY_PRICE:
            break;
            
            case self::FILTER_BY_CONDITION:
            break;
            
            case self::FILTER_BY_BRAND:
            break;
            
            case self::FILTER_BY_COLOR:
            break;
            
            default:
            $this->exception(self::ERR_TYPE);
            break;                        
         }
         
         $this->_type = $type;
         $this->setParams($params);         
    }
    
    private function exception($message = null)
    {
       throw new Exception(get_class($this).':'.$message); 
    }
    
    public function setParams ($params)
    {
        if(is_array($params))
        {
            foreach($params as $key => $value)
            {
                if(key_exists($key,$this->_data))
                {
                    $this->_data[$key] = $value; 
                }
            }
        } 
        return $this;
    }
    
    public function attach(Zend_Db_Select $select)
    {
                switch($this->_type)
         {
            case self::FILTER_BY_PRICE:
                if(is_null($this->_data['min'])  && is_null($this->_data['max'])) $this->exception(self::ERR_PARAM);
                if($this->_data['min'] > $this->_data['max']) $this->exception('min > max');
                
                if(!is_null($this->_data['min'])) $select->where(self::FILTER_BY_PRICE.' >= ?',(float)$this->_data['min']);
                if(!is_null($this->_data['max'])) $select->where(self::FILTER_BY_PRICE.' <= ?',(float)$this->_data['max']);
            break;
            
            case self::FILTER_BY_CONDITION:
            if(!$this->_data['conditions'] && !is_array($this->_data['conditions'])) $this->exception(self::ERR_PARAM);
                $select->where('`'.self::FILTER_BY_CONDITION.'` IN(?)',$this->_data['conditions']);                
            break;
            
            case self::FILTER_BY_BRAND:
            if(!$this->_data['brands'] && !is_array($this->_data['brands'])) $this->exception(self::ERR_PARAM);
            $select->where(self::FILTER_BY_BRAND.' IN(?)', $this->_data['brands']);
            break;
            
            case self::FILTER_BY_COLOR:
            if(!$this->_data['colors'] && !is_array($this->_data['colors'])) $this->exception(self::ERR_PARAM);
            $select->where(self::FILTER_BY_COLOR.' IN(?)', $this->_data['colors']);            
            break;
                      
         }
        
    
    }   
        
        
}