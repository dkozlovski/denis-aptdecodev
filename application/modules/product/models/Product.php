<?php

use \Product_Model_Product_Collection as Collection;

/**
 * Product model
 *
 * @method int getUserId()
 * @method string getTitle()
 * @method string getDescription()
 * @method float getPrice()
 * @method float getOriginalPrice()
 * @method float getOldPrice()
 * @method int getCategoryId()
 * @method int getManufacturerId()
 * @method string getCondition()
 * @method int getMaterialId()
 * @method int getColorId()
 * @method float getWidth()
 * @method float getHeight()
 * @method float getDepth()
 * @method int getAge()
 * @method int getAvailableFrom()
 * @method int getAvailableTill()
 * @method int getCreatedAt()
 * @method int getUpdatedAt()
 * @method boolean getIsPickUpAvailable()
 * @method boolean getIsDeliveryAvailable()
 * @method boolean getIsAvailableForPurchase()
 * @method int getIsVisible()
 * @method int getPickUpAddressId()
 * @method int getIsApproved()
 * @method string getPageUrl()
 * @method int getExpireAt()
 * @method int getIsRemindedAboutExpire()
 * @method int getQty()
 * @method string getSellingType()
 * @method int getNumberOfItemsInSet()
 * @method int getShippingCover()
 * @method int getPetInHome()
 * @method int getIsSmokeFree()
 * @method int getIsVerified()
 * @method int getIsManagePriceAllowed()
 * @method int getAdminEditedPrice()
 * @method int getApprovedAt()
 */
class Product_Model_Product extends Application_Model_Abstract implements
Application_Model_Event_Dispatcher_AwareInterface, Application_Interface_ObjectStorage_SymbolTable, Ikantam_EAV_Interface_Entity
{

    protected $_backendClass        = 'Product_Model_Product_Backend';
    protected $_seller;
    protected $_homeBackendClass    = 'Product_Model_Home_Backend';
    protected $_homeBackend         = null;
    protected $_expiryPeriod        = 15;
    protected $_fields_             = array('price', 'name', 'description', 'title');
    protected $_popularity          = array();
    protected static $_events;
    protected $_eventDispatcher;
    protected $_symbolTableName;
    protected static $_locatedInNYC = array('N/A' => false);

    /**
     * Describes EAV fields for products
     * @see $this::setEAVAttributeValue
     *
     * @var array
     */
    private $eavAttributes = array(
        'notification_manage_pricing_to_seller_sent' => 'smallint', // when admin lowered the price
        'price_lowered_by_suggestion'                => 'smallint', // when seller lowered the price by notification
        'exposed_cat'                                => 'smallint', // whether item has been exposed to cat
        'exposed_dog'                                => 'smallint', // whether item has been exposed to dog
        'has_scratches'                              => 'smallint', // whether item has scratches
        'initial_price'                              => 'decimal',
        // Assigns when product posted to Google Shopping. Used to update or delete from Google
        'google_rest_id'                             => 'varchar',
        // Used to mark product as needs to be updated on google (batch requests)
        'google_shopping_content_delayed_update'     => 'smallint',
    );

    /**
     * Contains EAV attributes which was set by using setEAVAttribute method
     * All these attributes will be saved if the "save" method is called
     *
     * @var array
     */
    protected $settedEavAttributes = array();

    const SHIPPING_DELIVERY = 'delivery';
    const SHIPPING_PICK_UP  = 'pick-up';

    /**
     * Represents "individual item" selling type
     */
    const SELLING_TYPE_INDIVIDUAL = 'individual_item';

    /**
     * Represents "set" selling type
     */
    const SELLING_TYPE_SET = 'set';

    /**
     * Get event dispatcher instance
     *
     * @return Application_Model_Event_Dispatcher
     */
    public function getEventDispatcher()
    {
        if (!$this->_eventDispatcher) {
            $this->_eventDispatcher = new Application_Model_Event_Dispatcher();
        }
        return $this->_eventDispatcher;
    }

    /**
     * Set event dispatcher instance
     *
     * @param Zend_EventManager_EventCollection $eventDispatcher
     * @return Product_Model_Product
     */
    public function setEventDispatcher(Zend_EventManager_EventCollection $eventDispatcher)
    {
        $this->_eventDispatcher = $eventDispatcher;
        return $this;
    }

    public function save()
    {
        $cache = Zend_Registry::get('output_cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('search_items'));

        if ($this->getIsPublished() && $this->getIsVisible() && !$this->getIsSold() && $this->getIsApproved()) {
            //$this->selfTriggerEvent('published');
        }

        if (null !== ($hasScrathces = $this->getHasScratches())) {
            $this->setEAVAttributeValue('has_scratches', $hasScrathces);
        }

        if ($this->getPrice() <= Shipping_Model_Rate::getMaximumValueSellerAbsorb()) {
            $this->setShippingCover(0);
        }

        // "old_price" used to indicate if a price reduced. So "old_price" always contains min price for
        // all history of price changes
        if ($this->getStored('price') !== $this->getPrice()) {
            if ($this->getOldPrice() === null) {
                $this->setOldPrice($this->getStored('price'));
            } else {
                $this->setOldPrice(min($this->getStored('price'), $this->getOldPrice()));
            }
        }


        $res = parent::save();
        if ($this->isChanged()) {
            $this->selfTriggerEvent('after.changes.save', array('from' => __method__));

            if (APPLICATION_ENV !== 'live') {
                //price reduced
                if ($this->getStored('price') > $this->getPrice()) {
                    $this->getEventDispatcher()->trigger('product.price.reduced', $this, array());
                }
                //sale window changed
                if ($this->getStored('available_till') != $this->getAvailableTill()) {
                    $this->getEventDispatcher()->trigger('product.sale_window.change', $this, array());
                }
            }
        }

        if (APPLICATION_ENV === 'testing') {
            if ($this->hasDataChanges() || $this->isDeleted()) {
                $this->getEventDispatcher()->trigger('product.save.after', $this, array());
            }
        }

        // Saving EAV
        if ($this->isExists()) {
            foreach ($this->settedEavAttributes as $eavItem) {
                list($eav, $value) = $eavItem;
                /** @var Ikantam_EAV $eav */
                $eav->saveValue($value);
            }
            $this->settedEavAttributes = array();
        }

        return $res;
    }

    public function getProductUrl()
    {
        if ($this->getPageUrl()) {
            return Ikantam_Url::getUrl('catalog/' . $this->getPageUrl());
        }

        return Ikantam_Url::getUrl('product/view/index', array('id' => $this->getId()));
    }

    public function getEditUrl()
    {
        return Ikantam_Url::getUrl('product/edit/' . $this->getId());
    }

    public function getAddToCartUrl($shipping = self::SHIPPING_PICK_UP)
    {
        return Ikantam_Url::getUrl('cart/index/add', array('id' => $this->getId(), 'shipping' => $shipping));
    }

    public function getAddToCartUrlAjax($qty = 1)
    {
        return Ikantam_Url::getUrl('cart/index/add', array('id' => $this->getId(), 'qty' => $qty));
    }

    public function getRemoveFromCartUrl()
    {
        return Ikantam_Url::getUrl('cart/index/remove', array('id' => $this->getId()));
    }

    public function getAddToWishlistUrl()
    {
        return Ikantam_Url::getUrl('wishlist/index/add', array('id' => $this->getId()));
    }

    public function getRemoveFromWishlistUrl()
    {
        return 'http://example.com/wishlist/add/id/' . $this->getId();
    }

    public function getSeller()
    {
        if (!$this->_seller) {
            $this->_seller = new Application_Model_User($this->getUserId());
        }
        return $this->_seller;
    }

    public function getProductLink()
    {
        return '<a href="' . $this->getProductUrl() . '">' . $this->getTitle() . '</a>';
    }

    public function getUserLink()
    {
        $user = new Application_Model_User($this->getUserId());
        return $user->getViewProfileLink();
    }

    public function getCategory()
    {
        return new Category_Model_Category($this->getCategoryId());
    }

    public function getColor()
    {
        return new Application_Model_Color($this->getColorId());
    }

    public function getManufacturer()
    {
        return new Application_Model_Manufacturer($this->getManufacturerId());
    }

    public function getMaterial()
    {
        return new Application_Model_Material($this->getMaterialId());
    }

    public function getConditionLabel()
    {
        switch ($this->getCondition()) {
            case 'new':
                return 'New: Product hasn\'t been unwrapped from box';
                break;

            case 'like-new':
                return 'Like New: Product has been unwrapped from box but looks new & doesn\'t have any scratches, blemishes, etc.';
                break;

            case 'good':
                return 'Good: Minor blemishes that most people won\'t notice';
                break;

            case 'satisfactory':
                return 'Satisfactory: Moderate wear and tear, but still has many good years left';
                break;

            case 'age-worn':
                return 'Age-worn: Has lived a full life and has a "distressed" look with noticeable wear';
                break;

            default:
                return null;
                break;
        }
    }

    public function getShortConditionLabel()
    {
        switch ($this->getCondition()) {
            case 'new':
                return 'New';
                break;

            case 'like-new':
                return 'Like New';
                break;

            case 'good':
                return 'Good';
                break;

            case 'satisfactory':
                return 'Satisfactory';
                break;

            case 'age-worn':
                return 'Age-worn';
                break;

            default:
                return null;
                break;
        }
    }

    public function getAgeLabel()
    {
        switch ($this->getAge()) {
            case 0:
                return '<1';
                break;
            case 1:
                return '1';
                break;
            case 2:
                return '2';
                break;
            case 3:
                return '3';
                break;
            case 4:
                return '4';
                break;
            case 5:
                return '5+';
                break;
            default:
                return null;
                break;
        }
    }

    public function getExtraAllImages()
    {
        $collection = new Product_Model_Image_Collection();
        $collection->getExtraByProductId($this->getId());
        return $collection;
    }

    public function hasLockedImage($collection = null)
    {
        if ($collection === null) {
            $collection = $this->getExtraAllImages();
        }

        foreach ($collection as $image) {
            if ($image->getIsLocked()) {
                return true;
            }
        }
        return false;
    }

    public function unlockAllImages()
    {
        foreach ($this->getExtraAllImages() as $image) {
            $image->setIsLocked(0)->save();
        }
    }

    /**
     *
     * @return \Product_Model_Image
     */
    public function getMainImage()
    {
        $image = new Product_Model_Image();
        $image->getMainByProductId($this->getId());

        if (!$image->getId()) {
            if ($this->getAllImages()->getSize() > 0) {
                $image = $this->getAllImages()->getFirstItem();
            }
        }
        return $image;
    }

    /**
     * Checks if at least one image has been uploaded
     *
     * @return bool
     */
    public function isUploadedImageExist()
    {
        return (bool) $this->getExtraAllImages()->getSize();
    }

    public function isReducedPrice()
    {
        return ($this->getOldPrice() !== null) && ($this->getPrice() < $this->getOldPrice());
    }

    /**
     * return price when created product
     * @return float
     */
    public function getInitialPrice()
    {
        if ($this->hasInitialPrice() !== true) {
            $initialPrice = $this->getEAVAttributeValue('initial_price');
            if ($initialPrice === null) {
                $initialPrice = max($this->getPrice(), $this->getOldPrice());
                /* quick save */
                $eav          = $this->getEAV($this->getEAVAttribute('initial_price'));
                $eav->saveValue($initialPrice);
            }
            $this->setData('initial_price', $initialPrice);
        }

        return $this->getData('initial_price');
    }

    /**
     *
     * @return \Product_Model_Image_Collection
     */
    public function getAllImages()
    {
        $collection = new Product_Model_Image_Collection();
        $collection->getByProductId($this->getId());
        return $collection;
    }

    public function getMainImageUrl($type = null)
    {
        if ($type == 'medium') {
            return $this->getMainImage()->getS3Url(500, 500, 'crop');
        }

        if ($type == 'squared') {
            return $this->getMainImage()->getS3Url(500, 500, 'crop');
        }

        if ($type == 'autocomplete') {
            return $this->getMainImage()->getS3Url(360, 300, 'frame');
        }

        return $this->getMainImage()->getS3Url(1500, 1500, 'frame');
    }

    public function getPickUpAddress()
    {
        return new User_Model_Address($this->getPickUpAddressId());
    }

    public function isSaleable()
    {
        return ($this->getIsVisible() && $this->getIsPublished() && !$this->getIsSold());
    }

    public function canBePublished()
    {
        $e = ($this->getId() && $this->getTitle() && $this->getPrice() && $this->getCondition());


        $category = new Category_Model_Category($this->getCategoryId());
        $brand    = new Application_Model_Manufacturer($this->getManufacturerId());

        $e = $e && $category->getId() && $brand->getId();

        $color    = new Application_Model_Color($this->getColorId());
        $material = new Application_Model_Material($this->getMaterialId());

        $e = $e && $color->getId() && $material->getId() && $this->getAge();

        $e = $e && ($this->getIsPickUpAvailable() || $this->getIsDeliveryAvailable());

        $e = $e && ($this->getSellingType() && $this->getQty());

        $address = $this->getPickUpAddress();
        $e       = $e && $address->isExists();

        $e = $e && $this->getMainImage()->getId();

        return $e;
    }

    protected function _getHomeBackend()
    {
        if (is_null($this->_homeBackend)) {
            return $this->_homeBackend = new $this->_homeBackendClass();
        } elseif ($this->_homeBackend instanceof $this->_homeBackendClass) {
            return $this->_homeBackend;
        }
    }

    public function fixProductAtHome()
    {
        $hbe = $this->_getHomeBackend();

        if ($this->getId()) {
            try {
                $hbe->addProduct($this);
            } catch (Exception $e) {
                if ($e->getCode() != 23000)
                    throw $e; //SQLSTATE[23000]: Duplicate entry                                       
            }
        }

        return $this;
    }

    public function removeFromHomePage()
    {
        if ($this->getId()) {
            $this->_getHomeBackend()->delete($this);
        }

        return $this;
    }

    public function getFixedAtHomeProduct()
    {
        $this->_getbackend()->getFixedProduct($this);
        return $this;
    }

    public function getByPageUrl($pageUrl)
    {
        $this->_getbackend()->getByPageUrl($this, $pageUrl);
        return $this;
    }

//EXPIRATION 

    /**
     * Return time how much the product will be available for purchase
     * !negative value might be returned in case when product is already unavailable
     *
     * @return int
     */
    public function getTimeToLife()
    {
        $times = array_filter(array($this->getAvailableTill(), $this->getExpireAt()));
        if (empty($times)) {
            return PHP_INT_MAX;
        }
        return min($times) - time();
    }

    public function getExpiryDateString($format = 'M d, Y')
    {
        $expiry = $this->getExpireAt(false);

        //if (!$expiry || $this->getIsSold() || !$this->getIsPublished())

        if (!$expiry) {
            return false;
        }

        return date($format, $expiry);
    }

    public function isAvailableToExtendExpirationPeriod()
    {
        $days = $this->_expiryPeriod;
        if ($this->getExpireAt() == 0 || (time() + $days * 86400 - $this->getExpireAt()) < 86400)
            return false;
        return true;
    }

    public function isExpired()
    {
        $now        = time();
        $currentExp = $this->getExpireAt();

        if ($currentExp < $now)
            return true;
        return false;
    }

    public function extendExpiration($days = null)
    {
        if (is_null($days))
            $days = $this->_expiryPeriod;

        $newExp     = time() + ($days * 86400);
        $currentExp = $this->getExpireAt();
        if (!$this->isAvailableToExtendExpirationPeriod())
            return false;

        $this->setExpireAt($newExp)->save();
        return true;
    }

    public function getExtendPublicationUrl()
    {
        return Ikantam_Url::getUrl('user/sale/extend-publication-period', array('id' => $this->getId()));
    }

//************************EXPIRATION

    public function getOrderItem($userId = null)
    {
        if (!$userId) {
            $userId = User_Model_Session::user()->getId();
        }

        return $this->_getbackend()->getOrderItem($userId, $this->getId());
    }

    public function getIsSold()
    {
        return $this->getQty() ? 0 : 1;
    }

    public function setIsSold($qty)
    {
        $myQty = $this->getQty();

        if ($myQty < $qty) {
            $myQty = 0;
        } else {
            $myQty = $myQty - $qty;
        }

        $this->setQty($myQty);

        return $this;
    }

    public function deliveryDateAvailable()
    {
        return $this->_getBackend()->deliveryDateAvailable($this);
    }

    public function isAvailable()
    {
        if ($this->getAvailableTill() && $this->getAvailableTill() < time()) {
            return false;
        }

        if ($this->getExpireAt() && $this->getExpireAt() < time()) {
            return false;
        }

        return true;
    }

    public function events(Zend_EventManager_EventCollection $events = null)
    {
        if (null !== $events) {
            self::$_events = $events;
        } elseif (null === self::$_events) {
            self::$_events = new Zend_EventManager_EventManager(__CLASS__);
        }
        return self::$_events;
    }

    public function getIsUnder15Pounds($default = null)
    {
        $value = $this->getData('is_under_15_pounds');
        if (is_null($value)) {
            return $default;
        }
        return $value;
    }

    public function setIsUnder15Pounds($value)
    {
        $this->setData('is_under_15_pounds', $value);
        return $this;
    }

    /**
     * Trigger event and use self as target
     * @param string $event - event id
     * @param array $params
     * @return object self
     */
    public function selfTriggerEvent($event, $params = array())
    {
        $this->events()->trigger($event, $this, $params);
        return $this;
    }

    /**
     * Return popularity object related with this product
     * @param string $type - see Product_Model_Popularity
     * @return object Product_Model_Popularity
     */
    public function popularity($type)
    {
        if (!$popularity = isset($this->_popularity[$type]) ? $this->_popularity[$type] : false) {
            $popularity               = $this->_popularity[$type] = new Product_Model_Popularity;
            $popularity->getIfExist($this->getId(), $type)
                    ->setProductId($this->getId());
        }

        return $popularity;
    }

    /**
     * Checks if user is owner(seller)
     *
     * @param  \Application_Model_User $user
     * @return bool
     */
    public function isBelongsToUser(\Application_Model_User $user)
    {
        return $this->isExists() && $this->getUserId() == $user->getId();
    }

    public function setAvailableFrom($time)
    {
        if (!is_numeric($time) && !is_null($time)) {
            $time = strtotime($time);
        }

        $this->_data['available_from'] = $time;
        return $this;
    }

    public function setAvailableTill($time)
    {
        if (!is_numeric($time) && !is_null($time)) {
            $time = strtotime($time);
        }

        $this->_data['available_till'] = $time;
        return $this;
    }

    /**
     * Product have specific data format for few fields
     *
     * @param mixed $key
     * @param mixed $value
     * @return self
     */
    public function setData($key, $value = null)
    {
        parent::setData($key, $value);
        $this->setAvailableFrom($this->getAvailableFrom());
        $this->setAvailableTill($this->getAvailableTill());
        return $this;
    }

    /**
     * Checks if seller absorb full amount of a shipping fee
     *
     * @return bool
     */
    public function isFreeShipping()
    {
        return $this->getShippingCover() == 100;
    }

    /**
     * Checks if seller absorb half of amount of a shipping fee
     *
     * @return bool
     */
    public function isHalfFreeShipping()
    {
        return $this->getShippingCover() == 50;
    }

    /**
     * Since updates address stores in user_addresses table
     *
     * @return mixed
     */
    public function getPickupPostcode()
    {   //@TODO: use correct field:  getPostcode either getPostalCode
        if ($this->getData('pickup_postcode')) {
            return $this->getData('pickup_postcode');
        }
        $address = $this->getPickUpAddress();
        return $address->getPostalCode($address->getPostcode());
    }

    public function getPickupCity()
    {
        $address = $this->getPickUpAddress();
        return parent::getPickupCity($address->getCity());
    }

    public function getPickupState()
    {
        $address = $this->getPickUpAddress();
        return parent::getPickupState($address->getState($address->getStateCode()));
    }

    public function getPickupFullName()
    {
        if ($this->getData('pickup_full_name')) {
            return $this->getData('pickup_full_name');
        }

        return $this->getPickUpAddress()->getFullName();
    }

    public function getPickupAddressLine1()
    {
        if ($this->getData('pickup_address_line1')) {
            return $this->getData('pickup_address_line1');
        }

        return $this->getPickUpAddress()->getAddressLine1();
    }

    public function getPickupAddressLine2()
    {
        if ($this->getData('pickup_address_line2')) {
            return $this->getData('pickup_address_line2');
        }

        return $this->getPickUpAddress()->getAddressLine2();
    }

    public function getPickupPhone()
    {
        if ($this->getData('pickup_phone')) {
            return $this->getData('pickup_phone');
        }

        return $this->getPickUpAddress()->getPhoneNumber();
    }

    /**
     * Converts form data to appropriate product data and set product data
     *
     * @param \Ikantam_Form $form
     * @return array - conversion result
     */
    public function convertProductData(\Ikantam_Form $form)
    {
        if (method_exists($form, 'convertProductData')) {
            return $form->convertProductData($this);
        }
        throw new \Ikantam_Form_Exception(get_class($form) . ' does not support product data convertion.');
    }

    /**
     * Calculate and round percent value to specified precision
     * @param $percent
     * @param int $precision
     * @return float
     */
    public function getPercentOfPrice($percent, $precision = 2)
    {
        return round($this->getPrice() / 100 * $percent, $precision);
    }

    public function setIsPetInHome($petInHome)
    {
        $user = new User_Model_User($this->getUserId());
        if ($user->getId()) {
            $user->personalSettings('pet_in_home', $petInHome);
        }
    }

    public function setIsSmokeFreeHome($isSmokeFree)
    {
        $user = new User_Model_User($this->getUserId());
        if ($user->getId()) {
            $user->personalSettings('is_smoke_free', $isSmokeFree);
        }
    }

    public function getExpiryDateTime()
    {
        if ($this->getExpireAt() || $this->getAvailableTill()) {

            $expTime = null;
            $expAt   = (int) $this->getExpireAt();
            $aTill   = (int) $this->getAvailableTill();

            if ($expAt > 0 && $aTill > 0) {
                $expTime = ($expAt < $aTill) ? $expAt : $aTill;
            } elseif ($expAt > 0) {
                $expTime = $expAt;
            } elseif ($aTill > 0) {
                $expTime = $aTill;
            }

            return $expTime;
        }

        return false;
    }

    /**
     * Returns a 32 character string defining the object
     * Each product has own hash signature based on id
     * @return string
     */
    public function getHashSignature()
    {
        return md5(crc32($this->getId() . $this->getId()) . ($this->getId() * 98123));
    }

    /**
     * Sets name that will be used for current symbol table
     * @param string $name
     * @throws InvalidArgumentException
     * @return mixed
     */
    public function setSymbolTableName($name)
    {
        //http://www.php.net/manual/en/language.variables.basics.php
        if (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $name)) {
            throw new InvalidArgumentException('Invalid variable name.');
        }
        $this->_symbolTableName = $name;
    }

    /**
     * Retrieve name to current symbol table
     * @return mixed
     */
    public function getSymbolTableName()
    {
        return $this->_symbolTableName;
    }

    /**
     * Return unique identifier for entity
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->getId();
    }

    /**
     * Return entity family (common for group)
     * @return string
     */
    public function getEntityFamily()
    {
        return 'products';
    }

    /**
     * Sets EAV attribute value related to this product
     * Attribute will be saved after the "save" method called
     * @param string $name - attribute name
     * @param mixed $value
     * @param string $type (optional) - value type
     * @param bool $allowNull (optional) - whether value could be null
     * @return $this
     */
    public function setEAVAttributeValue($name, $value, $type = null, $allowNull = false)
    {
        if (is_null($value) && !$allowNull) {
            trigger_error('Set NULL as "' . $name . '" attriute value.');
            return $this;
        }
        $eav                              = $this->getEAV($this->getEAVAttribute($name, $type));
        $this->settedEavAttributes[$name] = array($eav, $value);

        return $this;
    }

    /**
     * Retrieves value by attribute name
     * @param string $name
     * @return mixed
     */
    public function getEAVAttributeValue($name)
    {
        return $this->getEAV($this->getEAVAttribute($name))->getValue();
    }

    /**
     * Determine whether product located in Flat Rate area
     * @return bool
     */
    public function isProductLocatedInNYC()
    {
        if (!$this->isExists()) {
            return false;
        }
        // This is expensive operation, so we save the result in static array (common for all)
        if (!isset(self::$_locatedInNYC[$zip = $this->getPickupPostcode('N/A')])) {
            $filter   = new Shipping_Model_Borough_FlexFilter(new Shipping_Model_Borough_Collection);
            $filter->name('in', $boroughs = Shipping_Model_Rate::getFlatRateBoroughs())
                    ->postal_code('=', $zip);

            $productCity = trim(preg_replace('/[\s,-]+/', ' ', strtoupper($this->getPickupCity('N/A'))));
            $boroughs    = array_map(function($borough) {
                return strtoupper($borough);
            }, $boroughs);

            // For the first, checks the product city for flat rate and if no matches found, then it uses filter by zip
            self::$_locatedInNYC[$zip] = in_array($productCity, $boroughs) || (bool) $filter->count();
        }

        return self::$_locatedInNYC[$zip];
    }

    /**
     * Finds related orders
     * @return \Order_Model_Order_Collection
     */
    public function getRelatedOrders()
    {
        $orders = new \Order_Model_Order_Collection;
        if ($this->isExists()) {
            $orders->getFlexFilter()->id(
                    'in', Collection::flexFilter()
                            ->alwaysJoin('order') //inner join orders
                            ->id('=', $this->getId()) //for this product
                            ->group('order_id')
                            ->apply()
                            ->getColumn('order_id')
            )->apply();
        }

        return $orders;
    }

    /**
     * Finds related order items
     * @return \Order_Model_Item_Collection
     */
    public function getRelatedOrderItems()
    {
        $items = new \Order_Model_Item_Collection;
        if ($this->isExists()) {
            $items->getFlexFilter()->id(
                    'in', Collection::flexFilter()
                            ->alwaysJoin('order_item') //inner join order items
                            ->id('=', $this->getId()) //for this product
                            ->group('order_item_id')
                            ->apply()
                            ->getColumn('order_item_id')
            )->apply();
        }

        return $items;
    }

    /**
     * Returns label special shipping offer
     * @return null|string
     */
    public function getShippingOfferLabel()
    {
        $labels = array(
            0 => array(50 => '$32 Off Delivery', 100 => '$65 Off Delivery'),
            1 => array(50 => '50% Off Delivery in NYC', 100 => 'Free Delivery in NYC')
        );

        if (($inNYC = $this->isProductLocatedInNYC()) && $this->getIsUnder15Pounds()) {
            return '$20 Delivery in NYC';
        }

        return isset($labels[$inNYC][$this->getShippingCover()]) ? $labels[$inNYC][$this->getShippingCover()] : null;
    }

    /**
     * Create a new instance of images filter and set it to filter by this product id
     * @return Product_Model_Image_FlexFilter
     */
    public function getImagesFilter()
    {
        $images = new Product_Model_Image_Collection;
        return $images->getFlexFilter()->product_id('=', $this->getId());
    }

    /**
     * Return label which define "Pets" status
     * @return string
     */
    public function getPetInHomeLabel()
    {
        if (!$this->getPetInHome()) {
            return 'Pet free home.';
        }
        $label = '';
        if ($this->getEAVAttributeValue('exposed_cat')) {
            $label .= 'Cats, ';
        }
        if ($this->getEAVAttributeValue('exposed_dog')) {
            $label .= ' Dogs';
        }

        return trim($label, ', ');
    }

    /**
     * Creates attribute. Sets name and type.
     * @param $name
     * @param null $type
     * @return Application_Model_EAV_Attribute
     * @throws InvalidArgumentException
     */
    protected function getEAVAttribute($name, $type = null)
    {
        if (isset($this->eavAttributes[$name])) {
            $type = $this->eavAttributes[$name];
        } elseif (null === $type) {
            throw new \InvalidArgumentException('Undefined attribute type for "' . $name . '".');
        }

        return new \Application_Model_EAV_Attribute($type, $name);
    }

    /**
     * Creates EAV model. Sets attribute and entity(this model)
     * @param Application_Model_EAV_Attribute $attribute
     * @return Ikantam_EAV
     */
    protected function getEAV(\Application_Model_EAV_Attribute $attribute)
    {
        return new \Ikantam_EAV($this, $attribute, new \Application_Model_EAV_DB_MySql);
    }

    public function estimateDelivery($address)
    {
        if (is_array($address)) {
            $address = new User_Model_Address($address);
        }

        $planner         = new Shipping_Model_RoutePlanner();
        $isUnder15Pounds = $this->getData('is_under_15_pounds');
        $productZip      = $this->getPickupPostalCode($this->getPickupPostcode());
        $destinationZip  = $address->getPostalCode($address->getPostcode());

        // Find price for first item
        $defaultShippingPrice = $planner->getShippingPriceByPostalCodes(
                $productZip, $destinationZip, $isUnder15Pounds, false
        );
        return $defaultShippingPrice;
    }

    public function hasUserPurchasedProduct($userId)
    {
        if (!$userId) {
            return false;
        }

        if (!$this->getId()) {
            throw new Exception('Product ID must be not null');
        }

        $orderItem = new Order_Model_Item();
        return $orderItem->hasUserPurchasedProduct($userId, $this->getId());
    }

}