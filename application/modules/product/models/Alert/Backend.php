<?php

class Product_Model_Alert_Backend extends Application_Model_Abstract_Backend
{

    protected $_table     = 'product_alerts';
    protected $_sentTable = 'product_alert_sent';

    public function getByUserId(\Product_Model_Alert_Collection $collection, $userId, $addAttributes = true)
    {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();

        $select->from(array('main_table' => $this->_getTable()));

        if ($addAttributes) {
            $this->_addAttributeTitles($select);
        }

        $select->group('main_table.id');

        $select->where('main_table.user_id = ?', $userId)
            ->where('main_table.is_delete = ?', 0)
            ->order('main_table.id DESC');
        $result = $db->fetchAll($select);

        foreach ($result as $row) {
            $alert = new Product_Model_Alert();
            $alert->addData($row);

            $collection->addItem($alert);
        }
    }

    public function check(\Product_Model_Alert_Collection $collection)
    {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();

        $select->from(array('main_table' => $this->_getTable()))
            // filter by alert
            ->join(
                array('p' => 'products'),
                '(FIND_IN_SET(p.category_id, main_table.subcategory_ids) OR main_table.subcategory_ids IS NULL) AND
                    (FIND_IN_SET(p.condition, main_table.conditions) OR main_table.conditions IS NULL) AND
                    (FIND_IN_SET(p.color_id, main_table.color_ids) OR main_table.color_ids IS NULL) AND
                    (FIND_IN_SET(p.manufacturer_id, main_table.brand_ids) OR main_table.brand_ids IS NULL) AND
                    (p.price >= main_table.min_price OR main_table.min_price IS NULL) AND
                    (p.price <= main_table.max_price OR main_table.max_price IS NULL) AND
                    (p.created_at > main_table.created_at)',
                array('product_ids' => new Zend_Db_Expr('GROUP_CONCAT(DISTINCT p.id SEPARATOR ",") '))
            )
            ->joinLeft(
                array('pas' => $this->_sentTable),
                'p.id = pas.product_id AND main_table.id = pas.alert_id',
                array()
            )
            // filter by max last send date for each user (user can set sending period, default period = 86400 (one day) )
            ->join(
                array('main_table2' => new Zend_Db_Expr('
                    (SELECT `mt`.`user_id`, MAX(`last_send_date`) AS `max_last_send_date`, `uas`.`value` AS `uas_value`
                    FROM  `' .  $this->_getTable() . '` AS `mt`
                    LEFT JOIN `app_settings` AS `a_s`
                        ON  `a_s`.`name` = "user_notification_product_alerts"
                    LEFT JOIN `users_app_settings` AS `uas`
                        ON `mt`.`user_id` = `uas`.`user_id` AND `uas`.`app_setting_id` = `a_s`.`id`
                    GROUP BY `mt`.`user_id`
                    HAVING MAX(`last_send_date`) + IFNULL(uas.value, 86400) < ' . time() . ' OR MAX(`last_send_date`) IS NULL AND (uas.value <> 3155692600 OR uas.value IS NULL)
                    )
                ')),
                'main_table.user_id = main_table2.user_id OR main_table.user_id IS NULL AND main_table2.user_id IS NULL',
                array('max_last_send_date', 'uas_value')
            )

            ->where('pas.id IS NULL') //  product may be sent only once for each user
            ->where('main_table.is_delete = ?', 0)

           // only saleable products
            ->where('p.is_visible = ?', 1)
            ->where('p.is_published = ?', 1)
            ->where('p.is_approved = ?', 1)
            ->where('p.qty > ?', 0)
            ->where('p.available_till = 0
                    OR p.available_till IS NULL
                    OR p.available_till >= ?', time())
            ->where('p.expire_at = 0
                    OR p.expire_at >= ?', time())

            ->group('main_table.id');
        $this->_addAttributeTitles($select);

        $collectionSelect = clone $select;

//         for category
        $select->join(
                array('c' => 'categories'), '(c.id = p.category_id)  AND (c.parent_id = main_table.category_id)', array()
        );
//         for collection
        $collectionSelect->join(
                array('cp' => 'collections_products'), '(cp.collection_id = main_table.collection_id)  AND (cp.product_id = p.id)', array()
        );

        $result           = $db->fetchAll($select);
        $collectionResult = $db->fetchAll($collectionSelect);

        foreach (array_merge($result, $collectionResult) as $row) {
            $alert = new Product_Model_Alert();
            $alert->addData($row);
            $collection->addItem($alert);
        }
    }

    /*
      public function loadAttributeTitles(\Product_Model_Alert $alert)
      {
      $db = Zend_Db_Table::getDefaultAdapter();
      $select = $db->select();
      $select->from(array('main_table' => $this->_getTable()))
      ->where('main_table.id = ?', $alert->getId());
      $this->_addAttributeTitles($select);
      }
     */

    /**
     * required: 'main_table' alias
     * @param Zend_Db_Select $select
     */
    protected function _addAttributeTitles(\Zend_Db_Select $select)
    {
        $select->joinLeft(
                array('cat' => 'categories'),
                'cat.id = main_table.category_id',
                array('category_title' => 'cat.title')
            )
            ->joinLeft(
                array('collections'),
                'collections.id = main_table.collection_id',
                array('collection_title' => 'collections.name')
            )
            ->joinLeft(
                array('sc' => 'categories'),
                'FIND_IN_SET(sc.id, main_table.subcategory_ids)',
                array('subcategory_titles' => new Zend_Db_Expr('GROUP_CONCAT(DISTINCT sc.title SEPARATOR ", ") '))
            )
            ->joinLeft(
                array('m' => 'manufacturers'),
                'FIND_IN_SET(m.id, main_table.brand_ids)',
                array('brand_titles' => new Zend_Db_Expr('GROUP_CONCAT(DISTINCT m.title SEPARATOR ", ") '))
            )
            ->joinLeft(
                array('colors'),
                'FIND_IN_SET(colors.id, main_table.color_ids)',
                array('color_titles' => new Zend_Db_Expr('GROUP_CONCAT(DISTINCT colors.title SEPARATOR ", ") '))
            );
    }

    public function saveSentItems(\Product_Model_Alert $alert)
    {
        $sql    = 'INSERT INTO `' . $this->_sentTable . '`
        (`alert_id`, `product_id`)
        VALUES  ';
        $values = array();
        foreach ($alert->getProductIdsAsArray() as $_productId) {
            $values[] = '(:alertId' . $_productId . ', :productId' . $_productId . ')';
        }
        $sql .= implode(',', $values);

        $stmt = $this->_getConnection()->prepare($sql);

        foreach ($alert->getProductIdsAsArray() as $_productId) {
            $stmt->bindParam(':alertId' . $_productId, $alert->getId());
            $stmt->bindParam(':productId' . $_productId, $_productId);
        }
        $stmt->execute();
    }

}