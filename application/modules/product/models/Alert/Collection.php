<?php

/**
 * Class Product_Model_Alert_Collection
 * @method  Product_Model_Alert_Backend _getBackEnd()
 */
class Product_Model_Alert_Collection extends Application_Model_Abstract_Collection
{

    protected $_backendClass         = 'Product_Model_Alert_Backend';
    protected $_itemObjectClass      = 'Product_Model_Product';


    public function getByUserId($userId, $loadAttributes = true)
    {
        if (!is_int($userId)) {
            throw new Exception('Invalid Id. Id must be an integer value.');
        }

        $this->_getBackEnd()->getByUserId($this, $userId, $loadAttributes);
        return $this;
    }

    public function check()
    {
        $this->_getBackEnd()->check($this);
        return $this;
    }

}
