<?php

class Product_Model_Image extends Application_Model_Abstract
{

    protected static $_directoriesCount = 10000;
    protected $_backendClass            = 'Product_Model_Image_Backend';

    public function delete()
    {
        //@TODO: delete file from S3
        return parent::delete();
    }

    public function getMainByProductId($id)
    {
        $this->_getbackend()->getMainByProductId($this, $id);
        return $this;
    }

    public function setDefaultImage($productId, $imageId)
    {
        $cache = Zend_Registry::get('output_cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('search_items'));
        $this->_getbackend()->setDefaultImage($productId, $imageId);
        return $this;
    }

    public function save()
    {
        $cache = Zend_Registry::get('output_cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('search_items'));
        return parent::save();
    }

    public function getPath($type = null, $width = null, $height = null)
    {
        if ($this->getS3Path()) {
            if ($type == 'squared_white') {
                $type = 'frame';
            }
            if ($type != 'frame') {
                $type = 'crop';
            }
            if (!$width) {
                $width = 200;
            }
            if (!$height) {
                $height = 200;
            }
            if ($width == 294) {
                $width = 360;
            }
            if ($height == 245) {
                $height = 300;
            }
            return $this->getS3Url($width, $height, $type);
        }

        if ($type == 'squared_white') {
            $image = new Ikantam_Image();
            return $image->getPath($this->getData('path'), $width, $height);
        }

        return $this->getData('path');
    }

    public function getS3Url($width, $height, $type)
    {
        $angle = (int) $this->getDefaultAngle();

        $bp            = $this->getS3Path() . '/' . $width . '-' . $height . '-' . $type . '-' . $angle . '.jpg';
        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
        return $cloudFrontUrl . '/product-images/' . $bp;
    }

    public static function getTmpS3Path($dstPath)
    {
        $baseDir = APPLICATION_PATH . '/../' . Zend_Registry::get('config')->tmpUploadDir->productImages;

        $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;
        return $bucketName . '/product-images' . str_replace($baseDir, '', $dstPath);
    }

    public static function _getRandomDirName()
    {
        return time() . md5(Ikantam_Math_Rand::getString(16, Ikantam_Math_Rand::ALPHA_NUMERIC));
    }

    public static function getBaseTmpDir()
    {
        return APPLICATION_PATH . '/../' . Zend_Registry::get('config')->tmpUploadDir->productImages;
    }

    public static function getTmpUploadDir(Product_Model_Product $product)
    {
        $baseDir = self::getBaseTmpDir();

        $productId = (int) $product->getId();

        $th            = (int) (($productId - 1) / self::$_directoriesCount);
        $folder        = ($th * self::$_directoriesCount + 1) . '-' . ($th * self::$_directoriesCount + self::$_directoriesCount);
        $firstLevelDir = $baseDir . DIRECTORY_SEPARATOR . $folder;

        if (!file_exists($firstLevelDir)) {//one directory per X product IDs
            mkdir($firstLevelDir, 0777);
        }

        $productDir = $firstLevelDir . DIRECTORY_SEPARATOR . $productId;

        if (!file_exists($productDir)) {//one directory per product ID
            mkdir($productDir, 0777);
        }

        $rN       = self::_getRandomDirName();
        $randPath = $productDir . DIRECTORY_SEPARATOR . $rN;

        if (!file_exists($randPath)) {
            mkdir($randPath, 0777); //one directory per product image
        }

        return $folder . DIRECTORY_SEPARATOR . $productId . DIRECTORY_SEPARATOR . $rN; //$randPath;
    }

    protected function _getProductUploadDir($baseUploadPath, $productId)
    {
        $th     = (int) (($productId - 1) / $this->_directoriesCount);
        $folder = ($th * $this->_directoriesCount + 1) . '-' . ($th * $this->_directoriesCount + $this->_directoriesCount);

        $firstLevelDir = $baseUploadPath . DIRECTORY_SEPARATOR . $folder;

        if (!file_exists($firstLevelDir)) {//one directory per X product IDs
            mkdir($firstLevelDir, 0777);
        }

        $productDir = $firstLevelDir . DIRECTORY_SEPARATOR . $productId;

        if (!file_exists($productDir)) {//one directory per product ID
            mkdir($productDir, 0777);
        }

        return $folder . DIRECTORY_SEPARATOR . $productId;
    }

    /**
     * Get the image endpoints url for given type (user/admin)
     * 
     * @param string $type 
     * @return object stdClass
     */
    public static function getEndpoints($type = 'user')
    {
        if ($type === 'user') {
            return (object) array(
                        'delete'       => Ikantam_Url::getUrl('product/image/delete'),
                        'setAsDefault' => Ikantam_Url::getUrl('product/image/set-as-default'),
                        'rotate'       => Ikantam_Url::getUrl('product/image/rotate'),
            );
        } elseif ($type === 'admin') {
            return (object) array(
                        'delete'       => Ikantam_Url::getUrl('admin/edit-product/delete-image'),
                        'setAsDefault' => Ikantam_Url::getUrl('admin/edit-product/set-as-default-image'),
                        'rotate'       => Ikantam_Url::getUrl('admin/edit-product/rotate-image'),
                        'lock'         => Ikantam_Url::getUrl('admin/edit-product/lock-image'),
                        'unlock'       => Ikantam_Url::getUrl('admin/edit-product/unlock-image'),
            );
        }

        return new stdClass;
    }

}