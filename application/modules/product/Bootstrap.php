<?php

class Product_Bootstrap extends Zend_Application_Module_Bootstrap
{

    public function _initIocContainer()
    {
        $ioc = new Illuminate\Container\Container();

        $ioc->bind('product', function($container, $id) {
                    $product = new Product_Model_Product($id);
                    $product->setEventDispatcher(new Application_Model_Event_Dispatcher());
                    return $product;
                });

        Zend_Registry::set('ioc', $ioc);
    }

}
