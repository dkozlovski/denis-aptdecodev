<?php

class Product_Form_OptionalFieldsSubform extends Zend_Form_SubForm
{

    public function init()
    {
        $this->setDecorators(array(
            'FormElements',
        ));

        $this->addElement($this->getDescriptionElement());
        $this->addElement($this->getAvailableFromElement());
        $this->addElement($this->getAvailableTillElement());
        $this->addElement($this->getIsPickupAvailableElement());
        $this->addElement($this->getPetInHomeElement());
        $this->addElement($this->getIsSmokeFreeElement());
        //$this->addElement($this->getIsAbsorbingFeeElement());
        //$this->addElement($this->getAbsorbingAmountElement());
    }

    protected function getDescriptionElement()
    {
        $description = new Zend_Form_Element_Textarea('description');

        $description->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim', 'Null'))
                ->addValidator('StringLength', false, array(array('max' => 60000)))
                ->setAttrib('rows', 4)
                ->setAttrib('cols', 80)
                ->setAttrib('maxlength', 60000)
                ->setAttrib('class', 'fit')
                ->setAttrib('name', 'description')
                ->setLabel('Anything else you\'d like to mention?');

        $description->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        return $description;
    }

    protected function getAvailableFromElement()
    {
        $availableFrom = new Zend_Form_Element_Text('available_from');

        $dateFormat = new Zend_Validate_Date(array('format' => 'M dd, y'));
        $dateFormat->setLocale('en_US');

        $format = new Zend_Filter_LocalizedToNormalized(array('date_format' => 'M dd, y', 'locale'      => 'en_US'));

        $availableFrom->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim', 'Null'))
                ->addFilter($format)
                ->addValidator($dateFormat, true)
                ->setAttrib('id', 'datepicker')
                ->setAttrib('class', 'datepicker')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;')
                ->setAttrib('name', 'available_from')
                ->setLabel('<i class="aperiod"></i> 
                    <span class="getPopmsg">Available period</span>
                    <div class="popup-msg" style="display: none;">
                        <h3>Available period</h3>
                        <p>Specify dates when this product will be available for sale. Leave blank if you are not sure about it.</p>
                    </div>');

        $availableFrom->setDecorators(array(
            'ViewHelper',
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'pull-left dp-holder first')),
            array(array('fit' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'fit-els fit', 'openOnly' => true)),
            array(array('block' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'block-fields', 'openOnly' => true)),
            array('Label', array('class'     => 'block-lable', 'placement' => 'prepend', 'escape'    => false)),
            array(array('group' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'control-group', 'openOnly' => true))
        ));

        return $availableFrom;
    }

    protected function getAvailableTillElement()
    {
        $availableTill = new Zend_Form_Element_Text('available_till');

        $dateFormat = new Zend_Validate_Date(array('format' => 'M dd, y'));
        $dateFormat->setLocale('en_US');

        $format = new Zend_Filter_LocalizedToNormalized(array('date_format' => 'M dd, y', 'locale'      => 'en_US'));

        $availableTill->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim', 'Null'))
                ->addFilter($format)
                ->addValidator($dateFormat, true)
                ->setAttrib('id', 'datepicker2')
                ->setAttrib('class', 'datepicker')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('name', 'available_till')
                ->setAttrib('style', 'background:white;')
                ->setLabel(' &mdash; ');

        $availableTill->setDecorators(array(
            'ViewHelper',
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'pull-left dp-holder second')),
            array('Label', array('class'     => 'inner-label mdash', 'placement' => 'prepend', 'tag'       => 'div', 'tagClass'  => 'pull-left m-rl5', 'escape'    => false)),
            array(array('fit' => 'HtmlTag'), array('tag'       => 'div', 'class'     => 'fit-els fit', 'closeOnly' => true)),
            array(array('block' => 'HtmlTag'), array('tag'       => 'div', 'class'     => 'block-fields', 'closeOnly' => true)),
            array(array('group' => 'HtmlTag'), array('tag'       => 'div', 'class'     => 'control-group', 'closeOnly' => true))
        ));

        return $availableTill;
    }

    protected function getIsPickupAvailableElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('is_pick_up_available');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($multiOptions)
                ->setAttrib('name', 'is_pick_up_available')
                ->setLabel('Are you ok with buyers picking up on their own? <a href="' . Ikantam_Url::getUrl('') . '">Why?</a>')
                ->setDescription('<i class="apt-icon-sm-car"></i>');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array('Description', array('placement' => 'prepend', 'tag'       => 'li', 'escape'    => false)),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable', 'escape'    => false)),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getPetInHomeElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('pet_in_home');

        $currentCondition->setRequired(false)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($multiOptions)
                ->setAttrib('name', 'pet_in_home')
                ->setLabel('Are there pets in your home?')
                ->setDescription('<i class="apt-i-cat"></i>');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array('Description', array('placement' => 'prepend', 'tag'       => 'li', 'escape'    => false)),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getIsSmokeFreeElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('is_smoke_free');

        $currentCondition->setRequired(false)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($multiOptions)
                ->setAttrib('name', 'is_smoke_free')
                ->setLabel(' Is this a smoke free home?')
                ->setDescription('<i class="apt-i-cigaret"></i>');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array('Description', array('placement' => 'prepend', 'tag'       => 'li', 'escape'    => false)),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    // protected function getIsAbsorbingFeeElement() and protected function getAbsorbingAmountElement()
    // moved to AddressSubfrom.php considering design


    public function isValid($data)
    {
        $valid = parent::isValid($data);

        foreach ($this->getElements() as $element) {
            if ($element->hasErrors()) {
                if ($element->getName() == 'available_from' || $element->getName() == 'available_till') {
                    continue;
                }
                $element->addDecorator(array('group' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));

            }
        }

        $this->setErrorDecoratorForAvailablePeriod();

        foreach ($this->getDisplayGroups() as $displayGroup) {
            foreach ($displayGroup->getElements() as $element) {
                if ($element->hasErrors()) {
                    $element->removeDecorator('row');
                    $displayGroup->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));
                    break;
                }
            }
        }

        return $valid;
    }

    protected function setErrorDecoratorForAvailablePeriod()
    {
        $availableFrom = $this->getElement('available_from');
        $availableTill = $this->getElement('available_till');
        $className = 'control-group';
        if ($availableFrom->hasErrors() || $availableTill->hasErrors()) {
            $className .= ' has-error';
            if ($availableFrom->hasErrors()) {
                $className .= ' inp-1st';
            }
            if ($availableTill->hasErrors()) {
                $className .= ' inp-2nd';
            }
            $availableFrom->addDecorator(array('group' => 'HtmlTag'), array('tag'   => 'div', 'class' => $className, 'openOnly' => true));
            $availableTill->addDecorator(array('group' => 'HtmlTag'), array('tag'   => 'div', 'class' => $className, 'closeOnly' => true));

        }
    }

}