<?php

/**
 * Author: Alex P.
 * Date: 08.07.14
 * Time: 10:33
 */
class Product_Form_NewItem extends Ikantam_Form
{

    protected $conditions           = array(
        'new'          => 'New: Product hasn\'t been unwrapped from box',
        //'like-new'     => 'Like New: Product has been unwrapped from box but looks new & doesn\'t have any scratches, blemishes, etc.',
        'good'         => 'Good: Minor blemishes that most people won\'t notice',
        'satisfactory' => 'Satisfactory: Moderate wear and tear, but still has many good years left',
        'age-worn'     => 'Age-worn: Has lived a full life and has a "distressed" look with noticeable wear'
    );
    protected $defaultInputWrappers = array(
        array(
            array('colGroup' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')
        ),
        array(
            array(
                'control_group' => 'HtmlTag'
            ),
            array('tag' => 'div', 'class' => 'control-group')
        )
    );

    /**
     * @var \Product_Model_Product
     */
    protected $product;

    /**
     * @var \User_Model_Address_Collection
     */
    protected $addresses;
    protected $customErrorMessages = array(
        'isSet'     => false,
        'templates' => array(
            'NotEmpty'    => array(
                Zend_Validate_NotEmpty::IS_EMPTY => 'Value is required.',
            ),
            'GreaterThan' => array(
                Zend_Validate_GreaterThan::NOT_GREATER => "Value must be greater than '%min%'."
            ),
            'Between' => array(
                Zend_Validate_Between::NOT_BETWEEN =>  "Value must be greater than or equal to '%min%'."
            ),
            'LessThan'    => array(
                Zend_Validate_LessThan::NOT_LESS => "Value must be less than '%max%'"
            ),
            'Float'       => array(
                // null to set message to all validator error types
                null => "Incorrect value '%value%'. Value must be numeric."
            ),
            'InArray'     => array(
                Zend_Validate_InArray::NOT_IN_ARRAY => "Value '%value%' can not be used for this attribute.",
            )
        )
    );

    public function init()
    {
        $this->setAttrib('id', 'submit-form');
        $this->initFormDecorators();
        $this->initGeneralInfoBlock();
        $this->includeSubForms();
    }

    /**
     * List of available conditions for product
     * @return array
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * Sets all form and elements no required
     */
    public function _setNotRequired()
    {
        foreach ($this->getElements() as $element) {
            $element->setRequired(false)->setAllowEmpty(true);
        }

        foreach ($this->getSubforms() as $subform) {
            foreach ($subform->getElements() as $element) {
                $element->setRequired(false)->setAllowEmpty(true);
            }
        }
    }

    /**
     * Set current product
     * @param Product_Model_Product $product
     * @return $this
     */
    public function setProduct(\Product_Model_Product $product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * Set current addresses
     * @param User_Model_Address_Collection $addresses
     * @return $this
     */
    public function setAddresses($addresses)
    {
        $this->addresses = $addresses;
        return $this;
    }

    /**
     * @return User_Model_Address_Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Get current product
     * @return Product_Model_Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    public function convertProductData(\Product_Model_Product $product)
    {
        $productData  = $this->getValues();
        $manufacturer = new Application_Model_Manufacturer();
        $title        = $productData['manufacturer'];

        if (!empty($title)) {
            $al = new Manufacturer_Model_Association($title);

            if ($al->isExists()) {
                $manufacturer = $al->getManufacturer();
            } else {
                $manufacturer->getByTitle($title);
            }

            if (!$manufacturer->isExists()) {
                $manufacturer->setTitle($title)->save();
            }
        }

        $dateTime = new DateTime();

        $productData['manufacturer_id'] = $manufacturer->getId() ? : null;
        $productData['category_id']     = $productData['subcategory_id'];

        if (isset($productData['additional_details'])) {
            $productData += $productData['additional_details'];
            unset($productData['additional_details']);
        }


        if ($productData['available_from']) {
            $date = $productData['available_from'];

            if (is_array($date)) {
                $year                          = $date['year'];
                $month                         = $date['month'];
                $day                           = $date['day'];
                $productData['available_from'] = $dateTime->setDate($year, $month, $day)->setTime(0, 0, 0)->getTimestamp();
            } elseif (is_string($date)) {
                $productData['available_from'] = strtotime($date);
            }
        }

        if ($productData['available_till']) {
            $date = $productData['available_till'];

            if (is_array($date)) {
                $year                          = $date['year'];
                $month                         = $date['month'];
                $day                           = $date['day'];
                $productData['available_till'] = $dateTime->setDate($year, $month, $day)->setTime(23, 59, 59)->getTimestamp();
            } elseif (is_string($date)) {
                $productData['available_till'] = strtotime($date);
            }
        }

        if ($productData['cat_in_home'] || $productData['dog_in_home']) {
            $productData['pet_in_home'] = 1;
            $product->setPetInHome(1);
            $product->setEAVAttributeValue('exposed_cat', (int) min($productData['cat_in_home'], 1));
            $product->setEAVAttributeValue('exposed_dog', (int) min($productData['dog_in_home'], 1));
        } else {
            $product->setPetInHome(0);
            $product->setEAVAttributeValue('exposed_cat', 0);
            $product->setEAVAttributeValue('exposed_dog', 0);
        }

        if (!is_null($productData['is_smoke'])) {
            $productData['is_smoke_free'] = !(bool) $productData['is_smoke'];
            $product->setIsSmokeFreeHome((int) $productData['is_smoke_free']);
        }

        $productData['is_pick_up_available'] = (bool) $productData['pickup_address']['is_pick_up_available'];
        $productData['pickup_coubtry_code']  = $productData['pickup_address']['country_code'];

        $productData['shipping_cover'] = ($productData['pickup_address']['absorb_shipping']) ? (int) $productData['pickup_address']['amount'] : null;

        $productData['is_delivery_available'] = 1;
        //$productData['user_id']               = User_Model_Session::instance()->getUserId();

        if (isset($productData['selling_type'])) {
            // if selling type is 'set' then it is assumed as 1 item
            if ($productData['selling_type'] == \Product_Model_Product::SELLING_TYPE_SET) {
                $productData['number_of_items_in_set'] = $productData['qty'];
                $productData['qty']                    = 1;
            } else {
                $productData['number_of_items_in_set'] = null;
            }
        }

        if ($productData['has_scratches']) {
            $productData['condition'] = 'satisfactory';
        }

        $product->addData($productData);
        return $productData;
    }

    public function isValid($data)
    {
        $this->setCustomErrorMessages();

        $this->beforeIsValid($data);

        $result = parent::isValid($data);

        $this->afterIsValid($data);

        return $result;
    }

    public function populateByProduct(\Product_Model_Product $product)
    {
        return $this->populate($this->getFormData($product));
    }

    protected function getFormData(\Product_Model_Product $product)
    {
        $antiqueSet = new Application_Model_Set();
        $antiqueSet->getBy_name('Vintage + Antiques');

        $kidsSet = new Application_Model_Set();
        $kidsSet->getBy_name('Kid\'s Corner');

        $data                   = $product->getData();
        $data['price']          = (float) $product->getPrice();
        $data['original_price'] = (float) $product->getOriginalPrice();
        $data['width']          = (float) $product->getWidth();
        $data['height']         = (float) $product->getHeight();
        $data['depth']          = (float) $product->getDepth();

        $data['manufacturer']         = $product->getManufacturer()->getTitle();
        $data['subcategory_id']       = $product->getCategory()->getId();
        $data['category_id']          = $product->getCategory()->getParentId();
        $data['is_antiques']          = (int) $antiqueSet->hasProduct($product);
        $data['is_kids_corner']       = (int) $kidsSet->hasProduct($product);
        $data['has_scratches']        = $product->getEAVAttributeValue('has_scratches');
        $data['cat_in_home']          = (int) $product->getEAVAttributeValue('exposed_cat');
        $data['dog_in_home']          = (int) $product->getEAVAttributeValue('exposed_dog');
        $data['is_pick_up_available'] = $product->getIsPickUpAvailable(0);

        $address                         = $product->getPickUpAddress();
        $addressData                     = $address->getData();
        $addressData['telephone_number'] = $address->getPhoneNumber($address->getTelephoneNumber());
        $addressData['postal_code']      = $address->getPostcode($address->getPostalCode());

        $addressData['selling_type'] = $product->getSellingType();

        $data['pickup_address'] = $addressData;

        $data['pickup_address']['absorb_shipping']      = (bool) $product->getShippingCover();
        $data['pickup_address']['amount']               = (int) $product->getShippingCover();
        $data['pickup_address']['is_pick_up_available'] = (int) $product->getIsPickUpAvailable();

        if ($product->getSellingType() === \Product_Model_Product::SELLING_TYPE_SET) {
            $data['qty'] = $product->getNumberOfItemsInSet(1);
        }

        return $data;
    }

    protected function initGeneralInfoBlock()
    {
        $this->addElement($this->getCategoryElement());
        $this->addElement($this->getSubCategoryElement());
        $this->addElement($this->getTitleElement());
        $this->addElement($this->getConditionElement());
        $this->addElement($this->getManufacturerElement());
        $this->addElement($this->getColorElement());
        $this->addElement($this->getWidthElement());
        $this->addElement($this->getHeightElement());
        $this->addElement($this->getDepthElement());
        $this->addElement($this->getMaterialElement());
        $this->addElement($this->getAgeElement());
        $this->addElement($this->getAntiqueElement());
        $this->addElement($this->getIsChildrenElement());
        $this->addElement($this->getIsUnder15LbsElement());
        $this->addElement($this->getHasScratchesElement());
        $this->addElement($description = $this->getDescriptionElement());
        // photos block after description textarea
        $description->addDecorator(
                'ViewScript', array(
            'viewScript' => 'form_decorator/photo-block.phtml',
            'viewModule' => 'product',
            'product'    => $this->product,
                )
        );

        $actionType = new Zend_Form_Element_Hidden('action_type');
        $actionType->setAttrib('id', 'action_type');
        $this->addElement($actionType);
    }

    protected function includeSubForms()
    {
        $this->addSubForm(
                new Product_Form_NewItem_ListingInfo, 'additional_details'
        );

        $this->addSubForm(
                new Product_Form_NewItem_Address(
                array(
            'elementsBelongTo' => 'pickup_address',
            'addresses'        => $this->getAddresses(),
                )
                ), 'pickup_address'
        );

        /* $this->addSubForm(
          new Product_Form_NewItem_SocialSharing(array('elementsBelongTo' => 'social_share')),
          'social_share'
          ); */

        $this->addSubForm(new Product_Form_QuizSubform, 'quizform');
    }

    protected function getDescriptionElement()
    {
        $description = new Zend_Form_Element_Textarea('description');
        $description->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim', 'Null'))
                ->setAttrib('class', 'form-field fit-field other-info-field pull-left')
                ->setAttrib('placeholder', "Provide details to reduce buyer question and sell your item faster.")
                ->setAttrib('rows', 5)
                ->addValidator('StringLength', false, array(array('max' => 60000)));

        $description->setDecorators(array(
            'ViewHelper',
            array(
                'label' => $this->getLabelDecoratorForElement(
                        $description, 'Other info', array('class' => 'form-label pull-left')
                )
            ),
            array(array('form_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
            'clear'  => $this->getHtmlTagDecorator(
                    array('tag' => 'div', 'class' => 'clear', 'placement' => 'APPEND')
            ),
            array(array('control_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group')),
            'clear2' => $this->getHtmlTagDecorator(
                    array('tag' => 'div', 'class' => 'clear', 'placement' => 'APPEND')
            ),
        ));

        return $description;
    }

    protected function getHasScratchesElement()
    {
        $hasScratches = $this->createLiCheckboxElement(
                'has_scratches', 'has scratches, stains or rips bigger than 1 inch'
        );

        $hasScratches->addDecorators(
                array(
                    array(
                        array('ul_container' => 'HtmlTag'),
                        array(
                            'tag'       => 'ul',
                            'closeOnly' => true // Opened in antique element
                        )
                    ),
                    array(
                        array('form_group' => 'HtmlTag'),
                        array(
                            'tag'       => 'div',
                            'closeOnly' => true // Opened in antique element
                        )
                    ),
                    array(
                        array('side_group' => 'HtmlTag'),
                        array(
                            'tag'       => 'div',
                            'closeOnly' => true // Opened in antique element
                        )
                    ),
                    array(
                        array('control_group' => 'HtmlTag'),
                        array('tag' => 'div', 'closeOnly' => true) //Opened in material element
                    )
        ));

        return $hasScratches;
    }

    protected function getIsUnder15LbsElement()
    {
        $isUnder15Lbs = $this->createLiCheckboxElement('is_under_15_pounds', '<i class="apt-i-weight"></i>less than 15lbs');
        return $isUnder15Lbs;
    }

    protected function getIsChildrenElement()
    {
        $isChildren = $this->createLiCheckboxElement('is_kids_corner', 'children\'s furniture');

        return $isChildren;
    }

    protected function getAntiqueElement()
    {
        $antiqueElement = $this->createLiCheckboxElement('is_antiques', 'antique / vintage item');
        $antiqueElement->addDecorators(
                array(
                    array(
                        array('ul_container' => 'HtmlTag'),
                        array(
                            'tag'      => 'ul',
                            'class'    => 'h-list',
                            'openOnly' => true // Should be closed in last li element
                        )
                    ),
                    array('common_label' => $this->getLabelDecoratorForElement(
                                $antiqueElement, 'Check if the item is...', array(
                            'class' => 'form-label sideb-label',
                                )
                        )
                    ),
                    array(
                        array('form_group' => 'HtmlTag'),
                        array(
                            'tag'      => 'div',
                            'class'    => 'form-group',
                            'openOnly' => true // Should be closed in last li element
                        )
                    ),
                    array(
                        array('side_group' => 'HtmlTag'),
                        array(
                            'tag'      => 'div',
                            'class'    => 'col-form-group-side m-t12',
                            'openOnly' => true // Should be closed in last li element
                        )
                    ),
        ));

        return $antiqueElement;
    }

    protected function createLiCheckboxElement($name, $label = '')
    {
        $element = new Zend_Form_Element_Checkbox($name);
        $element->setRequired(false)
                ->addValidator('InArray', true, array(array(0, 1)));

        $element->setDecorators(array(
            'ViewHelper',
            array(array('checkbox_wrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'check-box')),
            'sublabel' => $this->getHtmlTagDecorator(array(
                'tag'       => 'span',
                'placement' => 'append',
                'content'   => $label,
            )),
            array(array('li_wrapper' => 'HtmlTag'), array('tag' => 'li')),
        ));

        return $element;
    }

    protected function getAgeElement()
    {
        $age = new Zend_Form_Element_Select('age');

        $options = array(1 => '<1', 2 => '1 - 2', 3 => '2 - 3', 4 => '3 - 4', 5 => '4 - 5', 6 => '5+');
        $select  = array('' => '-- Select --');

        $age->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($options)))
                ->addFilter('Null')
                ->setAttrib('class', 'form-field field-i time left')//time left
                ->addMultiOptions($select + $options);

        $age->setDecorators(array(
            'ViewHelper',
            $this->getLabelDecoratorForElement($age, 'Years old*'),
            array(
                array('col_group' => 'HtmlTag'),
                array('tag' => 'div', 'class' => 'form-group col-form-group')
            ),
            array(
                array('col_form_group' => 'HtmlTag'),
                array('tag' => 'div', 'closeOnly' => true) //Opened in material element
            )
        ));

        return $age;
    }

    protected function getMaterialElement()
    {
        $material   = new Zend_Form_Element_Select('material_id');
        $collection = new Application_Model_Material_Collection();
        $options    = $collection->getOptions();
        $select     = array('' => 'Select Material');

        $material->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($options)))
                ->addFilter('Null')
                ->setAttrib('class', 'form-field field-i cube left')//cube left
                ->setMultiOptions($select + $options);

        $material->setDecorators(array(
            'ViewHelper',
            $this->getLabelDecoratorForElement($material, 'Material*'),
            array(
                array('col_group' => 'HtmlTag'),
                array('tag' => 'div', 'class' => 'form-group col-form-group')
            ),
            array(
                array('col_form_group' => 'HtmlTag'),
                array('tag' => 'div', 'class' => 'col-form-group-side', 'openOnly' => true) //should be closed in Age element
            ),
            array(
                array('control_group' => 'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group', 'openOnly' => true) //should be closed in scratches element
            )
        ));

        return $material;
    }

    protected function getDepthElement()
    {
        $depth = $this->createDimensionElement('depth');
        $depth->addDecorators(array(
            'field_block' => $this->getHtmlTagDecorator(
                    array('tag' => 'div', 'closeOnly' => true)
            ),
            array(
                array(
                    'col_group' => 'HtmlTag'
                ),
                array('tag' => 'div', 'closeOnly' => true) //Opened in getWidthElement
            ),
            array(
                array('description' => $this->getHtmlTagDecorator(
                            array('tag' => 'small', 'class' => 'field-helper', 'placement' => 'APPEND'), 'Providing dimensions will help buyers know if the item will fit in their space and help you sell your item faster!'
                    )
                )
            ),
            array(
                array(
                    'control_group' => 'HtmlTag'
                ),
                array('tag' => 'div', 'closeOnly' => true) //Opened in getWidthElement
            ),
            array(
                array(
                    'control_block' => 'HtmlTag'
                ),
                array('tag' => 'div', 'closeOnly' => true) //Opened in Category element
            ),
        ));

        return $depth;
    }

    protected function getHeightElement()
    {
        $height = $this->createDimensionElement('height');
        return $height;
    }

    protected function getWidthElement()
    {
        $width = $this->createDimensionElement('width');

        $width->addDecorators(
                array(
                    'field_block' => $this->getHtmlTagDecorator(
                            array('tag' => 'div', 'class' => 'form-field-block', 'openOnly' => true)
                    ),
                    array('label' => $this->getLabelDecoratorForElement($width, 'Dimensions*')),
                    array(
                        array('col_group' => 'HtmlTag'),
                        array(
                            'tag'      => 'div',
                            'class'    => 'form-group',
                            'openOnly' => true, // should be closed on depth element
                        )
                    ),
                    array(
                        array('control_group' => 'HtmlTag'),
                        array(
                            'tag'      => 'div',
                            'class'    => 'control-group',
                            'openOnly' => true, // should be closed on depth element
                        )
                    )
                )
        );

        return $width;
    }

    protected function createDimensionElement($name)
    {
        $dimension = new Zend_Form_Element_Text($name);

        $dimension->setRequired(false)
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->setAttrib('class', 'form-field form-field-sm')
                ->setAttrib('placeholder', '0 inch(es)');

        $dimension->setDecorators(array(
            'ViewHelper',
            array(
                'sublabel' => $this->getHtmlTagDecorator(
                        array(
                    'tag'       => 'span',
                    'placement' => 'PREPEND'
                        ), ucfirst($name)
                )
            ),
            array(array('dim' => 'HtmlTag'), array('tag' => 'div', 'class' => 'dim')),
        ));

        return $dimension;
    }

    protected function getColorElement()
    {
        $color = new Ikantam_Form_Element_MultiRadio('color_id');

        $collection = new Application_Model_Color_Collection();
        $values     = $collection->getOptions2();

        $colorIdMap = $collection->getColumns(array('element_id'), true); // [id] => ['element_id' => 'some_element_id']

        $color->setRequired(true)
                ->addValidator('InArray', true, array(array_keys($values)))
                ->setMultiOptions($collection->getOptions());

        $color->setDecorators(array(
            'ViewHelper',
            'field_block' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'form-field-block')),
            'label'       => $this->getLabelDecoratorForElement($color, 'Color*'),
            array(array('col_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
            array(array('control_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group'))
        ));

        $color->addMultiDecorator($this->getHtmlTagDecorator(array(
                    'tag'   => 'div',
                    'class' => 'picolor',
                    'id'    => array('callback' => function($self) use ($colorIdMap) {
                    $data = $self->getOption('__current_multi_data');
                    return $colorIdMap[$data['value']]['element_id'];
                }),
        )));

        $color->addMultiDecorator($this->getHtmlTagDecorator(array(
                    'tag'          => 'div',
                    'class'        => 'addItemcolor ttip',
                    'data-tooltip' => array('callback' => function($self) {
                    $data = $self->getOption('__current_multi_data');
                    return $data['label'];
                }),
        )));

        $color->setSeparator('');

        return $color;
    }

    protected function getManufacturerElement()
    {
        $manufacturer = new Zend_Form_Element_Text('manufacturer');

        $manufacturer->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('max' => 255)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->setAttrib("autocomplete", "off")
                ->setAttrib('class', 'form-field block-field field-i search right')
                ->setAttrib('placeholder', 'Enter Brand Name or type \'Unknown\'');

        $manufacturer->setDecorators(array(
            'ViewHelper',
            array('label' => $this->getLabelDecoratorForElement($manufacturer, 'Brand*')),
            array(array('colGroup' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
            array('description' => $this->getHtmlTagDecorator(
                        array(
                    'class'     => 'field-helper',
                    'tag'       => 'small',
                    'placement' => 'APPEND',
                    'id'        => 'brand_characters_left',
                        ), 'Max 50 characters: 50 left.'
                )),
            array(array('control_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group'))
        ));

        return $manufacturer;
    }

    protected function getConditionElement()
    {
        $conditionElement = new Ikantam_Form_Element_Radio('condition');

        $conditionElement = new Zend_Form_Element_Select('condition');
        $values           = $this->getConditions();
        $select           = array('' => 'Select Condition');

        $conditionElement->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addFilters(array('Null'))
                ->setAttrib('class', 'form-field')
                ->setAttrib('id', 'condition_select')
                ->setMultiOptions($select + $values);


        $conditionElement->setDecorators(
                array(
                    'ViewHelper',
                    array('label' => $this->getLabelDecoratorForElement($conditionElement, 'Condition*')),
                    array(array('colGroup' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
                    array('description' => $this->getHtmlTagDecorator(
                                array(
                            'class'     => 'field-helper',
                            'tag'       => 'small',
                            'placement' => 'APPEND',
                                ), 'Please accurately describe the condition of your item.'
                        )),
                    array(array('control_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group'))
                )
        );

        return $conditionElement;
    }

    protected function getTitleElement()
    {
        $titleElement = new Zend_Form_Element_Text('title');

        $titleElement->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags'))
                ->addFilter(new Ikantam_Filter_Ascii())
                ->addFilter('Null')
                ->setAttrib('class', 'form-field block-field')
                ->setAttrib('placeholder', 'e.g. Crate and Barrel Red Leather Dining Chairs')
                ->setAttrib('maxlength', 255);

        $titleElement->setDecorators(
                array_merge(
                        array(
            'ViewHelper',
            $this->getLabelDecoratorForElement($titleElement, 'Title*'),
                        ), $this->defaultInputWrappers
                )
        );

        // user can't change title for approved product
        $request = Zend_Controller_Front::getInstance()->getRequest();
        if ($request->getModuleName() == 'product' && $request->getControllerName() == 'edit' && $request->getParam('id')) {
            $product = new Product_Model_Product($request->getParam('id'));
            if ($product->getIsApproved() == 1) {
                $titleElement->setAttrib('disabled', 'disabled');
                $titleElement->setRequired(false);
            }
        }

        return $titleElement;
    }

    protected function getCategoryElement()
    {
        $category   = new Zend_Form_Element_Select('category_id');
        $collection = new Category_Model_Category_Collection();
        $values     = $collection->getOptions3();
        $select     = array('' => 'Select Category');

        $category->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addFilters(array('Null'))
                ->setAttrib('class', 'form-field')
                ->setAttrib('id', 'category_id')
                ->setMultiOptions($select + $values);

        $category->setDecorators(array(
            'ViewHelper',
            $this->getLabelDecoratorForElement($category, 'Category*'),
            array(
                array('colGroup' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group col-form-group')
            ),
            array(
                array(
                    'control_group' => 'HtmlTag'
                ),
                array('tag' => 'div', 'class' => 'control-group', 'openOnly' => true) // should be closed in subcategory element
            ),
            array(
                array(
                    'control_block' => 'HtmlTag'
                ),
                array('tag' => 'div', 'class' => 'control-block', 'openOnly' => true) // should be closed in depth element
            ),
        ));

        return $category;
    }

    protected function getSubCategoryElement()
    {
        $category   = new Zend_Form_Element_Select('subcategory_id');
        $collection = new Category_Model_Category_Collection();
        $values     = $collection->getOptions4();
        $select     = array('' => 'Select Subcategory');

        $category->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addFilters(array('Null'))
                ->setAttrib('class', 'form-field')
                ->setMultiOptions($select + $values);

        $category->setDecorators(array(
            'ViewHelper',
            $this->getLabelDecoratorForElement($category, null, array('content' => 'Subcategory*')),
            array(
                array('colGroup' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group col-form-group')
            ),
            'notation' => $this->getHtmlTagDecorator(
                    array(
                        'tag'       => 'small',
                        'placement' => 'append',
                        'class'     => 'field-helper',
                        'content'   => 'Electronics and mattresses cannot be sold on Aptdeco'
                    )
            ),
            array(
                array(
                    'control_group' => 'HtmlTag'
                ),
                array('tag' => 'div', 'closeOnly' => true) // opened in getCategoryElement
            ),
        ));

        return $category;
    }

    /**
     * Attaches tag decorator as label for element
     * @param Zend_Form_Element $element
     * @param string $value - label value
     * @param array $options (optional)
     * @return mixed
     */
    protected function getLabelDecoratorForElement(\Zend_Form_Element $element = null, $value = null, $options = array())
    {
        if (!is_string($value)) {
            $value = $element->getFullyQualifiedName();
        }

        $decorator = $this->getHtmlTagDecorator(
                array_merge(
                        array(
            'tag'       => 'label',
            'class'     => 'form-label',
            'content'   => '<strong>' . $value . '</strong>',
            'placement' => 'PREPEND',
                        ), $options
                )
        );

        return $element ? array('label_' . $element->getFullyQualifiedName() => $decorator) : $decorator;
    }

    /**
     * Prepares form options and decorators
     */
    protected function initFormDecorators()
    {
        $this->setOptions(array('class' => 'apt-form apt-v-last'));
        $this->addDecorator(
                        array(
                            'headerDec' => $this->getHtmlTagDecorator(
                                    array(
                                        'tag'       => 'h3',
                                        'placement' => 'prepend',
                                        'class'     => 'section-heading form-section',
                                        'content'   => '<span class="text-uc">Describe Your Item</span>'
                                    )
                            )
                        )
                )
                ->addDecorator('FormElements')
                ->addDecorator('Form');
    }

    protected function setCustomErrorMessages()
    {
        if (!$this->customErrorMessages['isSet']) {
            $customErrorMessages = $this->customErrorMessages;
            $this->elementsWalk(function($element) use ($customErrorMessages) {
                /** @var \Zend_Form_Element $element */
                foreach ($customErrorMessages['templates'] as $validatorName => $templates) {
                    if ($validator = $element->getValidator($validatorName)) {
                        foreach ($templates as $type => $message) {
                            if (method_exists($validator, 'setMessage')) {
                                $validator->setMessage($message, $type === '' ? null : $type);
                            }
                        }
                    }
                }
            });
            $this->customErrorMessages['isSet'] = true;
        }
    }

    /**
     * Uses to define special conditions
     *
     * @param array $data
     */
    protected function beforeIsValid(array $data)
    {
        /**
         * For the following categories, the minimum price should be $100:
            - Sofas
            - Armoires / Wardrobe
            - Beds

         * For decor category min price is 1
         */

        if (isset($data['category_id'])) {
            if ($data['category_id'] == 42) { // 42 - Décor
                $this->getSubForm('additional_details')->getElement('price')->getValidator('Between')->setMin(1);
            }

            if ($data['category_id'] == 31 || $data['category_id'] == 5) {  // 31 - Sofas, 5 - Beds
                $this->getSubForm('additional_details')->getElement('price')->getValidator('Between')->setMin(100);
            }
        }

        if (isset($data['subcategory_id'])) {
            if ($data['subcategory_id'] == 56) { // 56 - Armoires / Wardrobes
                $this->getSubForm('additional_details')->getElement('price')->getValidator('Between')->setMin(100);
            }

        }

        if (isset($data['original_price'])) {
            $originalPriceElement = $this->getSubForm('additional_details')->getElement('original_price');
            if ($originalPriceElement->isValid($data['original_price']) && null !== $originalPriceElement->getValue()) {
                $priceElement = $this->getSubForm('additional_details')->getElement('price');
                if (!$priceElement->getValidator('LessThan')) {
                    $this->customErrorMessages['isSet'] = false;
                    $priceElement->addValidator('LessThan', false, array($originalPriceElement->getValue()));
                }
            }
        }
    }

    /**
     * This method is executed after isValid execution
     *
     * @param array $data
     */
    protected function afterIsValid(array $data)
    {
        $this->elementsWalk(function($element) {
            /** @var \Zend_Form_Element $element */
            if ($element->hasErrors()) {
                $messages = $element->getMessages();
                $element->setAttrib('class', $element->getAttrib('class') . ' error');

                $element->setAttrib('class', $element->getAttrib('class') . ' ttip')
                        ->setAttrib('data-tooltip', array_shift($messages));
            }
        });
    }

}