<?php
class Product_Form_Decorator_Errors extends Zend_Form_Decorator_Abstract
{
    /**
     * Some controll groups have more than 1 input element each of this
     * elements have their own error class. 
     * This property describes which classes belongs to which elements.
     * @type array 
     */
    protected $_errorHtmlClassesMap = array(
        array('price' => 'inp-1st', 'original_price' => 'inp-2nd'),
        array('material_id' => 'inp-1st', 'age' => 'inp-2nd'),
    );
    
    /**
     * Additional options to modified decorator
     */
    protected $_additionalOptions = array(
        'price' => array('openOnly' => true),
        'original_price' => array('closeOnly' => true),
        'material_id' => array('openOnly' => true),
        'age' => array('closeOnly' => true),
    );
    
    protected $_errorClass = 'error';
    
    protected $_specialErrorClass = 'has-error';
    
    protected $_modifiedDecoratorName = 'group';
    
    protected $_modifiedDecoratorClass = 'control-group';
    
    protected $_rendered =  array ();
    
    public function render($content)
    {    
        if(!($form = $this->getOption('form')) || !$form instanceof Zend_Form) {
            throw new Product_Form_Errors_Exception('Parent form is not set. Unable to get access to form elements.', 1);
        }        
        
        $element = $this->getElement();
        $view    = $element->getView();
        if (null === $view || in_array($element->getName(), $this->_rendered)) {
            return $content;
        }
        
        $errors = $element->getMessages();
        $isGrouped = $this->isGoupedElement($element);
        if (empty($errors)) {
            if(!$isGrouped) {
                return $content;   
            } 
        }

        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        
         $class = $this->getGroupErrorClassForElement($element);
         
        if($decorator = $element->getDecorator($this->_modifiedDecoratorName)) {     
            $decorator->setOption('class', $class);
            if(isset($this->_additionalOptions[$element->getName()])) {
                foreach($this->_additionalOptions[$element->getName()] as $option => $value) {
                   $decorator->setOption($option, $value); 
                }
            }
        }

        return str_replace($this->_modifiedDecoratorClass, $class, $content);
    } 
    
    /**
     * Return classes string for controll group
     * 
     * @param object $element  
     * @return string
     * @throws Product_Form_Errors_Exception - if form is not set
     */
    public function getGroupErrorClassForElement (\Zend_Form_Element $element)
    { 
        $form = $this->getOption('form');
        $result = $this->_modifiedDecoratorClass .' '. $this->_errorClass;
        $name = $element->getName();
        foreach($this->_errorHtmlClassesMap as $group) {
            if(array_key_exists($name, $group)) {
                $result = $this->_modifiedDecoratorClass .' '. $this->_specialErrorClass;
                foreach($group as $name => $class) {
                    if(($siblingElement = $form->getElement($name)) && $siblingElement->hasErrors()) {
                        $result .= ' ' . $group[$name];
                        $this->_rendered[] = $name;
                    }                    
                }
            }
        }
        
        return $result;
    }    

    
    /**
     * Checks if siblings element exist
     * 
     * @param  Zend_Form_Element $element
     * @return bool
     */
    public function isGoupedElement (\Zend_Form_Element $element)
    {
        $name = $element->getName();
        foreach($this->_errorHtmlClassesMap as $group) {
            if(array_key_exists($name, $group)){
                return true;
            }
        }
        return false;
    }   
}