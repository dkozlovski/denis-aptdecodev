<?php

class Product_Form_Product extends Ikantam_Form
{

    protected $_session;
    protected $_addresses;

    const ERROR_SHIPPING_VALUE = 'Please, select shipping.';
    const ERROR_TITLE_EMPTY    = 'Title is required.';
    const ERROR_TITLE_LENGTH   = 'Title should be less than 255 characters.';

    public function __construct($options = null, $session = null)
    {
        $this->_session = $session;
        parent::__construct($options);
    }

    public function setAddresses($addresses)
    {
        $this->_addresses = $addresses;
        return $this;
    }

    public function getAddresses()
    {
        return $this->_addresses;
    }

    public function init()
    {
        $this->addSubform(new Product_Form_ProductSubform(), 'productSubform');
        $this->addSubform(new Product_Form_AddressSubform(), 'addressSubform');

        $this->setDecorators(array(
            'PrepareElements',
            array('ViewScript', array('viewScript' => 'add/form_decorator.phtml', 'session'    => $this->_session)),
            'Form'
        ));

        $this->setAttrib('id', 'submit-form')
                ->setAttrib('class', 'apt-form')
                ->setMethod('post')
                ->setAction(Ikantam_Url::getUrl('product/add/post'));

        $this
                ->addElement($this->getShippingCoverElement());
    }

    protected function getShippingCoverElement()
    {
        $shipping = new Zend_Form_Element_Checkbox('shipping_cover');
        $shipping->setRequired(true)
                ->setAttrib('id', 'shipping_cover');

        $shipping->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $shipping;
    }

}