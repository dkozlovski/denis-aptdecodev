<?php

class Product_Form_Filter extends Ikantam_Form
{

	const ERROR_CATEGORY_VALUE = 'Please, select a category';
	const ERROR_SUBCATEGORY_VALUE = 'Please, select a subcategory';
	const ERROR_COLOR_VALUE = 'Please, select a color';
	const ERROR_CONDITION_VALUE = 'Please, select condition.';
	const ERROR_MANUFACTURER_VALUE = 'Manufacturer is required.';
	const ERROR_MIN_PRICE_VALUE = 'Please, enter correct min price.';
	const ERROR_MAX_PRICE_VALUE = 'Please, enter correct max price.';

	public function init()
	{
		$this->addElement($this->getQueryElement())
				->addElement($this->getMinPriceElement())
				->addElement($this->getMaxPriceElement())
				->addElement($this->getConditionElement())
				->addElement($this->getColorElement())
				->addElement($this->getCategoryElement())
				->addElement($this->getSubCategoryElement())
				->addElement($this->getManufacturerElement());
	}

	protected function getQueryElement()
	{
		$price = new Zend_Form_Element_Text('query');

		$price->setRequired(true)
				->addValidator('NotEmpty', true)
				->addErrorMessage('Enter query.');

		return $price;
	}

	protected function getMinPriceElement()
	{
		$price = new Zend_Form_Element_Text('min_price');

		$priceFormat = new Zend_Validate_Float();
		$priceFormat->setMessage(self::ERROR_MIN_PRICE_VALUE);

		$price->setRequired(false)
				->addFilter('StringTrim')
				->addValidator($priceFormat, true);

		return $price;
	}

	protected function getMaxPriceElement()
	{
		$price = new Zend_Form_Element_Text('max_price');

		$priceFormat = new Zend_Validate_Float();
		$priceFormat->setMessage(self::ERROR_MAX_PRICE_VALUE);

		$price->setRequired(false)
				->addFilter('StringTrim')
				->addValidator($priceFormat, true);

		return $price;
	}

	protected function getConditionElement()
	{
		$currentCondition = new Zend_Form_Element_MultiCheckbox('conditions');

		$values = array('new'/*, 'like-new'*/, 'good', 'satisfactory', 'age-worn');

		$currentCondition->setRequired(false)
				->addValidator('InArray', true, array($values))
				->addErrorMessage(self::ERROR_CONDITION_VALUE);

		return $currentCondition;
	}

	protected function getColorElement()
	{
		$color = new Zend_Form_Element_MultiCheckbox('colors');

		$collection = new Application_Model_Color_Collection();
		$values = $collection->getOptions2();

		$color->setRequired(false)
				->addValidator('InArray', true, array($values))
				->addErrorMessage(self::ERROR_COLOR_VALUE);

		return $color;
	}

	protected function getCategoryElement()
	{
		$category = new Zend_Form_Element_Select('parent_category_id');

		$collection = new Category_Model_Category_Collection();
		$values = array_keys($collection->getOptions());

		$category->setRequired(false)
				->addValidator('InArray', true, array($values))
				->addErrorMessage(self::ERROR_CATEGORY_VALUE);

		return $category;
	}

	protected function getSubCategoryElement()
	{
		$category = new Zend_Form_Element_MultiCheckbox('categories');

		$collection = new Category_Model_Category_Collection();
		$values = array_keys($collection->getOptions2());

		$category->setRequired(false)
				->addFilters(array('StripTags', 'StringTrim'))
				->addValidator('InArray', true, array($values))
				->addErrorMessage(self::ERROR_SUBCATEGORY_VALUE);

		return $category;
	}

	protected function getManufacturerElement()
	{
		$manufacturer = new Zend_Form_Element_MultiCheckbox('brands');

		$collection = new Application_Model_Manufacturer_Collection();
		$values = array_keys($collection->getOptions());

		$manufacturer->setRequired(false)
				->addFilters(array('StripTags', 'StringTrim'))
				->addValidator('InArray', true, array($values));

		return $manufacturer;
	}

}