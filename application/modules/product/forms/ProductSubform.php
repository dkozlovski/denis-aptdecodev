<?php

class Product_Form_ProductSubform extends Zend_Form_SubForm
{

    const ERROR_AGE_VALUE             = 'Please, select age.';
    const ERROR_MATERIAL_VALUE        = 'Please, select a material';
    const ERROR_TITLE_EMPTY           = 'Title is required.';
    const ERROR_TITLE_LENGTH          = 'Title should be less than 255 characters.';
    const ERROR_QTY_FORMAT            = 'Please, enter correct quantity.';
    const ERROR_QTY_VALUE             = 'Quantity can not be 0.';
    const ERROR_CATEGORY_VALUE        = 'Please, select a category';
    const ERROR_SUBCATEGORY_VALUE     = 'Please, select a subcategory';
    const ERROR_ORIGINAL_PRICE_FORMAT = 'Please, enter correct original price.';
    const ERROR_ORIGINAL_PRICE_VALUE  = 'Original price should be greater than 0.';
    const ERROR_PRICE_FORMAT          = 'Please, enter correct price.';
    const ERROR_PRICE_VALUE           = 'Price should be greater than 0.';
    const ERROR_MANUFACTURER_EMPTY    = 'Manufacturer is required.';
    const ERROR_MANUFACTURER_LENGTH   = 'Manufacturer should be less than 255 characters.';
    const ERROR_CONDITION_VALUE       = 'Please, select condition.';
    const ERROR_COLOR_VALUE           = 'Please, select a color';
    const ERROR_DESCRIPTION_LENGTH    = 'Description should be less than 60000 characters.';
    const ERROR_DEPTH_FORMAT          = 'Please, enter correct depth.';
    const ERROR_DEPTH_VALUE           = 'Depth should be greater than 0.';
    const ERROR_HEIGHT_FORMAT         = 'Please, enter correct height.';
    const ERROR_HEIGHT_VALUE          = 'Height should be greater than 0.';
    const ERROR_WIDTH_FORMAT          = 'Please, enter correct width.';
    const ERROR_WIDTH_VALUE           = 'Width should be greater than 0.';
    const ERROR_AVAILABLE_FROM_FORMAT = 'Please, enter correct available from date.';
    const ERROR_AVAILABLE_TILL_FORMAT = 'Please, enter correct available till date.';

    protected $_errorTitleRequired = 'Enter Title!';

    public function init()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div')),
            'Form',
        ));

        $this->setAttrib('class', 'apt-form');

        $this->addElement($this->getTitleElement());
        $this->addElement($this->getPriceElement());
        $this->addElement($this->getOriginalPriceElement());

        $this->addDisplayGroup(
                array('price', 'original_price'), 'aprices', array('disableLoadDefaultDecorators' => true));

        $this->addElement($this->getQtyElement());
        $this->addElement($this->getCategoryElement());
        $this->addElement($this->getSubCategoryElement());
        $this->addElement($this->getManufacturerElement());
        $this->addElement($this->getConditionElement());
        $this->addElement($this->getAgeElement());
        $this->addElement($this->getMaterialElement());

        $this->addDisplayGroup(
                array('material', 'age'), 'bmaterialage', array('disableLoadDefaultDecorators' => true));

        $this->addElement($this->getColorElement());
        $this->addElement($this->getDimensionsNoteElement());
        $this->addElement($this->getWidthElement());
        $this->addElement($this->getHeightElement());
        $this->addElement($this->getDepthElement());

        $this->addDisplayGroup(
                array('dimensions_note', 'width', 'depth', 'height'), 'dimensions', array('disableLoadDefaultDecorators' => true));

        $this->addElement($this->getDescriptionElement());

        $this->addElement($this->getAvailableFromElement());
        $this->addElement($this->getAvailableTillElement());

        $this->addDisplayGroup(
                array('available_from', 'available_till'), 'zavailability', array('disableLoadDefaultDecorators' => true));

        $this->addElement($this->getIsPickupAvailableElement());
        $this->addElement($this->getReasonElement());
        $this->addElement($this->getToBeVerifiedElement());
        $this->addElement($this->getIsConciergeAvailableElement());


        $this->_preparePrices($this->getDisplayGroup('aprices'));
        $this->_prepareMaterialAge($this->getDisplayGroup('bmaterialage'));
        $this->_prepareDimensions($this->getDisplayGroup('dimensions'));
        $this->_prepareAvailability($this->getDisplayGroup('zavailability'));
    }

    protected function getAvailableFromElement()
    {
        $availableFrom = new Zend_Form_Element_Text('available_from');

        $dateFormat = new Zend_Validate_Date(array('format' => 'M dd, y'));
        $dateFormat->setLocale('en_US');

        $availableFrom->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($dateFormat, true)
                ->addErrorMessage(self::ERROR_AVAILABLE_FROM_FORMAT)
                ->setAttrib('id', 'datepicker')
                ->setAttrib('class', 'datepicker')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;')
                ->setDescription('<span class="span-label mdash"> &mdash;&mdash; </span>');

        $availableFrom->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors',
            array('HtmlTag', array('tag'   => 'div', 'class' => 'pull-left dp-holder')),
            array('Description', array('tag'       => 'div', 'class'     => 'pull-left', 'placement' => 'append', 'escape'    => false))
        ));

        return $availableFrom;
    }

    protected function getAvailableTillElement()
    {
        $availableTill = new Zend_Form_Element_Text('available_till');

        $dateFormat = new Zend_Validate_Date(array('format' => 'M dd, y'));
        $dateFormat->setLocale('en_US');

        $availableTill->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($dateFormat, true)
                ->addErrorMessage(self::ERROR_AVAILABLE_TILL_FORMAT)
                ->setAttrib('id', 'datepicker2')
                ->setAttrib('class', 'datepicker')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;');

        $availableTill->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors',
            array('HtmlTag', array('tag'   => 'div', 'class' => 'pull-left dp-holder'))
        ));
        return $availableTill;
    }

    protected function _preparePrices($dg)
    {
        $dg->addDecorator('FormElements');
        $dg->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'));
    }

    protected function _prepareAvailability($dg)
    {
        $dg->addDecorator('FormElements');
        $dg->addDecorator(array('wrap0' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'fit-els fit'));
        $dg->addDecorator(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields'));
        $dg->addDecorator(array('label' => 'DtDdWrapper'));
        $dg->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'));

        $ld = $dg->getDecorator('label');

        $ld->setOptions(array('dtLabel' => '<label for="Availableperiod" class="block-lable">
          <i class="aperiod"></i>
          <span class="getPopmsg">Available period</span>
          <div class="popup-msg">
            <h3>Available period</h3>
            <p>Specify dates when this product will be available for sale. Leave blank if you are not sure about it.</p>
          </div>
          </label>'));
    }

    protected function _prepareMaterialAge($dg)
    {
        $dg->addDecorator('FormElements');
        $dg->addDecorator(array('wrap0' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'fit-els'));
        $dg->addDecorator(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields'));
        $dg->addDecorator(array('label' => 'DtDdWrapper'));
        $dg->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'));

        $ld = $dg->getDecorator('label');

        $ld->setOptions(array('dtLabel' => '<label for="Material" class="block-lable">Material:*</label>'));
    }

    protected function getAgeElement()
    {
        $age = new Zend_Form_Element_Select('age');

        $values = array(0 => '-- Select --', 1 => '<1', 2 => '1 - 2', 3 => '2 - 3', 4 => '3 - 4', 5 => '4 - 5', 6 => '5+');

        $age->setRequired(false)
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addMultiOptions($values)
                ->addErrorMessage(self::ERROR_AGE_VALUE)
                ->setAttrib('id', 'age')
                ->setAttrib('class', 'select-styled age')
                ->setLabel('Years Old:*');

        $age->setDecorators(array(
            'ViewHelper',
            'Errors',
            array('HtmlTag', array('tag'   => 'div', 'class' => 'pull-right')),
            array('Label', array('tag'            => 'div', 'tagClass'       => 'els pull-right', 'placement'      => 'append', 'class'          => 'block-lable span-label text-mute', 'escape'         => false, 'requiredPrefix' => '<i class="aperiod"></i> ')),
        ));

        return $age;
    }

    protected function getMaterialElement()
    {
        $material = new Zend_Form_Element_Select('material');

        $collection = new Application_Model_Material_Collection();
        $values     = array(0 => '-- Select --');
        foreach ($collection->getOptions() as $id => $title) {
            $values[$id] = $title;
        }

        $material->setRequired(true)
                ->addValidator('InArray', true, array($collection->getOptions2()))
                ->addErrorMessage(self::ERROR_MATERIAL_VALUE)
                ->setMultiOptions($values)
                ->setAttrib('id', 'product-material')
                ->setAttrib('name', 'material_id')
                ->setAttrib('class', 'select-styled material');

        $material->setDecorators(array(
            'ViewHelper',
            'Errors',
            array('HtmlTag', array('tag'   => 'div', 'class' => 'btn-group pull-left', 'id'    => 'pick-material')),
        ));

        return $material;
    }

    protected function _prepareDimensions($dg)
    {
        $dg->addDecorator('FormElements');
        $dg->addDecorator(array('wrap0' => 'HtmlTag'), array('tag' => 'p'));
        $dg->addDecorator(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'controls', 'id'    => 'dimensions'));
        $dg->addDecorator(array('label' => 'DtDdWrapper'));
        $dg->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'));

        $ld = $dg->getDecorator('label');

        $ld->setOptions(array('dtLabel' => '<label for="Dimensions" class="block-lable">
                                <i class="aperiod"></i> <span class="getPopmsg">Dimensions*</span>
                                <div class="popup-msg">
                                    <h3>Dimension</h3>
                                    <p>The best thing to do is to give as much information upfront. Most buyers will need to know the exact dimensions of the product to make sure it will fit in their space. So make sure the dimensions you enter are close/exact</p>
                                </div>
                            </label>'));
    }

    protected function getDimensionsNoteElement()
    {
        $note = new Ikantam_Form_Element_Note('dimensions_note');

        $note->setRequired(false)
                ->setDescription('&nbsp;');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'i', 'class'     => 'dimen', 'escape'    => false)),
        ));

        return $note;
    }

    protected function getWidthElement()
    {
        $width = new Zend_Form_Element_Text('width');

        $float = new Zend_Validate_Float('en_US');

        $widthValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $widthValue->setMessage(self::ERROR_WIDTH_VALUE);

        $width->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator($float, true)
                ->addValidator($widthValue, true)
                ->addErrorMessage(self::ERROR_WIDTH_FORMAT)
                ->setAttrib('class', 'dimensions')
                ->setLabel('Width:')
                ->setDescription('"');

        $width->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors',
            array('Description', array('placement' => 'append', 'tag'       => 'span')),
        ));

        return $width;
    }

    protected function getHeightElement()
    {
        $height = new Zend_Form_Element_Text('height');

        $float = new Zend_Validate_Float('en_US');

        $heightValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $heightValue->setMessage(self::ERROR_HEIGHT_VALUE);

        $height->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator($float, true)
                ->addValidator($heightValue, true)
                ->addErrorMessage(self::ERROR_HEIGHT_FORMAT)
                ->setAttrib('class', 'dimensions')
                ->setLabel('Height:')
                ->setDescription('"');

        $height->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors',
            array('Description', array('placement' => 'append', 'tag'       => 'span')),
        ));

        return $height;
    }

    protected function getDepthElement()
    {
        $depth = new Zend_Form_Element_Text('depth');

        $float = new Zend_Validate_Float('en_US');

        $depthValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $depthValue->setMessage(self::ERROR_DEPTH_VALUE);

        $depth->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator($float, true)
                ->addValidator($depthValue, true)
                ->addErrorMessage(self::ERROR_DEPTH_FORMAT)
                ->setAttrib('class', 'dimensions')
                ->setLabel('Depth:')
                ->setDescription('"');

        $depth->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors',
            array('Description', array('placement' => 'append', 'tag'       => 'span')),
        ));

        return $depth;
    }

    protected function getDescriptionElement()
    {
        $description = new Zend_Form_Element_Textarea('description');

        $descriptionLength = new Zend_Validate_StringLength(array('max' => 60000));
        $descriptionLength->setMessage(self::ERROR_DESCRIPTION_LENGTH);

        $description->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($descriptionLength, true)
                ->setAttrib('rows', 4)
                ->setAttrib('cols', 80)
                ->setAttrib('maxlength', 60000)
                ->setAttrib('class', 'fit')
                ->setLabel('Anything else you\'d like to mention?');

        $description->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        return $description;
    }

    protected function getColorElement()
    {
        $color = new Ikantam_Form_Element_Color('color_id');

        $collection = new Application_Model_Color_Collection();
        $values     = $collection->getOptions2();

        $color->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_COLOR_VALUE)
                ->setMultiOptions($collection->getAsOptions())
                ->setLabel('Color:*');

        $color->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'addItemcolor')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));


        $color->setSeparator('</div><div class="addItemcolor">');

        return $color;
    }

    protected function getManufacturerElement()
    {
        $manufacturer = new Zend_Form_Element_Text('manufacturer');

        //$manufacturerEmpty = new Zend_Validate_NotEmpty();
        //$manufacturerEmpty->setMessage(self::ERROR_MANUFACTURER_EMPTY);

        $manufacturerLength = new Zend_Validate_StringLength(array('max' => 255));
        $manufacturerLength->setMessage(self::ERROR_MANUFACTURER_LENGTH);

        $manufacturer->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator($manufacturerLength, true)
                ->setAttrib("autocomplete", "off")
                //->addValidator('NotEmpty')
                ->setAttrib('class', 'form-field')
                ->setAttrib('id', 'product-manufacturer')
                ->setAttrib('placeholder', 'Enter brand name here or type Unknown')
                ->setLabel('<span class="getPopmsg">Brand or manufacturer*</span>
                    <div class="popup-msg"><h3>Brand or manufactuerer</h3>
                    <p>If you don\'t know the brand or manufacturer, please feel free to write "Unknown"</p>
                    </div>')
                ->setDescription('&nbsp;');


        $manufacturer->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'append', 'escape'    => false, 'tag'       => 'i', 'class'     => 'apt-icon-sm-magnify')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement'      => 'prepend', 'class'          => 'block-lable', 'escape'         => false, 'requiredPrefix' => '<i class="aperiod"></i> ')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        return $manufacturer;
    }

    protected function _getConditions()
    {
        return array(
            'new'          => 'New: Product hasn\'t been unwrapped from box',
            //'like-new'     => 'Like New: Product has been unwrapped from box but looks new & doesn\'t have any scratches, blemishes, etc.',
            'good'         => 'Good: Minor blemishes that most people won\'t notice',
            'satisfactory' => 'Satisfactory: Moderate wear and tear, but still has many good years left',
            'age-worn'     => 'Age-worn: Has lived a full life and has a "distressed" look with noticeable wear');
    }

    protected function getIsPickupAvailableElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('is_pickup_available');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_CONDITION_VALUE)
                ->setMultiOptions($multiOptions)
                ->setAttrib('name', 'condition')
                ->setLabel('Are you ok with buyers picking up on their own?')
                ->setDescription('<i class="apt-icon-sm-car"></i>');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array('Description', array('placement' => 'prepend', 'tag'       => 'li', 'escape'    => false)),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getReasonElement()
    {
        $multiOptions = array('moving'       => 'Moving', 'redecorating' => 'Redecorating');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('reason');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_CONDITION_VALUE)
                ->setMultiOptions($multiOptions)
                ->setAttrib('name', 'condition')
                ->setLabel('Why are you selling your stuff?');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getToBeVerifiedElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('is_verification_available');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_CONDITION_VALUE)
                ->setMultiOptions($multiOptions)
                ->setAttrib('name', 'condition')
                ->setLabel('Would you like to be AptDeco Verified?');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getIsConciergeAvailableElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('is_concierge_available');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_CONDITION_VALUE)
                ->setMultiOptions($multiOptions)
                ->setAttrib('name', 'condition')
                ->setLabel('Would you like us to consider you for our concierge program?');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getConditionElement()
    {
        $multiOptions = $this->_getConditions();
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('condition');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_CONDITION_VALUE)
                ->setMultiOptions($multiOptions)
                ->setAttrib('name', 'condition')
                ->setLabel('<span class="getPopmsg">Current condition:*</span>
          <div class="popup-msg">
            <h3>Current condition*</h3>
            <p>To maintain the best experience for you and the rest of our customers, we generally look for and accept products that are in great condition only. The only exception is products that are  Age-worn\' antiques.</p>
          </div>');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            // array('Description', array('placement' => 'append', 'escape'    => false, 'tag'       => 'i', 'class'     => 'apt-icon-sm-magnify')),
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'h-list')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement'      => 'prepend', 'class'          => 'block-lable', 'escape'         => false, 'requiredPrefix' => '<i class="aperiod"></i> ')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getQtyElement()
    {
        $qty = new Zend_Form_Element_Text('qty');

        $int = new Zend_Validate_Int('en_US');

        $value = new Zend_Validate_GreaterThan(array('min' => 0));
        $value->setMessage(self::ERROR_QTY_VALUE);

        $qty->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator($int, true)
                ->addValidator($value, true)
                ->addErrorMessage(self::ERROR_QTY_FORMAT)
                ->setValue(1)
                ->addValidator('NotEmpty')
                ->setAttrib('id', 'qty')
                ->setLabel('Qty:*');


        $qty->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'qty-form pull-left')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        return $qty;
    }

    protected function getPriceElement()
    {
        $price = new Zend_Form_Element_Text('price');

        $priceFormat = new Zend_Validate_Float('en_US');
        $priceFormat->setMessage(self::ERROR_PRICE_FORMAT);

        $priceValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $priceValue->setMessage(self::ERROR_PRICE_VALUE);

        $price->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator($priceFormat, true)
                ->addValidator($priceValue, true)
                ->setAttrib('autocomplete', 'off')
                ->addValidator('NotEmpty')
                ->setAttrib('id', 'product-price')
                ->setAttrib('name', 'price')
                ->setLabel('Price:*')
                ->setDescription('$');

        $price->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'span', 'class'     => 'add-on')),
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'input-prepend pull-left')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields w-375'))
        ));

        return $price;
    }

    protected function getOriginalPriceElement()
    {
        $originalPrice = new Zend_Form_Element_Text('original_price');

        $priceFormat = new Zend_Validate_Float('en_US');
        $priceFormat->setMessage(self::ERROR_PRICE_FORMAT);

        $priceValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $priceValue->setMessage(self::ERROR_ORIGINAL_PRICE_VALUE);

        $originalPrice->setRequired(false)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator($priceFormat, true)
                ->addValidator($priceValue, true)
                ->addErrorMessage(self::ERROR_ORIGINAL_PRICE_FORMAT)
                ->setAttrib('id', 'original-price')
                ->setAttrib('name', 'original_price')
                ->setLabel('Original Price:')
                ->setDescription('$');

        $originalPrice->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'span', 'class'     => 'add-on')),
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'input-prepend pull-left')),
            array('Label', array('tag'       => 'div', 'tagClass'  => 'els pull-left', 'placement' => 'prepend', 'class'     => 'span-label text-mute m-0 p-10')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields w-375')),
        ));

        return $originalPrice;
    }

    protected function getCategoryElement()
    {
        $category = new Zend_Form_Element_Select('category_id');

        $collection = new Category_Model_Category_Collection();
        $values     = array_keys($collection->getOptions3());

        $multiOptions[0] = '-- Select --';

        foreach ($collection->getOptions3() as $key => $value) {
            $multiOptions[$key] = $value;
        }

        $category->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_CATEGORY_VALUE)
                ->setMultiOptions($multiOptions)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty')
                ->setAttrib('class', 'form-field')
                ->setLabel('Category:*');


        $category->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        return $category;
    }

    protected function getSubCategoryElement()
    {
        $category = new Zend_Form_Element_Select('subcategory_id');

        $collection = new Category_Model_Category_Collection();
        $values     = array_keys($collection->getOptions4());

        $category->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_SUBCATEGORY_VALUE)
                ->setMultiOptions(array_merge(array(0 => '-- Select --'), $collection->getOptions4()))
                ->setAttrib('style', 'zoom:1')
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty')
                ->setAttrib('class', 'form-field')
                ->setAttrib('id', 'product-subcategory')
                ->setLabel('Subcategory:*');

        $category->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        return $category;
    }

    protected function getTitleElement()
    {
        $fullName = new Zend_Form_Element_Text('title');

        $titleLength = new Zend_Validate_StringLength(array('max' => 255));
        $titleLength->setMessage(self::ERROR_TITLE_LENGTH);

        $fullName->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty')
                ->addErrorMessage($this->_errorTitleRequired)
                ->setAttrib('class', 'form-field')
                ->addValidator($titleLength, true)
                ->setAttrib('id', 'product_title')
                ->setAttrib('placeholder', '')
                ->setAttrib('maxlength', 255)
                ->setLabel('Product Title:*');

        $fullName->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        return $fullName;
    }

    public function isValid($data)
    {
        $valid = parent::isValid($data);

        foreach ($this->getElements() as $element) {
            if ($element->hasErrors()) {
                $element->addDecorator(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));
            }
        }

        return $valid;
    }

}