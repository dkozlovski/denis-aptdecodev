<?php

class Product_Form_Product2 extends Ikantam_Form
{

    protected $_addresses;
    protected $_errorHelper;

    public function init()
    {
        $this->_errorHelper = new Product_Form_ErrorHelper($this);

        $this->setAttrib('id', 'submit-form')
                ->setAttrib('class', 'apt-form')
                ->setMethod('post')
                ->setAction(Ikantam_Url::getUrl('product/add/new'));

        $this->addElement($this->getTitleElement());
        $this->addElement($this->getPriceElement());
        $this->addElement($this->getOriginalPriceElement());
        $this->addElement($this->getManageMyPricingElement());
        $this->addElement($this->getFeesElement());
        $this->addElement($this->getQtyElement());
        $this->addElement($this->getCategoryElement());
        $this->addElement($this->getSubCategoryElement());
        $this->addElement($this->getAntiquesElement());
        $this->addElement($this->getManufacturerElement());
        $this->addElement($this->getKidsCornerElement());
        $this->addElement($this->getConditionElement());
        $this->addElement($this->getMaterialElement());
        $this->addElement($this->getAgeElement());
        $this->addElement($this->getColorElement());
        $this->addElement($this->getDimensionsNoteElement());
        $this->addElement($this->getWidthElement());
        $this->addElement($this->getDepthElement());
        $this->addElement($this->getHeightElement());
        $this->addElement($this->getIsUnder15Element());
        $this->addElement($this->getActionTypeElement());

        $this->addSubform(new Product_Form_OptionalFieldsSubform(), 'additional_details');
        $this->addSubform(new Product_Form_AddressSubform(array('addresses' => $this->getAddresses())), 'pickup_address');
        $this->addSubForm(new Product_Form_QuizSubform(), 'quiz-form');

        // instead default decorators (HtmlTag dl...)
        /* $this->addDecorators(array(
          array('ViewScript', array('viewScript' => 'before_form_elements.phtml', 'viewModule' => 'product')),
          'FormElements',
          'Form',
          )); */
    }

    protected function getTitleElement()
    {
        $titleElement = new Zend_Form_Element_Text('title');

        $titleElement->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setAttrib('class', 'form-field')
                ->setAttrib('maxlength', 255)
                ->setLabel('Product Title:*');

        $titleElement->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')))),
        ));

        return $titleElement;
    }

    protected function getPriceElement()
    {
        $floatFormat = new Zend_Filter_LocalizedToNormalized(array('locale' => 'en_US', 'precision' => 2));

        $price = new Zend_Form_Element_Text('price');

        $price->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 50)))
                ->setAttrib('autocomplete', 'off')
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines'))
                ->addFilter($floatFormat)
                ->addFilter('Null')
                ->setLabel('Price:*')
                ->setAttrib('class', 'form-field m-0')
                ->setDescription('$');

        $price->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'span', 'class' => 'add-on')),
            array(array('wrap' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-prepend pull-left')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields w-375')),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')), 'openOnly' => true)),
        ));

        return $price;
    }

    protected function getOriginalPriceElement()
    {
        $floatFormat = new Zend_Filter_LocalizedToNormalized(array('locale' => 'en_US', 'precision' => 2));

        $originalPrice = new Zend_Form_Element_Text('original_price');

        $originalPrice->setRequired(false)
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines'))
                ->addFilter($floatFormat)
                ->addFilter('Null')
                ->setLabel('Original Price:')
                ->setAttrib('class', 'form-field m-0')
                ->setDescription('$');

        $originalPrice->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'span', 'class' => 'add-on')),
            array(array('wrap' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-prepend pull-left')),
            array('Label', array('tag' => 'div', 'tagClass' => 'els pull-left', 'placement' => 'prepend', 'class' => 'inner-label text-mute')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields w-375')),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true))
        ));

        return $originalPrice;
    }

    protected function getFeesElement()
    {
        $str = '<i class="apt-icon-warning orange v-top"></i>
            <p>
                <a class="text-ul none" target="_blank" href="' . Ikantam_Url::getUrl('faq/index') . '?q=fees">What are the fees for selling on AptDeco?</a>
            </p>';

        $note = new Ikantam_Form_Element_Note('fees_note');

        $note->setRequired(false)
                ->setLabel('&nbsp;')
                ->setDescription($str);

        $note->setDecorators(array(
            'ViewHelper',
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true)),
            array(array('blockFields' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true)),
            array('Description', array('placement' => 'prepend', 'tag' => 'div', 'escape' => false)),
            array(array('row3' => 'HtmlTag'), array('tag' => 'div', 'class' => 'aptd-alert success dynamic mxw-322')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable', 'escape' => false)),


        ));

        return $note;
    }

    protected function getQtyElement()
    {
        $qtyElement = new Zend_Form_Element_Text('qty');

        $qtyElement->setRequired(true)
                ->addValidator('NotEmpty')
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->addValidator('Int', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->setValue(1)
                ->setLabel('Qty:*');

        $qtyElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'qty-form pull-left')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')))),
        ));

        return $qtyElement;
    }

    protected function getCategoryElement()
    {
        $category   = new Zend_Form_Element_Select('category_id');
        $collection = new Category_Model_Category_Collection();
        $values     = $collection->getOptions3();
        $select     = array('' => '-- Select --');

        $category->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addFilters(array('Null'))
                ->setAttrib('class', 'form-field')
                ->setLabel('Category:*')
                ->setMultiOptions($select + $values);

        $category->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')))),
        ));

        return $category;
    }

    protected function getSubCategoryElement()
    {
        $category   = new Zend_Form_Element_Select('subcategory_id');
        $collection = new Category_Model_Category_Collection();
        $values     = $collection->getOptions4();
        $select     = array('' => '-- Select --');

        $category->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addFilters(array('Null'))
                ->setAttrib('class', 'form-field')
                ->setLabel('Subcategory:*')
                ->setMultiOptions($select + $values);

        $category->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')))),
        ));

        return $category;
    }

    protected function getAntiquesElement()
    {
        $options = array(0 => 'No', 1 => 'Yes');

        $isAntiquesElement = new Ikantam_Form_Element_Radio('is_antiques');

        $isAntiquesElement->setRequired(false)
                ->addValidator('InArray', true, array(array_keys($options)))
                ->setLabel('Is this an antique / vintage?')
                ->setMultiOptions($options);

        $isAntiquesElement->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group')),
        ));

        $isAntiquesElement->setSeparator('</li><li>');

        return $isAntiquesElement;
    }

    protected function getKidsCornerElement()
    {
        $options = array(0 => 'No', 1 => 'Yes');

        $isKidsCornerElement = new Ikantam_Form_Element_Radio('is_kids_corner');

        $isKidsCornerElement->setRequired(false)
                ->addValidator('InArray', true, array(array_keys($options)))
                ->setLabel('Is this item for children?')
                ->setMultiOptions($options);

        $isKidsCornerElement->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group')),
        ));

        $isKidsCornerElement->setSeparator('</li><li>');

        return $isKidsCornerElement;
    }

    protected function getManufacturerElement()
    {
        $manufacturer = new Zend_Form_Element_Text('manufacturer');

        $manufacturer->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('max' => 255)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->setAttrib("autocomplete", "off")
                ->setAttrib('class', 'form-field field-i search right')
                ->setAttrib('placeholder', 'Enter brand name here or type Unknown')
                ->setLabel('<span class="getPopmsg">Brand or manufacturer*</span>
                    <div class="popup-msg"><h3>Brand or manufactuerer</h3>
                    <p>If you don\'t know the brand or manufacturer, please feel free to write "Unknown"</p>
                    </div>');

        $manufacturer->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable', 'escape' => false, 'requiredPrefix' => '<i class="aperiod"></i> ')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')))),
        ));

        return $manufacturer;
    }

    protected function getConditionElement()
    {
        $multiOptions = $this->_getConditions();
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('condition');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($multiOptions)
                ->setLabel('<span class="getPopmsg">Current condition:*</span>
          <div class="popup-msg">
            <h3>Current condition*</h3>
            <p>To maintain the best experience for you and the rest of our customers, we generally look for and accept products that are in great condition only. The only exception is products that are  Age-worn\' antiques.</p>
          </div>');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'h-list')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable', 'escape' => false, 'requiredPrefix' => '<i class="aperiod"></i> ')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')))),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function _getConditions()
    {
        return array(
            'new'          => 'New: Product hasn\'t been unwrapped from box',
            //'like-new'     => 'Like New: Product has been unwrapped from box but looks new & doesn\'t have any scratches, blemishes, etc.',
            'good'         => 'Good: Minor blemishes that most people won\'t notice',
            'satisfactory' => 'Satisfactory: Moderate wear and tear, but still has many good years left',
            'age-worn'     => 'Age-worn: Has lived a full life and has a "distressed" look with noticeable wear');
    }

    protected function getMaterialElement()
    {
        $material   = new Zend_Form_Element_Select('material_id');
        $collection = new Application_Model_Material_Collection();
        $options    = $collection->getOptions();
        $select     = array('' => '-- Select --');

        $material->setRequired(true)
                ->addValidator('InArray', true, array(array_keys($options)))
                ->addFilter('Null')
                ->setAttrib('class', 'form-field w-128 field-i')//cube left
                ->setMultiOptions($select + $options)
                ->setLabel('Material:*');

        $material->setDecorators(array(
            'ViewHelper',
            array(array('wrap' => 'HtmlTag'), array('tag' => 'div', 'class' => 'btn-group pull-left first', 'id' => 'pick-material')),
            array(array('pull_left' => 'HtmlTag'), array('tag' => 'div', 'class' => 'pull-left')),
            array('Label', array('class' => 'block-lable', 'placement' => 'prepend')),
            array(array('width_div' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields w-390')),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')), 'openOnly' => true)),
        ));

        return $material;
    }

    protected function getAgeElement()
    {
        $age = new Zend_Form_Element_Select('age');

        $options = array(1 => '<1', 2 => '1 - 2', 3 => '2 - 3', 4 => '3 - 4', 5 => '4 - 5', 6 => '5+');
        $select  = array('' => '-- Select --');

        $age->setRequired(true)
                ->addValidator('InArray', true, array(array_keys($options)))
                ->addFilter('Null')
                ->setAttrib('class', 'form-field w-113 field-i')//time left
                ->setLabel('Years Old:*')
                ->addMultiOptions($select + $options);

        $age->setDecorators(array(
            'ViewHelper',
            array(array('second_wrap' => 'HtmlTag'), array('tag' => 'div', 'class' => 'second')),
            array('Label', array('class' => 'inner-label text-mute', 'placement' => 'prepend', 'tag' => 'div', 'tagClass' => 'els pull-left')),
            array(array('width_wrap' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields w-375')),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback')), 'closeOnly' => true)),
        ));

        return $age;
    }

    protected function getColorElement()
    {
        $color = new Ikantam_Form_Element_Color('color_id');

        $collection = new Application_Model_Color_Collection();
        $values     = $collection->getOptions2();

        $color->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($collection->getAsOptions())
                ->setLabel('Color:*');

        $color->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'div', 'class' => 'addItemcolor')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => array('callback' => array($this, 'errorCallback'))))
        ));

        $color->setSeparator('</div><div class="addItemcolor">');

        return $color;
    }

    protected function getDimensionsNoteElement()
    {
        $note = new Ikantam_Form_Element_Note('dimensions_note');

        $note->setRequired(false)
                ->setDescription('&nbsp;')
                ->setLabel('<i class="aperiod"></i>
        <span class="getPopmsg">Dimensions:</span>
        <div class="popup-msg">
            <h3>Dimension</h3>
            <p>The best thing to do is to give as much information upfront. Most buyers will need to know the exact dimensions of the product to make sure it will fit in their space. So make sure the dimensions you enter are close/exact</p>
        </div>');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'i', 'class' => 'dimen', 'escape' => false)),
            array(array('p' => 'HtmlTag'), array('tag' => 'p', 'openOnly' => true)),
            array(array('wrap' => 'HtmlTag'), array('tag' => 'div', 'class' => 'controls', 'id' => 'dimensions', 'openOnly' => true)),
            array('Label', array('class' => 'block-lable', 'placement' => 'prepend', 'escape' => false)),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group', 'openOnly' => true))
        ));

        return $note;
    }

    protected function getWidthElement()
    {
        $width = new Zend_Form_Element_Text('width');

        $width->setRequired(false)
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->setAttrib('class', 'dimensions')
                ->setLabel('Width:')
                ->setDescription('"');

        $width->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'append', 'tag' => 'span')),
            array('Label', array('placement' => 'prepend')),
        ));

        return $width;
    }

    protected function getDepthElement()
    {
        $depth = new Zend_Form_Element_Text('depth');

        $depth->setRequired(false)
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->setAttrib('class', 'dimensions')
                ->setLabel('Depth:')
                ->setDescription('"');

        $depth->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'append', 'tag' => 'span')),
            array('Label', array('placement' => 'prepend')),
        ));

        return $depth;
    }

    protected function getHeightElement()
    {
        $height = new Zend_Form_Element_Text('height');

        $height->setRequired(false)
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->setAttrib('class', 'dimensions')
                ->setLabel('Height:')
                ->setDescription('"');

        $height->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'append', 'tag' => 'span')),
            array('Label', array('placement' => 'prepend')),
            array(array('p' => 'HtmlTag'), array('tag' => 'p', 'closeOnly' => true)),
            array(array('wrap' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true)),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true))
        ));

        return $height;
    }

    /**
     * Converts form data to appropriate product data and set product data
     *
     * @param \Product_Model_Product $product
     * @return array - conversion result
     */
    public function convertProductData(\Product_Model_Product $product)
    {
        $productData  = $this->getValues();
        $manufacturer = new Application_Model_Manufacturer();
        $title        = $productData['manufacturer'];

        if (!empty($title)) {
            $al = new Manufacturer_Model_Association($title);

            if ($al->isExists()) {
                $manufacturer = $al->getManufacturer();
            } else {
                $manufacturer->getByTitle($title);
            }

            if (!$manufacturer->isExists()) {
                $manufacturer->setTitle($title)->save();
            }
        }

        $dateTime = new DateTime();

        $productData['manufacturer_id'] = $manufacturer->getId() ? : null;
        $productData['category_id']     = $productData['subcategory_id'];

        if ($productData['additional_details']['available_from']) {
            $date = $productData['additional_details']['available_from'];

            if (is_array($date)) {
                $year                          = $date['year'];
                $month                         = $date['month'];
                $day                           = $date['day'];
                $productData['available_from'] = $dateTime->setDate($year, $month, $day)->setTime(0, 0, 0)->getTimestamp();
            } elseif (is_string($date)) {
                $productData['available_from'] = strtotime($date);
            }
        }

        if ($productData['additional_details']['available_till']) {
            $date = $productData['additional_details']['available_till'];

            if (is_array($date)) {
                $year                          = $date['year'];
                $month                         = $date['month'];
                $day                           = $date['day'];
                $productData['available_till'] = $dateTime->setDate($year, $month, $day)->setTime(23, 59, 59)->getTimestamp();
            } elseif (is_string($date)) {
                $productData['available_till'] = strtotime($date);
            }
        }

        if ($productData['additional_details']['description']) {
            $productData['description'] = $productData['additional_details']['description'];
        }

        if (!is_null($productData['additional_details']['pet_in_home'])) {
            $productData['pet_in_home'] = (bool) $productData['additional_details']['pet_in_home'];
            $product->setIsPetInHome((int) $productData['pet_in_home']);
        }

        if (!is_null($productData['additional_details']['is_smoke_free'])) {
            $productData['is_smoke_free'] = (bool) $productData['additional_details']['is_smoke_free'];
            $product->setIsSmokeFreeHome((int) $productData['is_smoke_free']);
        }

        $productData['is_pick_up_available'] = (bool) $productData['additional_details']['is_pick_up_available'];

        $productData['shipping_cover']        = ($productData['pickup_address']['is_active']) ? (float) $productData['pickup_address']['amount'] : null;
        $productData['is_delivery_available'] = 1;
        //$productData['user_id']               = User_Model_Session::instance()->getUserId();

        unset($productData['additional_details']);

        $product->addData($productData);

        return $productData;
    }    
    
    
    public function getDimensionsErrorNoteElement()
    {
        $note = new Ikantam_Form_Element_Note('dimensions_error_note');

        $note->setRequired(false)
                ->setDescription('<i class="apt-icon-warning"></i><p>Please enter numeric values only</p>')
                ->setLabel('&nbsp;')
                ->setOptions(array('order' => 18));

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'div', 'class' => 'aptd-alert error', 'escape' => false, 'style' => 'width:auto;')),
            array('Label', array('class' => 'block-lable', 'placement' => 'prepend', 'escape' => false)),
            array(array('group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group'))
        ));

        return $note;
    }

    protected function getIsUnder15Element()
    {
         $options = array(0 => 'No', 1 => 'Yes');

        $isAntiquesElement = new Ikantam_Form_Element_Radio('is_under_15_pounds');

        $isAntiquesElement->setRequired(false)
                ->addValidator('InArray', true, array(array_keys($options)))
                ->setLabel('Is this item under 15 lbs?')
                ->setMultiOptions($options);

        $isAntiquesElement->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'v-inline-list')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class' => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group hidden')),
        ));

        $isAntiquesElement->setSeparator('</li><li>');

        return $isAntiquesElement;
    }

    protected function getActionTypeElement()
    {
        $currentCondition = new Zend_Form_Element_Hidden('action_type');
        $currentCondition->setRequired(false)->setValue('publish');

        $currentCondition->setDecorators(array(
            'ViewHelper',
        ));

        return $currentCondition;
    }

    protected function getManageMyPricingElement()
    {
        $checkbox = new Zend_Form_Element_Checkbox('is_manage_price_allowed');
        //$checkbox->setAttrib('class', 'aptdeco-styled-checkbox');
        $checkbox->addValidator('InArray', true, array(array(0, 1)));

        $checkbox->setDecorators(array(
                'ViewHelper',
                array('Callback', array('callback' => function($content, $element, array $options){
                        return    '<span class="getPopmsg">Manage my pricing</span> <i class="aperiod"></i>
                                  <span class="popup-msg">
                                    <span class="h3">Manage my pricing</span>
                                    <span class="p">
                                    Our pricing management feature allows AptDeco to reduce the price of your
                                    item within the last week of the item\'s listing. Prices are optimized based
                                    on sales in the same category and will never go below 50% of the initial price.
                                            Click <a href=""class="text-primary dark">here</a> for more information.
                                    </span>
                                  </span> ';
                    })),
                array(array('row3' => 'HtmlTag'), array('tag' => 'label', 'class' => 'block-lable m-b15')),
                array(array('row2' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields', 'openOnly' => true)),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group', 'openOnly' => true, 'id' => 'revenue-container')),
                array(array('spasing' => 'HtmlTag'), array('tag' => 'label', 'class' => 'block-lable', 'placement' => 'prepend')),
            ));

        return $checkbox;
    }

    public function _setNotRequired()
    {
        foreach ($this->getElements() as $element) {
            $element->setRequired(false)->setAllowEmpty(true);
        }

        foreach ($this->getSubforms() as $subform) {
            foreach ($subform->getElements() as $element) {
                $element->setRequired(false)->setAllowEmpty(true);
            }
        }
    }

    public function setAddresses($addresses)
    {
        $this->_addresses = $addresses;
        return $this;
    }

    public function getAddresses()
    {
        return $this->_addresses;
    }

    /**
     * Get the html classes string for controll group elements
     *
     * @param Zend_Form_Decorator_HtmlTag $decorator
     * @return string
     */
    public function errorCallback($decorator)
    {
        return $this->_errorHelper->getGroupClassForElement($decorator->getElement());
    }

    public function isValid($data)
    {
        $this->useSpecialValidationConditions();
        $isValid = parent::isValid($data);

        if ($this->getElement('width')->hasErrors() || $this->getElement('depth')->hasErrors() || $this->getElement('height')->hasErrors()) {
            $style     = 'border-color: #FF6666; box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;';
            $decorator = array('group' => 'HtmlTag');
            $options   = array(
                'tag'      => 'div',
                'class'    => 'control-group has-error',
                'openOnly' => true
            );

            $this->getElement('dimensions_note')->addDecorator($decorator, $options);

            if ($this->getElement('width')->hasErrors()) {
                $this->getElement('width')->setAttrib('style', $style);
            }

            if ($this->getElement('depth')->hasErrors()) {
                $this->getElement('depth')->setAttrib('style', $style);
            }

            if ($this->getElement('height')->hasErrors()) {
                $this->getElement('height')->setAttrib('style', $style);
            }
        }

        // temporary solution as long as the new submission form in development stage
        $priceElement = $this->getElement('price');
        if ($priceElement->hasErrors()) {
            if (in_array('notGreaterThan', $priceElement->getErrors())) {
                $priceElement->setAttrib('class', $priceElement->getAttrib('class') . ' ttip')
                    ->setAttrib('data-tooltip', 'Price should be higher than $50.');
            }
        }

        return $isValid;
    }

    /**
     * Uses to define special conditions
     */
    protected function useSpecialValidationConditions()
    {
        /**
         *  For decor category min price is 1
         */
        $chosenCategory = new Category_Model_Category($this->getValue('category_id'));
        if (in_array(strtolower($chosenCategory->getTitle()), array('décor', 'decor'))) {
            $this->getElement('price')->getValidator('GreaterThan')->setMin(1);
        }
    }

}