<?php

class Product_Form_AddressSubform extends Zend_Form_SubForm
{

    protected $_addresses;

    public function init()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div', 'id'  => 'shipping-address-form')),
        ));

        $this->addElement($this->getHeadingNoteElement())
                ->addElement($this->getFullNameElement())
                ->addElement($this->getAddressLine1Element())
                ->addElement($this->getAddressLine2Element())
                ->addElement($this->getCityElement())
                ->addElement($this->getStateElement())
                ->addElement($this->getPostcodeElement())
                ->addElement($this->getCountryCodeElement())
                //->addElement($this->getPhoneNoteElement())
                ->addElement($this->getPhoneNumberElement())
                ->addElement($this->getIsAbsorbingFeeElement())
                ->addElement($this->getAbsorbingAmountElement());
    }

    protected function getHeadingNoteElement()
    {
        $str = '<h3 class="apt-title section"><strong>Address</strong></h3>';

        $str .= '<p class="text-mute sub-title">
        For your protection, only your first name & parts of your address (city, state, & zip code) will be visible on the catalog.
    </p>';

        foreach ($this->getAddresses() as $address) {
            $str .= '<div class="aptd-alert success">';
            $str .= $address->toHtml();
            $str .= '<a href="javascript:void(0);" class="dark stick top-right" onclick=\'setPickupAddress(' . $address->getId() . ')\'>';
            $str .= 'Use this address <i class="apt-icon-sm-arrow"></i></a>';
            $str .= '</div>';
        }

        $note = new Ikantam_Form_Element_Note('heading_note');

        $note->setRequired(false)
                ->setDescription($str);

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'div', 'escape'    => false)),
        ));

        return $note;
    }

    protected function getFullNameElement()
    {
        $fullNameElement = new Zend_Form_Element_Text('full_name');

        $fullNameElement->setBelongsTo('pickup_address')
                ->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
                ->setAttrib('id', 'full_name')
                ->setAttrib('class', 'form-field')
                ->setLabel('Full Name*:');

        $fullNameElement->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'))
        ));

        return $fullNameElement;
    }

    protected function getAddressLine1Element()
    {
        $addressLine1Element = new Zend_Form_Element_Text('address_line1');

        $addressLine1Element->setBelongsTo('pickup_address')
                ->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
                ->setAttrib('id', 'address_line1')
                ->setAttrib('class', 'form-field')
                ->setLabel('Address Line1*:')
                ->setDescription('Street address, P.O. box, company name, c/o');

        $addressLine1Element->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'append', 'tag'       => 'small', 'class'     => 'field-helper text-mute', 'escape'    => false)),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement'      => 'prepend', 'class'          => 'block-lable', 'requiredPrefix' => '<i class="key"></i>', 'escape'         => false)),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        return $addressLine1Element;
    }

    protected function getAddressLine2Element()
    {
        $addressLine2Element = new Zend_Form_Element_Text('address_line2');

        $addressLine2Element->setBelongsTo('pickup_address')
                ->setRequired(false)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
                ->setAttrib('id', 'address_line2')
                ->setAttrib('class', 'form-field')
                ->setLabel('Address Line2:')
                ->setDescription('Apartment, suite, unit, building, floor, etc.');

        $addressLine2Element->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'append', 'tag'       => 'small', 'class'     => 'field-helper text-mute', 'escape'    => false)),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement'      => 'prepend', 'class'          => 'block-lable', 'requiredPrefix' => '<i class="key"></i>', 'escape'         => false)),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        return $addressLine2Element;
    }

    protected function getCityElement()
    {
        $cityElement = new Zend_Form_Element_Text('city');

        $cityElement->setBelongsTo('pickup_address')
                ->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
                ->setAttrib('id', 'city')
                ->setAttrib('class', 'form-field')
                ->setLabel('City*:');

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        return $cityElement;
    }

    protected function getStateElement()
    {
        $pickupState = new Zend_Form_Element_Text('state');

        $pickupState->setBelongsTo('pickup_address')
                ->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
                ->setAttrib('id', 'state')
                ->setAttrib('class', 'form-field w-175')
                ->setLabel('State/Province/Region:*');

        $pickupState->setDecorators(array(
            'ViewHelper',
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'pull-left')),
            array('Label', array('class'     => 'block-lable', 'placement' => 'prepend')),
            array(array('block' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields w-435')),
            array(array('group' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'control-group', 'openOnly' => true)),
        ));

        return $pickupState;
    }

    protected function getPostcodeElement()
    {
        $format = new Zend_Validate_PostCode(array('locale' => 'en_US'));

        $title = new Zend_Form_Element_Text('postal_code');

        $title->setBelongsTo('pickup_address')
                ->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator($format, true)
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
                ->setAttrib('id', 'postal_code')
                ->setAttrib('class', 'form-field w-90')
                ->setLabel('Zip:*');

        $title->setDecorators(array(
            'ViewHelper',
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'pull-right')),
            array('Label', array('tag'       => 'div', 'tagClass'  => 'pull-left', 'class'     => 'inner-label text-mute text-uc', 'placement' => 'prepend')),
            array(array('block' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields w-145')),
            array(array('group' => 'HtmlTag'), array('tag'       => 'div', 'closeOnly' => true)),
        ));

        return $title;
    }

    protected function getCountryCodeElement()
    {
        $options = $this->_getCountries();
        $select  = array('' => '-- Select --');

        $countryCodeElement = new Zend_Form_Element_Select('country_code');

        $countryCodeElement->setBelongsTo('pickup_address')
                ->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', false, array(array_keys($options)))
                ->addFilters(array('Null'))
                ->setAttrib('id', 'country_code')
                ->setAttrib('class', 'select-styled form-field')
                ->setLabel('Country*:')
                ->setValue('US')
                ->setMultiOptions($select + $options);

        $countryCodeElement->setDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        return $countryCodeElement;
    }

    protected function getPhoneNoteElement()
    {
        $note = new Ikantam_Form_Element_Note('phone_note');

        $note->setRequired(false)
                ->setDescription('Phone Number');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'strong')),
            array(array('data' => 'HtmlTag'), array('tag'   => 'h3', 'class' => 'apt-title section')),
        ));

        return $note;
    }

    protected function getPhoneNumberElement()
    {
        $title = new Zend_Form_Element_Text('telephone_number');

        $title->setBelongsTo('pickup_address')
                ->setRequired(true)
                ->addValidator('NotEmpty', true)
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
                ->setAttrib('id', 'telephone_number')
                ->setAttrib('class', 'form-field phone numb-format')
                ->setAttrib('placeholder', '(___) ___ ____')
                ->setLabel('Phone:*')
                ->setDescription('<i class="apt-icon-g-mobile"></i>');

        $title->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => '', 'escape'    => false)),
            array(array('data' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields phone-block pos-rltv')),
            array('Label', array('placement' => 'prepend', 'class'     => 'block-lable'/* , 'requiredPrefix' => '<i class="key"></i>' */, 'escape'    => false)),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group')),
        ));

        return $title;
    }

    protected function getIsAbsorbingFeeElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('is_active');

        $currentCondition->setBelongsTo('pickup_address')
                ->setRequired(false)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($multiOptions)
                ->setValue(0)
                ->setLabel('Would you like to significantly increase your chance of sale by covering the delivery fee?
                    <a target="_blank" href="' . Ikantam_Url::getUrl('cover-delivery/index') . '" class="text-prime">Why?</a>');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('ul' => 'HtmlTag'), array('tag'      => 'ul', 'class'    => 'v-inline-list', 'openOnly' => true)),
            array(array('block' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'block-fields', 'openOnly' => true)),
            array('Label', array('class'     => 'block-lable', 'placement' => 'prepend', 'escape'    => false)),
            array(array('group' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'control-group', 'id'       => 'absorb-group', 'openOnly' => true))
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getAbsorbingAmountElement()
    {
        $baseAmount = (float) Shipping_Model_Rate::getMaximumValueSellerAbsorb();
        $percents   = array(100, 50);

        foreach ($percents as $percent) {
            $values[$percent] = $percent . '% - $' . number_format($baseAmount * $percent / 100, 2);
        }

        $age = new Zend_Form_Element_Select('amount');

        $age->setBelongsTo('pickup_address')
                ->setRequired(false)
                ->addValidator('InArray', true, array(array_keys($values)))
                ->setAttrib('class', 'form-field w-188 align-list')
                ->addMultiOptions($values)
                ->setValue(100);

        $age->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('ul' => 'HtmlTag'), array('tag'       => 'ul', 'class'     => 'v-inline-list', 'closeOnly' => true)),
            array(array('block' => 'HtmlTag'), array('tag'       => 'div', 'class'     => 'block-fields', 'closeOnly' => true)),
            array(array('group' => 'HtmlTag'), array('tag'       => 'div', 'class'     => 'control-group', 'closeOnly' => true))
        ));

        return $age;
    }

    protected function _prepareStatePostcode($dg)
    {
        $dg->addDecorator('FormElements');
        $dg->addDecorator(array('wrap0' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'fit-els'));
        $dg->addDecorator(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields'));
        $dg->addDecorator(array('label' => 'DtDdWrapper'));
        $dg->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group'));

        $ld = $dg->getDecorator('label');

        $ld->setOptions(array('dtLabel' => '<label for="shipping-state" class="block-lable">State/Province/Region:*</label>'));
    }

    public function isValid($data)
    {
        $isValid = parent::isValid($data);

        if ($this->getElement('full_name')->hasErrors()) {
            $this->getElement('full_name')->addDecorator(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));
        }

        if ($this->getElement('address_line1')->hasErrors()) {
            $this->getElement('address_line1')->addDecorator(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));
        }

        if ($this->getElement('address_line2')->hasErrors()) {
            $this->getElement('address_line2')->addDecorator(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));
        }

        if ($this->getElement('city')->hasErrors()) {
            $this->getElement('city')->addDecorator(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));
        }

        if ($this->getElement('state')->hasErrors() || $this->getElement('postal_code')->hasErrors()) {
            $class = 'control-group has-error';

            if ($this->getElement('state')->hasErrors()) {
                $class .= ' inp-1st';
            }

            if ($this->getElement('postal_code')->hasErrors()) {
                $class .= ' inp-2nd';
            }

            $this->getElement('state')->addDecorator(array('group' => 'HtmlTag'), array('tag'      => 'div', 'class'    => $class, 'openOnly' => true));
        }

        if ($this->getElement('country_code')->hasErrors()) {
            $this->getElement('country_code')->addDecorator(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));
        }

        if ($this->getElement('telephone_number')->hasErrors()) {
            $this->getElement('telephone_number')->addDecorator(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group error'));
        }

        return $isValid;
    }

    public function setAddresses($addresses)
    {
        $this->_addresses = $addresses;
        return $this;
    }

    public function getAddresses()
    {
        return $this->_addresses ? : array();
    }

    protected function _getCountries()
    {
        $countries = new Application_Model_Country_Collection();
        return $countries->getOptions();
    }

}