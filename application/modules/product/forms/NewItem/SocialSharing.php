<?php
/**
 * Author: Alex P.
 * Date: 10.07.14
 * Time: 13:57
 */

class Product_Form_NewItem_SocialSharing extends Ikantam_Form
{

    public function init()
    {
        $this->initFormDecorators();
        $this->addElement($this->getIsShareEnabledElement());
        $this->addElement($this->getEmailListElement());
    }

    protected function getIsShareEnabledElement()
    {
        $checkbox = new Zend_Form_Element_Checkbox('share_with_socials');
        $checkbox->addValidator('InArray', false, array(array(0, 1)));

        $checkbox->setDecorators(array(
                'ViewHelper',
            ));

        return $checkbox;
    }

    protected function getEmailListElement()
    {
        $textarea = new Zend_Form_Element_Textarea('social_email_list');

        $textarea->setRequired(false)
            ->addValidator(
                'Regex',
                false,
                array('/^[\W]*([\w+\-.%]+@[\w\-.]+\.[A-Za-z]{2,4}[\W]*,{1}[\W]*)*([\w+\-.%]+@[\w\-.]+\.[A-Za-z]{2,4})[\W]*$/')
            )
            ->addFilter('PregReplace', array('match' =>'/\s/', 'replpace' => ''))
            ->setAttrib('class', 'form-field fit-field')
            ->setAttrib('rows', 5)
            ->setAttrib('placeholder', 'Enter email addresses...');

        $textarea->setDecorators(array(
                'ViewHelper',
            ));

        return $textarea;
    }

    /**
     * Attaches tag decorator as label for element
     * @param Zend_Form_Element $element
     * @param string $value - label value
     * @param array $options (optional)
     * @return mixed
     */
    protected function getLabelDecoratorForElement(\Zend_Form_Element $element = null, $value = null, $options = array())
    {
        if (!is_string($value)) {
            $value = $element->getFullyQualifiedName();
        }

        $decorator = $this->getHtmlTagDecorator(
            array_merge(
                array(
                    'tag' => 'label',
                    'class' => 'form-label',
                    'content' => '<strong>' . $value . '</strong>',
                    'placement' => 'PREPEND',
                ),
                $options
            )
        );

        return $element ? array('label_' . $element->getFullyQualifiedName() => $decorator) : $decorator;
    }

    /**
     * Prepares form options and decorators
     */
    protected function initFormDecorators()
    {
        $this /*->addDecorator(
            array(
                'headerDec' => $this->getHtmlTagDecorator(
                        array(
                            'tag' => 'h3',
                            'placement' => 'prepend',
                            'class' => 'section-heading form-section',
                            'content' => '<span class="text-uc">Share</span>
                                         <small class="text-mute font-base m-l15">
                                         Sell faster by sharing with friends!</small>'
                        )
                    )
            )
        )*/
            ->addDecorator(
                   'ViewScript',
                    array(
                        'viewScript' => 'form_decorator/social-share.phtml',
                        'viewModule' => 'product',
                        'placement' => false
                    )
                );
    }
} 