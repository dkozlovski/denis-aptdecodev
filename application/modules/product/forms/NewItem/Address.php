<?php
/**
 * Author: Alex P.
 * Date: 10.07.14
 * Time: 11:52
 */

class Product_Form_NewItem_Address extends Ikantam_Form
{
    /**
     * @var \User_Model_Address_Collection
     */
    protected $addresses;

    public function init()
    {
        $this->addElement($this->getFullNameElement());
        $this->addElement($this->getAddressLine1Element());
        $this->addElement($this->getAddressLine2Element());

        $this->includeCityStateBlock();
        $this->includeCountryZipBlock();

        $this->addElement($this->getPhoneElement());
        $this->addElement($this->getAbsorbingAmountElement());
        $this->addElement($this->getIsPickupAvailableElement());

        // Used by JS to highlight current address (populated form)
        $hiddenAddressId = new Zend_Form_Element_Hidden('address_id');
        $hiddenAddressId->setAttrib('id', 'address_id');
        $this->addElement($hiddenAddressId);

        $this->initFormDecorators();
    }

    /**
     * Set current addresses
     * @param User_Model_Address_Collection $addresses
     * @return $this
     */
    public function setAddresses($addresses)
    {
        $this->addresses = $addresses;
        return $this;
    }

    /**
     * @return User_Model_Address_Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    public function isValid($data)
    {

        $this->beforeIsValid($data);

        $result = parent::isValid($data);

        $this->afterIsValid($data);

        return $result;
    }

    public function getFieldsContainerDecorator()
    {
        return $this->getElement('full_name')->getDecorator('fields_container');
    }

    protected function getIsPickupAvailableElement()
    {
        $checkbox = new Zend_Form_Element_Checkbox('is_pick_up_available');

        $checkbox->addValidator('InArray', false, array(array(0, 1)));
        $checkbox->setDecorators(array(
                'ViewHelper',
                'cbwrap' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'check-box')),
                'icon' => $this->getHtmlTagDecorator(
                        array('tag' => 'i', 'class' => 'apt-icon-sm-car', 'placement' => 'append')
                    ),
                'description' => $this->getHtmlTagDecorator(array(
                            'tag' => 'span',
                            'class' => 'text-std',
                            'content' => 'Allow buyers to pick up items',
                            'placement' => 'append',
                        )),
                'form_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'form-group')),
                'field_helper' => $this->getHtmlTagDecorator(array(
                            'tag' => 'small',
                            'class' => 'field-helper',
                            'placement' => 'append',
                            'content' => 'If you leave unchecked, your item can only be picked up by our professional' .
                                ' moving partners. Fee will be deducted from your proceeds from the sale.
                                 <a target="_blank" href="'. Ikantam_Url::getUrl('why-allow-pickup.html/index')
                                .'" class="text-primary dark">Find out more</a>'
                        )),
                'control_group' => $this->getHtmlTagDecorator(
                        array('tag' => 'div', 'class' => 'control-group offset-label')
                    ),
                'clear' => $this->getHtmlTagDecorator(
                        array('tag' => 'div', 'class' => 'clear', 'placement' => 'append')
                    )
            ));

        return $checkbox;
    }


    protected function getAbsorbingAmountElement()
    {
        $baseAmount = (float) Shipping_Model_Rate::getMaximumValueSellerAbsorb();
        $percents   = array(100, 50);

        foreach ($percents as $percent) {
            $values[$percent] = 'Cover ' .  $percent . '% shipping in NYC ($' .
                number_format($baseAmount * $percent / 100, 2) . ')';
        }

        $select = new Zend_Form_Element_Select('amount');

        $select
            ->setRequired(false)
            ->addValidator('InArray', false, array($percents))
            ->setAttrib('class', 'form-field offer-field')
            ->addMultiOptions($values)
            ->setValue(100);

        $select->setDecorators(array(
                'ViewHelper',
                'wrap' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'm-t5 hidden set-vis',
                            'id' => 'absorb_shipping_container',
                        )),
                'control_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'control-group offset-label p-b15 hidden-info',
                            'content' => '<div class="form-group">
                                          <div class="check-box get"><input id="absorb_shipping" type="checkbox" name="'.
                                $this->createFullyQualifiedName('absorb_shipping').'" value="1"/></div>
                                          <span class="text-std">Significantly increase sale by covering ' .
                                '50-100% of the delivery fee</span></div>
                                 <small class="field-helper set-vis">
                                 This incentivizes buyers to select your item above other listings, ' .
                                'especially if you\'re in a time crunch or want to increase views.Fee ' .
                                'will be deducted from your proceeds from the sale.<a target="_blank" href="'.
                                Ikantam_Url::getUrl('') .'cover-delivery" ' .
                                'class="text-primary dark">Find out more</a></small>'
                        ))

            ));

        $absorbShipping = new Zend_Form_Element_Hidden('absorb_shipping');
        $absorbShipping->setDecorators(array());
        $absorbShipping->addValidator('InArray', false, array(array(0,1)));

        $this->addElement($absorbShipping);

        return $select;
    }

    protected function includeCityStateBlock()
    {
        $city = $this->getCityElement();
        $city->addDecorator(array(
                'control_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'control-group',
                            'openOnly' => true,
                        ))
            ));

        $state = $this->getStateElement();
        $state->addDecorator(array(
                'control_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'closeOnly' => true,
                        ))
            ));

        $this->addElement($city);
        $this->addElement($state);
    }

    protected function includeCountryZipBlock()
    {
        $country = $this->getCountryElement();
        $country->addDecorator(array(
                'control_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'control-group',
                            'openOnly' => true,
                        ))
            ));

        $zip = $this->getPostcodeElement();
        $zip->addDecorator(array(
                'control_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'closeOnly' => true,
                        ))
            ));

        $this->addElement($country);
        $this->addElement($zip);
    }

    protected function getPhoneElement()
    {
        $phone = new Zend_Form_Element_Text('telephone_number');

        $phone
            ->setRequired(true)
            ->addValidator('NotEmpty', true)
            ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
            ->setAttrib('id', 'telephone_number')
            ->setAttrib('class', 'form-field phone numb-format field-i tel left')
            ->setAttrib('placeholder', '(___) ___ ____');

        $phone->setDecorators(array(
                'ViewHelper',
                'fields_container'=>  $this->getHtmlTagDecorator(array('tag' => 'div', 'closeOnly' => true)), // Opened on a first address element
                'label' => $this->getLabelDecoratorForElement($phone, 'Phone*'),
                'form_group' => $this->getHtmlTagDecorator(array('tag' => 'div','class' => 'form-group')),
                'control_group' => $this->getHtmlTagDecorator(array('tag' => 'div','class' => 'control-group p-b15')),
            ));

        return $phone;
    }

    protected function getPostcodeElement()
    {
        $format = new Zend_Validate_PostCode(array('locale' => 'en_US'));

        $zip = new Zend_Form_Element_Text('postal_code');

        $zip
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->addValidator($format, true)
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
            ->setAttrib('id', 'postal_code')
            ->setAttrib('class', 'form-field');

        $zip->setDecorators(array(
                'ViewHelper',
                'label' => $this->getLabelDecoratorForElement(
                        $zip,
                        'ZIP',
                        array('class' => 'form-label sm-label')
                    ),
                'form_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'form-group col-form-group',
                        ))
            ));

        return $zip;
    }

    protected function getCountryElement()
    {
        $country = new Zend_Form_Element_Text('country_code');

        $country
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
            ->addValidator('InArray', false, array(array_keys($this->getCountries())))
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
            ->setValue('US')
            ->setAttrib('id', 'country')
            ->setAttrib('class', 'form-field');

        $country->setDecorators(array(
                'ViewHelper',
                'label' => $this->getLabelDecoratorForElement($country, 'Country*'),
                'form_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'form-group col-form-group',
                        ))
            ));

        return $country;
    }


    protected function getCityElement()
    {
        $city = new Zend_Form_Element_Text('city');

        $city
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
            ->setAttrib('id', 'city')
            ->setAttrib('class', 'form-field');

        $city->setDecorators(array(
                'ViewHelper',
                'label' => $this->getLabelDecoratorForElement(
                        $city,
                        'City*',
                        array('class' => 'form-label sm-label')
                    ),
                'form_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'form-group col-form-group',
                        ))
            ));

        return $city;
    }

    protected function getStateElement()
    {
        $pickupState = new Zend_Form_Element_Text('state');

        $pickupState
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
            ->setAttrib('id', 'state')
            ->setAttrib('class', 'form-field pos-rlv top--12');

        $pickupState->setDecorators(array(
                'ViewHelper',
                'label' => $this->getLabelDecoratorForElement(
                        $pickupState,
                        'State/Province/<br/> Region*',
                        array('class' => 'form-label sm-label break-line')
                    ),
                'form_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'form-group col-form-group set-max',
                        ))
            ));

        return $pickupState;
    }

    protected function getFullNameElement()
    {
        $fullNameElement = new Zend_Form_Element_Text('full_name');

        $fullNameElement
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
            ->setAttrib('id', 'full_name')
            ->setAttrib('class', 'form-field block-field field-i key right');

        $fullNameElement->setDecorators(array(
                'ViewHelper',
                'label' => $this->getLabelDecoratorForElement($fullNameElement, 'Full Name*'),
                'form_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'form-group')),
                'control_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'control-group')),
                'fields_container' =>  $this->getHtmlTagDecorator(
                        array('tag' => 'div', 'id' => 'address_fields', 'openOnly' => true) // Should be closed on a last address element
                    ),

            ));

        return $fullNameElement;
    }

    protected function getAddressLine1Element()
    {
        $addressLine1Element = new Zend_Form_Element_Text('address_line1');

        $addressLine1Element
            ->setRequired(true)
            ->addValidator('NotEmpty')
            ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
            ->setAttrib('id', 'address_line1')
            ->setAttrib('class', 'form-field');

        $addressLine1Element->setDecorators(array(
                'ViewHelper',
                'label' => $this->getLabelDecoratorForElement($addressLine1Element, 'Address Line 1*'),
                'form_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'form-group')),
                'field_helper' => $this->getHtmlTagDecorator(array(
                            'tag' => 'small',
                            'class' => 'field-helper',
                            'placement' => 'append',
                            'content' => 'Street address, P.O. box, company name, c/o',
                        )),
                'control_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'control-group')),
            ));

        return $addressLine1Element;
    }

    protected function getAddressLine2Element()
    {
        $addressLine2Element = new Zend_Form_Element_Text('address_line2');

        $addressLine2Element
            ->setRequired(false)
            ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim', 'Null'))
            ->setAttrib('id', 'address_line2')
            ->setAttrib('class', 'form-field');

        $addressLine2Element->setDecorators(array(
                'ViewHelper',
                'label' => $this->getLabelDecoratorForElement($addressLine2Element, 'Address Line 2'),
                'form_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'form-group')),
                'field_helper' => $this->getHtmlTagDecorator(array(
                            'tag' => 'small',
                            'class' => 'field-helper',
                            'placement' => 'append',
                            'content' => 'Apartment, suite, unit, building, floor, etc.',
                        )),
                'control_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'control-group')),
            ));

        return $addressLine2Element;
    }

    /**
     * Retrieves available countries
     * @return array
     */
    protected function getCountries()
    {
        $countries = new Application_Model_Country_Collection();
        return $countries->getOptions();
    }

    /**
     * Attaches tag decorator as label for element
     * @param Zend_Form_Element $element
     * @param string $value - label value
     * @param array $options (optional)
     * @return mixed
     */
    protected function getLabelDecoratorForElement(\Zend_Form_Element $element = null, $value = null, $options = array())
    {
        if (!is_string($value)) {
            $value = $element->getFullyQualifiedName();
        }

        $decorator = $this->getHtmlTagDecorator(
            array_merge(
                array(
                    'tag' => 'label',
                    'class' => 'form-label',
                    'content' => '<strong>' . $value . '</strong>',
                    'placement' => 'PREPEND',
                ),
                $options
            )
        );

        return $element ? array('label_' . $element->getFullyQualifiedName() => $decorator) : $decorator;
    }

    /**
     * Prepares form options and decorators
     */
    protected function initFormDecorators()
    {
        $this->addDecorator(
            array(
                'headerDec' => $this->getHtmlTagDecorator(
                        array(
                            'tag' => 'h3',
                            'placement' => 'prepend',
                            'class' => 'section-heading form-section',
                            'content' => '<span class="text-uc">Info</span>' .
                                '<small class="text-mute font-base m-l15">For ' .
                                'your protection, only your first name &amp; parts ' .
                                'of your address (city, state, &amp; zip code) will ' .
                                'be visible on the catalog.</small>'
                        )
                    )
            )
        );

        $this->addDecorator(
            array(
                'addressesContainer' => $this->getHtmlTagDecorator(
                        array(
                            'tag' => 'div',
                            'id' => 'addresses_container',
                            'placement' => 'APPEND',
                        )
                    )
            )
        );

        $isAdmin = Zend_Controller_Front::getInstance()->getRequest()->getModuleName() === 'admin';

        if (!User_Model_Session::instance()->isLoggedIn() && !$isAdmin) {
            $this->addDecorator(array(
                    'login' => $this->getHtmlTagDecorator(array(
                                'tag' => 'div',
                                'id' => 'form_login_container',
                                'class' => 'callout-box mention-block m-b30 set-max',
                                'placement' => 'append',
                                'content' => '<p class="m-0">
                                                Do you have an Account?
                                                <a id="form_login_button"  class="apt-btn primary m-l10">Sign in</a>
                                                </p>'
                            ))
                ));
        } else {

            $this->getDecorator('addressesContainer')
                ->setOption('content', $this->getAddresses()->toHtml());

            // make form fields invisible if at least one address exists
            if ($this->getAddresses()->getSize()) {
                // 'full_name' is the first element in address form. It contains decorator which used to group fields.
                // Opened on it and closed on the last element.
                $this->getFieldsContainerDecorator()->setOption('class', 'hidden');
            }
        }

        $this->addDecorator('FormElements');
    }

    /**
     * Uses to define special conditions
     *
     * @param array $data
     */
    protected function beforeIsValid(array $data)
    {

    }

    /**
     * This method is executed after isValid execution
     *
     * @param array $data
     */
    protected function afterIsValid(array $data)
    {
        // Fields will be shown if form contains error or new address entered
        if ($this->_errorsExist || !$this->getElement('address_id')->getValue()) {
            $this->getFieldsContainerDecorator()->removeOption('class');
        }
    }
}
