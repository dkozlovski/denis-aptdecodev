<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 09.07.14
 * Time: 14:38
 */

class Product_Form_NewItem_ListingInfo extends Ikantam_Form
{
    public function init()
    {
        $this->initFormDecorators();
        $this->initListingInfo();
    }

    protected function initListingInfo()
    {
        $this->addElement($this->getSellingTypeElement());
        $this->addElement($this->getQtyElement());
        $this->includePricingBlock();
        $this->includeSellingWindowBlock();
        $this->addElement($this->getManageMyPricingElement());
        $this->includeExposedBlock();
    }

    protected function includeExposedBlock()
    {
        $smoke = $this->getIsSmokeElement();
        $smoke->addDecorators(array(
                'ul_container' => $this->getHtmlTagDecorator(array(
                            'tag' => 'ul',
                            'class' => 'v-inline-list',
                            'openOnly' => true, //should be closed in dog element :)
                        )),
                'form_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'form-group',
                            'openOnly' => true,
                            'content' => '<span class="text-std">My item has been exposed to:</span>'
                        )),
                'control_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'control-group offset-label',
                            'openOnly' => true,
                        ))
            ));

        $dogs = $this->getDogInHomeElement();
        $dogs->addDecorators(array(
                'ul_container' => $this->getHtmlTagDecorator(array(
                            'tag' => 'ul',
                            'closeOnly' => true,
                        )),
                'form_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'closeOnly' => true,
                        )),
                'control_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'closeOnly' => true,
                        ))
            ));

        $this->addElement($smoke);
        $this->addElement($this->getCatInHomeElement());
        $this->addElement($dogs);

    }

    protected function getDogInHomeElement()
    {
        $checkbox = new Zend_Form_Element_Checkbox('dog_in_home');

        $checkbox->addValidator('InArray', true, array(array(0, 1)));

        $checkbox->setDecorators(array(
                'ViewHelper',
                'cbwrap' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'check-box')),
                'label' => $this->getHtmlTagDecorator(
                        array(
                            'tag' => 'span',
                            'class' => 'text-std',
                            'placement' => 'append',
                            'content' => 'Dogs',
                        )
                    ),
                'li_wrapper' => $this->getHtmlTagDecorator(array('tag' => 'li'))
            ));

        return $checkbox;
    }

    protected function getCatInHomeElement()
    {
        $checkbox = new Zend_Form_Element_Checkbox('cat_in_home');

        $checkbox->addValidator('InArray', true, array(array(0, 1)));

        $checkbox->setDecorators(array(
                'ViewHelper',
                'cbwrap' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'check-box')),
                'label' => $this->getHtmlTagDecorator(
                        array(
                            'tag' => 'span',
                            'class' => 'text-std',
                            'placement' => 'append',
                            'content' => 'Cats',
                        )
                    ),
                'li_wrapper' => $this->getHtmlTagDecorator(array('tag' => 'li'))
            ));

        return $checkbox;
    }

    protected function getIsSmokeElement()
    {
        $checkbox = new Zend_Form_Element_Checkbox('is_smoke');

        $checkbox->addValidator('InArray', true, array(array(0, 1)));

        $checkbox->setDecorators(array(
                'ViewHelper',
                'cbwrap' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'check-box')),
                'label' => $this->getHtmlTagDecorator(
                        array(
                            'tag' => 'span',
                            'class' => 'text-std',
                            'placement' => 'append',
                            'content' => 'Smoking',
                        )
                    ),
                'li_wrapper' => $this->getHtmlTagDecorator(array('tag' => 'li'))
            ));

        return $checkbox;
    }

    protected function getManageMyPricingElement()
    {
        $checkbox = new Zend_Form_Element_Checkbox('is_manage_price_allowed');

        $checkbox->addValidator('InArray', true, array(array(0, 1)));

        $checkbox->setDecorators(array(
                'ViewHelper',
                'cbwrap' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'check-box')),
                'label' => $this->getLabelDecoratorForElement($checkbox, 'Additional'),
                'description' => $this->getHtmlTagDecorator(
                        array('tag' => 'span', 'class' => 'text-std', 'placement' => 'append'),
                        'Please manage my pricing for me.'
                    ),
                'form_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'form-group')),
                'field_helper' => $this->getHtmlTagDecorator(
                        array(
                            'tag' => 'small',
                            'class' => 'field-helper',
                            'placement' => 'append',
                        ),
                    'Our pricing management feature allows AptDeco to reduce the price of your item. ' .
                    'Prices are optimized based on sales in the same category and will never go below ' .
                    '50% of the initial price.'
                    ),
                'control_group' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'control-group')),

            ));

        return $checkbox;
    }

    protected function includeSellingWindowBlock()
    {
        $availableFrom = $this->getAvailableFromElement();
        $availableTill = $this->getAvailableTillElement();

        $availableFrom->addDecorators(array(
                array(
                    array('control_group' => 'HtmlTag'),
                    array('tag' => 'div', 'class' => 'control-group', 'openOnly' => true)
                ),
            ));

        $availableTill->addDecorators(array(
                'description' => $this->getHtmlTagDecorator(array(
                            'tag' => 'small',
                            'class' => 'field-helper',
                            'placement' => 'append',
                            'content' => 'Specify the dates when this product will be available for sale.' .
                                ' Leave blank if indefinite or if you\'re not sure..
                                        <div class="clear"></div>
                                        <p id="delivery_days" class="text-std m-t10 hidden"></p>'
                        )),
                array(
                    array('control_group' => 'HtmlTag'),
                    array('tag' => 'div', 'closeOnly' => true)
                ),
            ));

        $this->addElement($availableFrom);
        $this->addElement($availableTill);
    }

    protected function getAvailableTillElement()
    {
        $availableTill = new Zend_Form_Element_Text('available_till');

        $dateFormat = new Zend_Validate_Date(array('format' => 'M dd, y'));
        $dateFormat->setLocale('en_US');

       // $format = new Zend_Filter_LocalizedToNormalized(array('date_format' => 'M dd, y', 'locale' => 'en_US'));

        $availableTill->setRequired(false)
            ->addFilters(array('StripTags', 'StringTrim', 'Null'))
            //->addFilter($format)
            ->addValidator($dateFormat, true)
            ->setAttrib('id', 'datepicker2')
            ->setAttrib('class', 'form-field')
            ->setAttrib('readonly', 'readonly');

        $availableTill->setDecorators(array(
                'ViewHelper',
                array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'pull-left dp-holder m-t7')),
                'form_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'form-group col-form-group',
                            'content' => '<span class="text-std show">Selling End date</span>'
                        ))
            ));

        return $availableTill;
    }

    protected function getAvailableFromElement()
    {
        $availableFrom = new Zend_Form_Element_Text('available_from');

        $dateFormat = new Zend_Validate_Date(array('format' => 'M dd, y'));
        $dateFormat->setLocale('en_US');

        //$format = new Zend_Filter_LocalizedToNormalized(array('date_format' => 'M dd, y', 'locale' => 'en_US'));

        $availableFrom->setRequired(false)
            ->addFilters(array('StripTags', 'StringTrim', 'Null'))
          //  ->addFilter($format)
            ->addValidator($dateFormat, true)
            ->setAttrib('id', 'datepicker')
            ->setAttrib('class', 'form-field')
            ->setAttrib('readonly', 'readonly');

        $availableFrom->setDecorators(array(
                'ViewHelper',
                array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'pull-left dp-holder m-t7')),
                'form_group' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'form-group col-form-group',
                            'content' => '<label class="form-label pull-left m-t17">
                                          <strong>Item <br> Availability*</strong>
                                          </label>
                                          <span class="text-std">Selling Start Date</span>'
                        ))
            ));

        return $availableFrom;
    }

    protected function includePricingBlock()
    {
        $this->addElement($this->getPriceElement());
        $this->addElement($this->getOriginalPriceElement());
    }

    protected function getOriginalPriceElement()
    {
        $floatFormat = new Zend_Filter_LocalizedToNormalized(array('locale' => 'en_US', 'precision' => 2));

        $originalPrice = new Zend_Form_Element_Text('original_price');

        $originalPrice->setRequired(false)
            ->addValidator('Float', false, array(array('locale' => 'en_US')))
            ->addValidator('GreaterThan', false, array(array('min' => 0)))
            ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines'))
            ->addFilter($floatFormat)
            ->addFilter('Null')
            ->setAttrib('class', 'form-field m-0');

        $originalPrice->setDecorators(array(
                'ViewHelper',
                'wrap' => $this->getHtmlTagDecorator(array(
                            'class' => 'input-prepend pull-left m-r10',
                            'tag' => 'div',
                            'content' => '<cite class="add-on">$</cite>'
                        )),
                'label' => $this->getHtmlTagDecorator(array(
                            'tag' => 'span',
                            'class' => 'form-label-inner',
                            'placement' => 'prepend',
                            'content' => 'Original price'
                        )),
                'description' => $this->getHtmlTagDecorator(array(
                            'tag' => 'span',
                            'class' => 'text-mute p-t12 show selling-type-helper',
                            'placement' => 'append',
                            'content' => 'per item',
                        )),
                'pricing_block' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'pricing')),
                'form_field' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'closeOnly' => true, //was opened in price element
                        )),

                array(
                    array('form_group' => 'HtmlTag'),
                    //was opened in price element
                    array('tag' => 'div', 'closeOnly' => true)
                ),
                array(
                    array('control_group' => 'HtmlTag'),
                    //was opened in price element
                    array('tag' => 'div', 'closeOnly' => true)),
            ));

        return $originalPrice;
    }

    protected function getPriceElement()
    {
        $floatFormat = new Zend_Filter_LocalizedToNormalized(array('locale' => 'en_US', 'precision' => 2));

        $price = new Zend_Form_Element_Text('price');

        $price->setRequired(true)
            ->addValidator('NotEmpty')
            ->addValidator('Float', false, array(array('locale' => 'en_US')))
            ->addValidator('Between', false, array(array('min' => 50, 'max' => 999999.99)))
            ->setAttrib('autocomplete', 'off')
            ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines'))
            ->addFilter($floatFormat)
            ->addFilter('Null')
            ->setAttrib('class', 'form-field m-0');

        $price->setDecorators(array(
                'ViewHelper',
                'wrap' => $this->getHtmlTagDecorator(array(
                            'class' => 'input-prepend pull-left m-r10',
                            'tag' => 'div',
                            'content' => '<cite class="add-on">$</cite>'
                        )),
                'label' => $this->getHtmlTagDecorator(array(
                            'tag' => 'span',
                            'class' => 'form-label-inner',
                            'placement' => 'prepend',
                            'content' => 'Listing price*'
                        )),
                array('Callback', array(
                    'placement' => 'append',
                    'callback' => function($content){
                        return '<span class="text-mute p-t12 show selling-type-helper">per item</span>
                                <div class="clear"></div>
                                <small>
                                <a target="_blank" href="'.
                                Ikantam_Url::getUrl('') . 'faq?q=fees'.
                                '" class="text-primary dark"> What are the fees for selling on AptDeco?</a>
                                </small>';
                    })),
                'pricing_block' => $this->getHtmlTagDecorator(array('tag' => 'div', 'class' => 'pricing')),
                array(array('revenue' => 'Callback'), array(
                    'placement' => 'append',
                    'callback' => function($content){
                            return '<div class="pricing pricing-revenue">
                                    <span class="form-label-inner">Your revenue</span>
                                        <p class="p-t11">
                                            <strong id="display_revenue" class="text-std">0.00</strong>
                                            <span class="text-mute selling-type-helper">per item</span>
                                        </p>
                                    </div>';
                        })),
                'form_field' => $this->getHtmlTagDecorator(array(
                            'tag' => 'div',
                            'class' => 'form-field-block pull-left',
                            'openOnly' => true, //should be closed in originalPrice element
                        )),
                'common_label' => $this->getLabelDecoratorForElement(
                        $price,
                        'Pricing*',
                        array('class' => 'form-label pull-left m-t30')
                    ),
                array(
                    array('form_group' => 'HtmlTag'),
                    //should be closed in originalPrice element
                    array('tag' => 'div', 'class' => 'form-group', 'openOnly' => true)
                ),
                array(
                    array('control_group' => 'HtmlTag'),
                    //should be closed in originalPrice element
                    array('tag' => 'div', 'class' => 'control-group', 'openOnly' => true)),

            ));

        return $price;
    }

    protected function getQtyElement()
    {
        $qtyElement = new Zend_Form_Element_Text('qty');

        $qtyElement->setRequired(true)
            ->addValidator('NotEmpty')
            ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
            ->setAttrib('style', 'background-color:white;')
            ->setAttrib('id', 'qty')
            ->addValidator('Int', false, array(array('locale' => 'en_US')))
            ->addValidator('GreaterThan', false, array(array('min' => 0)))
            ->setValue(1);

        $qtyElement->setDecorators(array(
                'ViewHelper',
                /* array('Callback', array('callback' => function(){
                         return '<button type="button" class="decrease"><i></i></button>
                                 <button type="button" class="increase"><i></i></button>';
                     })
                 ),*/
                array(array('els' => 'HtmlTag'), array('tag' => 'div', 'class' => 'els qty-form pull-left')),

                array('label' => $this->getLabelDecoratorForElement(
                        $qtyElement,
                        'Quantity*',
                        array(
                            'class' => 'form-label pull-left m-t8',
                        )
                    )),
                array(
                    'description' => $this->getHtmlTagDecorator(
                            array(
                                'tag' => 'span',
                                'class' => 'text-mute pull-left show m-t10 m-l10',
                                'placement' => 'append',
                                'content' => 'item(s)'
                            )
                        )
                ),
                array(array('form_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
                array(array('control_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group')),

            ));

        return $qtyElement;
    }

    protected function getSellingTypeElement()
    {
        $sellingType = new Ikantam_Form_Element_MultiRadio('selling_type');
        $sellingType->setSeparator('')
            ->setValue('individual_item');

        $values = array(
            'individual_item' => 'Individual Item(s)',
            'set' => 'Set',
        );

        $sellingType
            ->setRequired(true)
            ->setMultiOptions($values)
            ->addValidator('InArray', true, array(array_keys($values)));

        $sellingType->setDecorators(
            array(
                'ViewHelper',
                array(array('ul_container' => 'HtmlTag'), array('tag' => 'ul', 'class' => 'v-inline-list')),
                array(array('form_field' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-field-block pull-left')),
                array(
                    'label' => $this->getLabelDecoratorForElement(
                            $sellingType,
                            "I'm selling ...*",
                            array(
                                'class' => 'form-label pull-left m-t5'
                            )
                        )
                ),
                array(array('form_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
                array(array('control_group' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group')),
            )
        );

        $sellingType->addMultiDecorator(new Ikantam_Form_Decorator_HtmlTag(
                array(
                    'tag' => 'div',
                    'class' => array('callback' => function($self){
                            $data = $self->getOption('__current_multi_data');
                            $class = 'radio-btn';
                            if ($data['checked']) {
                                $class .= ' checkedRadio';
                            }
                            return  $class;
                        })
                )
            ))
            // label
            ->addMultiDecorator(new Ikantam_Form_Decorator_HtmlTag(
                    array(
                        'tag' => 'span',
                        'class' => 'show m-r30',
                        'placement' => 'append',
                        'content' => function($self){
                                $data = $self->getOption('__current_multi_data');
                                return ($data['value'] === 'set')
                                    ? $data['label'] . '<small class="field-helper">Buyers can only buy this item as a set.</small>'
                                    : $data['label'];
                            }
                    )
                ))
            ->addMultiDecorator(new Ikantam_Form_Decorator_HtmlTag(array('tag' => 'li')));

        return $sellingType;
    }

    /**
     * Attaches tag decorator as label for element
     * @param Zend_Form_Element $element
     * @param string $value - label value
     * @param array $options (optional)
     * @return mixed
     */
    protected function getLabelDecoratorForElement(\Zend_Form_Element $element = null, $value = null, $options = array())
    {
        if (!is_string($value)) {
            $value = $element->getFullyQualifiedName();
        }

        $decorator = $this->getHtmlTagDecorator(
            array_merge(
                array(
                    'tag' => 'label',
                    'class' => 'form-label',
                    'content' => '<strong>' . $value . '</strong>',
                    'placement' => 'PREPEND',
                ),
                $options
            )
        );

        return $element ? array('label_' . $element->getFullyQualifiedName() => $decorator) : $decorator;
    }

    /**
     * Prepares form options and decorators
     */
    protected function initFormDecorators()
    {
        $this->addDecorator(
            array(
                'headerDec' => $this->getHtmlTagDecorator(
                        array(
                            'tag' => 'h3',
                            'placement' => 'prepend',
                            'class' => 'section-heading form-section',
                            'content' => '<span class="text-uc">Listing Info</span>'
                        )
                    )
            )
        )
            ->addDecorator('FormElements');
    }
} 