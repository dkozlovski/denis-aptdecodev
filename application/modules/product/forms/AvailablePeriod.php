<?php

class Product_Form_AvailablePeriod extends Product_Form_OptionalFieldsSubform
{
    public function init()
    {
        $this->setDecorators(array(
            'FormElements',
        ));

        $this->addElement($this->getAvailableFromElement());
        $this->addElement($this->getAvailableTillElement());
    }
}