<?php
class Product_Form_ErrorHelper
{
    protected $_form ;
    
    public function __construct (\Zend_Form $form)
    {
        $this->_form = $form;   
    }
        
    /**
     * Some controll groups have more than 1 input element each of this
     * elements have their own error class. 
     * This property describes which classes belongs to which elements.
     * @type array 
     */
    protected $_errorHtmlClassesMap = array(
        array('price' => 'inp-1st', 'original_price' => 'inp-2nd'),
        array('material_id' => 'inp-1st', 'age' => 'inp-2nd'),
    );
    
    protected $_errorClass = 'error';
    
    protected $_specialErrorClass = 'has-error';
    
    protected $_modifiedDecoratorName = 'group';
    
    protected $_modifiedDecoratorClass = 'control-group';
    
    protected $_rendered =  array ();

    
    /**
     * Return classes string for controll group
     * 
     * @param object $element  
     * @return string
     * @throws Product_Form_Errors_Exception - if form is not set
     */
    public function getGroupClassForElement (\Zend_Form_Element $element)
    { 
        $form = $this->_form;
        $result = $this->_modifiedDecoratorClass;
        
        if($element->hasErrors()) {
            $result .= ' ' . $this->_errorClass;
        }
                
        $name = $element->getName();
        $hasError = false;
        foreach($this->_errorHtmlClassesMap as $group) {
            if(array_key_exists($name, $group)) {
                $result = $this->_modifiedDecoratorClass;
                if($element->hasErrors()) {
                    $hasError = true;
                }

                foreach($group as $name => $class) {
                    if(($siblingElement = $form->getElement($name)) && $siblingElement->hasErrors()) {
                        $hasError = true;
                        $result .= ' ' . $group[$name];
                        $this->_rendered[] = $name;
                    }                    
                }
            }
        }
        if($hasError) {
            $result .= ' ' . $this->_specialErrorClass; 
        }
        
        return $result;
    }    

    
    /**
     * Checks if siblings element exist
     * 
     * @param  Zend_Form_Element $element
     * @return bool
     */
    public function isGoupedElement (\Zend_Form_Element $element)
    {
        $name = $element->getName();
        foreach($this->_errorHtmlClassesMap as $group) {
            if(array_key_exists($name, $group)){
                return true;
            }
        }
        return false;
    }
      
}