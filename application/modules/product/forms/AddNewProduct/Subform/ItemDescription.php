<?php

class Product_Form_AddNewProduct_Subform_ItemDescription extends Zend_Form_SubForm
{

    public function init()
    {
        $this->addElement($this->getCategoryElement());
        $this->addElement($this->getSubCategoryElement());
        $this->addElement($this->getTitleElement());
        $this->addElement($this->getConditionElement());
        $this->addElement($this->getManufacturerElement());

        $this->addElement($this->getWidthElement());
        $this->addElement($this->getDepthElement());
        $this->addElement($this->getHeightElement());

        $this->addElement($this->getMaterialElement());
        $this->addElement($this->getAgeElement());

        $this->addElement($this->getDescriptionElement());

        $this->addElement($this->getIsAntiqueElement());
        $this->addElement($this->getKidsCornerElement());
        $this->addElement($this->getIsUnder15Element());
        $this->addElement($this->getIsDamagedElement());
    }

    protected function getCategoryElement()
    {
        $category   = new Zend_Form_Element_Select('category_id');
        $collection = new Category_Model_Category_Collection();
        $values     = $collection->getOptions3();
        $select     = array('' => 'Select Category');

        $category->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addFilters(array('Null'))
                ->setMultiOptions($select + $values);

        $category->setDecorators(array(
            'ViewHelper',
        ));

        return $category;
    }

    protected function getSubCategoryElement()
    {
        $category   = new Zend_Form_Element_Select('subcategory_id');
        $collection = new Category_Model_Category_Collection();
        $values     = $collection->getOptions4();
        $select     = array('' => 'Select Subcategory');

        $category->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addFilters(array('Null'))
                ->setMultiOptions($select + $values);

        $category->setDecorators(array(
            'ViewHelper',
        ));

        return $category;
    }

    protected function getTitleElement()
    {
        $titleElement = new Zend_Form_Element_Text('title');

        $titleElement->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)))
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'));

        $titleElement->setDecorators(array(
            'ViewHelper',
        ));

        return $titleElement;
    }

    protected function getConditionElement()
    {
        $values = array(
            'new'          => 'New: Product hasn\'t been unwrapped from box',
            //'like-new'     => 'Like New: Product has been unwrapped from box but looks new & doesn\'t have any scratches, blemishes, etc.',
            'good'         => 'Good: Minor blemishes that most people won\'t notice',
            'satisfactory' => 'Satisfactory: Moderate wear and tear, but still has many good years left',
            'age-worn'     => 'Age-worn: Has lived a full life and has a "distressed" look with noticeable wear'
        );

        $select = array('' => 'Select Condition');

        $currentCondition = new Zend_Form_Element_Select('condition');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array(array_keys($values)))
                ->setMultiOptions($select + $values);

        $currentCondition->setDecorators(array(
            'ViewHelper',
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getManufacturerElement()
    {
        $manufacturer = new Zend_Form_Element_Text('manufacturer');

        $manufacturer->setRequired(true)
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('max' => 255)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'));

        $manufacturer->setDecorators(array(
            'ViewHelper',
        ));

        return $manufacturer;
    }

    protected function getWidthElement()
    {
        $width = new Zend_Form_Element_Text('width');

        $width->setRequired(false)
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'));

        $width->setDecorators(array(
            'ViewHelper',
        ));

        return $width;
    }

    protected function getDepthElement()
    {
        $depth = new Zend_Form_Element_Text('depth');

        $depth->setRequired(false)
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'));

        $depth->setDecorators(array(
            'ViewHelper',
        ));

        return $depth;
    }

    protected function getHeightElement()
    {
        $height = new Zend_Form_Element_Text('height');

        $height->setRequired(false)
                ->addValidator('Float', false, array(array('locale' => 'en_US')))
                ->addValidator('GreaterThan', false, array(array('min' => 0)))
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'));

        $height->setDecorators(array(
            'ViewHelper',
        ));

        return $height;
    }

    protected function getMaterialElement()
    {
        $material   = new Zend_Form_Element_Select('material_id');
        $collection = new Application_Model_Material_Collection();
        $options    = $collection->getOptions();
        $select     = array('' => 'Select Material');

        $material->setRequired(true)
                ->addValidator('InArray', true, array(array_keys($options)))
                ->addFilter('Null')
                ->setMultiOptions($select + $options);

        $material->setDecorators(array(
            'ViewHelper',
        ));

        return $material;
    }

    protected function getAgeElement()
    {
        $age = new Zend_Form_Element_Select('age');

        $options = array(1 => '<1', 2 => '1 - 2', 3 => '2 - 3', 4 => '3 - 4', 5 => '4 - 5', 6 => '5+');
        $select  = array('' => 'Select Age');

        $age->setRequired(true)
                ->addValidator('InArray', true, array(array_keys($options)))
                ->addFilter('Null')
                ->addMultiOptions($select + $options);

        $age->setDecorators(array(
            'ViewHelper',
        ));

        return $age;
    }

    protected function getDescriptionElement()
    {
        $description = new Zend_Form_Element_Textarea('description');

        $description->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim', 'Null'))
                ->addValidator('StringLength', false, array(array('max' => 60000)));

        $description->setDecorators(array(
            'ViewHelper',
        ));

        return $description;
    }

    protected function getIsAntiqueElement()
    {
        $options = array(0 => 'No', 1 => 'Yes');

        $isAntiquesElement = new Zend_Form_Element_Checkbox('is_antique');

        $isAntiquesElement->setRequired(false)
                ->setAttrib('disableHidden', true)
                ->addValidator('InArray', true, array(array_keys($options)));

        $isAntiquesElement->setDecorators(array(
            'ViewHelper',
        ));

        return $isAntiquesElement;
    }

    protected function getKidsCornerElement()
    {
        $options = array(0 => 'No', 1 => 'Yes');

        $isKidsCornerElement = new Zend_Form_Element_Checkbox('is_kids_corner');

        $isKidsCornerElement->setRequired(false)
                ->setAttrib('disableHidden', true)
                ->addValidator('InArray', true, array(array_keys($options)));

        $isKidsCornerElement->setDecorators(array(
            'ViewHelper',
        ));

        return $isKidsCornerElement;
    }

    protected function getIsUnder15Element()
    {
        $options = array(0 => 'No', 1 => 'Yes');

        $isAntiquesElement = new Zend_Form_Element_Checkbox('is_under_15_pounds');

        $isAntiquesElement->setRequired(false)
                ->setAttrib('disableHidden', true)
                ->addValidator('InArray', true, array(array_keys($options)));

        $isAntiquesElement->setDecorators(array(
            'ViewHelper',
        ));

        return $isAntiquesElement;
    }
    
    protected function getIsDamagedElement()
    {
        $options = array(0 => 'No', 1 => 'Yes');

        $isAntiquesElement = new Zend_Form_Element_Checkbox('is_damaged');

        $isAntiquesElement->setRequired(false)
                ->setAttrib('disableHidden', true)
                ->addValidator('InArray', true, array(array_keys($options)));

        $isAntiquesElement->setDecorators(array(
            'ViewHelper',
        ));

        return $isAntiquesElement;
    }

}