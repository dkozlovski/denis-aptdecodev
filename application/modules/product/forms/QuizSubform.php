<?php

class Product_Form_QuizSubform extends Zend_Form_SubForm
{

    public function init()
    {
        $this->setDecorators(array(
            'FormElements',
        ));

        $this->addElement($this->getReasonElement());
        $this->addElement($this->getToBeVerifiedElement());
        $this->addElement($this->getHowYouHeardElement());
        $this->addElement($this->getOtherElement());
        //$this->addElement($this->getIsConciergeAvailableElement());
        $this->addElement($this->getModalButtonsElement());

        //$this->addDisplayGroup(
        //        array('reason', 'is_verification_available', 'is_concierge_available', 'modal_buttons'), 'zzreason', array('disableLoadDefaultDecorators' => true));

        $this->addDisplayGroup(
                array('reason', 'is_verification_available', 'how_you_heard_select', 'how_you_heard_text', 'modal_buttons'), 'zzreason', array('disableLoadDefaultDecorators' => true));

        $this->_prepareReason($this->getDisplayGroup('zzreason'));
    }

    protected function getReasonElement()
    {
        $multiOptions = array('moving'       => 'Moving', 'redecorating' => 'Redecorating');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('reason');

        $currentCondition->setBelongsTo('quiz')
                ->setRequired(false)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($multiOptions)
                ->setLabel('Why are you selling your stuff?')
                ->setAttrib('value_class', 'm-l7');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li', 'class' => 'm-r15')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'inline')),
            array('Label', array('placement' => 'prepend', 'class'     => 'h4')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getToBeVerifiedElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('is_verification_available');

        $currentCondition->setBelongsTo('quiz')
                ->setRequired(false)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($multiOptions)
                ->setAttrib('value_class', 'm-l7')
                ->setLabel('Would you like to be AptDeco Verified?
                    <a href="' . Ikantam_Url::getUrl('aptdeco-verified/index') . '" target="_blank" class="get text-sm text-prime">What\'s this?</a>')
                ->setDescription('Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li', 'class' => 'm-r15')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'inline')),
            array('Description', array('placement' => 'prepend', 'tag'       => 'p', 'class'     => 'set-vis hidden')),
            array('Label', array('placement' => 'prepend', 'class'     => 'h4', 'escape'    => false)),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'hidden-info')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getIsConciergeAvailableElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');
        $values       = array_keys($multiOptions);

        $currentCondition = new Ikantam_Form_Element_Radio('is_concierge_available');

        $currentCondition->setBelongsTo('quiz')
                ->setRequired(false)
                ->addValidator('InArray', true, array($values))
                ->setMultiOptions($multiOptions)
                ->setLabel('Would you like us to consider you for our concierge program?
                    <a href="#" class="get text-sm text-prime">What\'s this?</a>')
                ->setDescription('Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'inline')),
            array('Description', array('placement' => 'prepend', 'tag'       => 'p', 'class'     => 'set-vis hidden')),
            array('Label', array('placement' => 'prepend', 'class'     => 'h4', 'escape'    => false)),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'hidden-info')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getHowYouHeardElement()
    {
        $multiOptions =  array('' => 'Select') + Checkout_Form_HowYouHeard::getHeards();
        $element = new Zend_Form_Element_Select('how_you_heard_select');

        $element->setBelongsTo('quiz')
            ->setRequired(false)
            ->addValidator('InArray', true, array(array_keys($multiOptions)))
            ->setMultiOptions($multiOptions)
            ->setLabel('How did you hear about us?')
            ->setAttrib('onchange', 'toggleHeard(this)')
            ->setAttrib('class', 'pull-left');

        $element->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'p', 'class'     => 'set-vis hidden')),
            array('Label', array('placement' => 'prepend', 'class'     => 'h4', 'escape'    => false)),
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'eq-fields  select-el clearfix', 'openOnly' => true)),
        ));

        return $element;
    }

    protected function getOtherElement()
    {
        $otherElement = new Zend_Form_Element_Text('how_you_heard_text');
        $otherElement
            ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
            ->setRequired(false)
            ->setAttrib('class', 'form-field select-field hidden pull-left m-l15')
            ->setAttrib('data-option', '7');

        $otherElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'eq-fields m-t30 select-el', 'closeOnly' => true)),
        ));
        return $otherElement;
    }

    protected function getModalButtonsElement()
    {
        $note = new Ikantam_Form_Element_Note('modal_buttons');

        $note->setRequired(false)
                ->setDescription('<a class="apt-btn prime lrg" data-action="send" href="#">Send</a> 
                <a class="text-sm text-mute" href="#"  data-action="skip" data-dismiss="modal" aria-hidden="true">Skip</a>');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array(
                    'placement' => 'prepend',
                    'tag'       => 'div',
                    'class'     => 'apt-modal-action text-right',
                    'escape'    => false)),
        ));

        return $note;
    }

    protected function _prepareReason($dg)
    {
        $dg->addDecorator('FormElements');
        //$dg->addDecorator(array('wrap0' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'fit-els fit'));
        //$dg->addDecorator(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'block-fields'));
        $dg->addDecorator(array('label' => 'DtDdWrapper'));

        $dg->addDecorator(array('div' => 'HtmlTag'), array(
            'tag'             => 'div',
            'class'           => 'modal apt-modal hide fade',
            'tabindex'        => '-1',
            'id'              => 'question-modal',
            'role'            => 'dialog',
            'aria-labelledby' => 'myModalLabel',
            'aria-hidden'     => 'true'
        ));

        $ld = $dg->getDecorator('label');

        $ld->setOptions(array('dtLabel' => '<button data-action="skip" type="button" class="apt-btn transparent x-btn" data-dismiss="modal" aria-hidden="true">
                <i class="apt-i-x"></i>
            </button>

            <h2>Congrats! You\'re about to publish your first item on AptDeco. One last thing..</h2>'));
    }

}
