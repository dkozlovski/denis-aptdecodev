<?php

class Product_Form_ProductAlert extends Ikantam_Form
{
    public function init()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $this->setIsArray(true);
        /**/
        $collection = new Category_Model_Category_Collection();
        $values     =  $collection->getOptions3();

        $categoryId = new Zend_Form_Element_Select('category_id');
        $categoryId->setMultiOptions($values)
            ->addFilters(array('StringTrim', 'StripTags'));

        /**/
        $collection = new Application_Model_Set_Collection;
        $filter = $collection->getFlexFilter();
        $filter->menu_name('=', 'main_secondary');
        $items =  $filter->apply()->getItems();
        $values = array();
        foreach ($items as $_item) {
            $values[$_item->getId()] = $_item->getName();
        }

        $collectionId =  new Zend_Form_Element_Select('collection_id');
        $collectionId->setMultiOptions($values)
            ->addFilters(array('StringTrim', 'StripTags'));


        /**/
        $collection = new Category_Model_Category_Collection();

        $subcategoryIds = new Zend_Form_Element_Multiselect('subcategory_ids');
        $subcategoryIds->setMultiOptions($collection->getOptions4())
            ->addFilters(array('StringTrim', 'StripTags'));

        /**/
        $conditions = new Zend_Form_Element_Multiselect('conditions');
        $conditions->setMultiOptions(Product_Model_Alert::getShortConditionsLabel())
            ->addFilters(array('StringTrim', 'StripTags'));

        /**/
        $collection = new Application_Model_Color_Collection();

        $colorIds = new Zend_Form_Element_Multiselect('color_ids');
        $colorIds->setMultiOptions($collection->getAsOptions())
            ->addFilters(array('StringTrim', 'StripTags'));

        /**/
        $collection = new Application_Model_Manufacturer_Collection();

        $brandIds = new Zend_Form_Element_Multiselect('brand_ids');
        $brandIds->setMultiOptions($collection->getOptions())
            ->addFilters(array('StringTrim', 'StripTags'));

        $betweenErrorMessage = 'Please enter a valid price';
        $minPrice = new Zend_Form_Element_Text('min_price');
        $minPrice->addFilters(array('StringTrim', 'StripTags'))
            ->addValidator('between', true, array('min' => 0, 'max' => 1000000,  'inclusive' => true, 'messages' => $betweenErrorMessage));

        $betweenOptions = array('min' => 0, 'max' => 1000000, 'inclusive' => true, 'messages' => $betweenErrorMessage);
        if ($request->getParam('min_price') > 0) {
             $betweenOptions['min'] = (int)$request->getParam('min_price');
        }
        $maxPrice = new Zend_Form_Element_Text('max_price');
        $maxPrice ->addFilters(array('StringTrim', 'StripTags'))
            ->addValidator('between', true, $betweenOptions);

        $isAjax = new Zend_Form_Element_Hidden('is_ajax');

        $emailsSubForm = new Zend_Form();
        for ($i = 0; $i < 10; $i++) {
            $email =  new Zend_Form_Element_Text($i.'');
            if ($i == 0) {
                $email->setRequired(true);
                $email->addValidator('NotEmpty', true, array('messages' => 'Please enter a email'));
            }
            $email->addValidator('EmailAddress')
                ->addFilters(array('StringTrim', 'StripTags'));

            $emailsSubForm->addElement($email);
        }
        $this->addElements(array($categoryId, $collectionId, $subcategoryIds, $conditions, $colorIds, $brandIds, $minPrice, $maxPrice, $isAjax));
        $this->addSubForm($emailsSubForm, 'emails');
    }

}