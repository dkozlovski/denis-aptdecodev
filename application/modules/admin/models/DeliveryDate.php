<?php

/**
 * Class Admin_Model_DeliveryDate
 * @method int getDate()
 * @method string getDateString()
 * @method int getIsFirstPeriodAvailable()
 * @method int getIsFirstPeriodForSmallAvailable()
 * @method int getIsSecondPeriodAvailable()
 * @method int getIsSecondPeriodForSmallAvailable()
 * @method int getIsThirdPeriodAvailable()
 * @method int getIsThirdPeriodForSmallAvailable()
 * @method int getIsFourthPeriodAvailable()
 * @method int getIsFourthPeriodForSmallAvailable()
 *
 * @method Admin_Model_DeliveryDate setDate(int $value)
 * @method Admin_Model_DeliveryDate setDateString(string $value)
 * @method Admin_Model_DeliveryDate setIsFirstPeriodAvailable(int $value)
 * @method Admin_Model_DeliveryDate setIsFirstPeriodForSmallAvailable(int $value)
 * @method Admin_Model_DeliveryDate setIsSecondPeriodAvailable(int $value)
 * @method Admin_Model_DeliveryDate setIsSecondPeriodForSmallAvailable(int $value)
 * @method Admin_Model_DeliveryDate setIsThirdPeriodAvailable(int $value)
 * @method Admin_Model_DeliveryDate setIsThirdPeriodForSmallAvailable(int $value)
 * @method Admin_Model_DeliveryDate setIsFourthPeriodAvailable(int $value)
 * @method Admin_Model_DeliveryDate setIsFourthPeriodForSmallAvailable(int $value)
 *
 */
class Admin_Model_DeliveryDate extends Application_Model_Abstract {

    protected $_backendClass = 'Admin_Model_DeliveryDate_Backend';
    private static $_instance = null;
    
	/** Offset from now. Min available to delivery date base on it.
     *  checkWindowAvailability method use it
     * @type int     
     */    
    private static $_minAvailableDateOffset = 2;
    
    public function __construct($id = null) {
        self::$_instance = $this;
        if ($id) {
            $this->getById($id);
        }
        return $this;
    }

    public function getByDate($date)
    {
        $this->_getbackend()->getByDate($this, $date);
        return $this;
    }
    
    public function getByDateString ($date)
    {
        $this->_getbackend()->getByDateString($this, $date);
        return $this;
    }
    
    public static function instance ()
    {
        if(!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }
    
	/** Checks if date available
     * @param mixed $begin - timestamp, date string or null
     * @param mixed $end - timestamp, date string or null
     * @return bool - true if found date between $begin - $end
     */    
    public static function checkWindowAvailability ($begin, $end)
    {   if(!is_null($begin)) {
        $min = self::$_minAvailableDateOffset * 86400 + mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
        if(!is_numeric($begin) && is_string($begin)) {
            $begin = strtotime($begin);
        }

        
        if($begin < $min) {
            $begin = $min;
        }
            }
            
        if(!is_numeric($end) && is_string($end)) {
            $end = strtotime($end);
        }
                    
        return self::instance()->_getbackend()->checkWindowAvailability($begin, $end);
    }
    

}
