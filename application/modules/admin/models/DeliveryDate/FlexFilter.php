<?php

/**
 * Author: Alex P.
 * Date: 04.08.14
 * Time: 20:21
 */

/**
 * Class Admin_Model_DeliveryDate_FlexFilter
 * @method \Admin_Model_DeliveryDate_FlexFilter id(string $operator, int $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_id(string $operator, int $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter date(string $operator, int $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_date(string $operator, int $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter date_string(string $operator, string $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_date_string(string $operator, string $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter is_first_period_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter is_second_period_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter is_third_period_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter is_first_period_for_small_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter is_second_period_for_small_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter is_third_period_for_small_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_is_first_period_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_is_second_period_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_is_third_period_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_is_first_period_for_small_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_is_second_period_for_small_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_FlexFilter or_is_third_period_for_small_available(string $operator, bool $value)
 * @method \Admin_Model_DeliveryDate_Collection apply(int $limit = null, int $offset = null)
 */
class Admin_Model_DeliveryDate_FlexFilter extends Ikantam_Filter_Flex
{
    protected $_acceptClass = 'Admin_Model_DeliveryDate_Collection';

    protected $_joins = array();
    protected $_select = array();
    protected $_rules = array();
} 