<?php

class Admin_Model_DeliveryDate_Collection extends Application_Model_Abstract_Collection
    implements Ikantam_Filter_Flex_Interface
{
	protected $_backendClass = 'Admin_Model_DeliveryDate_Backend';

    protected function _getBackEnd()
    {
        return new $this->_backendClass();
    }
       
    	/**
         * @param array $id 
         * @return Category_Model_Category_Collection
         */
    public function getById ($id)
    {
        $this->_getBackEnd()->getCollectionById($this, $id);
        return $this;
    }
    
    	/**
         * @param  array $range (0=>timestamp1, 1=>timestamp2)
         * @return self
         */
    public function getByDateRange ($range)
    {
        $this->_getBackEnd()->getByDateRange($this, $range);
        return $this;
    }

    /**
     * Returns a filter related with collection.
     * @return object Ikantam_Filter_Flex
     */
    public function  getFlexFilter()
    {
        return new Admin_Model_DeliveryDate_FlexFilter($this);
    }
}

