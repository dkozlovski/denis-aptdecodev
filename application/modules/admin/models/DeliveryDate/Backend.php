<?php

class Admin_Model_DeliveryDate_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'delivery_dates';
    protected $_dateStringFormat = 'd M Y D';

    public function getCollectionById($collection, $id)
    {
        if (array_product($id)) {
            $in = implode(',', $id);
            $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `id` IN(' . $in . ')';

            $stmt = $this->_getConnection()->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($result as $row) {
                $object = new Admin_Model_DeliveryDate();
                $object->addData($row);

                $collection->addItem($object);
            }
        }
    }

    public function getByDateString(\Application_Model_Abstract $object, $date)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `date_string` = :date LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':date', $date);

        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            $object->setData($result);
        }
    }

    public function getByDate(\Application_Model_Abstract $object, $date)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `date` = :date LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':date', $date);

        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            $object->setData($result);
        }
    }

    public function getByDateRange(\Application_Model_Abstract_Collection $collection, $range)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `date` BETWEEN :begin AND :end ORDER BY `date` ASC";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':begin', $range[0]);
        $stmt->bindParam(':end', $range[1]);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $date = new Admin_Model_DeliveryDate();
            $date->addData($row);
            $collection->addItem($date);
        }
    }


    public function checkWindowAvailability ($begin, $end)
    {
        $sql = "SELECT COUNT(*) as 'available' FROM `" . $this->_getTable() . "` WHERE
               (`is_first_period_available` = 1 OR `is_second_period_available` = 1 OR `is_third_period_available` = 1)";
        
        if($begin) {
            $sql .= " AND `date` > :begin" ;
        }
        
        if($end) {
            $sql .= " AND `date` < :end" ;
        }     
   
                
        $stmt = $this->_getConnection()->prepare($sql);
        
        if($begin)
        $stmt->bindParam(':begin', $begin);
        if($end)
        $stmt->bindParam(':end', $end);
        
        $stmt->execute();
        
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        
        return  $result['available'] > 0;
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = "UPDATE `" . $this->_getTable() . "`
        SET
            `is_first_period_available`            = :FP,
            `is_first_period_for_small_available`  = :FPS,
            `is_second_period_available`           = :SP,
            `is_second_period_for_small_available` = :SPS,
            `is_third_period_available`            = :TP,
            `is_third_period_for_small_available`  = :TPS,
            `is_fourth_period_available`            = :FoP,
            `is_fourth_period_for_small_available`  = :FoPS
        WHERE `id` = :id";

        $stmt = $this->_getConnection()->prepare($sql);
        /* @var $object Admin_Model_DeliveryDate */
        $id  = $object->getId();
        $fp  = $object->getIsFirstPeriodAvailable(0);
        $fps = $object->getIsFirstPeriodForSmallAvailable(0);
        $sp  = $object->getIsSecondPeriodAvailable(0);
        $sps = $object->getIsSecondPeriodForSmallAvailable(0);
        $tp  = $object->getIsThirdPeriodAvailable(0);
        $tps = $object->getIsThirdPeriodForSmallAvailable(0);
        $fop  = $object->getIsFourthPeriodAvailable(0);
        $fops = $object->getIsFourthPeriodForSmallAvailable(0);

        $stmt->bindParam(':id', $id);
        //$stmt->bindParam(':date', $object->getDate());
        //$stmt->bindParam(':dateString', date($this->_dateStringFormat, $object->getDate()));
        $stmt->bindParam(':FP', $fp);
        $stmt->bindParam(':FPS', $fps);
        $stmt->bindParam(':SP', $sp);
        $stmt->bindParam(':SPS', $sps);
        $stmt->bindParam(':TP', $tp);
        $stmt->bindParam(':TPS', $tps);
        $stmt->bindParam(':FoP', $fop);
        $stmt->bindParam(':FoPS', $fops);

        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = "INSERT INTO `" . $this->_getTable() . "` 
        (`id`, 
        `date`,
        `date_string`, 
        `is_first_period_available`,
        `is_first_period_for_small_available`,
        `is_second_period_available`,
        `is_second_period_for_small_available`,
        `is_third_period_available`,
        `is_third_period_for_small_available`,
        `is_fourth_period_available`,
        `is_fourth_period_for_small_available`
        )
        
        VALUES (NULL, :date, :dateString, :FP, :FPS, :SP, :SPS, :TP, :TPS, :FoP, :FoPS)";

        $stmt = $this->_getConnection()->prepare($sql);
        /* @var $object Admin_Model_DeliveryDate */
        $date = $object->getDate();
        $dateString = date($this->_dateStringFormat, $object->getDate());
        $fp  = $object->getIsFirstPeriodAvailable(0);
        $fps = $object->getIsFirstPeriodForSmallAvailable(0);
        $sp  = $object->getIsSecondPeriodAvailable(0);
        $sps = $object->getIsSecondPeriodForSmallAvailable(0);
        $tp  = $object->getIsThirdPeriodAvailable(0);
        $tps = $object->getIsThirdPeriodForSmallAvailable(0);
        $fop  = $object->getIsFourthPeriodAvailable(0);
        $fops = $object->getIsFourthPeriodForSmallAvailable(0);


        $stmt->bindParam(':date', $date);
        $stmt->bindParam(':dateString', $dateString);
        $stmt->bindParam(':FP', $fp);
        $stmt->bindParam(':FPS', $fps);
        $stmt->bindParam(':SP', $sp);
        $stmt->bindParam(':SPS', $sps);
        $stmt->bindParam(':TP', $tp);
        $stmt->bindParam(':TPS', $tps);
        $stmt->bindParam(':FoP', $fop);
        $stmt->bindParam(':FoPS', $fops);

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

}
