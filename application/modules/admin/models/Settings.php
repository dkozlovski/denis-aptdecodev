<?php

class Admin_Model_Settings
{

    protected $_model = null;
    protected $_eav = null;
    protected $_settingRows = array();

    public function __construct()
    {
        $this->_model = new Zend_Db_Table('settings');
        $this->_model->setRowClass('Eav_Eav_Row');
        $this->_eav = new Eav_Setting($this->_model);
    }

    /**
     * @param $name
     * @param array $values
     *
     * example params:
     *
     * $name =  array('config_name' => array('attribute' => 'value'))
     *  OR
     * $name = 'config_name', $values = array('attribute' => 'value')
     *
     * 'config_name' -- field 'name' in 'settings"
     * 'attribute'   -- field 'attribute_name' in 'setting_attribute"
     * 'value'       -- some value
     *
     * @return $this
     */
    public function saveConfig($name, $values = array())
    {
        if (is_array($name) && !$values) {
            foreach ($name as $_name => $_values) {
                $this->saveConfig($_name, $_values);
            }
            return $this;
        }

        $settingsModel = $this->_getModel();
        $settingsEav = $this->_getEav();
        $select = $settingsModel->select()->where('name = ?', $name);
        $row = $settingsModel->fetchRow($select);

        if ($row === null) {
            $entityId = $settingsModel->createRow(array('name' => $name))->save();
            $select = $settingsModel->select()->where('entity_id = ?', $entityId);
            $row = $settingsModel->fetchRow($select);
        }

        try {
            foreach ($values as $_attribute => $_value) {
                $settingsEav->setAttributeValue($row, $_attribute, $_value);
            }
        } catch (Exception $e) {

        }
        return $this;
    }

    /**
     * @param $name
     * @return Zend_Db_Table_Row
     */
    protected  function _getSettingRows($name)
    {
        if (empty($this->_settingRows[$name])) {
            $settingsModel = $this->_getModel();
            $select = $settingsModel->select()->where('name = ?', $name);
            $settingRows = $settingsModel->fetchAll($select);
            $attributeModel = new Zend_Db_Table($this->_getEav()->getAttributeTableName());
            $attrs = $attributeModel->fetchAll();
            $this->_getEav()->loadAttributes($settingRows, $attrs);
            $this->_settingRows[$name] = $settingRows;
        }
        return $this->_settingRows[$name];
    }

    /**
     * @param $name
     * @param $attribute
     * @return mixed|null
     */
    public function getValue($name, $attribute)
    {
        try {
            $settingRows = $this->_getSettingRows($name);

            $row = new Zend_Db_Table_Row();
            foreach ($settingRows as $_row) {
                $row = $_row;
            }
            return  $this->_getEav()->getAttributeValue($row, $attribute);
        } catch (Exception $e) {
            return null;
        }
    }

    public function getValuesForAdminForm(array $names, array $attributes)
    {
        $attributeModel = new Zend_Db_Table($this->_getEav()->getAttributeTableName());
        $attrs = $attributeModel->fetchAll();

        $settingsModel = $this->_getModel();
        $select = $settingsModel->select()->where('name IN (?)', $names);
        $settingRows = $settingsModel->fetchAll($select);

        $this->_getEav()->loadAttributes($settingRows, $attrs);

        $values = array();
        foreach ($settingRows as $_row) {
            foreach ($attributes as $_attributes) {
                $values[$_row->name . '__' .$_attributes ] =  $this->_getEav()->getAttributeValue($_row, $_attributes);

            }
        }
        return $values;
    }

    /**
     * @return Zend_Db_Table
     */
    protected  function _getModel()
    {
        return  $this->_model;
    }

    /**
     * @return Eav_Setting
     */
    protected function _getEav()
    {
        return $this->_eav;
    }
}
