<?php

class Admin_Model_InviteRequest_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'invitation_requests';

    public function getCollectionById($collection, $id)
    {
        if (array_product($id)) {
            $in = implode(',', $id);
            $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `id` IN(' . $in . ')';

            $stmt = $this->_getConnection()->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            foreach ($result as $row) {
                $object = new Admin_Model_InviteRequest();
                $object->addData($row);

                $collection->addItem($object);
            }
        }
    }

    public function getPaginator($conditions, $orderBy)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from($this->_getTable());
        if (is_array($conditions)) {
            foreach ($conditions as $condition) {
                if (is_string($condition)) {
                    $select->where($condition);
                } if (is_array($condition) && count($conditions == 2)) {
                    $select->where($condition[0], $condition[1]);
                }
            }
        }
        $select->order($orderBy);
        $paginator = Zend_Paginator::factory($select);
        return $paginator;
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = "UPDATE `" . $this->_getTable() . "` 
       SET `email` = :email,`is_processed` = :isProcessed
       WHERE `id` = :id";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':id', $object->getId());

        $stmt->bindParam(':email', $object->getEmail());
        $stmt->bindParam(':isProcessed', $object->getIsProcessed());


        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = "INSERT INTO `" . $this->_getTable() . "` 
        (`id`, `email`, `is_processed`, `created_at`)
        VALUES (NULL, :email, :isProcessed, :created)";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':email', $object->getEmail());
        $stmt->bindParam(':isProcessed', $object->getIsProcessed(0));
        $stmt->bindParam(':created', time());

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

}
