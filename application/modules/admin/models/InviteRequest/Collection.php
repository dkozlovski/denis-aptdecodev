<?php

class Admin_Model_InviteRequest_Collection extends Application_Model_Abstract_Collection
{
	protected $_backendClass = 'Admin_Model_InviteRequest_Backend';
    protected $_paginator;  


    protected function _getBackEnd()
    {
        return new $this->_backendClass();
    }
       
    	/**
         * @param array $id 
         * @return Category_Model_Category_Collection
         */
    public function getById ($id)
    {
        $this->_getBackEnd()->getCollectionById($this, $id);
        return $this;
    }
    
    	/**
         * Return paginator and retrieves the relevant rows
         * @param int $currentPage
         * @param int $itemsPerPage
         * @param int $pageRange
         * @param array $conditions - SQL WHERE conditions: array('id IN (1,2,3)',..., array('email = ?', 'email@example.com')) 
         * @return Zend_Paginator
         */
    public function getPaginator ($currentPage = 1, $itemsPerPage = 10, $pageRange = 5, $conditions = null, $sortBy = null)
    {
        $this->clear();
        $this->_paginator = $this->_getBackEnd()->getPaginator($conditions, $sortBy);
        $this->_paginator->setPageRange($pageRange)
             ->setItemCountPerPage($itemsPerPage)
             ->setCurrentPageNumber($currentPage);
         foreach($this->_paginator as $row)
         {
            $req = new Admin_Model_InviteRequest();
            $req->addData($row);
            $this->addItem($req);
         }    
                     
        return $this->_paginator;
    }
    
    	/**
         * @param string $searchValue
         * @param int $howMany - if you need to retrieve all rows set it to -1
         * @return Zend_Paginator
         */
    
    public function searchByEmail ($searchValue, $howMany = 10, $page = null)
    {
        $page = $page ? $page : 1;
        return $this->getPaginator($page, $howMany,  5, array(array('email LIKE ?', $searchValue.'%')));           
    }
    
     


}

