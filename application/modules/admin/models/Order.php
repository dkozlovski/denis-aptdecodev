<?php
/**
 * Class Admin_Model_Order
 * @method Admin_Model_Order_Backend _getbackend()
 */
class Admin_Model_Order extends Order_Model_Order
{

    protected $_coupon;
    protected $_backendClass = 'Admin_Model_Order_Backend';

    public function __construct($id = null)
    {
        if ($id) {
            $this->getById($id);
        }
        return $this;
    }

    public function getAllCount()
    {
        $this->_getBackEnd()->getAllCount($this);
        return $this;
    }

    public function getAddress($type)
    {
        $this->_getBackEnd()->getAddress($this, $type);
        return $this;
    }

    public function getShippingMethod()
    {
        $this->_getBackEnd()->getShippingMethod($this);
        return $this;
    }

    public function getStatus()
    {
        $this->_getBackEnd()->getStatus($this);
        return $this;
    }

    public function getCoupon()
    {
        if (!$this->_coupon) {
            $coupon = new Cart_Model_Coupon();
            $coupon->getById($this->getCouponId());

            $this->_coupon = $coupon;
        }

        return $this->_coupon;
    }

    /* public function IsSearchedOrderByText($orderId, $searchText) {
      $this->_getBackEnd()->getSearchedOrderByText($this, $orderId, $searchText);
      if ($this->getCount() != 0) {
      return true;
      } else {
      return false;
      }
      } */

    /*
      public function getCountByUser($userId) {
      $this->_getBackEnd()->getCountByUser($this, $userId);
      return $this;
      } */

    protected function _insert(\Application_Model_Abstract $object)
    {
        
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        
    }

///not used there
    /* protected function _delete(\Application_Model_Abstract $object) {
      $sql = "DELETE FROM `" . $this->_getTable() . "`
      WHERE `id` = :id";

      $stmt = $this->_getConnection()->prepare($sql);

      $stmt->bindParam(':id', $object->getId());

      $stmt->execute();
      } */
}