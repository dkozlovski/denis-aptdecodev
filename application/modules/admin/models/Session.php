<?php

class Admin_Model_Session extends Ikantam_Session
{

	protected $_user;
	protected $_cart;
    private static $_instance_ = null;
    
    public function __construct()
    {
        parent::__construct();
        self::$_instance_ = $this;
    }
        
	public function getAdmin()
	{
		if (!$this->_user) {
			$this->_user = new Admin_Model_Admin();
			$this->_user->getById($this->getAdminId());
		}

		return $this->_user;
	}

	public function getCart()
	{
		if (!$this->_cart) {
			$this->_cart = new Cart_Model_Cart();
			if (!$this->getCartId()) {
				$this->_cart->save();
				$this->setCartId($this->_cart->getId());
			} else {
				$this->_cart->getById($this->getCartId());
			}
		}

		return $this->_cart;
	}

	public function isLoggedIn()
	{
		return (bool) $this->getAdmin()->getId();
	}

	public function authenticate($loginData)
	{
		$this->getAdmin()->authenticate($loginData);
		$this->setAdminId($this->getAdmin()->getId()); 
	}

	public function register($signupData)
	{
		$this->getAdmin()->register($signupData);
		$this->setAdminId($this->getAdmin()->getId());
	}
    
    public static function instance()
    {
        if (!self::$_instance_ instanceof self) {
            self::$_instance_ = new self();
        }

        return self::$_instance_;
    }    

}
