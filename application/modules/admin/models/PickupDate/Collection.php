<?php

class Admin_Model_PickupDate_Collection extends Admin_Model_DeliveryDate_Collection
    implements Ikantam_Filter_Flex_Interface
{
    protected $_backendClass = 'Admin_Model_PickupDate_Backend';

    /**
     * Returns a filter related with collection.
     * @return object Ikantam_Filter_Flex
     */
    public function  getFlexFilter()
    {
        return new Admin_Model_PickupDate_FlexFilter($this);
    }
}

