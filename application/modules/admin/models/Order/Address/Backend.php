<?php

class Admin_Model_Order_Address_Backend extends Order_Model_Order_Address_Backend
{
    
    public function getAddress($address, $orderId, $type) {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` 
            WHERE `order_id` = :order_id and `address_type` = :type";
        
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':order_id', $orderId);
        $stmt->bindParam(':type', $type);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $address->addData($row);
        }
    }
}

