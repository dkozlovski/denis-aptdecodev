<?php

/**
 * Class Admin_Model_Order_Item
 *  @method Admin_Model_Order_Item_Backend _getbackend()
 */
class Admin_Model_Order_Item extends Order_Model_Item_Abstract
{
    protected $_seller;
    protected $_buyer;

    public function getSeller()
    {
        if (!$this->_seller) {
            if ($this->getSellerId()) {
                $this->_seller = new User_Model_User($this->getSellerId());
            } else {
                $this->_seller = $this->getProduct()->getSeller();
            }
        }
        return $this->_seller;
    }

    public function getBuyer()
    {
        if (!$this->_buyer) {
            if ($this->getBuyerId()) {
                $this->_buyer = new User_Model_User($this->getBuyerId());
            } else {
                if ($this->getOrder()->getUserId()) {
                    $this->_buyer = new User_Model_User($this->getOrder()->getUserId());
                } else {
                    $this->_buyer = new User_Model_User();
                    $this->_buyer->setFullName($this->getOrder()->getUserFullName())
                        ->setEmail($this->getOrder()->getUserEmail());
                }
            }
        }
        return $this->_buyer;
    }

    public function getOrder()
    {
        if ($this->_order == null) {
            $this->_order = new Admin_Model_Order($this->getOrderId());
        }
        return $this->_order;
    }

    public function confirmAvailability($sellerDate, $options, $buyerDate = null)
    {
        Httpful\Bootstrap::init();
        RESTful\Bootstrap::init();
        Balanced\Bootstrap::init();
        Balanced\Settings::init();

        if ($this->getIsProductAvailable()) {//availability already confirmed, save new date
            $this->setSellerDates($sellerDate)
                ->setBuyerDate($buyerDate)->save();

            $mailer = new Notification_Model_Order_Confirmed();
            $mailer->sendEmails($this);

            return true;
        } else {

            $hold  = Balanced\Hold::get($this->getHoldUri());
            $debit = $hold->capture();

            $additionalDebitUri = null;
            if ($this->getAdditionalHoldUri()) {
                $additionalHold  = Balanced\Hold::get($this->getAdditionalHoldUri());
                $additionalDebit = $additionalHold->capture();
                $additionalDebitUri = $additionalDebit->uri;
            }

            if ($debit) {
                if (!$this->getOrder()->getUser()->getId()) {//guest order
                    $this->setAccessCode(Ikantam_Math_Rand::getString(64, Ikantam_Math_Rand::ALPHA_NUMERIC));
                }

                $this->setSellerDates($sellerDate)
                    ->setBuyerDate($buyerDate)
                    ->setSellerDeliveryOptions($options)
                    ->setIsAvailable(1)
                    ->setIsProductAvailable(1)
                    ->setStatus(Order_Model_Order::ORDER_STATUS_PENDING)
                    ->setDebitsUri($debit->uri)
                    ->setAdditionalDebitsUri($additionalDebitUri)
                    ->save();

                /* if ($sellerDate != 'other' || $this->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_PICK_UP) {
                     $mailer = new Notification_Model_Order_Confirmed();
                     $mailer->sendEmails($this);
                 }*/

                if ($sellerDate !== 'not-selected' && $sellerDate !== 'not-selected') {
                    $mailer = new Notification_Model_Order_Confirmed();
                    $mailer->sendEmails($this);
                }
                return true;
            }
        }

        return false;
    }

    public function invalidateAvailability()
    {
        Httpful\Bootstrap::init();
        RESTful\Bootstrap::init();
        Balanced\Bootstrap::init();
        Balanced\Settings::init();

        $payout = new Order_Model_Payout();
        $payout->getByItemId($this->getId());

        $hold    = Balanced\Hold::get($this->getHoldUri());
        $success = $hold->void();

        if ($this->getAdditionalHoldUri()) {
            $additionalHold    = Balanced\Hold::get($this->getAdditionalHoldUri());
            $additionalHold->void();
        }

        if ($success && $payout->getId()) {
            $this->setIsAvailable(0)
                ->setIsProductAvailable(0)
                ->setStatus(Order_Model_Order::ORDER_STATUS_CANCELED)
                ->save();

            if ($this->getProduct()->getIsSold()) {
                $this->getProduct()->setIsPublished(0)->save();
            }

            $payout->setIsActive(0)->save();

            $mailer = new Notification_Model_Order_Invalidated();
            $mailer->sendEmails($this);

            $this->_recalculatePayouts($this);
            return true;
        }

        return false;
    }

    protected function _recalculatePayouts($orderItem)
    {
        /* Recalculate payout with new shipping fee */

        $allItems = new Order_Model_Item_Collection();
        $payouts  = new Order_Model_Payout_Collection();

        $allItems->getByOrderId($orderItem->getOrderId());

        $totalShippingPrice    = 0.0;
        $numberOfDeliveryItems = 0;
        $options               = new Application_Model_Options();

        foreach ($allItems as $dItem) {
            $dItem->getProduct()->setQty(0)
                ->save();
            if ($dItem->getStatus() != Order_Model_Order::ORDER_STATUS_CANCELED && $dItem->getId() != $orderItem->getId()) {
                if ($dItem->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
                    $totalShippingPrice += $dItem->getSummaryPrice();
                    $numberOfDeliveryItems += $dItem->getQty(1);
                    $oldPayout = new Order_Model_Payout();   //disactive old payout
                    $oldPayout->getByItemId($dItem->getId())
                        ->setIsActive(0)
                        ->save();

                    $newPayout = $oldPayout->unsId(); //new payout same as old but shipping_price
                    $newPayout->setIsActive(1)
                        ->setShippingPrice(($options->getFee($newPayout->getProductPrice() * $dItem->getQty(1), $options::BUYER) * $newPayout->getProductPrice() * $dItem->getQty(1)) / 100);


                    $payouts->addItem($newPayout); //store in collection for future updates    
                    Cart_Model_Item_Collection::removeNotAvailable(); //clear carts
                }
            }
        }


        $SFCollection = new Application_Model_ShippingFeeRule_Collection();
        $shippingFee  = $SFCollection->getShippingFee($totalShippingPrice, $numberOfDeliveryItems); //new shipping price

        if ($shippingFee > 0 && $payouts->getSize() > 0) {

            $shippingFee = $shippingFee / $payouts->getSize();

            foreach ($payouts as $newPayout) {
                $newPayout->setShippingPrice($newPayout->getShippingPrice() + $shippingFee)
                    ->save();
            }
        }

        //*****************END*RECALCULATION************************ 
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Ikantam_Url::getUrl('admin/users/order', array('id' => $this->getId()));
    }

    public function getShippingStatuses()
    {
        return array('', 'outstanding', 'complete', 'cancelled');
    }

    public function isPaid()
    {
        return (bool) $this->getPayout()->getIsProcessed();
    }

    public function isDisputed()
    {
        return ($this->getPayout()->getDisputeStatus() != null);
    }

    public function getShippingPriceForOrderItem()
    {
        $planer = new Shipping_Model_RoutePlanner();
        $totalsShipping=  new Cart_Model_Totals_Shipping();
        return $totalsShipping->getShippingPriceForOrderItem($planer, $this->getOrder()->getShippingAddress(), $this, false);
    }
}