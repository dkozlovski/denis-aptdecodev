<?php
/**
 * Class Admin_Model_Order_Item_Collection
 * @method Admin_Model_Order_Item_Backend _getBackend()
 */
class Admin_Model_Order_Item_Collection extends Application_Model_Abstract_Collection
{

    public function getAllItems($params)
    {
        $this->_getBackend()->getAllItems($this, $params);
        return $this;
    }
    
    public function getAllItemsForAdmin($params)
    {
        $this->_getBackend()->getAllItemsForAdmin($this, $params);
        return $this;
    }

    public function getTrackingList($params = array())
    {
        $this->_getBackend()->getTrackingList($this, $params);
        return $this;
    }
}

