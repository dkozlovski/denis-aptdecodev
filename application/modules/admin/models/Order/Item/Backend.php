<?php

class Admin_Model_Order_Item_Backend extends Application_Model_Abstract_Backend
{

    protected $_table   = 'order_items';
    protected $_primary = 'id';
    protected $_fields  = array(
        'order_id',
        'product_id',
        'product_title',
        'product_price',
        'qty',
        'row_total',
        'shipping_method_id',
        'shipping_amount',
        'discount_amount',
        'options',
        'is_received',
        'is_delivered',
        'is_available',
        'is_product_available',
        'status',
        'hold_uri',
        'debits_uri',
        'additional_hold_uri',
        'additional_debits_uri',
        'is_notified',
        'access_code',
        'is_carrier_sent_email',
        'carrier_work_number',
        'shipping_status',
        'follow_up',
        'notes'
    );

    protected function _beforeInsert($object)
    {
        $object->setCreatedAt(time());
        $object->setUpdatedAt(time());

        if ($object->getIsCarrierSentEmail() === null) {
            $object->setIsCarrierSentEmail(0);
        }

        if ($object->getShippingStatus() === null) {
            $object->setShippingStatus('');
        }
    }

    protected function _beforeUpdate($object)
    {
        $object->setUpdatedAt(time());

        if ($object->getIsCarrierSentEmail() === null) {
            $object->setIsCarrierSentEmail(0);
        }

        if ($object->getShippingStatus() === null) {
            $object->setShippingStatus('');
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $this->_beforeInsert($object);

        $template = 'INSERT INTO `%s` (%s) VALUES (%s)';
        $bind     = array();
        $fields   = '`' . implode('`, `', $this->_fields) . '`';
        $values   = ':' . implode(', :', $this->_fields);

        foreach ($this->_fields as $field) {
            $bind[':' . $field] = $object->getData($field);
        }

        $sql  = sprintf($template, $this->_getTable(), $fields, $values);
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $this->_beforeUpdate($object);

        $bind = array();

        $template = 'UPDATE `%s` SET %s WHERE %s';

        $set = array();
        foreach ($this->_fields as $field) {
            $set[]              = sprintf('`%s` = :%s', $field, $field);
            $bind[':' . $field] = $object->getData($field);
        }

        $where                       = sprintf('`%s` = :%s', $this->_primary, $this->_primary);
        $bind[':' . $this->_primary] = $object->getData($this->_primary);
        $sql                         = sprintf($template, $this->_getTable(), implode(', ', $set), $where);

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
    }

    public function getAllItems(Application_Model_Abstract_Collection $collection, $params)
    {
        $where      = $bindParams = array();

        $sql = 'SELECT `order_items`.`id` as `id`, `products`.`user_id` AS `seller_id`,
			`orders`.`user_id` AS `buyer_id`,
			`products`.`id` AS `product_id`,
            `order_items`.`qty` AS `qty`
           
			FROM `orders`
			JOIN `order_items` ON `orders`.`id` = `order_items`.`order_id`
			JOIN `products` ON `order_items`.`product_id` = `products`.`id`';

        if (isset($params['seller_id'])) {
            $where[]                  = '( `products`.`user_id` = :seller_id)';
            $bindParams[':seller_id'] = $params['seller_id'];
        }

        if (isset($params['buyer_id'])) {
            $where[]                 = '(`orders`.`user_id` = :buyer_id)';
            $bindParams[':buyer_id'] = $params['buyer_id'];
        }

        if (isset($params['is_received'])) {
            $where[]                    = '(`order_items`.`is_received` = :is_received)';
            $bindParams[':is_received'] = $params['is_received'];
        }

        if (isset($params['is_delivered'])) {
            $where[]                     = '(`order_items`.`is_delivered` = :is_delivered)';
            $bindParams[':is_delivered'] = $params['is_delivered'];
        }

        if (isset($params['title'])) {
            $where[]              = '(`order_items`.`title` LIKE :title)';
            $bindParams[':title'] = '%' . str_replace('%', '', $params['title']) . '%';
        }

        if (count($where) > 0) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }



        $sql .= ' ORDER BY `orders`.`created_at` DESC';

        if (isset($params['offset'], $params['limit'])) {
            $sql .= sprintf(' LIMIT %d, %d', $params['offset'], $params['limit']);
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bindParams);

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Admin_Model_Order_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getAllItemsForAdmin(Application_Model_Abstract_Collection $collection, $params)
    {
        $where      = $bindParams = array();

        $sql = 'SELECT `order_items`.`id` as `id`, `products`.`user_id` AS `seller_id`,
			`orders`.`user_id` AS `buyer_id`,
			`products`.`id` AS `product_id`,
            `order_items`.`qty` AS `qty`,
            `sellers`.`full_name` as `seller_name`,
            `buyers`.`full_name` as `buyer_name`,
            `sellers`.`email` as `seller_email`,
            `buyers`.`email` as `buyer_email`,
            `order_addresses`.`full_name` as `delivery_name`,
            `user_addresses`.`full_name` as `pickup_name`,
            `orders`.`user_full_name` as `o_full_name`,
            `orders`.`user_email` as `o_email`

           
			FROM `orders`
			JOIN `order_items` ON `orders`.`id` = `order_items`.`order_id`
			LEFT JOIN `products` ON `order_items`.`product_id` = `products`.`id`
            LEFT JOIN `users` as `sellers` ON `products`.`user_id` = `sellers`.`id`
            LEFT JOIN `users` as `buyers` ON `orders`.`user_id` = `buyers`.`id`
            LEFT JOIN `order_addresses` ON `orders`.`shipping_address_id` = `order_addresses`.`id`
            LEFT JOIN `user_addresses` ON `products`.`pick_up_address_id` = `user_addresses`.`id`';

        if (isset($params['seller_id'])) {
            $where[]                  = '( `products`.`user_id` = :seller_id)';
            $bindParams[':seller_id'] = $params['seller_id'];
        }

        if (isset($params['order_id'])) {
            $where[]                 = '( `order_items`.`id` = :order_id1) OR ( `orders`.`id` = :order_id2)';
            $bindParams[':order_id1'] = $params['order_id'];
            $bindParams[':order_id2'] = $params['order_id'];
        }

        if (isset($params['buyer_id'])) {
            $where[]                 = '(`orders`.`user_id` = :buyer_id)';
            $bindParams[':buyer_id'] = $params['buyer_id'];
        }

        if (isset($params['status'])) {
            switch ($params['status']) {
                case Order_Model_Order::ORDER_STATUS_ON_HOLD:
                    $where[] = sprintf('(`order_items`.`status` = "%s")', Order_Model_Order::ORDER_STATUS_ON_HOLD);
                    break;
                case Order_Model_Order::ORDER_STATUS_PENDING:
                    $where[] = sprintf('(`order_items`.`status` = "%s")', Order_Model_Order::ORDER_STATUS_PENDING);
                    break;
                case 'pending-delivery-date':
                    $where[] = sprintf('(`order_items`.`status` = "%s")', Order_Model_Order::ORDER_STATUS_PENDING);
                    $where[] = '(`order_items`.`options` LIKE "%not-selected%")';
                    break;
                case Order_Model_Order::ORDER_STATUS_COMPLETE:
                    $where[] = sprintf('(`order_items`.`status` = "%s")', Order_Model_Order::ORDER_STATUS_COMPLETE);
                    break;
                case Order_Model_Order::ORDER_STATUS_CANCELED:
                    $where[] = sprintf('(`order_items`.`status` = "%s")', Order_Model_Order::ORDER_STATUS_CANCELED);
                    break;
            }
        }

        if (isset($params['title'])) {
            $where[] = '(`order_items`.`product_title` LIKE :title '
                    . 'OR `sellers`.`full_name` LIKE :sname '
                    . 'OR `buyers`.`full_name` LIKE :bname '
                    . 'OR `sellers`.`email` LIKE :semail '
                    . 'OR `buyers`.`email` LIKE :bemail '
                    . 'OR `order_addresses`.`full_name` LIKE :dname '
                    . 'OR `user_addresses`.`full_name` LIKE :pname '
                    . 'OR `products`.`pickup_full_name` LIKE :pfname '
                    . 'OR `orders`.`user_full_name` LIKE :oname '
                    . 'OR `orders`.`user_email` LIKE :oemail)';

            $bindParams[':title'] = '%' . str_replace('%', '', $params['title']) . '%';
            $bindParams[':sname'] = '%' . str_replace('%', '', $params['title']) . '%';
            $bindParams[':bname'] = '%' . str_replace('%', '', $params['title']) . '%';

            $bindParams[':semail'] = '%' . str_replace('%', '', $params['title']) . '%';
            $bindParams[':bemail'] = '%' . str_replace('%', '', $params['title']) . '%';

            $bindParams[':dname']  = '%' . str_replace('%', '', $params['title']) . '%';
            $bindParams[':pname']  = '%' . str_replace('%', '', $params['title']) . '%';
            $bindParams[':pfname'] = '%' . str_replace('%', '', $params['title']) . '%';

            $bindParams[':oname']  = '%' . str_replace('%', '', $params['title']) . '%';
            $bindParams[':oemail'] = '%' . str_replace('%', '', $params['title']) . '%';
        }

        if (count($where) > 0) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }

        $sql .= ' ORDER BY `orders`.`created_at` DESC';

        if (isset($params['offset'], $params['limit'])) {
            $sql .= sprintf(' LIMIT %d, %d', $params['offset'], $params['limit']);
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bindParams);

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Admin_Model_Order_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getTrackingList(Application_Model_Abstract_Collection $collection, $params = array())
    {
        $defaultParams = array(
            'shipping_method' => 'delivery',
            'status'          => 'pending'
        );
        $params        = array_merge($defaultParams, $params);

        $sql = 'SELECT `order_items`.`id` AS `id`,
            `order_items`.`product_title` AS `product_title`,
            `order_items`.`order_id` AS `order_id`,
            `order_items`.`status` AS `status`,
            `order_items`.`options` AS `options`,
            `order_items`.`is_carrier_sent_email` AS `is_carrier_sent_email`,
            `order_items`.`carrier_work_number` AS `carrier_work_number`,
            `order_items`.`shipping_status` AS `shipping_status`,
            `order_items`.`follow_up` AS `follow_up`,
            `order_items`.`notes` AS `notes`,
            `order_items`.`shipping_amount` AS `shipping_amount`,
            `products`.`user_id` AS `seller_id`,
			`products`.`id` AS `product_id`,
			`orders`.`user_id` AS `buyer_id`,
            `orders`.`created_at` AS `created_at`

			FROM `order_items`
			JOIN `orders` ON `orders`.`id` = `order_items`.`order_id`
			JOIN `products` ON `order_items`.`product_id` = `products`.`id`
			';

        $where      = $bindParams = array();
        if (isset($params['status'])) {
            $where[]               = '( `order_items`.`status` >= :status)';
            $bindParams[':status'] = $params['status'];
        }
        if (isset($params['start_date'])) {
            $where[]                   = '( `orders`.`created_at` >= :start_date)';
            $bindParams[':start_date'] = $params['start_date'];
        }
        if (isset($params['start_date'])) {
            $where[]                   = '( `orders`.`created_at` >= :start_date)';
            $bindParams[':start_date'] = $params['start_date'];
        }

        if (isset($params['end_date'])) {
            $where[]                 = '( `orders`.`created_at` <= :end_date)';
            $bindParams[':end_date'] = $params['end_date'];
        }

        if (isset($params['created_at'])) {
            $where[]                   = '(`orders`.`created_at` = :created_at)';
            $bindParams[':created_at'] = $params['created_at'];
        }

        if (count($where) > 0) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }

        $sql .= ' ORDER BY `order_items`.`id` DESC ';
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bindParams);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Admin_Model_Order_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

}