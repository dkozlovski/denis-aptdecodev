<?php

class Admin_Model_Order_Backend extends Order_Model_Order_Backend {

    //protected $_table = 'orders';

    public function getAll($collection) {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` as `a` WHERE 1';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Admin_Model_Order();
            $object->addData($row);
            $collection->addItem($object);
        }
    }
    
    public function getAllCount($orderItems) {
        $sql = 'SELECT COUNT(*) as `count` FROM `' . $this->_getTable() . '` WHERE 1';
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $orderItems->addData($row);
        }
    }

    /* public function SearchOrders($collection, $textSearch, $statusType, $paymentType) {
      $sql = "SELECT * FROM `" . $this->_getTable() . "` as `a`
      LEFT JOIN `order_items` as `b`
      ON `a`.`id` = `b`.`order_id`
      WHERE `b`.`title` like '%" . $textSearch . "%'";

      if ($statusType != '')
      $sql .= " and `is_delivered` = " . $statusType;
      /*if ($paymentType != 0)
      $sql .= */

    //$sql .= " GROUP BY `a`.`id`";

    /* $stmt = $this->_getConnection()->prepare($sql);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

      foreach ($result as $row) {
      $object = new Admin_Model_Order();
      $object->addData($row);
      $collection->addItem($object);
      }
      } */


    public function getShippingMethod($order) {
            /* LEFT JOIN `order_items` as `b`
              ON `a`.`id` = `b`.`order_id` */
            /* $sql = "SELECT  (
              SELECT COUNT(*)
              FROM `order_items`
              WHERE `order_id` = :order_id and `shipping_method_id`='pickup'
              ) AS `pick_up`,
              (
              SELECT COUNT(*)
              FROM `order_items`
              WHERE `order_id` = :order_id2 and `shipping_method_id`='delivery'
              ) AS `delivery` FROM dual";
             */
        $sql = "SELECT CASE count( * )
            WHEN 0 THEN 'Pick-up'
            WHEN (SELECT count( * )
                FROM `order_items`
            ) THEN 'Delivery'
            ELSE 'Pick-up & Delivery'
        END AS `shipping_method_id`
        FROM `order_items`
        WHERE `shipping_method_id` = 'delivery' 
            AND `order_id` = :order_id;";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':order_id', $order->getId());
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $order->addData($row);
        }
    }

    public function getStatus($order) {
        /* $sql = "SELECT COUNT(*) as `count` FROM `order_items`
          WHERE `order_id` = :order_id and `is_delivered`=0 "; */
        $sql = "SELECT CASE COUNT( * )
            WHEN 0 THEN 'Completed'
            ELSE 'In progress'
        END AS `status`
        FROM `order_items`
        WHERE `order_id` = :order_id
            AND `is_delivered` = 0";
        
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':order_id', $order->getId());
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $order->addData($row);
        }
    }



    public function searchOrders($collection, $searchText, $statusType) {
        /*$sql = "SELECT count(`a`.`id`) as `count` FROM `" . $this->_getTable() . "` as `a`
                    LEFT JOIN `order_items` as `b`
                        ON `a`.`id` = `b`.`order_id` 
            WHERE `a`.`id` = :order_id and `b`.`title` like '%" . $searchText . "%' 
            GROUP BY `a`.`id`";*/
        $sql = "SELECT `a`.*, 
            (SELECT CASE count( * )
                WHEN 0 THEN 'pick-up'
                WHEN (SELECT count( * )
                    FROM `order_items`
                ) THEN 'delivery'
                ELSE 'pick-up & delivery'
            END
            FROM `order_items`
            WHERE `shipping_method_id` = 'delivery' 
                AND `order_id` = `a`.`id`) AS `shipping_method_id`,
                
            (SELECT CASE COUNT( * )
                WHEN 0 THEN 'Completed'
                ELSE 'In progress'
            END 
            FROM `order_items`
            WHERE `order_id` = `a`.`id`
                AND `is_delivered` = 0) AS `status`
                
            FROM `" . $this->_getTable() . "` as `a`
                LEFT JOIN `order_items` as `b`
                    ON `a`.`id` = `b`.`order_id` 
            WHERE `b`.`title` like '%" . $searchText . "%' 
            GROUP BY `a`.`id` 
            HAVING `status` like '%" . $statusType . "%'";

        $stmt = $this->_getConnection()->prepare($sql);
        //$stmt->bindParam(':search_status', $statusType);
        //$stmt->bindParam(':order_id', $orderId);
        //$stmt->bindParam(':search_text', $searchText);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $order = new Admin_Model_Order();
            $order->addData($row);
            $collection->addItem($order);
        }
    }

   

    /* public function getCountByUser($review, $userId) {

      $sql = 'SELECT count(*) as `count` FROM `' . $this->_getTable() . '` WHERE `user_id` = :user_id';
      $stmt = $this->_getConnection()->prepare($sql);
      $stmt->bindParam(':user_id', $userId);
      $stmt->execute();
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      if ($row) {
      $review->addData($row);
      }
      }

      public function getByUser($collection, $userId) {

      $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `user_id` = ' . $userId;

      $stmt = $this->_getConnection()->prepare($sql);
      //$stmt->bindParam(':user_id', $userId);

      $stmt->execute();

      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

      foreach ($result as $row) {
      $review = new Review_Model_Review();
      $review->addData($row);
      $collection->addItem($review);
      }
      } */

    protected function _insert(\Application_Model_Abstract $object) {

        $sql = 'INSERT INTO `' . $this->_getTable() . '` 
            (`id`,
             `user_id`,
             `created_at`, 
             `total`,
             )
        VALUES ( 
            NULL,
            :user_id,
            :created_at,
            :total,
            )';
        $stmt = $this->_getConnection()->prepare($sql);

        $userId = $object->getUserId();
        $createdAt = $object->getCreatedAt();
        $total = $object->getTotal();

        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':total', $total);

        $stmt->execute();
    }

    protected function _update(\Application_Model_Abstract $object) {
        print_r('update not ready');
        die();
        $sql = "UPDATE `" . $this->_getTable() . "` 
            SET
                `id` = :id,
                `user_id` = :user_id,
                `created_at` = :created_at,
                `total` = :total,
            WHERE `id` = :id";
        $stmt = $this->_getConnection()->prepare($sql);

        $userId = $object->getUserId();
        $createdAt = $object->getCreatedAt();
        $total = $object->getTotal();

        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':total', $total);

        $stmt->execute();
    }

}