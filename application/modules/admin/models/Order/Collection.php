<?php

class Admin_Model_Order_Collection extends Order_Model_Order_Collection {

    protected $_backendClass = 'Admin_Model_Order_Backend';

    public function getAll() {
        $this->clear();
        $this->_getBackEnd()->getAll($this);
        return $this;
    }
    
    public function searchOrders($searchText, $statusType) {
        $this->clear();
        $this->_getBackEnd()->searchOrders($this, $searchText, $statusType);
        return $this;
    }
    
}

