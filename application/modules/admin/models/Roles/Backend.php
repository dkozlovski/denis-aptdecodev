<?php

class Admin_Model_Roles_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'admin_roles';

    public function getRole($id_role)
    {
        $dbh = Application_Model_DbFactory::getFactory()->getConnection();

        $sql  = "SELECT name FROM `" . $this->_getTable() . "` WHERE `id` = :role_id";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':role_id', $id_role);
        $stmt->execute();
        $row  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $row["name"];
    }

    public function getRoles()
    {

        $dbh  = Application_Model_DbFactory::getFactory()->getConnection();
        $sql  = "SELECT * FROM " . $this->getTable();
        $stmt = $dbh->prepare($sql);
        $stmt->execute();
        return $rows = $stmt->fetchAll();
    }

}