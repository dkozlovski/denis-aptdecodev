<?php

class Admin_Model_Settings_Backend extends Order_Model_Order_Backend
{

    protected $_table = 'settings';

    public function getAll($object)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` LIMIT 0,1';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            $object->addData($result);
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `' . $this->_getTable() . '` (`hours`, `text`, `subject`) VALUES (:hours, :text, :subject)';

        $hours   = $object->getHours();
        $text    = $object->getText();
        $subject = $object->getSubject();

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':hours', $hours);
        $stmt->bindParam(':text', $text);
        $stmt->bindParam(':subject', $subject);

        $stmt->execute();
        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = "UPDATE `" . $this->_getTable() . "` SET `hours` = :hours, `text` = :text, `subject` = :subject WHERE `id` = :id";

        $hours   = $object->getHours();
        $text    = $object->getText();
        $subject = $object->getSubject();
        $id      = $object->getId();

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':hours', $hours);
        $stmt->bindParam(':text', $text);
        $stmt->bindParam(':subject', $subject);
        $stmt->bindParam(':id', $id);

        $stmt->execute();
    }

}