<?php

class Admin_Model_Homepage_TmpObjects
{

    protected $_session         = null;
    protected $_categories      = null;
    protected $_brands          = null;
    protected $_background      = null;
    protected $_blog_background = null;
    protected $_product         = null;
    protected $_blog_post       = null;
    protected $_collection      = null; // set of products
    protected $lifeTime         = 2000;
    protected $startTime        = null;
    protected static $instance  = null;

    const BACKGROUND      = 'background';
    const BLOG_BACKGROUND = 'blog_background';
    const PRODUCT         = 'product';
    const CATEGORY        = 'category';
    const BRAND           = 'brand';
    const COLLECTION      = 'collection';
    const BLOG_POST       = 'blog_post';

    private function __construct()
    {
        $this->_session  = new Ikantam_Session();
        $this->startTime = $this->_session->getHPStartTime();
        if (!$this->startTime) {
            $this->startTime = time();
            $this->_session->setHPStartTime($this->startTime);
            $this->loadFromDB();
        } elseif ((time() - $this->startTime) >= $this->lifeTime) {
            $this->reset();
            $this->loadFromDB();
        }

        $this->load();
    }

    private function __clone()
    {
        
    }

    private function __wakeup()
    {
        
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function reset()
    {
        $this->_session->unsHPStartTime()
                ->unsTmpCategories()
                ->unsTmpBrands()
                ->unsTmpBackground()
                ->unsTmpBlogBackground()
                ->unsTmpProduct()
                ->unsTmpBlogPost();
    }

    protected function loadFromDB()
    {
        $bg = new Product_Model_HomeBackground();
        $this->save('background', $bg->getImageName());

        $blg = new Application_Model_BlogBackground();
        $this->save('blog_background', $blg->getImageName());

        $product = new Product_Model_Product();
        $this->save('product', $product->getFixedAtHomeProduct());

        $blog = new Application_Model_Static();
        $this->save('blog_post', $blog->getFixedAtHomeBlogPost());

        $categories = new Category_Model_Category_Collection();
        $categories->getCategoriesFixedAtHomePage();
        $brands     = new Application_Model_Manufacturer_Collection();
        $brands->getManufacturersFixedAtHomePage();

        foreach ($categories as $category) {
            $this->save('category', array('id' => $category->getId(), 'fileName' => $category->getHomeImageName()));
        }


        foreach ($brands as $brand) {
            $this->save('brand', array('id' => $brand->getId(), 'fileName' => $brand->getHomeImageName()));
        }
    }

    protected function load()
    {
        $sess = &$this->_session;

        $this->_product         = $sess->getTmpProduct();
        $this->_background      = $sess->getTmpBackground();
        $this->_categories      = $sess->getTmpCategories();
        $this->_brands          = $sess->getTmpBrands();
        $this->_blog_background = $sess->getTmpBlogBackground();
        $this->_blog_post       = $sess->getTmpBlogPost();
    }

    public function save($type, $data)
    {
        $uid = uniqid();
        switch ($type) {
            case self::BACKGROUND :

                $this->_background = (object) array('url' => Product_Model_HomeBackground::url($data), 'fileName' => $data);
                $this->_session->setTmpBackground($this->_background);
                break;

            case self::BLOG_BACKGROUND :

                $this->_blog_background = (object) array('url' => Application_Model_BlogBackground::url($data), 'fileName' => $data);
                $this->_session->setTmpBlogBackground($this->_blog_background);
                break;

            case self::PRODUCT :

                $this->_product = ($data instanceof Product_Model_Product) ? $data->getId() : (int) $data;
                $this->_session->setTmpProduct($this->_product);
                break;

            case self::BLOG_POST :
                $this->_blog_post = $data;
                $this->_session->setTmpBlogPost($this->_blog_post);
                break;

            case self::CATEGORY :

                if (!$this->_categories) {
                    $this->_categories = array();
                }

                $url                 = ($data['fileName']) ? Admin_Model_Homepage_Image::url($data['fileName']) : null;
                $this->_categories[] = (object) array('id'       => $data['id'],
                            'fileName' => $data['fileName'],
                            'url'      => $url,
                            'uid'      => $uid,
                );
                $this->_session->setTmpCategories($this->_categories);
                break;

            case self::BRAND :

                if (!$this->_brands)
                    $this->_brands   = array();
                $url             = ($data['fileName']) ? Admin_Model_Homepage_Image::url($data['fileName']) : null;
                $this->_brands[] = (object) array('id'       => $data['id'],
                            'fileName' => $data['fileName'],
                            'url'      => $url,
                            'uid'      => $uid,
                );
                $this->_session->setTmpBrands($this->_brands);
                break;

            default:
                throw new Exception(__METHOD__ . ' Unknown type "' . $type . '"');
                break;
        }

        return $uid;
    }

    public function attachImage($to, $uid, $fileName)
    {
        switch ($to) {
            case self::CATEGORY :
                if (is_array($this->_categories)) {
                    foreach ($this->_categories as $key => $std) {
                        if ($std->uid == $uid) {
                            $this->_categories[$key]->fileName = $fileName;
                            $this->_categories[$key]->url      = Admin_Model_Homepage_Image::url($fileName);
                            return $this->_categories[$key]->url;
                        }
                    }
                }
                break;


            case self::BRAND :
                if (is_array($this->_brands)) {
                    foreach ($this->_brands as $key => $std) {
                        if ($std->uid == $uid) {
                            $this->_brands[$key]->fileName = $fileName;
                            $this->_brands[$key]->url      = Admin_Model_Homepage_Image::url($fileName);
                            return $this->_brands[$key]->url;
                        }
                    }
                }
                break;

            default:
                throw new Exception(__METHOD__ . ' Unknown type "' . $type . '"');
                break;
        }

        return false;
    }

    public function remove($type, $uid = null)
    {

        switch ($type) {
            case self::BACKGROUND :
                $this->_session->unsTmpBackground();
                break;

            case self::PRODUCT :
                $this->_session->unsTmpProduct();
                break;

            case self::CATEGORY :
                if (is_array($this->_categories)) {
                    foreach ($this->_categories as $key => $std) {
                        if ($std->uid == $uid) {
                            unset($this->_categories[$key]);
                            $this->_session->setTmpCategories($this->_categories);
                        }
                    }
                }
                break;

            case self::BRAND :
                if (is_array($this->_brands)) {
                    foreach ($this->_brands as $key => $std) {
                        if ($std->uid == $uid) {
                            unset($this->_brands[$key]);
                            $this->_session->setTmpBrands($this->_brands);
                        }
                    }
                }
                break;

            case self::BLOG_BACKGROUND :
                $this->_session->unsTmpBlogBackground();
                break;

            case self::BLOG_POST:
                $this->_session->unsTmpBlogPost();
                break;

            default:
                throw new Exception(__METHOD__ . ' Unknown type "' . $type . '"');
                break;
        }

        return $this;
    }

    public function edit($type, $uid, $newId)
    {
        switch ($type) {

            case self::CATEGORY :

                if (is_array($this->_categories)) {
                    foreach ($this->_categories as $key => $std) {
                        if ($std->uid == $uid) {
                            $this->_categories[$key]->id = $newId;
                            //$this->_session->setTmpCategories($this->_categories);
                        }
                    }
                }
                break;

            case self::BRAND :

                if (is_array($this->_brands)) {
                    foreach ($this->_brands as $key => $std) {
                        if ($std->uid == $uid) {
                            $this->_brands[$key]->id = $newId;
                            //$this->_session->setTmpBrands($this->_brands);   
                        }
                    }
                }
                break;

            default:
                throw new Exception(__METHOD__ . ' Unknown type "' . $type . '"');
                break;
        }

        return $this;
    }

    public function getBackground($urlOnly = true)
    {
        if ($urlOnly) {
            if (is_object($this->_background)) {
                return $this->_background->url;
            }
            
            return '';
        }
        return $this->_background;
    }

    public function getBlogBackground($urlOnly = true)
    {
        if ($urlOnly)
            return $this->_blog_background->url;
        return $this->_blog_background;
    }

    public function getProduct()
    {
        if ($this->_product)
            return new Product_Model_Product($this->_product);
        return ($this->_product) ? $this->_product : new Product_Model_Product();
    }

    public function getBlogPost()
    {
        if ($this->_blog_post) {
            $st = new Application_Model_Static();
            $bp = $st->getBlogPosts(null, $this->_blog_post);

            return isset($bp[0]['post_name']) ? $bp[0]['post_name'] : '';
        }
        return '';
    }

    public function getCategories()
    {

        if (is_array($this->_categories)) {
            $categories = new Category_Model_Category_Collection();
            foreach ($this->_categories as $std) {
                $std->uid = ($std->uid) ? $std->uid : uniqid();
                $category = new Category_Model_Category($std->id);
                $category->setTmpUrl($std->url)
                        ->setTmpFileName($std->fileName)
                        ->setTmpUid($std->uid);
                $categories->addItem($category);
            }

            return $categories;
        }

        return ($this->_categories) ? $this->_categories : new Category_Model_Category_Collection();
    }

    public function getBrands()
    {
        if (is_array($this->_brands)) {
            $brands = new Application_Model_Manufacturer_Collection();
            foreach ($this->_brands as $std) {
                $std->uid = ($std->uid) ? $std->uid : uniqid();
                $brand    = new Application_Model_Manufacturer($std->id);
                $brand->setTmpUrl($std->url)
                        ->setTmpFileName($std->fileName)
                        ->setTmpUid($std->uid);
                $brands->addItem($brand);
            }
            return $brands;
        }

        return ($this->_brands) ? $this->_brands : new Application_Model_Manufacturer_Collection();
    }

    public function saveInDB($rewrite = true)
    {
        try {
            $categories = $this->getCategories();
            $brands     = $this->getBrands();
            $product    = $this->getProduct();
            $blogPost   = $this->getBlogPost();
            $bg         = new Product_Model_HomeBackground();
            $blg        = new Application_Model_BlogBackground();

            if ($rewrite) {
                $categories->releaseAllFixedCategories();
                $brands->releaseAllFixedBrands();
                $product->removeFromHomePage();
            }

            foreach ($categories as $category) {
                $category->fixAtHomepage();
            }

            foreach ($brands as $brand) {
                $brand->fixAtHomepage();
            }


            $product->fixProductAtHome();

            $d = new Application_Model_Blog_Backend();
            $d->addBlogPost($blogPost);

            $bg->change($this->getBackground(false)->fileName);
            $blg->change($this->getBlogBackground(false)->fileName);
        } catch (Exception $e) {
            return false;
        }


        return true;
    }

}