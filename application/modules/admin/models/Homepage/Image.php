<?php

class Admin_Model_Homepage_Image extends Application_Model_Abstract
{

    protected $_imageDir     = '/public/upload/homepage/';
    protected $_imageUri     = 'upload/homepage/';
    protected $_backendClass = null;

    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->_imageDir = realpath(APPLICATION_PATH . $this->_imageDir) . DIRECTORY_SEPARATOR;
    }

    public function change($imageName)
    {
        $this->_getbackend()->changeBackground($imageName, $this);
        return $this->getInsertedImageUrl();
    }

    public function remove()
    {
        $this->_getbackend()->removeBackground();
    }

    public static function url($imgName)
    {
        $obj = new self();
        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
        $url = $cloudFrontUrl . '/homepage/' . $imgName;

        return $url;
    }

    protected function getInsertedImageUrl()
    {
        if ($url = $this->getInsertedImage()) {
            $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
            $url = $cloudFrontUrl . '/homepage/' . $url;
        }

        return $url;
    }

    public function save()
    {
        
    }

}
