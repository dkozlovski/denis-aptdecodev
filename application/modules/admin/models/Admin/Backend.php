<?php

class Admin_Model_Admin_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'admins';

	public function getByEmail($user, $email)
	{
		$dbh = Application_Model_DbFactory::getFactory()->getConnection();

		$sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `email` = :email";
		$stmt = $dbh->prepare($sql);
		$stmt->bindParam(':email', $email);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row) {
			$user->setData($row);
		}
	}
	
	protected function _update(\Application_Model_Abstract $user)
	{
		$sql = "UPDATE `" . $this->_getTable() . "` SET
    `email` = :email,
    `full_name` = :full_name,
    `password` = :password,
    `sex` = :sex,
    `date_of_birth` = :date_of_birth,
	`rating` = :rating,
	`details` = :details
     WHERE `id` = :id";
		
		$id = $user->getId();
		$email = $user->getEmail();
		$fullName = $user->getFullName();
		$password = $user->getPassword();
		$sex = $user->getSex();
		$dateOfBirth = $user->getDateOfBirth();
		$rating = $user->getRating();
		$details = $user->getDetails();


		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':full_name', $fullName);
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':sex', $sex);
		$stmt->bindParam(':date_of_birth', $dateOfBirth);
		$stmt->bindParam(':rating', $rating);
		$stmt->bindParam(':details', $details);
		$stmt->bindParam(':id', $id);

		$stmt->execute();
	}

	protected function _insert(\Application_Model_Abstract $user)
	{
		$dbh = Application_Model_DbFactory::getFactory()->getConnection();

		$sql = "INSERT INTO `users`(`full_name`, `email`, `password`) values(:full_name, :email, :password);";
		$stmt = $dbh->prepare($sql);

		$fullName = $user->getFullName();
		$email = $user->getEmail();
		$password = $user->getPassword();

		$stmt->bindParam(':full_name', $fullName);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':password', $password);

		$stmt->execute();
		$user->setId($dbh->lastInsertId());
	}

}
