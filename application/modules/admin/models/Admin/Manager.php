<?php

class Admin_Model_Admin_Manager extends Zend_Db_Table_Abstract
{

    protected $_name = "admins";
    protected $_row;

    public function __construct($id = null)
    {
        parent::__construct();
        if ($id === null) {
            $this->_row = $this->createRow();
        } else {
            $this->_row = $this->find($id)->current();
        }
    }

    public function __set($name, $val)
    {
        if (isset($this->_row->$name)) {
            $this->_row->$name = $val;
        }
    }

    public function __get($name)
    {
        if (isset($this->_row->$name)) {
            return $this->_row->$name;
        }
    }

    public function fill($data)
    {
        foreach ($data as $key => $value) {
            if (isset($this->_row->$key)) {
                $this->_row->$key = $value;
            }
        }
    }

    public function save()
    {
        $this->_row->save();
    }

    public function del()
    {
        $this->_row->delete();
    }

    public function getAllAdmins()
    {
        $select = $this->getAdapter()->select();
        $select->from($this->_name)
                ->join('admin_roles', 'admin_roles.id = admins.role_id', 'name')
                ->where('admin_roles.name != ?', 'Super Admin');
        $stmt   = $this->getAdapter()->query($select);
        return $stmt->fetchAll();
    }

    public function populate()
    {
        return $this->_row->toArray();
    }

}