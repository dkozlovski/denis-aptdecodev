<?php

class Admin_Model_Admin extends Application_Model_Abstract implements Zend_Acl_Role_Interface
{

	protected $_backendClass = 'Admin_Model_Admin_Backend';
	protected $_invitation;
	protected $_wishlist;
    protected $_messagebox;
    protected $_emailCollection;
    protected $_phoneCollection;

	public function __construct($id = null)
	{
		if ($id) 
        {
			$this->getById($id);
		}


	}

	public function authenticate($loginData)
	{
		if (!isset($loginData['email'], $loginData['password'])) {
			throw new Ikantam_Exception_Auth('Email and Password Required');
		}

		$this->getByEmail($loginData['email']);

		if (!$this->isPasswordValid($loginData['password'])) {
			throw new Ikantam_Exception_Auth('The email address or the password that you entered does not match our records.');
		}

		return $this;
	}

	public function register($signupData)
	{
		try {
			if (!$this->getInvitation()->isValid($signupData)) {
				throw new Ikantam_Exception_Signup('Invalid Invitation Code');
			}
			$this->setData($signupData)
					->setPassword($this->hash($this->getPassword()))
					->save();
			$this->getInvitation()->setIsUsed(true)->save();

			return $this;
		} catch (PDOException $exc) {
			if ($exc->getCode() == "23000") {
				throw new Ikantam_Exception_Signup('This email is already taken');
			}
		}

		return $this;
	}

	//@TODO move somewhere
	protected function isPasswordValid($password)
	{
		$bcrypt = new Ikantam_Crypt_Password_Bcrypt();

		return $bcrypt->verify($password, $this->getPassword());
	}

	//@TODO move somewhere
	public function hash($password)
	{
		$bcrypt = new Ikantam_Crypt_Password_Bcrypt();

		return $bcrypt->create($password);
	}

	public function getByEmail($email)
	{
		$this->_getBackend()->getByEmail($this, $email);

		return $this;
	}
    
    //@TODO: Добавить в базу таблицу с группами(ролями). Связать с админами (admins_groups)
    public function getRoleId() {
        //Пока так.
        $roles = array(
            'v.o.v.a@tut.by' => 'Super_Admin',
            'support@aptdeco.com' => 'Super_Admin',
            'kalam8@gmail.com' => 'Super_Admin',
            'product.manager@aptdeco.com' => 'Product_Manager',
            'demo@ikantam.com' => 'Super_Admin',
        );
       // return 'Product_Manager';
        return isset($roles[$this->getEmail()]) ? $roles[$this->getEmail()] : 'Guest';
    }


}
