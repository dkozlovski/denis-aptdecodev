<?php

class Admin_Form_Login extends Ikantam_Form
{

	const ERROR_EMAIL_IS_EMPTY = 'Email is required.';
	const ERROR_EMAIL_IS_INVALID = 'Please enter a valid email address.';
	const ERROR_PASSWORD_IS_EMPTY = 'Password is required.';

	public function init()
	{
		$email = new Zend_Form_Element_Text('email');

		$notEmpty = new Zend_Validate_NotEmpty();
		$notEmpty->setMessage(self::ERROR_EMAIL_IS_EMPTY);

		$emailAddress = new Zend_Validate_EmailAddress();
		$emailAddress->setMessage(self::ERROR_EMAIL_IS_INVALID);

		$email->setRequired(true)
				->addFilter('StringTrim')
				->addValidator($notEmpty, true)
				->addValidator($emailAddress, true);

		$password = new Zend_Form_Element_Password('password');

		$password->setRequired(true)
				->addFilter('StringTrim')
				->addValidator('NotEmpty')
				->addErrorMessage(self::ERROR_PASSWORD_IS_EMPTY);

		$this->addElements(array($email, $password));
	}

}