<?php

class Admin_Form_EmailNotificationManagePrice extends Ikantam_Form
{
    const ERROR_TEXT_IS_EMPTY    = 'Text required.';
    const ERROR_SUBJECT_REQUIRED = 'Subject required.';

    public function init()
    {
        $this->setMethod('post');

        $text = new Zend_Form_Element_Textarea('text');

        $text->setRequired(true)
            ->setLabel('Message body')
            ->setAttrib('style', 'width: 600px;')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addErrorMessage(self::ERROR_TEXT_IS_EMPTY);

        $subject = new Zend_Form_Element_Text('subject');
        $subject->setRequired(true)
            ->setLabel('Email Subject')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addErrorMessage(self::ERROR_SUBJECT_REQUIRED);



        $this->addElements(array($subject, $text));

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');
        $this->addElement($submit);
    }
} 