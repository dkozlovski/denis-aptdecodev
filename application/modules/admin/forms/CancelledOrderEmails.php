<?php

class Admin_Form_CancelledOrderEmails extends Ikantam_Form
{
    const ERROR_TEXT_IS_EMPTY    = 'Text required.';
    const ERROR_SUBJECT_REQUIRED = 'Subject required.';

    protected  $_prefixName = 'abandoned_cart';

    public function init()
    {
        $this->setMethod('post');

        $subjectToBuyer = new Zend_Form_Element_Text('cancelled_order_email_to_buyer__subject');
        $subjectToBuyer->setRequired(true)
            ->setLabel('Email Subject (to buyer)')
            ->addFilter('StringTrim')
            ->addErrorMessage(self::ERROR_SUBJECT_REQUIRED);

        $textToBuyer = new Zend_Form_Element_Textarea('cancelled_order_email_to_buyer__text');
        $textToBuyer->setRequired(true)
            ->setLabel('Text (to buyer)')
            ->setAttrib('style', 'width: 600px;')
            ->addFilter('StringTrim')
            ->addErrorMessage(self::ERROR_TEXT_IS_EMPTY);

        $subjectToSeller = new Zend_Form_Element_Text('cancelled_order_email_to_seller__subject');
        $subjectToSeller->setRequired(true)
            ->setLabel('Email Subject (to seller)')
            ->addFilter('StringTrim')
            ->addErrorMessage(self::ERROR_SUBJECT_REQUIRED);

        $textToSeller = new Zend_Form_Element_Textarea('cancelled_order_email_to_seller__text');
        $textToSeller->setRequired(true)
            ->setLabel('Text (to seller)')
            ->setAttrib('style', 'width: 600px;')
            ->addFilter('StringTrim')
            ->addErrorMessage(self::ERROR_TEXT_IS_EMPTY);


        /* add separator line */

        $separator =  new Zend_Form_Element_Hidden('plaintext', array(
                'description' => '<div class="apt-form-action"></div>',
                'ignore' => true,
                'decorators' => array(
                    array('Description', array('escape' => false, 'tag' => '')),
                ),
            ));

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');
        $this->addElements(array($subjectToBuyer, $textToBuyer, $separator, $subjectToSeller, $textToSeller, $submit));

    }

}