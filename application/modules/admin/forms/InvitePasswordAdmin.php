<?php


class Admin_Form_InvitePasswordAdmin extends Zend_Form
{

    const ERROR_PASSWORD_EMPTY              = 'Please enter password.';
    const ERROR_PASSWORD_CONFIRMATION_EMPTY = 'Please retype password.';
    const ERROR_PASSWORD_CONFIRMATION       = 'Passwords not match.';

    public function __construct()
    {

        $this->setName("InvitePasswordAdmin");
        parent::__construct();

        $password = new Zend_Form_Element_Password("password");
        $password->setLabel("Password")
                ->setRequired(true)
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addErrorMessage(self::ERROR_PASSWORD_EMPTY);

        $confirm            = new Zend_Form_Element_Password("confirm");
        $vConfirmationEmpty = new Zend_Validate_NotEmpty();
        $vConfirmationEmpty->setMessage(self::ERROR_PASSWORD_CONFIRMATION_EMPTY);
        $identical          = new Zend_Validate_Identical('password');
        $identical->setMessage(self::ERROR_PASSWORD_CONFIRMATION);
        $confirm->setLabel("Confirm Password:")
                ->setRequired()
                ->addValidator($vConfirmationEmpty)
                ->addValidator($identical);

        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setLabel("Confirm");
        $this->addElements(array($password, $confirm, $submit));
    }

}

