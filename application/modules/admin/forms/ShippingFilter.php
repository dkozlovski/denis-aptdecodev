<?php

class Admin_Form_ShippingFilter extends Zend_Form{
    
    public function init() {
        
        $this->addDecorator('FormElements')
                ->addDecorator('Form');

        $this->setMethod('GET');

        $this->addElement($this->_getStartDateElement());
        $this->addElement($this->_getEndDateElement());
        $this->addElement($this->_getSubmitElement());
        
    }
      
    protected function _getStartDateElement()
    {
        $statusElement = new Zend_Form_Element_Text('start_date');

        $statusElement->setRequired(false)
                ->setLabel("Select dates:")
                ->setAttrib('id', 'datepicker4')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;')
                ->addFilters(array('StripNewlines', 'StripTags', 'StringTrim', 'Null'))
                ->setDescription('&mdash;');

        $statusElement->setDecorators(array(
            'ViewHelper',
            'label',
            array('Description', array('tag'    => 'span', 'class'  => 'mdash', 'escape' => false))
        ));

        return $statusElement;
    }

    protected function _getEndDateElement()
    {
        $statusElement = new Zend_Form_Element_Text('end_date');

        $statusElement->setRequired(false)
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;')
                ->addFilters(array('StripNewlines', 'StripTags', 'StringTrim', 'Null'))
                ->setAttrib('id', 'datepicker5');

        $statusElement->setDecorators(array(
            'ViewHelper'
        ));

        return $statusElement;
    }
    
       protected function _getSubmitElement()
    {
        $submitElement = new Zend_Form_Element_Button('submit');
        
        $submitElement->setLabel('Search')
                ->setIgnore(true)
                ->setRequired(false)
                ->setAttrib('class', 'apt-btn def')
                ->setAttrib('id', 'shippnig-filter');
        $submitElement->setDecorators(array('ViewHelper'));
        return $submitElement;
    }
    
}

