<?php

class Admin_Form_EditSmsText extends Zend_Form
{
    public function __construct() {
        $this->setName('editSmsText');
        parent::__construct();
    
        $smsText = new Zend_Form_Element_Textarea('text');
        $smsText->setLabel('Sms text:')
                ->setAttrib('id', 'smsText')
                ->setAttrib('cols', '70')
                ->setAttrib('rows', '10');
        $button = new Zend_Form_Element_Submit('submit');
        $button->setLabel('save');
        
        $this->addElements(array($smsText, $button));
    }
}

