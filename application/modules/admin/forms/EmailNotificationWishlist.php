<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 18.06.14
 * Time: 15:32
 */

class Admin_Form_EmailNotificationWishlist extends Ikantam_Form
{
    const ERROR_TEXT_IS_EMPTY    = 'Text required.';
    const ERROR_SUBJECT_REQUIRED = 'Subject required.';

    public function init()
    {
        $this->setMethod('post');

        $text = new Zend_Form_Element_Textarea('text');

        $text->setRequired(true)
            ->setLabel('Message body')
            ->setAttrib('style', 'width: 600px;')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addErrorMessage(self::ERROR_TEXT_IS_EMPTY);

        $subject = new Zend_Form_Element_Text('subject');
        $subject->setRequired(true)
            ->setLabel('Email Subject')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addErrorMessage(self::ERROR_SUBJECT_REQUIRED);

        $saleWindowText = new Zend_Form_Element_Textarea('sw_text');

        $saleWindowText->setRequired(true)
            ->setLabel('Message body')
            ->setAttrib('style', 'width: 600px;')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addErrorMessage(self::ERROR_TEXT_IS_EMPTY);

        $saleWindowSubject = new Zend_Form_Element_Text('sw_subject');
        $saleWindowSubject->setRequired(true)
            ->setLabel('Email Subject')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addErrorMessage(self::ERROR_SUBJECT_REQUIRED);

        $saleWindowSubject->addDecorator('Callback', array('placement' => 'prepend','callback' => function(){
                    return '<h2>Email notification for users who added product to their wishlist. When seller lowered the price.</h2>';
                }));


        $this->addElements(array($subject, $text, $saleWindowSubject, $saleWindowText));

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');
        $this->addElement($submit);
    }
} 