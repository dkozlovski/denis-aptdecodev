<?php

class Admin_Form_Optimization extends Zend_Form
{
    public function init() {
        
        $this->setName("optimization");
        $this->addElement($this->_jsSetDir());
        $this->addElement($this->_jsNewSetDir());
        $this->addElement($this->_cssSetDir());
        $this->addElement($this->_cssNewSetDir());
        $this->addElement($this->_pngSetDir());
        $this->addElement($this->_pngNewSetDir());
        $this->addElement($this->_submit());
        
    }
    
    protected function _jsSetDir() {
        
        $element = new Zend_Form_Element_Text("js_dir");
        $element->setLabel("JS Folder:")
                ->setValue("js");
        return $element;
        
    }
    
    protected function _jsNewSetDir() {
        
        $element = new Zend_Form_Element_Text("js_new_dir");
        $element->setLabel("JS New Folder:");
        return $element;
        
    }
    
     protected function _cssSetDir() {
        
        $element = new Zend_Form_Element_Text("css_dir");
        $element->setLabel("CSS Folder:")
                ->setValue("css");
        return $element;
        
    }
    
    protected function _cssNewSetDir() {
        
        $element = new Zend_Form_Element_Text("css_new_dir");
        $element->setLabel("CSS New Folder:");
        return $element;
        
    }
    
      protected function _pngSetDir() {
        
        $element = new Zend_Form_Element_Text("png_dir");
        $element->setLabel("PNG Folder:")
                ->setValue("png");
        return $element;
        
    }
    
    protected function _pngNewSetDir() {
        
        $element = new Zend_Form_Element_Text("png_new_dir");
        $element->setLabel("PNG New Folder:");
        return $element;
        
    }
    
    protected function _submit(){
        
        $elemet = new Zend_Form_Element_Submit("submit");
        $elemet->setLabel("Optimization");
        return $elemet;
    }
}

