<?php

class Admin_Form_OrderItemAvailability extends Ikantam_Form
{

    protected $_dataRequired = false;
    protected $_times = array(
        '7-12'  => '7AM - 12PM',
        '12-16' => '12PM - 4PM',
        '16-20' => '4PM - 8PM'
    );

    public function init()
    {
        $this->addElement($this->_getIsProductAvailableElement())
            ->addElement($this->_getPickUpDateElement())
            ->addElement($this->_getSetPickupDateElement())
            ->addElement($this->_getSetPickupTimeElement())
            ->addElement($this->_getSetDeliveryDateElement())
            ->addElement($this->_getSetDeliveryTimeElement())
            ->addElement($this->_getTelephoneNumberElement())
            ->addElement($this->_getBuildingTypeElement())
            ->addElement($this->_getFlighsOfStairsElement())
            ->addElement($this->_getPaperworkElement())
            ->addElement($this->_getSubmitElement());
    }

    public function isValid($data)
    {
        if ($this->_dataRequired) {
            if (isset($data['is_product_available']) && $data['is_product_available'] === '0') {
                $this->setDataRequired(false);
            } else {
                if (isset($data['building_type']) && $data['building_type'] === 'elevator') {
                    $this->getElement('flights_of_stairs')->setRequired(false);
                }
            }
        }

        $isValid = parent::isValid($data);

        return $isValid;
    }

    public function setDataRequired($flag)
    {
        if ($flag) {
            $this->_dataRequired = true;
        } else {
            $this->removeElement('delivery_date');
            $this->removeElement('set_pickup_date');
            $this->removeElement('set_pickup_time');
            $this->removeElement('set_delivery_date');
            $this->removeElement('set_delivery_time');
            $this->removeElement('telephone_number');
            $this->removeElement('building_type');
            $this->removeElement('flights_of_stairs');
            $this->removeElement('paperwork');
        }
    }

    protected function _getIsProductAvailableElement()
    {
        $multiOptions = array(0 => 'No', 1 => 'Yes');

        $isProductAvailableElement = new Zend_Form_Element_Radio('is_product_available');

        $isProductAvailableElement->setRequired(true)
                ->addValidator('InArray', true, array(array_keys($multiOptions)))
                ->setMultiOptions($multiOptions)
                ->setLabel('Is this product available now?');

        $isProductAvailableElement->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'lis-pickd')),
            array('Label', array('placement' => 'prepend')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'item-vailability product-info')),
        ));


        $isProductAvailableElement->setSeparator('</li><li>');

        return $isProductAvailableElement;
    }

    protected function _getPickUpDateElement()
    {
        $availableDates = array();

        $deliveryDateElement = new Zend_Form_Element_Radio('delivery_date', array('escape' => false));

        $deliveryDateElement->setRequired(true)
                ->setMultiOptions($availableDates)
                ->addValidator('InArray', false, array(array_keys($availableDates)))
                ->setLabel('Pick up Date');

        $deliveryDateElement->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('ul' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'm-0 p-0')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'lis-pickd m-0 p-0', 'openOnly' => true)),
            array('Label', array('placement' => 'prepend')),
        ));

        $deliveryDateElement->setSeparator('</li><li>');

        return $deliveryDateElement;
    }


    protected function _getSetPickupDateElement()
    {
        $setPickupDate = new Zend_Form_Element_Text('set_pickup_date');

        $dateFormat = new Zend_Validate_Date(array('format' => 'd M, y'));
        $dateFormat->setLocale('en_US');


        $setPickupDate->setRequired(false)
            ->setLabel('Pickup:')
            ->addFilters(array('StripTags', 'StringTrim', 'Null'))
            ->addValidator($dateFormat, true)
            ->setAttrib('class', 'form-field w-113')
            ->setAttrib('readonly', 'readonly');

        $this->_setDecoratorForDateElement($setPickupDate);

        return $setPickupDate;
    }

    protected function _getSetPickupTimeElement()
    {
        $setPickupTime = new Zend_Form_Element_Select('set_pickup_time');
        $setPickupTime->setRequired(false)
            ->setAttrib('class', 'w-128')
            ->setMultiOptions($this->_times)
            ->setValue('12-16');

        $this->_setDecoratorForTimeElement($setPickupTime);
        return $setPickupTime;
    }


    protected function _getSetDeliveryDateElement()
    {
        $setDeliveryDate = new Zend_Form_Element_Text('set_delivery_date');

        $dateFormat = new Zend_Validate_Date(array('format' => 'd M, y'));
        $dateFormat->setLocale('en_US');

        $setDeliveryDate->setRequired(false)
            ->setLabel('Delivery:')
            ->addFilters(array('StripTags', 'StringTrim', 'Null'))
            ->addValidator($dateFormat, true)
            ->setAttrib('class', 'form-field w-113')
            ->setAttrib('readonly', 'readonly');
        $this->_setDecoratorForDateElement($setDeliveryDate);

        return $setDeliveryDate;
    }

    protected function _getSetDeliveryTimeElement()
    {
        $setDeliveryTime = new Zend_Form_Element_Select('set_delivery_time');

        $setDeliveryTime->setRequired(false)
            ->setAttrib('class', 'w-128')
            ->setMultiOptions($this->_times)
            ->setValue('16-20');

        $this->_setDecoratorForTimeElement($setDeliveryTime);
        return $setDeliveryTime;
    }

    protected function _getTelephoneNumberElement()
    {
        $telephoneNumberElement = new Zend_Form_Element('telephone_number');

        $telephoneNumberElement->setRequired(true)
                ->addFilter('StringTrim')
                ->addValidator('Regex', false, array('/^\(?\+?\d\d\d\)?\s?\d\d\d\s?\d\d\d\d$/u'))
                ->addErrorMessage('Please enter correct phone number.')
                ->setLabel('What is the best number we can reach you on to confirm delivery?')
                ->setAttrib('class', 'numb-format delivery-phone-number');

        $telephoneNumberElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'p', 'class' => 'dd-title clearfix')),
            array('Label', array('placement' => 'prepend')),
        ));

        return $telephoneNumberElement;
    }

    protected function _getBuildingTypeElement()
    {
        $buildingTypes = array('elevator' => 'elevator', 'walkup'   => 'walkup');

        $buildingTypeElement = new Zend_Form_Element_Radio('building_type');

        $buildingTypeElement->setRequired(true)
                ->setMultiOptions($buildingTypes)
                ->addValidator('InArray', false, array(array_keys($buildingTypes)))
                ->setLabel('Building Type');

        $buildingTypeElement->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'lis-pickd')),
            array('Label', array('placement' => 'prepend')),
        ));

        $buildingTypeElement->setSeparator('</li><li>');

        return $buildingTypeElement;
    }

    protected function _getFlighsOfStairsElement()
    {
        $select         = array('' => '-- Select --');
        $flighsOfStairs = array(
            1  => 1,
            2  => 2,
            3  => 3,
            4  => 4,
            5  => 5,
            6  => 6,
            7  => 7,
            8  => 8,
            9  => 9,
            10 => 10,
        );

        $flighsOfStairsElement = new Zend_Form_Element_Select('flights_of_stairs');

        $flighsOfStairsElement->setRequired(true)
                ->setMultiOptions($select + $flighsOfStairs)
                ->addValidator('InArray', false, array(array_keys($flighsOfStairs)))
                ->setLabel('How many flights of stairs');

        $flighsOfStairsElement->setDecorators(array(
            'ViewHelper',
            array('Label', array('placement' => 'prepend')),
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'lis-pickd')),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'doc-req')),
        ));

        return $flighsOfStairsElement;
    }

    protected function _getPaperworkElement()
    {

        $paperworkElement = new Zend_Form_Element_Textarea('paperwork');

        $paperworkElement->setRequired(false)
                ->setLabel('Leave additional info here')
                ->setAttrib('cols', '25')
                ->setAttrib('rows', '3');

        $paperworkElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'p', 'class' => 'dd-title clearfix')),
            array('Label', array('placement' => 'prepend')),
        ));

        return $paperworkElement;
    }

    protected function _getSubmitElement()
    {
        $submitElement = new Zend_Form_Element_Submit('submit');

        $submitElement->setLabel('Save pick up details');

        return $submitElement;
    }

    protected function _setDecoratorForDateElement(\Zend_Form_Element_Text $element)
    {
        $decorators = array(
            'ViewHelper',
            array(array('col-form-group' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'form-group col-form-group', 'openOnly' => true)),
            array(array('group-side' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'col-form-group-side', 'openOnly' => true)),
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group', 'openOnly' => true)),
            array('Description', array('tag'   => 'span', 'class' => 'dark-orange', 'title' => 'Borough', 'placement' => 'prepend')),
            array('Label', array('placement' => 'prepend')),
        );
        if ($element->getName() == 'set_pickup_date') {
            $decorators[] = array(array('container' => 'HtmlTag'), array('tag'   => 'div', 'id' => 'container-other', 'style' => 'display: none;', 'openOnly' => true));
        }
        $element->setDecorators($decorators);
    }

    protected function _setDecoratorForTimeElement(\Zend_Form_Element_Select $element)
    {
        $decorators = array(
            'ViewHelper',
            array(array('col-form-group' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'form-group col-form-group', 'closeOnly' => true)),
            array(array('group-side' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'col-form-group-side', 'closeOnly' => true)),
            array(array('row' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group', 'closeOnly' => true)),
            array('Label', array('placement' => 'prepend'))
        );

        if ($element->getName() == 'set_delivery_time') {
            $decorators[] = array(array('container' => 'HtmlTag'), array('tag'   => 'div', 'closeOnly' => true));
            $decorators[] = array(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'control-group', 'closeOnly' => true));
        }

        $element->setDecorators($decorators);
    }


}
