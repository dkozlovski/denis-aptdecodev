<?php

class Admin_Form_InboxPage extends Ikantam_Form
{
    const COUNT_ELEMENTS = 6;
    protected  $_prefixName = 'inbox_page';

    public function init()
    {
        $this->setMethod('post');

        for ($i = 1; $i <= self::COUNT_ELEMENTS ; $i++) {

            $subject = new Zend_Form_Element_Text($this->_prefixName . $i . '__subject');
            $subject->setLabel('Subject (' . $i . ')')
                ->addFilter('StringTrim');

            $text = new Zend_Form_Element_Textarea($this->_prefixName . $i . '__text');
            $text->setLabel('Text (' . $i . ')')
                ->setAttrib('style', 'width: 600px;')
                ->addFilter('StringTrim');


            $this->addElements(array($subject, $text));

            /* add separator line */
            if ($i < self::COUNT_ELEMENTS) {
                $this->addElement('hidden', 'plaintext' . $i, array(
                    'description' => '<div class="apt-form-action"></div>',
                    'ignore' => true,
                    'decorators' => array(
                        array('Description', array('escape' => false, 'tag' => '')),
                    ),
                ));
            }
        }

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');
        $this->addElement($submit);
    }

    public function getPrefixName()
    {
        return $this->_prefixName;
    }

}