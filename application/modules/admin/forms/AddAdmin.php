<?php

class Admin_Form_AddAdmin extends Zend_Form
{

    const ERROR_PASSWORD_EMPTY              = 'Please enter password.';
    const ERROR_FULLNAME_EMPTY              = 'Please enter full name.';
    const ERROR_PASSWORD_CONFIRMATION_EMPTY = 'Please retype password.';
    const ERROR_PASSWORD_CONFIRMATION       = 'Passwords not match.';
    const ERROR_FULLNAME_LENGTH             = 'Full name should be under 100 characters.';

    public function __construct()
    {

        $this->setName("AddAdmins");
        parent::__construct();

        $email = new Zend_Form_Element_Text("email");
        $email->setLabel("Email:")
                ->setRequired(true)
                ->addValidator('Db_NoRecordExists', false, array(
                    'table' => 'admins',
                    'field' => 'email'
                ))
                ->addValidator('notEmpty')
                ->addValidator('EmailAddress')
                ->addValidator('StringLength', array('max' => 100));

        $role  = new Zend_Form_Element_Select("role_id");
        $role->setLabel("Role");
        $roles = new Admin_Model_Roles_Backend();
        $rows  = $roles->getRoles();
        foreach ($rows as $row) {
            if ($row["name"] != "Super Admin") {
                $role->addMultiOption($row["id"], $row["name"]);
            }
        }
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setLabel("Create");
        $this->addElements(array($email, $role, $submit));
    }

}