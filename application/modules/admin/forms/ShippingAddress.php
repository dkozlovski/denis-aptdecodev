<?php

class Admin_Form_ShippingAddress extends User_Form_ShippingAddress
{


    public function init()
    {
        parent::init();
        $this->setAction(Ikantam_Url::getUrl('admin/users/save-address'));

        $userIdElement = new Zend_Form_Element_Hidden('user_id');
        $userIdElement->setAttrib('name', 'shipping_address[user_id]')
            ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Int', 'Null'))
            ->setRequired(false);
        $userIdElement->setDecorators(array('ViewHelper'));
        $this->addElement($userIdElement);
    }
}