<?php

class Admin_Form_ProductFilter extends Zend_Form
{

    public function init()
    {
        $this->addDecorator('FormElements')
                ->addDecorator('Form');

        $this->setMethod('GET');

        $this->addElement($this->_getSearchElement());
        $this->addElement($this->_getStatusElement());
        $this->addElement($this->_getCategoryElement());
        $this->addElements($this->_getPeriodElements());
        $this->addElement($this->_getStartDateElement());
        $this->addElement($this->_getEndDateElement());
        $this->addElement($this->_getSubmitElement());
    }
    
   

    protected function _getSearchElement()
    {
        $statusElement = new Zend_Form_Element_Text('search');

        $statusElement->setRequired(false)
                ->addFilters(array('StripNewlines', 'StripTags', 'StringTrim', 'Null'))
                ->setAttrib('autocomplete', 'off')
                ->setAttrib('id', 'filter-items')
                ->setDescription('&nbsp;');

        $statusElement->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag'   => 'i', 'class' => 'icon-search')),
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'id'    => 'filter-my-items', 'class' => 'input-append'))
        ));

        return $statusElement;
    }

    protected function _getStatusElement()
    {
        $statuses = array(
            'all'           => 'All Statuses',
            'not-processed' => 'Not Processed',
            'approved'      => 'Approved',
            'rejected'      => 'Rejected',
            'in-progress'   => 'In Delivery',
            'received'      => 'Received',
            'published'     => 'Published',
            'unpublished'   => 'Unpublished',
            'sold'          => 'Sold',
            'notsold'       => 'Not Sold',
        );

        $statusElement = new Zend_Form_Element_Select('status');

        $statusElement->setRequired(false)
                ->setMultiOptions($statuses)
                ->addValidator('InArray', false, array(array_keys($statuses)));

        $statusElement->setDecorators(array(
            'ViewHelper',
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'btn-group'))
        ));

        return $statusElement;
    }

    protected function _getCategoryElement()
    {
        $categoryCollection = new Category_Model_Category_Collection();

        $categories = $categoryCollection->getOptions2();

        $select = array('' => '-- Select category --');


        $statusElement = new Zend_Form_Element_Select('category_id');

        $statusElement->setRequired(false)
                ->setMultiOptions($select + $categories)
                ->addValidator('InArray', false, array(array_keys($categories)));

        $statusElement->setDecorators(array(
            'ViewHelper',
            array(array('wrap' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'btn-group'))
        ));

        return $statusElement;
    }

    protected function _getPeriodElements()
    {
        $periodAll = new Ikantam_Form_Element_Note('period_all');

        $periodAll->setRequired(false)
                ->setDescription('All');

        $periodAll->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'a', 'class' => 'show p-t7', 'href'      => 'javascript:void(0)', 'onclick' => 'setPeriod("all")')),
            array(array('li' => 'HtmlTag'), array('tag' => 'li')),
            array(array('ul' => 'HtmlTag'), array('tag'      => 'ul', 'openOnly' => true)),
            array(array('wrap' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'filter-myitems', 'openOnly' => true))
        ));
        
        $periodDay = new Ikantam_Form_Element_Note('period_day');

        $periodDay->setRequired(false)
                ->setDescription('Today');

        $periodDay->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'a', 'class' => 'show p-t7', 'href'      => 'javascript:void(0)', 'onclick' => 'setPeriod("day")')),
            array(array('li' => 'HtmlTag'), array('tag' => 'li'))
        ));
        
        $periodWeek = new Ikantam_Form_Element_Note('period_week');

        $periodWeek->setRequired(false)
                ->setDescription('Week');

        $periodWeek->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'a', 'class' => 'show p-t7', 'href'      => 'javascript:void(0)', 'onclick' => 'setPeriod("week")')),
            array(array('li' => 'HtmlTag'), array('tag' => 'li'))
        ));

        $periodMonth = new Ikantam_Form_Element_Note('period_month');

        $periodMonth->setRequired(false)
                ->setDescription('Month');

        $periodMonth->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'a', 'class' => 'show p-t7', 'href'      => 'javascript:void(0)', 'onclick' => 'setPeriod("month")')),
            array(array('li' => 'HtmlTag'), array('tag' => 'li'))
        ));

        $periodYear = new Ikantam_Form_Element_Note('period_year');

        $periodYear->setRequired(false)
                ->setDescription('Year');

        $periodYear->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag'       => 'a', 'class' => 'show p-t7', 'href'      => 'javascript:void(0)', 'onclick' => 'setPeriod("year")')),
            array(array('li' => 'HtmlTag'), array('tag' => 'li'))
        ));

        return array($periodAll, $periodDay, $periodWeek, $periodMonth, $periodYear);
    }

    protected function _getStartDateElement()
    {
        $statusElement = new Zend_Form_Element_Text('start_date');

        $statusElement->setRequired(false)
                ->setAttrib('id', 'datepicker4')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;')
                ->addFilters(array('StripNewlines', 'StripTags', 'StringTrim', 'Null'))
                ->setDescription('&mdash;');

        $statusElement->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag'    => 'span', 'class'  => 'mdash', 'escape' => false)),
            array(array('li' => 'HtmlTag'), array('tag'      => 'li', 'openOnly' => true)),
        ));

        return $statusElement;
    }

    protected function _getEndDateElement()
    {
        $statusElement = new Zend_Form_Element_Text('end_date');

        $statusElement->setRequired(false)
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;')
                ->addFilters(array('StripNewlines', 'StripTags', 'StringTrim', 'Null'))
                ->setAttrib('id', 'datepicker5');

        $statusElement->setDecorators(array(
            'ViewHelper',
            array(array('li' => 'HtmlTag'), array('tag'       => 'li', 'closeOnly' => true))
        ));

        return $statusElement;
    }
    
    
     protected function _getSubmitElement()
    {
        $submitElement = new Zend_Form_Element_Submit('submit');
        
        $submitElement->setLabel('Search')->setIgnore(true)->setRequired(false)->setAttrib('class', 'apt-btn def');
        
        $submitElement->setDecorators(array(
            'ViewHelper',
            array(array('li' => 'HtmlTag'), array('tag'       => 'li')),
            array(array('ul' => 'HtmlTag'), array('tag'       => 'ul', 'closeOnly' => true)),
            array(array('wrap' => 'HtmlTag'), array('tag'       => 'div', 'class'     => 'filter-myitems', 'closeOnly' => true))
        ));        
        
        return $submitElement;
    }

}
