<?php

class Admin_Form_CarrierEmail extends Ikantam_Form
{
    protected  $_prefixName = 'carrier_email';

    public function init()
    {

        $email = new Zend_Form_Element_Text($this->_prefixName  . '__email');
        $email->setRequired(true)
            ->setLabel('Email')
            ->addFilters(array('StripTags', 'StringTrim'))
            ->addValidator('notEmpty', true, array('messages' => 'Please enter an email'))
            ->addValidator('EmailAddress', true);

        $text = new Zend_Form_Element_Textarea($this->_prefixName . '__text');
        $text->setRequired(true)
            ->setLabel('Text')
            ->setAttrib('style', 'width: 600px;')
            ->addFilter('StringTrim');

        $subject = new Zend_Form_Element_Text($this->_prefixName . '__subject');
        $subject->setRequired(true)
            ->setLabel('Email Subject')
            ->addFilters(array('StripTags', 'StringTrim'));

        $plainDescription = '<div>
            <strong>Available variables:</strong>
            %product_title%, %order_id%, %seller_name%, %seller_email%,%pick_up_address%, %pick_up_date%, %pick_up_building_type%,
            %buyer_name%, %buyer_email%, %delivery_address%, %delivery_date%, %delivery_building_type%
            </div>';

        $hint = new Zend_Form_Element_Hidden('hint');
        $hint->setDescription($plainDescription)
            ->setIgnore(true)
            ->setDecorators(array(array('Description', array('escape' => false, 'tag' => ''))));

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');

        $this->addElements(array($email, $subject, $text, $hint, $submit));
    }
}