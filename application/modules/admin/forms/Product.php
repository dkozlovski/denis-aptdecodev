<?php

class Admin_Form_Product extends Ikantam_Form
{

    protected $_session;

    const ERROR_AGE_VALUE = 'Please, select age.';
    const ERROR_AVAILABLE_FROM_FORMAT = 'Please, enter correct available from date.';
    const ERROR_AVAILABLE_TILL_FORMAT = 'Please, enter correct available till date.';
    const ERROR_CATEGORY_VALUE = 'Please, select a category';
    const ERROR_SUBCATEGORY_VALUE = 'Please, select a subcategory';
    const ERROR_COLOR_VALUE = 'Please, select a color';
    const ERROR_CONDITION_VALUE = 'Please, select condition.';
    const ERROR_DEPTH_FORMAT = 'Please, enter correct depth.';
    const ERROR_DEPTH_VALUE = 'Depth should be greater than 0.';
    const ERROR_DESCRIPTION_LENGTH = 'Description should be less than 60000 characters.';
    const ERROR_HEIGHT_FORMAT = 'Please, enter correct height.';
    const ERROR_HEIGHT_VALUE = 'Height should be greater than 0.';
    const ERROR_MANUFACTURER_EMPTY = 'Manufacturer is required.';
    const ERROR_MANUFACTURER_LENGTH = 'Manufacturer should be less than 255 characters.';
    const ERROR_MATERIAL_VALUE = 'Please, select a material';
    const ERROR_ORIGINAL_PRICE_FORMAT = 'Please, enter correct original price.';
    const ERROR_ORIGINAL_PRICE_VALUE = 'Original price should be greater than 0.';
    const ERROR_PRICE_FORMAT = 'Please, enter correct price.';
    const ERROR_PRICE_VALUE = 'Price should be greater than 0.';
    const ERROR_SHIPPING_VALUE = 'Please, select shipping.';
    const ERROR_TITLE_EMPTY = 'Title is required.';
    const ERROR_TITLE_LENGTH = 'Title should be less than 255 characters.';
    const ERROR_WIDTH_FORMAT = 'Please, enter correct width.';
    const ERROR_WIDTH_VALUE = 'Width should be greater than 0.';

    public function __construct($options = null, $session = null)
    {
        $this->_session = $session;
        parent::__construct($options);
    }

    public function init()
    {
        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'product/form_decorator.phtml', 'session' => $this->_session)),
            'Form')
        );



        $this->setAttrib('id', 'submit-form')
                ->setAttrib('class', 'form-horizontal')
                ->setMethod('post')
                ->setAction(Ikantam_Url::getUrl('product/add/post'));

        $this->addElement($this->getTitleElement())
                ->addElement($this->getDescriptionElement())
                ->addElement($this->getAvailableFromElement())
                ->addElement($this->getAvailableTillElement())
                ->addElement($this->getPriceElement())
                ->addElement($this->getOriginalPriceElement())
                ->addElement($this->getConditionElement())
                ->addElement($this->getWidthElement())
                ->addElement($this->getHeightElement())
                ->addElement($this->getDepthElement())
                ->addElement($this->getAgeElement())
                ->addElement($this->getShippingElement())
                ->addElement($this->getMaterialElement())
                ->addElement($this->getColorElement())
                ->addElement($this->getCategoryElement())
                ->addElement($this->getSubCategoryElement())
                ->addElement($this->getManufacturerElement())
                ->addElement($this->getFullNameElement())
                ->addElement($this->getAddressLine1Element())
                ->addElement($this->getAddressLine2Element())
                ->addElement($this->getCityElement())
                ->addElement($this->getStateElement())
                ->addElement($this->getPostcodeElement())
                ->addElement($this->getPhoneNumberElement())
                ->addElement($this->getCountryCodeElement())


        //->addElement($this->getFilesElement())
        //->addElement($this->getDefaultImageElement());
        ;
    }

    public function getDefaultImageElement()
    {
        $files = new Zend_Form_Element_Hidden('default_image');
        return $files;
    }

    protected function getFilesElement()
    {
        $files = new Zend_Form_Element_Multiselect('files');

        $files->setRequired(false)
                ->setAttrib('name', 'files[]');

        return $files;
    }

    protected function getFullNameElement()
    {
        $fullName = new Zend_Form_Element_Text('pickup_full_name');
		$fullName->setRequired(true)
				->addFilters(array('StringTrim', 'StripTags'))
				->addValidator('NotEmpty')
                ->setAttrib('id', 'shipping-full-name')
				->addErrorMessage('Enter Full Name!')
                ->setDecorators(array(
                    'ViewHelper',
                    'Label',
                    'Errors'
        ));

        return $fullName;
    }

    protected function getAddressLine1Element()
    {
        $addressLine1 = new Zend_Form_Element_Text('pickup_address_line1');
		$addressLine1->setRequired(true)
				->addFilters(array('StringTrim', 'StripTags'))
				->addValidator('NotEmpty')
                ->setAttrib('id', 'address-line-1')
				->addErrorMessage('Enter Address')
                ->setDecorators(array(
                    'ViewHelper',
                    'Label',
                    'Errors'
        ));


        return $addressLine1;
    }

    protected function getAddressLine2Element()
    {
        $title = new Zend_Form_Element_Text('pickup_address_line2');
        $title->setRequired(false)->setAttrib('id', 'address-line-2')->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $title;
    }

    protected function getCityElement()
    {
        $title = new Zend_Form_Element_Text('pickup_city');
        $title->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
				->addValidator('NotEmpty')
				->addErrorMessage('Enter City!')
                ->setAttrib('id', 'shipping-city')->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $title;
    }

    protected function getStateElement()
    {
        $title = new Zend_Form_Element_Text('pickup_state');
        $title->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
				->addValidator('NotEmpty')
				->addErrorMessage('Enter state!')
                ->setAttrib('id', 'shipping-state')->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $title;
    }

    protected function getPostcodeElement()
    {
        $format = new Zend_Validate_PostCode(array('locale' => 'en_US'));
        
        $title = new Zend_Form_Element_Text('pickup_postcode');
        $title->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
				->addValidator('NotEmpty', true)	
				->addValidator($format, true)
				->addErrorMessage('Enter Postcode!')
                ->setAttrib('id', 'shipping-zip')->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $title;
    }
    
    protected function getPhoneNumberElement()
    {
        $title = new Zend_Form_Element_Text('pickup_phone');
        $title->setRequired(true)
                ->setAttrib('id', 'shipping-phone')
                ->setAttrib('placeholder', '(___) ___ ____')
                ->setAttrib('class', 'numb-format verify-ur-phone')
                ->addFilters(array('StringTrim', 'StripTags'))
				->addValidator('NotEmpty', true)	
				->addErrorMessage('Enter Phone Number!')
                ->setDecorators(array(
                    'ViewHelper',
                    'Label',
                    'Errors'
        ));

        return $title;
    }

    protected function getCountryCodeElement()
    {
        $options = array(0 => '-- Select Country --', 'US' => 'United States', 'CA' => 'Canada');

        $titleEmpty = new Zend_Validate_InArray(array('US', 'CA'));
        
        
        $title = new Zend_Form_Element_Select('pickup_country_code');
        $title->setRequired(true)
                ->addValidator($titleEmpty, true)
                ->setAttrib('id', 'source5')
                ->setAttrib('class', 'select-styled')
                ->setMultiOptions($options)
                ->setDecorators(array(
                    'ViewHelper',
                    'Label',
                    'Errors'
                ));

        return $title;
    }

    protected function getTitleElement()
    {
        $title = new Zend_Form_Element_Text('title');

        $titleEmpty = new Zend_Validate_NotEmpty();
        $titleEmpty->setMessage(self::ERROR_TITLE_EMPTY);

        $titleLength = new Zend_Validate_StringLength(array('max' => 255));
        $titleLength->setMessage(self::ERROR_TITLE_LENGTH);

        $title->setRequired(true)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($titleEmpty, true)
                ->addValidator($titleLength, true)
                ->setAttrib('id', 'product_title')
                ->setAttrib('placeholder', '')
                ->setAttrib('maxlength', 255);

        $title->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $title;
    }

    protected function getDescriptionElement()
    {
        $description = new Zend_Form_Element_Textarea('description');

        $descriptionLength = new Zend_Validate_StringLength(array('max' => 60000));
        $descriptionLength->setMessage(self::ERROR_DESCRIPTION_LENGTH);

        $description->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($descriptionLength, true)
                ->setAttrib('rows', 4)
                ->setAttrib('cols', 80)
                ->setAttrib('maxlength', 20000);

        $description->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $description;
    }

    protected function getAvailableFromElement()
    {
        $availableFrom = new Zend_Form_Element_Text('available_from');

        $dateFormat = new Zend_Validate_Date(array('format' => 'M dd, y'));
        $dateFormat->setLocale('en_US');

        $availableFrom->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($dateFormat, true)
                ->addErrorMessage(self::ERROR_AVAILABLE_FROM_FORMAT)
                ->setAttrib('id', 'datepicker')
                ->setAttrib('class', 'datepicker2')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;');

        $availableFrom->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $availableFrom;
    }

    protected function getAvailableTillElement()
    {
        $availableTill = new Zend_Form_Element_Text('available_till');

        $dateFormat = new Zend_Validate_Date(array('format' => 'M dd, y'));
        $dateFormat->setLocale('en_US');

        $availableTill->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($dateFormat, true)
                ->addErrorMessage(self::ERROR_AVAILABLE_TILL_FORMAT)
                ->setAttrib('id', 'datepicker2')
                ->setAttrib('class', 'datepicker2')
                ->setAttrib('readonly', 'readonly')
                ->setAttrib('style', 'background:white;');

        $availableTill->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));
        return $availableTill;
    }

    protected function getPriceElement()
    {
        $price = new Zend_Form_Element_Text('price');

        $priceFormat = new Zend_Validate_Float('en_US');
        $priceFormat->setMessage(self::ERROR_PRICE_FORMAT);

        $priceValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $priceValue->setMessage(self::ERROR_PRICE_VALUE);

        $price->setRequired(true)
                ->addFilter('StringTrim')
                ->addValidator($priceFormat, true)
                ->addValidator($priceValue, true)
                ->setAttrib('placeholder', '')
                ->setAttrib('autocomplete', 'off');

        $price->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $price;
    }

    protected function getOriginalPriceElement()
    {
        $originalPrice = new Zend_Form_Element_Text('original_price');

        $priceFormat = new Zend_Validate_Float('en_US');
        $priceFormat->setMessage(self::ERROR_PRICE_FORMAT);

        $priceValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $priceValue->setMessage(self::ERROR_ORIGINAL_PRICE_VALUE);

        $originalPrice->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator($priceFormat, true)
                ->addValidator($priceValue, true)
                ->addErrorMessage(self::ERROR_ORIGINAL_PRICE_FORMAT)
                ->setAttrib('placeholder', '')
                ->setAttrib('id', 'original-price');

        $originalPrice->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $originalPrice;
    }
    
    protected function getConditionElement()
    {
        $currentCondition = new Zend_Form_Element_Radio('condition');

        $values = array('new'/*, 'like-new'*/, 'good', 'satisfactory', 'age-worn');

        $currentCondition->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_CONDITION_VALUE)
                ->setMultiOptions(array(
                    'new' => 'New: Product hasn\'t been unwrapped from box',
                    //'like-new' => 'Like New: Product has been unwrapped from box but looks new & doesn\'t have any scratches, blemishes, etc.',
                    'good' => 'Good: Minor blemishes that most people won\'t notice',
                    'satisfactory' => 'Satisfactory: Moderate wear and tear, but still has many good years left',
                    'age-worn' => 'Age-worn: Has lived a full life and has a "distressed" look with noticeable wear'));

        return $currentCondition;
    }

    protected function getWidthElement()
    {
        $width = new Zend_Form_Element_Text('width');

        $float = new Zend_Validate_Float('en_US');

        $widthValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $widthValue->setMessage(self::ERROR_WIDTH_VALUE);

        $width->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator($float, true)
                ->addValidator($widthValue, true)
                ->addErrorMessage(self::ERROR_WIDTH_FORMAT)
                ->setAttrib('class', 'dimensions');

        $width->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $width;
    }

    protected function getHeightElement()
    {
        $height = new Zend_Form_Element_Text('height');

        $float = new Zend_Validate_Float('en_US');

        $heightValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $heightValue->setMessage(self::ERROR_HEIGHT_VALUE);

        $height->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator($float, true)
                ->addValidator($heightValue, true)
                ->addErrorMessage(self::ERROR_HEIGHT_FORMAT)
                ->setAttrib('class', 'dimensions');

        $height->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $height;
    }

    protected function getDepthElement()
    {
        $depth = new Zend_Form_Element_Text('depth');

        $float = new Zend_Validate_Float('en_US');

        $depthValue = new Zend_Validate_GreaterThan(array('min' => 0));
        $depthValue->setMessage(self::ERROR_DEPTH_VALUE);

        $depth->setRequired(false)
                ->addFilter('StringTrim')
                ->addValidator($float, true)
                ->addValidator($depthValue, true)
                ->addErrorMessage(self::ERROR_DEPTH_FORMAT)
                ->setAttrib('class', 'dimensions');

        $depth->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $depth;
    }

    protected function getAgeElement()
    {
        $age = new Zend_Form_Element_Select('age');

        $values = array(1 => '<1', 2 => '1 - 2', 3 => '2 - 3', 4 => '3 - 4', 5 => '4 - 5', 6 => '5+');

        $age->setRequired(true)
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addMultiOptions(array_merge(array(0 => '-- Select --'), $values))
                ->addErrorMessage(self::ERROR_AGE_VALUE)
                ->setAttrib('id', 'source2')
                ->setAttrib('class', 'select-styled age');

        $age->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $age;
    }

    protected function getShippingElement()
    {
        $shipping = new Zend_Form_Element_MultiCheckbox('shipping');

        $values = array('pick-up', 'delivery', 'both');

        $shipping->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_SHIPPING_VALUE)
                ->setMultiOptions($values);

        $shipping->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $shipping;
    }

    protected function getMaterialElement()
    {
        $material = new Zend_Form_Element_Select('material_id');

        $collection = new Application_Model_Material_Collection();
        $values = array(0 => '-- Select --');
        foreach ($collection->getOptions() as $id => $title) {
            $values[$id] = $title;
        }


        $material->setRequired(true)
                ->addValidator('InArray', true, array($collection->getOptions2()))
                ->addErrorMessage(self::ERROR_MATERIAL_VALUE)
                ->setMultiOptions($values)
                ->setAttrib('id', 'source1')
                ->setAttrib('class', 'select-styled material');

        $material->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $material;
    }

    protected function getColorElement()
    {
        $color = new Zend_Form_Element_Select('color_id');

        $collection = new Application_Model_Color_Collection();
        $values = $collection->getOptions2();

        $color->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_COLOR_VALUE)
                ->setMultiOptions($collection->getOptions3());

        $color->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $color;
    }

    protected function getCategoryElement()
    {
        $category = new Zend_Form_Element_Select('category_id');

        $collection = new Category_Model_Category_Collection();
        $values = array_keys($collection->getOptions3());

        $multiOptions[0] = '-- Select --';

        foreach ($collection->getOptions3() as $key => $value) {
            $multiOptions[$key] = $value;
        }

        $category->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_CATEGORY_VALUE)
                ->setMultiOptions($multiOptions);

        $category->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $category;
    }

    protected function getSubCategoryElement()
    {
        $category = new Zend_Form_Element_Select('subcategory_id');

        $collection = new Category_Model_Category_Collection();
        $values = array_keys($collection->getOptions4());

        $category->setRequired(true)
                ->addValidator('InArray', true, array($values))
                ->addErrorMessage(self::ERROR_SUBCATEGORY_VALUE)
                ->setMultiOptions(array_merge(array(0 => '-- Select --'), $collection->getOptions4()))
                ->setAttrib('style', 'zoom:1');

        $category->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $category;
    }

    protected function getManufacturerElement()
    {
        $manufacturer = new Zend_Form_Element_Text('manufacturer');

        $manufacturerEmpty = new Zend_Validate_NotEmpty();
        $manufacturerEmpty->setMessage(self::ERROR_MANUFACTURER_EMPTY);

        $manufacturerLength = new Zend_Validate_StringLength(array('max' => 255));
        $manufacturerLength->setMessage(self::ERROR_MANUFACTURER_LENGTH);

        $manufacturer->setRequired(true)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($manufacturerEmpty, true)
                ->addValidator($manufacturerLength, true)
                ->setAttrib("autocomplete", "off")
                ->setAttrib('placeholder', 'Enter brand name here or type Unknown');

        $manufacturer->setDecorators(array(
            'ViewHelper',
            'Label',
            'Errors'
        ));

        return $manufacturer;
    }

}
