<?php

class Admin_Form_EditCollection extends Zend_Form
{

    public function __construct()
    {

        $this->setName("AddCollection");
        parent::__construct();

        $name = new Zend_Form_Element_Text("name");
        $name->setLabel("Name collection:")
                ->setRequired(true)
                ->addValidator('notEmpty')
                ->addValidator('StringLength', array('max' => 100));
        
        $description = new Zend_Form_Element_Textarea('description');
        $description->setLabel('Description:')
                ->addValidator('StringLength', array('max' => 255))
                ->setAttrib('cols', '50')
                ->setAttrib('rows', '10');
        
        $image = new Zend_Form_Element_File('image');
        $image->setLabel('Select image:')
              ->addValidator('IsImage', false);
        
        $page_url = new Zend_Form_Element_Text('page_url');
        $page_url->setLabel('Page url:');
        
        $is_active = new Zend_Form_Element_Select('is_active');
        $is_active->setLabel('Status');
        $is_active->addMultiOption(1,'Active');
        $is_active->addMultiOption(0,'Deactive');
                

        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setLabel("Create");
        
        $this->addElements(array($name, $description, $image, $page_url, $is_active, $submit));
    }

}

