<?php

class Admin_Form_SearchOrder extends Ikantam_Form {

    public function __construct($options = null, $session = null) {
        $this->_session = $session;
        parent::__construct($options);
    }

    protected $_session;

    //const ERROR_STATUS_VALUE = 'Please, select age.';
    /* const ERROR_AVAILABLE_FROM_FORMAT = 'Please, enter correct available from date.'; */

    public function init() {
        $this->setDecorators(array('Form',
            array('ViewScript', array('viewScript' => 'order/form_decorator.phtml')),
                )
        );


        $this
        //->setAttrib('id', 'search-order')
          //      ->setAttrib('name', 'search-order')
                //->setAttrib('class', 'form-horizontal')
                ->setMethod('get');

        $this->addElement($this->getSearchTextElement())
                ->addElement($this->getStatusElement())
                ->addElement($this->getSearchElement());
    }

    public function getStatusElement() {
        $status = new Zend_Form_Element_Hidden('search_status');
        $status->setAttrib('id', 'search_status')
                ->setAttrib('name', 'search_status')
                ->removeDecorator('HtmlTag')
                ->removeDecorator('label');

        return $status;
    }

    protected function getSearchTextElement() {
        $searchText = new Zend_Form_Element_Text('search_text');
        $searchText->setRequired(true)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->setAttrib('id', 'filter-items')
                ->setAttrib('name', 'search_text')
                ->removeDecorator('HtmlTag')
                ->removeDecorator('label');
        return $searchText;
    }

    public function getSearchElement() {
        $search = new Zend_Form_Element_Submit('search');
        $search->setAttribs(array('style' => 'display: none;'))
                ->removeDecorator('DtDdWrapper');
/*print_r($search->getDecorators());
die();*/
        return $search;
    }

}