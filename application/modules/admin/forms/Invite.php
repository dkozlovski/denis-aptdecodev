<?php

class Admin_Form_Invite extends Zend_Form
{

	public function init()
	{

		$email = new Zend_Form_Element_Text('email');

		$email->addFilters(array('StripTags', 'StringTrim'))
				->setRequired(true)
				->addValidator('notEmpty', true)
				->addValidator('EmailAddress', true)
				->addValidator('StringLength', array('max' => 100))
				->addErrorMessage('Please enter a valid email')
				->setAttrib('Placeholder', 'Email')
				->removeDecorator('label');

		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Send Invitation');

		$this->addElements(array($email, $submit));
	}

}
