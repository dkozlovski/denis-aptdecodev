<?php

class Admin_Form_AbandonedCarts extends Ikantam_Form
{

    const ERROR_EMAIL_IS_EMPTY   = 'Hours required.';
    const ERROR_HOURS_IS_INVALID = 'Hours should be an integer >= 0';
    const ERROR_TEXT_IS_EMPTY    = 'Text required.';
    const ERROR_SUBJECT_REQUIRED = 'Subject required.';

    protected  $_prefixName = 'abandoned_cart';

    public function init()
    {
        $this->setMethod('post');

        for ($i = 1; $i <= 3 ; $i++) {
            $hours = new Zend_Form_Element_Text($this->_prefixName . $i . '__hours');

            $notEmpty = new Zend_Validate_NotEmpty();
            $notEmpty->setMessage(self::ERROR_EMAIL_IS_EMPTY);

            $IntValidator = new Zend_Validate_Int();
            $IntValidator->setMessage(self::ERROR_HOURS_IS_INVALID);



            $hours->setLabel('Hours (' . $i . ')')
                ->addFilter('StringTrim')
                ->addValidator($IntValidator, true)
                ->addValidator('between', true, array('min' => 0, 'max' => 1000, 'inclusive' => true, 'messages' =>self::ERROR_HOURS_IS_INVALID));

            $text = new Zend_Form_Element_Textarea($this->_prefixName . $i . '__text');
            $text->setLabel('Text (' . $i . ')')
                ->setAttrib('style', 'width: 600px;')
                ->addFilter('StringTrim')
                ->addErrorMessage(self::ERROR_TEXT_IS_EMPTY);

            $subject = new Zend_Form_Element_Text($this->_prefixName . $i . '__subject');
            $subject->setLabel('Email Subject (' . $i . ')')
                ->addFilter('StringTrim')
                ->addErrorMessage(self::ERROR_SUBJECT_REQUIRED);

            if ($i == 1) {
                $hours->setRequired(true);
                $subject->setRequired(true);
                $text->setRequired(true);
            }


            $this->addElements(array($hours, $subject, $text));

            /* add separator line */
            if ($i < 3) {
                $this->addElement('hidden', 'plaintext' . $i, array(
                    'description' => '<div class="apt-form-action"></div>',
                    'ignore' => true,
                    'decorators' => array(
                        array('Description', array('escape' => false, 'tag' => '')),
                    ),
                ));
            }
        }

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');
        $this->addElement($submit);
    }

}