<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{
	protected function _initSession()
	{
		ini_set('session.name', 'aptdecobackend');
	}
    
    protected function _initPlugins()
    { 
        Zend_Controller_Front::getInstance()
                              ->registerPlugin(new Application_Plugin_Admin_Acl_Route);  
    }    
}
