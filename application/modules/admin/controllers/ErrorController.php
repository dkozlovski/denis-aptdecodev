<?php

class Admin_ErrorController extends Ikantam_Controller_Admin
{

    const ERRORACCESS = "Error Acces!";

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }

        $this->_helper->_layout->setLayout('admin');
    }

    public function accessAction()
    {
        $this->view->error = self::ERRORACCESS;
    }

}