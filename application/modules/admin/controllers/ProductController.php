<?php

class Admin_ProductController extends Ikantam_Controller_Admin
{

    private $_comission = 15;
    private $_productForm;

    protected function getComission()
    {
        $options = new Application_Model_Options();
        return $options->getOption('comission_from_seller', $this->_comission);
    }

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_setProductForm(new Admin_Form_Product(null, $this->getSession()));
        $this->_helper->_layout->setLayout('admin');
    }

    public function saveAction($redirect = true)
    {
        $productId = $this->getRequest()->getParam('id');

        $product = new Product_Model_Product($productId);

        if (!$product->getId()) {
            throw new Exception('Product does not exist.');
        }

        foreach ($product->getExtraAllImages() as $image) {
            $image->setIsVisible(1)->save();
        }

        $postData = $this->getRequest()->getPost();

        $form = $this->_getProductForm();

        if ($form->isValid($postData) || 1) {

            $productData = $form->getValues();


            $manufacturer = new Application_Model_Manufacturer();

            if (!empty($postData['manufacturer'])) {
                $manufacturer->getByTitle($productData['manufacturer']);
            }

            if (!$manufacturer->getId() && !empty($productData['manufacturer'])) {
                $manufacturer->setTitle($productData['manufacturer'])->save();
            }
            $productData['manufacturer_id'] = $manufacturer->getId();


            if (isset($postData['subcategory_id'])) {
                $productData['category_id'] = $postData['subcategory_id'];
            }

            if (!empty($productData['available_from'])) {
                $productData['available_from'] = strtotime($productData['available_from']);
            } else {
                $productData['available_from'] = null;
            }

            if (!empty($productData['available_till'])) {
                $productData['available_till'] = strtotime($productData['available_till']);
            } else {
                $productData['available_till'] = null;
            }

            $delivery = (isset($productData['shipping']) && is_array($productData['shipping'])) ? $productData['shipping'] : array();



            $productData['is_pick_up_available'] = in_array('pick-up', $delivery) ? 1 : 0;
            $productData['is_delivery_available'] = in_array('delivery', $delivery) ? 1 : 0;

            if (in_array('both', $delivery)) {
                $productData['is_pick_up_available'] = $productData['is_delivery_available'] = 1;
            }
            
            $addressData = array(
                    'full_name' => $productData['pickup_full_name'],
                    'address_line1' => $productData['pickup_address_line1'],
                    'address_line2' => $productData['pickup_address_line2'],
                    'city' => $productData['pickup_city'],
                    'state' => $productData['pickup_state'],
                    'postcode' => $productData['pickup_postcode'],
                    'phone_number' => $productData['pickup_phone'],
                    'country_code' => $productData['pickup_country_code']
                );

                $addressForm = new User_Form_Address();

                if ($addressForm->isValid($addressData)) {
                    $address = new User_Model_Address();

                    $address->addData($addressForm->getValues())
                            ->setUserId($product->getUserId())
                            ->setIsPrimary(0)
                            ->save();
                }

            $productData['created_at'] = time();
            $productData['user_id'] = $product->getUserId();

            if (empty($productData['condition'])) {
                $productData['condition'] = 'new';
            }

            if (!$productData['category_id']) {
                $productData['category_id'] = null;
            }
            if (!$productData['color_id']) {
                $productData['color_id'] = null;
            }
            if (!$productData['material_id']) {
                $productData['material_id'] = null;
            }
            if (!$productData['manufacturer_id']) {
                $productData['manufacturer_id'] = null;
            }

            if (!$productData['price']) {
                $productData['price'] = null;
            } else {
                $productData['price'] = (float) str_replace(',', '', $productData['price']);
            }

            if (!$productData['original_price']) {
                $productData['original_price'] = null;
            } else {
                $productData['original_price'] = (float) str_replace(',', '', $productData['original_price']);
            }

            if (!$productData['width']) {
                $productData['width'] = null;
            }
            if (!$productData['height']) {
                $productData['height'] = null;
            }
            if (!$productData['depth']) {
                $productData['depth'] = null;
            }

            /////////////////
            if (!$productData['pickup_full_name']) {
                $productData['pickup_full_name'] = null;
            }

            if (!$productData['pickup_address_line1']) {
                $productData['pickup_address_line1'] = null;
            }

            if (!$productData['pickup_address_line2']) {
                $productData['pickup_address_line2'] = null;
            }

            if (!$productData['pickup_city']) {
                $productData['pickup_city'] = null;
            }

            if (!$productData['pickup_state']) {
                $productData['pickup_state'] = null;
            }

            if (!$productData['pickup_postcode']) {
                $productData['pickup_postcode'] = null;
            }
            
            if (!$productData['pickup_phone']) {
                $productData['pickup_phone'] = null;
            }

            if (!$productData['pickup_country_code']) {
                $productData['pickup_country_code'] = null;
            }
            //////////////

            $initialPrice = $product->getEAVAttributeValue('initial_price');
            if ($initialPrice === null) {
                $initialPrice = max($product->getPrice(), $product->getOldPrice());
                $product->setEAVAttributeValue('initial_price', $initialPrice);
            }

            $product->addData($productData)->setIsPublished(0)->setIsVisible(1)->save();

            if (isset($productData['files']) && is_array($productData['files'])) {

                foreach ($productData['files'] as $file) {
                    $isMain = 0;
                    if (isset($productData['default_image']) && $productData['default_image'] == $file) {
                        $isMain = 1;
                    }
                    $productImage = new Product_Model_Image();
                    $productImage->setPath($file)
                            ->setProductId($product->getId())
                            ->setIsMain($isMain)
                            ->setIsLocked(0)
                            ->save();
                }
            }

            $message = sprintf('<p>You have saved your <a href="%s">%s</a>.</p>
			<p>What\'s next: Our curation specialists are reviewing your post now. 
            If we have questions about your posting, we\'ll reach out to you immediately. 
            In the meantime, feel free to add another item or 
            browse our <a href="%s">other listings</a></p>
		    <p>At AptDeco, maintaining the highest experience levels to both buyers 
            and sellers is our top priority. Because of this, 
            there are instances where we choose not to post an item if it\'s not 
            a viable for the Aptdeco shopper.</p>
		    <p>We\'ll follow up shortly with an update.</p>', 
                    $product->getProductUrl(), $product->getTitle(), Ikantam_Url::getUrl('catalog/index'));

            if ($redirect) {
                $this->getSession()->addMessage('success', $message);
            }
        } else {
            if ($redirect)
                $this->getSession()->addFormErrors($form->getFlatMessages());

            if ($redirect)
                $this->getSession()->addFormData($postData);

            if (empty($postData['manufacturer'])) {
                $this->getSession()->addFormErrors(array('manufacturer' => 'Manufacturer Required'));
            }
        }

        if ($redirect) {
            $this->_helper->redirector('success', 'add', 'product');
            //$this->_redirect('product/add/new');
        }
    }

    protected function _setProductForm(\Zend_Form $form)
    {
        $this->_productForm = $form;
        return $this;
    }

    /**
     * 
     * @return Ikantam_Form
     */
    protected function _getProductForm()
    {
        return $this->_productForm;
    }

    public function editAction()
    {
        $productId = $this->getRequest()->getParam('id');

        $product = new Product_Model_Product($productId);

        if (!$product->getId()) {
            throw new Exception('Product not found');
            return;
        }

        $this->view->session = $this->getSession();

        $colorCollection = new Application_Model_Color_Collection();
        $categoryCollection = new Category_Model_Category_Collection();
        $materialCollection = new Application_Model_Material_Collection();

        $this->view->categories = $categoryCollection->getAll();
        $this->view->colors = $colorCollection->getAll();
        $this->view->materials = $materialCollection->getAll();

        $this->view->form = $this->_getProductForm();

        $category = $product->getCategory();

        if ($category->getId()) {
            $options = $categoryCollection->getSubOptions($category->getParentId());
            $this->view->form->getElement('subcategory_id')->setMultiOptions($options);
        }

        $product->setSubcategoryId($product->getCategoryId());
        $product->setCategoryId($product->getCategory()->getParentId());
        $product->setManufacturer($product->getManufacturer()->getTitle());


        $shipping = array();

        if ($product->getIsPickUpAvailable()) {
            $shipping['shipping'][] = 'pick-up';
        }
        if ($product->getIsDeliveryAvailable()) {
            $shipping['shipping'][] = 'delivery';
        }

        $this->view->form->populate($shipping);

        $this->view->form->populate($product->getData());

        if ($product->getData('price'))
            $this->view->form->populate(array('price' => number_format($product->getData('price'), 2, '.', '')));
        if ($product->getData('original_price'))
            $this->view->form->populate(array('original_price' => number_format($product->getData('original_price'), 2, '.', '')));

        if ($product->getData('width'))
            $this->view->form->populate(array('width' => number_format($product->getData('width'), 2, '.', '')));
        if ($product->getData('height'))
            $this->view->form->populate(array('height' => number_format($product->getData('height'), 2, '.', '')));
        if ($product->getData('depth'))
            $this->view->form->populate(array('depth' => number_format($product->getData('depth'), 2, '.', '')));

        if ($product->getAvailableFrom()) {
            $this->view->form->populate(array('available_from' => date('M d, Y', $product->getAvailableFrom())));
        }
        if ($product->getAvailableTill()) {
            $this->view->form->populate(array('available_till' => date('M d, Y', $product->getAvailableTill())));
        }


        $this->view->catsConfig = Zend_Json::encode($categoryCollection->getJsonConfig());
        $this->view->product = $product;
        $this->view->comission = $this->getComission();
    }

    public function publishAction()
    {
        $this->saveAction(false);
        $productId = $this->getRequest()->getParam('id');

        $product = new Product_Model_Product($productId);

        if (!$product->getId()) {
            throw new Exception('Product not found');
            return;
        }

        $mainImage = $product->getMainImage();

        $postData = $this->getRequest()->getPost();

        $form = $this->_getProductForm();

        if ($form->isValid($postData) && $mainImage->getId()) {

            $productData = $form->getValues();

            $manufacturer = new Application_Model_Manufacturer();

            $manufacturer->getByTitle($productData['manufacturer']);
            if (!$manufacturer->getId()) {
                $manufacturer->setTitle($productData['manufacturer'])->save();
            }
            $productData['manufacturer_id'] = $manufacturer->getId();

            if (isset($postData['subcategory_id'])) {
                $productData['category_id'] = $postData['subcategory_id'];
            }

            if (!empty($productData['available_from'])) {
                $productData['available_from'] = strtotime($productData['available_from']);
            } else {
                $productData['available_from'] = null;
            }

            if (!empty($productData['available_till'])) {
                $productData['available_till'] = strtotime($productData['available_till']);
            } else {
                $productData['available_till'] = null;
            }

            if (!empty($productData['price'])) {
                $productData['price'] = (float) str_replace(',', '', $productData['price']);
            } else {
                $productData['price'] = null;
            }

            if (!empty($productData['original_price'])) {
                $productData['original_price'] = (float) str_replace(',', '', $productData['original_price']);
            } else {
                $productData['original_price'] = null;
            }

            if (!empty($productData['width'])) {
                $productData['width'] = (float) $productData['width'];
            } else {
                $productData['width'] = null;
            }

            if (!empty($productData['height'])) {
                $productData['height'] = (float) $productData['height'];
            } else {
                $productData['height'] = null;
            }

            if (!empty($productData['depth'])) {
                $productData['depth'] = (float) $productData['depth'];
            } else {
                $productData['depth'] = null;
            }


            //$productData['available_from'] = strtotime($productData['available_from']);
            //$productData['available_till'] = strtotime($productData['available_till']);

            $delivery = (isset($productData['shipping']) && is_array($productData['shipping'])) ? $productData['shipping'] : array();
            $productData['is_pick_up_available'] = in_array('pick-up', $delivery) ? 1 : 0;
            $productData['is_delivery_available'] = in_array('delivery', $delivery) ? 1 : 0;

            if (in_array('both', $delivery)) {
                $productData['is_pick_up_available'] = $productData['is_delivery_available'] = 1;
            }

            $productData['created_at'] = time();
            $productData['user_id'] = $product->getUserId();

            $product->addData($productData)->setIsVisible(1)->save();

            $message = sprintf('<p>You have published your <a href="%s">%s</a>.</p>
			<p>What\'s next: Our curation specialists are reviewing your post now. 
            If we have questions about your posting, we\'ll reach out to you immediately. 
            In the meantime, feel free to add another item or 
            browse our <a href="%s">other listings</a></p>
		    <p>At AptDeco, maintaining the highest experience levels to both buyers 
            and sellers is our top priority. Because of this, 
            there are instances where we choose not to post an item if it\'s not 
            a viable for the Aptdeco shopper.</p>
		    <p>We\'ll follow up shortly with an update.</p>', 
                    $product->getProductUrl(), $product->getTitle(), Ikantam_Url::getUrl('catalog/index'));
            
            
            $this->getSession()->addMessage('success', $message);



            $product->setIsPublished(1)->save();

            $this->_helper->redirector('manage', 'products', 'admin');
            //$this->_helper->redirector('new', 'add', 'product');
        } else {

            if (!$form->isValid($postData)) {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }

            if (!$mainImage->getId()) {
                $this->getSession()->addFormErrors(array('product_image' => 'Image Required'));
            }
            $this->getSession()->addFormData($postData);
        }
        $this->getSession()->addMessage('error', 'Opps, looks like some of the fields have errors or were left blank. Please update all fields highlighted in red and publish again');

        $this->_helper->redirector('edit', 'product', 'admin', array('id' => $product->getId()));
    }

    public function rotateImageAction()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $imageId = $this->getRequest()->getParam('image_id');
        $angle = (int) ($this->getRequest()->getParam('angle') % 360);

        $product = new Product_Model_Product($productId);

        if ($angle != 90 && $angle != 0 && $angle != 180 && $angle != 270) {
            echo 0;
            die();
        }
        
        if (!$product->getId()) {
            echo 0;
            die();
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            echo 0;
            die();
        }
     
        $image->setDefaultAngle($angle)->save();
        
        if ($angle == 0) {
            echo $image->getS3Url(500, 500, 'crop');
            die();
        }
        
        if ($angle == 90) {
            if ($image->get90Exists()) {
                echo $image->getS3Url(500, 500, 'crop');
                die();
            } else {
                $image->set90Exists(1)->save();
            }
        }
        
        if ($angle == 180) {
            if ($image->get180Exists()) {
                echo $image->getS3Url(500, 500, 'crop');
                die();
            } else {
                $image->set180Exists(1)->save();
            }
        } 
        
        if ($angle == 270) {
            if ($image->get270Exists()) {
                echo $image->getS3Url(500, 500, 'crop');
                die();
            }  elseif ($angle == 270) {
                $image->set270Exists(1)->save();
            }
        }
        
        $uploadDir = APPLICATION_PATH . '/../tmp/product-images';
        $imagePath = $image->getS3Path();
        $productId = $product->getId();
        
        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
        
        $oi = $cloudFrontUrl . '/product-images/' . $imagePath;
        
        $parts = explode('/', $imagePath);
        
        $tPath = $uploadDir;
        
        foreach ($parts as $part) {
            $tPath = $tPath . DIRECTORY_SEPARATOR . $part;
            if (!file_exists($tPath)) {
                mkdir($tPath, 0777);
            }
        }
        
        //КОСТЫЛЬ!
        if(!file_exists($file_path)) {
            $file_path = $oldUploadDir . DIRECTORY_SEPARATOR . $imagePath;
        }        
        
        $file_path = $uploadDir . DIRECTORY_SEPARATOR . $imagePath;

        $img = new Ikantam_Image();
        $img->rotateOriginalImage($oi, $file_path, $angle);

        $img->createScaledImages($uploadDir, $imagePath, $productId, $angle);

        echo $image->getS3Url(500, 500, 'crop');
        die();
    }

    public function removeImageAction()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $imageId = $this->getRequest()->getParam('image_id');

        $product = new Product_Model_Product($productId);

        if (!$product->getId()) {
            echo 0;
            die();
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            echo 0;
            die();
        }

        $image->delete();
        echo 1;
        die();
    }

    public function setDefaultImageAction()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $imageId = $this->getRequest()->getParam('image_id');

        $product = new Product_Model_Product($productId);

        if (!$product->getId()) {
            echo 0;
            die();
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            echo 0;
            die();
        }

        $image->setDefaultImage($product->getId(), $image->getId());
        echo 1;
        die();
    }

    protected function create_scaled_image($file_name)
    {
        $options = array(
            'max_width' => 540,
            'max_height' => 540,
            'jpeg_quality' => 100
        );

        $file_path = APPLICATION_PATH . '/../public/upload/products/' . $file_name;
        $version_dir = APPLICATION_PATH . '/../public/upload/products/squared';
        $new_file_path = $version_dir . '/' . $file_name;

        list($img_width, $img_height) = @getimagesize($file_path);

        if (!$img_width || !$img_height) {
            return false;
        }
        //$scale = min(
        $scale = max(
                $options['max_width'] / $img_width, $options['max_height'] / $img_height
        );
        /* if ($scale >= 1) {
          if ($file_path !== $new_file_path) {
          return copy($file_path, $new_file_path);
          }
          return true;
          } */
        //$new_width = $img_width * $scale;
        //$new_height = $img_height * $scale;
        $new_width = $options['max_width'];
        $new_height = $options['max_height'];

        $new_img = @imagecreatetruecolor($new_width, $new_height);
        switch (strtolower(substr(strrchr($file_name, '.'), 1))) {
            case 'jpg':
            case 'jpeg':
                $src_img = @imagecreatefromjpeg($file_path);
                $write_image = 'imagejpeg';
                $image_quality = isset($options['jpeg_quality']) ?
                        $options['jpeg_quality'] : 75;
                break;
            case 'gif':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                $src_img = @imagecreatefromgif($file_path);
                $write_image = 'imagegif';
                $image_quality = null;
                break;
            case 'png':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                @imagealphablending($new_img, false);
                @imagesavealpha($new_img, true);
                $src_img = @imagecreatefrompng($file_path);
                $write_image = 'imagepng';
                $image_quality = isset($options['png_quality']) ?
                        $options['png_quality'] : 9;
                break;
            default:
                $src_img = null;
        }
        $scaledWidth = $new_width / $scale;
        $scaledHeight = $new_height / $scale;

        $startX = (int) (($img_width - $scaledWidth) / 2);
        $startY = (int) (($img_height - $scaledHeight) / 2);

        $success = $src_img && @imagecopyresampled(
                        $new_img, $src_img,
                        //0, 0, 0, 0,
                        0, 0, $startX, $startY, $new_width, $new_height, $scaledWidth, $scaledHeight
                        //$img_width,
                        //$img_height
                ) && $write_image($new_img, $new_file_path, $image_quality);
        // Free up memory (imagedestroy does not delete files):
        @imagedestroy($src_img);
        @imagedestroy($new_img);
        return $success;
    }

    public function successAction()
    {
        $this->view->session = $this->getSession();
    }

}
