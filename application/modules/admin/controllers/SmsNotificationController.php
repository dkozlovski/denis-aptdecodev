<?php

class Admin_SmsNotificationController extends Ikantam_Controller_Admin
{   
    
    public function init()
    {
        $this->_helper->_layout->setLayout("admin");
    }
    
    public function indexAction()
    {
        $collection = new Application_Model_SmsNotification_Collection();
        $this->view->collections = $collection->getAll_();
    }
    
    public function editAction()
    {
        $form = new Admin_Form_EditSmsText();
        
        if($id = $this->getRequest()->getParam('id')){
            $smsNotification = new Application_Model_SmsNotification($id);
            $form->populate($smsNotification->getData());
            $this->view->form = $form;
            if($this->getRequest()->isPost())
            {
                $smsNotification->setData('text', $this->getRequest()->getPost('text'));
                $smsNotification->save();
                $this->redirect('admin/sms-notification');
            }
        }
    }
}

