<?php

class Admin_TransactionsController extends Ikantam_Controller_Admin
{

    protected $_defaultSuccessRedirect = 'admin/invite/index';
    protected $_defaultFailureRedirect = 'admin/login/index';

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect($this->getFailureRedirect());
        }
        $this->_helper->_layout->setLayout('admin');
    }

    public function indexAction()
    {
        $status             = trim($this->getRequest()->getParam('status'));
        $this->view->status = $status;

        $itemsCountPerPage = 10;
        $params            = array();
        $products          = new Admin_Model_Order_Item_Collection();

        $params['status'] = ($status != 'disputed') ? $status : Order_Model_Order::ORDER_STATUS_COMPLETE;

        $this->view->query = null;

        if ($this->getRequest()->getParam('q')) {
            $params['title']   = trim($this->getRequest()->getParam('q'));
            $this->view->query = $params['title'];
        }

        if ($this->getRequest()->getParam('order_id')) {
            $params['order_id']   = (int) preg_replace('/\D/', '', $this->getRequest()->getParam('order_id'));
            $this->view->order_id = $params['order_id'];
        }

        $products->getAllItemsForAdmin($params);

        $collection          = new Admin_Model_Order_Item_Collection();

        foreach ($products as $item) {
            $orderItem = new Order_Model_Item($item->getId());
            if ($status == 'disputed' && $orderItem->getPayout()->getDisputeStatus() !== Order_Model_Payout::PAYOUT_DISPUTE_INITIATED) {
                continue;
            }
            $collection->addItem($item);
        }



        $totalResults = $collection->getSize();

        $totalPages = (int) ceil($totalResults / $itemsCountPerPage);

        $page = $this->getRequest()->getParam('page');

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $itemsCountPerPage;

        $params['offset'] = $offset;
        $params['limit']  = $itemsCountPerPage;
        $collection->pagination($offset, $itemsCountPerPage);
        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;
        $this->view->products    = $collection;
    }

    public function purchasesAction()
    {
        $itemsCountPerPage = 10;

        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);

        if (!$user->getId()) {
            $this->getSession()->addMessage('error', 'this user does not exist');
            $this->_redirect('admin/user/index');
        }

        $this->view->user = $user;

        $page = $this->getRequest()->getParam('page');

        $params = array('buyer_id' => $user->getId());

        $products = new Admin_Model_Order_Item_Collection();
        $products->getAllItems($params);

        $totalResults = $products->getSize();

        $totalPages = (int) ceil($totalResults / $itemsCountPerPage);

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $itemsCountPerPage;

        $params ['offset'] = $offset;
        $params ['limit']  = $itemsCountPerPage;

        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;

        $collection           = new Admin_Model_Order_Item_Collection();
        $this->view->products = $collection->getAllItems($params);
    }

    public function orderAction()
    {
        $itemId = $this->getRequest()->getParam('id');

        $item = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            $this->getSession()->addMessage('error', 'This order does not exist');
            $this->_redirect('admin/user/index');
        }

        $this->view->item = $item;
    }

    /**
     * Process login form
     */
    public function postAction()
    {
        $loginData = $this->getRequest()->getPost();

        try {
            $form = new Admin_Form_Login();

            if ($form->isValid($loginData)) {
                $this->getSession()->authenticate($form->getValues());
                $this->_redirect($this->getSuccessRedirect());
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Ikantam_Exception_Auth $exception) {
            $this->getSession()->addMessage('error', $exception->getMessage());
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to log in. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->getSession()->addFormData($loginData, 'email');
        $this->_redirect($this->getFailureRedirect());
    }

    public function feeAction()
    {
        $options = new Application_Model_Options();

        $sellerConditions = $options->loadOption($options::SELLER)->getConditions();
        $buyerConditions  = $options->loadOption($options::BUYER)->getConditions();

        $this->view->sellerConditions  = $sellerConditions;
        $this->view->buyerConditions   = $buyerConditions;
        $this->view->individualOptions = $options->getAllIndividualOptions();
    }

    protected function getSuccessRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-success');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultSuccessRedirect;
    }

    protected function getFailureRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-failure');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultFailureRedirect;
    }

    private function __getFeeParams($edit = false)
    {
        $rqst       = $this->getRequest();
        $fee        = $rqst->getParam('fee_val', false);
        $cond_value = $rqst->getParam('condition_val', false);
        $operator   = $rqst->getParam('operator', false);
        $code       = $rqst->getParam('code');
        if ($fee === false || $cond_value === false || !$operator)
            exit;

        $result = array($fee, $cond_value, $operator, $code);
        if ($edit) {
            $editData = $rqst->getParam('editData', false);
            if (!$editData)
                exit;
            $result[] = array('operator' => $editData['operator'], 'value' => $editData['condition_val']);
        }
        return $result;
    }

    public function addFeeConditionAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            exit;


        list($fee, $cond_value, $operator, $code) = $this->__getFeeParams();

        $condition = array('operator' => $operator, 'value' => $cond_value);
        $option    = new Application_Model_Options();
        $option->getBy_code($code);
        $result    = array('success' => false);
        try {
            $result['success'] = (bool) $option->addNewConditionValue($fee, $condition);
        } catch (Exception $ex) {
            $result['success'] = false;
            if ($ex->getCode() == 23000) {
                $result['msg'] = 'Condition already exists.';
            } else {
                $result['msg'] = 'Error occured. Try again later.';
            }
        }


        exit(Zend_Json::encode($result));
    }

    public function editFeeConditionAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        list($fee, $cond_value, $operator, $code, $editData) = $this->__getFeeParams(true);
        $option = new Application_Model_Options();
        $result = array('success' => false);
        try {
            $result['success'] = $option->editCondition($code, $editData, array('operator' => $operator, 'value' => $cond_value, 'fee' => $fee));
        } catch (Exception $ex) {
            $result['success'] = false;
            $result['msg']     = 'Error occured. Try again later.';
        }

        exit(Zend_Json::encode($result));
    }

    public function editIndividualConditionAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);


        $option   = new Application_Model_Options();
        $code     = $option::INDIVIDUAL;
        $userId   = $this->getRequest()->getParam('id');
        $editData = array('operator' => 'true', 'user' => $userId);
        $result   = array('success' => false);
        try {
            $result['success'] = $option->editCondition($code, $editData, array('operator' => $operator, 'value' => $cond_value, 'fee' => $fee));
        } catch (Exception $ex) {
            $result['success'] = false;
            $result['msg']     = 'Error occured. Try again later.';
        }

        exit(Zend_Json::encode($result));
    }

    public function deleteConditionAction()
    {
        $rqst      = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        $condition = $rqst->getParam('condition', array());
        $code      = $rqst->getParam('code', '');
        $options   = new Application_Model_Options();

        exit(Zend_Json::encode(array('success' => $options->deleteCondition($code, $condition))));
    }

    public function testFeeAction()
    {
        $code    = $this->getRequest()->getParam('code');
        $sum     = $this->getRequest()->getParam('sum');
        $options = new Application_Model_Options();
        try {
            $fee = $options->getFee($sum, $code);
            $sum = $sum - ($sum / 100 * $fee);
        } catch (Exception $ex) {
            if ($ex->getCode() == 7) {
                $fee = 'err';
                $sum = 'err (sum must be a number)';
                throw $ex;
            }
        }
        exit(Zend_Json::encode(array('fee' => $fee, 'sum' => $sum)));
    }

    public function getUserAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $id = $this->getRequest()->getParam('id');

        $user   = new User_Model_User((int) $id);
        $result = array(
            'success'     => (bool) $user->getId(),
            'name'        => $user->getFullName(),
            'email'       => $user->getMainEmail(),
            'profile_url' => $user->getProfileUrl(),
            'id'          => $user->getId(),
        );
        exit(Zend_Json::encode($result));
    }

    public function addIndividualFeeAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $userId  = $rqst->getParam('id');
        $fee     = $rqst->getParam('fee');
        $options = new Application_Model_Options();
        $result  = array();
        try {
            $result['success'] = $options->addIndividualFee($userId, $fee);
        } catch (Exception $ex) {
            $result['success'] = false;
        }
        exit(Zend_Json::encode($result));
    }

    public function editIndividualFeeAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $userId  = $rqst->getParam('id');
        $fee     = $rqst->getParam('fee');
        $options = new Application_Model_Options();

        exit(Zend_Json::encode(array('success' => $options->editIndividualFee($userId, $fee))));
    }

    public function deleteIndividualFeeAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $userId = $rqst->getParam('id');
        $code   = $rqst->getParam('code');

        $options = new Application_Model_Options();
        $result  = array();

        $result['success'] = $options->deleteIndividualFee($userId, $code);

        exit(Zend_Json::encode($result));
    }

    public function shippingFeeAction()
    {
        $SFCollection             = new Application_Model_ShippingFeeRule_Collection();
        $this->view->SFCollection = $SFCollection->getAll_();
    }

    public function addShippingFeeAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $SF = new Application_Model_ShippingFeeRule();
        $SF->setSum($rqst->getParam('sum'))
                ->setFee($rqst->getParam('fee'))
                ->setOperator($rqst->getParam('operator'));

        exit(Zend_Json::encode(array('success' => (bool) $SF->save()->getId(false))));
    }

    public function deleteShippingFeeAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $pair = $rqst->getParam('condition');

        $SF = new Application_Model_ShippingFeeRule();

        $SF->getByPair($pair['operator'], $pair['value']);

        if (!$SF->isExists()) {
            exit(Zend_Json::encode(array('success' => false)));
        }

        $SF->delete();

        exit(Zend_Json::encode(array('success' => !$SF->isExists())));
    }

    public function editShippingFeeAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $operator = $rqst->getParam('operator');
        $sum      = $rqst->getParam('sum');
        $fee      = $rqst->getParam('fee');

        $oldData = $rqst->getParam('editData');

        if ($fee == $oldData['fee'] && $operator == $oldData['operator'] && $sum == $oldData['sum']) {
            exit(Zend_Json::encode(array('success' => true)));
        }

        $SF = new Application_Model_ShippingFeeRule();

        $SF->getByPair($oldData['operator'], $oldData['sum']);

        if (!$SF->isExists()) {

            exit(Zend_Json::encode(array('success' => false, 'msg' => 'Edit error: condition not exists.')));
        }

        try {
            $SF->setSum($sum)
                    ->setFee($fee)
                    ->setOperator($operator)
                    ->save();
        } catch (Exception $ex) {
            if ($ex->getCode() == 23000) {
                exit(Zend_Json::encode(array('success' => false, 'msg' => 'Duplicate error: condition already exists.')));
            }
            throw $ex;
        }

        exit(Zend_Json::encode(array('success' => $SF->isExists())));
    }

    public function testShippingFeeAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $SFCollection = new Application_Model_ShippingFeeRule_Collection();


        exit(Zend_Json::encode(array('fee' => $SFCollection->getShippingFee($rqst->getParam('sum'), $rqst->getParam('num')))));
    }

    /**
     * to store additional parameters
     */
    public function nvpAction()
    {
        $allowedOptions = array(
            'transaction_fee_from_sellers',
        );
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        foreach ($this->getRequest()->getParams() as $key => $value) {
            if (in_array($key, $allowedOptions)) {
                Application_Model_NVP::option($key, $value);
            }
        }
        $this->getResponse()->setHttpResponseCode(204);
    }

}