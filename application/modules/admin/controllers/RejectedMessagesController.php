<?php

class Admin_RejectedMessagesController extends Ikantam_Controller_Admin
{
    protected $_perPage = 12;
    
    public function indexAction ()
    {
        $rejectedMessages = new Conversation_Model_Conversation_Message_Invalid_Collection;
        
        $this->view->messages = $rejectedMessages;
        
        $page = $this->getRequest()->getParam('page', 1);
        $offset = ($page - 1) * $this->_perPage;
        
        $paginator = new Zend_Paginator(new Ikantam_Paginator_Flex_Adapter($rejectedMessages)); 
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage($this->_perPage)
                  ->getCurrentItems(); // collection fill

        $this->view->paginator = $paginator;
    }
    
}