<?php

class Admin_HomepageController extends Ikantam_Controller_Admin
{

    protected $_categories = null;
    protected $_brands     = null;
    protected $_product    = null;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_helper->_layout->setLayout('admin');


        $this->_categories = new Category_Model_Category_Collection();
        $this->_brands     = new Application_Model_Manufacturer_Collection();
        $this->_product    = new Product_Model_Product();

        $this->_categories->getCategoriesFixedAtHomePage();
        $this->_brands->getManufacturersFixedAtHomePage();
        $this->_product->getFixedAtHomeProduct();
    }

    public function indexAction()
    {
        $tmpObjects = Admin_Model_Homepage_TmpObjects::getInstance();

        $this->view->product  = $tmpObjects->getProduct();
        $this->view->blogPost = $tmpObjects->getBlogPost();

        $this->view->background     = $tmpObjects->getBackground();
        $this->view->blogBackground = $tmpObjects->getBlogBackground();

        $categories             = new Category_Model_Category_Collection();
        $this->view->categories = $categories->getAll(); // select list


        $this->view->firstBrand        = Application_Model_Manufacturer::getFirst();
        $this->view->categoryTemplates = $tmpObjects->getCategories();
        $this->view->brandTemplates    = $tmpObjects->getBrands();

        $settings              = new Admin_Model_Settings();
        $this->view->pageTitle = $settings->getValue('home_page_title', 'home_page_title');

        $sliderForm   = new Widget_Form_Slider();
        $asSeenInForm = new Widget_Form_AsSeenIn();

        $slides = new Widget_Model_Homepage_Slider_Collection();
        $slides->getAll();

        $asSeenIns = new Widget_Model_Homepage_AsSeenIn_Collection();
        $asSeenIns->getAll();

        $productCollection = new Product_Model_Product_Collection();

        $this->view->slides           = $slides;
        $this->view->sliderForm       = $sliderForm;
        $this->view->asSeenIn         = $asSeenIns;
        $this->view->asSeenInForm     = $asSeenInForm;
        $this->view->featuredProducts = $productCollection->getFeatured();
    }

    public function setPageTitleAction()
    {
        $homePageTitle = $this->getRequest()->getPost('home_page_title');
        $settings      = new Admin_Model_Settings();
        $settings->saveConfig('home_page_title', array('home_page_title' => $homePageTitle));

        $this->_redirect('admin/homepage/index');
    }

    public function previewAction()
    {
        $tmpObjects = Admin_Model_Homepage_TmpObjects::getInstance();
        $this->view->layout()->disableLayout();



        $product = $tmpObjects->getProduct();

        $this->view->background = $tmpObjects->getBackground();
        $this->view->categories = $tmpObjects->getCategories();
        $this->view->brands     = $tmpObjects->getBrands();
        $this->view->product    = $product;
        $this->view->seller     = $product->getSeller();
    }

    public function removeBackgroundAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;
        Admin_Model_Homepage_TmpObjects::getInstance()->remove('background');
        exit;
    }

    public function removeBlogBackgroundAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;
        Admin_Model_Homepage_TmpObjects::getInstance()->remove('blog_background');
        exit;
    }

    public function addCategoryTemplateAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;
        $id = $this->getRequest()->getParam('id');
        if (!(int) $id)
            exit;

        $uid = Admin_Model_Homepage_TmpObjects::getInstance()->save('category', array('id' => $id, 'fileName' => null));

        exit(Zend_Json::encode(array('uid' => $uid)));
    }

    public function removeCategoryTemplateAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;
        $uid = $this->getRequest()->getParam('uid');
        if (!$uid)
            exit;

        Admin_Model_Homepage_TmpObjects::getInstance()->remove('category', $uid);
        exit;
    }

    public function modifyCategoryTemplateAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;
        $uid   = $this->getRequest()->getParam('uid', false);
        $newId = $this->getRequest()->getParam('id', false);
        if (!$uid || !$newId)
            exit;

        Admin_Model_Homepage_TmpObjects::getInstance()->edit('category', $uid, $newId);

        exit;
    }

    public function addBrandTemplateAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;
        if (!$id = (int) $this->getRequest()->getParam('id', false))
            exit;

        $uid = Admin_Model_Homepage_TmpObjects::getInstance()->save('brand', array('id' => $id, 'fileName' => null));

        exit(Zend_Json::encode(array('uid' => $uid)));
    }

    public function modifyBrandTemplateAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;
        $uid   = $this->getRequest()->getParam('uid', false);
        $newId = $this->getRequest()->getParam('id', false);
        if (!$uid || !$newId)
            exit;

        Admin_Model_Homepage_TmpObjects::getInstance()->edit('brand', $uid, $newId);
        $brand = new Application_Model_Manufacturer($newId);

        exit(Zend_Json::encode(array('success' => true, 'title' => $brand->getTitle())));
    }

    public function removeBrandTemplateAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;
        $uid = $this->getRequest()->getParam('uid', false);
        if (!$uid)
            exit;

        Admin_Model_Homepage_TmpObjects::getInstance()->remove('brand', $uid);
        exit;
    }

    public function addProductAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;

        $id = $this->getRequest()->getParam('id', false);
        if (!$id)
            exit;

        $success = false;
        $product = new Product_Model_Product();

        if (is_numeric($id)) {
            $product->getById((int) $id);
        } else {
            $product->getByPageUrl($id);
        }

        if ($product->getId()) {
            Admin_Model_Homepage_TmpObjects::getInstance()->save('product', $product->getId());
            $success = true;
            $text    = $product->getTitle();
        }

        exit(Zend_Json::encode(array('success' => $success, 'product' => $text)));
    }

    public function addBlogAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;

        $slug = $this->getRequest()->getParam('slug', false);
        if (!$slug)
            exit;

        $success = false;
        $static  = new Application_Model_Static();

        $blogPost = $static->getBlogPosts(null, $slug);

        if (is_array($blogPost) && count($blogPost)) {
            Admin_Model_Homepage_TmpObjects::getInstance()->save('blog_post', $blogPost[0]['post_name']);
            $success = true;
            $text    = $blogPost[0]['post_name'];
        }

        exit(Zend_Json::encode(array('success' => $success, 'post_title' => $text)));
    }

    public function saveChangesAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;

        $result = Admin_Model_Homepage_TmpObjects::getInstance()->saveInDB();

        exit(Zend_Json::encode(array('success' => $result)));
    }

}