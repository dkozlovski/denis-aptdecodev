<?php

class Admin_PickupAndDeliveryDateController extends Ikantam_Controller_Admin
{

    protected $_range;
    protected $_collection;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_range = $this->getRnge();
        $this->_helper->_layout->setLayout('admin');
    }

    public function indexAction()
    {
        $tableDeliveryDates = $this->_getTableDeliveryDates($this->_range);
        $tablePickupDates = $this->_getTablePickupDates($this->_range);
        $this->view->tableDeliveryDates = $tableDeliveryDates;
        $this->view->tablePickupDates    = $tablePickupDates;
        $this->view->rowsPerTable = 7;
        $this->view->limit = count($tableDeliveryDates);

    }

    public function modifyAction()//AJAX
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $dateString = $this->getRequest()->getParam('dateString', false);
        $periods = $this->getRequest()->getParam('periods', false);

        if ($this->getParam('method') == 'delivery') {
            $date = new Admin_Model_DeliveryDate();
        } elseif ($this->getParam('method') == 'pickup'){
            $date = new Admin_Model_PickupDate();
        } else {
            throw new Exception('Undefined method');
        }

        $date->getByDateString($dateString);

        $date->setIsFirstPeriodAvailable($periods[0])
                ->setIsFirstPeriodForSmallAvailable($periods[1])
                ->setIsSecondPeriodAvailable($periods[2])
                ->setIsSecondPeriodForSmallAvailable($periods[3])
                ->setIsThirdPeriodAvailable($periods[4])
                ->setIsThirdPeriodForSmallAvailable($periods[5])
                ->setDate(strtotime($dateString))
                ->save();
    }

    protected function getRnge($days = 70)
    {
        $current = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        return array($current, $current + (86400 * $days));
    }

    protected function _getTableDeliveryDates($range, $format = 'd M Y D')
    {
        $collection = new Admin_Model_DeliveryDate_Collection();
        $collection->getByDateRange($this->_range);
        return $this->_getDeliveryDates($collection, $range, $format);
    }

    protected function _getTablePickupDates($range, $format = 'd M Y D')
    {
        $collection = new Admin_Model_PickupDate_Collection();
        $collection->getByDateRange($this->_range);
        return $this->_getDeliveryDates($collection, $range, $format);
    }

    /**
     * @param $collection
     * @param array $range
     * @param string $format
     * @return array      
     * RESULT STRUCTURE:
     * Array (
     *          0 => array (1, 1, 1, 0, 'stringDate'=>'05 JAN 2012'), - first, second and third period availability
     *          ...
     *          )
     */
    protected function _getDeliveryDates($collection, $range, $format = 'd M Y D')
    {
        $aux = array();
        $result = array();

        for ($i = $range[0]; $i < $range[1]; $i += 86400) {
            $aux[date($format, $i)] = array(0, 0, 0, 0, 0, 0, 0, 0);
        }

        foreach ($collection->getItems() as $date) {
            /* @var $date Admin_Model_DeliveryDate */
            if (key_exists($date->getDateString(), $aux)) {
                $aux[$date->getDateString()] = array(
                    $date->getIsFirstPeriodAvailable(),
                    $date->getIsFirstPeriodForSmallAvailable(),
                    $date->getIsSecondPeriodAvailable(),
                    $date->getIsSecondPeriodForSmallAvailable(),
                    $date->getIsThirdPeriodAvailable(),
                    $date->getIsThirdPeriodForSmallAvailable(),
                    $date->getIsFourthPeriodAvailable(),
                    $date->getIsFourthPeriodForSmallAvailable()
                );
            }
        }

        foreach (array_keys($aux) as $stringDate) {
            $result[] = array_merge($aux[$stringDate], array('stringDate' => $stringDate));
        }

        return $result;
    }

}