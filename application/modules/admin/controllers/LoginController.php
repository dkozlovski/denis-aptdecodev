<?php

class Admin_LoginController extends Ikantam_Controller_Admin
{

    protected $_defaultSuccessRedirect = array('admin/invite/index', 'admin/products/index', '', 'admin/transactions/index'); // see Application_Model_Acl
    protected $_defaultFailureRedirect = 'admin/login/index';

    public function init()
    {
        if ($this->getSession()->isLoggedIn()) {
            $this->_redirect($this->getSuccessRedirect());
        }
    }

    public function indexAction()
    {
        $this->view->session = $this->getSession();
        $this->_helper->_layout->setLayout('admin');
    }

    /**
     * Process login form
     */
    public function postAction()
    {
        $loginData = $this->getRequest()->getPost();

        try {
            $form = new Admin_Form_Login();

            if ($form->isValid($loginData)) {
                $this->getSession()->authenticate($form->getValues());
                $this->_redirect($this->getSuccessRedirect());
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Ikantam_Exception_Auth $exception) {
            $this->getSession()->addMessage('error', $exception->getMessage());
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to log in. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->getSession()->addFormData($loginData, 'email');
        $this->_redirect($this->getFailureRedirect());
    }

    protected function getSuccessRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-success');
        if (!empty($url)) {
            return urldecode($url);
        }
        foreach ($this->_defaultSuccessRedirect as $url) {
            if ($this->_helper->isAllowed($url)) {
                return $url;
            }
        }
        // no default success redirect url allowed to this role
        throw new Application_Model_Acl_Exception_Route('No allowed url defined.', 404);
    }

    protected function getFailureRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-failure');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultFailureRedirect;
    }

}