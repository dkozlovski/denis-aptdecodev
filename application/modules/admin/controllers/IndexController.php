<?php

class Admin_IndexController extends Ikantam_Controller_Admin
{

	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('admin/login/index');
		}
        $this->_helper->_layout->setLayout('admin');
		parent::init();
	}

	public function indexAction()
	{
	   	if($this->_helper->isAllowed('admin/users/index')){
            $this->_redirect('admin/users/index');    
        }elseif($this->_helper->isAllowed('admin/products/manage')){
            $this->_redirect('admin/products/manage');
        }
		
	}


}
