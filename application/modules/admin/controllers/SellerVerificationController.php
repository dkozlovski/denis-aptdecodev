<?php

class Admin_SellerVerificationController extends Ikantam_Controller_Admin
{
    public function init()
    {
        $this->_helper->_layout->setLayout('admin');
    }
    
    public function indexAction()
    {   
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/handlebars-v1.3.0.min.js'));
        $this->view->session = $this->getSession();

        $search = $this->_getSearchParam();
        if (!$search) {
            $search = null;
        }
        
        $limit = 50;

        $users  = new Application_Model_User_Collection();        

        $totalPages = (int) ceil($users->countVerification($search) / $limit);

        $page = (int) $this->getRequest()->getParam('page');

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $limit;
       
        $this->view->users       = $users->getVerification($offset, $limit, $search);
        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;
        $this->view->search      = $search;
    }
    
    protected function _getSearchParam()
    {
        $search = $this->getRequest()->getParam('search');
        return trim(str_replace('%', '', $search));
    }
}
