<?php

class Admin_ItemsController extends Ikantam_Controller_Admin
{
	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('admin/login/index');
		}
		$this->_helper->_layout->setLayout('admin');
	}

	public function indexAction()//users list
	{
		$this->_forward('view');
	}
	
	public function viewAction()//users list
	{
		$id = $this->getRequest()->getParam('id');
		
		$item = new Order_Model_Item($id);
		
		if (!$item->getId()) {
			$this->_redirect('admin/index/index');
		}
		
		$this->view->item = $item;
	}
	
	
}