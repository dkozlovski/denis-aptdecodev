<?php

class Admin_CollectionsController extends Ikantam_Controller_Admin
{   
    
    public function init()
    {
        $this->_helper->_layout->setLayout("admin");
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery.autocomplete.js'));
        $this->view->headScript()
            ->appendFile('//rawgithub.com/algolia/algoliasearch-client-js/master/dist/algoliasearch.min.js');
    }

    public function indexAction()
    {

        $collections = new Application_Model_Set_Collection();

        $this->view->collections = $collections->getAll_();
    }
    
    public function addAction()
    {
        $form = new Admin_Form_AddCollection();
        
        if($this->getRequest()->isPost())
        {
            if($form->isValid($this->getRequest()->getPost()))
            {
                $collection = new Application_Model_Set();
                $collection->setName($this->getRequest()->getPost('name'));
                $collection->setDescription($this->getRequest()->getPost('description'));
                $collection->setPage_url($this->getRequest()->getPost('page_url'));
                $collection->setIs_active($this->getRequest()->getPost('is_active'));
                $collection->save();
                if($_FILES['image']['name']){
                    
                    $path = APPLICATION_PATH.'/../public/upload/collections';
                    $image = new Ikantam_Image();
                    $collection->setData('image_path',$image->uploadImage($path,$collection->getData('id')));
                    $collection->save();
                    $pathConvector = $path.'/'.$collection->getData('image_path');
                    $iConvector =new Ikantam_File_ImageConverter();
                    $res = $iConvector->createScaledImages($pathConvector.'/original-0.jpg', $pathConvector);
                    
                }
                $this->redirect("admin/collections");

            }
        }
        
        $this->view->form = $form;
        
    }
    
    public function editAction()
    {
        $collection = new Application_Model_Set();
        $collection->getById($this->getRequest()->getParam('id'));
        $form = new Admin_Form_EditCollection();
        
        if($this->getRequest()->isPost() && $collection->getdata())
        {
            if($form->isValid($this->getRequest()->getPost()))
            {
                $collection->setName($this->getRequest()->getPost('name'));
                $collection->setDescription($this->getRequest()->getPost('description'));
                $collection->setPage_url($this->getRequest()->getPost('page_url'));
                $collection->setIs_active($this->getRequest()->getPost('is_active'));
                $collection->save();
                
                if($_FILES['image']['name']){
                    
                    $path = APPLICATION_PATH.'/../public/upload/collections';
                    $image = new Ikantam_Image();
                    $collection->setData('image_path',$image->uploadImage($path,$collection->getData('id')));
                    $collection->save();
                    $pathConvector = $path.'/'.$collection->getData('image_path');
                    $iConvector =new Ikantam_File_ImageConverter();
                    $res = $iConvector->createScaledImages($pathConvector.'/original-0.jpg', $pathConvector);

                }
                
                $this->redirect("admin/collections");

            }
        }elseif($collection->getData()){
            
            $form->getElement('name')->setValue($collection->getName());
            $form->getElement('description')->setValue($collection->getDescription());
            $form->getElement('page_url')->setValue($collection->getPageUrl());
            $form->getElement('is_active')->setValue($collection->getIsActive());
            $this->view->form = $form;
        }else{
            $this->redirect("admin/collections");
        }
        
        $this->view->form = $form;
    }
    
    public function deleteAction()
    {
        $collection = new Application_Model_Set($this->getRequest()->getParam('id'));
        if($collection->getData()){
            $collection->delete();           
        }
        $this->redirect("admin/collections");
    }
    
    public function addProductAction()
    {
         if($this->getRequest()->getParam('id')){
            $collection = new Application_Model_Set($this->getRequest()->getParam('id'));
            if($collection->getData()){
                $products = $collection->getProducts();
                $idProducts = array();
                foreach($products as $product){
                    $idProducts[] = $product->getId();
                }
                $this->view->idProducts = json_encode($idProducts);
                $this->view->idCollection = $this->getRequest()->getParam('id');
            }else{
                $this->redirect("admin/collections");
            }
        }
    }
    
    public function ajaxAddProductAction()
    {
       $collection = new Application_Model_Set($this->getRequest()->getParam('collection'));
       $collection->addProducts($this->getRequest()->getParam('product'));
       echo $this->getRequest()->getParam('product');
       exit;
    }
    
    public function ajaxDeleteProductAction()
    {
       $collection = new Application_Model_Set($this->getRequest()->getParam('collection'));
       $collection->removeProducts($this->getRequest()->getParam('product'));
       echo $this->getRequest()->getParam('product');
       exit;
    }
    
    public function ajaxSearchProductsAction()
    {
        $search = $this->_getSearchParam();
        $products =new Product_Model_Product_Collection();
        $filter = $products->flexFilter();
        if ($search) {
            $filter->startGroup()
                ->title('like',  $search . '%')
                ->or_id('like',  $search . '%')
                ->or_page_url('like',  $search . '%')
                ->endGroup();
            
            $searchProducts = $filter->apply(12, 0);
            $jsonProducts = array();
            foreach ($searchProducts as $item){
                
                $jsonProducts[] = array('id' => $item->getId(), 
                                        'title' => $item->getTitle());
                
            }
            echo json_encode($jsonProducts);
            exit;
        }else{
            echo '[]';
            exit;
        }
  
    }
    
    protected function _getSearchParam()
    {
        $search = $this->getRequest()->getParam('search');
        $search = str_replace(Ikantam_Url::getUrl('').'catalog/','',$search);
        return trim(str_replace('%', '', $search));
    }
}

