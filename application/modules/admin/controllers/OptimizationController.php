<?php

class Admin_OptimizationController extends Ikantam_Controller_Admin
{
    public function init()
    {
        $this->_helper->_layout->setLayout("admin");
    }
    
    public function indexAction()
    {   
        $form = new Admin_Form_Optimization();
        $this->view->form = $form;
        if($this->getRequest()->isPost())
        {
            if (Zend_Registry::isRegistered('config')) {
                $config = Zend_Registry::get('config');
                if (isset($config->assets->version) && $config->assets->version) {
                    $_version = 'static/v/' . $config->assets->version . '/';
                }else{
                    $_version = "";
                    
                }
                
                if(isset($config->PathOutApplications) && $config->PathOutApplications){
                    $pathapp = $config->PathOutApplications.'/';
                }else{
                    //$pathapp = APPLICATION_PATH."/outapp/"
                }
            }
            
            $optimization = new Ikantam_ResMin();
            $optimization->setPathApp($pathapp);
            
            if($this->getRequest()->getPost("js_dir") != ""){
                $optimization->setDir(APPLICATION_PATH.'/../public/'.$_version.$this->getRequest()->getPost("js_dir"));
                if($this->getRequest()->getPost("js_new_dir") != ""){
                    $optimization->setNewDir(APPLICATION_PATH.'/../public/'.$_version.$this->getRequest()->getPost("js_new_dir"));
                }
                $optimization->minify();
            }
            
            if($this->getRequest()->getPost("css_dir") != ""){
                $optimization->setDir(APPLICATION_PATH.'/../public/'.$_version.$this->getRequest()->getPost("css_dir"));
                if($this->getRequest()->getPost("css_new_dir") != ""){
                    $optimization->setNewDir(APPLICATION_PATH.'/../public/'.$_version.$this->getRequest()->getPost("css_new_dir"));
                }
                $optimization->minify();
            }
            
            if($this->getRequest()->getPost("png_dir") != ""){
                $optimization->setDir(APPLICATION_PATH.'/../public/'.$_version.$this->getRequest()->getPost("png_dir"));
                if($this->getRequest()->getPost("png_new_dir") != ""){
                    $optimization->setNewDir(APPLICATION_PATH.'/../public/'.$_version.$this->getRequest()->getPost("png_new_dir"));
                }
                $optimization->minify();
            }
        }
    }
}
