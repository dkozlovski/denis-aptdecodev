<?php

class Admin_InviteAdminController extends Ikantam_Controller_Admin
{   
    public function init()
    {
        $this->_helper->_layout->setLayout("admin");
    }
    
    public function indexAction()
    {   
        $form = new Admin_Form_InvitePasswordAdmin();
       
        if($this->getRequest()->getParam('code')){          
            $invitation = new Application_Model_Invitation();
            $invitationAdmin = $invitation->getInvitationAdmin($this->getRequest()->getParam('code'));

            if($invitationAdmin && $invitationAdmin['is_used'] == 0){
                
                if ($this->getRequest()->isPost()) {
                    if ($form->isValid($this->getRequest()->getPost())) {

                        $manager = new Admin_Model_Admin_Manager($invitationAdmin['admin_id']);
                        if($manager->id){
                            $pas               = new Ikantam_Crypt_Password_Bcrypt();
                            $password          = $pas->create($this->getRequest()->getPost("password"));
                            $manager->password = $password;
                            $manager->save();
                            $invitation->confirmInvitationAdmin($invitationAdmin['code']);
                            $this->redirect("admin/login");
                        }else{
                            $this->redirect("");
                        }
                        
                    }
                    $this->view->form = $form;
                    $this->view->email = $invitationAdmin['email'];
                    
                }else{
                    $this->view->form = $form;
                    $this->view->email = $invitationAdmin['email'];
                }
                
            }else{
            
                $this->redirect("");
            
            }
        }else{
            
            $this->redirect("");
            
        }
    }
}