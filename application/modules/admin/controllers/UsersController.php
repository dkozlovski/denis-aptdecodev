<?php

class Admin_UsersController extends Ikantam_Controller_Admin
{

    const ERROR_MESSAGE_DATA_REQUIRED = 'Opps, looks like some of the fields have errors or were left blank.';
    const MESSAGE_ITEM_NOT_AVAILABLE  = 'Order cancelled. We have notified buyer that the product is not available.';
    const MESSAGE_ITEM_AVAILABLE      = 'Order confirmed.';

    protected $_defaultSuccessRedirect = 'admin/invite/index';
    protected $_defaultFailureRedirect = 'admin/login/index';

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect($this->getFailureRedirect());
        }
        $this->_helper->_layout->setLayout('admin');
    }

    protected function _getSearchParam()
    {
        $search = $this->getRequest()->getParam('search');
        return trim(str_replace('%', '', $search));
    }

    public function indexAction()//users list
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/handlebars-v1.3.0.min.js'));
        $this->view->session = $this->getSession();

        $search = $this->_getSearchParam();

        $users  = new Application_Model_User_Collection();
        $filter = $users->getFlexFilter();

        if ($search) {
            $filter->startGroup()
                ->full_name('like', '%' . $search . '%')
                ->or_email('like', '%' . $search . '%')
                ->endGroup();
        }

        //Filter by "user_product_verification_available" setting value
        if ($verifiedFilterParam = $this->getParam('filter_verified')) {
            if ($verifiedFilterParam != 'all') {
                $filter->setting_name('=', 'user_product_verification_available')//!
                    ->setting_value('=', $verifiedFilterParam);
            }
        }

        $this->view->filter_verified = $verifiedFilterParam;

        $limit = 50;

        $totalPages = (int) ceil($filter->count() / $limit);

        $page = (int) $this->getRequest()->getParam('page');

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $limit;

        $filter->order('id desc');

        $this->view->users       = $filter->apply($limit, $offset);
        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;
        $this->view->search      = $search;
    }

    public function blockAction()
    {
        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);
        if ($user->getId()) {
            $user->setIsActive(0)->save();
            $this->unpublishUserProducts($user);
        }
        $this->_helper->redirector('index', 'users', 'admin');
    }

    public function unblockAction()
    {
        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);
        if ($user->getId()) {
            $user->setIsActive(1)->save();
        }
        $this->_helper->redirector('index', 'users', 'admin');
    }

    public function viewAction()
    {
        //view particular user
        $this->_forward("products");
    }

    public function productsAction()
    {
        $itemsCountPerPage = 10;

        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);

        if (!$user->getId()) {
            $this->getSession()->addMessage('error', 'this user does not exist');
            $this->_redirect('admin/user/index');
        }

        $this->view->user = $user;

        $page = (int) $this->getRequest()->getParam('page');


        $products = new Product_Model_Product_Collection();
        $filter   = $products->getFlexFilter();

        $filter->user_id('=', $user->getId())->is_visible('=', 1);

        $limit = 50;

        $totalPages = (int) ceil($filter->count() / $limit);


        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $limit;

        $filter->order('id desc');

        $this->view->products    = $filter->apply($limit, $offset);
        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;
        $this->view->activeMenu  = 'products';
    }

    public function salesAction()
    {
        $itemsCountPerPage = 10;

        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);

        if (!$user->getId()) {
            $this->getSession()->addMessage('error', 'this user does not exist');
            $this->_redirect('admin/user/index');
        }

        $this->view->user = $user;

        $page = $this->getRequest()->getParam('page');

        $params = array('seller_id' => $user->getId());

        $products = new Admin_Model_Order_Item_Collection();
        $products->getAllItems($params);

        $totalResults = $products->getSize();

        $totalPages = (int) ceil($totalResults / $itemsCountPerPage);

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $itemsCountPerPage;

        $params['offset'] = $offset;
        $params['limit']  = $itemsCountPerPage;

        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;

        $collection             = new Admin_Model_Order_Item_Collection();
        $this->view->products   = $collection->getAllItems($params);
        $this->view->activeMenu = 'sales';
    }

    public function purchasesAction()
    {
        $itemsCountPerPage = 10;

        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);

        if (!$user->getId()) {
            $this->getSession()->addMessage('error', 'this user does not exist');
            $this->_redirect('admin/user/index');
        }

        $this->view->user = $user;

        $page = $this->getRequest()->getParam('page');

        $params = array('buyer_id' => $user->getId());

        $products = new Admin_Model_Order_Item_Collection();
        $products->getAllItems($params);

        $totalResults = $products->getSize();

        $totalPages = (int) ceil($totalResults / $itemsCountPerPage);

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $itemsCountPerPage;

        $params ['offset'] = $offset;
        $params ['limit']  = $itemsCountPerPage;

        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;

        $collection             = new Admin_Model_Order_Item_Collection();
        $this->view->products   = $collection->getAllItems($params);
        $this->view->activeMenu = 'purchases';
    }

    protected function _formatBuyerDates($dates)
    {
        asort($dates, SORT_STRING);
        foreach ($dates as &$date) {
            $date = ' ' . $this->view->formatDate($date);
        }
        return $dates;
    }

    public function orderAction()
    {
        $itemId = $this->getRequest()->getParam('id');

        $item = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            $this->getSession()->addMessage('error', 'This order does not exist');
            $this->_redirect('admin/user/index');
        }

        $form = new Admin_Form_OrderItemAvailability();

        if ($item->getShippingMethodId() === Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
            if ($item->getSellerDates() != 'other') {
                $form->setDataRequired(true);

                $additionalValues =  array('not-selected' => ' Not selected', 'other' => ' Other date...',); // only on admin area
                $values = $this->_formatBuyerDates((array)$item->getBuyerDatesForSeller()) + $additionalValues;

                $form->getElement('delivery_date')
                    ->setMultiOptions($values)
                    ->addValidator('InArray', false, array(array_keys($values)));

                $form->getElement('set_pickup_date')->setDescription($item->getPickupBoroughName());
                $form->getElement('set_delivery_date')->setDescription($item->getDeliveryBoroughName());

                $form->getElement('telephone_number')->setValue($item->getProduct()->getPickupPhone());
            } else {

                $form->setDataRequired(true);

                $values = $this->_formatBuyerDates($item->getBuyerDatesForSeller());

                $form->getElement('delivery_date')
                    ->setMultiOptions($values)
                    ->addValidator('InArray', false, array(array_keys($values)));

                $isProductAvailableElement = new Zend_Form_Element_Hidden('is_product_available');

                $isProductAvailableElement->setRequired(true)
                    ->setValue(1)
                    ->addValidator('InArray', true, array(array(1)));

                $isProductAvailableElement->setDecorators(array('ViewHelper'));

                $form->addElement($isProductAvailableElement);

                $form->removeElement('telephone_number');
                $form->removeElement('building_type');
                $form->removeElement('flights_of_stairs');
                $form->removeElement('paperwork');
            }
        } else {
            $form->setDataRequired(false);
        }

        $request = $this->getRequest();

        if ($request->isPost()) {
            if ($item->getStatus() != Order_Model_Order::ORDER_STATUS_ON_HOLD && $item->getSellerDates() != 'other') {
                $this->getSession()->addMessage('error', 'Availability of this item is already confirmed.');
                $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
            }

            if ($this->getRequest()->getParam('delivery_date') == 'other') {
                $form->getElement('set_pickup_date')->setRequired(true);
                $form->getElement('set_pickup_time')->setRequired(true);
//                $form->getElement('set_delivery_date')->setRequired(true);
                $form->getElement('set_delivery_time')->setRequired(true);
            }
            $form->isValid($request->getPost());

            if ($form->isValid($request->getPost())) {
                try {
                    if ($form->getValue('is_product_available')) {

                        if ($item->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
                            $formDate = $form->getValue('delivery_date');

                            switch($formDate) {
                                case 'other':
                                    $setDeliveryDate = $form->getValue('set_delivery_date') ? $form->getValue('set_delivery_date') : $form->getValue('set_pickup_date');

                                    $sellerDate = date('Y-m-d', strtotime($form->getValue('set_pickup_date'))) . ' ' . $form->getValue('set_pickup_time');
                                    $buyerDate  = date('Y-m-d', strtotime($setDeliveryDate)) . ' ' . $form->getValue('set_delivery_time');
                                    break;
                                case 'not-selected':
                                    $sellerDate = 'not-selected';
                                    $buyerDate = 'not-selected';
                                    break;
                                default:
                                    /*  delivery_date param example: 2014-08-10 17-21|2014-08-10 8-12  */
                                    $dates = explode('|', $formDate);
                                    $buyerDate  = isset($dates[0]) ? $dates[0] : null;
                                    $sellerDate = isset($dates[1]) ? $dates[1] : null;
                                    break;
                            }

                            $deliveryOptions = array(
                                'building_type'     => $form->getValue('building_type'),
                                'flights_of_stairs' => $form->getValue('flights_of_stairs'),
                                'paperwork_info'    => $form->getValue('paperwork'),
                                'phone_number'      => $form->getValue('telephone_number'),
                            );
                        } else {
                            $sellerDate      = NULL;
                            $buyerDate      = NULL;
                            $deliveryOptions = array();
                        }

                        if ($item->confirmAvailability($sellerDate, $deliveryOptions, $buyerDate)) {
                            $order =  $item->getOrder();
                            $couponId = $order->getCouponId();
                            if ($couponId) {
                                $coupon = new Cart_Model_Coupon();
                                $coupon->getById($couponId);
                                $coupon->addUse($order->getUserId(), $order->getId());
                            }
                            $this->getSession()->addMessage('success', self::MESSAGE_ITEM_AVAILABLE);
                        } else {
                            $this->getSession()->addMessage('error', self::MESSAGE_REQUEST_ERROR);
                        }
                    } else {
                        if ($item->invalidateAvailability()) {
                            $this->getSession()->addMessage('success', self::MESSAGE_ITEM_NOT_AVAILABLE);
                        } else {
                            $this->getSession()->addMessage('error', self::MESSAGE_REQUEST_ERROR);
                        }
                    }
                } catch (Exception $ex) {
                    $this->_logException($ex);
                    $this->getSession()->addMessage('error', 'Cannot save pick up details. Please try again later.');
                }

                $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
            } else {
                $this->view->errorMessage = self::ERROR_MESSAGE_DATA_REQUIRED;
                $form->populate($request->getPost());
            }
        }

        $this->view->item    = $item;
        $this->view->session = $this->getSession();
        $this->view->form    = $form;
        $this->view->is_show_delivery = $this->getRequest()->getParam('is_show_delivery');
    }

    public function editSellerDateAction()
    {
        $request = $this->getRequest();

        $orderItemId = $request->getPost('id');

        $orderItem = new Order_Model_Item($orderItemId);
        $oldSellerDate = $orderItem->getSellerDates();

        try {
            if (!$orderItem->getId()) {
                throw new Exception('Order does not exist');
            }

            $date = strtotime($request->getPost('new_seller_date'));
            $time = $request->getPost('new_seller_time');

            if (!$date || !in_array($time, array('7-12', '12-16', '16-20'))) {
                throw new Exception('Invalid date selected');
            }

            $sellerDate = date('Y-m-d', $date) . ' ' . $time;

//        $orderItem->setBuyerDate($sellerDate);
            $orderItem->setSellerDates($sellerDate)->save();

            if ($oldSellerDate === 'not-selected' && $oldSellerDate !== $sellerDate ) {
                $mailer = new Notification_Model_Order_Confirmed();
                $mailer->toSeller($orderItem);
            }
        } catch (Exception $e) {
            $this->getSession()->addMessage('error', $e->getMessage());
        }

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $orderItem->getId()));
    }

    public function editBuyerDatesAction()
    {
        $request     = $this->getRequest();
        $orderItemId = $request->getParam('id');
        $orderItem   = new Order_Model_Item($orderItemId);
        $dates       = (array) $request->getPost('buyer_dates');
        $selectedBuyerDate = (array)$request->getPost('selected_buyer_date');
        $newSelectedBuyerDate = null;

        try {

            if (!$orderItem->getId()) {
                die('Order does not exist');
            }

            $oldBuyerDate = $orderItem->getBuyerDate();
            $newBuyerDates = array();
            if ($dates) {
                if (count($dates) != 3) {
                    throw new Exception('Select 3 dates');
                }
                foreach ($dates as $dateTime) {
                    if (!isset($dateTime['date'], $dateTime['time'])) {
                        throw new Exception('Select 3 dates & times');
                    }

                    $date = strtotime($dateTime['date']);
                    $time = $dateTime['time'];

                    if (!$date || !in_array($time, array('7-12', '12-16', '16-20'))) {
                        throw new Exception('Invalid date or time selected');
                    }

                    $newBuyerDates[] = date('Y-m-d', $date) . ' ' . $time;
                }

                $orderItem->setBuyerDates($newBuyerDates);
            }

            if ($selectedBuyerDate) {
                if (!isset($selectedBuyerDate['date'], $selectedBuyerDate['time'])) {
                    throw new Exception('Select 3 dates & times');
                }

                $date = strtotime($selectedBuyerDate['date']);
                $time = $selectedBuyerDate['time'];

                if (!$date || !in_array($time, array('7-12', '12-16', '16-20'))) {
                    throw new Exception('Invalid date or time selected');
                }

                $newSelectedBuyerDate = date('Y-m-d', $date) . ' ' . $time;
                $orderItem->setBuyerDate($newSelectedBuyerDate);
            }

            $orderItem->save();

            if ($selectedBuyerDate && $oldBuyerDate === 'not-selected' && $newSelectedBuyerDate !== $oldBuyerDate ) {
                $mailer = new Notification_Model_Order_Confirmed();
                $mailer->toBuyer($orderItem);
            }
        } catch (Exception $e) {
            $this->getSession()->addMessage('error', $e->getMessage());
        }
        $this->_helper->redirector('order', 'users', 'admin', array('id' => $orderItem->getId(), 'is_show_delivery' => $this->getRequest()->getParam('is_show_delivery')));
    }

    public function changePickupAddressAction()
    {
        $addressData = (array) $this->getRequest()->getPost('address');
        $itemId      = $this->getRequest()->getParam('id');
        $item        = new Order_Model_Item($itemId);
        $product     = $item->getProduct();
        $addressForm = new User_Form_Address();

        if (!$product->getId()) {
            throw new Exception('This product does not exist.');
        }

        $addressData['full_name'] = $product->getSeller()->getFullName();

        if ($addressForm->isValid($addressData)) {
            $address = new User_Model_Address();

            $address->addData($addressForm->getValues())
                ->setUserId($product->getUserId())
                ->setIsPrimary(0)
                ->save();

            $product->setPickUpAddressId($address->getId())
                ->setPickupAddressLine1($addressForm->getValue('address_line1'))
                ->setPickupAddressLine2($addressForm->getValue('address_line2'))
                ->setPickupCity($addressForm->getValue('city'))
                ->setPickupState($addressForm->getValue('state'))
                ->setPickupPostcode($addressForm->getValue('postcode'))
                ->setPickupPhone($addressForm->getValue('phone_number'))
                ->setPickupCountryCode($addressForm->getValue('country_code'))
                ->save();

            $this->getSession()->addMessage('success', 'Pick up address saved.');
        } else {
            $this->getSession()->addMessage('error', 'Opps, looks like some of the fields have errors or were left blank.');
        }

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    public function changeDeliveryAddressAction()
    {
        $addressData     = (array) $this->getRequest()->getPost('address');
        $itemId          = $this->getRequest()->getParam('id');
        $item            = new Order_Model_Item($itemId);
        $addressForm     = new User_Form_Address();

        $buildingTypeElement = new Zend_Form_Element_Select('building_type');
        $buildingTypeElement->setAttrib('name', 'shipping_address[building_type]')
            ->setRequired(true)
            ->setAttrib('class', 'form-field m-r0')
            ->setMultiOptions(array('elevator' => 'Elevator', 'walkup' => 'Walk-up'))
            ->setDescription('&nbsp;');
        $addressForm->addElement($buildingTypeElement);

        $numberOfStairsOptions = array('' => 'Number of flights');
        for ($i = 1; $i <= 7; $i++) {
            $numberOfStairsOptions[$i] = $i;
        }
        $addressParam =  $this->getRequest()->getParam('address');


        $numberOfStairsElement = new Zend_Form_Element_Select('number_of_stairs');
        $numberOfStairsElement->setAttrib('name', 'shipping_address[building_type]')
            ->setRequired(isset($addressParam['building_type']) && $addressParam['building_type'] == 'walkup')
            ->setAttrib('class', 'form-field m-r0')
            ->setMultiOptions($numberOfStairsOptions)
            ->setDescription('&nbsp;');
        $addressForm->addElement($numberOfStairsElement);


        $deliveryAddress = $item->getOrder()->getShippingAddress();
        $order = $item->getOrder();

        if (!$item->getId()) {
            throw new Exception('This product does not exist.');
        }

        $addressData['full_name'] = $item->getOrder()->getUser()->getFullName();

        if ($addressForm->isValid($addressData)) {
            $address = new User_Model_Address();

            $address->addData($addressForm->getValues())
                ->setUserId($item->getOrder()->getUser()->getId())
                ->setIsPrimary(0)
                ->save();

            $deliveryAddress->setUserAddressId($address->getId())
                ->setAddressLine1($addressForm->getValue('address_line1'))
                ->setAddressLine2($addressForm->getValue('address_line2'))
                ->setCity($addressForm->getValue('city'))
                ->setStateCode($addressForm->getValue('state'))
                ->setPostalCode($addressForm->getValue('postcode'))
                ->setTelephone($addressForm->getValue('phone_number'))
                ->setCountryCode($addressForm->getValue('country_code'))
                ->setBuildingType($addressForm->getValue('building_type'))
                ->setNumberOfStairs($addressForm->getValue('building_type') == 'walkup' ? $addressForm->getValue('number_of_stairs') : null)
                ->save();

            if (!$order->getShippingAddressId()) {
                $order->setShippingAddressId($deliveryAddress->getId());
                $order->save();
            }
            $this->getSession()->addMessage('success', 'Delivery address saved.');
        } else {
            $this->getSession()->addMessage('error', 'Opps, looks like some of the fields have errors or were left blank.');
        }

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId(), 'is_show_delivery' => $this->getRequest()->getParam('is_show_delivery')));
    }

    public function changeShippingInfoAction()
    {
        $itemId = $this->getRequest()->getParam('id');
        $item   = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            throw new Exception('This product does not exist.');
        }
        try {
            $item->setCarrierWorkNumber(trim($this->_getParam('carrier_work_number')))
                ->setShippingStatus($this->_getParam('shipping_status'))
                ->setFollowUp(trim($this->_getParam('follow_up')))
                ->save();
            $this->getSession()->addMessage('success', 'Shipping info has been saved.');
        } catch (Exception $e) {
            $this->getSession()->addMessage('error', $e->getMessage());
        }
        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    public function changeNotesAction()
    {
        $itemId = $this->getRequest()->getParam('id');
        $item   = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            throw new Exception('This product does not exist.');
        }
        try {
            $item->setNotes(trim($this->_getParam('notes')))
                ->save();
            $this->getSession()->addMessage('success', 'Notes has been saved.');
        } catch (Exception $e) {
            $this->getSession()->addMessage('error', $e->getMessage());
        }

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    public function changeShippingMethodAction()
    {
        $itemId = $this->getRequest()->getParam('id');
        $item   = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            throw new Exception('This order item does not exist.');
        }

        if ($item->getShippingMethodId() != 'pickup') {
            throw new Exception('Shipping method should be Pick Up.');
        }

        try {

            Httpful\Bootstrap::init();
            RESTful\Bootstrap::init();
            Balanced\Bootstrap::init();
            Balanced\Settings::init();

            $user = $item->getOrder()->getUser();
            $bp = $user->getBpCustomer();

            if ($bp == false) {
                throw new Exception('Payment cannot be made. User not have an account of Balanced Payment');
            }

            $amountInCents =  $item->getShippingPriceForOrderItem() * 100;

            if ($item->getStatus() == 'pending') {
                $debit = $bp->debit(
                    (int) floor($amountInCents),
                    $appears_on_statement_as = null,
                    $meta = null,
                    $description = 'Delivery by AptDeco partner'
                );

                if ($debit->status !== 'succeeded') {
                    throw new Exception('It looks like a card has been declined by a bank.');
                }
                $item->setAdditionalDebitsUri($debit->uri);
            } elseif ($item->getStatus() == 'holded') {
                $marketplace = Balanced\Marketplace::mine();
                $card = $bp->activeCard();
                $hold = $marketplace->holds->create(array(
                    "amount"     => (int) floor($amountInCents),
                    "description" => 'Delivery by AptDeco partner',
                    "source_uri" => $card->uri
                ));

                if (!$hold) {
                    throw new Exception('It looks like a card has been declined by a bank.');
                }
                $item->setAdditionalHoldUri($hold->uri);
            } else {
                throw new Exception('Shipping method change action available for only orders with statuses "Sold - In Progress", "Awaiting Seller Confirmation", "Sold in progress - pending delivery date"');
            }

            $item->setShippingMethodId('delivery')
                ->setShippingMethod('delivery')
                ->setShippingAmount($amountInCents / 100)
                ->save();


            $this->getSession()->addMessage('success', 'Shipping method has been changed.');
        } catch (Exception $e) {
            $this->getSession()->addMessage('error', $e->getMessage());
        }

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }


    public function sendEmailToCarrierAction()
    {
        $itemId = $this->getRequest()->getParam('id');
        $item   = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            throw new Exception('This product does not exist.');
        }

        $confirm = new Notification_Model_Order_Confirmed();

        if ($confirm->sendEmailToCarrier($item)) {
            $this->getSession()->addMessage('success', 'Email has been sent successfully to Dumbo');
        } else {
            $this->getSession()->addMessage('error', 'Error');
        }

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    protected function _logException($exception)
    {
        $fileName = APPLICATION_PATH . '/log/exception.log';

        $data = $this->prepareException($exception) . $this->prepareRequest() . $this->prepareUserInfo() . "\n";

        file_put_contents($fileName, $data, FILE_APPEND);
    }

    protected function prepareException($exception)
    {
        return date('Y-m-d H:i:s') . ': Unable to confirm item availability:' . "\n" . $exception->getMessage() . "\n" . $exception->getTraceAsString() . "\n";
    }

    protected function prepareRequest()
    {
        $str = "Request Parameters:\n";
        foreach ($this->getRequest()->getParams() as $param => $value) {
            $str .= $param . ': ' . $value . "\n";
        }
        return $str;
    }

    protected function prepareUserInfo()
    {
        $str = "User info:\n";
        $str .= "IP: " . $this->getRequest()->getClientIp() . "\n";
        $str .= "User agent: " . $_SERVER['HTTP_USER_AGENT'] . "\n\n";
        $str .= '*************************************************************' . "\n";
        return $str;
    }

    /**
     * Process login form
     */
    public function postAction()
    {
        $loginData = $this->getRequest()->getPost();

        try {
            $form = new Admin_Form_Login();

            if ($form->isValid($loginData)) {
                $this->getSession()->authenticate($form->getValues());
                $this->_redirect($this->getSuccessRedirect());
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Ikantam_Exception_Auth $exception) {
            $this->getSession()->addMessage('error', $exception->getMessage());
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to log in. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->getSession()->addFormData($loginData, 'email');
        $this->_redirect($this->getFailureRedirect());
    }

    protected function getSuccessRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-success');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultSuccessRedirect;
    }

    protected function getFailureRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-failure');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultFailureRedirect;
    }

    public function confirmdeliveryAction()
    {
        $itemId = $this->getRequest()->getParam('item_id');

        $item = new Order_Model_Item($itemId);

        $item->confirmDelivery();

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    public function sendEmailAction()
    {
        $result = array('success' => 'true');
        try {
            $data = $this->getRequest()->getPost();
            $mail = new Ikantam_Mail($data);
            $mail->send();
        } catch (Exception $ex) {
            $result['success'] = false;
        }

        exit(Zend_Json::encode($result));
    }

    public function deleteRestoreAction()
    {
        $userId   = $this->getRequest()->getParam('id');
        $status   = $this->getRequest()->getParam('delete');
        $user     = new User_Model_User($userId);
        $products = new Product_Model_Product_Collection();
        $products->getAllByUser($userId);


        foreach ($products as $product) {

            $product->delete();
        }


        if ($user->getId()) {
            $this->unpublishUserProducts($user);

            $user->setIsActive(0)
                ->setIsUserDeleted(1)->save();
        }
        $this->_helper->redirector('index', 'users', 'admin');
    }

    public function getInfoBeforeDeleteAction()
    {
        $products = new Product_Model_Product_Collection();
        $userId   = $this->getRequest()->getParam('id');

        $products->getByUserAndOrderStatus($userId, 'pending')
            ->getByUserAndOrderStatus($userId, 'holded');

        $result = array();

        foreach ($products as $product) {
            $result[] = array(
                'title'      => $product->getTitle(),
                'productUrl' => $product->getProductUrl(),
                'orderUrl'   => Ikantam_Url::getUrl('admin/users/order', array('id' => $product->getItemId())),
                'orderId'    => $product->getItemId()
            );
        }
        exit(Zend_Json::encode($result));
    }

    public function cancelOrderAction()
    {
        $itemId = $this->getRequest()->getParam('item_id');

        $item = new Order_Model_Item($itemId);

        if (!$item->getId()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        if (!$item->getIsReceived() && !$item->getIsDelivered() && $item->getStatus() == Order_Model_Order::ORDER_STATUS_PENDING) {

            try {
                $this->_refund($item);

                $payout = new Order_Model_Payout();
                $payout->getByItemId($item->getId());

                $item->setIsAvailable(0)
                    ->setIsProductAvailable(0)
                    ->setStatus(Order_Model_Order::ORDER_STATUS_CANCELED)
                    ->save();

                if ($item->getProduct()->getIsSold()) {
                    $item->getProduct()->setIsPublished(0)->save();
                }

                $payout->setIsActive(0)->save();
                $this->_sendCancelEmails($item);
            } catch (Exception $e) {
                $this->getSession()->addMessage('error', 'Order can\'t be canceled or refunded');
            }
        }

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    protected function _sendCancelEmails($item)
    {
        /* send emails */
        $settings = new Admin_Model_Settings();

        $buyer     = $item->getOrder()->getUser();
        $buyerName = $buyer->getFirstName();

        $seller     = $item->getProduct()->getSeller();
        $sellerName = $seller->getFirstName();

        $view = clone $this->view;

        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        $replaces     = array(
            '%product_title%',
            '%order_id%',
            '%seller_name%',
            '%buyer_name%',
        );
        $replacements = array(
            $item->getProductTitle(),
            $item->getId(),
            $sellerName,
            $buyerName
        );

        $recipients = array(
            'cancelled_order_email_to_buyer'  => $buyer->getMainEmail(),
            'cancelled_order_email_to_seller' => $seller->getMainEmail(),
        );

        foreach ($recipients as $_configName => $_email) {
            $view->content = str_replace($replaces, $replacements, $settings->getValue($_configName, 'text'));
            $subject       = str_replace($replaces, $replacements, $settings->getValue($_configName, 'subject'));

            if (empty($view->content) || empty($subject)) {
                continue;
            }

            $output = $view->render('order_cancelled/default.phtml');

            $toSend = array(
                'email'   => $_email,
                'subject' => $subject,
                'body'    => $output
            );

            $mail = new Ikantam_Mail($toSend);
            //$mail->sendCopy($output);
            $mail->send(null, true);
        }
    }

    /**
     * Updates user personal setting 'product_verification_available' with new status
     *
     * input parameters:
     * int user_id
     * int verification_status
     */
    public function verificationAction()
    {
        $user = new User_Model_User($this->getParam('user_id'));
        if ($user->isExists()) {
            $availableStatuses = array(
                Application_Model_User::VERIFICATION_VERIFIED,
                Application_Model_User::VERIFICATION_REJECTED,
            );
            $newStatus         = $this->getParam('verification_status');
            if (in_array($newStatus, $availableStatuses)) {
                $user->personalSettings('product_verification_available', $newStatus);
            }
        }

        $this->responseJSON(array(
            'new_status' => $user->personalSettings('product_verification_available')
        ))
            ->sendResponse();
        exit;
    }

    protected function _refund(Order_Model_Item_Abstract $item)
    {
        if ($item->getDebitsUri()) {
            Httpful\Bootstrap::init();
            RESTful\Bootstrap::init();
            Balanced2\Bootstrap::init();
            Balanced2\Settings::init();

            $debit = Balanced2\Debit::get($item->getDebitsUri());

            $refundAmount = (int) $debit->amount; //refund full amount of debit (in cents)

            $refunds = $debit->refunds->query()->all();

            if (is_array($refunds) && count($refunds) > 0) {
                foreach ($refunds as $refund) {
                    $refundAmount -= (int) $refund->amount;
                }
            }

            if ($refundAmount > 0) {
                $debit->refund($refundAmount, 'Refund for Order #' . $item->getId());
            }

            if ($item->getAdditionalDebitsUri()) {
                $additionalDebit = Balanced2\Debit::get($item->getAdditionalDebitsUri());

                $additionalRefundAmount = (int) $additionalDebit->amount; //refun
                $additionalDebit->refund($additionalRefundAmount, 'Refund for Shipping for Order #' . $item->getId());
            }
        }
    }

    public function setCheckPayoutAction()
    {
        $itemId = $this->getRequest()->getParam('item_id');
        $status = $this->getRequest()->getParam('status') ? 1 : 0;

        $item = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        if ($item->getPayout()->getIsProcessed()) {
            throw new Zend_Controller_Action_Exception('Seller has been already paid', 404);
        }

        $item->getPayout()->setIsPaidByCheck($status)->save();

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    public function createDisputeAction()
    {
        $itemId = $this->getRequest()->getParam('item_id');

        $item = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        if ($item->getPayout()->getIsProcessed()) {
            throw new Zend_Controller_Action_Exception('Seller has been already paid', 404);
        }

        $item->getPayout()
            ->setDisputeStatus(Order_Model_Payout::PAYOUT_DISPUTE_INITIATED)
            ->save();

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }


    public function changeDisputeSellerAction()
    {
        $itemId = $this->getRequest()->getParam('item_id');

        $item = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        if ($item->getPayout()->getIsProcessed()) {
            throw new Zend_Controller_Action_Exception('Seller has been already paid', 404);
        }

        if ($item->getPayout()->getDisputeStatus() !== Order_Model_Payout::PAYOUT_DISPUTE_INITIATED) {
            throw new Zend_Controller_Action_Exception('You should initiate a dispute case', 404);
        }

        $item->getPayout()
            ->setDisputeStatus(Order_Model_Payout::PAYOUT_DISPUTE_SELLER)
            ->save();

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    public function changeDisputeBuyerAction()
    {
        $itemId = $this->getRequest()->getParam('item_id');

        $item = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        if ($item->getPayout()->getIsProcessed()) {
            throw new Zend_Controller_Action_Exception('Seller has been already paid', 404);
        }

        if ($item->getPayout()->getDisputeStatus() !== Order_Model_Payout::PAYOUT_DISPUTE_INITIATED) {
            throw new Zend_Controller_Action_Exception('You should initiate a dispute case', 404);
        }

        $item->getPayout()
            ->setDisputeStatus(Order_Model_Payout::PAYOUT_DISPUTE_BUYER)
            ->save();

        $this->_refund2($item);

        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    public function saveAddressAction()
    {
        $response = array(
            'success'  => false,
            'message'  => '',
            'template' => ''
        );

        if ($this->getRequest()->isPost()) {
            $form = new Admin_Form_ShippingAddress();
            if ($form->isValid($this->getRequest()->getPost('shipping_address'))) {
                $address = new User_Model_Address();
                $address->addData($form->getValues());

                if ($address->getBuildingType() == 'elevator') {
                    $address->setNumberOfStairs(null);
                }

                $address->setIsPrimary(0);
                $address->setCountryCode('US')
                    ->save();

                $form = new Admin_Form_ShippingAddress();
                $form->populate($address->getData());

                $this->view->addresses = array($address);
                $this->view->addressesForm = array();
                $this->view->addressesForm[$address->getId()] = $form;
                $response['success'] = true;
                $response['template']  = $this->view->render('users/order/pickup_to_delivery/addresses_template.phtml');

            } else {
                $response['message'] = 'Looks like some fields are missing or left blank';
            }
        }
        $this->responseJSON($response)
            ->sendResponse();
        exit;
    }

    //Issue refund to buyer (less delivery fees)
    protected function _refund2(Order_Model_Item_Abstract $item)
    {
        if ($item->getDebitsUri()) {
            Httpful\Bootstrap::init();
            RESTful\Bootstrap::init();
            Balanced2\Bootstrap::init();
            Balanced2\Settings::init();

            $debit = Balanced2\Debit::get($item->getDebitsUri());

            $refundAmount = (int) $debit->amount; //refund full amount of debit (in cents)

            $refunds = $debit->refunds->query()->all();

            if (is_array($refunds) && count($refunds) > 0) {
                foreach ($refunds as $refund) {
                    $refundAmount -= (int) $refund->amount;
                }
            }

            $refundAmount -= $item->getShippingAmount() * 100;

            if ($refundAmount > 0) {
                $debit->refund($refundAmount, 'Refund for Order #' . $item->getId());
            }

            if ($item->getAdditionalDebitsUri()) {
                $additionalDebit = Balanced2\Debit::get($item->getAdditionalDebitsUri());

                $additionalRefundAmount = (int) $additionalDebit->amount; //
                $additionalDebit->refund($additionalRefundAmount, 'Refund for Shipping for Order #' . $item->getId());
            }
        }
    }

    /**
     * Unpublish all published user's products
     *
     * @param Application_Model_User $user
     * @return $this
     */
    protected function unpublishUserProducts(\Application_Model_User $user)
    {
        \Product_Model_Product_Collection::flexFilter()
            ->user_id('=', $user->getId())
            ->is_published('=', 1)
            ->apply()
            ->unpublishCollection();

        return $this;
    }
}
