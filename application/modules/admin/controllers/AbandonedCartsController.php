<?php

class Admin_AbandonedCartsController extends Ikantam_Controller_Admin
{

    protected $_defaultSuccessRedirect = 'admin/invite/index';
    protected $_defaultFailureRedirect = 'admin/login/index';

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect($this->getFailureRedirect());
        }
        $this->_helper->_layout->setLayout('admin');
    }

    public function indexAction()//users list
    {
        $request = $this->getRequest();
        $form = new Admin_Form_AbandonedCarts();
        $settings = new Admin_Model_Settings();
  
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                $values = array();
                foreach ($this->getAllParams() as $_name => $_value) {
                    $arr = explode('__', $_name);
                    if (count($arr) === 2) {
                        if (isset($values[$arr[0]])) {
                            $values[$arr[0]] += array($arr[1] => $_value);
                        }  else {
                            $values[$arr[0]] = array($arr[1] => $_value);
                        }
                    }
                }

                $settings->saveConfig($values);

                $this->_helper->redirector('index');
            }
        } else {
            $form->populate($settings->getValuesForAdminForm(array('abandoned_cart1', 'abandoned_cart2', 'abandoned_cart3'), array('hours', 'text', 'subject')));
        }

        $this->view->form = $form->setAction($this->_helper->url('index'));
    }

    public function blockAction()
    {
        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);
        if ($user->getId()) {
            $user->setIsActive(0)->save();
        }
        $this->_helper->redirector('index', 'users', 'admin');
    }

    public function unblockAction()
    {
        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);
        if ($user->getId()) {
            $user->setIsActive(1)->save();
        }
        $this->_helper->redirector('index', 'users', 'admin');
    }

    public function viewAction()
    {
        //view particular user
        $this->_forward("sales");
    }

    public function salesAction()
    {
        $itemsCountPerPage = 10;

        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);

        if (!$user->getId()) {
            $this->getSession()->addMessage('error', 'this user does not exist');
            $this->_redirect('admin/user/index');
        }

        $this->view->user = $user;

        $page = $this->getRequest()->getParam('page');

        $params = array('seller_id' => $user->getId());

        $products = new Admin_Model_Order_Item_Collection();
        $products->getAllItems($params);

        $totalResults = $products->getSize();

        $totalPages = (int) ceil($totalResults / $itemsCountPerPage);

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        
        if ($page < 1) {
            $page = 1;
        }
        
        $offset = ($page - 1) * $itemsCountPerPage;

        $params['offset'] = $offset;
        $params['limit']  = $itemsCountPerPage;

        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;

        $collection           = new Admin_Model_Order_Item_Collection();
        $this->view->products = $collection->getAllItems($params);
    }

    public function purchasesAction()
    {
        $itemsCountPerPage = 10;

        $userId = $this->getRequest()->getParam('id');
        $user   = new User_Model_User($userId);

        if (!$user->getId()) {
            $this->getSession()->addMessage('error', 'this user does not exist');
            $this->_redirect('admin/user/index');
        }

        $this->view->user = $user;

        $page = $this->getRequest()->getParam('page');

        $params = array('buyer_id' => $user->getId());

        $products = new Admin_Model_Order_Item_Collection();
        $products->getAllItems($params);

        $totalResults = $products->getSize();

        $totalPages = (int) ceil($totalResults / $itemsCountPerPage);

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }
        $offset = ($page - 1) * $itemsCountPerPage;

        $params ['offset'] = $offset;
        $params ['limit']  = $itemsCountPerPage;

        $this->view->currentPage = $page;
        $this->view->totalPages  = $totalPages;

        $collection           = new Admin_Model_Order_Item_Collection();
        $this->view->products = $collection->getAllItems($params);
    }

    public function orderAction()
    {
        $itemId = $this->getRequest()->getParam('id');

        $item = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            $this->getSession()->addMessage('error', 'This order does not exist');
            $this->_redirect('admin/user/index');
        }

        $this->view->item = $item;
    }

    /**
     * Process login form
     */
    public function postAction()
    {
        $loginData = $this->getRequest()->getPost();

        try {
            $form = new Admin_Form_Login();

            if ($form->isValid($loginData)) {
                $this->getSession()->authenticate($form->getValues());
                $this->_redirect($this->getSuccessRedirect());
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Ikantam_Exception_Auth $exception) {
            $this->getSession()->addMessage('error', $exception->getMessage());
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to log in. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->getSession()->addFormData($loginData, 'email');
        $this->_redirect($this->getFailureRedirect());
    }

    protected function getSuccessRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-success');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultSuccessRedirect;
    }

    protected function getFailureRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-failure');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultFailureRedirect;
    }

    public function confirmdeliveryAction()
    {
        $itemId = $this->getRequest()->getParam('item_id');

        $item = new Order_Model_Item($itemId);

        if ($item->getId() && !$item->getIsReceived() && !$item->getIsDelivered()) {

            $item->setIsReceived(1)
                    ->setIsDelivered(1)
                    ->setStatus(Order_Model_Order::ORDER_STATUS_COMPLETE)
                    ->save();

            $payout = new Order_Model_Payout();
            $payout->getByItemId($item->getId());

            if ($payout->getId() && is_null($payout->getIsProcessed())) {

                if ($item->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
                    $procTime = time() + 86400; //set 24-hour processing time
                } else {
                    $procTime = time(); //set immediate processing time
                }

                $payout->setIsProcessed(0)//set 0 for further processing by cron   
                        ->setProcessingTime($procTime)
                        ->save();

                $seller = $item->getProduct()->getSeller();

                if (!$seller->getMerchant()->getBankAccountUri() || !$seller->getMerchant()->getAccountUri()) {
                    //seller does not have payout account

                    if ($seller->getMainEmail()) {
                        $this->view->seller = $seller;
                        $output             = $this->view->render('email_reminder_payout_settings.phtml');

                        $toSend = array(
                            'email'   => $seller->getMainEmail(),
                            'subject' => 'Action Required: Your recent sale payout is pending',
                            'body'    => $output);

                        $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                        $mail->sendCopy($output);
                        $mail->send();
                    }
                }
            }
        }
        $this->_helper->redirector('order', 'users', 'admin', array('id' => $item->getId()));
    }

    public function sendEmailAction()
    {
        $result = array('success' => 'true');
        try {
            $data = $this->getRequest()->getPost();
            $mail = new Ikantam_Mail($data);
            $mail->send();
        } catch (Exception $ex) {
            $result['success'] = false;
        }

        exit(Zend_Json::encode($result));
    }

    public function deleteRestoreAction()
    {
        $userId   = $this->getRequest()->getParam('id');
        $status   = $this->getRequest()->getParam('delete');
        $user     = new User_Model_User($userId);
        $products = new Product_Model_Product_Collection();
        $products->getAllByUser($userId);


        foreach ($products as $product) {

            $product->delete();
        }


        if ($user->getId()) {
            $user->setIsActive(0)
                    ->setIsUserDeleted(1)->save();
        }
        $this->_helper->redirector('index', 'users', 'admin');
    }

    public function getInfoBeforeDeleteAction()
    {
        $products = new Product_Model_Product_Collection();
        $userId   = $this->getRequest()->getParam('id');

        $products->getByUserAndOrderStatus($userId, 'pending')
                ->getByUserAndOrderStatus($userId, 'holded');

        $result = array();

        foreach ($products as $product) {
            $result[] = array(
                'title' => $product->getTitle(),
                'productUrl' => $product->getProductUrl(),
                'orderUrl' => Ikantam_Url::getUrl('admin/users/order', array('id' => $product->getItemId())),
                'orderId' => $product->getItemId()
            );
        }
        exit(Zend_Json::encode($result));
    }

}
