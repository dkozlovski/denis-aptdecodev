<?php

class Admin_ReferersController extends Ikantam_Controller_Admin
{
    protected $_itemsPerPage = 10;
    
    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_helper->_layout->setLayout('admin');
        parent::init();
    }

    public function indexAction()
    {
        $request = $this->getRequest();
        $filters = array();

        if ($request->getParam('start_date')) {
            $startDate = strtotime($this->_getParam('start_date'));

            if ($startDate) {
                $filters['start_date'] = $startDate;
            }
        }

        if ($request->getParam('end_date')) {
            $endDate = strtotime($this->_getParam('end_date'));

            if ($endDate) {
                $filters['end_date'] = $endDate + 86400;
            }
        }

        $refererCollection = new Application_Model_Referer_Collection();
        $totalResults      = $refererCollection->countForAdmin($filters);

        list($currentPage, $totalPages, $offset, $limit) = $this->_getPaginationParams($totalResults);

        $this->view->start_date  = $request->getParam('start_date');
        $this->view->end_date    = $request->getParam('end_date');
        $this->view->referers    = $refererCollection->clear()->getForAdmin($filters, $offset, $limit);
        $this->view->currentPage = $currentPage;
        $this->view->totalPages  = $totalPages;
    }

    protected function _getPaginationParams($totalResults)
    {
        $currentPage = $this->getRequest()->getParam('page');

        $totalPages = ceil($totalResults / $this->_itemsPerPage);

        if ($currentPage > $totalPages) {
            $currentPage = $totalPages;
        }
        if ($currentPage < 1) {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * $this->_itemsPerPage;
        $limit  = $this->_itemsPerPage;

        return array($currentPage, $totalPages, $offset, $limit);
    }

}
