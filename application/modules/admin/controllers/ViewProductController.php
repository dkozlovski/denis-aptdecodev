<?php

class Admin_ViewProductController extends Ikantam_Controller_Admin
{

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('admin/login/index');
		}
        $this->_helper->_layout->setLayout('admin');
    }

    public function indexAction()
    {
        $productId = $this->getRequest()->getParam('id');

        $product = new Product_Model_Product();
        $product->getById($productId);

        if (!$product->getId() || !$product->getIsVisible()) {
            throw new Zend_Controller_Action_Exception('This product does not exist', 404);
        }

        $this->view->product = $product;
		
		$products = new Product_Model_Product_Collection();
		$products->getByUser($product->getSeller()->getId());
		$this->view->products = $products;
		$this->view->session = $this->getSession();
    }

}
