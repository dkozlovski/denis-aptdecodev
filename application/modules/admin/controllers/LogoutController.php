<?php

class Admin_LogoutController extends Ikantam_Controller_Admin
{

	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('admin/login/index');
		}
	}

	public function indexAction()
	{
		$this->getSession()->setAdminId(null);
		$this->_redirect('admin/login/index');
	}

}
