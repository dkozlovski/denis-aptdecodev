<?php

class Admin_CouponsController extends Ikantam_Controller_Admin
{

	protected $_defaultSuccessRedirect = 'admin/invite/index';
	protected $_defaultFailureRedirect = 'admin/login/index';

	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect($this->getFailureRedirect());
		}
		$this->_helper->_layout->setLayout('admin');
	}

	public function indexAction()
	{
        $params = array();
        
        
        $status = $this->getRequest()->getParam('status');
        $type = $this->getRequest()->getParam('type');
        $code = $this->getRequest()->getParam('code');
        $sortBy = $this->getRequest()->getParam('sort_by');
        
        if ($status == 'active') {
            $this->view->status = 'Active';
        } elseif ($status == 'used') {
            $this->view->status = 'Used';
        } elseif ($status == 'expired') {
            $this->view->status = 'Expired';
        } else {
            $this->view->status = 'All Statuses';
        }
        
        if ($type == 'percent') {
            $this->view->type = 'Percent Off';
        } elseif ($type == 'shipping') {
            $this->view->type = 'Free Shipping';
        } elseif ($type == 'fixed') {
            $this->view->type = 'Fixed Amount';
        }  else {
            $this->view->type = 'All Types';
        }
        
        if ($sortBy == 'expiry_date') {
            $this->view->sortBy = 'Expiry Date';
        } elseif ($sortBy == 'active') {
            $this->view->sortBy = 'Active First';
        }  else {
            $this->view->sortBy = 'Sort By';
        }
        
        $this->view->code = $code;

		$itemsCountPerPage = 10;
		
		$params['status'] = $status;
		$params['type'] = $type;
        $params['sortBy'] = $sortBy;
        $params['code'] = $code;
		
		
		$coupons = new Cart_Model_Coupon_Collection();
		$coupons->getAll($params);
		
		$totalResults = $coupons->getSize();
		
		$totalPages = (int) ceil($totalResults / $itemsCountPerPage);
        
		$page = $this->getRequest()->getParam('page');
        
		if ($page > $totalPages) {
			$page = $totalPages;
		}
		if ($page < 1) {
			$page = 1;
		}
		$offset = ($page - 1) * $itemsCountPerPage;

		$this->view->currentPage = $page;
		$this->view->totalPages = $totalPages;
		
		$collection = new Cart_Model_Coupon_Collection();
		$this->view->coupons = $collection->getAllPaginated($params, $offset, $itemsCountPerPage); 
        $this->view->session = $this->getSession();

	}
    
    public function newAction()
    {
        $type = $this->getRequest()->getPost('type');
        $couponLimit = (int) $this->getRequest()->getPost('coupon_limit');
        $userLimit = (int) $this->getRequest()->getPost('user_limit');
        $code = $this->getRequest()->getPost('code');

        if ($couponLimit < 1) {
            $couponLimit = 1;
        }
        if ($userLimit < 1) {
            $userLimit = 1;
        }


        if ($type != 'shipping') {
            if ($type != 'percent') {
                $type = 'fixed';
            }

            $amount = (float) $this->getRequest()->getPost('amount');

            if ($type == 'percent' && ($amount < 0 || $amount > 100)) {
                $this->getSession()->addMessage('error', 'Discount amount should be between 0 and 100 percent');
                $this->_redirect('admin/coupons/index');
            }
            if ($type == 'fixed' && $amount < 0) {
                $this->getSession()->addMessage('error', 'Discount amount should be greater than 0');
                $this->_redirect('admin/coupons/index');
            }
        } else {
            $amount = null;
        }

        $exDate = strtotime($this->getRequest()->getPost('valid_till'));

        if (!$exDate || $exDate < time()) {
            $this->getSession()->addMessage('error', 'Expiry date should be in the future');
            $this->_redirect('admin/coupons/index');
        }

        if (empty($code)) {
            $code = Ikantam_Math_Rand::getString(16, Ikantam_Math_Rand::ALPHA_NUMERIC_UPPERCASE);
        }

        $categoryId = $this->getParam('category_id');
        if (!$categoryId) {
            $categoryId = null;
        }

        $postcode = $this->getParam('postcode');
        if (!$postcode) {
            $postcode = null;
        }


        $minPrice = $this->getParam('min_price');
        if (!$minPrice) {
            $minPrice = null;
        }

        $maxPrice = $this->getParam('max_price');
        if (!$maxPrice) {
            $maxPrice = null;
        }

        $coupon = new Cart_Model_Coupon();
        $coupon->setCode($code)
            ->setType($type)
            ->setAmount($amount)
            ->setCreatedAt(time())
            ->setValidTill($exDate)
            ->setCouponLimit($couponLimit)
            ->setUserLimit($userLimit)
            ->setCategoryId($categoryId)
            ->setPostcode($postcode)
            ->setMinPrice($minPrice)
            ->setMaxPrice($maxPrice)
            ->setIsActive(1)
            ->save();

        $this->getSession()->addMessage('success', 'New coupon code successfully generated: ' . $coupon->getCode());
        $this->_redirect('admin/coupons/index');
    }
	
	public function purchasesAction()
	{
		$itemsCountPerPage = 10;
		
		$userId = $this->getRequest()->getParam('id');
		$user = new User_Model_User($userId);
		
		if (!$user->getId()) {
			$this->getSession()->addMessage('error', 'this user does not exist');
			$this->_redirect('admin/user/index');
		}
		
		$this->view->user = $user;
		
		$page = $this->getRequest()->getParam('page');
		
		$params = array('buyer_id' => $user->getId());
		
		$products = new Admin_Model_Order_Item_Collection();
		$products->getAllItems($params);
		
		$totalResults = $products->getSize();
		
		$totalPages = (int) ceil($totalResults / $itemsCountPerPage);
		
		if ($page > $totalPages) {
			$page = $totalPages;
		}
		if ($page < 1) {
			$page = 1;
		}
		$offset = ($page - 1) * $itemsCountPerPage;
		
		$params ['offset'] = $offset;
		$params ['limit'] = $itemsCountPerPage;
		
		$this->view->currentPage = $page;
		$this->view->totalPages = $totalPages;
		
		$collection = new Admin_Model_Order_Item_Collection();
		$this->view->products = $collection->getAllItems($params);
	}
	
	public function orderAction()
	{
		$itemId = $this->getRequest()->getParam('id');
		
		$item = new Admin_Model_Order_Item($itemId);
		
		if (!$item->getId()) {
			$this->getSession()->addMessage('error', 'This order does not exist');
			$this->_redirect('admin/user/index');
		}
		
		$this->view->item = $item;
	}

	/**
	 * Process login form
	 */
	public function postAction()
	{
		$loginData = $this->getRequest()->getPost();

		try {
			$form = new Admin_Form_Login();

			if ($form->isValid($loginData)) {
				$this->getSession()->authenticate($form->getValues());
				$this->_redirect($this->getSuccessRedirect());
			} else {
				$this->getSession()->addFormErrors($form->getFlatMessages());
			}
		} catch (Ikantam_Exception_Auth $exception) {
			$this->getSession()->addMessage('error', $exception->getMessage());
		} catch (Exception $exception) {
			$this->getSession()->addMessage('error', 'Unable to log in. Please, try again later');
			$this->logException($exception, $logParams = false);
		}

		$this->getSession()->addFormData($loginData, 'email');
		$this->_redirect($this->getFailureRedirect());
	}
    
    public function feeAction ()
    {
        $options = new Application_Model_Options();        
        
        $sellerConditions = $options->loadOption($options::SELLER)->getConditions();
        $buyerConditions = $options->loadOption($options::BUYER)->getConditions();
        
        $this->view->sellerConditions = $sellerConditions;
        $this->view->buyerConditions = $buyerConditions;
        $this->view->individualOptions = $options->getAllIndividualOptions();
    }    

	protected function getSuccessRedirect()
	{
		$url = $this->getRequest()->getParam('redirect-success');
		if (!empty($url)) {
			return urldecode($url);
		}
		return $this->_defaultSuccessRedirect;
	}

	protected function getFailureRedirect()
	{
		$url = $this->getRequest()->getParam('redirect-failure');
		if (!empty($url)) {
			return urldecode($url);
		}
		return $this->_defaultFailureRedirect;
	}
    
    private function __getFeeParams ($edit = false) {
        $rqst = $this->getRequest();
        $fee = $rqst->getParam('fee_val', false);
        $cond_value = $rqst->getParam('condition_val', false);
        $operator = $rqst->getParam('operator', false);
        $code = $rqst->getParam('code');
        if($fee === false || $cond_value === false || !$operator)
                                                                 exit;
                                                                 
        $result = array($fee, $cond_value, $operator, $code);
        if($edit) {
            $editData = $rqst->getParam('editData', false);
            if(!$editData)
                        exit;
            $result[] = array('operator' => $editData['operator'], 'value' => $editData['condition_val']);                        
        }                                                                 
        return $result;
    }
    
    public function addFeeConditionAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
                                    exit;
        

        list($fee, $cond_value, $operator, $code) = $this->__getFeeParams();
        
        $condition = array ('operator' => $operator, 'value' => $cond_value);
        $option = new Application_Model_Options();
        $option->getBy_code($code);
        $result = array('success' => false);
        try {
           $result['success'] = (bool) $option->addNewConditionValue($fee, $condition);
        } catch(Exception $ex) {
            $result['success'] = false;
            if($ex->getCode() == 23000) {                
                $result['msg'] = 'Condition already exists.';
            } else {
               $result['msg'] = 'Error occured. Try again later.'; 
            }
        }
        
        
        exit(Zend_Json::encode($result));
    }
    
    
    public function editFeeConditionAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
              throw new Zend_Controller_Action_Exception('Page not found', 404);

        list($fee, $cond_value, $operator, $code, $editData) = $this->__getFeeParams(true);
        $option = new Application_Model_Options();
        $result = array ('success' => false);
        try {
                $result['success'] = $option->editCondition($code, $editData, array('operator' => $operator, 'value' => $cond_value, 'fee' => $fee));
            } catch(Exception $ex) {
                $result['success'] = false;
                $result['msg'] = 'Error occured. Try again later.';
            }
            
         exit(Zend_Json::encode($result));                                           
    }
    
    public function editIndividualConditionAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
              throw new Zend_Controller_Action_Exception('Page not found', 404);


        $option = new Application_Model_Options();
        $code = $option::INDIVIDUAL;
        $userId = $this->getRequest()->getParam('id');
        $editData =  array('operator' => 'true', 'user' => $userId);
        $result = array ('success' => false);
        try {
                $result['success'] = $option->editCondition($code, $editData, array('operator' => $operator, 'value' => $cond_value, 'fee' => $fee));
            } catch(Exception $ex) {
                $result['success'] = false;
                $result['msg'] = 'Error occured. Try again later.';
            }
            
         exit(Zend_Json::encode($result));                                           
    }    
    
    public function deleteConditionAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
              throw new Zend_Controller_Action_Exception('Page not found', 404);
        $condition = $rqst->getParam('condition', array());
        $code = $rqst->getParam('code', '');
        $options = new Application_Model_Options();

        exit(Zend_Json::encode(array('success' => $options->deleteCondition($code, $condition))));                                    
    
    }
    
    public function testFeeAction ()
    {
        $code = $this->getRequest()->getParam('code');
        $sum = $this->getRequest()->getParam('sum');
        $options = new Application_Model_Options();
        try{
        $fee = $options->getFee($sum, $code);
        $sum = $sum - ($sum / 100 * $fee);
        } catch (Exception $ex){
            if($ex->getCode() == 7) {
                $fee = 'err';
                $sum = 'err (sum must be a number)'; throw $ex;
            }
        }
        exit(Zend_Json::encode(array('fee' => $fee, 'sum' => $sum)));
    } 
    
    public function getUserAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
           throw new Zend_Controller_Action_Exception('Page not found', 404);
           
        $id = $this->getRequest()->getParam('id');
        
        $user = new User_Model_User((int)$id);
        $result = array(
            'success' => (bool)$user->getId(),
            'name' => $user->getFullName(),
            'email' => $user->getMainEmail(),
            'profile_url' => $user->getProfileUrl(),
            'id' => $user->getId(),
        );
        exit(Zend_Json::encode($result));                                     
        
    }
    
    public function addIndividualFeeAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
           throw new Zend_Controller_Action_Exception('Page not found', 404);
           
        $userId = $rqst->getParam('id');
        $fee = $rqst->getParam('fee');
        $options = new Application_Model_Options();
        $result = array();
        try{
            $result['success'] = $options->addIndividualFee($userId, $fee);
            } catch(Exception $ex) {
               $result['success'] = false; 
            }
        exit(Zend_Json::encode($result));       
    } 
    

    public function editIndividualFeeAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
           throw new Zend_Controller_Action_Exception('Page not found', 404);
           
        $userId = $rqst->getParam('id');
        $fee = $rqst->getParam('fee');
        $options = new Application_Model_Options();
        
        exit(Zend_Json::encode(array('success' => $options->editIndividualFee($userId, $fee))));
                  
    } 
       
    public function deleteIndividualFeeAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
           throw new Zend_Controller_Action_Exception('Page not found', 404);
           
        $userId = $rqst->getParam('id');
        $code = $rqst->getParam('code');
        
        $options = new Application_Model_Options();
        $result = array();
        
        $result['success'] =  $options->deleteIndividualFee($userId, $code);
        
        exit(Zend_Json::encode($result));       
    }
    
    public function shippingFeeAction ()
    {
      $SFCollection = new Application_Model_ShippingFeeRule_Collection();
      $this->view->SFCollection = $SFCollection->getAll_(); 
    }
    
    public function addShippingFeeAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
           throw new Zend_Controller_Action_Exception('Page not found', 404);
           
        $SF = new Application_Model_ShippingFeeRule();
        $SF->setSum($rqst->getParam('sum'))
           ->setFee($rqst->getParam('fee'))
           ->setOperator($rqst->getParam('operator'));
           
       exit(Zend_Json::encode(array('success' => (bool)$SF->save()->getId(false)))); 
    } 
    
    public function deleteShippingFeeAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
           throw new Zend_Controller_Action_Exception('Page not found', 404);
        
        $pair = $rqst->getParam('condition');
           
        $SF = new Application_Model_ShippingFeeRule();
        
        $SF->getByPair($pair['operator'], $pair['value']);
        
        if(!$SF->isExists()) {
            exit(Zend_Json::encode(array('success' => false)));
        }
        
        $SF->delete();
        
        exit(Zend_Json::encode(array('success' => !$SF->isExists())));      
    }
    
    public function editShippingFeeAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
           throw new Zend_Controller_Action_Exception('Page not found', 404);
        
        $operator = $rqst->getParam('operator');
        $sum = $rqst->getParam('sum');
        $fee = $rqst->getParam('fee');
        
        $oldData = $rqst->getParam('editData');
        
        if($fee == $oldData['fee'] && $operator == $oldData['operator'] && $sum == $oldData['sum']) {
            exit(Zend_Json::encode(array('success' => true)));
        }
        
       $SF = new Application_Model_ShippingFeeRule();

       $SF->getByPair($oldData['operator'], $oldData['sum']);

        if(!$SF->isExists()) {

            exit(Zend_Json::encode(array('success' => false, 'msg' => 'Edit error: condition not exists.')));
        }
        
        try {
            $SF->setSum($sum)
               ->setFee($fee)
               ->setOperator($operator)
               ->save();
               } catch(Exception $ex) {
                if($ex->getCode() == 23000) {
                   exit(Zend_Json::encode(array('success' => false, 'msg' => 'Duplicate error: condition already exists.')));  
                }
                throw $ex;
               }
               
        exit(Zend_Json::encode(array('success' => $SF->isExists())));                         
    }
    
    public function testShippingFeeAction ()
    {
        $rqst = $this->getRequest();
        if(!$rqst->isXmlHttpRequest())
           throw new Zend_Controller_Action_Exception('Page not found', 404);
           
        $SFCollection = new Application_Model_ShippingFeeRule_Collection();        

        
        exit(Zend_Json::encode(array('fee' => $SFCollection->getShippingFee($rqst->getParam('sum'), $rqst->getParam('num')))));       
    }                                         

}
