<?php

/**
 * @author Aleksei Poliushkin
 * @copyright iKantam 2013/7/29
 */
class Admin_AssociationController extends Ikantam_Controller_Admin {
    
    protected function _initJsCss()
    {
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/jquery-ui.1.10.3.themes.smoothness.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/jquery.tagsinput.css'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery.tagsinput.min.js'));
    }
    
    public function indexAction ()
    {
        
    }
    
    public function addAction ()
    {
        if(!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }
        
        $ascn = new Manufacturer_Model_Association();
        
        $mid = $this->getParam('id');
        $association = $this->getParam('association');
        
        $ascn->setManufacturerId($mid)
             ->setAssociation($association)
             ->save();             
        
        $ascn->updateProducts();
        
        $manufacturer = new Application_Model_Manufacturer();
        $manufacturer->getByTitle($association);
        $manufacturer->delete();
        
            
        $this->_helper->json->sendJson(array('success' => true));             
                
    }
    
    public function removeAction ()
    {
        if(!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }
        
        $al = new Manufacturer_Model_Association();
        $al->getByManufacturerIdAscn($this->getParam('id'), $this->getParam('association'));
        if($al->isExists()) {
           $al->delete();  
        }
        
        exit;           
    }
    
    public function brandAutocompleteAction ()
    {
        if(!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }
        
        $searchVal = $this->getParam('title');
        $mc = new Application_Model_Manufacturer_Collection();
        $mc->findSimilar($searchVal);
        
        $this->_helper->json->sendJson($mc->getColumns(array('id', 'title')));
    }
    
    public function listAction()
    {
        if(!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        $ac = new Manufacturer_Model_Association_Collection();
        $ac->getBy_manufacturer_id($this->getParam('id'));
        
        $this->_helper->json->sendJson($ac->getColumn('association'));
            
    }
    
}