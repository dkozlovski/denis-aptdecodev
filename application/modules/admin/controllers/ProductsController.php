<?php

class Admin_ProductsController extends Ikantam_Controller_Admin
{

    protected $_expiryPeriod = 15; //When the product unpublish. (Days)
    protected $_itemsPerPage = 10;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_helper->_layout->setLayout('admin');
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/aptdeco.overlay.js'));
    }

    public function indexAction()
    {
        $this->_forward('manage');
    }

    public function manageAction()
    {

        $params  = $this->getRequest()->getParams();
        $filters = array();

        $form = new Admin_Form_ProductFilter();

        if ($form->isValid($params)) {
            $filters = $form->getValues();
        }

        $productCollection = new Product_Model_Product_Collection();
        $totalResults      = $productCollection->getCountForAdminList($filters);

        list($currentPage, $totalPages, $offset, $limit) = $this->_getPaginationParams($totalResults);

        $form->populate($params);

        $this->view->form        = $form->setAction(Ikantam_Url::getUrl('admin/products/manage'));
        $this->view->products    = $productCollection->clear()->getForAdminList($filters, $offset, $limit);
        $this->view->currentPage = $currentPage;
        $this->view->totalPages  = $totalPages;

        $queryParams = array();

        if ($this->getRequest()->getParam('search')) {
            $queryParams['search'] = $this->getRequest()->getParam('search');
        }
        if ($this->getRequest()->getParam('status')) {
            $queryParams['status'] = $this->getRequest()->getParam('status');
        }
        if ($this->getRequest()->getParam('category_id')) {
            $queryParams['category_id'] = $this->getRequest()->getParam('category_id');
        }
        if ($this->getRequest()->getParam('start_date')) {
            $queryParams['start_date'] = $this->getRequest()->getParam('start_date');
        }
        if ($this->getRequest()->getParam('end_date')) {
            $queryParams['end_date'] = $this->getRequest()->getParam('end_date');
        }

        $this->view->params = $queryParams;

        //save URI params in case some action called, e.g. approve product
        $this->getSession()->setAdminRedirectUrl($this->getRequest()->getRequestUri());
    }

    public function managePricingAction()
    {
        $request = $this->getRequest();
        $filters = array();
        foreach (array('date', 'product', 'category') as $_paramName) {
            if ($request->getParam($_paramName)) {
                $filters[$_paramName] = $request->getParam($_paramName);
            }
        }

        $category = new Category_Model_Category();

        if (!empty($filters['category'])) {
            $category->getById($filters['category']);
        }

        $products = new Product_Model_Product_Collection();
        $filter   = $products->getConfiguredFilterForManagePricing($filters);

        $paginator = new \Zend_Paginator(new \Ikantam_Paginator_Flex_Adapter($filter));
        $paginator->setCurrentPageNumber($this->getParam('page'))
                ->setItemCountPerPage(25);



        $categories                  = new Category_Model_Category_Collection();
        $this->view->categories      = $categories->getCategoriesForManagePriceFilter();
        $this->view->date            = isset($filters['date']) ? $filters['date'] : 'all';
        $this->view->currentCategory = $category;

        $this->view->paginator = $paginator;
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/bootstrap-confirmation.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery.tinyscrollbar.min.js'));
    }

    public function managePricingSuggestionsAction()// AJAX action
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->getRequest()->isPost()) {
            return;
        }

        $result = array();
        $search = $this->getParam('search');

        if (is_string($search) && strlen($search) > 0) {
            $products = new Product_Model_Product_Collection();
            $filter   = $products->getConfiguredFilterForManagePricing(array('search' => $search));

            $paginator = new \Zend_Paginator(new \Ikantam_Paginator_Flex_Adapter($filter));
            $paginator->setItemCountPerPage(5);

            foreach ($paginator as $product) {
                $result['products'][] = array(
                    'id'      => $product->getId(),
                    'title'   => $product->getTitle(),
                    'encoded' => urlencode($product->getTitle()),
                    'url'     => $product->getProductUrl()
                );
            }
        }

        echo Zend_Json::encode($result);
    }

    public function editPriceAction()
    {
        $product       = new \Product_Model_Product($this->getParam('product_id'));
        $price         = (float) $this->getParam('price');
        $availableTill = $this->getParam('available_till');
        $error         = false;
        if (!$product->isExists()) {
            $error = 'Product is not exist';
        } elseif ($price && $price > 0) {
            $product->setPrice($price)
                    ->setAdminEditedPrice(true)
                    ->setAvailableTill($availableTill)
                    ->save();

            $product->getEventDispatcher()->trigger('product.price.manage_pricing.lowered_by_admin', $product, array());
        } else {
            $error = 'Price must be greater than 0';
        }

        $this->responseJSON(array(
                    'product_id'               => $product->getId(),
                    'error'                    => $error,
                    'count'                    => \Product_Model_Product_Collection::countManagePricingProducts(),
                    'price'                    => $product->getPrice(),
                    'formatted_price'          => $this->view->currency($price, array('position' => Zend_Currency::LEFT, 'currency' => 'USD')),
                    'formatted_available_till' => date('d.m.Y', $product->getAvailableTill())
                ))
                ->sendResponse();
        exit;
    }

    public function approveAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $product   = new Product_Model_Product($productId);

        if ($product->getId() && $product->getIsVisible()) {
            $this->_approveProduct($product);
        }

        if ($this->getSession()->getAdminRedirectUrl()) {
            $ru = $this->getSession()->getAdminRedirectUrl();
            $this->getSession()->setAdminRedirectUrl(null);
            $this->_redirect($ru);
        } else {
            $this->_helper->redirector('manage', 'products', 'admin');
        }
    }

    public function rejectAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $product   = new Product_Model_Product($productId);

        if ($product->getId() && $product->getIsVisible()) {
            $this->_rejectProduct($product);
        }

        if ($this->getSession()->getAdminRedirectUrl()) {
            $ru = $this->getSession()->getAdminRedirectUrl();
            $this->getSession()->setAdminRedirectUrl(null);
            $this->redirect($ru);
        } else {
            $this->_helper->redirector('manage', 'products', 'admin');
        }
    }

    public function verifyAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $product   = new Product_Model_Product($productId);

        if ($product->getId() && $product->getIsVisible()) {
            $product->setIsVerified(true)->save();
        }

        if ($this->getSession()->getAdminRedirectUrl()) {
            $ru = $this->getSession()->getAdminRedirectUrl();
            $this->getSession()->setAdminRedirectUrl(null);
            $this->_redirect($ru);
        } else {
            $this->_helper->redirector('manage', 'products', 'admin');
        }
    }

    public function rejectverificationAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $product   = new Product_Model_Product($productId);

        if ($product->getId() && $product->getIsVisible()) {
            $product->setIsVerified(false)->save();
        }

        if ($this->getSession()->getAdminRedirectUrl()) {
            $ru = $this->getSession()->getAdminRedirectUrl();
            $this->getSession()->setAdminRedirectUrl(null);
            $this->_redirect($ru);
        } else {
            $this->_helper->redirector('manage', 'products', 'admin');
        }
    }

    public function massActionAction()
    {
        $request    = $this->getRequest();
        $actionType = $request->getPost('mass_action');

        if (in_array($actionType, array('publish', 'unpublish', 'approve', 'reject'))) {
            $products = (array) $request->getPost('products');

            foreach ($products as $productId => $value) {
                if ($value) {
                    $product = new Product_Model_Product($productId);

                    if (!$product->getId() || !$product->getIsVisible()) {
                        continue;
                    }

                    if ($actionType == 'publish' && $product->canBePublished()) {
                        $this->_publishProduct($product);
                    } elseif ($actionType == 'unpublish') {
                        $this->_unpublishProduct($product);
                    } elseif ($actionType == 'approve' && $product->getIsApproved() == 0) {
                        $this->_approveProduct($product);
                    } elseif ($actionType == 'reject' && $product->getIsApproved() == 0) {
                        $this->_rejectProduct($product);
                    }
                }
            }
        }

        if ($this->getSession()->getAdminRedirectUrl()) {
            $ru = $this->getSession()->getAdminRedirectUrl();
            $this->getSession()->setAdminRedirectUrl(null);
            $this->_redirect($ru);
        }

        $this->_helper->redirector('manage', 'products', 'admin');
    }

    public function publishAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $product   = new Product_Model_Product($productId);
        if ($product->getId() && $product->getIsVisible()) {
            $this->_publishProduct($product);
        }

        if ($this->getSession()->getAdminRedirectUrl()) {
            $ru = $this->getSession()->getAdminRedirectUrl();
            $this->getSession()->setAdminRedirectUrl(null);
            $this->_redirect($ru);
        } else {
            $this->_helper->redirector('manage', 'products', 'admin');
        }
    }

    public function unpublishAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $product   = new Product_Model_Product($productId);
        if ($product->getId() && $product->getIsVisible()) {
            $this->_unpublishProduct($product);
        }

        if ($this->getSession()->getAdminRedirectUrl()) {
            $ru = $this->getSession()->getAdminRedirectUrl();
            $this->getSession()->setAdminRedirectUrl(null);
            $this->_redirect($ru);
        } else {
            $this->_helper->redirector('manage', 'products', 'admin');
        }
    }

    public function republishAction()
    {
        $productId = $this->getRequest()->getParam('id');
        $product   = new Product_Model_Product($productId);
        if ($product->getId() && $product->getIsSold() && $product->getIsVisible()) {
            $this->_republish($product);
        }

        if ($this->getSession()->getAdminRedirectUrl()) {
            $ru = $this->getSession()->getAdminRedirectUrl();
            $this->getSession()->setAdminRedirectUrl(null);
            $this->_redirect($ru);
        } else {
            $this->_helper->redirector('manage', 'products', 'admin');
        }
    }

    /**
     * 
     * @param Product_Model_Product $product
     */
    protected function _publishProduct($product)
    {
        $expireAt = time() + ($this->_expiryPeriod * 86400);

        $product->setIsPublished(1)
                ->setExpireAt($expireAt)
                ->save();
    }

    /**
     * 
     * @param Product_Model_Product $product
     */
    protected function _unpublishProduct($product)
    {
        $product->setIsPublished(0)->save();
    }

    /**
     * 
     * @param Product_Model_Product $product
     */
    protected function _republish($product)
    {
        $expireAt = time() + ($this->_expiryPeriod * 86400);

        $product->setIsPublished(1)
                ->setExpireAt($expireAt)
                ->setIsSold(-1)//restore 1 item
                ->save();
    }

    protected function _approveProduct($product)
    {
        $user     = new Application_Model_User($product->getUserId());
        $expireAt = 0;
        if (!$product->getAvailableTill()) {
            $expireAt = time() + ($this->_expiryPeriod * 86400);
        }

        if ($user->personalSettings('product_verification_available') == Application_Model_User::VERIFICATION_VERIFIED) {
            $product->setIsVerified(1);
        }
        $product->setIsApproved(1)
                ->setApprovedAt(time())
                ->setExpireAt($expireAt)
                ->save();

        $product->selfTriggerEvent('approved.by.admin');

        $this->view->decision = 'approved';
        $this->view->product  = $product;
        $this->view->user     = $user;

        if ($user->getMainEmail()) {
            $mail = new Ikantam_Mail(array(
                'subject' => $product->getTitle() . ' is now live on AptDeco!',
                'email'   => $user->getMainEmail(),
                'body'    => $this->view->render('decision_letter_approved.phtml'),
            ));

            try {
                $mail->send(null, true);
            } catch (Exception $e) {
                
            }
        }
    }

    protected function _rejectProduct($product)
    {
        $product->setIsApproved(-1)->save();

        $mailer = new Notification_Model_Product_RejectedByAdmin();
        $mailer->sendEmails($product, $this->getParam('reason'));
    }

    protected function _getPaginationParams($totalResults)
    {
        $currentPage = $this->getRequest()->getParam('page');

        $totalPages = ceil($totalResults / $this->_itemsPerPage);

        if ($currentPage > $totalPages) {
            $currentPage = $totalPages;
        }
        if ($currentPage < 1) {
            $currentPage = 1;
        }

        $offset = ($currentPage - 1) * $this->_itemsPerPage;
        $limit  = $this->_itemsPerPage;

        return array($currentPage, $totalPages, $offset, $limit);
    }

}