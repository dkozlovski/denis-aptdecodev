<?php

class Admin_TextController extends Ikantam_Controller_Admin
{
    protected $_defaultSuccessRedirect = 'admin/invite/index';
    protected $_defaultFailureRedirect = 'admin/login/index';
    protected $_inboxTextAttributes = array(
        'text',
        'subject'
    );

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login');
        }
        $this->_helper->_layout->setLayout('admin');
        $this->view->session = $this->getSession();
    }


    public function inboxPageAction()
    {
        $request = $this->getRequest();
        $form = new Admin_Form_InboxPage();
        $settings = new Admin_Model_Settings();

        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                try {
                    $this->_saveSettings($form->getValues());
                    $this->getSession()->addMessage('success', 'Your settings have been saved');
                } catch (Exception $e) {
                    $this->getSession()->addMessage('error', $e->getMessage());
                }
                $this->_helper->redirector('inbox-page');
            }
        } else {
            $names = array();
            for ($i = 1; $i <= Admin_Form_InboxPage::COUNT_ELEMENTS; $i++) {
                $names[] = $form->getPrefixName() . $i;
            }

            $form->populate($settings->getValuesForAdminForm($names, $this->_inboxTextAttributes));
        }
        $this->view->form = $form->setAction($this->_helper->url('inbox-page'));
        $this->view->header = 'FAQ text to inbox page';
    }

    protected function _saveSettings(array $values)
    {
        $settings = new Admin_Model_Settings();
        $formattedValues = array();
        foreach ($values as $_name => $_value) {
            $arr = explode('__', $_name);
            if (count($arr) === 2) {
                if (isset($formattedValues[$arr[0]])) {
                    $formattedValues[$arr[0]] += array($arr[1] => $_value);
                }  else {
                    $formattedValues[$arr[0]] = array($arr[1] => $_value);
                }
            }
        }
        $settings->saveConfig($formattedValues);
    }

}
