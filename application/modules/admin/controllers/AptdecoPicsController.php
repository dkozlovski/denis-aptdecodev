<?php

class Admin_AptdecoPicsController extends Ikantam_Controller_Admin
{

    protected $_categories = null;
    protected $_brands     = null;
    protected $_product    = null;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_helper->_layout->setLayout('admin');
    }

    public function indexAction()
    {
        $productCollection = new Product_Model_Product_Collection();
        
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $products = (array) $this->getRequest()->getPost('products');
            
            $featuredProducts = array();
            
            foreach ($products as $productId) {
                if ($productId) {
                    $prod = new Product_Model_Product($productId);
                    if ($prod->getId()) {
                        $featuredProducts[] = $prod->getId();
                    }
                }
            }
            
            $productCollection->saveFeatured($featuredProducts);
        }
        
        $this->_redirect('admin/homepage/index');
    }

    public function addProductAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest())
            exit;

        $id = $this->getRequest()->getParam('id', false);

        if (!$id)
            exit;

        $success = false;
        $product = new Product_Model_Product();

        if (is_numeric($id)) {
            $product->getById((int) $id);
        } else {
            $product->getByPageUrl($id);
        }

        if ($product->getId()) {
            Admin_Model_Homepage_TmpObjects::getInstance()->save('product', $product->getId());
            $success = true;
            $text    = $product->getTitle();
            $url     = $product->getMainImage()->getS3Url(360, 300, 'frame');
            $id      = $product->getId();
        }

        exit(Zend_Json::encode(array('success' => $success, 'product' => $text, 'url' => $url, 'id' => $id)));
    }

}