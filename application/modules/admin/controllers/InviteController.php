<?php

class Admin_InviteController extends Ikantam_Controller_Admin
{

    private $_inviteForm;
    private $_invRequests;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_setInviteForm(new Admin_Form_Invite());
        $this->_invRequests = new Admin_Model_InviteRequest_Collection();
        parent::init();
        $this->_helper->_layout->setLayout('admin');
    }

    public function indexAction()
    {
        // action body
        $this->view->form    = $this->_getInviteForm();
        $this->view->session = $this->getSession();
    }

    public function handlingAction()
    {
        $inviteRequests = &$this->_invRequests;
        $page           = $this->getRequest()->getParam('page', 1);
        $search         = $this->getRequest()->getParam('search', false);
        $id             = $this->getRequest()->getParam('id', false);
        $sort           = (int) $this->getRequest()->getParam('sort');
        $sortBy         = null;
        if (!in_array($sort, array(1, 2, 3, 4))) {
            $this->view->sort = 0;
        } else {
            $this->view->sort = $sort;
            if ($sort === 1) {
                $sortBy = array('created_at DESC');
            }
            if ($sort === 2) {
                $sortBy = array('created_at ASC');
            }
            if ($sort === 3) {
                $sortBy = array('is_processed ASC');
            }
            if ($sort === 4) {
                $sortBy = array('is_processed DESC');
            }
        }

        $this->view->params = array(); // Params for pagination control (search)

        if ($search) {
            $paginator          = $inviteRequests->searchByEmail($search, 50, $page);
            $this->view->params = array('search' => $search);
        } else {
            $condition = array();
            if ($id)
                $condition = array(array('id = ?', $id));

            $paginator = $inviteRequests->getPaginator($page, 50, 100, $condition, $sortBy);
        }

        $this->view->requests  = $inviteRequests->getItems();
        $this->view->paginator = $paginator;
        $this->view->cpage     = $page;
        $this->view->isSend    = $this->getSession()->getIsSend();
        $this->view->session   = $this->getSession();
        $this->getSession()->setIsSend(false);
    }

    public function sendAction() //Use for send requested invitations
    {
        $invCount = 0;
        $invIds   = $this->getRequest()->getParam('invitations', false);
        if (is_array($invIds) && !empty($invIds)) {

            $invRequests = &$this->_invRequests;
            $invRequests->getById($invIds);
            foreach ($invRequests->getItems() as $invReq) {

                $email = $invReq->getEmail();

                $user = new User_Model_User();
                $user->getByEmail($email);

                if ($user->getId()) {
                    $this->getSession()->addMessage('success', 'User with email ' . $email . ' is already registered');
                    continue;
                }
                $invitation = new Application_Model_Invitation();



                $invitation->setEmail($invReq->getEmail())
                        ->setIsUsed(0)
                        ->setCode($invitation->generateCode())
                        ->setCreatedAt(time())
                        ->save();

                $this->view->code     = $invitation->getCode();
                $this->view->imageKey = Ikantam_Math_Rand::get62String(16);
                $output               = $this->view->render('invite_letter.phtml');

                if (!$invitation->getEmail()) {
                    continue;
                }

                $invCount++;

                $toSend = array(
                    'email' => $invitation->getEmail(),
                    'subject' => 'Welcome to AptDeco!',
                    'body' => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                $mail->send();

                $viewedEmail = new Application_Model_ViewedEmail();
                $viewedEmail->setEmail($invitation->getEmail())
                        ->setCode($this->view->imageKey)
                        ->save();

                $invReq->setIsProcessed(true)
                        ->save();
            }
            $this->getSession()->setIsSend(true);
        }
        $this->getSession()->addMessage('success', $invCount . ' intivations have been sent.');

        $this->_helper->redirector('handling', 'invite', 'admin', array(
            'page' => $this->getRequest()->getParam('cpage', 1),
        ));
    }

    public function postAction() //Use for send own invite
    {
        $formData = $this->getRequest()->getPost();
        $form     = $this->_getInviteForm();

        if ($form->isValid($formData)) {
            $email = $form->getValue('email');

            $user = new User_Model_User();
            $user->getByEmail($email);

            if (!$user->getId()) {
                $invitation = new Application_Model_Invitation();

                $invitation->setEmail($email)
                        ->setIsUsed(0)
                        ->setCode($invitation->generateCode())
                        ->setCreatedAt(time())
                        ->save();

                $this->view->code     = $invitation->getCode();
                $this->view->imageKey = Ikantam_Math_Rand::get62String(16);
                $output               = $this->view->render('invite_letter.phtml');

                if (!$invitation->getEmail()) {
                    $this->_redirect('admin/invite/index');
                }

                $toSend = array(
                    'email' => $invitation->getEmail(),
                    'subject' => 'Welcome to AptDeco!',
                    'body' => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                $mail->send();

                $viewedEmail = new Application_Model_ViewedEmail();
                $viewedEmail->setEmail($invitation->getEmail())
                        ->setCode($this->view->imageKey)
                        ->save();

                $this->getSession()->addMessage('success', 'Invitation has been sent to ' . $email);
            } else {
                $this->getSession()->addMessage('success', 'User with email ' . $email . ' is already registered');
            }
        }

        $this->_redirect('admin/invite/index');
    }

    public function suggestionsAction()// AJAX
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $serachEmail = $this->getRequest()->getParam('searchEmail', false);
        $result      = array();
        if ($serachEmail) {
            $this->_invRequests->searchByEmail($serachEmail);

            foreach ($this->_invRequests->getItems() as $inv) {
                $result[] = array('email' => $inv->getEmail(), 'id' => $inv->getId());
            }
        }

        echo Zend_Json::encode($result);
    }

    protected function _setInviteForm(\Zend_Form $form)
    {
        $form->setAction(Ikantam_Url::getUrl('admin/invite/post', null, true));
        $this->_inviteForm = $form;

        return $this;
    }

    protected function _getInviteForm()
    {
        return $this->_inviteForm;
    }

}
