<?php

class Admin_CronEmulatorController extends Ikantam_Controller_Admin
{

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_helper->_layout->setLayout('admin');

        defined('BASE_ORDER_URL') || define('BASE_ORDER_URL', 'http://www.test.aptdeco.com');
    }

    public function productAlertAction()
    {
        $collection = new Product_Model_Alert_Collection();
        $collection->check();
        Zend_Debug::dump($collection->getItems());
        foreach ($collection->getItems() as $_alert) {
            $_alert->sendEmails(true);
        }

        die('ok');
    }

    public function abandonedCartAction()
    {
        $cron = new Application_Model_Cron_AbandonedCarts();
        $cron->doCron();

        die('ok');
    }

    public function abandonedWishlistAction()
    {
        $cron = new Application_Model_Cron_AbandonedWishlist();
        $cron->doCron();

        die('ok');
    }

    public function sendEmailAction()
    {
        $item   = new Order_Model_Item(352);
        $buyer  = $item->getOrder()->getUser();
        $seller = $item->getProduct()->getSeller();

        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/modules/notification/views/scripts'));

        $payout = new Order_Model_Payout();
        $payout->getByItemId($item->getId());

        $view->product = $item->getProduct();
        $view->item    = $item;
        $view->buyer   = $buyer;
        $view->seller  = $seller;
        $view->payout  = $payout;

        if ($seller->getMainEmail()) {
            $output = $view->render('before_delivery/to_seller.phtml');

            $toSend = array(
                'email'   => 'stephylane57@gmail.com',
                'subject' => 'Reminder: Your Upcoming Pick up - ' . $item->getProduct()->getTitle() . ' - Please Read Carefully',
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            $mail->sendCopy($output);
            $mail->send();
        }
        die('ok');
    }

}