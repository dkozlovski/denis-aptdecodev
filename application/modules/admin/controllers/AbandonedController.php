<?php

class Admin_AbandonedController extends Ikantam_Controller_Admin
{
    protected $_defaultSuccessRedirect = 'admin/invite/index';
    protected $_defaultFailureRedirect = 'admin/login/index';
    protected $_abandonedAttributes = array(
        'hours',
        'text',
        'subject'
    );

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect($this->getFailureRedirect());
        }
        $this->_helper->_layout->setLayout('admin');
        $this->view->session = $this->getSession();
    }

    public function indexAction()
    {
        $this->forward('cart');
    }

    public function cartAction()
    {
        $this->_helper->viewRenderer('default');
        $request = $this->getRequest();
        $form = new Admin_Form_AbandonedCarts();
        $settings = new Admin_Model_Settings();

        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                try {
                    $this->_saveSettings($form->getValues());
                    $this->getSession()->addMessage('success', 'Your settings have been saved');
                } catch (Exception $e) {
                    $this->getSession()->addMessage('error', $e->getMessage());
                }
                $this->_helper->redirector('cart');
            }
        } else {
            $form->populate($settings->getValuesForAdminForm(array('abandoned_cart1', 'abandoned_cart2', 'abandoned_cart3'), $this->_abandonedAttributes));
        }
        $this->view->form = $form->setAction($this->_helper->url('cart'));
        $this->view->header = 'Abandoned Carts';
    }


    public function wishlistAction()
    {
        $this->_helper->viewRenderer('default');
        $request = $this->getRequest();
        $form = new Admin_Form_AbandonedWishlist();
        $settings = new Admin_Model_Settings();

        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                try {
                    $this->_saveSettings($form->getValues());
                    $this->getSession()->addMessage('success', 'Your settings have been saved');
                } catch (Exception $e) {
                    $this->getSession()->addMessage('error', $e->getMessage());
                }
                $this->_helper->redirector('wishlist');
            }
        } else {
            $form->populate($settings->getValuesForAdminForm(array('abandoned_wishlist1', 'abandoned_wishlist2', 'abandoned_wishlist3'), $this->_abandonedAttributes));
        }
        $this->view->form = $form->setAction($this->_helper->url('wishlist'));
        $this->view->header = 'Abandoned Wishlist';
    }

    protected function _saveSettings(array $values)
    {
        $settings = new Admin_Model_Settings();
        $formattedValues = array();
        foreach ($values as $_name => $_value) {
            $arr = explode('__', $_name);
            if (count($arr) === 2) {
                if (isset($formattedValues[$arr[0]])) {
                    $formattedValues[$arr[0]] += array($arr[1] => $_value);
                }  else {
                    $formattedValues[$arr[0]] = array($arr[1] => $_value);
                }
            }
        }
        $settings->saveConfig($formattedValues);
    }

    protected function getSuccessRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-success');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultSuccessRedirect;
    }

    protected function getFailureRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-failure');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultFailureRedirect;
    }

}
