<?php

class Admin_WidgetsController extends Ikantam_Controller_Admin
{

    protected $_categories = null;
    protected $_brands     = null;
    protected $_product    = null;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        
        $this->_helper->_layout->setLayout('admin');
    }

    public function asSeenInAction()
    {
        $request = $this->getRequest();
        $form    = new Widget_Form_AsSeenIn();

        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $widget = new Widget_Model_Homepage_AsSeenIn();

                $path       = APPLICATION_PATH . '/../tmp/widgets';
                $uploader   = new Ikantam_File_Uploader();
                $fileName   = $path . '/' . $form->getValue('image');
                $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;
                $uploader->uploadToS3($fileName, $bucketName . '/homepage/widgets/as-seen-in/' . basename($fileName));

                unlink($fileName);

                $widget->setTitle($form->getValue('title'))
                        ->setPageUrl($form->getValue('page_url'))
                        ->setImagePath('/homepage/widgets/as-seen-in/' . basename($fileName))
                        ->setSortOrder($form->getValue('sort_order'))
                        ->save();
            }
        }

        $this->_redirect('admin/homepage/index');
    }

    public function sliderAction()
    {
        $request = $this->getRequest();
        $form    = new Widget_Form_Slider();

        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $widget = new Widget_Model_Homepage_Slider();

                $path       = APPLICATION_PATH . '/../tmp/widgets';
                $uploader   = new Ikantam_File_Uploader();
                $fileName   = $path . '/' . $form->getValue('image');
                $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;
                $uploader->uploadToS3($fileName, $bucketName . '/homepage/widgets/slider/' . basename($fileName));

                unlink($fileName);

                $widget->setTitle($form->getValue('title'))
                        ->setType($form->getValue('type'))
                        ->setUrl($form->getValue('url'))
                        ->setSize($form->getValue('size'))
                        ->setImagePath('/homepage/widgets/slider/' . basename($fileName))
                        ->setSortOrder($form->getValue('sort_order'))
                        ->save();
            }
        }

        $this->_redirect('admin/homepage/index');
    }

    public function removeWidgetAction()
    {
        $id     = $this->getRequest()->getParam('id');
        $widget = new Widget_Model_Homepage_AsSeenIn($id);

        if ($widget->getId()) {
            $widget->delete();
        }

        $this->_redirect('admin/homepage/index');
    }

    public function removeSliderWidgetAction()
    {
        $id     = $this->getRequest()->getParam('id');
        $widget = new Widget_Model_Homepage_Slider($id);

        if ($widget->getId()) {
            $widget->delete();
        }

        $this->_redirect('admin/homepage/index');
    }

}
