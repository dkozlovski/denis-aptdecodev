<?php

class Admin_EditProductController extends Ikantam_Controller_Admin
{

    const ERROR_MESSAGE_DATA_REQUIRED       = 'Opps, looks like some of the fields have errors or were left blank. Please update all fields highlighted in red and publish again';
    const ERROR_MESSAGE_IMAGE_REQUIRED      = 'Image required';
    const SUCCESS_MESSAGE_PRODUCT_PUBLISHED = 'Your changes have been saved.';

    private $_productForm;
    private $_product;
    protected $_relatedObjects = array();
    protected $_js             = array(
        'js/jQuery-File-Upload-master/js/vendor/jquery.ui.widget.js',
        'js/jQuery-File-Upload-master/js/jquery.iframe-transport.js',
        'js/jQuery-File-Upload-master/js/jquery.fileupload.js',
        'js/input.spin.js',
        'js/aptdeco_image_admin.js',
        'js/plugins/iCheck/icheck.js',
        'js/iKantam/modules/aptdeco.overlay.js',
    );

    public function indexAction()
    {
        $product = $this->_getProduct();
        $form    = $this->_getProductForm($this->_getFormData());

        $form->removeDecorator('ViewScript');

        $this->view->form           = $form;
        $this->view->product        = $product;
        $this->view->productImages  = $product->getExtraAllImages();

        $this->_setAdditionalViewData();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form    = $this->_getProductForm();
            $product = $this->_getProduct();

            if (!$product->getIsPublished()) {
                $form->_setNotRequired();
            }

            foreach ($product->getExtraAllImages() as $image) {
                $image->setIsVisible(1)->save();
            }

            $mainImage = $product->getMainImage();

            if ($form->isValid($request->getPost()) && ($mainImage->getId() || (APPLICATION_ENV !== 'live' && APPLICATION_ENV !== 'testing'))) {
                $productData = $product->convertProductData($form);

                $pickupAddress = $this->_saveAddress();
                $product->addData($productData)
                        ->setIsVisible(1)
                        ->setPickUpAddressId($pickupAddress->getId())
                        ->setPickupAddressLine1($pickupAddress->getAddressLine1())
                        ->setPickupAddressLine2($pickupAddress->getAddressLine2())
                        ->setPickupCity($pickupAddress->getCity())
                        ->setPickupState($pickupAddress->getState())
                        ->setPickupPostalCode($pickupAddress->getPostalCode())
                        ->setPickupPostcode($pickupAddress->getPostcode())
                        ->setPickupPhone($pickupAddress->getPhoneNumber())
                        ->setPickupFullName($pickupAddress->getFullName())
                        ->save();


                // disable product expiration date on if period of availability was changed        
                $changes = $product->getExistChanges();
                if (isset($changes['available_till']) || isset($changes['available_from'])) {
                    $product->setExpireAt(0);
                }

                $product->save();

                //Antique collection?
                if ($this->getParam('is_antiques', false)) {
                    $this->_getRelatedObject('AntiqueSet')->addProducts($product);
                } else {
                    $this->_getRelatedObject('AntiqueSet')->removeProducts($product);
                }
                //Kid's corner collection?
                if ($this->getParam('is_kids_corner', false)) {
                    $this->_getRelatedObject('KidsCornerSet')->addProducts($product);
                } else {
                    $this->_getRelatedObject('KidsCornerSet')->removeProducts($product);
                }

                $message = self::SUCCESS_MESSAGE_PRODUCT_PUBLISHED;

                $this->getSession()->addMessage('success', $message);
                if ($this->getParam('publish')) {
                    $product->setIsPublished(1);
                }

                if ($this->getParam('approve')) {
                    $product->setIsApproved(1);
                }
                $product->save();

                if ($this->getSession()->getAdminRedirectUrl()) {
                    $rU = $this->getSession()->getAdminRedirectUrl();
                    $this->getSession()->setAdminRedirectUrl(null);
                    $this->_redirect($rU);
                } else {
                    $this->_helper->redirector('manage', 'products', 'admin');
                }
            } else {
                $form->populate($request->getPost());

                if (!$form->isValid($request->getPost())) {
                    $this->view->messages[] = self::ERROR_MESSAGE_DATA_REQUIRED;
                }

                if (!$product->isUploadedImageExist()) {
                    $this->view->messages[] = self::ERROR_MESSAGE_IMAGE_REQUIRED;
                }
            }
        }
    }

    public function init()
    {
        $this->_helper->_layout->setLayout('admin');

        $product = $this->_getProduct();

        $addresses = new User_Model_Address_Collection();
        $addresses->getLastAddressesByUserId($product->getUserId(), 3);

        //We show 3 last user addresses by default, but product could has address out of that range (3 last)
        //So it is need to be added
        if ($addressId = $product->getPickUpAddressId()) {
            //Exactly collection ;)
            $productAddress = new User_Model_Address_Collection();
            $productAddress->getFlexFilter()->id('=', $addressId)->apply(1);
            // Merging exclude duplicates
            $addresses = $addresses->merge($productAddress);
        }

        $form = new Product_Form_NewItem(array('addresses' => $addresses, 'product' => $product));
        $form->setAction(Ikantam_Url::getUrl('admin/edit-product/index', array('id' => $this->_getProduct()->getId())));

        if ($this->getParam('publish')) {
            $publish = new Zend_Form_Element_Hidden('publish');
            $form->addElement($publish);
        }

        $this->_setProductForm($form);
    }

    protected function _setProductForm(\Zend_Form $form)
    {
        $this->_productForm = $form;
        return $this;
    }

    /**
     * Get product form
     * 
     * @param array $data (Optional) - populate form with data if passed 
     * @return Ikantam_Form
     */
    protected function _getProductForm(array $data = null)
    {
        if ($data) {
            $this->_productForm->populate($data);
        }

        return $this->_productForm;
    }

    /**
     * Create and validate requested product
     * 
     * @return Product_Model_Product
     */
    protected function _getProduct()
    {
        if (!$this->_product) {
            $productId      = (int) $this->getParam('id', $this->getParam('product_id'));
            $product        = new Product_Model_Product($productId);
            /* if (!$product->isBelongsToUser($this->getSession()->getUser())) {
              $this->_helper->show404();
              } */
            $this->_product = $product;
        }

        return $this->_product;
    }

    /**
     * Retrieves and filter product data
     *
     * @return array
     */
    protected function _getFormData()
    {
        $product                = $this->_getProduct();
        $data                   = $product->getData();
        $data['price']          = (float) $product->getPrice();
        $data['original_price'] = (float) $product->getOriginalPrice();
        $data['width']          = (float) $product->getWidth();
        $data['height']         = (float) $product->getHeight();
        $data['depth']          = (float) $product->getDepth();

        $data['manufacturer']         = $this->_getRelatedObject('Manufacturer')->getTitle();
        $data['subcategory_id']       = $this->_getRelatedObject('SubCategory')->getId();
        $data['category_id']          = $this->_getRelatedObject('RootCategory')->getId();
        $data['is_antiques']          = (int) $this->_getRelatedObject('AntiqueSet')->hasProduct($product);
        $data['is_kids_corner']       = (int) $this->_getRelatedObject('KidsCornerSet')->hasProduct($product);
        $data['has_scratches']        = $product->getEAVAttributeValue('has_scratches');
        $data['cat_in_home']          = (int) $product->getEAVAttributeValue('exposed_cat');
        $data['dog_in_home']          = (int) $product->getEAVAttributeValue('exposed_dog');
        $data['is_pick_up_available'] = $product->getIsPickUpAvailable(0);
        $data['is_smoke']             = (int)!$product->getIsSmokeFree(0);

        $address                         = $this->_getRelatedObject('PickUpAddress');
        $addressData                     = $address->getData();
        $addressData['telephone_number'] = $address->getPhoneNumber($address->getTelephoneNumber());
        $addressData['postal_code']      = $address->getPostcode($address->getPostalCode());

        $addressData['selling_type'] = $product->getSellingType();

        $data['pickup_address'] = $addressData;

        $data['pickup_address']['absorb_shipping'] = (bool) $product->getShippingCover();
        $data['pickup_address']['amount']    = (int) $product->getShippingCover();
        $data['pickup_address']['is_pick_up_available'] = (int) $product->getIsPickUpAvailable();
        $data['pickup_address']['address_id'] = $address->getId();

        if ($product->getSellingType() === \Product_Model_Product::SELLING_TYPE_SET) {
            $data['qty'] = $product->getNumberOfItemsInSet(1);
        }

        if ($this->getParam('publish')) {
            $data['publish'] = true;
        }

        if ($data['available_from']) {
            $data['available_from'] = date('M d, Y', $data['available_from']);
        }

        if ($data['available_till']) {
            $data['available_till'] = date('M d, Y', $data['available_till']);
        }


        return $data;
    }

    /**
     * Pass to view necessary options
     *
     * @return void
     */
    protected function _setAdditionalViewData()
    {
        $addresses                   = new User_Model_Address_Collection();
        $options                     = $addresses->getJsonConfigByUserId($this->_getProduct()->getUserId());
        $this->view->pickupAddresses = Zend_Json::encode($options);

        $categoryCollection                         = new Category_Model_Category_Collection();
        $this->view->catsConfig                     = Zend_Json::encode($categoryCollection->getJsonConfig());
        $this->view->session                        = $this->getSession();
        $this->view->additionalImageUploaderOptions = Zend_Json::encode(array(
                    'edit_mode'  => 1,
                    'product_id' => $this->_getProduct()->getId()
        ));

        $this->view->messages       = array();
        $this->view->imageEndpoints = Product_Model_Image::getEndpoints('admin');
    }

    /**
     * Create manufacturer object for current product
     * 
     * @return Application_Model_Manufacturer
     */
    protected function _getManufacturer()
    {
        return new Application_Model_Manufacturer($this->_getProduct()->getManufacturerId());
    }

    /**
     * Create category object for current product
     *   
     * @return Category_Model_Category
     */
    protected function _getSubCategory()
    {
        return $this->_getProduct()->getCategory();
    }

    /**
     * Retrieves parent category for current product
     *
     * @return  Category_Model_Category
     */
    protected function _getRootCategory()
    {
        return $this->_getRelatedObject('SubCategory')->getParent();
    }

    /**
     * Creates antique set model
     * 
     * @return Application_Model_Set
     */
    protected function _getAntiqueSet()
    {
        $set = new Application_Model_Set();
        $set->getBy_name('Vintage + Antiques');

        return $set;
    }

    /**
     * Creates kid's corner set model
     * 
     * @return Application_Model_Set
     */
    protected function _getKidsCornerSet()
    {
        $set = new Application_Model_Set();
        $set->getBy_name('Kid\'s Corner');

        return $set;
    }

    /**
     * Retrieves related pickup address model
     *  
     * @return User_Model_Address 
     */
    protected function _getPickUpAddress()
    {
        return $this->_getProduct()->getPickUpAddress();
    }

    /**
     * Cache for related objects
     * 
     * @param  $name - method name without _get prefix: _getRootCategory = RootCategory
     * @return mixed object
     */
    protected function _getRelatedObject($name)
    {
        if (isset($this->_relatedObjects[$name])) {
            return $this->_relatedObjects[$name];
        } elseif (!method_exists($this, ($method = '_get' . $name))) {
            throw new Exception('Unable to get related object "' . $name . '". Because appropriate method not exist.');
        }

        $this->_relatedObjects[$name] = $this->{$method}();

        return $this->_relatedObjects[$name];
    }

    /**
 * Save new address
 *
 * @return address
 */
    protected function _saveAddress()
    {
        $pickupAddressForm = new Product_Form_AddressSubform;
        $pickupAddressData = $this->getRequest()->getPost('pickup_address');
        $address           = new User_Model_Address();

        if (!$pickupAddressForm->isValid($pickupAddressData)) {
            return $address;
        }

        $addressData                 = $pickupAddressForm->getValues('pickup_address');
        $addressData                 = $addressData['pickup_address'];
        $addressData['postcode']     = $addressData['postal_code'];
        $addressData['phone_number'] = $addressData['telephone_number'];


        $address->addData($addressData)
            ->setUserId($this->_getProduct()->getUserId())
            ->setIsPrimary(0)
            ->save();

        return $address;
    }

    /**
     * Admin edited address
     *
     */
    protected function _changeAddress($data)
    {
        $pickupAddressForm = new Product_Form_AddressSubform;
        $pickupAddressData = $data;
        $address           = new User_Model_Address($data['id']);

        if (!$pickupAddressForm->isValid($pickupAddressData)) {
          return false;
        }

        $addressData                 = $pickupAddressForm->getValues('pickup_address');
        $addressData                 = $addressData['pickup_address'];
        $addressData['postcode']     = $addressData['postal_code'];
        $addressData['phone_number'] = $addressData['telephone_number'];
        $address->addData($addressData);

        $address->save();
        return $address;

    }

    /* IMAGE EDIT */

    public function rotateImageAction()
    {

        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot rotate this image',
            'redirect' => false
        );

        $product = $this->_getProduct();
        $imageId = $this->getRequest()->getParam('image_id');
        $angle   = (int) ($this->getRequest()->getParam('angle') % 360);


        if (!in_array($angle, array(0, 90, 180, 270), true)) {
            $this->_helper->json($response);
        }

        $image = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $image->setDefaultAngle($angle)->save();

        if ($angle == 0 || ($angle == 90 && $image->get90Exists()) || ($angle == 180 && $image->get180Exists()) || ($angle == 270 && $image->get270Exists())) {
            $response['success'] = true;
            $response['error']   = false;
            $response['url']     = $image->getS3Url(500, 500, 'crop');

            $this->_helper->json($response);
        }

        if ($angle == 90 && !$image->get90Exists()) {
            $image->set90Exists(1)->save();
        }

        if ($angle == 180 && !$image->get180Exists()) {
            $image->set180Exists(1)->save();
        }

        if ($angle == 270 && $image->get270Exists()) {
            $image->set270Exists(1)->save();
        }

        $uploadDir    = APPLICATION_PATH . '/../tmp/product-images';
        $oldUploadDir = APPLICATION_PATH . '/../product-images';
        $imagePath    = $image->getS3Path();

        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;

        $oi = $cloudFrontUrl . '/product-images/' . $imagePath;

        $parts = explode('/', $imagePath);

        $tPath = $uploadDir;

        foreach ($parts as $part) {
            $tPath = $tPath . DIRECTORY_SEPARATOR . $part;
            if (!file_exists($tPath)) {
                mkdir($tPath, 0777);
            }
        }

        $file_path = $uploadDir . DIRECTORY_SEPARATOR . $imagePath;
        //КОСТЫЛЬ!
        if (!file_exists($file_path)) {
            $file_path = $oldUploadDir . DIRECTORY_SEPARATOR . $imagePath;
        }

        $img = new Ikantam_Image();
        $img->rotateOriginalImage($oi, $file_path, $angle);

        $img->createScaledImages($uploadDir, $imagePath, $product->getId(), $angle);

        $response['success'] = true;
        $response['error']   = false;
        $response['url']     = $image->getS3Url(500, 500, 'crop');

        $this->_helper->json($response);
    }

    public function deleteImageAction()
    {
        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot delete this image',
            'redirect' => false
        );


        $product = $this->_getProduct();
        $imageId = $this->getRequest()->getParam('image_id');
        $image   = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $image->delete();

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

    public function setAsDefaultImageAction()
    {
        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot set this image as default',
            'redirect' => false
        );

        $product = $this->_getProduct();

        $imageId = $this->getRequest()->getParam('image_id');
        $image   = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $product->unlockAllImages();

        $image->setDefaultImage($product->getId(), $image->getId());

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

    public function lockImageAction()
    {
        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot lock this image',
            'redirect' => false
        );

        $product = $this->_getProduct();

        $imageId = $this->getRequest()->getParam('image_id');
        $image   = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $product->unlockAllImages();

        $image->setIsLocked(1)->save();

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }
    
    public function unlockImageAction()
    {
        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot unlock this image',
            'redirect' => false
        );

        $product = $this->_getProduct();

        $imageId = $this->getRequest()->getParam('image_id');
        $image   = new Product_Model_Image($imageId);

        if (!$image->getId() || $image->getProductId() !== $product->getId()) {
            $this->_helper->json($response);
        }

        $product->unlockAllImages();

        $response['success'] = true;
        $response['error']   = false;
        $response['message'] = null;

        $this->_helper->json($response);
    }

    public function editAddressAction()
    {
        $data = $this->getParam('pickup_address');
        
        if ($address = $this->_changeAddress($data)) {
            $product = new Product_Model_Product($data['productId']);
            $product->setPickUpAddressId($address->getId())
                ->setPickupAddressLine1($address->getAddressLine1())
                ->setPickupAddressLine2($address->getAddressLine2())
                ->setPickupCity($address->getCity())
                ->setPickupState($address->getState())
                ->setPickupPostalCode($address->getPostalCode())
                ->setPickupPostcode($address->getPostcode())
                ->setPickupPhone($address->getPhoneNumber())
                ->setPickupFullName($address->getFullName())
                ->save();
        }

        $response = array('success' => (bool)$address);
        if ($address instanceof \User_Model_Address) {
            $response['html'] = $address->toHtml();
        }
        
        echo Zend_Json::encode($response);
        exit;
    }

}