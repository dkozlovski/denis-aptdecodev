<?php

class Admin_ShippingController extends Ikantam_Controller_Admin
{

    public static function compareDeliveryDates($a, $b)
    {
        $dateA = self::_prepareDeliveryDateForCompare($a->getSellerDates());
        $dateB = self::_prepareDeliveryDateForCompare($b->getSellerDates());
        return strcmp($dateA, $dateB);
    }

    protected static function _prepareDeliveryDateForCompare($string)
    {
        $string = substr($string, 0, 13); // 2014-02-26 16,  2014-02-26 8-
        // add 0 before 8 hours
        if ($string[12] == '-') {
            $string[12] = $string[11];
            $string[11] = '0';
        }
        return $string;
    }

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_helper->_layout->setLayout('admin');
        parent::init();
    }

    public function indexAction()
    {
        $this->view->items  = $this->_getItems();
        $this->view->events = $this->_getJsonEventsForCalendar();       
    }

    public function ajaxAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer('items');
        $this->view->items = $this->_getItems();
    }

    protected function _getItems()
    {
        $collection       = new Admin_Model_Order_Item_Collection();
        $dataObj          = new DateTime();
        $dataObj->modify('first day of this month');
        $defaultStartDate = $dataObj->format('Y-m-d');
        $dataObj->modify('last day of this month');
        $defaultEndDate   = $dataObj->format('Y-m-d');

        $startDate = strtotime($this->_getParam('start_date', $defaultStartDate));
        $endDate   = strtotime($this->_getParam('end_date', $defaultEndDate)) + 86399;


        $items = array();
        foreach ($collection->getTrackingList() as $_item) {
            /* @var $_item Admin_Model_Order_Item */
            if (!$_item->getOptions()) {
                continue;
            }
            $date = @$_item->getSellerDates();
            $date = substr($date, 0, 10);
            $time = strtotime($date);

            if ($startDate) {
                if ($startDate > $time) {
                    continue;
                }
            }

            if ($endDate) {
                if ($endDate < $time) {
                    continue;
                }
            }
            $items[] = $_item;
        }
        usort($items, array('Admin_ShippingController', 'compareDeliveryDates'));
        $form = new Admin_Form_ShippingFilter();
        $form->populate(array('start_date'=>date('M d, Y',$startDate), 'end_date'=>date('M d, Y',$endDate)));
        $this->view->form = $form;
        $this->view->startDate = $startDate;
        $this->view->endDate   = $endDate;
        return $items;
    }

    public function editCarrierAction()
    {
        $form     = new Admin_Form_CarrierEmail();
        $settings = new Admin_Model_Settings();
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            if ($form->isValid($post)) {
                $this->_saveSettings($form->getValues());
                $this->getSession()->addMessage('success', 'Your settings have been saved');
                ;
            } else {
                $this->getSession()->addMessage('error', 'Invalid data');
            }
        } else {
            $form->populate($settings->getValuesForAdminForm(array('carrier_email'), array('email', 'subject', 'text')));
        }
        $this->view->form    = $form;
        $this->view->session = $this->getSession();
    }

    public function changeAjaxAction()
    {
        $itemId = (int) $this->_getParam('id');
        $item   = new Admin_Model_Order_Item($itemId);

        if (!$item->getId()) {
            exit;
        }

        $allowedParams = array('carrier_work_number', 'shipping_status', 'follow_up');

        foreach ($allowedParams as $_param) {
            $item->setData($_param, $this->_getParam($_param));
        }

        $item->save();
        $this->view->item = $item;
        $this->view->layout()->disableLayout();
    }

    protected function _getJsonEventsForCalendar()
    {
        $items  = new Admin_Model_Order_Item_Collection();
        $items->getTrackingList();
        $config = array();

        foreach ($items as $_item) {
            /* @var $_item Admin_Model_Order_Item */

            if (!$_item->getOptions()) {
                continue;
            }

            $date    = @$_item->getSellerDates();
            $dateArr = explode(' ', $date);

            $hours = explode('-', array_pop($dateArr));

            $startDate = date('Y-m-d H:i:s', strtotime(@$dateArr[0] . ' ' . @$hours[0] . ':00:00'));
            $endDate   = date('Y-m-d H:i:s', strtotime(@$dateArr[0] . ' ' . @$hours[1] . ':00:00'));

            $color = 'orange';
            /*            if ($_item->getIsCarrierSentEmail()) {
              $color = null;
              } */
            if ($_item->getCarrierWorkNumber()) {
                $color = 'green';
            }

            $config[] = array(
                'id'     => $_item->getId(),
                'title'  => '#' . $_item->getOrderId() . ': ' . $_item->getProductTitle(),
                'start'  => $startDate,
                'end'    => $endDate,
                'allDay' => false,
                'url'    => $_item->getUrl(),
                'color'  => $color
            );
        }
        return json_encode($config);
    }

    protected function _initJsCss()
    {
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/additional.css'));

        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/fullcalendar/jquery-ui.min.css'));
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/fullcalendar/fullcalendar.css'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/fullcalendar.min.js'));
        parent::_initJsCss();
    }

    protected function _saveSettings(array $values)
    {
        $settings        = new Admin_Model_Settings();
        $formattedValues = array();
        foreach ($values as $_name => $_value) {
            $arr = explode('__', $_name);
            if (count($arr) === 2) {
                if (isset($formattedValues[$arr[0]])) {
                    $formattedValues[$arr[0]] += array($arr[1] => $_value);
                } else {
                    $formattedValues[$arr[0]] = array($arr[1] => $_value);
                }
            }
        }
        $settings->saveConfig($formattedValues);
    }

    public function exportAction()
    {
        $collection = new Admin_Model_Order_Item_Collection();

        $startDate = $this->_getParam('start_date');
        $endDate   = $this->_getParam('end_date');


        $items = array();
        foreach ($collection->getTrackingList() as $_item) {
            /* @var $_item Admin_Model_Order_Item */
            if (!$_item->getOptions()) {
                continue;
            }
            $sellersDate = @$_item->getSellerDates();
            $date        = substr($sellersDate, 0, 10);
            $time        = strtotime($date);

            if ($startDate) {
                if ($startDate > $time) {
                    continue;
                }
            }

            if ($endDate) {
                if ($endDate < $time) {
                    continue;
                }
            }
            $items[] = $_item;
        }
        usort($items, array('Admin_ShippingController', 'compareDeliveryDates'));

        $this->_generateXls($items, date("Ymd", $startDate), date("Ymd", $endDate));
    }

    /**
     * 
     * @param Admin_Model_Order_Item[] $items
     * @param type $startDate
     * @param type $endDate
     */
    protected function _generateXls($items, $startDate, $endDate)
    {
        $filename = sprintf('%s/../tmp/pick-up-and-shipping-%s-%s.xls', APPLICATION_PATH, $startDate, $endDate);

        $realPath = realpath($filename);

        if (false === $realPath) {
            touch($filename);
            chmod($filename, 0777);
        }

        $handle    = fopen(realpath($filename), "w");
        $finalData = array();

        foreach ($items AS $item) {

            $delOpts           = $item->getOrder()->getShippingAddress();
            $sellerOptions     = $item->getSellerDeliveryOptions();
            $pickUpPostalCode  = $item->getProduct()->getPickupPostcode();
            $dropOffPostalCode = $item->getOrder()->getShippingAddress()->getPostalCode();
            $itemShippingAmount = new Cart_Model_Totals_Shipping();
            

            $dbh = Application_Model_DbFactory::getFactory()->getConnection();
            $sql = 'SELECT `name` FROM `boroughs` '
                    . 'JOIN `postal_codes` ON `boroughs`.`id` = `postal_codes`.`borough_id` '
                    . 'WHERE `postal_codes`.`code` = :postal_code';

            $stmt = $dbh->prepare($sql);

            //
            $stmt->bindParam(':postal_code', $pickUpPostalCode);
            $stmt->execute();
            $res                 = $stmt->fetch(PDO::FETCH_ASSOC);
            $pickUpNeighbourhood = isset($res['name']) ? $res['name'] : '';

            $stmt->closeCursor();
            //
            $stmt->bindParam(':postal_code', $dropOffPostalCode);
            $stmt->execute();
            $res                  = $stmt->fetch(PDO::FETCH_ASSOC);
            $dropOffNeighbourhood = isset($res['name']) ? $res['name'] : '';

            $finalData[] = array(
                '', //Order: this is entered by the team once the route for the day is determined - so leave blank
                '', //Timing: this is entered by the team once the route for the day is determined so leave blank
                utf8_decode('Pick Up'), //Pick up / drop off: this column indicates whether it's a pick up from the seller or a drop off to the buyer.
                utf8_decode($item->getOrderId()), //PO #: this is the AptDeco work #
                utf8_decode($item->getProductTitle()), //Item: this is the product title
                utf8_decode($this->view->formatAddress($item->getProduct())), //Address: The customer address
                utf8_decode(str_replace('&ndash;', ' - ', $this->view->formatDate($item->getSellerDates(), 'short'))), //Approved time: the pick up or delivery window selected
                utf8_decode($item->getProduct()->getProductUrl()), //Link: the link to the product page
                utf8_decode($sellerOptions['building_type'] == 'walkup' ? $sellerOptions['flights_of_stairs'] : 'elevator'), //Flights: Elevator or # of stairs
                utf8_decode($pickUpNeighbourhood), //Area: neighbourhood (this can be entered manually by admin if auto populating is complicated (though with google API should be able to populate neighbourhood)
                utf8_decode($item->getNotes()), //Notes: any additional notes left by admin or customer
                utf8_decode($this->view->currency($itemShippingAmount->collectTotalsForOrderItem($item->getOrder()))), //The total delivery cost of this job excluding any discounts / offers. This will help us determine how much to pay each delivery crew.
                utf8_decode($item->getProduct()->getCondition()), //Condition
            );

            $finalData[] = array(
                '', //Order: this is entered by the team once the route for the day is determined - so leave blank
                '', //Timing: this is entered by the team once the route for the day is determined so leave blank
                utf8_decode('Drop Off'), //Pick up / drop off: this column indicates whether it's a pick up from the seller or a drop off to the buyer.
                utf8_decode($item->getOrderId()), //PO #: this is the AptDeco work #
                utf8_decode($item->getProductTitle()), //Item: this is the product title
                utf8_decode($this->view->formatAddress($item->getOrder()->getShippingAddress())), //Address: The customer address
                utf8_decode(str_replace('&ndash;', ' - ', $this->view->formatDate($item->getBuyerDate(), 'short'))), //Approved time: the pick up or delivery window selected
                utf8_decode($item->getProduct()->getProductUrl()), //Link: the link to the product page
                utf8_decode(($delOpts->getBuildingType() == 'walkup') ? $delOpts->getNumberOfStairs() : 'elevator'), //Flights: Elevator or # of stairs
                utf8_decode($dropOffNeighbourhood), //Area: neighbourhood (this can be entered manually by admin if auto populating is complicated (though with google API should be able to populate neighbourhood)
                utf8_decode($item->getNotes()), //Notes: any additional notes left by admin or customer
                utf8_decode($this->view->currency($itemShippingAmount->collectTotalsForOrderItem($item->getOrder()))), //The total delivery cost of this job excluding any discounts / offers. This will help us determine how much to pay each delivery crew.
                utf8_decode($item->getProduct()->getCondition()), //Condition
            );
        }

        foreach ($finalData AS $finalRow) {
            fputcsv($handle, $finalRow, "\t");
        }

        fclose($handle);

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $this->getResponse()->setRawHeader("Content-Type: application/vnd.ms-excel; charset=UTF-8")
                ->setRawHeader("Content-Disposition: attachment; filename=" . basename($filename))
                ->setRawHeader("Content-Transfer-Encoding: binary")
                ->setRawHeader("Expires: 0")
                ->setRawHeader("Cache-Control: must-revalidate, post-check=0, pre-check=0")
                ->setRawHeader("Pragma: public")
                ->setRawHeader("Content-Length: " . filesize($filename))
                ->sendResponse();

        readfile($filename);
        unlink($filename);
        exit();
    }

}