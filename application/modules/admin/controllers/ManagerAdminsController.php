<?php

class Admin_ManagerAdminsController extends Ikantam_Controller_Admin
{

    public function init()
    {
        $this->_helper->_layout->setLayout("admin");
    }

    public function indexAction()
    {
        $manager            = new Admin_Model_Admin_Manager();
        $this->view->admins = $manager->getAllAdmins();
    }

    public function addAdminAction()
    {

        $form = new Admin_Form_AddAdmin();

        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->getRequest()->getPost())) {

                $manager           = new Admin_Model_Admin_Manager();
                $manager->fill($this->getRequest()->getPost());
                $invitation = new Application_Model_Invitation();
                $password          = $invitation->generateCode().time();
                $manager->password = $password;
                $manager->save();
                $code = $invitation->generateCode();
                $invitation->createInvitationAdmin(array(
                    'admin_id' => $manager->id,
                    'email' => $manager->email,
                    'code' => $code,
                    'is_used'=> 0
                ));
                
                $this->view->code = $code;
                $output           = $this->view->render('invite_admin_letter.phtml');
                
                $toSend = array(
                    'email' => $manager->email,
                    'subject' => 'Invitation to sign up as administrator',
                    'body' => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                $mail->send(null, true);
                
                $this->redirect("admin/manager-admins");
            }
        }

        $this->view->form = $form;
    }

    public function editAdminAction()
    {
        $form             = new Admin_Form_AddAdmin();
        $manager          = new Admin_Model_Admin_Manager($this->getRequest()->getParam("id"));
        $form->populate($manager->populate());
        $this->view->form = $form;
        if ($this->getRequest()->isPost()) {

            if ($form->isValid($this->getRequest()->getPost())) {
                $manager->fill($this->getRequest()->getPost());
                $pas               = new Ikantam_Crypt_Password_Bcrypt();
                $password          = $pas->create($this->getRequest()->getPost("password"));
                $manager->password = $password;
                $manager->save();
                $this->redirect("admin/manager-admins");
            }
        }
    }

    public function deleteAdminAction()
    {
        $manager = new Admin_Model_Admin_Manager($this->getRequest()->getParam("id"));
        $manager->del();
        $this->redirect("admin/manager-admins");
    }

}