<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 18.06.14
 * Time: 15:19
 */

class Admin_NotificationsController extends Ikantam_Controller_Admin
{
    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect($this->getFailureRedirect());
        }
        $this->_helper->_layout->setLayout('admin');
        $this->view->session = $this->getSession();
    }

    public function wishlistPromoAction()
    {
        $this->_helper->viewRenderer('default');
        $request = $this->getRequest();
        $form = new Admin_Form_EmailNotificationWishlist;
        $settings = new Admin_Model_Settings();

        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                try {
                    $settings->saveConfig(
                        'wishlist_notification_price_reduced',
                        array('text' => $form->getValue('text'))
                    );
                    $settings->saveConfig(
                        'wishlist_notification_price_reduced',
                        array('subject' => $form->getValue('subject'))
                    );
                    $settings->saveConfig(
                        'wishlist_notification_sale_window_changed',
                        array('text' => $form->getValue('sw_text'))
                    );
                    $settings->saveConfig(
                        'wishlist_notification_sale_window_changed',
                        array('subject' => $form->getValue('sw_subject'))
                    );
                    $this->getSession()->addMessage('success', 'Your settings have been saved');
                } catch (Exception $e) {
                    $this->getSession()->addMessage('error', $e->getMessage());
                }
                $this->_helper->redirector('wishlist-promo');
            }
        } else {
            $text = $settings->getValue('wishlist_notification_price_reduced', 'text');
            $subject = $settings->getValue('wishlist_notification_price_reduced', 'subject');
            $swText = $settings->getValue('wishlist_notification_sale_window_changed', 'text');
            $swSubject = $settings->getValue('wishlist_notification_sale_window_changed', 'subject');

            $form->populate(array(
                    'text' => $text,
                    'subject' => $subject,
                    'sw_text' => $swText,
                    'sw_subject' => $swSubject,
                ));
        }
        $this->view->form = $form->setAction(Ikantam_Url::getUrl('admin/notifications/wishlist-promo'));
        $this->view->header = 'Email notification for users who added product to their wishlist. When seller lowered the price.';
        $this->view->header .= '<br> You could use shortcodes: %first_name%, %full_name%, %price%, %product_title%';
    }

    public function sellerPriceLoweredAction()
    {
        $this->_helper->viewRenderer('default');
        $request = $this->getRequest();
        $form = new Admin_Form_EmailNotificationManagePrice;
        $settings = new Admin_Model_Settings();

        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                try {
                    $settings->saveConfig(
                        'email_notification_to_seller_price_lowered',
                        array('text' => $form->getValue('text'))
                    );
                    $settings->saveConfig(
                        'email_notification_to_seller_price_lowered',
                        array('subject' => $form->getValue('subject'))
                    );
                    $this->getSession()->addMessage('success', 'Your settings have been saved');
                } catch (Exception $e) {
                    $this->getSession()->addMessage('error', $e->getMessage());
                }
                $this->_helper->redirector('seller-price-lowered');
            }
        } else {
            $text = $settings->getValue('email_notification_to_seller_price_lowered', 'text');
            $subject = $settings->getValue('email_notification_to_seller_price_lowered', 'subject');


            $form->populate(array(
                    'text' => $text,
                    'subject' => $subject,
                ));
        }
        $this->view->form = $form->setAction(Ikantam_Url::getUrl('admin/notifications/seller-price-lowered'));
        $this->view->header = 'Email notification for sellers when admin lowered price.';
        $this->view->header .= '<br> You could use shorcodes: %first_name%, %full_name%, %price%, %product_title%, %old_price%';
    }

    public function sellerPriceLoweringSuggestionAction()
    {
        $this->_helper->viewRenderer('default');
        $request = $this->getRequest();
        $form = new Admin_Form_EmailNotificationManagePrice;
        $settings = new Admin_Model_Settings();
        $settingName = 'email_notification_to_seller_price_lowering_suggestion';

        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                try {
                    $settings->saveConfig(
                        $settingName,
                        array('text' => $form->getValue('text'))
                    );
                    $settings->saveConfig(
                        $settingName,
                        array('subject' => $form->getValue('subject'))
                    );
                    $this->getSession()->addMessage('success', 'Your settings have been saved');
                } catch (Exception $e) {
                    $this->getSession()->addMessage('error', $e->getMessage());
                }
                $this->_helper->redirector('seller-price-lowering-suggestion');
            }
        } else {
            $text = $settings->getValue($settingName, 'text');
            $subject = $settings->getValue($settingName, 'subject');


            $form->populate(array(
                    'text' => $text,
                    'subject' => $subject,
                ));
        }
        $this->view->form = $form->setAction(Ikantam_Url::getUrl('admin/notifications/seller-price-lowering-suggestion'));
        $this->view->header = '<br> You could use shorcodes: %first_name%, %full_name%, %price%, %product_title%';
    }
} 