<?php

class Admin_OrderController extends Ikantam_Controller_Admin {

    public function init() {
        if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('admin/login/index');
		}
        $this->_helper->_layout->setLayout('admin');
        parent::init();
    }

    public function historyAction() {
        $form = new Admin_Form_SearchOrder();


//------if order status updated  
        $getData = $this->_request->getQuery();
        $postData = $this->getRequest()->getPost();
        $session = $this->getSession();

        $this->view->form = $form->populate($getData);

        $orderModel = new Admin_Model_Order();
        $this->view->ordersCount = $orderModel->getAllCount();

        $orderItemModel = new Admin_Model_Order_Item();
        $this->view->itemsCount = $orderItemModel->getAllCount();

        if (!empty($postData['status']) && !empty($postData['order_list'])) {
            foreach ($postData['order_list'] as $orderId) {
                $itemsCollection = new Order_Model_Item_Collection();
                $items = $itemsCollection->getByOrderId($orderId);
                $count = 0;

                if ($postData['status'] == 'completed') {
                    $status = 'completed';
                    foreach ($items as $item) {
                        $item->setIsDelivered(1)->save();
                        $count++;
                    }
                }
                if ($postData['status'] == 'inprogress') {
                    $status = 'in proogress';
                    foreach ($items as $item) {
                        $item->setIsDelivered(0)->save();
                        $count++;
                    }
                }
            }
            $session->addMessage('success', $count . '`s status marked as "' . $status . '"');
        }
        $this->view->messages = $session;

        $orderCollection = new Admin_Model_Order_Collection();
        if (isset($getData['search_status']) || !empty($getData['search_text'])) {
//------searching orders 
            $statusType = '';
            if ($getData['search_status'] == '0')
                $statusType = 'In progress';
            if ($getData['search_status'] == '1')
                $statusType = 'Completed';
            $orders = $orderCollection->searchOrders($getData['search_text'], $statusType);
        } else {

//------get all orders
            $orders = $orderCollection->getAll();
            foreach ($orders as $order) {
                $order->getShippingMethodId();
                $order->getStatus();
            }
        }

//-------------Search
        /* if ($getData['search_status'] != '' || $getData['search_text'] != '') {
          //populate form
          //$searchOrder->populate($getData);

          $searchOrdersCollection = new Admin_Model_Order_Collection();
          //$searchOrdersCollection->clear();
          /*foreach ($orders as $order) {
          if (($getData['search_status'] == 0) && ($order->getStatus() == 'In progress')) {
          if (($getData['search_text'] != '') &&
          ($order->IsSearchedOrderByText($order->getId(), $getData['search_text']))) {
          $searchOrdersCollection->addItem($order);
          }
          if ($getData['search_text'] == '') {
          $searchOrdersCollection->addItem($order);
          }
          //if (($getData['search_text'] != 0) && ($order->
          //      ->get$getData['search_text'] != 0))
          }
          if (($getData['search_status'] == 1) && ($order->getStatus() == 'Completed')) {
          if (($getData['search_text'] != '') &&
          ($order->IsSearchedOrderByText($order->getId(), $getData['search_text']))) {
          $searchOrdersCollection->addItem($order);
          }
          if ($getData['search_text'] == '') {
          $searchOrdersCollection->addItem($order);
          }
          }
          }
          $statusType = ($getData['search_status'] == '0') ? 'In progress' : 'Completed';

          $searchOrdersCollection->searchOrders($getData['search_text'], $statusType);
          $orders = $searchOrdersCollection;
          } */
//-------My Paginator       
        /* $per_page = 1;
          $num_page = 2;
          //$page = $this->_getParam('page', 1);
          $start_row = $page * $per_page - 1;
          //(!empty($_GET['p']))? intval($_GET['p']): 0;
          //$start_row = 55;
          $total = $orders->getSize();
          if ($start_row < 0)
          $start_row = 0;
          if ($start_row > $total)
          $start_row = $total;

          echo $this->pagination($total,$per_page,$num_page,$start_row,''); */
//-------Paginator        
        $page = $this->_getParam('page', 1);
        $ordersArray = array();
        foreach ($orders as $order) {
            $ordersArray[] = $order->getData();
        }

        $paginator = Zend_Paginator::factory($ordersArray);
        $paginator->setItemCountPerPage(2);
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(5);
        $this->view->paginator = $paginator;

        //$this->view->form = $searchOrder;
    }

    /* function pagination($total, $per_page, $num_links, $start_row, $url = '') {
      //Получаем общее число страниц
      $num_pages = ceil($total / $per_page);

      //if ($num_pages == 1) return '';
      //Получаем количество элементов на страницы
      $cur_page = $start_row;

      //Если количество элементов на страницы больше чем общее число элементов
      // то текущая страница будет равна последней
      if ($cur_page > $total) {
      $cur_page = ($num_pages - 1) * $per_page;
      }

      //Получаем номер текущей страницы
      $cur_page = floor(($cur_page / $per_page) + 1);

      //Получаем номер стартовой страницы выводимой в пейджинге
      $start = (($cur_page - $num_links) > 0) ? $cur_page - $num_links : 0;
      //Получаем номер последней страницы выводимой в пейджинге
      $end = (($cur_page + $num_links) < $num_pages) ? $cur_page + $num_links : $num_pages;

      $output = '<span class="ways">';

      //Формируем ссылку на предыдущую страницу

      if ($cur_page != 1) {
      $i = $start_row - $per_page;
      if ($i <= 0)
      $i = 0;
      $output .= '<a href="' . $url . '?p=' . $i . '">&lt</a><i></i>';
      }
      else {

      $output .='<span>&lt<i></i></span>';
      }

      $output .= '<span class="divider"></span>';
      //Формируем ссылку на следующую страницу
      // Формируем список страниц с учетом стартовой и последней страницы   >
      for ($loop = $start; $loop <= $end; $loop++) {
      $i = ($loop * $per_page) - $per_page;

      if ($i >= 0) {
      if ($cur_page == $loop) {
      //Текущая страница
      $output .= '<span>' . $loop . '</span>';
      } else {

      $n = ($i == 0) ? '' : $i;

      $output .= '<a href="' . $url . '?p=' . $n . '">' . $loop . '</a>';
      }
      }
      }

      if ($cur_page < $num_pages) {
      $output .= '<a href="' . $url . '?p=' . ($cur_page * $per_page) . '">&gt</a><i></i>';
      } else {
      $output .= '<span>&gt<i></i></span>';
      }

      $output .= '</span><br/>';

      return '<div class="wrapPaging">' . $output . '</div>';
      } */

    public function purchaseAction() {

        $orderModel = new Admin_Model_Order();
        $this->view->ordersCount = $orderModel->getAllCount();

        $orderItemModel = new Admin_Model_Order_Item();
        $this->view->itemsCount = $orderItemModel->getAllCount();

        $userId = $this->getRequest()->getParam('user_id');

        $user = new Application_Model_User($userId);
        //$user->setViewProfileLink($user->getViewProfileLink());

        /* $phone = $user->getPrimaryPhone();
          if ($phone) $user->setPrimaryPhone($phone); */
//---------OR
        $phoneModel = new User_Model_Phone();
        $phone = $phoneModel->getPrimaryByUserId($userId);
        $user->setPrimaryPhone($phone->getPhone());


        $address = $user->getPrimaryAddress();
        if ($address)
            $user->setPrimaryAddress($address);
//---------OR
        /* $address = new User_Model_Address();
          $address->getPrimaryByUserId($userId); */
        $this->view->user = $user;

        $itemCollection = new Admin_Model_Order_Item_Collection();
        $items = $itemCollection->getUserPurchase($userId);

        foreach ($items as $item) {
            $currentProduct = new Product_Model_Product($item->getProductId());
            $item->setProductUrl($currentProduct->getProductUrl());

            $imageModel = new Product_Model_Image();
            $imageModel->getMainByProductId($item->getProductId());
            $item->setPath($imageModel->getPath());

            $user = new Application_Model_User($item->getUserId());
            $item->setViewProfileLink($user->getViewProfileLink());

            //$item->setFullName($user->getFullName());

            $category = new Category_Model_Category($item->getCategoryId());
            $parentCategory = $category->getParent();
            $item->setCategoryTitle($parentCategory->getTitle());
        }

        //$this->view->products = $items;
//-------Paginator        
        $page = $this->_getParam('page', 1);
        $itemsArray = array();
        foreach ($items as $item) {
            $itemsArray[] = $item->getData();
        }

        $paginator = Zend_Paginator::factory($itemsArray);
        $paginator->setItemCountPerPage(2);
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(5);
        $this->view->paginator = $paginator;
    }

    public function salesAction() {

        $orderModel = new Admin_Model_Order();
        $this->view->ordersCount = $orderModel->getAllCount();

        $orderItemModel = new Admin_Model_Order_Item();
        $this->view->itemsCount = $orderItemModel->getAllCount();

        $userId = $this->getRequest()->getParam('user_id');

        $user = new Application_Model_User($userId);
        //$user->setViewProfileLink($user->getViewProfileLink());

        /* $phone = $user->getPrimaryPhone();
          if ($phone) $user->setPrimaryPhone($phone); */
//---------OR
        $phoneModel = new User_Model_Phone();
        $phone = $phoneModel->getPrimaryByUserId($userId);
        $user->setPrimaryPhone($phone->getPhone());

        $address = $user->getPrimaryAddress();
        if ($address)
            $user->setPrimaryAddress($address);
//---------OR
        /* $address = new User_Model_Address();
          $address->getPrimaryByUserId($userId); */
        $this->view->user = $user;

        $itemCollection = new Admin_Model_Order_Item_Collection();
        $items = $itemCollection->getUserSales($userId);

        foreach ($items as $item) {
            $currentProduct = new Product_Model_Product($item->getProductId());
            $item->setProductUrl($currentProduct->getProductUrl());

            $imageModel = new Product_Model_Image();
            $imageModel->getMainByProductId($item->getProductId());
            $item->setPath($imageModel->getPath());

            $user = new Application_Model_User($item->getUserId());
            $item->setViewProfileLink($user->getViewProfileLink());

            $category = new Category_Model_Category($item->getCategoryId());
            $parentCategory = $category->getParent();
            $item->setCategoryTitle($parentCategory->getTitle());
        }
        //$this->view->products = $items;   
//-------Paginator        
        $page = $this->_getParam('page', 1);
        $itemsArray = array();
        foreach ($items as $item) {
            $itemsArray[] = $item->getData();
        }

        $paginator = Zend_Paginator::factory($itemsArray);
        $paginator->setItemCountPerPage(2);
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(5);
        $this->view->paginator = $paginator;
    }

    public function viewAction() {

        $session = $this->getSession();

        $orderModel = new Admin_Model_Order();
        $this->view->ordersCount = $orderModel->getAllCount();

        $orderItemModel = new Admin_Model_Order_Item();
        $this->view->itemsCount = $orderItemModel->getAllCount();

//-----if status is update
        $postData = $this->getRequest()->getPost();
        if (!empty($postData['status'])) {

            $item = new Order_Model_Item($postData['item_id']);

            if ($postData['status'] == 'completed') {
                $status = 'completed';
                $item->setIsDelivered(1)->save();
            }
            if ($postData['status'] == 'inprogress') {
                $status = 'in proogress';
                $item->setIsDelivered(0)->save();
            }
            $session->addMessage('success', 'Product #' . $item->getProductId() . ' status marked as "' . $status . '"');
        }

        $this->view->messages = $session;






//------select all items in order
        $orderId = $this->getRequest()->getParam('order_id');
        $this->view->orderId = $orderId;

        $itemCollection = new Admin_Model_Order_Item_Collection();
        $items = $itemCollection->getItemsByOrderId($orderId);
        $order = new Order_Model_Order($orderId);
        $buyer = new Application_Model_User($order->getUserId());

        foreach ($items as $item) {
            $product = new Product_Model_Product($item->getProductId());
            $item->setProductLink($product->getProductLink());
            $item->setPath($product->getMainImageUrl());
//----------OR
            /* $imageModel = new Product_Model_Image();
              $image = $imageModel->getMainByProductId($item->getProductId());
              $item->setPath($image->getPath());
             */

            $seller = new Application_Model_User($item->getUserId());
            $item->setSellerViewProfileLink($seller->getViewProfileLink());

            $item->setBuyerViewProfileLink($buyer->getViewProfileLink());

            $addressModel = new Admin_Model_Order_Address();
            $shippingAddress = $addressModel->getAddressByOrderId($item->getOrderId(), 'shipping');
            $item->setShippingAddress($shippingAddress);
        }
        $this->view->items = $items;
    }

    public function productAction() {

        $orderModel = new Admin_Model_Order();
        $this->view->ordersCount = $orderModel->getAllCount();

        $orderItemModel = new Admin_Model_Order_Item();
        $this->view->itemsCount = $orderItemModel->getAllCount();

        $itemCollection = new Admin_Model_Order_Item_Collection();
        $items = $itemCollection->getAllProducts();

        foreach ($items as $item) {
            $product = new Product_Model_Product($item->getProductId());
            $item->setProductUrl($product->getProductUrl());

            $imageModel = new Product_Model_Image();
            $imageModel->getMainByProductId($item->getProductId());
            $item->setPath($imageModel->getPath());

            $buyer = new Application_Model_User($item->getBuyerId());
            //$item->setBuyerId($buyer->getBuyerId());
            $item->setBuyerFullName($buyer->getFullName());

            $seller = new Application_Model_User($item->getSellerId());
            //$item->setSellerId($buyer->getSellerId());
            $item->setSellerFullName($seller->getFullName());

            $category = new Category_Model_Category($item->getCategoryId());
            $parentCategory = $category->getParent();
            $item->setCategoryTitle($parentCategory->getTitle());
        }

//-------Paginator        
        $page = $this->_getParam('page', 1);
        $itemsArray = array();

        foreach ($items as $item) {
            $itemsArray[] = $item->getData();
        }

        $paginator = Zend_Paginator::factory($itemsArray);
        $paginator->setItemCountPerPage(2);
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(5);
        $this->view->paginator = $paginator;

        //$this->view->products = $items;
    }

    public function cancelledOrderEmailsSettingAction()
    {
        $request = $this->getRequest();
        $form = new Admin_Form_CancelledOrderEmails();
        $settings = new Admin_Model_Settings();

        if ($request->isPost()) {
            $data = $request->getPost();
            if ($form->isValid($data)) {
                try {
                    $formattedValues = array();
                    foreach ($form->getValues() as $_name => $_value) {
                        $arr = explode('__', $_name);
                        if (count($arr) === 2) {
                            if (isset($formattedValues[$arr[0]])) {
                                $formattedValues[$arr[0]] += array($arr[1] => $_value);
                            }  else {
                                $formattedValues[$arr[0]] = array($arr[1] => $_value);
                            }
                        }
                    }
                    $settings->saveConfig($formattedValues);
                    $this->getSession()->addMessage('success', 'Your settings have been saved');
                } catch (Exception $e) {
                    $this->getSession()->addMessage('error', $e->getMessage());
                }
            }
        } else {
            $form->populate($settings->getValuesForAdminForm(array('cancelled_order_email_to_buyer', 'cancelled_order_email_to_seller'), array('text','subject')));
        }
        $this->view->form = $form->setAction($this->_helper->url('cancelled-order-emails-setting'));
        $this->view->header = 'Cancelled Order Emails Setting';
        $this->view->session = $this->getSession();
    }

}
