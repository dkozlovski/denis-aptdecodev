<?php

class Admin_DeliveryDateController extends Ikantam_Controller_Admin
{

    protected $_range;
    protected $_collection;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('admin/login/index');
        }
        $this->_range = $this->getRnge();
        $this->_collection = new Admin_Model_DeliveryDate_Collection();
        $this->_collection->getByDateRange($this->_range);
        $this->_helper->_layout->setLayout('admin');
    }

    public function indexAction()
    {
        $tableDates = $this->getTableDates($this->_range);
        $rowsPerTable = 12;
        $amountTables = ceil(count($tableDates) / $rowsPerTable);

        $this->view->tableDates = $tableDates;
        $this->view->rowsPerTable = $rowsPerTable;
        $this->view->amountTables = $amountTables;
        $this->view->limit = count($tableDates);
    }

    public function modifyAction()//AJAX
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $dateString = $this->getRequest()->getParam('dateString', false);
        $periods = $this->getRequest()->getParam('periods', false); {
            $date = new Admin_Model_DeliveryDate();
            $date->getByDateString($dateString);

            $date->setIsFirstPeriodAvailable($periods[0])
                    ->setIsFirstPeriodForSmallAvailable($periods[1])
                    ->setIsSecondPeriodAvailable($periods[2])
                    ->setIsSecondPeriodForSmallAvailable($periods[3])
                    ->setIsThirdPeriodAvailable($periods[4])
                    ->setIsThirdPeriodForSmallAvailable($periods[5])
                    ->setIsFourthPeriodAvailable($periods[6])
                    ->setIsFourthPeriodForSmallAvailable($periods[7])
                    ->setDate(strtotime($dateString))
                    ->save();
        }
    }

    protected function getRnge($days = 70)
    {
        $current = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        return array($current, $current + (86400 * $days));
    }

    /**
     * @param array $range
     * @param string $format
     * @return array      
     * RESULT STRUCTURE:
     * Array (
     *          0 => array (1, 1, 1, 0, 'stringDate'=>'05 JAN 2012'), - first, second and third period availability
     *          ...
     *          )
     */
    protected function getTableDates($range, $format = 'd M Y D')
    {
        $aux = array();
        $result = array();

        for ($i = $range[0]; $i < $range[1]; $i += 86400) {
            $aux[date($format, $i)] = array(0, 0, 0, 0, 0, 0, 0, 0);
        }

        foreach ($this->_collection->getItems() as $date) {
            /* @var $date Admin_Model_DeliveryDate */
            if (key_exists($date->getDateString(), $aux)) {
                $aux[$date->getDateString()] = array(
                    $date->getIsFirstPeriodAvailable(),
                    $date->getIsFirstPeriodForSmallAvailable(),
                    $date->getIsSecondPeriodAvailable(),
                    $date->getIsSecondPeriodForSmallAvailable(),
                    $date->getIsThirdPeriodAvailable(),
                    $date->getIsThirdPeriodForSmallAvailable(),
                    $date->getIsFourthPeriodAvailable(),
                    $date->getIsFourthPeriodForSmallAvailable()
                );
            }
        }

        foreach (array_keys($aux) as $stringDate) {
            $result[] = array_merge($aux[$stringDate], array('stringDate' => $stringDate));
        }

        return $result;
    }

}