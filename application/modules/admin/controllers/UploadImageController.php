<?php

class Admin_UploadImageController extends Ikantam_Controller_Admin
{

    protected $_avatarUploadDir;
    protected $_backgroundUploadDir;
    protected $_categoryUploadDir;
    protected $_manufacturerUploadDir;

    public function init()
    {
        $this->_avatarUploadDir       = APPLICATION_PATH . '/../public/upload/avatars';
        $this->_uploadDir             = APPLICATION_PATH . '/../product-images';
        $this->_categoryUploadDir     = realpath(APPLICATION_PATH . '/../public/upload/homepage') . DIRECTORY_SEPARATOR;
        $this->_manufacturerUploadDir = realpath(APPLICATION_PATH . '/../public/upload/homepage') . DIRECTORY_SEPARATOR;
        $this->_backgroundUploadDir   = realpath(APPLICATION_PATH . '/../public/upload/homepage/background') . DIRECTORY_SEPARATOR;
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function indexAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            //@TODO:return error message
            die();
        }

        $productId = $this->getRequest()->getParam('product_id');

        $product = new Product_Model_Product($productId);

        if (!$product->getId()) {
            //@TODO:return error message
            die();
        }

        $img       = new Ikantam_Image();
        $imagePath = $img->uploadImage($this->_uploadDir, $product->getId());
        $img->createScaledImages($this->_uploadDir, $imagePath, $product->getId());

        $productImage = new Product_Model_Image();

        $productImage->setProductId($product->getId())
                ->setPath('original-0.jpg')
                ->setS3Path($imagePath)
                ->setIsVisible(0)
                ->setIsMain(0)
                ->setIsLocked(0)
                ->setIsUploaded(0)
                ->save();

        $response                 = array();
        $response['success']      = true;
        $response['file']['id']   = $productImage->getId();
        $response['file']['name'] = $imagePath . '/original-0.jpg';
        $response['file']['url']  = $productImage->getS3Url(500, 500, 'crop');

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(json_encode($response));
    }

    public function avatarAction()
    {
        $cropper = new Ikantam_Crop(array(
            'src_dir' => $this->_avatarUploadDir,
        ));

        $uploadHandler = new Ikantam_UploadHandler(/* array(
                  'image_versions' => array('thumbnail' => null),
                  ) */);
        $response      = $uploadHandler->initialize(true);

        if (is_array($response['files'])) {
            $user = $this->getSession()->getUser();

            $file = array_shift($response['files']);
            if ($file) {

                $cropper->setImage($file->name);
                $err = $cropper->getErrorMessages();
                if (!$err) {
                    $avatar = $user->getAvatar(true);

                    if ($avatar->getIsAvailable()) {
                        $path = $this->_avatarUploadDir . '/' . $avatar->getTitle();
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }

                    $avatar->setTitle($file->name)
                            ->setUserId($user->getId())
                            ->setIsAvailable(false)
                            ->save();

                    $info         = $cropper->getInfo();
                    $file->src    = Ikantam_Url::getPublicUrl('upload/avatars/' . $file->url);
                    $file->width  = $info[0];
                    $file->height = $info[1];
                } else {
                    $err         = array_shift($err);
                    $file->error = true;

                    if ($err['type'] == $cropper::FILE_TYPE) {
                        $file->err_msg = 'Uploaded file has unsupported type. Please choose gif,jpg or png image file.';
                    }
                }
            }
        }

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(Zend_Json::encode($file));
    }

    public function cropAction()
    {
        $params = $this->getRequest()->getParam('cropParams', false);
        if (!$params)
            return;

        $user = $this->getSession()->getUser();

        $cropper = new Ikantam_Crop(array(
            'src_dir' => $this->_avatarUploadDir,
            'square' => true, //Width and height must have equal length.
            'save_original' => false, //original image will be delete
            'output_format' => IMG_JPG,
            'quality' => 80,
            'interlace' => false, //will be saved like progressive JPEG
            'prefix' => $user->getId() . '_avtr',
            'random_name' => true,
            'create_thumbnail' => true,
            'thumbnail' => array('width' => 80, 'height' => 80, 'dir' => $this->_avatarUploadDir . '/thumbnail'),
            'width' => $params['w'], //selected area width
            'height' => $params['h'], //selected area height
        ));

        $cropper->setImage($user->getAvatar(true)->getTitle());

        $response = (object) array('error' => false);


        if (!$cropper->getErrorMessages()) {



            $file_info = $cropper->getInfo();

            $receivedHeight = $params['Height'];
            $originalHeight = $file_info[1];

            $R = $originalHeight / $receivedHeight;

            $x = round($params['x'] * $R);
            $y = round($params['y'] * $R);
            $h = round($params['h'] * $R);
            $w = round($params['w'] * $R);

            $cropper->crop($x, $y, $w, $h);

            $avatar = $user->getAvatar(true)
                    ->setTitle($cropper->getName())
                    ->setIsAvailable(true)
                    ->setUserId($user->getId())
                    ->save();

            $response->src = $user->getAvatarUrl();
        } else {
            $err               = array_shift($cropper->getErrorMessages());
            $response->error   = true;
            $response->message = $err['message'];
        }

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(Zend_Json::encode($response));
    }

    // Upload background and store information about uploaded image in session. For preview.
    public function homeBackgroundAction()
    {


        $path = APPLICATION_PATH . '/../tmp/homepage';

        $uploader = new Ikantam_File_Uploader();

        $uploader->setAdapter(new Zend_File_Transfer_Adapter_Http)
                ->setUploadPath($path);

        if (!$uploader->upload()) {
            $errors = $uploader->getErrorMessages();
            echo implode("\n", $errors);
            die();
            //@TODO: return error messages
        } else {
            $fileName = $uploader->getFileName();

            $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;

            $uploader->uploadToS3($fileName, $bucketName . '/homepage/background/' . basename($fileName));


            $file = new stdClass();

            $tmpObjects    = Admin_Model_Homepage_TmpObjects::getInstance();
            $tmpObjects->save('background', 'background/' . basename($fileName));
            $file->url     = $tmpObjects->getBackground();
            $file->success = true;
        }

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(Zend_Json::encode($file));
    }
    
    // Upload background and store information about uploaded image in session. For preview.
    public function homeBlogBackgroundAction()
    {


        $path = APPLICATION_PATH . '/../tmp/homepage';

        $uploader = new Ikantam_File_Uploader();

        $uploader->setAdapter(new Zend_File_Transfer_Adapter_Http)
                ->setUploadPath($path);

        if (!$uploader->upload()) {
            $errors = $uploader->getErrorMessages();
            echo implode("\n", $errors);
            die();
            //@TODO: return error messages
        } else {
            $fileName = $uploader->getFileName();

            $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;

            $uploader->uploadToS3($fileName, $bucketName . '/homepage/blog-background/' . basename($fileName));


            $file = new stdClass();

            $tmpObjects    = Admin_Model_Homepage_TmpObjects::getInstance();
            $tmpObjects->save('blog_background', 'blog-background/' . basename($fileName));
            $file->url     = $tmpObjects->getBlogBackground();
            $file->success = true;
        }

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(Zend_Json::encode($file));
    }

    public function homeCategoryAction()
    {
        if (!$uid = $this->getRequest()->getParam('uid', false))
            exit;


        $path = APPLICATION_PATH . '/../tmp/categories';

        $uploader = new Ikantam_File_Uploader();

        $uploader->setAdapter(new Zend_File_Transfer_Adapter_Http)
                ->setUploadPath($path);

        if (!$uploader->upload()) {
            $errors = $uploader->getErrorMessages();
            echo implode("\n", $errors);
            die();
            //@TODO: return error messages
        } else {
            $fileName = $uploader->getFileName();

            $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;
            $uploader->uploadToS3($fileName, $bucketName . '/homepage/categories/' . basename($fileName));


            $file = new stdClass();

            $tmpObjects    = Admin_Model_Homepage_TmpObjects::getInstance();
            $file->url     = $tmpObjects->attachImage('category', $uid, 'categories/' . basename($fileName));
            $file->success = true;
        }



        $this->getResponse()
                //->setHeader('Content-type', 'text/plain')
                ->setHeader('Content-type', 'application/json')
                ->setBody(Zend_Json::encode($file));
    }

    public function homeBrandAction()
    {
        if (!$uid = $this->getRequest()->getParam('uid', false))
            exit;


        $path = APPLICATION_PATH . '/../tmp/brands';


        $uploader = new Ikantam_File_Uploader();

        $uploader->setAdapter(new Zend_File_Transfer_Adapter_Http)
                ->setUploadPath($path);

        if (!$uploader->upload()) {
            $errors = $uploader->getErrorMessages();
            echo implode("\n", $errors);
            die();
            //@TODO: return error messages
        } else {
            $fileName = $uploader->getFileName();

            $bucketName = Zend_Registry::get('config')->amazon_s3->bucketName;
            $uploader->uploadToS3($fileName, $bucketName . '/homepage/brands/' . basename($fileName));


            $file = new stdClass();

            $tmpObjects    = Admin_Model_Homepage_TmpObjects::getInstance();
            $file->url     = $tmpObjects->attachImage('brand', $uid, 'brands/' . basename($fileName));
            $file->success = true;
        }


        $this->getResponse()
                //->setHeader('Content-type', 'text/plain')
                ->setHeader('Content-type', 'application/json')
                ->setBody(Zend_Json::encode($file));
    }

}
