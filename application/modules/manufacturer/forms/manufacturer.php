<?php

class Manufacturer_Form_Manufacturer extends Zend_Form
{

    public function init()
    {  
        $title = new Zend_Form_Element_Text('title');
        
        $title->setLabel('Enter new manufacturer or brand*')
              ->setRequired()
              ->addValidator('Db_NoRecordExists',false ,array('table'=>'manufacturers','field'=>'title'));
              
        $submit = new Zend_Form_Element_Submit('submit');
        
        $submit->setLabel('Save');
        
        $this->addElements(array($title, $submit));        
	
    }
    
    public function throwException ()
    {
    $msg = '';
    foreach($this->getMessages() as $field => $messages)
    {
    $msg.='<br /><b>Field "'.$field.'":</b><br />';
    foreach($messages as $message)
    {
      $msg.=$message.'<br />';
    }
   }
    if($msg!='')
    {
         throw new Exception($msg); 
    }
    return false;
    }


}

