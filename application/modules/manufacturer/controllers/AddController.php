<?php

class Manufacturer_AddController extends Ikantam_Controller_Front
{

	private $_manufacturerForm;

	public function init()
	{


		$this->_setManufacturerForm(new Manufacturer_Form_Manufacturer());
		parent::init();
	}


    public function indexAction() 
    {
        $this->view->form = $this->_getManufacturerForm();

    }
    
    public function postAction ()
    {
    $form = $this->_getManufacturerForm();
    $postData = $this->getRequest()->getPost();
        
            if($form->isValid($postData))
            {
                $manufacturer = new Application_Model_Manufacturer();
                $manufacturer->setTitle($form->getValue('title'))->save();
            } else
                {
                   $form->throwException();
                }
            
    $this->_helper->redirector(array('module'=>'manufacturer', 'controller'=>'add', 'action'=>'index'));

           
    }
    
    
	protected function _setManufacturerForm(\Zend_Form $form)
	{
	   $form->setAction(Ikantam_Url::getUrl('manufacturer/add/post'));
		$this->_manufacturerForm = $form;
		return $this;
	}

	protected function _getManufacturerForm()
	{
		return $this->_manufacturerForm;
	}
}
