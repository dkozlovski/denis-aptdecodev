<?php

class Manufacturer_AsyncController extends Ikantam_Controller_Front
{

    protected $_filters = array();
    protected $_searchValue;

    public function init()
    {

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $this->_filters[]   = new Zend_Filter_StringTrim();
        $this->_filters[]   = new Zend_Filter_StringToLower();
        $this->_searchValue = $this->getRequest()->getParam('manufacturerValue', false);
        $this->filter($this->_searchValue);
    }

    protected function filter(&$val)
    {
        if (!$val)
            return false;
        foreach ($this->_filters as $filter) {
            if ($filter instanceof Zend_Filter_Interface) {
                $filter->filter($val);
            }
        }
    }

    private function match($val)
    {
        return (stripos($val, $this->_searchValue) === 0);
    }

    public function findAction()
    {
        if ($this->getRequest()->isXmlHttpRequest)
            throw new Zend_Controller_Action_Exception('Page not Found', 404);

        $limit         = 15;
        $manufacturers = new Application_Model_Manufacturer_Collection();
        $value         = $this->getRequest()->getParam('term'); //$this->_searchValue;
        $this->filter($value);
        $list          = $manufacturers->getForAutocomplete($value, $limit); //$manufacturers->findSimilar($value, $limit)->getTitles();

        if ($this->getParam('for_product_alert')) {
            $list = array('suggestions' => $list);
        }
        echo Zend_Json::encode($list);
    }

    public function homepageAdminAction()
    {
        if ($this->getRequest()->isXmlHttpRequest)
            throw new Zend_Controller_Action_Exception('Page not Found', 404);
        $limit         = 10;
        $manufacturers = new Application_Model_Manufacturer_Collection();
        $value         = $this->_searchValue;
        $this->filter($value);
        $list          = $manufacturers->findSimilar($value, $limit)->getColumns(array('id', 'title'));
        echo Zend_Json::encode($list);
    }

    public function purchasebrandsAction()
    {
        $searchValue = $this->_searchValue;
        if ($searchValue) {
            $manufacturers = new Application_Model_Manufacturer_Collection();
            $user          = $this->getSession()->getUser();

            $result = array_filter($manufacturers->getByUserPurchaseHistory($user->getId())->getTitles(), array($this, 'match'));
            foreach ($result as $key => $val) {
                $result[$key] = array('title'   => $val, 'encoded' => urlencode($val));
            }
        }
        echo Zend_Json::encode($result);
    }

}