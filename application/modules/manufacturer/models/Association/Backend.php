<?php
/**
 * @author Aleksei Poliushkin
 * @copyright iKantam 2013/7/29
 */
 
class Manufacturer_Model_Association_Backend extends Application_Model_Abstract_Backend {
    
    protected $_table = 'manufacturers_association_list' ;
    
    public function getByIdAscn ($mid, $ascn, \Application_Model_Abstract $object)
    {
        $sql = "SELECT * FROM `". $this->_getTable() ."` WHERE `manufacturer_id` = :mid AND `association` = :ascn";
        $stmt = $this->_prepareSql($sql);
        
        $stmt->bindParam(':mid', $mid);
        $stmt->bindParam(':ascn', $ascn);
        $stmt->execute();
        
        $object->setData($stmt->fetch(PDO::FETCH_ASSOC));
    }
    
    public function updateProductsWithNewAssocciation (\Application_Model_Abstract $object)
    {
        $stmt = $this->_prepareSql("UPDATE `products` AS `p`
                                   INNER JOIN `manufacturers` AS `m` ON `m`.`id` = `p`.`manufacturer_id`
                                   SET `p`.`manufacturer_id` = :mid
                                   WHERE `m`.`title` = :association");                       
                                   
         $mid = $object->getManufacturerId();
         $ascn = $object->getAssociation();
         
         $stmt->bindParam(':mid', $mid);
         $stmt->bindParam(':association', $ascn);

         $stmt->execute();                         
    }
    
    protected function _insert(\Application_Model_Abstract $object)
    {
       $sql = "INSERT INTO `". $this->_getTable() ."` (`manufacturer_id`, `association`) VALUES(:mid, :ascn)";
       $stmt = $this->_prepareSql($sql);
       
       $mid = $object->getManufacturerId();
       $ascn = $object->getAssociation();
       
       $stmt->bindParam(':mid', $mid);
       $stmt->bindParam(':ascn', $ascn);
       
       $stmt->execute();
    }
    
    protected function _update(\Application_Model_Abstract $object)
    {
       $sql = "UPDATE `". $this->_getTable() ."` SET `manufacturer_id` = :mid, `association` = :ascn WHERE `id` = :id";
       $stmt = $this->_prepareSql($sql);
       
       $id = $object->getId();
       $mid = $object->getManufacturerId();
       $ascn = $object->getAssociation();
       
       $stmt->bindParam(':id', $id);
       $stmt->bindParam(':mid', $mid);
       $stmt->bindParam(':ascn', $ascn);
       
       $stmt->execute(); 
    }
}