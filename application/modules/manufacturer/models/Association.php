<?php
/**
 * @author Aleksei Poliushkin
 * @copyright iKantam 2013/7/29
 */
 
class Manufacturer_Model_Association extends Application_Model_Abstract {
    
    protected $_backendClass = 'Manufacturer_Model_Association_Backend';
    protected $_manufacturer ;
    
    public function __construct($id = null)
    {
        if(is_string($id) && !is_numeric($id)) {
            $this->getBy_association($id);
        } else {
            parent::__construct($id);
        }
    }
    
    public function addAssociation($association)
    {
        $newAscn = new self();
        $newAscn->setManufacturerId($this->getManufacturerId())
                ->setAssociation($association);
        try{
            $newAscn->save();
        } catch(PDOException $ex) {
            if($ex->errorInfo[1] !== 1062) {
                throw $ex ;
            }
        }
        
        return $this ;
    }
    
    public function getAssociationArray ()
    {
        return $this->_collection()
                    ->getBy_manufacturer_id($this->getManufacturerId())
                    ->getColumn('association');  
    }
    
    public function getByManufacturerIdAscn ($manufacturerId, $association)
    {
        $this->_getbackend()->getByIdAscn($manufacturerId, $association, $this);
        return $this ;
    }
    
    public function getManufacturer ()
    {
        return ($this->_manufacturer) 
            ? $this->_manufacturer 
            : $this->_manufacturer = new Application_Model_Manufacturer($this->getManufacturerId());
    }
    
    protected function _collection()
    {
        return new Manufacturer_Model_Association_Collection();
    } 
    
    public function updateProducts ()
    {
        $this->_getbackend()->updateProductsWithNewAssocciation($this); 
        return $this;
    }  

}