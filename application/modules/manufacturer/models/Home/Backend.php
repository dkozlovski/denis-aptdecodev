<?php

class Manufacturer_Model_Home_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'manufacturers_home';


    
    public function addManufacturer (\Application_Model_Manufacturer $object, $imagePath = null)
    {
        $this->_insert($object->setImagePath($imagePath));
    }
    
    public function delete (\Application_Model_Manufacturer $object)
    {
        $sql = "DELETE FROM `".$this->_getTable()."` WHERE `manufacturer_id` = :mid LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $id = $object->getId();
        $stmt->bindParam(':mid', $id);
        $stmt->execute();
    }
    
    public function getImagePath (\Application_Model_Manufacturer $object)
    {
        $sql = "SELECT `image_path` FROM `".$this->_getTable()."` WHERE `manufacturer_id` = :mid LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $id = $object->getId();
        $stmt->bindParam(':mid', $id);
        $stmt->execute();
        
        return $stmt->fetch(PDO::FETCH_COLUMN);
         
    } 
    
    	/**
         * Fix multiple manufacturers
         * @param  array $ids
         */
    public function fixMultiple ($ids)
    {       

        $parts = array();
        $binds = array(); 

        foreach($ids as $id)
        {
            $parts[] = '(NULL, :id'.$id.')';
            $binds[':id'.$id]=$id;
        }
     $sql = "INSERT IGNORE INTO `".$this->_getTable()."` (`id`, `manufacturer_id`) VALUES ".implode(',', $parts);
     $stmt = $this->_getConnection()->prepare($sql);     

     $stmt->execute($binds);
    }      
     
    public function deleteAll ()
    {
        $sql = "DELETE FROM `".$this->_getTable()."` WHERE 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
    }


    	/**
         * !IMPORTANT!
         * @param  Application_Model_Manugacturer $object
         */
	protected function _update(\Application_Model_Abstract $object)
	{
   
	}

    	/**
         * !IMPORTANT!
         * @param  Application_Model_Manugacturer $object
         */
	protected function _insert(\Application_Model_Abstract $object)
	{

	    $dbh = $this->_getConnection();
        $sql = "INSERT INTO `".$this->_getTable()."` (`id`, `manufacturer_id`, `image_path`) VALUES (NULL, :mid, :img_path)";
        
        $stmt = $dbh->prepare($sql);
        
        $id = $object->getId();
         $imgPath = $object->getImagePath();
         
        $stmt->bindParam(':mid', $id);
        $stmt->bindParam(':img_path', $imgPath);        
        
        $stmt->execute();
        
        $object->setId($dbh->lastInsertId());       
        
	}



}
