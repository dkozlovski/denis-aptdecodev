<?php
class Shipping_Model_DistanceBetweenPostalCodes extends Application_Model_Abstract
{
    protected static $_instance;
    
    /**
     * Save or update record
     * 
     * @param  string $code1
     * @param  string $code2
     * @param  float $distance
     * @return object self
     */
    public static function set ($code1, $code2, $distance)
    {
        $inst = self::getInstance();
        $inst->setCode1($code1)
             ->setCode2($code2)
             ->getByCodes()
             ->setDistance($distance)
             ->save();
             
        return $inst;              
    }
    
    /**
     * Return distance if record exist
     * 
     * @param string $code1
     * @param string $code2 
     * @return float - distance
     */
    public static function get ($code1, $code2)
    {
        $inst = self::getInstance();
        $inst->setCode1($code1)
             ->setCode2($code2)
             ->getByCodes();
             
        return $inst->getDistance();      
    }
    
    public function getByCodes ()
    {
        $this->_getbackend()->getByCodes($this);
        return $this;
    }
    
    protected static function getInstance ()
    {
        if(!isset(self::$_instance)){
            self::$_instance = new self;
        }
        return self::$_instance;
    }
}