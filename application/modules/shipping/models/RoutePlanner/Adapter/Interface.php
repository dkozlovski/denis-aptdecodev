<?php
interface Shipping_Model_RoutePlanner_Adapter_Interface
{
    /**
     * Return distance in MILES between two points based on postal codes
     * 
     * @param string $code1
     * @param string $code2 
     * @return mixed float - on success , false - on failure
     */
    public function getDistanceBetween2PostalCodes ($code1, $code2);
   
    
}