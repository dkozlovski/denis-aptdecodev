<?php
class Shipping_Model_RoutePlanner_Adapter_Mapquest extends Ikantam_Maps_Service_Mapquest
{
    protected $_errors ;
    
    /**
     * Implementation of Shipping_Model_RoutePlanner_Adapter_Interface
     * @param  string $code1
     * @param  string $code2
     * @return float
     */
    public function getDistanceBetween2PostalCodes ($code1, $code2)
    {
        if(!$code1 || !$code2){
            $this->_errors = 'The "LocationCollection" must contain at least two locations.';
            return false;    
        }
        
        $response = $this->call($this->_directionsRequest(
            array(
                'from' => $code1, 
                'to' => $code2,
                'manMaps' => 'false',
                'narrativeType' => 'none',
                'stateBoundaryDisplay' => 'false',
                'countryBoundaryDisplay' => 'false',
                'sideOfStreetDisplay' => 'false',                
            )
             ),
           'route'  
         ); 
        
         if($response->isEmpty()) {
            $this->_errors = 'Service unavailable. Response is empty.';
            return false;
         }
         if( $response->info['statuscode'] != 0){
                $this->_errors = $response->info['messages'];
                return false;
         }         

         return $response->route['distance'];
    }
    
    /**
     * Implementation of Shipping_Model_RoutePlanner_Adapter_Interface
     *
     * @return bool
     */    
    public function hasError ()
    {
        return (bool)count($this->_errors);
    }
    
    /**
     * Build request object with provided parameters
     * 
     * @param  array $parameters
     * @return object Ikantam_Service_Request
     */
    protected function _directionsRequest(array $parameters)
    {
        $req = new Ikantam_Maps_Directions_Request_Mapquest_NVP;
        $req->fromArray($parameters);
        return $req;
    }
    
}