<?php
class Shipping_Model_DistanceBetweenPostalCodes_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'distance_between_postal_codes';
    protected $_fields_ = array('id', 'code1', 'code2', 'distance');
    
    public function getByCodes (\Application_Model_Abstract $object)
    {
        $sql = "SELECT * FROM `".$this->_getTable()."` WHERE `code1` = :code1 AND `code2` = :code2 LIMIT 1";
        $c1 = $object->getCode1();
        $c2 = $object->getCode2();
        
        $stmt = $this->_prepareSql($sql);
        $stmt->bindParam(':code1', $c1);
        $stmt->bindParam(':code2', $c2);
        
        $stmt->execute();
        
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        if(is_array($data)) {
            $object->addData($data);    
        }
        
        return $object;
    }
    
    protected function _insert(\Application_Model_Abstract $object)
    {
        $this->runStandartInsert(array('code1', 'code2', 'distance'), $object);
    } 
    
    protected function _update(\Application_Model_Abstract $object)
    {
        $this->runStandartUpdate(array('code1', 'code2', 'distance'), $object);
    }     
}