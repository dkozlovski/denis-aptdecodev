<?php
class Shipping_Model_Borough_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'boroughs';
    protected $_fields = array('id', 'city_id', 'name');
    
    protected function _insert(\Application_Model_Abstract $object)
    {  
       $this->runStandartInsert(array('city_id', 'name'), $object);     
    }
    
    protected function _update(\Application_Model_Abstract $object)
    { 
        $this->runStandartUpdate(array('city_id', 'name'), $object);
    } 
}