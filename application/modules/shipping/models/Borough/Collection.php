<?php
class Shipping_Model_Borough_Collection extends  Application_Model_Abstract_Collection
{
    public function getFlexFilter ()
    {
        return new Shipping_Model_Borough_FlexFilter($this);    
    }
}