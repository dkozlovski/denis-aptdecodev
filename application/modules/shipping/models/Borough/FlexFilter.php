<?php

class Shipping_Model_Borough_FlexFilter extends Ikantam_Filter_Flex {
    
    protected $_acceptClass = 'Shipping_Model_Borough_Collection';
    
    protected $_joins = array (
       'postal_code' => 'INNER JOIN `postal_codes` AS `postal_code` ON `postal_code`.`borough_id` = `boroughs`.`id`',
    );
    
    protected $_select = array  ( 
        'postal_code' => '`postal_code`.`code` AS `postal_code`'   
    );
    
    protected $_rules = array(
       'postal_code'  => '`postal_code`.`code`',
    );     
}