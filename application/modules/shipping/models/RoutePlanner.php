<?php

use \Shipping_Model_DistanceBetweenPostalCodes as Distance;

class Shipping_Model_RoutePlanner
{

    protected $_serviceAdapter;
    protected $_defaultAdapter = 'Mapquest';

    /**
     * Stores additional condition properties for fee calculating
     * for example: 
     *  'shipping_method_id' => 'delivery', 'some_prop', 'prop2' => 'value'
     * and then before return value checks for consistent method which hanlde value...
     * 
     * @type array
     */
    protected $_additionalConditions = array();

    /**
     * Sets service adapter
     *  
     * @param mixed $adapter(Optional) - adapter name or instance Shipping_Model_RoutePlanner_Adapter_Interface  
     * @return 
     */
    public function __construct($adapter = null)
    {
        if (null === $adapter) {
            $adapter = $this->_defaultAdapter;
        }
        if (is_string($adapter)) {
            $adapterClass          = 'Shipping_Model_RoutePlanner_Adapter_' . $adapter;
            $this->_serviceAdapter = new $adapterClass();
        } elseif ($adapter instanceof Shipping_Model_RoutePlanner_Adapter_Interface) {
            $this->_serviceAdapter = $adapter;
        } else {
            throw new Exception('Invalid adapter type. Adapter must implements Shipping_Model_RoutePlanner_Adapter_Interface');
        }
    }

    /**
     * Calculate shipping price based on distance obtained by third party service
     *
     * @param  string $code1
     * @param string $code2
     * @param bool $isUnder15pounds
     * @param bool $isAdditionalItem
     * @return int shipping price
     */
    public function getShippingPriceByPostalCodes(
        $code1,
        $code2,
        $isUnder15pounds = false,
        $isAdditionalItem = false
    ) {
        if ($isAdditionalItem) {
            return Shipping_Model_Rate::getRateForMultipleItems($isUnder15pounds);
        }

        $distance   = null;
        $filter     = new Shipping_Model_Borough_FlexFilter(new Shipping_Model_Borough_Collection);
        $filter->name('in', Shipping_Model_Rate::getFlatRateBoroughs());
        $isHomeArea = false;
        //if equals or if in flat rate zone
        if (($code1 == $code2) || ((int) $filter->postal_code('=', $code1, $code2)->count() === 2)) {
            $isHomeArea = true;
            $price      = Shipping_Model_Rate::getFlatRate($isUnder15pounds);
        } else {
            if (!$distance = Distance::get($code1, $code2)) {
                if (!$distance = $this->_serviceAdapter->getDistanceBetween2PostalCodes($code1, $code2)) {
                    $price = Shipping_Model_Rate::getFlatRate($isUnder15pounds); //service return error or unavailable 
                } else {
                    Distance::set($code1, $code2, $distance);
                }
            }
            if ($distance !== false) {
                $price = Shipping_Model_Rate::getStartPrice() + (ceil($distance) * Shipping_Model_Rate::getPricePerMile());
            }
        }

        return $price;
    }

}