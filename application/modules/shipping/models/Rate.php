<?php

class Shipping_Model_Rate
{

    /**
     * Price for 1 mile
     *  
     * @return int
     */
    public static function getPricePerMile()
    {
        return 2;
    }

    /**
     * When  price calculates by distance this is minimum price
     *
     * @return int
     */
    public static function getStartPrice()
    {
        return 95; //57;
    }

    /**
     * Rate for the 4 boroughs
     *
     * @param bool $isUnder15pounds
     * @return int
     */
    public static function getFlatRate($isUnder15pounds = false)
    {
        if ($isUnder15pounds) {
            return self::getRateForItemsUnder15Pounds();
        }
        return 65;
    }

    public static function getRateForItemsUnder15Pounds()
    {
        return 20;
    }

    /**
     * If more than one product
     * Price for each next item
     *
     * @param bool $isUnder15pounds
     * @return int
     */
    public static function getRateForMultipleItems($isUnder15pounds = false)
    {
        if ($isUnder15pounds) {
            return self::getRateForItemsUnder15Pounds();
        }
        return 25; // 10
    }

    /**
     * Maximum value which seller can absorb
     * 
     * @return int 
     */
    public static function getMaximumValueSellerAbsorb($isUnder15pounds = false)
    {
        if ($isUnder15pounds) {
            return self::getMaximumValueSellerAbsorbForSmall();
        }
        return 65;
    }

    /**
     * Maximum value which seller can absorb for small items
     * 
     * @return int 
     */
    public static function getMaximumValueSellerAbsorbForSmall()
    {
        return 20;
    }

    /*
     * If a buyer indicates that their building is 4 or more flights during
     * the check out process the delivery fee
     * should increase by $10 for each additional flight.
     */
    public static function getRateForAdditionalFlights()
    {
        return 10;
    }

    /**
     * Boroughs in which flat rate is applied
     * @return array
     */
    public static function getFlatRateBoroughs()
    {
        return array('Brooklyn', 'Bronx', 'Manhattan', 'Queens');
    }
}