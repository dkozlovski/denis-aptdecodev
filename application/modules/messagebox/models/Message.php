<?php

class Messagebox_Model_Message extends Application_Model_Abstract
{
	protected $_backendClass = 'Messagebox_Model_Message_Backend';
    protected $_relatedBackend = 'Messagebox_Model_Relation_Backend';
    
    const MESSAGE_TYPE_INBOX = 'recipient_id';
    const MESSAGE_TYPE_SEND  = 'author_id';
    
    
    public function markAsRead ()
    {
     $this->setIsNew(false);
     $this->_getRelatedBackend()->updateRelation($this);
     return $this;
    }
    
    public function markAsNew ()
    {
     $this->setIsNew(true);
     $this->_getRelatedBackend()->updateRelation($this);
     return $this;
    }    
    
    public function markAsDeleted ()
    {
        $this->setIsVisible(false);
        $this->_getRelatedBackend()->updateRelation($this);
        return $this;
    }
    
    public function getMessageById($messageId, $userId, $msgType = self::MESSAGE_TYPE_INBOX)
    {
        $this->_getBackend()->getMessageById($this, $messageId, $userId, $msgType);
        return $this;
    }
    
    public function save ()
    {
        parent::save();
            if(!$this->getUserId())
                {
            $sess = new User_Model_Session();
            $user = $sess->getUser();
            $this->setUserId($user->getId());
                }
 
        if(!is_null($this->getIsVisible())){
       $this->_getRelatedBackend()->updateRelation($this);}
    }
    
    protected function _getRelatedBackend()
    {
        return new $this->_relatedBackend;
    }
    
    protected function _getBackend()
    {
        return new $this->_backendClass;
    }
}