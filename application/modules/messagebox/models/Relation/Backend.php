<?php

    class Messagebox_Model_Relation_Backend extends Application_Model_Abstract_Backend
{
            protected $_table = 'messages_users';
            
            
            
    public function createRelation (\Application_Model_Abstract $object)
    {
        $this->_insert($object);
    }
    
    public function updateRelation (\Application_Model_Abstract $object)
    {
      // throw  new Exception('turned');  
        $this->_update($object);
    }
            
//-------------------------------------------------------------------------------


    
	protected function _update(\Application_Model_Abstract $object)
	{
	   if(!$object->getUserId()) throw new Exception('Can\'t update messasge relation for undefined user');
        $sql = "UPDATE `" . $this->_getTable() . "` 
        set `is_visible` = :visible, `is_new` = :new WHERE (`user_id` = :u_id AND `message_id` = :m_id)";
        
		$stmt = $this->_getConnection()->prepare($sql);     

        $stmt->bindParam(':m_id',    $object->getId());
        $stmt->bindParam(':u_id',    $object->getUserId());
        $stmt->bindParam(':visible', $object->getIsVisible());
        $stmt->bindParam(':new',     $object->getIsNew());
		$stmt->execute();
		
	}
	
	protected function _insert(\Application_Model_Abstract $object)
	{
	   $dbh = $this->_getConnection();
       
       $relSql = "INSERT INTO `".$this->_getTable()."` (`message_id`, `user_id`, `is_visible`, `is_new`)
                        VALUES(:m_id, :u_id, 1, :new)";
            
       $stmtSender = $dbh->prepare($relSql);
       $stmtRecipient =  $dbh->prepare($relSql);
  
       
       $stmtSender->bindParam(':m_id', $object->getId());
       $stmtSender->bindParam(':u_id', $object->getAuthorId());
       $stmtSender->bindParam(':new', $new = false);      
       $stmtSender->execute();
       

       $stmtRecipient->bindParam(':m_id', $object->getId());
       $stmtRecipient->bindParam(':u_id', $object->getRecipientId());
       $stmtRecipient->bindParam(':new', $new = true);        
       $stmtRecipient->execute(); 
          
    }


}    