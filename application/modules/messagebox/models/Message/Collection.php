<?php
class Messagebox_Model_Message_Collection extends Application_Model_Abstract_Collection
{
	protected $_backendClass = 'Messagebox_Model_Message_Backend';
    
    const MESSAGES_TYPE_INBOX = 'recipient_id';
    const MESSAGES_TYPE_SEND  = 'author_id';
    const MESSAGES_STATUS_ALL   = 'allMessages';
    const MESSAGES_STATUS_NEW   = 'newMessages';
    
    	/**
         * 
         * @param integer $userId
         * @param const string $msgsType (type of return messages inbox/send)
         * @param const string $msgsStatus  (status of return messages: new/all)
         *  
         * @return Messagebox_Model_Message_Collection
         */ 
    
    public function getMessages ($userId, $paginator, $msgsType = self::MESSAGES_TYPE_INBOX, $msgsStatus = self::MESSAGES_STATUS_ALL)
    {
        $this->_getBackend()->getMessages($this, $userId, $msgsType, $msgsStatus, &$paginator);
        return $this;
    }
    
    public function getMessagesById($messageId, $userId, $msgType)
    {
        $this->_getBackend()->getMessagesById($this, $messageId, $userId, $msgType = self::MESSAGES_TYPE_INBOX);
        return $this;
    }
    
    public function getNewMessageInboxCount ($userId)
    {
       return $this->_getBackend()->getNewMessageInboxCount($userId);    
    }
    

    
}