<?php

class Messagebox_Model_Message_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'messages';    
    protected $_relatedBackend = 'Messagebox_Model_Relation_Backend';



    public function getMessages (\Application_Model_Abstract_Collection $collection, $uId, $msgsType, $msgsStatus, $paginator)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $options = $paginator;        
        $select = new Zend_Db_Select($db);        
        $select->from(array('m' => $this->_getTable()))
               ->join(array('mu' => $this->_getRelatedBackend()->_getTable()),
               "m.{$msgsType} = mu.user_id AND mu.message_id = m.id")
               ->where("m.{$msgsType} = ? ", $uId)
               ->where('mu.is_visible = 1');
               
               if($msgsStatus === $collection::MESSAGES_STATUS_NEW) $select->where('mu.is_new = 1');               
               //echo($select->__toString()); die();
               $paginator = Zend_Paginator::factory($select);
            if($options['itemsPerPage']) $paginator->setItemCountPerPage($options['itemsPerPage']);
            if($options['pageRange']) $paginator->setPageRange($options['pageRange']);
            if($options['currentPageNumber']) $paginator->setCurrentPageNumber($options['currentPageNumber']);
               
                
        
        foreach($paginator as $row)
        { 
            $message = new Messagebox_Model_Message();
            $message->addData($row);
            
            $collection->addItem($message);
            
        }       
    }
    
    	/**
         * Retrieves a single message from db
         * @param Messagebox_Model_Message $message
         * @param integer $messageId
         * @param integer $userId
         * @param const string $msgType (Inbox/Send) 
         */
  public function getMessageById(\Application_Model_Abstract $message, $messageId, $userId, $msgType)
  {

    $sql = "SELECT `m`.*, `mu`.* FROM `".$this->_getTable()."` AS `m` 
           JOIN `".$this->_getRelatedBackend()->_getTable()."` AS `mu` 
           ON (m.{$msgType} = mu.user_id) AND (mu.message_id = m.id) 
           WHERE (m.{$msgType} = :u_id)AND (mu.message_id = :m_id) AND (mu.is_visible = 1) LIMIT 1";
           
    $stmt = $this->_getConnection()->prepare($sql);
    $stmt->bindParam(':u_id', $userId);
    $stmt->bindParam(':m_id', $messageId);
   // print_d($stmt); die();
    
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if(is_array($result[0]))
    {
        $message->setData($result[0]);
    }
    
  }

    	/**
         * @param Messagebox_Model_Message_Collection $collection
         * @param array $messageId
         * @param integer $userId
         * @param const string $msgType (Inbox/Send)          
         */  
  
  public function getMessagesById(\Application_Model_Abstract_Collection $collection, $messageId, $userId, $msgType)
  {
    if(empty($messageId)) return false;
    $in = '';

    if(is_array($messageId) && array_product($messageId)!= 0)
    {
        $in  = implode(',', $messageId);
    }

        $sql = "SELECT `m`.*, `mu`.* FROM `".$this->_getTable()."` AS `m` 
           JOIN `".$this->_getRelatedBackend()->_getTable()."` AS `mu` 
           ON (m.{$msgType} = mu.user_id) AND (mu.message_id = m.id) 
           WHERE (m.{$msgType} = :u_id)AND (mu.message_id IN ({$in})) AND (mu.is_visible = 1)";
           
    $stmt = $this->_getConnection()->prepare($sql);
    $stmt->bindParam(':u_id', $userId);
    
    $stmt->execute();
    
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($result as $row)
    {
        $message = new Messagebox_Model_Message();
        $message->addData($row);
        $collection->addItem($message);
    }
  return true;
  }
  
  public function getNewMessageInboxCount ($userId)
  {
    $sql = "SELECT COUNT(*) as count FROM `messages_users`
         WHERE `user_id` = :u_id AND `is_new` = 1  AND `is_visible` =  1";
         
    $stmt = $this->_getConnection()->prepare($sql);
    $stmt->bindParam(':u_id', $userId);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return ($result[0]['count']) ? $result[0]['count'] : 0;
  }
    

//--------------------------------------------------------------------------
    protected function _getRelatedBackend()
    {
        return new $this->_relatedBackend;
    }
    
	protected function _update(\Application_Model_Abstract $object)
	{
	   $sql = "UPDATE `".$this->_getTable()."` 
       SET `author_id` = :author, `recipient_id` = :recipient, `subject` = :subject,
       `text` = :text, `created_at` = :created WHERE `id` = :id";
       
       $stmt = $this->_getConnection()->prepare($sql);
       
       $stmt->bindParam(':id', $object->getId());
       $stmt->bindParam(':author', $object->getAuthorId());
       $stmt->bindParam(':recipient', $object->getRecipientId());
       $stmt->bindParam(':subject', $object->getSubject());
       $stmt->bindParam(':text', $object->getText());
       $stmt->bindParam(':created', $object->getCreatedAt());
       
       $stmt->execute();
		
	}
	
	protected function _insert(\Application_Model_Abstract $object)
	{
       $dbh = $this->_getConnection();

	   $sql = "INSERT INTO `".$this->_getTable().
       "` (`id`, `author_id`, `recipient_id`,  `subject`, `text`, `created_at`) 
       VALUES (NULL, :author, :recipient, :subject, :text, :created)";       
        
              
       $stmt = $dbh->prepare($sql);
       
       $stmt->bindParam(':author', $object->getAuthorId());
       $stmt->bindParam(':recipient', $object->getRecipientId());
       $stmt->bindParam(':subject', $object->getSubject());
       $stmt->bindParam(':text', $object->getText());
       $stmt->bindParam(':created', time());
       
       $stmt->execute();
       $object->getById($dbh->lastInsertId()); 
       
       $this->_getRelatedBackend()->createRelation($object);
	}
}

