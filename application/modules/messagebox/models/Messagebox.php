<?php
    
    class Messagebox_Model_Messagebox
    
    {
        const ERROR_USER = 'User is undefined.';
        const ERROR_RECIPIENT = 'Recipient is undefined.';
        protected static $_instance = null; 
        protected $_messageCollection = null;
        protected $_userId = null;
        protected $_paginator = null;
        protected $_itemsPerPage = null;
        protected $_pageRange = null;
        protected $_currentPage = null;
        protected $_messageStatus = null;
        private $_isInboxLoad = false;
        private $_isSentLoad = false;
        
       
        
        private function __construct ($userId = null)
        {
                $this->setUserId($userId);
                $this->_messageCollection = new Messagebox_Model_Message_Collection();                                        
        }
        
        private function __clone()
        {          
        }
        
        public function getInstance ($userId = null)
        {
            if(is_null(self::$_instance))
            {
                self::$_instance = new self($userId);
            }
            return self::$_instance;
        }
        
        public function setUserId ($id)
        {
            
        if(is_int($id) && $id  > 0)
            {
            $this->_userId = $id;
            } 
        }
        
        	/**
             * Throw exception if user id is not defined.
             * @param string $method - Method name  
             * 
             */
        protected function checkUser($method)
        {
            if(!$this->_userId) throw new Exception($method.' '.self::ERROR_USER);
        
        }
        
            	/**               
                 * @param const string $status - Status of messages for retrieve: all/new  
                 * @return Messagebox_Model_Messagebox
                 */
        public function getInboxMessages ($status = Messagebox_Model_Message_Collection::MESSAGES_STATUS_ALL)
        {     
             $this->checkUser(__METHOD__);
             if(!$this->_isInboxLoad || $this->_messageStatus !== $status)
             { 
             $this->_messageStatus = $status;
             $this->_messageCollection->clear();
             $this->_paginator = $this->prepareOptionsForPaginator();
             $this->_messageCollection->getMessages($this->_userId, &$this->_paginator, Messagebox_Model_Message_Collection::MESSAGES_TYPE_INBOX, $status);
             $this->_isInboxLoad = true; $this->_isSentLoad = false;
             }
             return $this;
        }
            
        public function getSendMessages ()
        {
              $this->checkUser(__METHOD__);
              if(!$this->_isSentLoad)
              {
              $this->_messageCollection->clear(); 
              $this->_paginator = $this->prepareOptionsForPaginator();
              $this->_messageCollection->getMessages($this->_userId, &$this->_paginator, Messagebox_Model_Message_Collection::MESSAGES_TYPE_SEND);
              $this->_isInboxLoad = false; $this->_isSentLoad = true;
              }
              return $this;
        }
        
        public function getMessageById ($messageId, $msgType = Messagebox_Model_Message::MESSAGE_TYPE_INBOX)
        {
            $this->checkUser(__METHOD__);
            $message = new Messagebox_Model_Message();
            $message->getMessageById($messageId, $this->getUserId(), $msgType);
            return $message;
        }
        
        public function getMessagesById ($messageId, $msgType = Messagebox_Model_Message_Collection::MESSAGES_TYPE_INBOX)
        {
            $this->checkUser(__METHOD__);
            $this->_messageCollection->clear();
            $this->_messageCollection->getMessagesById($messageId, $this->getUserId(),$msgType);
            return $this;        
        }
        
        public function sendMessage ($recipient, $subject, $text)
        {
            if(is_int($recipient) && $recipient > 0)
            {
            $this->checkUser(__METHOD__);
            if($recipient == $this->_userId) return false;
            $stripTags = new Zend_Filter_StripTags();
            $subject = $stripTags->filter($subject);
            
            $message = new Messagebox_Model_Message();
            $message->setAuthorId($this->_userId)
                    ->setRecipientId($recipient)
                    ->setSubject($subject)
                    ->setText($text)
                    ->setUserId($this->_userId)
                    ->save();
                    
            return true;
            }
            
           return false;
        }
        
        public function getUserId ()
        {
            return $this->_userId;
        }
        

        	/**
             * Return number - count of new inbox messages
             * @return integer 
             */
        public function getNewMessageInboxCount ()
        {
           return $this->_messageCollection->getNewMessageInboxCount($this->getUserId());         
        }
        
        public function sentMessageCount ()
        {
       
        }
        
        public function getPaginator ()
        {  
            if(!$this->_isInboxLoad && $this->_isSentLoad)
            {
              throw new Exception(__METHOD__.' Can\'t create paginator. Messages was not loaded.');
            }

            return $this->_paginator;
        }
        
        public function setItemsPerPage ($itemsCount)
        {
            $this->_itemsPerPage = ((int) $itemsCount > 0) ? (int) $itemsCount : null; 
            return $this;
        }
        
        public function setPageRange ($pageRange)
        { 
            $this->_pageRange = ((int) $pageRange > 0) ? (int)$pageRange : null;
            return $this;
        }
        
        public function setCurrentPageNumber ($pageNumber)
        {
            $this->_currentPage = ((int) $pageNumber > 0) ? (int)  $pageNumber : 1;
            return $this;
        }
        
        public function getCurrentPageNumber ()
        {
            return $this->_currentPage;
        }
        
        private function prepareOptionsForPaginator()
        {
            $options = array();
            
            $options['itemsPerPage'] = ($this->_itemsPerPage) ? $this->_itemsPerPage : 10; 
            $options['pageRange'] = ($this->_pageRange) ? $this->_pageRange : 10;
            $options['currentPageNumber'] =  ($this->_currentPage) ?  $this->_currentPage : 1;
           return $options;               
        }
        
        public function getCollection ()
        {
            return $this->_messageCollection;
        }
        
        
    }