<?php

class Messagebox_ViewController extends Ikantam_Controller_Front
{

	public function init()
	{

	}
    
    public function inboxAction ()
    {

        $this->view->avatar = new User_Model_Avatar();
        $this->view->user = new Application_Model_User();
        $messagebox = $this->getSession()->getUser()->getMessagebox();
        
        $status = ($this->getRequest()->getParam('status') === 'new')
         ? Messagebox_Model_Message_Collection::MESSAGES_STATUS_NEW
         : Messagebox_Model_Message_Collection::MESSAGES_STATUS_ALL;         
         
        
        $messagebox->setCurrentPageNumber($this->getRequest()->getParam('page', 1))
                   ->setPageRange(10)
                   ->setItemsPerPage(10)
                   ->getInboxMessages($status);       
        $this->view->messagebox = $messagebox;
        $this->view->status = $status;
    }
    
    public function sentitemsAction ()
    {
        
    }

}
