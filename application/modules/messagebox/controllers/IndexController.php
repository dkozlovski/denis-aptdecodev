<?php

class Messagebox_IndexController extends Ikantam_Controller_Front
{
    protected $_messagesId = array();
    protected $_messagebox = null;
    protected $_page = null;
    protected $_status = null; 
    

	public function init()
	{	    
	    $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $action = null;
	    $rqst = $this->getRequest();
        $this->_page = $rqst->getParam('page');
        $this->_status = $rqst->getParam('status','all');
        $this->_messagesId = $rqst->getParam('messages', null);
        
        $this->_messagebox = $this->getSession()->getUser()->getMessagebox();       

        if($rqst->getParam('mark_read')) $action = 'markread';
        if($rqst->getParam('delete')) $action = 'delete';
        
        $rqst->setActionName($action);         
	}
    
    
    public function postAction ()
    {
        //formal action
    }
    
    public function markreadAction ()
    {
        $this->_messagebox->getMessagesById($this->_messagesId);
        foreach($this->_messagebox->getCollection()->getItems() as $message)
        {
            $message->markAsRead();
        }
    $this->_helper->redirector('inbox','view','messagebox', array('page'=>$this->_page, 'status'=>$this->_status));
    }
    
    public function deleteAction ()
    {
        $this->_messagebox->getMessagesById($this->_messagesId);
        foreach($this->_messagebox->getCollection()->getItems() as $message)
        {
            $message->markAsDeleted();
        }
        
    $this->_helper->redirector('inbox','view','messagebox', array('page'=>$this->_page, 'status'=>$this->_status));
    }
    


}
