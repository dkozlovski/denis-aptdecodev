<?php

class Catalog_Model_Color_Collection extends Core_Model_Abstract_Collection
{
    protected $_table = 'colors';
    
    public function getFields()
    {
        return array('id', 'title', 'hex');
    }
}