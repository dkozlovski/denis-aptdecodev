<?php

class Catalog_Model_RouterBackend extends Application_Model_Abstract_Backend
{

    protected function _insert(\Application_Model_Abstract $object)
    {
        
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        
    }

    public function getRoute($path)
    {
        $sql1 = 'SELECT * FROM `categories` WHERE `page_url` = :page_url AND `is_active` = 1';
        $stmt1 = $this->_getConnection()->prepare($sql1);
        $stmt1->bindParam(':page_url', $path);
        $stmt1->execute();
        $result1 = $stmt1->fetch(PDO::FETCH_ASSOC);
        if ($result1) {
            $category = new Category_Model_Category();
            $category->addData($result1);
            return $category;
        }

        $sql2 = 'SELECT * FROM `products` WHERE `page_url` = :page_url';
        $stmt2 = $this->_getConnection()->prepare($sql2);
        $stmt2->bindParam(':page_url', $path);
        $stmt2->execute();
        $result2 = $stmt2->fetch(PDO::FETCH_ASSOC);
        if ($result2) {
            $product = new Product_Model_Product();
            $product->addData($result2);
            return $product;
        }
    }

}