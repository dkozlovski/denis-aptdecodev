<?php

class Catalog_Model_Color extends Core_Model_Abstract
{
    protected $_table = 'colors';   
    
    public function getFrontendStyle()
    {
        if ($this->getTitle() == 'White' || $this->getTitle() == 'Silver') {
            return 'border: 1px solid #DBDBDB !important; background-color: ' . $this->getHex();
        }

        if ($this->getHex() && $this->getTitle() != 'Pattern') {
            return 'background-color: ' . $this->getHex();
        }
        
        if ($this->getTitle() == 'Pattern') {
            return "background-image: url('" . Ikantam_Url::getPublicUrl('images/pattern.png') . "');";
        }
    }
}