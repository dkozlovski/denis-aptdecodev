<?php

class Catalog_Model_Manufacturer_Collection extends Core_Model_Abstract_Collection
{
    protected $_table = 'manufacturers';
    
    public function getFields()
    {
        return array('id', 'title');
    }
}