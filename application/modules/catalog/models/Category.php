<?php

class Catalog_Model_Category extends Core_Model_Abstract
{
    protected $_table = 'categories';
    
    public function getSubCategories()
    {
        $subcategories = new Catalog_Model_Category_Collection();
        $subcategories->addFilter('parent_id', $this->getId(), 'eq');
        return $subcategories;
    }
}