<?php

class Catalog_Model_Product extends Core_Model_Abstract
{
    protected $_table = 'products';
    
    public function getSalableProducts($userId)
    {
        $select = $this->select()
                ->where('user_id = ?', $userId)
                ->where('is_visible = ?', 1)
                ->where('is_sold = ?', 0)
                ->where('is_published = ?', 1);
        
        $rows = $this->fetchAll($select);
        $products = new Product_Model_Product2_Collection();
        foreach ($rows as $row) {
            $item = new Product_Model_Product($row->id);
            $products->addItem($item);
        }
        return $products;
    }
    
    public function getProductUrl()
    {
        if ($this->getPageUrl()) {
            return Ikantam_Url::getUrl('catalog/' . $this->getPageUrl());
        }
        
        return Ikantam_Url::getUrl('catalog/product/view', array('id' => $this->getId()));
    }
    
    public function getMainImageUrl($type)
    {
        if ($type == 'medium') {
            return 'http://localhost/aptdeco.local/public/upload/products/31Z9NzNOGbL.jpg';
        }
    }
    
    
}