<?php

class Catalog_Model_CatalogSearch
{

    protected $_filterData;
    protected $_page;
    protected $_query;
    protected $_itemsCountPerPage;
    protected $_sortOrder;
    protected $_facets;

    public function getFilterData()
    {
        return $this->_filterData;
    }

    public function setFilterData($filterData)
    {
        $this->_filterData = $filterData;
        return $this;
    }

    public function getPage()
    {
        return $this->_page;
    }

    public function setPage($page)
    {
        $page = (int) $page;

        if ($page < 1) {
            $page = 1;
        }

        $this->_page = $page - 1; //algolia pages start from 0
        return $this;
    }

    public function getQuery()
    {
        return $this->_query;
    }

    public function setQuery($query)
    {
        $this->_query = $query;
        return $this;
    }

    public function getItemsCountPerPage()
    {
        return $this->_itemsCountPerPage;
    }

    public function setItemsCountPerPage($query)
    {
        $this->_itemsCountPerPage = $query;
        return $this;
    }

    public function getSortOrder()
    {
        return $this->_sortOrder;
    }

    public function setSortOrder($sortOrder)
    {
        $this->_sortOrder = $sortOrder;
        return $this;
    }

    public function getFacets()
    {
        return $this->_facets;
    }

    public function setFacets($facets)
    {
        $this->_facets = $facets;
        return $this;
    }

    protected function _getClient($indexName, $readOnly = true)
    {
        $config = \Zend_Registry::get('config')->algolia;

        if ($readOnly) {
            $apiKey = $config->seachOnlyApiKey;
        } else {
            $apiKey = $config->apiKey;
        }

        $indexes = $config->indexes;

        $client = new \AlgoliaSearch\Client($config->applicationId, $apiKey);

        return $client->initIndex($indexes->$indexName);
    }

    protected function _getSlaveIndexes()
    {
        $indexes = \Zend_Registry::get('config')->algolia->indexes;

        return array(
            $indexes->created_at_asc,
            $indexes->created_at_desc,
            $indexes->price_asc,
            $indexes->price_desc,
            $indexes->seller_rating_asc,
            $indexes->seller_rating_desc
        );
    }

    public function deleteProductIndexById($productId)
    {
        $masterIndex = $this->_getClient('master', false);

        // delete the record
        $masterIndex->deleteObject((int) $productId);
    }

    public function deleteProductIndex($product)
    {
        $masterIndex = $this->_getClient('master', false);

        // delete the record
        $masterIndex->deleteObject((int) $product->getId());
    }

    public function updateProductIndex($product)
    {
        $row['id']                 = (int) $product->getId();
        $row['title']              = $product->getTitle();
        $row['description']        = $product->getDescription();
        $row['price']              = (float) $product->getPrice();
        $row['color_id']           = (int) $product->getColorId();
        $row['manufacturer_id']    = (int) $product->getManufacturerId();
        $row['category_id']        = (int) $product->getCategoryId();
        $row['created_at']         = (int) $product->getCreatedAt();
        $row['updated_at']         = (int) $product->getUpdatedAt();
        $row['qty']                = (int) ($product->getQty() > 0);
        $row['category_title']     = $product->getCategory()->getTitle();
        $row['condition_title']    = $product->getShortConditionLabel();
        $row['manufacturer_title'] = $product->getManufacturer()->getTitle();
        $row['color_title']        = $product->getColor()->getTitle();
        $row['material_title']     = $product->getMaterial()->getTitle();
        $row['seller_rating']      = (float) $product->getSeller()->getRating();
        $row['seller_name']        = (string) $product->getSeller()->getFirstName();
        $row['_tags']              = array($product->getCondition());
        $row['objectID']           = (int) $product->getId();
        $row['exp_time']           = time() + 36000000; //10000 hours (~14 months) from now

        $conciergeIds = \Zend_Registry::get('config')->algolia->concierge_ids->toArray();

        if (in_array($product->getUserId(), $conciergeIds)) {
            $row['is_adc'] = 1;
        } else {
            $row['is_adc'] = 0;
        }

        if ($product->getExpireAt() || $product->getAvailableTill()) {
            $expAt = (int) $product->getExpireAt();
            $aTill = (int) $product->getAvailableTill();

            if ($expAt > 0 && $aTill > 0) {
                $row['exp_time'] = ($expAt < $aTill) ? $expAt : $aTill;
            } elseif ($expAt > 0) {
                $row['exp_time'] = $expAt;
            } elseif ($aTill > 0) {
                $row['exp_time'] = $aTill;
            }
        }
        $row['is_expired'] = (int) ($row['exp_time'] < time());

        $row['page_url']  = $product->getProductUrl();
        $row['image_url'] = $product->getMainImageUrl('autocomplete');

        $masterIndex = $this->_getClient('master', false);

        // the record is created if it doesn't exist
        $masterIndex->saveObject($row);
    }

    public function createIndex($result)
    {
        $masterIndex = $this->_getClient('master', false);

        $attributesToIndex = array(
            'title',
            'category_title',
            'manufacturer_title',
            'material_title',
            'description',
            'color_title',
            'condition_title',
            'seller_name',
        );

        $ranking = array(
            'custom',
            'typo',
            'geo',
            'proximity',
            'attribute',
            'exact',
        );

        $attributesToRetrieve = array(
            'id',
            'category_id',
            'color_id',
            'price',
            'manufacturer_id'
        );

        $attributesForFaceting = array(
            'category_id',
            'price',
            'color_id',
            'manufacturer_id'
        );

        $masterIndex->setSettings(array(
            'attributesToIndex'     => $attributesToIndex,
            'attributesToRetrieve'  => $attributesToRetrieve,
            'attributesForFaceting' => $attributesForFaceting,
            //'ranking'               => $ranking,
            'slaves'                => $this->_getSlaveIndexes(),
            'customRanking'         => array('asc(is_adc)')
        ));



        if ($result) {
            $batch = array();
            // iterate over results and send them by batch of 10000 elements

            foreach ($result as $row) {
                // select the identifier of this row
                $row['id']              = (int) $row['id'];
                $row['price']           = (float) $row['price'];
                $row['color_id']        = (int) $row['color_id'];
                $row['manufacturer_id'] = (int) $row['manufacturer_id'];
                $row['category_id']     = (int) $row['category_id'];
                $row['created_at']      = (int) $row['created_at'];
                $row['updated_at']      = (int) $row['updated_at'];
                $row['seller_rating']   = (float) $row['seller_rating'];
                $row['_tags']           = array($row['condition']);
                $row['qty']             = (int) ($row['qty'] > 0);
                $row['objectID']        = (int) $row['id'];
                $row['exp_time']        = time() + 36000000; //10000 hours (~14 months) from now

                if ($row['expire_at'] || $row['available_till']) {
                    $expAt = (int) $row['expire_at'];
                    $aTill = (int) $row['available_till'];

                    if ($expAt > 0 && $aTill > 0) {
                        $row['exp_time'] = ($expAt < $aTill) ? $expAt : $aTill;
                    } elseif ($expAt > 0) {
                        $row['exp_time'] = $expAt;
                    } elseif ($aTill > 0) {
                        $row['exp_time'] = $aTill;
                    }
                }
                $row['is_expired'] = (int) ($row['exp_time'] < time());

                if (!empty($row['page_url'])) {
                    $row['page_url'] = Ikantam_Url::getUrl('catalog/' . $row['page_url']);
                } else {
                    $row['page_url'] = Ikantam_Url::getUrl('product/view/index', array('id' => $row['id']));
                }

                $row['image_url'] = '';

                $product = new Product_Model_Product($row['id']);

                if ($product->getId()) {
                    $row['image_url'] = $product->getMainImageUrl('autocomplete');
                }

                $conciergeIds = \Zend_Registry::get('config')->algolia->concierge_ids->toArray();

                if (in_array($product->getUserId(), $conciergeIds)) {
                    $row['is_adc'] = 1;
                } else {
                    $row['is_adc'] = 0;
                }

                $row['seller_name'] = (string) $product->getSeller()->getFirstName();

                unset($row['condition']);
                unset($row['expire_at']);
                unset($row['available_till']);

                array_push($batch, $row);

                if (count($batch) == 10000) {
                    $masterIndex->saveObjects($batch);
                    $batch = array();
                }
            }

            $masterIndex->saveObjects($batch);
        }


        $indexCreatedAtAsc     = $this->_getClient('created_at_asc', false);
        $indexCreatedAtDesc    = $this->_getClient('created_at_desc', false);
        $indexPriceAsc         = $this->_getClient('price_asc', false);
        $indexPriceDesc        = $this->_getClient('price_desc', false);
        $indexSellerRatingAsc  = $this->_getClient('seller_rating_asc', false);
        $indexSellerRatingDesc = $this->_getClient('seller_rating_desc', false);

        $indexCreatedAtAsc->setSettings(array(
            'attributesToIndex'     => $attributesToIndex,
            'attributesToRetrieve'  => $attributesToRetrieve,
            'attributesForFaceting' => $attributesForFaceting,
            'ranking'               => $ranking,
            'customRanking'         => array('desc(qty)', 'asc(is_expired)', 'asc(created_at)')
        ));

        $indexCreatedAtDesc->setSettings(array(
            'attributesToIndex'     => $attributesToIndex,
            'attributesToRetrieve'  => $attributesToRetrieve,
            'attributesForFaceting' => $attributesForFaceting,
            'ranking'               => $ranking,
            'customRanking'         => array('desc(qty)', 'asc(is_expired)', 'desc(created_at)')
        ));

        $indexPriceAsc->setSettings(array(
            'attributesToIndex'     => $attributesToIndex,
            'attributesToRetrieve'  => $attributesToRetrieve,
            'attributesForFaceting' => $attributesForFaceting,
            'ranking'               => $ranking,
            'customRanking'         => array('desc(qty)', 'asc(is_expired)', 'asc(price)')
        ));

        $indexPriceDesc->setSettings(array(
            'attributesToIndex'     => $attributesToIndex,
            'attributesToRetrieve'  => $attributesToRetrieve,
            'attributesForFaceting' => $attributesForFaceting,
            'ranking'               => $ranking,
            'customRanking'         => array('desc(qty)', 'asc(is_expired)', 'desc(price)')
        ));

        $indexSellerRatingAsc->setSettings(array(
            'attributesToIndex'     => $attributesToIndex,
            'attributesToRetrieve'  => $attributesToRetrieve,
            'attributesForFaceting' => $attributesForFaceting,
            'ranking'               => $ranking,
            'customRanking'         => array('desc(qty)', 'asc(is_expired)', 'asc(seller_rating)')
        ));

        $indexSellerRatingDesc->setSettings(array(
            'attributesToIndex'     => $attributesToIndex,
            'attributesToRetrieve'  => $attributesToRetrieve,
            'attributesForFaceting' => $attributesForFaceting,
            'ranking'               => $ranking,
            'customRanking'         => array('desc(qty)', 'asc(is_expired)', 'desc(seller_rating)')
        ));
    }

    protected function _out($arr1, $arr2 = array(), $arr3 = array(), $page, $limit)
    {
        $out = array();

        if (isset($arr1['hits'])) {
            foreach ($arr1['hits'] as $hit) {
                $out[] = $hit;
            }
        }

        if (isset($arr2['hits'])) {
            foreach ($arr2['hits'] as $hit) {
                $out[] = $hit;
            }
        }

        if (isset($arr3['hits'])) {
            foreach ($arr3['hits'] as $hit) {
                $out[] = $hit;
            }
        }

        return array_slice($out, $page * $limit, $limit);
    }

    public function search()
    {
        $order = $this->getSortOrder();
        $data  = $this->getFilterData();

        if ($order === null) {//no sorting selected, perform query against master index
            $searchClient = $this->_getClient('master', true);

            if (!isset($data['only_available']) || $data['only_available'] != true) {
                $origPage  = $this->getPage();
                $origLimit = $this->getItemsCountPerPage();

                $results = $searchClient->search($this->getQuery(), $this->_getParams());

                //get facets for all results
                if (isset($results['facets'])) {
                    $this->setFacets($results['facets']);
                }

                //at first return available products
                $this->_filterData['only_available'] = 1;
                $this->setPage(1);
                $this->setItemsCountPerPage(1000);

                $results1 = $searchClient->search($this->getQuery(), $this->_getParams());

                if (!isset($results1['hits']) || !is_array($results1['hits'])) {
                    return array();
                }

                //then return expired products
                $this->setPage(1);
                $this->setItemsCountPerPage(1000);

                $this->_filterData['only_available'] = 0;
                $this->_filterData['only_expired']   = 1;

                $results2 = $searchClient->search($this->getQuery(), $this->_getParams());

                if (!isset($results2['hits']) || !is_array($results2['hits'])) {
                    return $this->_out($results1, array(), array(), $origPage, $origLimit);
                }

                //and then return sold products
                $this->_filterData['only_expired'] = 0;
                $this->_filterData['only_sold']    = 1;

                $this->setPage(1);
                $this->setItemsCountPerPage(1000);

                $results3 = $searchClient->search($this->getQuery(), $this->_getParams());

                if (!isset($results3['hits']) || !is_array($results3['hits'])) {
                    return $this->_out($results1, $results2, array(), $origPage, $origLimit);
                }

                return $this->_out($results1, $results2, $results3, $origPage, $origLimit);
            }


            /* if (!isset($data['only_available']) || $data['only_available'] != true) {
              $results = $searchClient->search($this->getQuery(), $this->_getParams());

              //get facets for all results
              if (isset($results['facets'])) {
              $this->setFacets($results['facets']);
              }

              //at first return available products
              $this->_filterData['only_available'] = 1;
              $results1                            = $searchClient->search($this->getQuery(), $this->_getParams());

              if (!isset($results1['hits']) || !is_array($results1['hits'])) {
              return array();
              }

              if (count($results1['hits']) == $this->getItemsCountPerPage()) {
              return $this->_out($results1);
              } elseif (count($results1['hits']) == 0) {//no results, now query for unavailable products
              $resCount             = 0;
              $currentPage          = $this->_page + 1;
              $lastPageWithProducts = $currentPage;

              do {
              $lastPageWithProducts = $lastPageWithProducts - 1;
              $this->setPage($lastPageWithProducts);

              $results = $searchClient->search($this->getQuery(), $this->_getParams());

              if (!isset($results['hits']) || !is_array($results['hits'])) {
              return $this->_out($results1);
              }

              $resCount = count($results['hits']);
              } while ($resCount < 1 && $lastPageWithProducts > 1);

              $this->setPage($currentPage - $lastPageWithProducts);

              $this->_filterData['only_available'] = 0;
              $this->_filterData['only_expired']   = 1;

              $results2 = $searchClient->search($this->getQuery(), $this->_getParams());

              if (!isset($results2['hits']) || !is_array($results2['hits'])) {
              return $this->_out($results1);
              }

              if (count($results2['hits']) == $this->getItemsCountPerPage()) {
              return $this->_out($results1, $results2);
              } elseif (count($results2['hits']) > 0) {//not enough results, query for sold products
              $this->_filterData['only_expired'] = 0;
              $this->_filterData['only_sold']    = 1;

              $this->setPage(1);
              $this->setItemsCountPerPage($this->_itemsCountPerPage - count($results2['hits']));

              $results3 = $searchClient->search($this->getQuery(), $this->_getParams());

              if (!isset($results3['hits']) || !is_array($results3['hits'])) {
              return $this->_out($results1, $results2);
              }

              return $this->_out($results1, $results2, $results3);
              } else {//no results, query for sold products
              $resCount             = 0;
              $currentPage          = $this->_page + 1;
              $lastPageWithProducts = $currentPage;

              do {
              $lastPageWithProducts = $lastPageWithProducts - 1;
              $this->setPage($lastPageWithProducts);

              $results = $searchClient->search($this->getQuery(), $this->_getParams());

              if (!isset($results['hits']) || !is_array($results['hits'])) {
              return $this->_out($results1);
              }

              $resCount = count($results['hits']);
              } while ($resCount < 1 && $lastPageWithProducts > 1);

              $this->setPage($currentPage - $lastPageWithProducts);

              $this->_filterData['only_expired'] = 0;
              $this->_filterData['only_sold']    = 1;

              $results3 = $searchClient->search($this->getQuery(), $this->_getParams());

              if (!isset($results3['hits']) || !is_array($results3['hits'])) {
              return $this->_out($results1, $results2);
              }

              return $this->_out($results1, $results2, $results3);
              }
              } else {//not enough results, query for not available products
              $this->_filterData['only_available'] = 0;
              $this->_filterData['only_expired']   = 1;

              $this->setPage(1);
              $this->setItemsCountPerPage($this->_itemsCountPerPage - count($results1['hits']));

              $results2 = $searchClient->search($this->getQuery(), $this->_getParams());

              if (!isset($results2['hits']) || !is_array($results2['hits'])) {
              return $this->_out($results1);
              }

              if (count($results2['hits']) == $this->getItemsCountPerPage()) {
              return $this->_out($results1, $results2);
              } else {//not enough results, query for sold products
              $this->_filterData['only_expired'] = 0;
              $this->_filterData['only_sold']    = 1;

              if (count($results2['hits']) > 0) {
              $this->setPage(1);
              $this->setItemsCountPerPage($this->_itemsCountPerPage - count($results2['hits']));
              }

              $results3 = $searchClient->search($this->getQuery(), $this->_getParams());

              if (!isset($results3['hits']) || !is_array($results3['hits'])) {
              return $this->_out($results1, $results2);
              }

              return $this->_out($results1, $results2, $results3);
              }
              }

              return array();
              } */
        } else {
            $searchClient = $this->_getClient($order['field'] . '_' . $order['direction'], true);
        }

        $results = $searchClient->search($this->getQuery(), $this->_getParams());

        if (isset($results['facets'])) {
            $this->setFacets($results['facets']);
        }

        if (isset($results['hits']) && is_array($results['hits']) && count($results['hits']) > 0) {
            return $results['hits'];
        }

        return array();
    }

    public function _getOrderParam($request)
    {
        $order = null;

        if ($request->getParam('order_price')) {
            $order = array(
                'field'     => 'price',
                'direction' => ($request->getParam('order_price') == 'asc') ? 'asc' : 'desc'
            );
        } elseif ($request->getParam('order_rating')) {
            $order = array(
                'field'     => 'seller_rating',
                'direction' => ($request->getParam('order_rating') == 'asc') ? 'asc' : 'desc'
            );
        } elseif ($request->getParam('order_date')) {
            $order = array(
                'field'     => 'created_at',
                'direction' => ($request->getParam('order_date') == 'asc') ? 'asc' : 'desc'
            );
        }

        return $order;
    }

    public function _getFacets($query, $products)
    {
        //facets
        $categories    = array();
        $colors        = array();
        $manufacturers = array();
        $prices        = array();

        if ($products->getSize() > 0) {//search query + filters has returned some results
            foreach ($this->getFacets() as $type => $facets) {
                foreach ($facets as $value => $count) {
                    if ($type == 'category_id') {
                        $categories[] = (int) $value;
                    } elseif ($type == 'color_id') {
                        $colors[] = (int) $value;
                    } elseif ($type == 'manufacturer_id') {
                        $manufacturers[] = (int) $value;
                    } elseif ($type == 'price') {
                        $prices[] = (float) $value;
                    }
                }
            }
        } else {//disable filters & run query again
            $catalogSearch2 = new Catalog_Model_CatalogSearch();
            $catalogSearch2->setQuery($query)
                    ->setSortOrder(array('field' => 'created_at', 'direction' => 'desc'));

            $catalogSearch2->search();

            foreach ($catalogSearch2->getFacets() as $type => $facets) {
                foreach ($facets as $value => $count) {
                    if ($type == 'category_id') {
                        $categories[] = (int) $value;
                    } elseif ($type == 'color_id') {
                        $colors[] = (int) $value;
                    } elseif ($type == 'manufacturer_id') {
                        $manufacturers[] = (int) $value;
                    } elseif ($type == 'price') {
                        $prices[] = (float) $value;
                    }
                }
            }

            if (count($prices) < 1) {//no products match query
                $prices[] = 0;
            }
        }//end of facets

        $categories2    = array();
        $colors2        = array();
        $manufacturers2 = array();

        foreach (array_unique($categories) as $categoryId) {
            $category = new Category_Model_Category($categoryId);
            if ($category->getId()) {
                $categories2[$category->getId()] = $category->getTitle();
            }
        }

        foreach (array_unique($colors) as $colorId) {
            $color = new Application_Model_Color($colorId);
            if ($color->getId()) {
                $colors2[$color->getId()] = $color;
            }
        }

        foreach (array_unique($manufacturers) as $manufacturerId) {
            $manufacturer = new Application_Model_Manufacturer($manufacturerId);
            if ($manufacturer->getId()) {
                $manufacturers2[$manufacturer->getId()] = $manufacturer->getTitle();
            }
        }

        return array($categories2, $colors2, $manufacturers2, $prices);
    }

    protected function _getParams()
    {
        $numericFilters = $this->_getNumericFilters();
        $tagFilters     = $this->_getTagFilters();

        $params = array(
            'page'        => $this->getPage(),
            'hitsPerPage' => $this->getItemsCountPerPage(),
        );

        if (count($numericFilters) > 0) {
            $params['numericFilters'] = implode(',', $numericFilters);
        }

        if (count($tagFilters) > 0) {
            $params['tagFilters'] = implode(',', $tagFilters);
        }

        $params['facets'] = '*';

        return $params;
    }

    protected function _getNumericFilters()
    {
        $data           = $this->getFilterData();
        $numericFilters = array();

        if (!empty($data['min_price'])) {
            $numericFilters[] = 'price>=' . (float) $data['min_price'];
        }

        if (!empty($data['max_price'])) {
            $numericFilters[] = 'price<=' . (float) $data['max_price'];
        }

        if (!empty($data['colors']) && is_array($data['colors']) && count($data['colors']) > 0) {
            $colorFilter = array();
            foreach ($data['colors'] as $color) {
                $colorFilter[] = 'color_id=' . (int) $color;
            }

            if (count($colorFilter) > 1) {
                $numericFilters[] = '(' . implode(',', $colorFilter) . ')';
            } else {
                $numericFilters[] = current($colorFilter);
            }
        }

        if (!empty($data['brands']) && is_array($data['brands']) && count($data['brands']) > 0) {
            $brandFilter = array();
            foreach ($data['brands'] as $brand) {
                $brandFilter[] = 'manufacturer_id=' . (int) $brand;
            }

            if (count($brandFilter) > 1) {
                $numericFilters[] = '(' . implode(',', $brandFilter) . ')';
            } else {
                $numericFilters[] = current($brandFilter);
            }
        }

        if (!empty($data['categories']) && is_array($data['categories']) && count($data['categories']) > 0) {
            $categoryFilter = array();
            foreach ($data['categories'] as $category) {
                $categoryFilter[] = 'category_id=' . (int) $category;
            }

            if (count($categoryFilter) > 1) {
                $numericFilters[] = '(' . implode(',', $categoryFilter) . ')';
            } else {
                $numericFilters[] = current($categoryFilter);
            }
        }

        if (isset($data['only_available']) && $data['only_available'] == true) {
            $numericFilters[] = 'qty=1,is_expired=0,exp_time>' . time();
        }

        if (isset($data['only_expired']) && $data['only_expired'] == true) {
            $numericFilters[] = 'qty=1,is_expired=1,exp_time<=' . time();
        }

        if (isset($data['only_sold']) && $data['only_sold'] == true) {
            $numericFilters[] = 'qty=0';
        }


        return $numericFilters;
    }

    protected function _getTagFilters()
    {
        $data       = $this->getFilterData();
        $tagFilters = array();

        if (!empty($data['conditions']) && is_array($data['conditions']) && count($data['conditions']) > 0) {
            $conditionFilter = array();
            foreach ($data['conditions'] as $condition) {
                $conditionFilter[] = $condition;
            }

            if (count($conditionFilter) > 1) {
                $tagFilters[] = '(' . implode(',', $conditionFilter) . ')';
            } else {
                $tagFilters[] = current($conditionFilter);
            }
        }

        return $tagFilters;
    }

}