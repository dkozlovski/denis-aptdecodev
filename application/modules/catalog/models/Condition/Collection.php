<?php

class Catalog_Model_Condition_Collection extends Core_Model_Abstract_Collection
{
    protected $_table = 'conditions';
    
    public function getFields()
    {
        return array('id', 'code', 'title', 'short_title');
    }
}