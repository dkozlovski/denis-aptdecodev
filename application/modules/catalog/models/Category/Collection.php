<?php

class Catalog_Model_Category_Collection extends Core_Model_Abstract_Collection
{
    protected $_table = 'categories';
    
    public function getFields()
    {
        return array('id', 'parent_id', 'title', 'sort_order');
    }
}