<?php

class Catalog_Model_Product_Backend extends Core_Model_Abstract_Backend
{
    public function getMinPrice()
    {
        $sql = 'SELECT MIN(`price`) as `min_price` FROM `products` WHERE `is_visible` = 1 AND `is_published` = 1 AND `qty` > 0';
        
        $stmt = $this->query($sql, array());
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if ($row && isset($row['min_price'])) {
            return (float) $row['min_price'];
        }
        return 0;
    }
    
    public function getMaxPrice()
    {
        $sql = 'SELECT MAX(`price`) as `min_price` FROM `products` WHERE `is_visible` = 1 AND `is_published` = 1 AND `qty` > 0';
        
        $stmt = $this->query($sql, array());
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if ($row && isset($row['min_price'])) {
            return (float) $row['min_price'];
        }
        return 0;
    }
}