<?php

class Catalog_Model_Product_Collection extends Core_Model_Abstract_Collection
{
    protected $_table = 'products';
    
    public function getFields()
    {
        return array('id',  
            'user_id',
            'title',
            'description',
            'price',
            'original_price',
            'category_id',
            'manufacturer_id',
            'condition',
            'material_id',
            'color_id',
            'width',
            'height',
            'depth',
            'age',
            'available_from',
            'available_till',
            'is_published',
            'created_at',
            'is_pick_up_available',
            'is_delivery_available',
            'is_available_for_purchase',
            'is_visible',
            'pick_up_address_id',
            'is_approved',
            'qty'
            );
    }
    
    public function getMinPrice()
    {
        return $this->_getBackend()->getMinPrice();
    }
    
    public function getMaxPrice()
    {
        return $this->_getBackend()->getMaxPrice();
    }
}