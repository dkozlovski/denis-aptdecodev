<?php

class Catalog_BrandController extends Ikantam_Controller_Front
{

    protected $_itemsCountPerPage = 42;
    protected $_maxCountPage;
    protected $_maxLinkPage = 6;

    protected function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/hash.uri.js'));
    }

    public function indexAction()
    {
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('js/plugins/iCheck/skins/flat/aptdeco.css'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/plugins/iCheck/icheck.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery.mousewheel.min.js'));

        $form             = new Category_Form_ProductFilter();
        $this->view->form = $form;

        $itemsPerPage   = $this->_itemsCountPerPage;
        $page           = 1;
        $data           = $this->getRequest()->getParams();
        $manufacturerId = $this->getRequest()->getParam('id');

        $this->view->manufacturer = new Application_Model_Manufacturer($manufacturerId);
        $this->view->headTitle($this->view->manufacturer->getTitle());
        $this->view->appendHeader = false;



        if ($form->isValid($data)) {

            $offset = ($page - 1) * $itemsPerPage;

            $data = $form->getValues();

            if (empty($data['min_price'])) {
                unset($data['min_price']);
            }
            if (empty($data['max_price'])) {
                unset($data['max_price']);
            }
            if (empty($data['colors']) || !is_array($data['colors']) || !count($data['colors'])) {
                unset($data['colors']);
            }
            if (empty($data['brands']) || !is_array($data['brands']) || !count($data['brands'])) {
                unset($data['brands']);
            }
            if (empty($data['categories']) || !is_array($data['categories']) || !count($data['categories'])) {
                unset($data['categories']);
            }
            if (empty($data['conditions']) || !is_array($data['conditions']) || !count($data['conditions'])) {
                unset($data['conditions']);
            }

            $data['is_approved'] = 1;
            $data['brands']      = array($manufacturerId);

            $c = new Product_Model_Product2_Collection();
            ###
            $maxCountElement = clone $c;
            $maxCountElement = count($maxCountElement->setFilters($data)->load());
            ###
            $c->setFilters($data)
                    ->setOrder(array('is_product_sold' => 'asc', 'is_expired' => 'asc', 'created_at' => 'desc'))
                    ->setOffset($offset)
                    ->setLimit($itemsPerPage)
                    ->load();
            ###
            $this->_maxCountPage = ceil($maxCountElement/$itemsPerPage);
            ###
            $products = $c;

            $this->view->products = $products;
            ###
            $this->view->currentPage = 1;
            $this->view->maxCountPage = $this->_maxCountPage;
            if($this->_maxCountPage < $this->_maxLinkPage){
                $this->view->maxLinkPage = $this->_maxCountPage;
            }else{
                $this->view->maxLinkPage = $this->_maxLinkPage;
            }
            ###
            if ($products->getSize() == 0) {
                $this->view->appendHeader = true;
            }

            $availableCategories = array();
            $availableColors     = array();

            $categories = new Category_Model_Category_Collection();
            $colors     = new Application_Model_Color_Collection();

            foreach ($categories->getAllSubcategoriesByManufacturer($manufacturerId) as $category) {
                $availableCategories[$category->getId()] = $category->getTitle();
            }

            foreach ($colors->getByManufacturer($manufacturerId) as $color) {
                $availableColors[$color->getId()] = $color;
            }

            $this->view->form->getElement('categories')->setMultiOptions($availableCategories);
            $this->view->form->getElement('colors')->setMultiOptions($availableColors);
            $this->view->form->removeElement('manufacturer_note');
            $this->view->form->removeElement('manufacturer_slider');
            $this->view->form->removeElement('brands');

            $productsCollection = new Product_Model_Product_Collection();

            $this->view->minPrice = (int) floor($productsCollection->getMinPriceByManufacturer($manufacturerId) / 10) * 10;
            $this->view->maxPrice = (int) ceil($productsCollection->getMaxPriceByManufacturer($manufacturerId) / 10 * 10);

            $this->view->selectedMinPrice = (int) $this->getRequest()->getParam('min_price', $this->view->minPrice);
            $this->view->selectedMaxPrice = (int) $this->getRequest()->getParam('max_price', $this->view->maxPrice);

            $this->view->form->populate(array(
                'min_price'  => $this->view->selectedMinPrice,
                'max_price'  => $this->view->selectedMaxPrice,
                'categories' => (array) $this->getRequest()->getParam('categories'),
                'brands'     => (array) $this->getRequest()->getParam('brands'),
                'colors'     => (array) $this->getRequest()->getParam('colors'),
                'conditions' => (array) $this->getRequest()->getParam('conditions')
            ));
        }
    }

    public function ajaxAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $session  = $this->getSession();
        $pageHash = $session->getPageHash();

        $response                 = array(
            'success'  => false,
            'error'    => 'Cannot add item to cart',
            'message'  => '',
            'subtotal' => ''
        );
        $this->view->appendHeader = false;
        $hp                       = $this->getRequest()->getParam('history_page', false);
        $itemsPerPage             = $this->_itemsCountPerPage;

        $page = (int) $this->getRequest()->getParam('page', 1);
        $data = $this->getRequest()->getParams();

        $form = new Category_Form_ProductFilter();

        if ($page < 1) {
            $page = 1;
        }

        if ($form->isValid($data)) {


            $offset = ($page - 1) * $itemsPerPage;

            $data = $form->getValues();
            unset($data['query']);

            if (empty($data['min_price'])) {
                unset($data['min_price']);
            }
            if (empty($data['max_price'])) {
                unset($data['max_price']);
            }
            if (empty($data['colors']) || !is_array($data['colors']) || !count($data['colors'])) {
                unset($data['colors']);
            }
            if (empty($data['brands']) || !is_array($data['brands']) || !count($data['brands'])) {
                unset($data['brands']);
            }
            if (empty($data['categories']) || !is_array($data['categories']) || !count($data['categories'])) {
                unset($data['categories']);
            }
            if (empty($data['conditions']) || !is_array($data['conditions']) || !count($data['conditions'])) {
                unset($data['conditions']);
            }

            $manufacturerId = $this->getRequest()->getParam('id');

            $data['is_approved'] = 1;
            $data['brands']      = array($manufacturerId);

            $c = new Product_Model_Product2_Collection();
            ###
            $maxCountElement = clone $c;
            $maxCountElement = count($maxCountElement->setFilters($data)->load());
            ###
            if ($hp) {
                //$offset       = 0;
                //$itemsPerPage = $hp * $this->_itemsCountPerPage - $this->_itemsCountPerPage;
            }

            $c->setFilters($data)
                    ->setOffset($offset)
                    ->setLimit($itemsPerPage)
                    ->setOrder(array('is_product_sold' => 'asc', 'is_expired' => 'asc'));


            $orderPrice  = $this->getRequest()->getParam('order_price');
            $orderRating = $this->getRequest()->getParam('order_rating');
            $orderDate   = $this->getRequest()->getParam('order_date');

            if ($orderPrice == 'asc' || $orderPrice == 'desc') {
                $c->addOrder('price', $orderPrice);
            } elseif ($orderRating == 'asc' || $orderRating == 'desc') {
                $c->addOrder('seller_rating', $orderRating);
            } elseif ($orderDate == 'asc' || $orderDate == 'desc') {
                $c->addOrder('created_at', $orderDate);
            } else {
                $c->addOrder('updated_at', 'desc')
                        ->addOrder('created_at', 'desc');
            }

            $c->load();
            $products = $c;
            ###
            $this->view->manufacturer = new Application_Model_Manufacturer($manufacturerId);
            $this->_maxCountPage = ceil($maxCountElement/$itemsPerPage);
            $this->view->currentPage = (int)$this->getRequest()->getParam('page');
            $this->view->maxCountPage = $this->_maxCountPage;
            if($this->_maxCountPage < $this->_maxLinkPage){
                $this->view->maxLinkPage = $this->_maxCountPage;
            }else{
                $this->view->maxLinkPage = $this->_maxLinkPage;
            }
            ###

//			$products = new Product_Model_Product_Collection();
//			$offset = ($page - 1) * $itemsPerPage;
//			$products->filterAndFilter($form->getValues(), $offset, $itemsPerPage);

            if ($products->getSize() == 0 && $page == 1) {
                $this->view->appendHeader = true;
            }

            $this->view->products = $products;

            $this->view->setScriptPath(APPLICATION_PATH . '/views/scripts');
            $response['template'] = $this->view->render('search/search_items.phtml');


            $response['success'] = true;
        } else {

        }

        $this->getResponse()
                ->setHeader('Content-type', 'application/json')
                ->setBody(json_encode($response));
    }

}