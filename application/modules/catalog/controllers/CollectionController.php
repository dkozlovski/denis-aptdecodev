<?php

// For this controller used routes. See application bootstrap
class Catalog_CollectionController extends Ikantam_Controller_CatalogAbstract
{

    protected $_itemsCountPerPage = 42;
    protected $_maxCountPage;
    protected $_maxLinkPage = 6;
    
    protected function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/hash.uri.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery.autocomplete.js'));
    }

    public function indexAction()
    {
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('js/plugins/iCheck/skins/flat/aptdeco.css'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/plugins/iCheck/icheck.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery.mousewheel.min.js'));

        $form             = new Category_Form_ProductFilter();
        $this->view->form = $form;

        $set = new Application_Model_Set();
        $set->getBy_page_url($this->getRequest()->getParam('slug'));

        $metaInfo=array('Sale'=>array('furniture','furniture'),
            "Kid's Corner"=>array('kids','kids'),
            'Vintage + Antiques'=>array('vintage furniture','antique')
        );
        $setName=$set->getName();
        $this->view->pageTitle = $setName;
        if(array_key_exists($setName,$metaInfo)){
            $this->view->headTitle(sprintf('Used %s for sale in NYC',$metaInfo[$setName][0]));
            $msg=sprintf('Used %s for sale in NYC available for delivery through AptDeco. Browse gently used furniture'.
                ' from a trusted community of sellers and arrange for affordable delivery on AptDeco.',$metaInfo[$setName][1]);
            $this->view->headMeta()->appendName('description', $msg);
        }

        $itemsPerPage = $this->_itemsCountPerPage;
        $page         = 1;

        $data                     = $this->getRequest()->getParams();
        $this->view->appendHeader = false;


        if ($form->isValid($data)) {

            $offset = ($page - 1) * $itemsPerPage;
            ######
            $maxCountElement = count($set->getProducts(null, null, $this->getRequest()));
            $this->_maxCountPage = ceil($maxCountElement/$itemsPerPage);
            $this->view->currentPage = 1;
            $this->view->maxCountPage = $this->_maxCountPage;
            if($this->_maxCountPage < $this->_maxLinkPage){
                $this->view->maxLinkPage = $this->_maxCountPage;
            }else{
                $this->view->maxLinkPage = $this->_maxLinkPage;
            }
            ######
            $products = $set->getProducts($itemsPerPage, $offset, $this->getRequest());

            $this->view->products   = $products;
            $this->view->collection = $set;

            $alertParams = Product_Model_Alert::getParamsFromRequest($this->getRequest());
            $alertParams['is_ajax'] = 1;
            $alertParams['collection_id'] = $set->getId();
            if ($products->getSize() == 0) {
                $this->view->appendHeader = true;
                $this->view->params = $alertParams;
            }

            $categoriesC = new Category_Model_Category_Collection();
            $categories  = $categoriesC->getFlexFilter()
                    ->id_of_set('=', $set->getId())
                    ->products_approved('=', 1)
                    ->products_visible('=', 1)
                    ->products_published('=', 1)
                    ->group('id')
                    ->order('title', 'asc')
                    ->apply();

            $colorsC = new Application_Model_Color_Collection();
            $colors  = $colorsC->getFlexFilter()
                    ->id_of_set('=', $set->getId())
                    ->colors_active('=', 1)
                    ->products_approved('=', 1)
                    ->products_visible('=', 1)
                    ->products_published('=', 1)
                    ->group('id')
                    ->order('sort_order', 'asc')
                    ->apply();

            $brandsC = new Application_Model_Manufacturer_Collection();
            $brands  = $brandsC->getFlexFilter()
                    ->id_of_set('=', $set->getId())
                    ->products_approved('=', 1)
                    ->products_visible('=', 1)
                    ->products_published('=', 1)
                    ->group('id')
                    ->apply();

            $availableCategories = array();
            $availableColors     = array();
            $availableBrands     = array();

            foreach ($categories as $category) {
                $availableCategories[$category->getId()] = $category->getTitle();
            }

            foreach ($colors as $color) {
                $availableColors[$color->getId()] = $color;
            }
            foreach ($brands as $brand) {
                $availableBrands[$brand->getId()] = $brand->getTitle();
            }

            $this->view->form->getElement('categories')->setMultiOptions($availableCategories);
            $this->view->form->getElement('colors')->setMultiOptions($availableColors);
            $this->view->form->getElement('brands')->setMultiOptions($availableBrands);

            $this->view->minPrice = (int) floor($set->productsFilter(null, true)->order('price')->apply(1)->getFirstItem()->getPrice() / 10) * 10;
            $this->view->maxPrice = (int) ceil($set->productsFilter(null, true)->order('price', 'desc')->apply(1)->getFirstItem()->getPrice() / 10) * 10;

            $this->view->selectedMinPrice = (int) $this->getRequest()->getParam('min_price', $this->view->minPrice);
            $this->view->selectedMaxPrice = (int) $this->getRequest()->getParam('max_price', $this->view->maxPrice);

            $this->view->form->populate(array(
                'min_price'  => $this->view->selectedMinPrice,
                'max_price'  => $this->view->selectedMaxPrice,
                'categories' => (array) $this->getRequest()->getParam('categories'),
                'brands'     => (array) $this->getRequest()->getParam('brands'),
                'colors'     => (array) $this->getRequest()->getParam('colors'),
                'conditions' => (array) $this->getRequest()->getParam('conditions')
            ));
        }
    }

    public function ajaxAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $session  = $this->getSession();
        $pageHash = $session->getPageHash();

        $response = array(
            'success'  => false,
            'error'    => 'Cannot add item to cart',
            'message'  => '',
            'subtotal' => ''
        );

        $this->view->appendHeader = false;
        $hp                       = $this->getRequest()->getParam('history_page', false);
        $itemsPerPage             = $this->_itemsCountPerPage;

        $page = (int) $this->getRequest()->getParam('page');

        if ($page < 1) {
            $page = 1;
        }

        $data = $this->getRequest()->getParams();

        $form = new Product_Form_Filter2();

        if ($form->isValid($data)) {
            $offset = ($page - 1) * $itemsPerPage;

            if ($hp) {
               // $offset       = 0;
               // $itemsPerPage = $hp * $this->_itemsCountPerPage - $this->_itemsCountPerPage;
            }

            $set      = new Application_Model_Set((int) $data['id']);
            ######
            $maxCountElement = count($set->getProducts(null, null, $this->getRequest()));
            $this->_maxCountPage = ceil($maxCountElement/$itemsPerPage);
            $this->view->currentPage = (int) $this->getRequest()->getParam('page');
            $this->view->maxCountPage = $this->_maxCountPage;
            if($this->_maxCountPage < $this->_maxLinkPage){
                $this->view->maxLinkPage = $this->_maxCountPage;
            }else{
                $this->view->maxLinkPage = $this->_maxLinkPage;
            }
            ######
            $products = $set->getProducts($itemsPerPage, $offset, $this->getRequest());
            $this->view->collection = $set;

            $alertParams = Product_Model_Alert::getParamsFromRequest($this->getRequest());
            $alertParams['is_ajax'] = 1;
            $alertParams['collection_id'] = $set->getId();

            if ($products->getSize() == 0) {
                $this->view->appendHeader = true;
                $this->view->params = $alertParams;

            }

            $this->view->products = $products;

            $this->view->setScriptPath(APPLICATION_PATH . '/views/scripts');
            $response['template'] = $this->view->render('search/search_items.phtml');
            $response['alert_url'] = Ikantam_Url::getUrl('product/alert/popup', $alertParams);
            $response['success'] = true;
        } else {
            
        }

        $this->getResponse()
                ->setHeader('Content-type', 'application/json')
                ->setBody(Zend_Json::encode($response));
    }

}