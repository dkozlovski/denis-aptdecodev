<?php

class Catalog_SearchController extends Ikantam_Controller_CatalogAbstract
{

    public function init()
    {
        /*if (!$this->getSession()->isLoggedIn()) {
            $this->_helper->redirector('index', 'login', 'user');
        }*/
        parent::init();
    }

    public function indexAction()
    {
        $products = new Catalog_Model_Product_Collection();

        $this->applyQueryFilter($products);
        $this->applyCategoriesFilter($products);
        $this->applyPricesFilter($products);
        $this->applyConditionsFilter($products);
        $this->applyColorsFilter($products);
        $this->applyBrandsFilter($products);
        $this->applyMiscFilter($products);
        $this->applyOrder($products);
        $this->applyLimit($products);

        $this->view->products = $products;

        if ($this->getRequest()->isXmlHttpRequest()) {
            $response['success'] = true;
            $response['template'] = $this->view->render('catalog_items.phtml');
            $this->_helper->json($response);
        }

        $this->view->categories = new Catalog_Model_Category_Collection();

        $categoryId = $this->getRequest()->getParam('category');

        if ($categoryId) {
            $category = new Catalog_Model_Category();
            $category->load($categoryId);
            if ($category->getId() && !$category->getParentId()) {
                $this->view->categories = $category->getSubCategories();
            }
        }

        $this->view->conditions = new Catalog_Model_Condition_Collection();

        $colors = new Catalog_Model_Color_Collection();
        $colors->setTable('view_distinct_product_colors');
        $this->view->colors = $colors;

        $brands = new Catalog_Model_Manufacturer_Collection();
        $brands->setTable('view_product_manufacturers');
        $this->view->brands = $brands;

        $this->view->minPrice = (int) floor($products->getMinPrice() / 10) * 10;
        $this->view->maxPrice = (int) ceil($products->getMaxPrice() / 10) * 10;
        $this->view->query = $this->getRequest()->getParam('q');
        $this->view->url = Ikantam_Url::getUrl('catalog/search/index');

        $this->_helper->viewRenderer('catalog', null, true);
    }

}