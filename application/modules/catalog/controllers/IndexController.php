<?php

class Catalog_IndexController extends Ikantam_Controller_Front
{

	public function init()
    {

    }

	public function indexAction()
	{
        //$this->_forward('index', 'category', 'catalog');

		$categories = new Category_Model_Category_Collection();
		$category = $categories->getParents()->getFirstItem();
		if ($category->getId()) {
            if ($category->getPageUrl()) {
                $this->_helper->redirector('index', $category->getPageUrl(), 'catalog');
            }
			$this->_helper->redirector('index', 'index', 'category', array('id' => $category->getId()));
		}
		$this->_helper->redirector('index', 'index', 'category');
	}

    public function routeAction()
    {
        $path = $this->getRequest()->getParam('path');

        if ($path) {
            $rb = new Catalog_Model_RouterBackend();
            $object = $rb->getRoute($path);


            if ($object instanceof Category_Model_Category) {
                $this->_forward('index', 'index', 'category', array('id' => $object->getId()));
                return;
            } elseif ($object instanceof Product_Model_Product) {
                $this->_forward('index', 'view', 'product', array('id' => $object->getId()));
                return;
            }
        }

        $this->_forward('index', 'index', 'catalog');
    }


    public function testAction()
    {
        $this->_forward('index', 'category', 'catalog');
    }

}

