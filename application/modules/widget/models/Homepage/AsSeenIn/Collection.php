<?php

class Widget_Model_Homepage_AsSeenIn_Collection extends Application_Model_Abstract_Collection
{

    protected $_backendClass = 'Widget_Model_Homepage_AsSeenIn_Backend';

    public function getAll()
    {
        $this->_getBackend()->getAll($this);
        return $this;
    }

}