<?php

class Widget_Model_Homepage_AsSeenIn extends Application_Model_Abstract
{
    public function getImageUrl()
    {
        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
        return $cloudFrontUrl . $this->getImagePath();
    }
}