<?php

class Widget_Model_Homepage_Slider_Backend extends Application_Model_Abstract_Backend
{

    protected $_table   = 'widget_homepage_slider';
    protected $_primary = 'id';
    protected $_fields  = array(
        'type',
        'size',
        'title',
        'image_path',
        'url',
        'sort_order',
    );

    public function getAll(\Application_Model_Abstract_Collection $collection)
    {
        $sql  = 'SELECT * FROM `' . $this->_getTable() . '` ORDER BY `sort_order` ASC';
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Widget_Model_Homepage_AsSeenIn();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $template = 'INSERT INTO `%s` (%s) VALUES (%s)';
        $bind     = array();
        $fields   = '`' . implode('`, `', $this->_fields) . '`';
        $values   = ':' . implode(', :', $this->_fields);

        foreach ($this->_fields as $field) {
            $bind[':' . $field] = $object->getData($field);
        }

        $sql  = sprintf($template, $this->_getTable(), $fields, $values);
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $bind = array();

        $template = 'UPDATE `%s` SET %s WHERE %s';

        $set = array();
        foreach ($this->_fields as $field) {
            $set[]              = sprintf('`%s` = :%s', $field, $field);
            $bind[':' . $field] = $object->getData($field);
        }

        $where                       = sprintf('`%s` = :%s', $this->_primary, $this->_primary);
        $bind[':' . $this->_primary] = $object->getData($this->_primary);
        $sql                         = sprintf($template, $this->_getTable(), implode(', ', $set), $where);

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
    }

}