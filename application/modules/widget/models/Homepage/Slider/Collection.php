<?php

class Widget_Model_Homepage_Slider_Collection extends Application_Model_Abstract_Collection
{

    protected $_backendClass = 'Widget_Model_Homepage_Slider_Backend';

    public function getAll()
    {
        $this->_getBackend()->getAll($this);
        return $this;
    }

}