<?php

class Widget_Form_AsSeenIn extends Zend_Form
{

    public function __construct($options = null)
    {
        parent::__construct($options);
        
        $this->setName('upload');
        $this->setAction(Ikantam_Url::getUrl('admin/widgets/as-seen-in'));
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
        
        
        $ad = new Zend_File_Transfer_Adapter_Http();
        $ad->setDestination(APPLICATION_PATH . '/../tmp/widgets');
        
        $size = new Zend_Validate_File_Size(array('min' => '1kB', 'max' => '14MB'));
        $extension = new Zend_Validate_File_Extension(array('jpg', 'jpeg', 'png', 'gif'));
        $isImage = new Zend_Validate_File_IsImage();

        $ad->addValidator($size, true)
                ->addValidator($extension, true)
                ->addValidator($isImage, true);

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Title')
                ->setRequired(true)
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim'))
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)));
        
        $pageUrl = new Zend_Form_Element_Text('page_url');
        $pageUrl->setLabel('Page URL')
                ->setRequired(true)
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim'))
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)));
        
        $sortOrder = new Zend_Form_Element_Text('sort_order');
        $sortOrder->setLabel('Sort Order')
                ->setRequired(false)
                ->addFilters(array('Int'));

        $image = new Zend_Form_Element_File('image');
        $image->setLabel('Image')
                ->setRequired(true)
                ->addValidator('NotEmpty')
                ->setTransferAdapter($ad);
        

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');
        

        $this->addElements(array($title, $pageUrl, $sortOrder, $image, $submit));
    }

}