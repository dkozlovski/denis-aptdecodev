<?php

class Widget_Form_Slider extends Zend_Form
{

    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->setName('upload');
        $this->setAction(Ikantam_Url::getUrl('admin/widgets/slider'));
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');


        $ad = new Zend_File_Transfer_Adapter_Http();
        $ad->setDestination(APPLICATION_PATH . '/../tmp/widgets');

        $size      = new Zend_Validate_File_Size(array('min' => '1kB', 'max' => '14MB'));
        $extension = new Zend_Validate_File_Extension(array('jpg', 'jpeg', 'png', 'gif'));
        $isImage   = new Zend_Validate_File_IsImage();

        $ad->addValidator($size, true)
                ->addValidator($extension, true)
                ->addValidator($isImage, true);

        $type = new Zend_Form_Element_Select('type');
        $type->setLabel('Type')
                ->setRequired(true)
                ->setAttrib('class', 'hidden')
                ->setDecorators(array("ViewHelper"))
                ->setMultiOptions(array('image' => 'Image', 'video' => 'Video'))
                ->addValidator('InArray', false, array(array('image', 'video')));

        $size = new Zend_Form_Element_Select('size');
        $size->setLabel('Size')
                ->setRequired(true)
                ->setMultiOptions(array('large' => 'Large', 'standard' => 'Standard', 'small' => 'Small', 'extra_small' => 'Extra Small'))
                ->addValidator('InArray', false, array(array('large', 'standard', 'small', 'extra_small')));

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Title')
                ->setRequired(false)
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim'))
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)));

        $url = new Zend_Form_Element_Text('url');
        $url->setLabel('URL')
                ->setRequired(false)
                ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim'))
                ->addValidator('NotEmpty')
                ->addValidator('StringLength', false, array(array('min' => 1, 'max' => 255)));

        $sortOrder = new Zend_Form_Element_Text('sort_order');
        $sortOrder->setLabel('Sort Order')
                ->setRequired(false)
                ->addFilters(array('Int'));

        $image = new Zend_Form_Element_File('image');
        $image->setLabel('Image')
                ->setRequired(true)
                ->addValidator('NotEmpty')
                ->setTransferAdapter($ad);


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');


        $this->addElements(array($type, $size, $title, $url, $sortOrder, $image, $submit));
    }

    public function isValid($data)
    {
        return parent::isValid($data);
    }

}