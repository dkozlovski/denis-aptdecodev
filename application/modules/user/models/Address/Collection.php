<?php

class User_Model_Address_Collection extends Application_Model_Abstract_Collection
    implements Ikantam_Filter_Flex_Interface
{

    public function getByUserId($id)
    {
        $this->_getBackend()->getByUserId($this, $id);
        return $this;
    }

    public function getPrimaryByUserId($id)
    {
        return $this->_getBackend()->getPrimaryByUserId($this, $id);
    }

    public function getJsonConfigByUserId($id = null, $limit = null, $offset = null)
    {
        if ($id) {
            $this->getLastAddressesByUserId($id, $limit, $offset);
        }

        $options = array();

        $fields = array(
            'full_name',
            'country_code',
            'city',
            'state',
            'address_line1',
            'address_line2',
            'postal_code',
            'telephone_number',
            'phone_number',
            'postcode',
        );

        foreach ($this as $address) {
            $data = array();

            foreach ($fields as $field) {
                $data[$field] = trim($address->getData($field));
            }

            $options[$address->getId()] = $data;
        }

        return $options;
    }

    public function toHtml()
    {
        $html = '';
        foreach ($this->getItems() as $address) {
            $html .= '<div class="aptd-alert" data-address-id="'. $address->getId() .'" id="address_'. $address->getId() .'">' .
                $address->toHtml() .
                '<a href="javascript:void(0);" class="dark stick top-right" onclick=\'setPickupAddress('
                . $address->getId() . ')\'>'
                .'Use this address <i class="apt-icon-sm-arrow"></i></a>'.
                '</div>';
        }

        if ($html) {
            $html .= '<button type="button" id="add_address" class="apt-btn sm-flat">' .
                '<i class="apt-i-plus"></i>New Address</button>';
        }

        return $html;
    }

    /**
     * Returns a filter related with collection.
     * @return User_Model_Address_Collection_FlexFilter
     */
    public function  getFlexFilter()
    {
        return new User_Model_Address_FlexFilter($this);
    }

    /**
     * Retrieves last created user addresses
     * @param int $userId
     * @param int (optional) $limit
     * @param int (optional) $offset
     * @return $this
     */
    public function getLastAddressesByUserId($userId, $limit = null, $offset = null)
    {
        if ((int)$userId) {
            $this->getFlexFilter()->user_id('=', $userId)
                ->deleted('=', 0)
                ->order('id', 'desc')
                ->apply($limit, $offset);
        }

        return $this;
    }
}