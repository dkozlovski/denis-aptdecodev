<?php

class User_Model_Address_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'user_addresses';

    public function getByUserId($collection, $id)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `user_id` = :user_id AND `deleted` = 0';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam('user_id', $id);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new $this->_itemClass();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getByHash($object, $hash)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `hash` = :hash';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam('hash', $hash);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function getPrimaryByUserId($collection, $id)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `user_id` = :user_id AND `is_primary` = 1';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam('user_id', $id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $item = new User_Model_Address();

        if ($row) {
            $item->addData($row);
        }
        return $item;
    }

    public function setPrimary(\User_Model_Address $object)
    {
        $sql = 'UPDATE  `' . $this->_getTable() . '` SET `is_primary` = 0 WHERE `user_id` = :user_id ';
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam('user_id', $object->getUserId());
        $stmt->execute();


        $sql = 'UPDATE  `' . $this->_getTable() . '` SET `is_primary` = 1 WHERE `id` = :id ';
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam('id', $object->getId());
        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $object)
    {

        $userId         = $object->getUserId();
        $fullName       = $object->getFullName();
        $countryCode    = $object->getCountryCode();
        $city           = $object->getCity();
        $state          = $object->getState();
        $stateCode      = $object->getStateCode();
        $addressLine1   = $object->getAddressLine1();
        $addressLine2   = $object->getAddressLine2();
        $postcode       = $object->getPostcode();
        $phoneNumber    = $object->getPhoneNumber($object->getTelephoneNumber());
        $hash           = md5($fullName . $countryCode . $city . $state . $stateCode . $addressLine1 . $addressLine2 . $postcode . $phoneNumber);
        $isPrimary      = $object->getIsPrimary();
        $buildingType   = $object->getBuildingType();
        $numberOfStairs = $object->getNumberOfStairs();

        $sql0  = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `hash` = :hash';
        $stmt0 = $this->_getConnection()->prepare($sql0);

        $stmt0->bindParam(':hash', $hash);
        $stmt0->execute();
        $row0 = $stmt0->fetch(PDO::FETCH_ASSOC);

        if ($row0 && isset($row0['id']) && (int) $row0['id'] > 0) {
            $object->setId((int) $row0['id']);
            return;
        }

        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 
            `user_id` = :user_id AND `full_name` = :full_name AND 
            `country_code` = :country_code AND `city` = :city AND 
            `state` = :state AND `state_code` = :state_code AND `address_line1` = :address_line1 AND 
            `address_line2` = :address_line2 AND `postcode` = :postcode 
            LIMIT 1';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':country_code', $countryCode);
        $stmt->bindParam(':city', $city);
        $stmt->bindParam(':state', $state);
        $stmt->bindParam(':state_code', $stateCode);
        $stmt->bindParam(':address_line1', $addressLine1);
        $stmt->bindParam(':address_line2', $addressLine2);
        $stmt->bindParam(':postcode', $postcode);

        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row && isset($row['id']) && (int) $row['id'] > 0) {
            if (empty($row['phone_number']) || $row['phone_number'] == $phoneNumber || $row['hash'] == $hash) {
                $sql1 = 'UPDATE `' . $this->_getTable() . '` 
                    SET `phone_number` = :phone_number 
                    WHERE `id` = :id';

                $stmt1 = $this->_getConnection()->prepare($sql1);

                $stmt1->bindParam(':phone_number', $phoneNumber);
                $stmt1->bindParam(':id', $row['id']);
                $stmt1->execute();

                $object->setId((int) $row['id']);
                return;
            }
        }

        $sql2 = 'INSERT INTO `' . $this->_getTable() . '` 
				(`user_id`, `full_name`, `country_code`, `city`, `state`, `state_code`,
                `address_line1`, `address_line2`, `postcode`, `phone_number`, 
                `hash`, `is_primary`, `building_type`, `number_of_stairs`) 
                
				VALUES (:user_id, :full_name, :country_code, :city, :state, :state_code,
                :address_line1, :address_line2, :postcode, :phone_number, 
                :hash, :is_primary, :building_type, :number_of_stairs)';

        $stmt2 = $this->_getConnection()->prepare($sql2);

        $stmt2->bindParam(':user_id', $userId);
        $stmt2->bindParam(':full_name', $fullName);
        $stmt2->bindParam(':country_code', $countryCode);
        $stmt2->bindParam(':city', $city);
        $stmt2->bindParam(':state', $state);
        $stmt2->bindParam(':state_code', $stateCode);
        $stmt2->bindParam(':address_line1', $addressLine1);
        $stmt2->bindParam(':address_line2', $addressLine2);
        $stmt2->bindParam(':postcode', $postcode);
        $stmt2->bindParam(':phone_number', $phoneNumber);
        $stmt2->bindParam(':hash', $hash);
        $stmt2->bindParam(':is_primary', $isPrimary);
        $stmt2->bindParam(':building_type', $buildingType);
        $stmt2->bindParam(':number_of_stairs', $numberOfStairs);

        $stmt2->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql            = 'UPDATE `' . $this->_getTable() . '` SET 
			`user_id` = :user_id, 
			`full_name` = :full_name, 
			`country_code` = :country_code, 
			`city` = :city,
			`state` = :state, 
            `state_code` = :state_code,
			`address_line1` = :address_line1, 
			`address_line2` = :address_line2, 
			`postcode` = :postcode, 
            `phone_number` = :phone_number,
			`hash` = :hash, 
			`is_primary` = :is_primary,
            `deleted` = :deleted,
            `building_type` = :building_type,
            `number_of_stairs` = :number_of_stairs
			WHERE `id` = :id';
        $stmt           = $this->_getConnection()->prepare($sql);
        $id             = $object->getId();
        $userId         = $object->getUserId();
        $fullName       = $object->getFullName();
        $countryCode    = $object->getCountryCode();
        $city           = $object->getCity();
        $state          = $object->getState();
        $stateCode      = $object->getStateCode();
        $addressLine1   = $object->getAddressLine1();
        $addressLine2   = $object->getAddressLine2();
        $postcode       = $object->getPostcode();
        $phoneNumber    = $object->getPhoneNumber();
        $hash           = md5($fullName . $countryCode . $city . $state . $stateCode . $addressLine1 . $addressLine2 . $postcode . $phoneNumber);
        $deleted        = $object->getDeleted();
        $buildingType   = $object->getBuildingType();
        $numberOfStairs = $object->getNumberOfStairs();

        $object->setHash($hash);

        $isPrimary = $object->getIsPrimary();

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':country_code', $countryCode);
        $stmt->bindParam(':city', $city);
        $stmt->bindParam(':state', $state);
        $stmt->bindParam(':state_code', $stateCode);
        $stmt->bindParam(':address_line1', $addressLine1);
        $stmt->bindParam(':address_line2', $addressLine2);
        $stmt->bindParam(':postcode', $postcode);
        $stmt->bindParam(':phone_number', $phoneNumber);
        $stmt->bindParam(':hash', $hash);
        $stmt->bindParam(':is_primary', $isPrimary);
        $stmt->bindParam(':deleted', $deleted);
        $stmt->bindParam(':building_type', $buildingType);
        $stmt->bindParam(':number_of_stairs', $numberOfStairs);

        $stmt->execute();
    }

}