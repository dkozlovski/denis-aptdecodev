<?php
/**
 * Author: Alex P.
 * Date: 11.08.14
 * Time: 16:30
 */

/**
 * Class User_Model_Address_FlexFilter
 * @method \User_Model_Address_FlexFilter user_id($operator, $value)
 * @method \User_Model_Address_Collection apply($limit, $offset)
 */
class User_Model_Address_FlexFilter extends Ikantam_Filter_Flex
{
    /**
     * @var string
     */
    protected $_acceptClass = 'User_Model_Address_Collection';
} 