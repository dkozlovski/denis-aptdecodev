<?php

class User_Model_Message extends Application_Model_Abstract
{
	
	public function getAuthor()
	{
		$author = new Application_Model_User($this->getAuthorId());
		return $author;
	}
	
	public function save()
	{
		$this->_getbackend()->save($this);

		return $this;
	}
	
	/**
	 * 
	 * @param type $user
	 * @return \User_Model_Message
	 */
	public function setAsReadedBy($userId)
	{
		$this->_getbackend()->setAsReadedBy($this->getId(), $userId);
		return $this;
	}
	
	public function setAsDeletedBy($userId)
	{
		$this->_getbackend()->setAsDeletedBy($this->getId(), $userId);
		return $this;
	}
	
	
	
}