<?php

class User_Model_Merchant_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'merchants';

    public function getByUserId($collection, $id)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `user_id` = :user_id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam('user_id', $id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $collection->addData($row);
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `' . $this->_getTable() . '` 
			(`user_id`, `account_uri`, `bank_account_uri`, `full_name`, `phone_number`,
            `postal_code`, `street_address`, `date_of_birth`, `routing_number`, `bank_name`,
            `account_number`, `account_type`) 
			VALUES (:user_id, :account_uri, :bank_account_uri, :full_name, :phone_number, 
            :postal_code, :street_address, :date_of_birth, :routing_number, 
            :bank_name, :account_number, :account_type)';

        $stmt = $this->_getConnection()->prepare($sql);

        $userId = $object->getUserId();
        $accountUri = $object->getAccountUri();
        $bankAccountUri = $object->getBankAccountUri();
        $fullName = $object->getFullName();
        $phoneNumber = $object->getPhoneNumber();
        $postalCode = $object->getPostalCode();
        $streetAddress = $object->getStreetAddress();
        
        $dateOfBirth = $object->getDateOfBirth();
        $routingNumber = $object->getRoutingNumber();
        $bankName = $object->getBankName();
        $accountNumber = $object->getAccountNumber();
        $accountType = $object->getAccountType();
        

        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':account_uri', $accountUri);
        $stmt->bindParam(':bank_account_uri', $bankAccountUri);
        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':phone_number', $phoneNumber);
        $stmt->bindParam(':postal_code', $postalCode);
        $stmt->bindParam(':street_address', $streetAddress);
        $stmt->bindParam(':date_of_birth', $dateOfBirth);
        $stmt->bindParam(':routing_number', $routingNumber);
        $stmt->bindParam(':bank_name', $bankName);
        $stmt->bindParam(':account_number', $accountNumber);
        $stmt->bindParam(':account_type', $accountType);

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = 'UPDATE `' . $this->_getTable() . '` SET 
			`user_id` = :user_id, 
			`account_uri` = :account_uri, 
			`bank_account_uri` = :bank_account_uri, 
			`full_name` = :full_name,
			`phone_number` = :phone_number, 
			`postal_code` = :postal_code, 
			`street_address` = :street_address, 
			`date_of_birth` = :date_of_birth, 
			`routing_number` = :routing_number, 
			`bank_name` = :bank_name,
            `account_number` = :account_number,
            `account_type` = :account_type
			WHERE `id` = :id';
        
        $stmt = $this->_getConnection()->prepare($sql);
        
        
        $id = $object->getId();
        $userId = $object->getUserId();
        $accountUri = $object->getAccountUri();
        $bankAccountUri = $object->getBankAccountUri();
        $fullName = $object->getFullName();
        $phoneNumber = $object->getPhoneNumber();
        $postalCode = $object->getPostalCode();
        $streetAddress = $object->getStreetAddress();
        
        $dateOfBirth = $object->getDateOfBirth();
        $routingNumber = $object->getRoutingNumber();
        $bankName = $object->getBankName();
        $accountNumber = $object->getAccountNumber();
        $accountType = $object->getAccountType();
        
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':account_uri', $accountUri);
        $stmt->bindParam(':bank_account_uri', $bankAccountUri);
        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':phone_number', $phoneNumber);
        $stmt->bindParam(':postal_code', $postalCode);
        $stmt->bindParam(':street_address', $streetAddress);
        $stmt->bindParam(':date_of_birth', $dateOfBirth);
        $stmt->bindParam(':routing_number', $routingNumber);
        $stmt->bindParam(':bank_name', $bankName);
        $stmt->bindParam(':account_number', $accountNumber);
        $stmt->bindParam(':account_type', $accountType);

        $stmt->execute();
    }

}