<?php

class User_Model_Address_Collection extends Application_Model_Abstract_Collection
{

	public function getByUserId($id)
	{
		$this->_getBackend()->getByUserId($this, $id);
		return $this;
	}
	
	public function getPrimaryByUserId($id)
	{
		return $this->_getBackend()->getPrimaryByUserId($this, $id);
	}
	
	
	
}