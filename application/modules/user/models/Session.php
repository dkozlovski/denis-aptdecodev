<?php
/**
 * Class User_Model_Session
 * @method int getUserId()
 */
class User_Model_Session extends Ikantam_Session
{

    protected $_user;
    protected $_cart;
    private static $_instance_ = null;

    public function getUser()
    {
        if (!$this->_user) {
            $this->_user = new User_Model_User();
            //$this->_user->getById($this->getUserId());
            $this->_user->getActiveById($this->getUserId());
        }

        return $this->_user;
    }

    public function getCart()
    {
        if (!$this->_cart) {
            $this->_cart = new Cart_Model_Cart();
            if (!$this->getCartId()) {
                if ($this->getUser()->getId()) {
                    $this->_cart->setUserId($this->getUser()->getId());
                }
                $this->_cart->save();
                $this->setCartId($this->_cart->getId());
            } else {
                $this->_cart->getById($this->getCartId());
            }
        }

        return $this->_cart;
    }

    public function isLoggedIn()
    {
        return (bool) $this->getUser()->getId();
    }

    public function authenticate($loginData)
    {
        $this->getUser()->authenticate($loginData);
        $this->setUserId($this->getUser()->getId());
        $this->getCart()->checkItems($this->getUserId()); //User may add own items when he not logged in and then sign in. 
        //This call clear user's cart from his own products.
    }

    public function register($signupData)
    {
        $this->getUser()->register($signupData);
        $this->setUserId($this->getUser()->getId());
    }

    public function registerFacebook($signupData, $dateOfBirth)
    {
        $this->getUser()->registerFacebook($signupData, $dateOfBirth);
        $this->setUserId($this->getUser()->getId());
    }

    public static function instance()
    {
        if (!self::$_instance_ instanceof self) {
            self::$_instance_ = new self();
        }

        return self::$_instance_;
    }

    public static function user()
    {
        return self::instance()->getUser();
    }

}
