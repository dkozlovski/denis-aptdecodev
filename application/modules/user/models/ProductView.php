<?php

/**
 * to store "product view" just set user and product id and then save
 * it will create new or update existing record 
 * you can specify id's by passing it to constructor (if both params provided, it will call save method automatically)
 */
class User_Model_ProductView extends Application_Model_Abstract
{

    protected $_backendClass = 'User_Model_ProductView_Backend';
    protected $_product      = null;

    public function __construct($productId = null, $userId = null)
    {
        if ($productId) {
            $this->setProductId($productId);
        }

        if ($userId) {
            $this->setUserId($userId);
        }

        if ($this->_checkPair()) {
            $this->save();
        }
    }

    public function getByPair($userId = null, $productId = null)
    {
        if (!$userId) {
            $userId = $this->getUserId();
        }

        if (!$productId) {
            $productId = $this->getProductId();
        }

        $this->_getbackend()->getByPairUserProduct($this);
    }

    protected function _checkPair()
    {
        return $this->getUserId() && $this->getProductId();
    }

    public function setUserId($id = null)
    {
        parent::setUserId($id);

        if ($this->_checkPair()) {
            $this->getByPair();
        }
    }

    public function setProductId($id = null)
    {
        parent::setProductId($id);

        if ($this->_checkPair()) {
            $this->getByPair();
        }
    }

    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = new Product_Model_Product($this->getProductId());
        }
        return $this->_product;
    }

}