<?php

class User_Model_User extends Application_Model_User
{

    public function countNewInboxMessages()
    {
        $collection = new Conversation_Model_Conversation_Message_Collection();
        $collection->getNewByUserId($this->getId(), $offset     = null, $limit      = null);
        return $collection->getSize();
    }

    public function countInboxMessages()
    {
        $collection = new Conversation_Model_Conversation_Message_Collection();
        $collection->getByUserId($this->getId(), $offset     = null, $limit      = null);
        return $collection->getSize();
    }

    public function countOutboxMessages()
    {
        $collection = new Conversation_Model_Conversation_Message_Collection();
        $collection->getOutboxByUserId($this->getId(), $offset     = null, $limit      = null);
        return $collection->getSize();
    }

    public function getOutboxMessages($offset = null, $limit = null)
    {
        $collection = new Conversation_Model_Conversation_Message_Collection();
        $collection->getOutboxByUserId($this->getId(), $offset, $limit);
        return $collection;
    }

    public function getNewInboxMessages($offset = null, $limit = null)
    {
        $collection = new Conversation_Model_Conversation_Message_Collection();
        $collection->getNewByUserId($this->getId(), $offset, $limit);
        return $collection;
    }

    public function getInboxMessages($offset = null, $limit = null)
    {
        $collection = new Conversation_Model_Conversation_Message_Collection();
        $collection->getByUserId($this->getId(), $offset, $limit);
        return $collection;
    }

}