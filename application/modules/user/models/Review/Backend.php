<?php

class User_Model_Review_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'reviews';

	public function getByUserId($collection, $id)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `user_id` = :user_id ORDER BY `created_at` DESC';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam('user_id', $id);

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new $this->_itemClass();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `' . $this->_getTable() . '` (`parent_id`, `user_id`, `author_id`, `text`, `created_at`, `rating`) VALUES (:parent_id, :user_id, :author_id, :text, :created_at, :rating)';

		$stmt = $this->_getConnection()->prepare($sql);

		$parentId = $object->getParentId();
		$userId = $object->getUserId();
		$authorId = $object->getAuthorId();
		$text = $object->getText();
		$createdAt = $object->getCreatedAt();
		$rating = $object->getRating();

		$stmt->bindParam(':parent_id', $parentId);
		$stmt->bindParam(':user_id', $userId);
		$stmt->bindParam(':author_id', $authorId);
		$stmt->bindParam(':text', $text);
		$stmt->bindParam(':created_at', $createdAt);
		$stmt->bindParam(':rating', $rating);

		$stmt->execute();

		$object->setId($this->_getConnection()->lastInsertId());
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		
	}

}