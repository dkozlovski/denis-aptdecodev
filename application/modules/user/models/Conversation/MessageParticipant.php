<?php

class User_Model_Conversation_MessageParticipant extends Application_Model_Abstract
{

	public function getAuthor()
	{
		$author = new Application_Model_User($this->getAuthorId());
		return $author;
	}

	public function getRecipient()
	{
		$author = new Application_Model_User($this->getRecipientId());
		return $author;
	}

}