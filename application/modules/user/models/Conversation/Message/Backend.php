<?php

class User_Model_Conversation_Message_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'conversation_messages';
	
	public function getByConversationId($collection, $conversationId, $userId)
	{
		$sql = 'SELECT `conversation_messages`.* FROM `conversation_messages`
			JOIN `conversations` ON `conversation_messages`.`conversation_id` = `conversations`.`id`
			JOIN `conversation_participants` ON `conversations`.`id` = `conversation_participants`.`conversation_id`
			JOIN `messages_participants` ON `conversation_messages`.`id` = `messages_participants`.`message_id` AND `conversation_participants`.`id` = `messages_participants`.`participant_id`
			WHERE `conversation_messages`.`conversation_id` = :conversation_id AND `conversation_participants`.`user_id` = :user_id AND `messages_participants`.`is_visible` = 1
			ORDER BY `conversation_messages`.`created_at` DESC';

				
		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':user_id', $userId);
		$stmt->bindParam(':conversation_id', $conversationId);
		
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new User_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}
	
	public function countByUserId($collection, $userId)
	{
		//`conversation_messages`.`author_id` <> `conversation_participants`.`id` - get inbox messages
		$sql = 'SELECT count(*) as `count` FROM `conversation_messages`
			JOIN `conversations` ON `conversation_messages`.`conversation_id` = `conversations`.`id`
			JOIN `conversation_participants` ON `conversations`.`id` = `conversation_participants`.`conversation_id`
			JOIN `messages_participants` ON `conversation_messages`.`id` = `messages_participants`.`message_id` AND `conversation_participants`.`id` = `messages_participants`.`participant_id`
			WHERE `conversation_messages`.`author_id` <> `conversation_participants`.`id` AND `conversation_participants`.`user_id` = :user_id AND `messages_participants`.`is_visible` = 1
			ORDER BY `conversation_messages`.`created_at` DESC';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row && isset($row['count'])) {
			return (int) $row['count'];
		}
		return 0;
	}
	
	
	public function getByUserId($collection, $userId, $offset = 0, $limit = 10)
	{
		//`conversation_messages`.`author_id` <> `conversation_participants`.`id` - get inbox messages
		$sql = 'SELECT `conversation_messages`.*, `messages_participants`.`is_new` FROM `conversation_messages`
			JOIN `conversations` ON `conversation_messages`.`conversation_id` = `conversations`.`id`
			JOIN `conversation_participants` ON `conversations`.`id` = `conversation_participants`.`conversation_id`
			JOIN `messages_participants` ON `conversation_messages`.`id` = `messages_participants`.`message_id` AND `conversation_participants`.`id` = `messages_participants`.`participant_id`
			WHERE `conversation_messages`.`author_id` <> `conversation_participants`.`id` AND `conversation_participants`.`user_id` = :user_id AND `messages_participants`.`is_visible` = 1
			ORDER BY `conversation_messages`.`created_at` DESC
			LIMIT ' . $offset . ', ' . $limit . ';';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new User_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getNewByUserId($collection, $userId, $offset = 0, $limit = 10)
	{
		//`conversation_messages`.`author_id` <> `conversation_participants`.`id` - get inbox messages
		$sql = 'SELECT `conversation_messages`.*, `messages_participants`.`is_new` FROM `conversation_messages`
			JOIN `conversations` ON `conversation_messages`.`conversation_id` = `conversations`.`id`
			JOIN `conversation_participants` ON `conversations`.`id` = `conversation_participants`.`conversation_id`
			JOIN `messages_participants` ON `conversation_messages`.`id` = `messages_participants`.`message_id` AND `conversation_participants`.`id` = `messages_participants`.`participant_id`
			WHERE `conversation_messages`.`author_id` <> `conversation_participants`.`id` AND `conversation_participants`.`user_id` = :user_id AND `messages_participants`.`is_visible` = 1 AND `messages_participants`.`is_new` = 1
			ORDER BY `conversation_messages`.`created_at` DESC
			LIMIT ' . $offset . ', ' . $limit . ';';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new User_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}
	
	public function countNewByUserId($collection, $userId)
	{
		//`conversation_messages`.`author_id` <> `conversation_participants`.`id` - get inbox messages
		$sql = 'SELECT count(*) as `count` FROM `conversation_messages`
			JOIN `conversations` ON `conversation_messages`.`conversation_id` = `conversations`.`id`
			JOIN `conversation_participants` ON `conversations`.`id` = `conversation_participants`.`conversation_id`
			JOIN `messages_participants` ON `conversation_messages`.`id` = `messages_participants`.`message_id` AND `conversation_participants`.`id` = `messages_participants`.`participant_id`
			WHERE `conversation_messages`.`author_id` <> `conversation_participants`.`id` AND `conversation_participants`.`user_id` = :user_id AND `messages_participants`.`is_visible` = 1 AND `messages_participants`.`is_new` = 1
			ORDER BY `conversation_messages`.`created_at` DESC';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row && isset($row['count'])) {
			return (int) $row['count'];
		}
		return 0;
	}
	
	
	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `conversation_messages` 
			(`author_id`, `text`, `created_at`, `conversation_id`) 
			VALUES (:author_id, :text, :created_at, :conversation_id)';

		$stmt = $this->_getConnection()->prepare($sql);

		$authorId = $object->getAuthorId();
		$text = $object->getText();
		$createdAt = $object->getCreatedAt();
		$conversationId = $object->getConversationId();

		$stmt->bindParam(':author_id', $authorId);
		$stmt->bindParam(':text', $text);
		$stmt->bindParam(':created_at', $createdAt);
		$stmt->bindParam(':conversation_id', $conversationId);

		$stmt->execute();

		$object->setId($this->_getConnection()->lastInsertId());
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		
	}
	
	
		public function setAsReadedBy($messageId, $userId)
	{
	
		$sql = 'UPDATE `messages_participants`
JOIN `conversation_participants` ON `messages_participants`.`participant_id` = `conversation_participants`.`id`
set `is_new` = 0
WHERE `messages_participants`.`message_id` = :message_id
AND `conversation_participants`.`user_id` = :user_id';
				
				
		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':message_id', $messageId);
		$stmt->bindParam(':user_id', $userId);
		
		$stmt->execute();
	}
	
	public function setAsDeletedBy($messageId, $userId)
	{
		$sql = 'UPDATE `messages_participants`
JOIN `conversation_participants` ON `messages_participants`.`participant_id` = `conversation_participants`.`id`
set `is_visible` = 0
WHERE `messages_participants`.`message_id` = :message_id
AND `conversation_participants`.`user_id` = :user_id';
		
		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':message_id', $messageId);
		$stmt->bindParam(':user_id', $userId);
		
		$stmt->execute();
	}

}