<?php

class User_Model_Conversation_Message_Collection  extends Application_Model_Abstract_Collection
{
	public function getByConversationId($conversationId, $userId)
	{
		$this->_getBackend()->getByConversationId($this, $conversationId, $userId);
		return $this;
	}
	
	public function getByUserId($userId, $offset = 0, $limit = 10)
	{
		$this->_getBackend()->getByUserId($this, $userId, $offset, $limit);
		return $this;
	}
	
	public function countByUserId($userId)
	{
		return $this->_getBackend()->countByUserId($this, $userId);
	}
	
	public function getNewByUserId($userId, $offset = 0, $limit = 10)
	{
		$this->_getBackend()->getNewByUserId($this, $userId, $offset, $limit);
		return $this;
	}
	
	public function countNewByUserId($userId)
	{
		return $this->_getBackend()->countNewByUserId($this, $userId);
	}
}