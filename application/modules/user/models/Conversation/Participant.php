<?php

class User_Model_Conversation_Participant extends Application_Model_Abstract
{
	public function getByUserId($userId, $conversationId)
	{
		$this->_getBackend()->getByUserId($this, $userId, $conversationId);
		return $this;
	}
	
	public function getUser()
	{
		return new Application_Model_User($this->getUserId());
	}
	
}
?>
