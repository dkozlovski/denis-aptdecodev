<?php

class User_Model_Conversation_Participant_Backend  extends Application_Model_Abstract_Backend
{
	protected $_table = 'conversation_participants';


	public function getByUserId($object, $userId, $conversationId)
	{
		$sql = 'SELECT * FROM `conversation_participants` WHERE `user_id` = :user_id AND `conversation_id` = :conversation_id ';
		
		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':conversation_id', $conversationId);
		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();
		
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			$object->addData($row);
		}
	}

	public function getByConversationId($object, $conversationId, $userId)
	{
		$sql = 'SELECT * FROM `conversation_participants` WHERE `user_id` <> :user_id AND `conversation_id` = :conversation_id ';
		
		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':conversation_id', $conversationId);
		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();
		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($rows) {
			foreach ($rows as $row) {
				$item = new User_Model_Conversation_Participant();
				$item->addData($row);
				$object->addItem($item);
			}
			
		}
	}





	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `conversation_participants` 
			(`conversation_id`, `user_id`) 
			VALUES (:conversation_id, :user_id)';

		$stmt = $this->_getConnection()->prepare($sql);

		$conversationId = $object->getConversationId();
		$userId= $object->getUserId();

		$stmt->bindParam(':conversation_id', $conversationId);
		$stmt->bindParam(':user_id', $userId);

		$stmt->execute();

		$object->setId($this->_getConnection()->lastInsertId());
	}
	
	protected function _update(\Application_Model_Abstract $object)
	{
		
	}
}