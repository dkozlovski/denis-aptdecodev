<?php

class User_Model_Conversation_Participant_Collection  extends Application_Model_Abstract_Collection
{
	public function getByConversationId($conversationId, $userId)
	{
		$this->_getBackend()->getByConversationId($this, $conversationId, $userId);
		return $this;
	}
}