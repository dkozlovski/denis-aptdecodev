<?php

class User_Model_Conversation_Message_Collection  extends Application_Model_Abstract_Collection
{
	public function getByConversationId($conversationId)
	{
		$this->_getBackend()->getByConversationId($this, $conversationId);
		return $this;
	}
}