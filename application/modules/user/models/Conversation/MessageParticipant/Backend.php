<?php

class User_Model_Conversation_MessageParticipant_Backend  extends Application_Model_Abstract_Backend
{
	public function getByConversationId($collection, $conversationId)
	{
		$sql = 'SELECT distinct(`messages`.`id`) as `id`, `messages2`.* FROM `messages` 
			JOIN `messages_users` ON `messages`.`id` = `messages_users`.`message_id`
			JOIN `messages` as `messages2` ON `messages`.`id` = `messages2`.`id`
			
			WHERE `messages`.`conversation_id` = :conversation_id
			AND `messages_users`.`is_visible` = 1
			ORDER BY `messages`.`created_at` DESC';
		
		$stmt = $this->_getConnection()->prepare($sql);
		
		$stmt->bindParam(':conversation_id', $conversationId);
		
		$stmt->execute();
		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if ($rows) {
			foreach ($rows as $row) {
				$item = new User_Model_Conversation_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}
	
	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `messages_participants` 
			(`message_id`, `participant_id`, `is_visible`, `is_new`) 
			VALUES (:message_id, :participant_id, :is_visible, :is_new)';
		
		$stmt = $this->_getConnection()->prepare($sql);

		$messageId = $object->getMessageId();
		$participantId = $object->getParticipantId();
		$isVisible = $object->getIsVisible();
		$isNew = $object->getIsNew();

				
		$stmt->bindParam(':message_id', $messageId);
		$stmt->bindParam(':participant_id', $participantId);
		$stmt->bindParam(':is_visible', $isVisible);
		$stmt->bindParam(':is_new', $isNew);
		
		$stmt->execute();
	}
	
	protected function _update(\Application_Model_Abstract $object)
	{
		
	}
}