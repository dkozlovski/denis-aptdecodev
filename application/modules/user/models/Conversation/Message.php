<?php

class User_Model_Conversation_Message  extends Application_Model_Abstract
{
	
		/**
	 * 
	 * @param type $user
	 * @return \User_Model_Message
	 */
	public function setAsReadedBy($userId)
	{
		$this->_getbackend()->setAsReadedBy($this->getId(), $userId);
		return $this;
	}
	
	public function setAsDeletedBy($userId)
	{
		$this->_getbackend()->setAsDeletedBy($this->getId(), $userId);
		return $this;
	}
	
	
	
	public function getAuthor()
	{
		$participant = new User_Model_Conversation_Participant($this->getAuthorId());
		$author = new Application_Model_User($participant->getUserId());
		return $author;
	}
	
		public function getRecipient()
	{
		$author = new Application_Model_User($this->getRecipientId());
		return $author;
	}
	
	public function getConversation()
	{
		$conversation = new User_Model_Conversation($this->getConversationId());
		return $conversation;
	}
}