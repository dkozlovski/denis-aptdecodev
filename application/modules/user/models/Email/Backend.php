<?php

class User_Model_Email_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'user_emails';

    public function getByUserId(\Application_Model_Abstract_Collection $collection, $userId)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `user_id` = :uId";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':uId', $userId);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($result)) {
            foreach ($result as $row) {
                $email = new User_Model_Email();
                $email->addData($row);
                $collection->addItem($email);
            }
        }
    }

    public function getPrimaryByUserId($email, $userId)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `user_id` = :userId and `is_primary` = 1";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':userId', $userId);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $email->addData($row);
        }
    }

    //---------------------------------------------------------------------------
    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `' . $this->_getTable() . '` (`id`, `user_id`, `email`, `is_primary`) VALUES(NULL, :uId, :email, :prim)';

        $stmt = $this->_getConnection()->prepare($sql);
        
        $userId = $object->getUserId();
        $email = $object->getEmail();
        $prim = $object->getIsPrimary(0);
        
        $objClone = clone $object;
        
        $objClone->getBy_email($object->getEmail());
        
        if($prim) {
           $upd_stmt =  $this->_prepareSql("UPDATE `". $this->_getTable() ."` SET `is_primary` = 0 WHERE `user_id` = :uid");
           $upd_stmt->bindParam(':uid', $userId);
           $upd_stmt->execute();
        }
        
        if($objClone->isExists() && $objClone->getUserId() == $userId) {
            $objClone->setIsPrimary(1)
                     ->save();
            return;         
        }        
        
        $stmt->bindParam(':uId', $userId);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':prim', $prim);
        
        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = "UPDATE `" . $this->_getTable() . "` SET `user_id` = :uId, `email` = :email, `is_primary` = :primary WHERE `id` = :id";

        $stmt = $this->_getConnection()->prepare($sql);
        
        $userId = $object->getUserId();
        $email = $object->getEmail();
        $id = $object->getId();
        $isPrimary = $object->getIsPrimary();
        
        
        $stmt->bindParam(':uId', $userId);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':primary', $isPrimary);
        $stmt->execute();
    }

}