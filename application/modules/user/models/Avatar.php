<?php

class User_Model_Avatar extends Application_Model_Abstract
{

	protected $_backendClass = 'User_Model_Avatar_Backend';

	public function clear()
	{
		$this->_data = array();
		return $this;
	}

	public function getByUserId($userId)
	{
		$this->clear();
		$this->_getbackend()->getByUserId($this, $userId);
		return $this;
	}
	
	public function save()
	{
		$cache = Zend_Registry::get('output_cache');
		$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('search_items'));
		parent::save();
	}

}