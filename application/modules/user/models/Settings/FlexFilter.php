<?php

class User_Model_Settings_FlexFilter extends Ikantam_Filter_Flex {
    
    protected $_acceptClass = 'User_Model_Settings_Collection';
    
    protected $_joins = array (
        'app_settings' => 'INNER JOIN `app_settings` ON `users_app_settings`.`app_setting_id` = `app_settings`.`id`',
        'user' => 'INNER JOIN `users` ON `users_app_settings`.`user_id` = `users`.`id`',
    );
    
    protected $_select = array  (
    
    );
    
    protected $_rules = array(
        'user_id' => array('user' => '`users`.`id`'),
        'app_setting_name' => array('app_settings' => '`app_settings`.`name`'),
        'app_setting_id' => array('app_settings' => '`app_settings`.`id`'),
        'app_setting_description' => array('app_settings' => '`app_settings`.`description'),
    );     
}