<?php

class User_Model_Settings_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'users_app_settings';
    protected function _init()
    {
        $this->setSettings(array('db_insert' => array('on_duplicate_entry' => 'ignore'))); 
    }

}