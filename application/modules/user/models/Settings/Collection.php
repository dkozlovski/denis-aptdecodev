<?php
class User_Model_Settings_Collection extends Application_Model_Abstract_Collection implements 
    Ikantam_Filter_Flex_Interface
{
    protected $_itemObjectClass = 'User_Model_Settings';
    
    public function getFlexFilter() 
    {
        return new User_Model_Settings_FlexFilter($this);    
    }
    
}