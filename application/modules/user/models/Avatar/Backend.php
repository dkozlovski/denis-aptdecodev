<?php

class User_Model_Avatar_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'avatars';
 
 
 public function getByUserId (\Application_Model_Abstract $object, $userId)
 {
    $sql = "SELECT * FROM `".$this->_getTable()."` WHERE `user_id` = :uId LIMIT 1";
    
    $stmt = $this->_getConnection()->prepare($sql);
    
    $stmt->bindParam(':uId', $userId);
    $stmt->execute();
    $result =  $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(isset($result[0])) $object->setData($result[0]);
    
 }
 
 //---------------------------------------------------------------------------
 	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `' . $this->_getTable() . '` (`id`, `user_id`, `path`) VALUES(NULL, :uId, :path)';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':uId', $object->getUserId());
		$stmt->bindParam(':path', $object->getPath());
		$stmt->execute();

		$object->setId($this->_getConnection()->lastInsertId());
	}

	protected function _update(\Application_Model_Abstract $object)
	{
	    $sql = "UPDATE `".$this->_getTable()."` SET `user_id` = :uId, `path` = :path WHERE `id` = :id";
        
		$stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':id', $object->getId());
		$stmt->bindParam(':uId', $object->getUserId());
		$stmt->bindParam(':path', $object->getPath());
		$stmt->execute();		
	}  
}