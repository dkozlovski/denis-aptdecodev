<?php

class User_Model_Address extends Application_Model_Abstract
{

    public function getStateName()
    {
        $state = new Application_Model_State();
        $state->getByCode($this->getState());
        return $state->getName();
    }

    public function getByHash($hash)
    {
        $this->_getBackend()->getByHash($this, $hash);
        return $this;
    }

    public function setPrimary()
    {
        if (!$this->getId()) {
            throw new Exception('ID should be required');
        }

        if (!$this->getUserId()) {
            throw new Exception('User ID should be required');
        }

        $this->_getbackend()->setPrimary($this);
        return $this;
    }

    public function toString($delimiter = "\n")
    {
        $str = '';

        $str .= $this->getFullName() . $delimiter;

        if ($this->getPhoneNumber()) {
            $str .= $this->getPhoneNumber() . $delimiter;
        }

        $str .= $this->getAddressLine1();

        if ($this->getAddressLine2()) {
            $str .= ' ' . $this->getAddressLine2();
        }

        $str .= $delimiter;
        $str .= $this->getCity() . ' ' . $this->getState() . ' ' . $this->getPostcode() . $delimiter;
        $str .= $this->getCountryCode();

        return $str;
    }
    
    public function toHtml()
    {
        $str = '<ul>';

        $str .= '<li>' . $this->getFullName() . '</li>';

        if ($this->getTelephoneNumber()) {
            $str .= '<li>' . $this->getTelephoneNumber() . '</li>';
        }elseif ($this->getPhoneNumber()) {
            $str .= '<li>' . $this->getPhoneNumber() . '</li>';
        }

        $str .= '<li>' . $this->getAddressLine1();

        if ($this->getAddressLine2()) {
            $str .= ' ' . $this->getAddressLine2();
        }
        
        $str .=  '</li>';
        
        $str .= '<li>' . $this->getCity() . ' ' . $this->getState() . ' ' . $this->getPostalCode() . '</li>';
        $str .= '<li>' . $this->getCountryCode() . '</li>';

        return $str . '</ul>';
    }
    
    public function toOnlyAddress($delimiter = "\n")
    {
        $str = '';

        if ($this->getPhoneNumber()) {
            $str .= $this->getPhoneNumber() . $delimiter;
        }

        $str .= $this->getAddressLine1();

        if ($this->getAddressLine2()) {
            $str .= ' ' . $this->getAddressLine2();
        }

        $str .= $delimiter;
        $str .= $this->getCity() . ' ' . $this->getState() . ' ' . $this->getPostcode() . $delimiter;
        $str .= $this->getCountryCode();

        return $str;
    }
}
