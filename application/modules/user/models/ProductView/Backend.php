<?php

class User_Model_ProductView_Backend extends Application_Model_Abstract_Backend
{

    protected $_table   = 'user_product_view_history';
    protected $_fields_ = array('id', 'user_id', 'product_id', 'first_view', 'last_view', 'view_count');

    protected function _insert(Application_Model_Abstract $object)
    {

        $object->setFirstView($object->getFirstView(time())) //if value empty set it to current time...
                ->setLastView($object->getLastView(time()))
                ->setViewCount($object->getViewCount(1));

        $this->runStandartInsert(array('user_id', 'product_id', 'first_view', 'last_view', 'view_count'), $object);
    }

    protected function _update(Application_Model_Abstract $object)
    {

        $object->setLastView(time())
                ->setViewCount($object->getViewCount(1) + 1);

        $this->runStandartUpdate(array('last_view', 'view_count'), $object);
    }

    public function getByPairUserProduct(Application_Model_Abstract $object)
    {
        $sql  = "SELECT * FROM `" . $this->_getTable() . "` WHERE `user_id` = :uid AND `product_id` = :pid LIMIT 1";
        $u_id = $object->getUserId();
        $p_id = $object->getProductId();

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':uid', $u_id);
        $stmt->bindParam(':pid', $p_id);

        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($result) {
            $object->setData($result);
        }
    }

    public function getUserLiveHistory(Application_Model_Abstract_Collection $collection, $userId, $limit)
    {

        $cartId = User_Model_Session::instance()->getCart()->getId();


        $sql = "SELECT `history`.* FROM `" . $this->_getTable() . "` AS `history`
                INNER JOIN `products` ON `products`.`id` = `history`.`product_id`
                AND `products`.`qty` > 0
                AND `products`.`is_published` = 1
                AND `products`.`is_visible` = 1
                AND `products`.`is_available_for_purchase` = 1
                AND `products`.`user_id` != :uid
                WHERE `history`.`user_id` = :uid1 
                AND `history`.`product_id` NOT IN (SELECT `product_id` FROM `cart_items` WHERE `cart_id` = :cart_id)
                ORDER BY `history`.`last_view` DESC";

        if ($limit) {
            $sql .= ' LIMIT :lim';
        }

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':cart_id', $cartId);

        $stmt->bindParam(':uid', $userId);
        $stmt->bindParam(':uid1', $userId);

        if ($limit) {
            $stmt->bindParam(':lim', $limit);
        }

        $this->fillCollection($stmt, $collection);
    }

}