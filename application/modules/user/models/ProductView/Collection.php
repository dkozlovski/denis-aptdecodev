<?php
class User_Model_ProductView_Collection extends Application_Model_Abstract_Collection {
    
   protected $_backendClass = 'User_Model_ProductView_Backend';   

	/** Retreives product view history for the CURRENT USER. Exclude unsaleable, products in cart and own products,
     *  ordered by last view. 
     * @param int $limit 
     * @return self
     */  
   public function getUserViewHistoryWithLiveProducts ($limit = null)
   {
        $userId = User_Model_Session::instance()->getUserId();
        $this->_getBackend()->getUserLiveHistory($this, $userId, $limit);
        return $this;
   }  
   

}