<?php

class User_Model_FacebookUser extends Application_Model_Abstract
{

	protected $_backendClass = 'User_Model_FacebookUser_Backend';

    public function getByFacebookUserId($userId)
	{
		$this->_getbackend()->getByFacebookUserId($this, $userId);
		return $this;
	}
    
	public function getByUserId($userId)
	{
		$this->_getbackend()->getByUserId($this, $userId);
		return $this;
	}


}