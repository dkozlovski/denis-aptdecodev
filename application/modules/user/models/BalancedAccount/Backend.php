<?php

class User_Model_BalancedAccount_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'balanced_accounts';

    public function getByUserId(\Application_Model_Abstract $object, $userId)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `user_id` = :user_id LIMIT 1";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $userId);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            $object->setData($result);
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `balanced_accounts` 
            
            (`id`, `user_id`, `name`, `email_address`, `uri`,
            `bank_accounts_uri`, `cards_uri`, `credits_uri`, `debits_uri`,
            `holds_uri`, `refunds_uri`, `transactions_uri`, `created_at`,
            `is_buyer`, `is_merchant`)
            
            VALUES(:id, :user_id, :name, :email_address, :uri,
            :bank_accounts_uri, :cards_uri, :credits_uri, :debits_uri,
            :holds_uri, :refunds_uri, :transactions_uri, :created_at,
            :is_buyer, :is_merchant)';

        $stmt = $this->_getConnection()->prepare($sql);

        $id = null;
        $userId = $object->getUserId();
        $name = $object->getName();
        $emailAddress = $object->getEmailAddress();
        $uri = $object->getUri();
        $bankAccountsUri = $object->getBankAccountsUri();
        $cardsUri = $object->getCardsUri();
        $creditsUri = $object->getCreditsUri();
        $debitsUri = $object->getDebitsUri();
        $holdsUri = $object->getHoldsUri();
        $refundsUri = $object->getRefundsUri();
        $transactionsUri = $object->getTransactionsUri();
        $createdAt = $object->getCreatedAt();
        $isBuyer = $object->getIsBuyer();
        $isMerchant = $object->getIsMerchant();

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email_address', $emailAddress);
        $stmt->bindParam(':uri', $uri);
        $stmt->bindParam(':bank_accounts_uri', $bankAccountsUri);
        $stmt->bindParam(':cards_uri', $cardsUri);
        $stmt->bindParam(':credits_uri', $creditsUri);
        $stmt->bindParam(':debits_uri', $debitsUri);
        $stmt->bindParam(':holds_uri', $holdsUri);
        $stmt->bindParam(':refunds_uri', $refundsUri);
        $stmt->bindParam(':transactions_uri', $transactionsUri);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':is_buyer', $isBuyer);
        $stmt->bindParam(':is_merchant', $isMerchant);

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = "UPDATE `balanced_accounts` SET
            `user_id` = :user_id, `name` = :name, `email_address` = :email_address,
            `uri` = :uri, `bank_accounts_uri` = :bank_accounts_uri,
            `cards_uri` = :cards_uri, `credits_uri` = :credits_uri,
            `debits_uri` = :debits_uri, `holds_uri` = :holds_uri, 
            `refunds_uri` = :refunds_uri, `transactions_uri` = :transactions_uri,
            `created_at` = :created_at, `is_buyer` = :is_buyer,
            `is_merchant` = :is_merchant
            
            WHERE `id` = :id";

        $id = null;
        $userId = $object->getUserId();
        $name = $object->getName();
        $emailAddress = $object->getEmailAddress();
        $uri = $object->getUri();
        $bankAccountsUri = $object->getBankAccountsUri();
        $cardsUri = $object->getCardsUri();
        $creditsUri = $object->getCreditsUri();
        $debitsUri = $object->getDebitsUri();
        $holdsUri = $object->getHoldsUri();
        $refundsUri = $object->getRefundsUri();
        $transactionsUri = $object->getTransactionsUri();
        $createdAt = $object->getCreatedAt();
        $isBuyer = $object->getIsBuyer();
        $isMerchant = $object->getIsMerchant();

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email_address', $emailAddress);
        $stmt->bindParam(':uri', $uri);
        $stmt->bindParam(':bank_accounts_uri', $bankAccountsUri);
        $stmt->bindParam(':cards_uri', $cardsUri);
        $stmt->bindParam(':credits_uri', $creditsUri);
        $stmt->bindParam(':debits_uri', $debitsUri);
        $stmt->bindParam(':holds_uri', $holdsUri);
        $stmt->bindParam(':refunds_uri', $refundsUri);
        $stmt->bindParam(':transactions_uri', $transactionsUri);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':is_buyer', $isBuyer);
        $stmt->bindParam(':is_merchant', $isMerchant);

        $stmt->execute();
    }

}