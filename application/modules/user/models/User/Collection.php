<?php

class User_Model_User_Collection extends Application_Model_User_Collection
{

    protected $_itemObjectClass = 'User_Model_User';

    public function getByConversationId($id)
    {
        $this->_getBackend()->getByConversationId($this, $id);
        return $this;
    }
    
    public function getUser($id)
    {   
       $user = new Application_Model_User();
       $this->_getBackend()->getById($user, $id);
       return $user;
    }
    
    public function userRatingUp($userID, $rating)
    {
        $this->_getBackend()->userRatingUp($userID, $rating);
    }

}