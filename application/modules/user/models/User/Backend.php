<?php

class User_Model_User_Backend extends Application_Model_User_Backend
{

    public function getByConversationId($collection, $id)
    {
        $sql = 'SELECT DISTINCT (
            `conversations_users`.`user_id`
            ) AS `id` , `users` . *
            FROM `conversations_users`
            JOIN `users` ON `users`.`id` = `conversations_users`.`user_id`
            WHERE `conversation_id` = :id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new User_Model_User();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

}