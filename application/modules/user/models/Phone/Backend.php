<?php

class User_Model_Phone_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'user_phones';

    public function getByUserId(\Application_Model_Abstract_Collection $collection, $userId)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `user_id` = :uId";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':uId', $userId);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($result)) {
            foreach ($result as $row) {
                $phone = new User_Model_Phone();
                $phone->addData($row);
                $collection->addItem($phone);
            }
        }
    }

    public function getPrimaryByUserId($phone, $userId)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `user_id` = :uId and `is_primary` = 1";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':uId', $userId);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $phone->addData($row);
        }
    }

    //---------------------------------------------------------------------------
    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `' . $this->_getTable() . '` (`id`, `user_id`, `phone`, `is_primary`) VALUES(NULL, :uId, :phone, :is_primary)';

        $stmt = $this->_getConnection()->prepare($sql);
        $userId = $object->getUserId();
        $phone = $object->getPhone();
        $isPrimary = $object->getIsPrimary();

        $stmt->bindParam(':uId', $userId);
        $stmt->bindParam(':phone', $phone);
        $stmt->bindParam(':is_primary', $isPrimary);
        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = "UPDATE `" . $this->_getTable() . "` SET `user_id` = :uId, `phone` = :phone, `is_primary` = :is_primary WHERE `id` = :id";

        $stmt = $this->_getConnection()->prepare($sql);
        $id = $object->getId();
        $userId = $object->getUserId();
        $phone = $object->getPhone();
        $isPrimary = $object->getIsPrimary();

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':uId', $userId);
        $stmt->bindParam(':phone', $phone);
        $stmt->bindParam(':is_primary', $isPrimary);
        $stmt->execute();
    }

}