<?php

class User_Model_Phone_Collection extends Application_Model_Abstract_Collection
{

    protected $_backendClass = 'User_Model_Email_Backend';

    public function getByUserId($id)
    {
        $this->_getBackend()->getByUserId($this, $id);
        return $this;
    }

}