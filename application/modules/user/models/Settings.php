<?php
class User_Model_Settings extends Application_Model_Abstract
{
    public function getCollection ()
    {
        return new User_Model_Settings_Collection();
    } 
    
    public function getAppSettings ()
    {
        return new Application_Model_AppSettings();
    }
    
    /**
     * Get item
     * 
     * @param array $conditions  
     * @return mixed
     */
    public function getByCondition (array $conditions)
    {
        $filter = $this->getCollection()->getFlexFilter();
        foreach ($conditions as $method => $args) {
            call_user_func_array(array($filter, $method), $args);
        }
        
        $this->setData($filter->order('id asc')->apply(1)->getFirstItem()->getData());        
        return $this;
    }       
    
    /**
     * Get setting value
     * 
     * @param array $conditions  
     * @return mixed
     */
    public function getValueByCondition (array $conditions)
    {
        $filter = $this->getCollection()->getFlexFilter();
        foreach ($conditions as $method => $args) {
            call_user_func_array(array($filter, $method), $args);
        }
        
        return $filter->apply(1)->getFirstItem()->getValue();
    }
    
    /**
     * Save setting for user | overwrite existing
     * 
     * @param  User_Model_User $user
     * @param  string $name - setting name
     * @param  string $value
     * @return object self
     */
    public function saveSetting (\User_Model_User $user, $name, $value)
    {
        if(!$user->isExists()) {
            return $this;
        }
        
        $appSetting = $this->getAppSettings()->getByCondition(array('name' => array('=', $name)));

        if(!$appSetting->isExists()){ 
            $appSetting->setName($name)->save();
        } 
        
        //overwrite if exist
        $this->getByCondition(array('user_id' => array('=', $user->getId()), 'app_setting_id' => array('=', $appSetting->getId()))); 
        
        $this->setAppSettingId($appSetting->getId())
             ->setUserId($user->getId())
             ->setValue($value)
             ->save();
             
        return $this;                   
    }
}