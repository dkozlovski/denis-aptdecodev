<?php

class User_Model_Conversation extends Application_Model_Abstract
{

	protected $_messages;
	protected $_message;
	protected $_author;
	protected $_participants = array();
	
	public function __construct($id = null)
	{
		$this->_participants = new User_Model_Conversation_Participant_Collection();
		parent::__construct($id);
	}

	public function getAllParticipants($userId)
	{
		$this->_participants->getByConversationId($this->getId(), $userId);
		return $this->_participants;
	}

	public function getAllMessages($userId)
	{
		if (!$this->_messages) {
			$this->_messages = new User_Model_Conversation_Message_Collection();
			$this->_messages->getByConversationId($this->getId(), $userId);
		}
		return $this->_messages;
	}

	public function addParticipant($userId)
	{
		$user = new Application_Model_User($userId);

		if ($user->getId()) {
			$participant = new \User_Model_Conversation_Participant();
			$participant->setUserId($userId);
			$this->_participants->addItem($participant);
		}
		return $this;
	}

	public function setMessage($message)
	{
		$this->_message = $message;
		return $this;
	}

	protected function getMessage()
	{
		return $this->_message;
	}

	protected function getAuthor()
	{
		return $this->_author;
	}

	public function setAuthor($author)
	{
		$participant = new User_Model_Conversation_Participant();
		$participant->getByUserId($author, $this->getId());
		if ($participant->getId()) {
			$this->_author = $participant->getId();
		} else {
			$this->_author = $author;
		}
		
		return $this;
	}

	protected function getParticipants()
	{
		return $this->_participants;
	}

	public function save()
	{
		$this->_getBackend()->save($this);
		$this->_afterSave();
		return $this;
	}

	protected function _afterSave()
	{
		$this->getAllParticipants(0);
		
		foreach ($this->getParticipants() as $participant) {
			
			$participant->setConversationId($this->getId())->save();
			if ($participant->getUserId() == $this->getAuthor()) {
				$this->setAuthor($participant->getId());
			}
		}

		$this->getMessage()
				->setAuthorId($this->getAuthor())
				->setConversationId($this->getId())
				->setCreatedAt(time())
				->save();

		foreach ($this->getParticipants() as $participant) {
			$messageParticipant = new User_Model_Conversation_MessageParticipant();
			$messageParticipant->setMessageId($this->getMessage()->getId())
					->setParticipantId($participant->getId())
					->setIsVisible(1)
					->setIsNew(1)
					->save();
		}
	}

}