<?php

class User_Model_PasswordReset extends Application_Model_Abstract
{

    protected $_backendClass = 'User_Model_PasswordReset_Backend';

    public function __construct($id = null)
    {

        if (!is_int($id) && !empty($id)) {
            throw new Exception('Invalid id. Id must be an integer value.');
        }

        if ($id) {
            $this->getById($id);
        }

        return $this;
    }

    public function getByCode($code)
    {
        $this->_getbackend()->getByCode($this, $code);
        return $this;
    }

    public function IsExpiryValid()
    {
        $expiry = time() - $this->getExpiryTime();

        if ($expiry >= 0 && $expiry <= 3600) {
            return true;
        }

        return false;
    }

}
