<?php

class User_Model_PasswordReset_Backend extends Application_Model_Abstract_Backend
{
	protected $_table = 'password_resets';
    
    
    
    public function getByCode(\Application_Model_Abstract $object, $code)
    {
        $sql = "SELECT * FROM `".$this->_getTable()."` WHERE `code` = :code LIMIT 1";
        
        $stmt = $this->_getConnection()->prepare($sql);
        
        $stmt->bindParam(':code', $code);
        $stmt->execute();
        
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
		if ($row) {
        	$object->addData($row);
        }
    }
	
	protected function _insert(\Application_Model_Abstract $object)
	{
 		$sql = "INSERT INTO `" . $this->_getTable() . "` (`user_email`, `expiry_time`, `code`)
  VALUES (:email, :expiry_time, :code)";
        
        $stmt = $this->_getConnection()->prepare($sql);
        
        $email      = $object->getUserEmail();
        $expiryTime = $object->getExpiryTime();
        $code       = $object->getCode();
        
        
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':expiry_time', $expiryTime);
        $stmt->bindParam(':code',  $code);
		
		$stmt->execute();
		$object->setId($this->_getConnection()->lastInsertId());
	}
	
	protected function _update(\Application_Model_Abstract $object)
	{
    	$sql = "UPDATE `" . $this->_getTable() . "` set `user_email` = :email, `expiry_time` = :expiry_time, `code`=:code WHERE `id` = :id";
       
        $stmt = $this->_getConnection()->prepare($sql);
        
        $id         = $object->getId();
        $email      = $object->getUserEmail();
        $expiryTime = $object->getExpiryTime();
        $code       = $object->getCode();
        
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':expiry_time', $expiryTime);
        $stmt->bindParam(':code',  $code);
        $stmt->bindParam(':id', $id);
		
		$stmt->execute();
		
	}
	
}
