<?php

class User_Model_Message_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'messages';

	public function getByUserId($collection, $id)
	{
		$sql = 'SELECT * FROM `messages_users` 
			JOIN `messages` on `messages_users`.`user_id` = `messages`.`recipient_id` 
			AND `messages`.`id` = `messages_users`.`message_id` 
			WHERE `messages`.`recipient_id` = :user_id 
			AND `messages_users`.`is_visible` = 1';
				
		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $id);

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new $this->_itemClass();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getNewByUserId($collection, $id)
	{
		$sql = 'SELECT * FROM `messages` 
			JOIN `messages_users` on `messages_users`.`user_id` = `messages`.`recipient_id` 
			AND `messages`.`id` = `messages_users`.`message_id` 
			WHERE `messages`.`recipient_id` = :user_id 
			AND `messages_users`.`is_visible` = 1 
			AND `messages_users`.`is_new` = 1';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':user_id', $id);

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new $this->_itemClass();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	public function getByAuthorId($collection, $id)
	{
		$sql = 'SELECT * FROM `messages` WHERE `author_id` = :author_id';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':author_id', $id);

		$stmt->execute();

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
			foreach ($rows as $row) {
				$item = new $this->_itemClass();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}

	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = 'INSERT INTO `' . $this->_getTable() . '` 
			(`author_id`, `recipient_id`, `subject`, `text`, `created_at`) 
			VALUES (:author_id, :recipient_id, :subject, :text, :created_at)';

		$stmt = $this->_getConnection()->prepare($sql);

		$authorId = $object->getAuthorId();
		$recipientId = $object->getRecipientId();
		$subject = $object->getSubject();
		$text = $object->getText();
		$createdAt = $object->getCreatedAt();

		$stmt->bindParam(':author_id', $authorId);
		$stmt->bindParam(':recipient_id', $recipientId);
		$stmt->bindParam(':subject', $subject);
		$stmt->bindParam(':text', $text);
		$stmt->bindParam(':created_at', $createdAt);

		$stmt->execute();

		$object->setId($this->_getConnection()->lastInsertId());
		
		

		$sql2 = 'INSERT INTO `messages_users` 
			(`message_id`, `user_id`, `is_visible`, `is_new`) 
			VALUES (:message_id1, :author_id, 1, 0), (:message_id2, :recipient_id, 1, 1)';
		
		$stmt2 = $this->_getConnection()->prepare($sql2);

		$messageId = $object->getId();
		
		$stmt2->bindParam(':message_id1', $messageId);
		$stmt2->bindParam(':message_id2', $messageId);
		$stmt2->bindParam(':author_id', $authorId);
		$stmt2->bindParam(':recipient_id', $recipientId);
		
		$stmt2->execute();
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		
	}

	
	
	public function setAsReadedBy($messageId, $userId)
	{
		$sql = 'UPDATE `messages_users` set `is_new` = 0
			WHERE `message_id` = :message_id AND `user_id` = :user_id';
		
		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':message_id', $messageId);
		$stmt->bindParam(':user_id', $userId);
		
		$stmt->execute();
	}
	
	public function setAsDeletedBy($messageId, $userId)
	{
		$sql = 'UPDATE `messages_users` set `is_visible` = 0
			WHERE `message_id` = :message_id AND `user_id` = :user_id';
		
		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':message_id', $messageId);
		$stmt->bindParam(':user_id', $userId);
		
		$stmt->execute();
	}
	
	/**
	 * 
	 * @param Application_Model_Abstract_Collection $collection
	 * @param string|numeric $userId
	 * @param string|numeric $messageId
	 */
	public function getConversationByUserIds($collection, $recipientId, $authorId)
	{
		$sql = 'SELECT distinct(`messages`.`id`) as `id`, `messages2`.`subject`, `messages2`.`text`, `messages2`.`created_at`
FROM `messages`
JOIN `messages_users` ON `messages`.`id` = `messages_users`.`message_id`

JOIN `messages` as `messages2` ON `messages`.`id` = `messages2`.`id`


WHERE (
`messages`.`author_id` = :author_id1
AND `messages`.`recipient_id` = :recipient_id1
)
OR (
`messages`.`author_id` = :author_id2
AND `messages`.`recipient_id` = :recipient_id2
)
AND `messages_users`.`is_visible` =1
ORDER BY `messages`.`created_at` DESC';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':author_id1', $authorId);
		$stmt->bindParam(':recipient_id1', $recipientId);
		
		$stmt->bindParam(':author_id2', $recipientId);
		$stmt->bindParam(':recipient_id2', $authorId);
		
		$stmt->execute();
		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if ($rows) {
			foreach ($rows as $row) {
				$item = new User_Model_Message();
				$item->addData($row);
				$collection->addItem($item);
			}
		}
	}
	
}