<?php

class User_Model_Message_Collection extends Application_Model_Abstract_Collection
{

	public function getByUserId($id)
	{
		$this->_getBackend()->getByUserId($this, $id);
		return $this;
	}

	public function getNewByUserId($id)
	{
		$this->_getBackend()->getNewByUserId($this, $id);
		return $this;
	}

	public function getByAuthorId($id)
	{
		$this->_getBackend()->getByAuthorId($this, $id);
		return $this;
	}
	
	public function getConversationByUserIds($recipientId, $authorId)
	{
		$this->_getBackend()->getConversationByUserIds($this, $recipientId, $authorId);
		return $this;
	}

}