<?php

class User_Model_FacebookUser_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'facebook_users';

    public function getByUserId(\Application_Model_Abstract $object, $userId)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `user_id` = :user_id LIMIT 1";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->setData($row);
        }
    }
    
    public function getByFacebookUserId(\Application_Model_Abstract $object, $facebookUserId)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `facebook_id` = :facebook_id LIMIT 1";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':facebook_id', $facebookUserId);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->setData($row);
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `' . $this->_getTable() . '` (`facebook_id`, `user_id`) VALUES(:facebook_id, :user_id)';

        $stmt = $this->_getConnection()->prepare($sql);
        
        $facebookId = $object->getFacebookId();
        $userId = $object->getUserId();
        
        $stmt->bindParam(':facebook_id', $facebookId);
        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        
    }

}