<?php

class User_Model_Email extends Application_Model_Abstract
{

    protected $_backendClass = 'User_Model_Email_Backend';

    public function getPrimaryByUserId($id)
    {
        $this->_getbackend()->getPrimaryByUserId($this, $id);
        return $this;
    }

}