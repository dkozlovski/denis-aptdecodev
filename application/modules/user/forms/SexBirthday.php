<?php

class User_Form_SexBirthday extends Ikantam_Form
{
    
    public function init()
	{
	   
       $sex = new Zend_Form_Element_Radio('sex');
       $sex->addMultiOptions(array('MALE'=>'Male', 'FEMALE'=>'Female'))
           ->setRequired();
       
       $birthday = new Zend_Form_Element_Text('birthday');
       $birthday->setRequired();
       
       $this->addElements(array($sex, $birthday));   
	
	}    
    
}