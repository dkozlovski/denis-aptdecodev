<?php

class User_Form_Address extends Ikantam_Form
{

    public function init()
    {
        $fullName = new Zend_Form_Element_Text('full_name');
        $fullName->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty')
                ->addErrorMessage('Enter Full Name!');

        $this->addElement($fullName);

        $addressLine1 = new Zend_Form_Element_Text('address_line1');
        $addressLine1->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty')
                ->addErrorMessage('Enter Address');
        $this->addElement($addressLine1);

        $addressLine2 = new Zend_Form_Element_Text('address_line2');
        $addressLine2->addFilters(array('StringTrim', 'StripTags'));
        $addressLine2->setRequired(false);
        $this->addElement($addressLine2);

        $city = new Zend_Form_Element_Text('city');
        $city->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty')
                ->addErrorMessage('Enter City!');
        $this->addElement($city);

        $state = new Zend_Form_Element_Text('state');
        $state->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty')
                ->addErrorMessage('Enter state!');
        $this->addElement($state);

        $postcode = new Zend_Form_Element_Text('postcode');

        $format = new Zend_Validate_PostCode(array('locale' => 'en_US'));

        $postcode->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty', true)
                ->addValidator($format, true)
                ->addErrorMessage('Enter Postcode!');
        $this->addElement($postcode);

        $phoneNumber = new Zend_Form_Element_Text('phone_number');

        $phoneNumber->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty', true)
                ->addErrorMessage('Enter Phone Number!');
        $this->addElement($phoneNumber);

        $country = new Zend_Form_Element_Select('country_code');
        $country->setRequired(true)
                ->addFilters(array('StringTrim', 'StripTags'))
                ->addValidator('NotEmpty')
                ->addValidator('InArray', true, array(array('US', 'CA')))
                ->addErrorMessage('Enter country!');
        $this->addElement($country);

        $phone = new Zend_Form_Element_Text('phone');
        $phone->addErrorMessage('Enter phone number!')
                ->addFilters(array('StringTrim', 'StripTags'));


        $this->addElement($phone);
    }

}
