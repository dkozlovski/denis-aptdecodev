<?php

class User_Form_AccountDetails extends Ikantam_Form
{
    
    public function init()
	{
	  $details = new Zend_Form_Element_Textarea('details');
      
      $details->addFilters(array('StringTrim','StripTags'));
      
      $this->addElement($details);		
	}    
    
}