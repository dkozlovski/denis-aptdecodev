<?php

class User_Form_Conversation_Existing extends Ikantam_Form
{

	public function init()
	{
		$text = new Zend_Form_Element_Textarea('text');

		$text->setRequired(true)
				->addFilters(array('StringTrim', 'StripTags'))
				->addValidator('NotEmpty')
				->addErrorMessage('Text is required');

		$this->addElement($text);
	}

}