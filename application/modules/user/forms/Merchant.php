<?php

class User_Form_Merchant extends Ikantam_Form
{

    const ERROR_FULL_NAME_EMPTY = 'Full name required.';
    const ERROR_FULL_NAME_LENGTH = 'Full name length should be between 2 and 100 characters.';
    const ERROR_PHONE_NUMBER_EMPTY = 'Phone number required.';
    const ERROR_PHONE_NUMBER_LENGTH = 'Phone number length should be between 2 and 15 characters.';
    const ERROR_POSTAL_CODE_EMPTY = 'Postal code required.';
    const ERROR_POSTAL_CODE_LENGTH = 'Postal code length should be between 2 and 20 characters.';
    const ERROR_STREET_ADDRESS_EMPTY = 'Street address required.';
    const ERROR_STREET_ADDRESS_LENGTH = 'Street address length should be between 1 and 255 characters.';
    const ERROR_DATE_OF_BIRTH_EMPTY = 'Date of birth required.';
    const ERROR_DATE_OF_BIRTH_VALUE = 'Date of birth should be a valid date.';
    const ERROR_ROUTING_NUMBER_EMPTY = 'Routing number required.';
    const ERROR_ROUTING_NUMBER_LENGTH = 'Routing number should be 9';
    const ERROR_ACCOUNT_NUMBER_EMPTY = 'Account number required.';
    const ERROR_ACCOUNT_NUMBER_LENGTH = 'Account number length should be between 1 and 50 characters.';
    const ERROR_ACCOUNT_TYPE_EMPTY = 'Account type required.';
    const ERROR_ACCOUNT_TYPE_VALUE = 'Account type required';
    const ERROR_BA_URI_EMPTY = 'Bank account information required';

    protected function getFullNameElement()
    {
        $fullName = new Zend_Form_Element_Text('full_name');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_FULL_NAME_EMPTY);

        $length = new Zend_Validate_StringLength(array('min' => 2, 'max' => 100));
        $length->setMessage(self::ERROR_FULL_NAME_LENGTH);

        $fullName->setRequired(true)
                ->addFilters(array('StringTrim'))
                ->addValidator($notEmpty, true)
                ->addValidator($length, true);
        
        $fullName->setDecorators(array('ViewHelper'));

        return $fullName;
    }

    protected function getPhoneNumberElement()
    {
        $fullName = new Zend_Form_Element_Text('phone_number');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_PHONE_NUMBER_EMPTY);

        $length = new Zend_Validate_StringLength(array('min' => 2, 'max' => 15));
        $length->setMessage(self::ERROR_PHONE_NUMBER_LENGTH);

        $fullName->setRequired(true)
                ->addFilters(array('StringTrim', 'Digits'))
                ->addValidator($notEmpty, true)
                ->addValidator($length, true)
                ->setAttrib('Placeholder', '+000 0000 000');
        
        $fullName->setDecorators(array('ViewHelper'));

        return $fullName;
    }

    protected function getPostalCodeElement()
    {
        $fullName = new Zend_Form_Element_Text('postal_code');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_POSTAL_CODE_EMPTY);

        $length = new Zend_Validate_StringLength(array('min' => 2, 'max' => 20));
        $length->setMessage(self::ERROR_POSTAL_CODE_LENGTH);

        $fullName->setRequired(true)
                ->addFilters(array('StringTrim'))
                ->addValidator($notEmpty, true)
                ->addValidator($length, true);
        
        $fullName->setDecorators(array('ViewHelper'));

        return $fullName;
    }

    protected function getStreetAddressElement()
    {
        $fullName = new Zend_Form_Element_Text('street_address');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_STREET_ADDRESS_EMPTY);

        $length = new Zend_Validate_StringLength(array('min' => 1, 'max' => 255));
        $length->setMessage(self::ERROR_STREET_ADDRESS_LENGTH);

        $fullName->setRequired(true)
                ->addFilters(array('StringTrim'))
                ->addValidator($notEmpty, true)
                ->addValidator($length, true);
        
        $fullName->setDecorators(array('ViewHelper'));

        return $fullName;
    }

    protected function getDateOfBirthElement()
    {
        $fullName = new Zend_Form_Element_Text('date_of_birth');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_DATE_OF_BIRTH_EMPTY);

        $length = new Zend_Validate_Date();
        $length->setMessage(self::ERROR_DATE_OF_BIRTH_VALUE);

        $fullName->setRequired(true)
                ->addFilters(array('StringTrim'))
                ->addValidator($notEmpty, true)
                ->addValidator($length, true);
        
        $fullName->setDecorators(array('ViewHelper'));

        return $fullName;
    }

    protected function getRoutingNumberElement()
    {
        $fullName = new Zend_Form_Element_Text('routing_number');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_ROUTING_NUMBER_EMPTY);

        $length = new Zend_Validate_StringLength(array('min' => 9, 'max' => 9));
        $length->setMessage(self::ERROR_ROUTING_NUMBER_LENGTH);

        $fullName->setRequired(true)
                ->addFilters(array('StringTrim', 'Digits'))
                ->addValidator($notEmpty, true)
                ->addValidator($length, true)
                ->setAttrib('Placeholder', '000 000 000');
        
        $fullName->setDecorators(array('ViewHelper'));

        return $fullName;
    }

    protected function getAccountNumberElement()
    {
        $fullName = new Zend_Form_Element_Text('account_number');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_ACCOUNT_NUMBER_EMPTY);

        $length = new Zend_Validate_StringLength(array('min' => 1, 'max' => 50));
        $length->setMessage(self::ERROR_ACCOUNT_NUMBER_LENGTH);

        $fullName->setRequired(true)
                ->setIgnore(true)
                ->addFilters(array('StringTrim'))
                ->addValidator($notEmpty, true)
                ->addValidator($length, true);
        
        $fullName->setDecorators(array('ViewHelper'));

        return $fullName;
    }

    protected function getAccountTypeElement()
    {
        $fullName = new Zend_Form_Element_Radio('account_type');

        $options = array('checking' => 'checking', 'savings' => 'savings');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_ACCOUNT_TYPE_EMPTY);

        $inArray = new Zend_Validate_InArray($options);
        $inArray->setMessage(self::ERROR_ACCOUNT_TYPE_VALUE);

        $fullName->setRequired(true)
                ->addFilters(array('StringTrim'))
                ->addValidator($notEmpty, true)
                ->addValidator($inArray, true)
                ->addMultiOptions($options);
        
        $fullName->setDecorators(array('ViewHelper'));

        return $fullName;
    }
    
    protected function getBankAccountUriElement()
    {
        $fullName = new Zend_Form_Element_Hidden('ba_uri');
        
        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_BA_URI_EMPTY);
        
        $fullName->setRequired(true)
                ->addFilters(array('StringTrim'))
                ->addValidator($notEmpty, true);
        
        $fullName->setDecorators(array('ViewHelper'));
        
        return $fullName;
    }
    
    protected function getSubmitElement()
    {
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save');
        return $submit;
    }

    public function init()
    {
        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'payout/form_decorator.phtml'))
        ));
        
        $this->setMethod('post')
                ->addElement($this->getFullNameElement())
                //->addElement($this->getPhoneNumberElement())
                //->addElement($this->getPostalCodeElement())
               // ->addElement($this->getStreetAddressElement())
               // ->addElement($this->getDateOfBirthElement())
                ->addElement($this->getRoutingNumberElement())
                ->addElement($this->getAccountNumberElement())
                //->addElement($this->getAccountTypeElement())
                ->addElement($this->getBankAccountUriElement())
                ->addElement($this->getSubmitElement());
    }

}