<?php

class User_Form_ChangePassword extends Ikantam_Form
{   
    const  OLD_PASS_EMPTY       = 'Old password is required.';
    const  NEW_PASS_EMPTY       = 'New password is required.';
    const  CONFIRM_PASS_EMPTY   = 'Confirm password is required.';
    const  PASS_NOT_MATCH       = 'New password does not match the confirm password.';
    
    public function init()
	{
	   $vOldEmpty = new Zend_Validate_NotEmpty();
       $vOldEmpty->setMessage(self::OLD_PASS_EMPTY);
       
       $vNewEmpty = new Zend_Validate_NotEmpty();
       $vNewEmpty->setMessage(self::NEW_PASS_EMPTY);
       
       $vConfirmEmpty = new Zend_Validate_NotEmpty();
       $vConfirmEmpty->setMessage(self::CONFIRM_PASS_EMPTY);
       
       $vIdentical = new Zend_Validate_Identical();
       $vIdentical->setMessage(self::PASS_NOT_MATCH);
       
	   $oldPass = new Zend_Form_Element_Password('oldpass');
       $newPass = new Zend_Form_Element_Password('newpass');
       $confirm = new Zend_Form_Element_Password('confirm');
       
       $oldPass->setRequired()
               ->addValidator($vOldEmpty, true);
               
       $newPass->setRequired()
               ->addValidator($vNewEmpty, true);
               
       $confirm->setRequired()
               ->addValidator($vConfirmEmpty, true)
               ->addValidator($vIdentical, true, array('token'=>'newpass'));
               
       $this->addElements(array($oldPass, $newPass, $confirm));
		
	}
    
    
}