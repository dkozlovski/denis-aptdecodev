<?php

class User_Form_EmailsAndAlerts extends Zend_Form
{
    public function init()
    {
        $defaultDecorator = array(
            'ViewHelper',
            'Errors',
            array('Description', array('tag' => 'p', 'placement' => 'prepend', 'class' => 'el-heading description', 'escape' => false)),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'  => 'control-group')),
            array('Label', array('tag' => 'div', 'class' => 'el-heading')),
        );

        $defaultOptions = array(
            '0'          => 'Receive Individual emails',
            '86400'      => 'Receive Daily emails',
            '604800'     => 'Receive Weekly emails',
            '2592000'    => 'Receive Monthly emails',
            '3155692600' => 'Never receive emails'
        );

       /* $newsletter =  new Zend_Form_Element_Select('notification_newsletter');
        $newsletter->setLabel('AptDeco Newsletter')
            ->setDescription('New items, new in blog')
            ->setMultiOptions($defaultOptions)
            ->setValue(86400) // day
            ->setAttrib('class', 'form-field')
            ->setDecorators($defaultDecorator);*/

        $productAlerts =  new Zend_Form_Element_Select('notification_product_alerts');
        $productAlerts->setLabel('Product Alerts')
            ->setDescription('We will send you an email alert as soon as a new product that matches your criteria is added. <a href="' . Ikantam_Url::getUrl('product/alert') . '"><strong>Manage alerts</strong></a>')
            ->setMultiOptions($defaultOptions)
            ->setValue(86400) // day
            ->setAttrib('class', 'form-field')
            ->setDecorators($defaultDecorator);

        $cartAndWishlist =  new Zend_Form_Element_Select('notification_cart_and_wishlist');
        $cartAndWishlist->setLabel('Wishlist and Cart Notifications')
            ->setDescription('Notification about products in you Cart and Wishlist')
            ->setMultiOptions($defaultOptions)
            ->setValue(86400) // month
            ->setAttrib('class', 'form-field')
            ->setDecorators($defaultDecorator);


      /*  $promotions =  new Zend_Form_Element_Select('notification_promotions');
        $promotions->setLabel('Promotions')
            ->setDescription('Notifications about products with reduced price. Check <a href="' . Ikantam_Url::getUrl('catalog/collection/sale') . '"><strong>Sale Collection</strong></a>')
            ->setMultiOptions($defaultOptions)
            ->setValue(86400) // day
            ->setAttrib('class', 'form-field')
            ->setDecorators($defaultDecorator);*/


        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div', 'class' => '' )),
            'Form',
        ));
        $this->setAttrib('class', 'apt-form apt-v-last dec-form');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save')
            ->setAttrib('class', 'apt-btn prime');

        $this->addElements(array(/*$newsletter,*/ $productAlerts, $cartAndWishlist, /*$promotions,*/ $submit));
    }


}