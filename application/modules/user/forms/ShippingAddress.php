<?php

class User_Form_ShippingAddress extends Checkout_Form_ShippingAddress
{

    public function init()
    {
        parent::init();
        $this->setAction(Ikantam_Url::getUrl('user/profile/edit-address'))
            ->setAttrib('class', 'edit-address-form');

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('Save')
            ->setAttrib('class', 'apt-btn  prime'); // .edit-address-form  uses for submit form
        $this->addElement($submit);

        $this->_setDataRequired();

    }

    public function _setDataRequired()
    {
        parent::_setDataRequired();
        $this->getElement('id')->setRequired(false);
        $this->getElement('submit')->setRequired(false);
    }

    protected function _getPostalCodeElement()
    {
        $cityElement = new Zend_Form_Element_Text('postcode');

        $cityElement->setAttrib('name', 'shipping_address[postcode]')
            ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
            ->setRequired(false)
            ->setAttrib('class', 'form-field m-r5')
            ->setAttrib('placeholder', 'ZIP')
            ->setAttrib('maxlength', '5');

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'eq-fields', 'openOnly' => true)),
        ));

        return $cityElement;
    }

    protected function _getPhoneNumberElement()
    {
        $cityElement = new Zend_Form_Element_Text('phone_number');

        $cityElement->setAttrib('name', 'shipping_address[phone_number]')
            ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
            ->setRequired(false)
            ->setAttrib('class', 'form-field field-i tel left numb-format')
            ->setAttrib('placeholder', 'Your Phone');

        $cityElement->setDecorators(array(
            'ViewHelper',
        ));

        return $cityElement;
    }

}
