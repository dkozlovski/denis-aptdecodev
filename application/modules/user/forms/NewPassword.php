<?php

class User_Form_NewPassword extends Ikantam_Form
{
	const ERROR_PASSWORD_EMPTY = 'Password is required.';
    const ERROR_OLDPASSWORD_EMPTY = 'Old password is required.';
	const ERROR_CONFIRMATION_EMPTY = 'Confirmation is required.';
	const ERROR_CONFIRMATION_WRONG = 'Passwords should match.';
	
	public function init()
	{
	   
	    $oldPassword = new Zend_Form_Element_Password('oldpassword');
         
        $vEmptyOld = new Zend_Validate_NotEmpty();
        $vEmptyOld->setMessage(self::ERROR_OLDPASSWORD_EMPTY);
        $oldPassword->setRequired()
                    ->addValidator($vEmptyOld, true);
       
		$password = new Zend_Form_Element_Password('newpassword');

		
		$notEmpty = new Zend_Validate_NotEmpty();
		$notEmpty->setMessage(self::ERROR_PASSWORD_EMPTY);

		$password->setRequired()
				 ->addValidator($notEmpty, true);

		
		$confirmation = new Zend_Form_Element_Password('confirmation');

		$notEmpty2 = new Zend_Validate_NotEmpty();
		$notEmpty2->setMessage(self::ERROR_CONFIRMATION_EMPTY);
		
		$identical = new Zend_Validate_Identical('newpassword');
		$identical->setMessage(self::ERROR_CONFIRMATION_WRONG);
		
		$confirmation->setRequired()
    				 ->addValidator($notEmpty2, true)
    				 ->addValidator($identical, true); 	
                     
		$this->addElements(array($oldPassword, $password, $confirmation));
	}

}
