<?php

class User_Form_Registration extends Ikantam_Form
{

    const ERROR_PASSWORD_EMPTY              = 'Please enter password.';
    const ERROR_FULLNAME_EMPTY              = 'Please enter full name.';
    const ERROR_PASSWORD_CONFIRMATION_EMPTY = 'Please retype password.';
    const ERROR_PASSWORD_CONFIRMATION       = 'Passwords not match.';
    const ERROR_FULLNAME_LENGTH             = 'Full name should be under 100 characters.';

    //const ERROR_PASSWORD_CONFIRMATION_REQ = 'Password confirmation required.';

    public function init()
    {

        $fullName = new Zend_Form_Element_Text('full_name');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_FULLNAME_EMPTY);

        $stringLength = new Zend_Validate_StringLength(array('max' => 100));
        $stringLength->setMessage(self::ERROR_FULLNAME_LENGTH);

        $fullName->setRequired(true)
                ->addFilters(array('StripTags', 'StringTrim'))
                ->addValidator($notEmpty)
                ->addValidator($stringLength);

        $password = new Zend_Form_Element_Password('password');

        $password->setRequired(true)
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addErrorMessage(self::ERROR_PASSWORD_EMPTY);

        $identical = new Zend_Validate_Identical('password');
        $identical->setMessage(self::ERROR_PASSWORD_CONFIRMATION);

        $confirm = new Zend_Form_Element_Text('password_confirm');

        $vConfirmationEmpty = new Zend_Validate_NotEmpty();
        $vConfirmationEmpty->setMessage(self::ERROR_PASSWORD_CONFIRMATION_EMPTY);

        $confirm->setRequired()
                ->addValidator($vConfirmationEmpty)
                ->addValidator($identical);


        $email = new Zend_Form_Element_Hidden('email');

        $email->addFilters(array('StripTags', 'StringTrim'))
                ->setRequired(true)
                ->addValidator('notEmpty')
                ->addValidator('EmailAddress')
                ->addValidator('StringLength', array('max' => 100))
                ->addErrorMessage('Please enter a valid email.');

        $terms = new Zend_Form_Element_Checkbox('terms');

        $terms->addValidator(new Zend_Validate_InArray(array(1)))
                ->addErrorMessage('You should accept Terms of Use an Privacy Policy.');

        $this->addElements(array($fullName, $email, $password, $confirm, $terms));
    }

}
