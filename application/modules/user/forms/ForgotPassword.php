<?php

class User_Form_ForgotPassword extends Ikantam_Form
{

    const ERROR_EMAIL_EMPTY   = 'Please enter your e-mail.';
    const ERROR_EMAIL_INVALID = 'The email is not valid.';

    public function init()
    {
        $email = new Zend_Form_Element_Text('email');

        $notEmpty = new Zend_Validate_NotEmpty();
        $notEmpty->setMessage(self::ERROR_EMAIL_EMPTY);

        $emailAddress = new Zend_Validate_EmailAddress();
        $emailAddress->setMessage(self::ERROR_EMAIL_INVALID);

        $email->setRequired(true)
                ->addFilter('StringTrim')
                ->addValidator($notEmpty, true)
                ->addValidator($emailAddress, true);

        $this->addElement($email);
    }

}
