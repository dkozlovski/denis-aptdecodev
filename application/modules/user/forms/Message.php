<?php

class User_Form_Message extends Ikantam_Form
{

	const ERROR_SUBJECT_LENGTH = 'Subject length should be less than 255 characters.';
	const ERROR_SUBJECT_VALUE = 'Subject required.';
	const ERROR_TEXT_VALUE = 'Provide text.';
	const ERROR_RECIPIENT_VALUE = 'This user does not exist';

	public function init()
	{
		$subject = new Zend_Form_Element_Text('subject');

		$length = new Zend_Validate_StringLength(array('min' => 1, 'max' => 255));
		$length->setMessage(self::ERROR_SUBJECT_LENGTH);

		$empty = new Zend_Validate_NotEmpty();
		$empty->setMessage(self::ERROR_SUBJECT_VALUE);

		$subject->setRequired(true)
				->addFilters(array('StripTags', 'StringTrim'))
				->addValidator($empty, true)
				->addValidator($length, true);

		$this->addElement($subject);


		$text = new Zend_Form_Element_Text('text');

		$text->setRequired(true)
				->addFilters(array('StringTrim'))
				->addValidator('NotEmpty')
				->addErrorMessage(self::ERROR_TEXT_VALUE);

		$this->addElement($text);
	}

}