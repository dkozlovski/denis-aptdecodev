<?php

class User_Form_PhoneNumber extends Ikantam_Form
{
    
    const  INVALID_NUMBER = 'Please enter a  valid  number';
    const  INVALID_CODE   = 'Please enter a  valid  code';
    const  CODE_EPMTY     = 'Code is required';
    const  NUMBER_EMPTY   = 'Number is required';
    
    public function init()
	{
	   
       $vNumberDigits = new Zend_Validate_Digits();
       $vNumberDigits->setMessage(self::INVALID_NUMBER);
       
       $vCodeDigits = new Zend_Validate_Digits();
       $vCodeDigits->setMessage(self::INVALID_CODE);
       
       $vEmptyCode = new Zend_Validate_NotEmpty();
       $vEmptyCode->setMessage(self::CODE_EPMTY);
       
       $vEmptyNumber = new Zend_Validate_NotEmpty();
       $vEmptyNumber->setMessage(self::NUMBER_EMPTY);
        
        $number = new Zend_Form_Element_Text('number');
        $number->addValidator($vEmptyNumber,  true)
               ->addValidator($vNumberDigits, true);
        
        $code   = new Zend_Form_Element_Text('code');
        $code->addValidator($vEmptyCode, true)
             ->addValidator($vCodeDigits, true);
             
        
        $this->addElements(array($number, $code));	
	}    
    
}