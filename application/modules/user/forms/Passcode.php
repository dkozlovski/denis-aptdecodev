<?php

class User_Form_Passcode extends Ikantam_Form
{

	const ERROR_EMAIL_IS_EMPTY = 'Email required.';
	const ERROR_EMAIL_IS_INVALID = 'Please enter a valid email address.';
	const ERROR_PASSWORD_IS_EMPTY = 'Passcode required.';
    const ERROR_PASSWORD_INVALID = 'Passcode is invalid.';

	public function init()
	{
		$email = new Zend_Form_Element_Text('email');

		$notEmpty = new Zend_Validate_NotEmpty();
		$notEmpty->setMessage(self::ERROR_EMAIL_IS_EMPTY);

		$emailAddress = new Zend_Validate_EmailAddress();
		$emailAddress->setMessage(self::ERROR_EMAIL_IS_INVALID);

		$email->setRequired(true)
				->addFilter('StringTrim')
				->addValidator($notEmpty, true)
				->addValidator($emailAddress, true);

		$password = new Zend_Form_Element_Select('passcode');
        $collection = new Application_Model_Passcode_Collection();
        
        $notEm = new Zend_Validate_NotEmpty();
        $notEm->setMessage(self::ERROR_PASSWORD_IS_EMPTY);
        
        $inArray = new Zend_Validate_InArray($collection->getOptions());
        $inArray->setMessage(self::ERROR_PASSWORD_INVALID);

		$password->setRequired(true)
				->addFilter('StringTrim')
				->addValidator($notEm, true)
                ->addValidator($inArray, true);

		$this->addElements(array($email, $password));
	}

}