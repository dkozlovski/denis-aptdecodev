<?php

class User_Form_Email extends Ikantam_Form
{   

	const ERROR_EMAIL_INVALID = 'Please enter a valid email address.';
        
    public function init()
	{
	   
        $primary = new Zend_Form_Element_Radio('primary');                   
        $email = new Zend_Form_Element_Text('email');
       
		$vEmailAddress = new Zend_Validate_EmailAddress();
		$vEmailAddress->setMessage(self::ERROR_EMAIL_INVALID);  
        
        $email->addFilter('StringTrim')
              ->addValidator($vEmailAddress, true); 
              
        $primary->setRegisterInArrayValidator(false); 
        $primary->addValidator('Digits', true)
                ->removeValidator('InArray');
              
              
        $this->addElements(array($primary, $email)); 
		
	}    
    
}