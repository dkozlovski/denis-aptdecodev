<?php

class User_Form_Birthday extends Ikantam_Form
{
    const ERROR_EMPTY = "Field 'birthday' is required and cannot be empty.";
    const ERROR_DATE_INVALID = "Please enter a valid date.";
    
    public function init()
	{  
	   $vDate = new Ikantam_Validate_Date('mdy');
       $vDate->setMessage(self::ERROR_DATE_INVALID);
       
       $vEmpty = new Zend_Validate_NotEmpty();
       $vEmpty->setMessage(self::ERROR_EMPTY);
       
       $birthday = new Zend_Form_Element_Text('date');
       $birthday->setRequired()
                ->addValidator($vEmpty, true)
                ->addValidator($vDate, true);
       
       $this->addElement($birthday);   
	
	} 
    

    	/**
         * Retrieves Unix timestamp from provided date string
         * @return integer 
         */
    public function getTime ()
    {
        
        $date = $this->getElement('date')
                     ->getValidator('Ikantam_Validate_Date')
                     ->getParsedDate();
        if(!empty($date))
        {
        return mktime(0,0,0, $date['month'], $date['day'], $date['year']);
        }
        return false;
    }  
    
}