<?php
class User_Form_Order_Availability extends Ikantam_Form {
    
    public function init ()
    {
        $buildingType = new Zend_Form_Element('building_type');
        $buildingType->setRequired(true)
                     ->addValidator('InArray', false, array(array('elevator', 'walkup')));
                     
        $flightsOfStairs = new Zend_Form_Element('flights_of_stairs');
        $flightsOfStairs->setRequired(true)
                        ->addValidator('Between', false, array('min' => 1, 'max' => 10));

        $phone = new Zend_Form_Element('phone_number');
        $phone->setRequired(true)
              ->addFilter('StringTrim')
              ->addValidator('Regex', false, array('/^\(?\+?\d\d\d\)?\s?\d\d\d\s?\d\d\d\d$/u'))
              ->addErrorMessage('Please enter correct phone number.');

        $this->addElements(array($buildingType, $flightsOfStairs, $phone));
                                     
    }
    public function isValid($values)
    {
        if (isset($values['building_type']) && $values['building_type'] == 'elevator') {
            $flightsOfStairs = $this->getElement('flights_of_stairs');
            $flightsOfStairs->setRequired(false)
                ->removeValidator('Between');
        }

        return parent::isValid($values);
    }
    
}
