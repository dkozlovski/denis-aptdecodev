<?php

class User_Form_Signup extends Ikantam_Form
{

	const ERROR_PASSWORD_EMPTY = 'Password is required.';
	const ERROR_FULLNAME_EMPTY = 'Full name is required.';
	const ERROR_FULLNAME_LENGTH = 'Full name should be under 100 characters.';

	public function init()
	{

		$fullName = new Zend_Form_Element_Text('full_name');

		$notEmpty = new Zend_Validate_NotEmpty();
		$notEmpty->setMessage(self::ERROR_FULLNAME_EMPTY);

		$stringLength = new Zend_Validate_StringLength(array('max' => 100));
		$stringLength->setMessage(self::ERROR_FULLNAME_LENGTH);

		$fullName->setRequired(true)
				->addFilters(array('StripTags', 'StringTrim'))
				->addValidator($notEmpty, true)
				->addValidator($stringLength, true);

		$password = new Zend_Form_Element_Password('password');

		$password->setRequired(true)
				->addFilter('StringTrim')
				->addValidator('NotEmpty')
				->addErrorMessage(self::ERROR_PASSWORD_EMPTY);


		$email = new Zend_Form_Element_Hidden('email');

		$email->addFilters(array('StripTags', 'StringTrim'))
				->setRequired(true)
				->addValidator('notEmpty', true)
				->addValidator('EmailAddress', true)
				->addValidator('StringLength', array('max' => 100))
				->addErrorMessage('Please enter a valid email');



		$invitationCode = new Zend_Form_Element_Hidden('code');

		$invitationCode->addFilters(array('Alnum'))
				->setRequired(true)
				->addValidator('notEmpty', true)
				->addValidator('StringLength', false, array('max' => 16, 'min' => 16))
				->addErrorMessage('Invitation code is invalid');
        
        
        $terms = new Zend_Form_Element_Text('terms');
        
        $inArray = new Zend_Validate_InArray(array('agreed'));

		$terms->addFilters(array('StringTrim'))
				->setRequired(true)
				->addValidator('notEmpty', true)
				->addValidator($inArray, true)
				->addErrorMessage('You should accept Terms of Use an Privacy Policy');

		$this->addElements(array($fullName, $email, $password, $invitationCode, $terms));
	}

}
