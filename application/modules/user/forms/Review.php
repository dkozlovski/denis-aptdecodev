<?php

class User_Form_Review extends Ikantam_Form
{

	const ERROR_RATING_IS_INVALID = 'Please select rating.';
	const ERROR_TEXT_IS_EMPTY = 'Text is required.';

	public function init()
	{
		$email = new Zend_Form_Element_Select('rating');

		$email->setRequired(false)
				->addFilter('StringTrim')
				->addValidator('InArray', true, array(array(1,2,3,4,5)))
				->addErrorMessage(self::ERROR_RATING_IS_INVALID);

		$password = new Zend_Form_Element_Textarea('text');

		$password->setRequired(true)
				->addFilters(array('StringTrim', 'StripTags'))
				->addValidator('NotEmpty')
				->addErrorMessage(self::ERROR_TEXT_IS_EMPTY);

		$this->addElements(array($email, $password));
	}

}