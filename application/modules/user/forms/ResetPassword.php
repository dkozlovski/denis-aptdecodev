<?php

class User_Form_ResetPassword extends Ikantam_Form
{
	const ERROR_PASSWORD_EMPTY = 'Password is required.';
	const ERROR_CONFIRMATION_EMPTY = 'Confirmation is required.';
	const ERROR_CONFIRMATION_WRONG = 'Passwords should match.';
	
	public function init()
	{
		$password = new Zend_Form_Element_Password('password');

		
		$notEmpty = new Zend_Validate_NotEmpty();
		$notEmpty->setMessage(self::ERROR_PASSWORD_EMPTY);

		$password->addFilter('StringTrim')
				->setRequired(true)
				->addValidator($notEmpty, true);

		
		$confirmation = new Zend_Form_Element_Password('confirmation');

		$notEmpty2 = new Zend_Validate_NotEmpty();
		$notEmpty2->setMessage(self::ERROR_CONFIRMATION_EMPTY);
		
		$identical = new Zend_Validate_Identical('password');
		$identical->setMessage(self::ERROR_CONFIRMATION_WRONG);
		
		$confirmation->addFilter('StringTrim')
				->setRequired(true)
				->addValidator($notEmpty2, true)
				->addValidator($identical, true);
		
		$code = new Zend_Form_Element_Hidden('code');

		$code->addFilters(array('Alnum'))
				->setRequired(true)
				->addValidator('notEmpty', true)
				->addValidator('StringLength', false, array('max' => 32, 'min' => 32))
				->addErrorMessage('Code is invalid');

		$this->addElements(array($password, $confirmation, $code));
	}

}
