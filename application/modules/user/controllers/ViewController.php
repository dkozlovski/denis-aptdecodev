<?php

class User_ViewController extends Ikantam_Controller_Front
{

    public function init()
    {
        /* if (!$this->getSession()->isLoggedIn()) {
          $this->_redirect('user/login/index');
          } */
    }

    public function indexAction()
    {
        $this->_forward('items');
    }

    public function itemsAction()
    {
        $id = $this->getRequest()->getParam('id');

        $user = new Application_Model_User($id);

        if (!$user->getId()) {
            if ($this->getSession()->isLoggedIn()) {
                $user = $this->getSession()->getUser();
            } else {
                throw new Zend_Controller_Action_Exception('This user does not exist', 404);
            }
        }

        $products = new Product_Model_Product2_Collection();

        $categoryId = $this->getRequest()->getParam('category');

        $category = new Category_Model_Category($categoryId);
        $products->getSalableByUser($user->getId(), $category->getId());


        $categories = new Category_Model_Category_Collection();

        $this->view->category   = $category;
        $this->view->categories = $categories->getByUser($user->getId());

        $this->view->products   = $products;
        $this->view->user       = $user;
        $this->view->loggedUser = $this->getSession()->getUser();

        $review              = new User_Model_Review_Collection();
        $this->view->reviews = $review->getByUserId($user->getId());
        $this->view->headTitle('Items');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

    public function itemsAjaxAction()
    {
        $response = array('success' => false);

        $id = $this->getRequest()->getParam('id');

        $user = new Application_Model_User($id);

        if ($user->getId()) {
            $products = new Product_Model_Product_Collection();

            $categoryId = $this->getRequest()->getParam('category');

            $category = new Category_Model_Category($categoryId);

            if ($category->getId()) {
                $products->getByUserAndCategory($user->getId(), $category->getId());
            } else {
                $products->getByUser($user->getId());
            }

            $this->view->products = $products;

            $response['template'] = $this->view->render('view/product_list.phtml');
            $response['success']  = true;
        }

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(json_encode($response));
    }

    public function reviewsAction()
    {
        $id = $this->getRequest()->getParam('id');

        $user = new Application_Model_User($id);

        if (!$user->getId() && !$this->getSession()->isLoggedIn()) {
            throw new Zend_Controller_Action_Exception('This user does not exist', 404);
        } elseif (!$user->getId() && $this->getSession()->isLoggedIn()) {
            $user = $this->getSession()->getUser();
        }

        $review                     = new User_Model_Review_Collection();
        $this->view->reviews        = $review->getByUserId($id);
        $this->view->user           = $user;
        $this->view->session        = $this->getSession();
        $this->view->loggedUser     = $this->getSession()->getUser();
        $this->view->reviewRequired = (bool) $this->getRequest()->getParam('q');

        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/jquery.rating.css'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery.rating.js'));
        $this->view->headTitle('Reviews');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

    public function reviewPostAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $userId = $this->getRequest()->getParam('id');

        $user = new Application_Model_User($userId);

        if (!$user->getId()) {
            $this->getSession()->addMessage('error', 'This user does not exist');
            $this->_helper->redirector('reviews', 'view', 'user', array('id' => $userId));
        }

        $data = $this->getRequest()->getPost();

        try {
            $form = new User_Form_Review();

            if ($form->isValid($data)) {
                $review = new User_Model_Review();

                $review->addData($form->getValues())
                        ->setParentId(null)
                        ->setCreatedAt(time())
                        ->setAuthorId($this->getSession()->getUserId())
                        ->setUserId($user->getId())
                        ->save();

                $user->recalculateRating();
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to create review. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->_helper->redirector('reviews', 'view', 'user', array('id' => $userId));
    }

}