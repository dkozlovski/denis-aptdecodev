<?php

class User_PayoutController extends Ikantam_Controller_Front
{

    const PAYOUT_INFO_SAVED = 'Your payout preference has been saved.';
    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }
        
         Httpful\Bootstrap::init();
        RESTful\Bootstrap::init();
        Balanced\Bootstrap::init();
        Balanced\Settings::init();

       /* $merchantUri = $this->getRequest()->getParam('merchant_uri');
        $emailAddress = $this->getRequest()->getParam('email_address');

        if ($merchantUri && $emailAddress) {
            $data = array(
                'merchant_uri' => $merchantUri,
                'email_address' => $emailAddress);

            $marketplace = new Ikantam_Balanced_Marketplace();
            
            try {
                $merchant = $marketplace->createMerchant($data);
                

                $basUri = $merchant->getBankAccountsUri();
                $bankAcc = $marketplace->getOneBankAccount($basUri);
                $user = $this->getSession()->getUser();

                $merc = $user->getMerchant();

                $merc->setUserId($user->getId())->setAccountUri($merchant->getUri());

                if (isset($bankAcc['uri'])) {
                    $merc->setBankAccountUri($bankAcc['uri']);
                }

                if (isset($bankAcc['name'])) {
                    $merc->setFullName($bankAcc['name']);
                }

                if (isset($bankAcc['last_four'])) {
                    $merc->setAccountNumber($bankAcc['last_four']);
                }

                if (isset($bankAcc['bank_code'])) {
                    $merc->setRoutingNumber($bankAcc['bank_code']);
                }

                if (isset($bankAcc['type'])) {
                    $merc->setAccountType($bankAcc['type']);
                }

                $merc->save();
            } catch (Ikantam_Exception_DuplicateEmail $e) {
                $this->getSession()->addMessage('error', $e->getMessage());
            }
        }*/
    }

    protected function prepareData($user)
    {
        $data = array();

        if (!$user->getMerchant()->getAccountUri()) {
            $data['full_name'] = $user->getFullName();
           // $data['date_of_birth'] = $user->getDateOfBirth();

            //if ($user->getPrimaryAddress()->getId()) {
              //  $data['postal_code'] = $user->getPrimaryAddress()->getPostcode();
              //  $data['street_address'] = $user->getPrimaryAddress()->getAddressLine1();
            //}

           // if ($user->getPrimaryPhone()->getId()) {
           //     $data['phone_number'] = $user->getPrimaryPhone()->getPhone();
          //  }
        } else {
            $data['full_name'] = $user->getMerchant()->getFullName();
         //   $data['date_of_birth'] = date('Y-m-d', $user->getMerchant()->getDateOfBirth());
          //  $data['postal_code'] = $user->getMerchant()->getPostalCode();
          //  $data['street_address'] = $user->getMerchant()->getStreetAddress();
          //  $data['phone_number'] = $user->getMerchant()->getPhoneNumber();
        }
        return $data;
    }

    public function indexAction()
    {
        $this->view->errorMessages = (array) $this->getSession()->getMessages('error');
        $this->view->successMessages = (array) $this->getSession()->getMessages('succesful');
        
        $messages = (array) $this->getSession()->getMessages('success');
        
        if (count($messages))
            $this->getSession()->addMessage('success', current($messages));

        $productsIds = (array) $this->getSession()->getMessages('product_id');
        if (count($productsIds))
            $this->getSession()->addMessage('product_id', current($productsIds));


        $user = $this->getSession()->getUser();

        if (!$user->getMerchant()->getBankAccountUri()) {
            $form = new User_Form_Merchant();
            $merchantData = $this->getSession()->getFlashData('_merchantData');
            
            if(is_array($merchantData)) {
                $form->populate($merchantData);    
            }
            
            $request = $this->getRequest();

            if ($request->isPost()) {

                $_POST['date_of_birth'] = $request->getPost('dob_year')
                        . '-' . $request->getPost('dob_month')
                        . '-' . $request->getPost('dob_day');

                if ($form->isValid($request->getPost())) {
                    try {
                        $marketplace = new Ikantam_Balanced_Marketplace();

                        $baUri = $form->getValue('ba_uri');

                        if (!$user->getMerchant()->getAccountUri()) {

                            try {
                                $data = array(
                                    'name' => $form->getValue('full_name')
                                );
                                
                                
                                $merchant = new Balanced\Customer($data);
                                $merchant->addBankAccount($baUri);

                            } catch (Ikantam_Exception_Redirect $exc) {
                                $redirectUrl = $exc->getMessage();

                                $dd = '?' . http_build_query($data);


                                $dd .= '&redirect_uri=' . urlencode(Ikantam_Url::getUrl('user/payout/index'));

                                $this->_redirect($redirectUrl . $dd);
                            } catch (Exception $e) {
                                var_dump($e->getMessage());
                                die();
                            }

                            if ($merchant->uri) {
                                $user->getMerchant()
                                        ->setUserId($user->getId())
                                        ->setAccountUri($merchant->uri)
                                        ->setBankAccountUri($baUri)
                                        ->setFullName($form->getValue('full_name'))
                                        ->setAccountType($form->getValue('account_type'))
                                        ->setAccountNumber(substr($form->getValue('account_number'), -4))
                                        ->setRoutingNumber($form->getValue('routing_number'))
                                        ->save();
                            }
                        } else {
                            $marketplace->addBankAccount($user->getMerchant()->getAccountUri(), $baUri);
                            $user->getMerchant()->setBankAccountUri($baUri)->save();
                        }


                        if ($this->getRequest()->getParam('redirect') == 'product') {
                            $this->_helper->redirector('new', 'add', 'product');
                        }
                        
                        if ($this->getRequest()->getParam('redirect') == 'success') {
                            $productsIds = (array) $this->getSession()->getMessages('product_id');
                            if (count($productsIds))
                                $pId = current($productsIds);
                            $product = new Product_Model_Product($pId);
                            if ($product->getId() && $product->getUserId() == $this->getSession()->getUserId()) {
                                $product->setIsPublished(1)->save();
                            }

                            $this->_helper->redirector('success', 'add', 'product');
                        }
                        
                        $this->getSession()->addMessage('succesful', self::PAYOUT_INFO_SAVED);

                        $this->_helper->redirector('index', 'payout', 'user');
                        echo 'success';
                        die();
                    } catch (Exception $e) {
                        //var_dump($e);
                        die();
                    }
                } else {
                    $elements = $form->getElements();
                    //var_dump($elements['account_number']);
                    $elements['account_number']->setValue('');
                    //var_dump($form->getValues());
                    //var_dump($form->getMessages());
                    //die();
                }
            } else {
                $form->populate($this->prepareData($user));
            }
            $this->view->form = $form;
        }
        $this->view->user = $this->getSession()->getUser();

        if ($this->getRequest()->getParam('redirect') == 'product') {
            $this->view->required = 'product';
        }
        if ($this->getRequest()->getParam('redirect') == 'success') {
            $this->view->required = 'success';
        }

        $this->view->headTitle('Payout information');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }
    
    public function editMerchantAction ()
    {
       $merchant =  $this->getSession()
                         ->getUser()
                         ->getMerchant();
                         
        $stored = $merchant->getData();
        unset($stored['account_number']);
        $this->getSession()->setFlashData('_merchantData', $stored);
        
        $merchant->setData(array())
                 ->setId($stored['id'])
                 ->setUserId($stored['user_id'])
                 ->save();
                 
        $this->redirect('user/payout/index');                                  
    }

}