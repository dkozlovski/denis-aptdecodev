<?php

class User_SettingsController extends Ikantam_Controller_Front
{

    /**
     * Save settings from popup window (first product submission) 
     * 
     * @return void
     */
    public function firstSubmissionAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        $user = $this->getSession()->getUser();

        if ($answers = (array) $this->getRequest()->getParam('quizform')) {

            $form = new Product_Form_QuizSubform;
            if ($form->isValid($answers)) {

                if (isset($answers['reason'])) {
                    $user->personalSettings('selling_reason', $answers['reason']);
                } else {
                    $user->personalSettings('selling_reason', 0);
                }

                if (isset($answers['is_verification_available'])) {
                    $user->personalSettings('product_verification_available', (int) (bool) $answers['is_verification_available']);
                } else {
                    $user->personalSettings('product_verification_available', 0);
                }

                if (isset($answers['is_concierge_available'])) {
                    $user->personalSettings('product_concierge_available', (int) (bool) $answers['is_concierge_available']);
                } else {
                    $user->personalSettings('product_concierge_available', 0);
                }

                if (isset($answers['how_you_heard_select'])) {
                    $how = '';
                    $heards = Checkout_Form_HowYouHeard::getHeards();
                    if (isset($heards[$answers['how_you_heard_select']])) {
                        $how = $heards[$answers['how_you_heard_select']];

                        if ($how == 'Other' && !empty($answers['how_you_heard_text'])) {
                            $how = $answers['how_you_heard_text'];
                        }
                    }
                    $user->personalSettings('how_you_heard', $how);
                }
            }
        } else {
            $user->personalSettings('selling_reason', -1);
            $user->personalSettings('product_verification_available', -1);
            $user->personalSettings('product_concierge_available', -1);
        }
    }

}
