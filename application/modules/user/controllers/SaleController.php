<?php

class User_SaleController extends Ikantam_Controller_Front
{

    protected $_filters = array();
    protected $_user    = null;
    protected $_searchValue;
    protected $_filterParams;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {

            $post = (object) $this->getRequest()->getParams();

            if ($post->action == 'extend-publication-period' && isset($post->id)) {
                $this->getSession()->setRefererRedirectUrl(Ikantam_Url::getUrl('user/sale/extend-publication-period', array('id' => $post->id)));
            }

            $this->redirect('user/login/index');
        }
        $this->_filters[]   = new Zend_Filter_StringTrim();
        $this->_filters[]   = new Zend_Filter_StringToLower();
        $this->_searchValue = $this->getRequest()->getParam('search', false);
        $this->filter($this->_searchValue);
        $this->_user        = $this->getSession()->getUser();
        $this->view->session = $this->getSession();
    }

    public function listAction()
    {
        $products = $this->_user->getProductsForSale(
                $this->prepareFilters(), $this->getRequest()->getParam('page', 1), 10, $this->_searchValue
        );

        $this->view->paginator = $products->getPaginator()->setPageRange(5);
        $this->view->products  = $products->getItems();

        $categories             = new Category_Model_Category_Collection();
        $categories->getByUserSaleList($this->_user->getId());
        $this->view->categories = $categories->getItems();

        $this->view->userObj = new Application_Model_User();
        $this->view->status  = ($this->_filterParams['status']) ? $this->_filterParams['status'] : 'all';
        $this->view->date    = ($this->_filterParams['date']) ? $this->_filterParams['date'] : 'all';
        $from                = ($this->_filterParams['from']) ? $this->_filterParams['from'] : false;
        $till                = ($this->_filterParams['till']) ? $this->_filterParams['till'] : false;
        $this->view->from    = str_replace('-', '/', $from);
        $this->view->till    = str_replace('-', '/', $till);
        if ($from || $till) {
            $this->view->date = 'between';
        }
        $category = new Category_Model_Category();

        $this->view->currentCategory = $category;
        if (!empty($this->_filterParams['category'])) {
            $category->getById($this->_filterParams['category']);
        }

        // https://app.asana.com/0/6707107011362/15715576531152
        // For better user experience, we show transactions which need confirmation
        $this->view->transactionsNeedConfirmation = \Order_Model_Item_Collection::flexFilter()
            ->user_id('=', $this->getSession()->getUserId())
            ->shipping_method_id('=', \Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY)
            ->status('=', \Order_Model_Order::ORDER_STATUS_ON_HOLD )
            ->apply(5);


        // This action also uses as background for price reduction confirmation popup
        $this->checkDiscount();
        $this->view->currentCategory  = $category;
        $this->view->categoryObj      = new Category_Model_Category();
        $this->view->paginationParams = $this->getPaginationParams();
        $this->view->activeMenu       = 'listings';
        $this->view->headTitle('All postings');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

    public function suggestionsAction()// AJAX action
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->getRequest()->isPost()) {
            return;
        }

        $result = array();

        if (is_string($this->_searchValue) && strlen($this->_searchValue) > 0) {
            $productsByTitle = $this->_user->getProductsForSale(
                            array('title'          => $this->_searchValue, 'groupByProduct' => true), 1, -1)->getItems();

            foreach ($productsByTitle as $product) {
                $result['products'][] = array(
                    'id'      => $product->getId(),
                    'title'   => $product->getTitle(),
                    'encoded' => urlencode($product->getTitle()),
                    'url'     => $product->getProductUrl()
                );
            }
        }

        echo Zend_Json::encode($result);
    }

    protected function filter(&$val)
    {
        if (!$val)
            return false;
        foreach ($this->_filters as $filter) {
            if ($filter instanceof Zend_Filter_Interface) {
                $filter->filter($val);
            }
        }
    }

    protected function prepareRequest()
    {
        $str = "Request Parameters:\n";
        foreach ($this->getRequest()->getParams() as $param => $value) {
            $str .= $param . ': ' . $value . "\n";
        }
        return $str;
    }

    protected function prepareUserInfo()
    {
        $str = "User info:\n";
        $str .= "IP: " . $this->getRequest()->getClientIp() . "\n";
        $str .= "User agent: " . $_SERVER['HTTP_USER_AGENT'] . "\n\n";
        $str .= '*************************************************************' . "\n";
        return $str;
    }

    protected function _logUpdate()
    {
        $fileName = APPLICATION_PATH . '/log/update-product.log';

        $data = $this->prepareRequest() . $this->prepareUserInfo() . "\n";

        file_put_contents($fileName, $data, FILE_APPEND);
    }

    public function updateAction()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (APPLICATION_ENV === 'live') {
            $this->_logUpdate();
        }

        $id       = $this->getRequest()->getParam('id', false);
        $act      = $this->getRequest()->getParam('updAction', false);
        $result   = false;
        $extended = '';
        $message  = '';
        if ($id && $act) {
            $product = new Product_Model_Product((int) $id);

            if ($this->_user->getId() === $product->getUserId()) {
                $result = 1;
                switch ($act) {
                    case 'publish':
                        if ($product->getIsApproved() && $product->isExpired()) {
                            $extended = $product->extendExpiration();
                        }

                        if ($product->canBePublished()/* && $this->getSession()->getUser()->getMerchant()->getBankAccountUri() */) {
                            $product->setIsPublished(1)->save();
                            $result = true;
                        } else {
                            $result = 0;
                        }

                        break;

                    case 'unpublish':
                        $product->setIsPublished(0)
                                ->save();
                        $result = true;
                        $message = $product->getTitle() . ' has been unpublished';
                        break;

                    case 'resubmit':
                        $product->setIsApproved(0)
                            ->save();
                        $result = true;

                        break;

                    case 'delete':
                        $product->delete();
                        $result = true;
                        break;
                }
            }
            $date = $product->getExpiryDateString();
        }

        if (!$this->isAjax()) {
            if ($message) {
                //This will show popup message once redirect is occurred
                $this->getSession()->setFlashData(
                    'modal_message',
                    array(
                        '',//header
                        $message,
                        '',
                        7000 // will be closed after milliseconds
                    )
                );
            }
            $this->redirect('user/sale/list');
        }

        echo Zend_Json::encode(array('result'   => $result, 'extended' => $extended, 'date'     => $date, 'elem'     => 'date' . $id));
    }

    public function extendPublicationPeriodAction()
    {
        $id            = $this->getRequest()->getParam('id', false);
        $product       = new Product_Model_Product($id);
        $days = $this->getRequest()->getParam('days');

        if (!in_array($days, array(7, 14, 30))) {
            $days = null; //default
        }

        $continue            = (bool) $id && $this->getSession()->getUser()->getId() === $product->getUserId();
        $this->view->product = $product;

        if ($continue) {
            $continue = $product->extendExpiration($days);
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            if (!$continue) {
                exit(Zend_Json::encode(array('success' => false)));
            }

            exit(Zend_Json::encode(array('date'    => $product->getExpiryDateString(), 'success' => true)));
        } else {
            if (!$continue) {
                $this->redirect('user/login');
            }
           //This will show popup message once redirect is occurred
            $this->getSession()->setFlashData(
                'modal_message',
                array(
                    '',//header
                    'Publication period has been extended to ' . $product->getExpiryDateString(),
                    '',
                    7000 // will be closed after milliseconds
                )
            );

            $this->redirect('user/sale/list');
        }
    }

//------------------------  
    public function loadfrompageAction() //AJAX
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $page = $this->getRequest()->getParam('page', false);
        if (!$page)
            return;

        $products = $this->_user->getProductsForSale(
                $this->prepareFilters(), $page
        );

        $paginator = $products->getPaginator();

        $category = new Category_Model_Category();
        $currency = new Zend_Currency(array('display' => Zend_Currency::USE_SYMBOL), 'en_US');

        $result = array();
        foreach ($products->getItems() as $product) {

            $result['products'][] = array(
                'id'          => $product->getId(),
                'title'       => $product->getTitle(),
                'price'       => $currency->toCurrency($product->getPrice()),
                'category'    => $category->getById($product->getCategoryId())->getTitle(),
                'linkUrl'     => $product->getProductUrl(),
                'imgUrl'      => $product->getMainImageUrl(),
                'editUrl'     => $product->getEditUrl(),
                'shipping'    => $product->getShippingLabel(),
                'isPublished' => (bool) ($product->getIsPublished()),
                'isSold'      => (bool) $product->getIsSold(),
                'isReceived'  => (is_null($product->getIsReceived())) ? 'notSold' : (bool) $product->getIsReceived(),
            );
        }

        $result['totalPageCount'] = ($paginator) ? $paginator->count() : 0;

        $result['paginationControlHTML'] = $this->view->paginationControl(
                $paginator, null, 'loadfrompage_pagination_controll.phtml', array('params' => $this->getPaginationParams(), 'path'   => 'user/sale/list'));

        echo Zend_Json::encode($result);
    }

    /**
     * Retrieves field value from objects and pass it to result array.         
     * @param array $objArray - array of Ikantam_Objects
     * @param string $method - method name to retrieve value
     * @param string $keyMethod - method name to retrieve key
     * @return array
     */
    private function convertToSimpleArray($objArray, $method, $keyMethod)
    {
        $result = array();
        foreach ($objArray as $obj) {
            $result[$obj->$keyMethod()] = $obj->$method();
        }
        return $result;
    }

    private function getFilterParams()
    {
        $params = &$this->_filterParams;
        if (is_null($params)) {
            $rqst = $this->getRequest();

            $params['brand']    = $rqst->getParam('brand', false);
            $params['user']     = $rqst->getParam('user', false);
            $params['date']     = $rqst->getParam('date', false);
            $params['from']     = $rqst->getParam('from', false);
            $params['till']     = $rqst->getParam('till', false);
            $params['product']  = $rqst->getParam('product', false);
            $params['status']   = $rqst->getParam('status', false);
            $params['order']    = $rqst->getParam('order', false);
            $params['category'] = $rqst->getParam('category', false);

            if (!$params['from']) {
                $params['from'] = $rqst->getParam('cfrom', false);
            }

            if (!$params['till']) {
                $params['till'] = $rqst->getParam('ctill', false);
            }

            if (!$params['date']) {
                $params['date'] = $rqst->getParam('cdate', false); // cdate = 'all' by default. (current date)
            } else {
                $params['from'] = false;
                $params['till'] = false;
            }

            /* if(!$params['date'])
              {
              $params['date'] = $rqst->getParam('cdate', false);// cdate = 'all' by default. (current date)
              } else
              {
              if($params['date']!= 'all' && $params['date']!= 'between')
              {
              $params['from'] = false;
              $params['till'] = false;
              }

              } */
        }
        return $params;
    }

    private function prepareFilters()
    {
        $filter     = array();
        /* @var $collection Product_Model_Product_Collection */
        $collection = 'Product_Model_Product_Collection';
        $vDate      = new Ikantam_Validate_Date('mdy');
        foreach (array_filter($this->getFilterParams()) as $key => $value) {
            if ($key == 'status') {
                if ($value == 'received') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_RECEIVED;
                } elseif ($value == 'in-progress') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_IN_DELIVERY;
                } elseif ($value == 'published') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_PUBLISHED;
                } elseif ($value == 'not-published') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_NOT_PUBLISHED;
                } elseif ($value == 'sold') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_SOLD;
                } elseif ($value == 'not-sold') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_NOT_SOLD;
                } elseif ($value == 'need-to-approve') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_NEED_TO_APPROVE;
                } elseif ($value == 'rejected') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_REJECTED;
                }
            }//status

            if ($key == 'product') {
                $filter[$collection::FILTER_TYPE_ID] = $value;
            }

            if ($key == 'brand') {
                $filter[$collection::FILTER_TYPE_BRAND] = $value;
            }//brand

            if ($key == 'date') {
                if ($value == 'week') {
                    $filter[$collection::FILTER_TYPE_DATE] = $collection::FILTER_WEEK;
                }

                if ($value == 'month') {
                    $filter[$collection::FILTER_TYPE_DATE] = $collection::FILTER_MONTH;
                }

                if ($value == 'year') {
                    $filter[$collection::FILTER_TYPE_DATE] = $collection::FILTER_YEAR;
                }

            }//week-month-year

            if ($key == 'from') {
                if ($vDate->isValid($value)) {
                    $date                                              = $vDate->getParsedDate();
                    $filter[$collection::FILTER_TYPE_BETWEEN_DATES][0] = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
                }
            }

            if ($key == 'till') {
                if ($vDate->isValid($value)) {
                    $date                                              = $vDate->getParsedDate();
                    $filter[$collection::FILTER_TYPE_BETWEEN_DATES][1] = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
                }
            }//between dates

            if ($key == 'user') {
                $filter[$collection::FILTER_TYPE_USER] = $value;
            }
            //Order
            if ($key == 'order') {
                $filter[$collection::FILTER_TYPE_ORDER] = $value;
            }

            //Category
            if ($key == 'category') {
                $filter[$collection::FILTER_TYPE_CATEGORY] = $value;
            }

            $filter['dateByProduct'] = true; //Switches date filter to `products`.`created_at` column. 
        }
        return $filter;
    }

//prepare filters 

    private function getPaginationParams()
    {
        $params = array_filter($this->getFilterParams());
        if (isset($params['till'])) {
            $params['till'] = str_replace(' ', '', str_replace('/', '-', $params['till']));
        }
        if (isset($params['from'])) {
            $params['from'] = str_replace(' ', '', str_replace('/', '-', $params['from']));
        }
        return $params;
    }

    private function checkDiscount()
    {
        if ($this->getParam('act') === 'discount') {
            $product = new Product_Model_Product($this->getParam('id'));

            $isValidSignature = $product->getHashSignature() === $this->getParam('signature');

            if ($product->isBelongsToUser($this->getSession()->getUser()) && $isValidSignature) {
                $percent = $this->getParam('percent');
                if (!$product->getEAVAttributeValue('price_lowered_by_suggestion') && in_array($percent, array(15, 30))) {
                    Zend_Registry::set(
                        'modal-template-url',
                        Ikantam_Url::getUrl('front-helper/modal-html', array('template' => 'price-reduction-popup'))
                    );
                    $this->view->discountProduct = $product;
                    $this->view->discountPercent = $percent;
                }
            }
        }

    }

}
