<?php

class User_SignupController extends Ikantam_Controller_Front
{
    //From config | see init()
    protected $_appId ;
    protected $_appSecret ;
    //------------------------
    private $_signupForm;
    protected $_scope = 'email,user_birthday';
    
    public function init()
    {
        $this->setSignupForm(new User_Form_Signup());
        
        //$cfg = $this->getXmlConfig('additional_settings', 'facebook');
        $cfg = Zend_Registry::get('config')->facebook;
        $this->_appId = $cfg->appId;
        $this->_appSecret = $cfg->appSecret;     

        
    }

    protected function getInvitation()
    {
        return $this->getSession()->getUser()->getInvitation();
    }

    public function passcodeAction()
    {
        $form = new User_Form_Passcode();
        $messages = array('success' => array(), 'error' => array());

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            if ($form->isValid($data)) {

                $invitation = new Application_Model_Invitation();

                $invitation->setEmail($form->getValue('email'))
                        ->setIsUsed(0)
                        ->setCode($invitation->generateCode())
                        ->setCreatedAt(time())
                        ->save();

                $this->view->code = $invitation->getCode();
                $this->view->imageKey = Ikantam_Math_Rand::get62String(16);
                $output = $this->view->render('invite_letter.phtml');

                if ($invitation->getEmail()) {
                    $toSend = array(
                    'email' => $invitation->getEmail(),
                    'subject' => 'Welcome to AptDeco!',
                    'body' => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                $mail->send();

                $viewedEmail = new Application_Model_ViewedEmail();
                $viewedEmail->setEmail($invitation->getEmail())
                        ->setCode($this->view->imageKey)
                        ->save();

                $messages['success'][] = 'Invitation has been sent to your email.';
                }
                
            } else {
                $this->view->formMessages = $form->getFlatMessages();
                $this->view->formData = $form->getValues();
            }
        }
        $this->view->messages = $messages;
    }
    
    protected function getRedirectUrl()
    {
        return urlencode(Ikantam_Url::getUrl('user/login/facebook'));
    }

    public function indexAction()
    {
        $invitationCode = $this->getRequest()->getParam('code');

        if (!$this->getInvitation()->isValid($invitationCode)) {
            $this->_forward('passcode');
        }

        $this->view->invitation = $this->getInvitation();
        $this->view->session = $this->getSession();
        $this->view->appId = $this->_appId;
        $this->view->redirectUrl = $this->getRedirectUrl();
        $this->view->scope = $this->_scope;
    }

    public function postAction()
    {

        $signupData = $this->getRequest()->getPost();

        try {
            $form = $this->getSignupForm();

            if ($form->isValid($signupData)) {
                $this->getSession()->register($form->getValues());
                $this->_redirect($this->getSuccessRedirect());
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Ikantam_Exception_Signup $exception) {
            $this->getSession()->addMessage('error', $exception->getMessage());
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to sign up. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->getSession()->addFormData($signupData, array('full_name', 'terms'));
        $this->_redirect($this->getFailureRedirect());
    }

    protected function getSuccessRedirect()
    {
        return Ikantam_Url::getUrl('index/index');
    }

    protected function getFailureRedirect()
    {
        $code = $this->getRequest()->getPost('code'); //@TODO: sanitize code
        return Ikantam_Url::getUrl('user/signup/index', array('code' => $code));
    }

    protected function setSignupForm(Zend_Form $form)
    {
        $this->_signupForm = $form;

        return $this;
    }

    protected function getSignupForm()
    {
        return $this->_signupForm;
    }

}
