<?php

class User_PurchaseController extends Ikantam_Controller_Front
{

    protected $_filters = array();
    protected $_user    = null;
    protected $_searchValue;
    protected $_filterParams;

    public function init()
    {
        $this->_filters[]   = new Zend_Filter_StringTrim();
        $this->_filters[]   = new Zend_Filter_StringToLower();
        $this->_searchValue = $this->getRequest()->getParam('search', false);
        $this->filter($this->_searchValue);
        $this->_user        = $this->getSession()->getUser();
    }

    public function indexAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $this->_forward('history');
    }

    public function historyAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $products = $this->_user->getPurchaseHistory(
                $this->prepareFilters(), $this->getRequest()->getParam('page', 1), 10, $this->_searchValue
        );

        foreach ($products as $product) {
            if ($product->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_PICK_UP) {
                if ($product->getIsProductAvailable() !== '1') {
                    $product->setIsConfirmationAvailable(false);
                } else {
                    $product->setIsConfirmationAvailable(true);
                }
            } else {

                $availability     = false;
                if ($orderItemOptions = $product->getOrderItemOptions(false)) {

                    $orderItemOptions = unserialize($orderItemOptions);
                    $sellerDate       = (isset($orderItemOptions['seller_dates'])) ? $orderItemOptions['seller_dates'] : false;
                    if ($sellerDate) {
                        $now          = time();
                        $compareDate  = strtotime(substr($sellerDate, 0, 10));
                        $availability = $compareDate < $now;
                    }
                }
                $product->setIsConfirmationAvailable($availability);
            }
        }

        $this->view->paginator        = $products->getPaginator()->setPageRange(5);
        $this->view->products         = $products;
        $this->view->userObj          = new Application_Model_User();
        $this->view->status           = ($this->_filterParams['status']) ? $this->_filterParams['status'] : 'all';
        $this->view->date             = ($this->_filterParams['date']) ? $this->_filterParams['date'] : 'all';
        $this->view->from             = $this->_filterParams['from'];
        $this->view->till             = $this->_filterParams['till'];
        $this->view->paginationParams = $this->getPaginationParams();

        if ($this->view->from || $this->view->till) {
            $this->view->date = 'between';
        }
        $this->view->activeMenu = 'purchases';
        $this->view->headTitle('My Purchase History');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

    public function orderAction()
    {
        $orderId    = $this->getRequest()->getParam('id', 0);
        $accessCode = $this->getRequest()->getParam('code');

        $item = new Order_Model_Item($orderId);
        if (!$item->getId()) {
            throw new Exception('This product does not exist');
        }

        $product = $item->getProduct();

        $this->view->product = $product;
        $this->view->seller  = $product->getSeller();
        $this->view->orderId = $item->getOrder()->getId();

        $order = $item->getOrder();

        $this->view->shipping   = ($item->getShippingMethodId() == 'delivery') ? 'Delivery' : 'Pick up';
        $this->view->userObj    = new Application_Model_User($order->getUserId());
        $this->view->activeMenu = 'purchase';

        if (!$order->getUserId()) {//guest order
            if (!$item->getAccessCode() || !$accessCode || $accessCode != $item->getAccessCode()) {
                throw new Exception('This product does not exist');
            }
        } else {

            if (!$this->getSession()->isLoggedIn()) {
                $this->_redirect('user/login/index');
            }

            if ($order->getUserId() != $this->getSession()->getUserId()) {
                throw new Exception('This product does not exist');
            }
        }

        if ($item->getShippingMethodId() == 'delivery' && $item->getBuyerDates()) {
            $this->view->orderedDates = $this->orderDates($item->getBuyerDates());
        }

        $this->view->item         = $item;
        $this->view->order        = $order;
        $this->view->loggedInUser = $this->getSession()->getUser();
        $this->view->accessCode   = $accessCode;
    }

    public function confirmdeliveryAction()//AJAX action
    {
        $accessCode = $this->getRequest()->getParam('code');
        $response   = array('confirmed' => false);

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->getRequest()->isPost()) {
            return;
        }

        $itemId = $this->getRequest()->getParam('confirm', false);

        $item  = new Order_Model_Item($itemId);
        $order = $item->getOrder();

        if ($item->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
            //Confirmation not available till seller date
            $sellerDate = $item->getSellerDates();
            if (!$sellerDate || time() < strtotime(substr($sellerDate, 0, 10))) {
                exit(Zend_Json::encode(array('confirmed' => false)));
            }
        }

        if (!$order->getUserId()) {//guest order
            if (!$item->getAccessCode() || !$accessCode || $accessCode != $item->getAccessCode()) {
                exit(Zend_Json::encode(array('confirmed' => false)));
            }
        } else {
            if ($order->getUserId() != $this->getSession()->getUserId()) {
                exit(Zend_Json::encode(array('confirmed' => false)));
            }
        }

        if ($item->confirmDelivery()) {
            $response['confirmed'] = true;

            if ($item->getAccessCode() && $accessCode == $item->getAccessCode()) {
                $response['redirect'] = Ikantam_Url::getUrl('user/purchase/order', array('id' => $itemId, 'code' => $accessCode));
            } else {
                $response['redirect'] = Ikantam_Url::getUrl('user/view/reviews', array('id' => $item->getProduct()->getUserId(), 'q' => 1));
            }
        }

        echo Zend_Json::encode($response);
    }

    public function suggestionsAction()// AJAX action
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->getRequest()->isPost())
            return;
        $result = array();
        if (is_string($this->_searchValue) && strlen($this->_searchValue) > 0) {
            $productsByUserName = $this->_user->getPurchaseHistory(
                            array('user' => $this->_searchValue), 1, -1)->getItems();

            $productsByTitle = $this->_user->getPurchaseHistory(
                            array('title' => $this->_searchValue, 'groupByProduct' => true), 1, -1)->getItems();

            //$productsByTitle = $this->convertToSimpleArray($productsByTitle,'getTitle','getOrderId');
            $productsByUserName = array_unique($this->convertToSimpleArray($productsByUserName, 'getFullName', 'getUserId'));

            foreach ($productsByTitle as $product) {

                $result['products'][] = array('id'      => $product->getId(),
                    'title'   => $product->getTitle(),
                    'encoded' => urlencode($product->getTitle()),
                    'url'     => $product->getProductUrl());
            }


            foreach ($productsByUserName as $id => $name) {
                $result['users'][] = array('id' => $id, 'name' => $name);
            }
        }

        echo Zend_Json::encode($result);
    }

    protected function filter(&$val)
    {
        if (!$val)
            return false;
        foreach ($this->_filters as $filter) {
            if ($filter instanceof Zend_Filter_Interface) {
                $filter->filter($val);
            }
        }
    }

    /**
     * Retrieves field value from objects and pass it to result array.         
     * @param array $objArray - array of Ikantam_Objects
     * @param string $method - method name to retrieve value
     * @param string $keyMethod - method name to retrieve key
     * @return array
     */
    private function convertToSimpleArray($objArray, $method, $keyMethod)
    {
        $result = array();
        foreach ($objArray as $obj) {
            $result[$obj->$keyMethod()] = $obj->$method();
        }
        return $result;
    }

    private function getFilterParams()
    {
        $params = &$this->_filterParams;
        if (is_null($params)) {
            $rqst = $this->getRequest();

            $params['brand']   = $rqst->getParam('brand', false);
            $params['user']    = $rqst->getParam('user', false);
            $params['date']    = $rqst->getParam('date', false);
            $params['from']    = $rqst->getParam('from', false);
            $params['till']    = $rqst->getParam('till', false);
            $params['product'] = $rqst->getParam('product', false);
            $params['status']  = $rqst->getParam('status', false);
            $params['order']   = $rqst->getParam('order', false);
            $params['product'] = $rqst->getParam('product', false);

            if (!$params['from']) {
                $params['from'] = $rqst->getParam('cfrom', false);
            }

            if (!$params['till']) {
                $params['till'] = $rqst->getParam('ctill', false);
            }

            if (!$params['product']) {
                $params['product'] = $rqst->getParam('cproduct', false);
            }

            if (!$params['brand']) {
                $params['brand'] = $rqst->getParam('cbrand', false);
            }

            if (!$params['date']) {
                $params['date'] = $rqst->getParam('cdate', false); // cdate = 'all' by default. (current date)
            } else {
                $params['from'] = false;
                $params['till'] = false;
            }
        }
        return $params;
    }

    private function prepareFilters()
    {
        $filter     = array();
        $collection = 'Product_Model_Product_Collection';
        $vDate      = new Ikantam_Validate_Date('mdy');
        foreach (array_filter($this->getFilterParams()) as $key => $value) {

            if ($key == 'product') {
                $filter[$collection::FILTER_TYPE_TITLE] = $value;
            }


            if ($key == 'status') {
                if ($value == 'received')
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_RECEIVED;
                if ($value == 'in-progress')
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_IN_DELIVERY;
            }//status

            if ($key == 'brand') {
                $filter[$collection::FILTER_TYPE_BRAND] = $value;
            }//brand

            if ($key == 'date') {
                if ($value == 'month')
                    $filter[$collection::FILTER_TYPE_DATE] = $collection::FILTER_MONTH;
                if ($value == 'year')
                    $filter[$collection::FILTER_TYPE_DATE] = $collection::FILTER_YEAR;
            }//month-year            

            if ($key == 'from') {
                if ($vDate->isValid($value)) {
                    $date                                              = $vDate->getParsedDate();
                    $filter[$collection::FILTER_TYPE_BETWEEN_DATES][0] = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
                }
            }

            if ($key == 'till') {
                if ($vDate->isValid($value)) {
                    $date                                              = $vDate->getParsedDate();
                    $filter[$collection::FILTER_TYPE_BETWEEN_DATES][1] = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
                }
            }//between dates

            if ($key == 'user') {
                $filter[$collection::FILTER_TYPE_USER] = $value;
            }

            if ($key == 'order') {
                $filter[$collection::FILTER_TYPE_ORDER] = $value;
            }
        }
        return $filter;
    }

//prepare filters 

    private function getPaginationParams()
    {
        $params = array_filter($this->getFilterParams());
        if (isset($params['till'])) {
            $params['till'] = str_replace(' ', '', str_replace('/', '-', $params['till']));
        }
        if (isset($params['from'])) {
            $params['from'] = str_replace(' ', '', str_replace('/', '-', $params['from']));
        }
        return $params;
    }

//------------------------  
    public function loadfrompageAction() //AJAX
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        //print_d($this->getRequest()->getParams()); die();
        //print_d($this->prepareFilters()); die(); 
        $page = $this->getRequest()->getParam('page', false);
        if (!$page)
            return;

        $products = $this->_user->getPurchaseHistory(
                $this->prepareFilters(), $page
        );

        $paginator = $products->getPaginator();

        $currency = new Zend_Currency(array('display' => Zend_Currency::USE_SYMBOL), 'en_US');
        $userObj  = new Application_Model_User();
        $result   = array();
        foreach ($products->getItems() as $product) {

            $result['products'][] = array(
                'id'          => $product->getId(),
                'title'       => $product->getTitle(),
                'price'       => $currency->toCurrency($product->getPrice()),
                'userName'    => $userObj->getById($product->getUserId())->getFullName(),
                'userUrl'     => $userObj->getProfileUrl(),
                'linkUrl'     => $product->getProductUrl(),
                'imgUrl'      => $product->getMainImageUrl(),
                'shipping'    => ($product->getShippingMethodId() == 'delivery') ? 'Delivery' : 'Pick up by customer',
                'orderDate'   => date('d M Y', $product->getOrderDate()),
                'orderId'     => $product->getOrderId(),
                'orderItemId' => $product->getOrderItemId(),
                'isReceived'  => (bool) $product->getIsReceived(),
            );
        }
        //print_d($paginator); die('!!!!>'.(string)$paginator->count());
        $result['totalPageCount'] = ($paginator) ? $paginator->count() : 0;

        $result['paginationControlHTML'] = $this->view->paginationControl(
                $paginator, null, 'loadfrompage_pagination_controll.phtml', array('params' => $this->getPaginationParams(), 'path' => 'user/purchase/history'));
        //die($result['paginationControlHTML']);
        //print_d($result); die();

        echo Zend_Json::encode($result);
    }

    protected function orderDates($deliveryDates)
    {
        $out = array();

        $minDate = $minTime = null;

        foreach ($deliveryDates as $deliveryDate) {
            $date = strtotime(substr($deliveryDate, 0, 10));
            $time = trim(substr($deliveryDate, 10));

            if ($minDate === null && $date) {
                $minDate = $date;
                $minTime = $time;
            } elseif ($minDate !== null && $date <= $minDate) {
                $minDate = $date;
                if ($date == $minDate) {
                    if ($minTime == '12 - 16' && $time == '8 - 12') {
                        $minTime = $time;
                    } elseif ($minTime == '16 - 20') {
                        $minTime = $time;
                    } else {
                        $minTime = $time;
                    }
                }
            }
        }

        $out[0] = date('Y-m-d', $minDate) . ' ' . $minTime;

        $minDate = $minTime = null;

        foreach ($deliveryDates as $deliveryDate) {
            if ($deliveryDate == $out[0]) {
                continue;
            }

            $date = strtotime(substr($deliveryDate, 0, 10));
            $time = trim(substr($deliveryDate, 10));

            if ($minDate === null && $date) {
                $minDate = $date;
                $minTime = $time;
            } elseif ($minDate !== null && $date <= $minDate) {
                $minDate = $date;
                if ($date == $minDate) {
                    if ($minTime == '12 - 16' && $time == '8 - 12') {
                        $minTime = $time;
                    } elseif ($minTime == '16 - 20') {
                        $minTime = $time;
                    } else {
                        $minTime = $time;
                    }
                }
            }
        }

        $out[1] = date('Y-m-d', $minDate) . ' ' . $minTime;

        $minDate = $minTime = null;

        foreach ($deliveryDates as $deliveryDate) {
            if ($deliveryDate == $out[1] || $deliveryDate == $out[0]) {
                continue;
            }

            $date = strtotime(substr($deliveryDate, 0, 10));
            $time = trim(substr($deliveryDate, 10));

            if ($minDate === null && $date) {
                $minDate = $date;
                $minTime = $time;
            } elseif ($minDate !== null && $date <= $minDate) {
                $minDate = $date;
                if ($date == $minDate) {
                    if ($minTime == '12 - 16' && $time == '8 - 12') {
                        $minTime = $time;
                    } elseif ($minTime == '16 - 20') {
                        $minTime = $time;
                    } else {
                        $minTime = $time;
                    }
                }
            }
        }

        $out[2] = date('Y-m-d', $minDate) . ' ' . $minTime;
        return $out;
    }

}