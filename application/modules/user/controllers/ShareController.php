<?php

/**
 * Class User_ShareController
 * @method User_Model_Session getSession
 */
class User_ShareController extends Ikantam_Controller_Front
{

	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->redirect('user/login/index');
		}
	}

	public function indexAction()
	{
        $this->view->user =  $this->getSession()->getUser();
	}

    public function popupAction()
    {
        $this->view->default_message = '';

        $productId = $this->getParam('product_id');


        if ($productId) {
            $product = new Product_Model_Product($productId);

            if (!$this->isAjax()) {
                $this->redirect($product->getProductUrl() . '?share=1');
            }

            if ($product->getId()) {
                $this->view->default_message = 'I\'m selling my ' .  $product->getTitle() . ' on AptDeco! Check it out here: ' . $product->getProductUrl();
            }
        }


        $isUser = $this->getParam('is_user');
        if ($isUser) {
            $user = $this->getSession()->getUser();
            $this->view->default_message = 'I\'m selling some stuff on AptDeco! Check out my profile here: ' . $user->getProfileUrl() ;


        }

        $this->view->user      =  $this->getSession()->getUser();
        $this->view->productId = $productId;

        $response['success'] = true;
        $response['html']    = $this->view->render('share/popup.phtml');
        $this->responseJSON($response)
            ->sendResponse();
        exit;

    }

    public function popupPostAction()
    {
        $message = $this->getRequest()->getParam('message');
        $user = $this->getSession()->getUser();
        $productId = $this->getParam('product_id');

        if ($this->getParam('facebook')) {
            if ($productId) {
                $product = new Product_Model_Product($productId);
                $user->postingToFacebook($message, $product);
            } else {
                $user->postingToFacebook($message, $user);
            }

        }

        if ($this->getParam('twitter')) {
            $user->postingToTwitter($message);
        }


        if ($this->getRequest()->getParam('email')) {
            $this->view->text = $message;
            $output = $this->view->render('email_template.phtml');
            foreach (explode(',', $this->getParam('emails')) as $_email) {
                $toSend = array(
                    'email'   => trim($_email),
                    'subject' => 'Check out my products!',
                    'body'    => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                try {
                    $mail->sendCopy($output);
                    $mail->send();
                } catch (Exception $e) {

                }
            }

        }

        $response['success'] = true;
        $response['message'] = 'The message has been shared';
        $this->responseJSON($response)
            ->sendResponse();
        exit;
    }

    public function facebookAction()
    {

        $user = $this->getSession()->getUser();
        $redirectUri = Ikantam_Url::getUrl('user/share/facebook');
        $facebook = $user->initFacebook();
        $fbUser = $facebook->getUser();

        if (!$fbUser) {
            $loginUrl = $facebook->getLoginUrl(array('redirect_uri'=>$redirectUri,'scope'=> array('publish_actions')));
            header('Location: ' . $loginUrl);
        }

        $user->personalSettings('facebook_token', $facebook->getAccessToken());

    }

    public function twitterAction()
    {
        $config   = $this->getXmlConfig('additional_settings', 'twitter');
        $session = $this->getSession();
        $this->view->error = false;

        require_once(APPLICATION_PATH . '/../library/twitteroauth/twitteroauth.php');


        if (!$this->getRequest()->getParam('oauth_verifier')) {
            /* before redirect to twitter */
            /* Build TwitterOAuth object with client credentials. */
            $connection = new TwitterOAuth($config->appId, $config->appSecret);

            /* Get temporary credentials. */
            $request_token = $connection->getRequestToken(Ikantam_Url::getUrl('user/share/twitter'));

            /* Save temporary credentials to session. */
            $token = $request_token['oauth_token'];

            $session->setTwitterOauthToken($request_token['oauth_token']);
            $session->setTwitterOauthTokenSecret($request_token['oauth_token_secret']);

            /* If last connection failed don't display authorization link. */

            switch ($connection->http_code) {
                case 200:
                    /* Build authorize URL and redirect user to Twitter. */
                    $url = $connection->getAuthorizeURL($token);
                    header('Location: ' . $url);
                    exit;
                    break;
                default:
                    /* Show notification if something went wrong. */
                    echo 'Could not connect to Twitter. Refresh the page or try again later.';
            }

        } else {
            /* callback */

            /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
            $connection = new TwitterOAuth($config->appId, $config->appSecret, $session->getTwitterOauthToken(), $session->getTwitterOauthTokenSecret());

            /* Request access tokens from twitter */
            $access_token = $connection->getAccessToken($this->getRequest()->getParam('oauth_verifier'));

            /* Save the access tokens. Normally these would be saved in a database for future use. */

            /* Remove no longer needed request tokens */
            $session->unsTwitterOauthToken();
            $session->unsTwitterOauthTokenSecret();

            $connection = new TwitterOAuth($config->appId, $config->appSecret, $access_token['oauth_token'], $access_token['oauth_token_secret']);

            /* If method is set change API call made. Test is called by default. */
            $twUser = $connection->get('account/verify_credentials');
            if ($twUser->id) {
                $session->getUser()->personalSettings('twitter_token', $access_token['oauth_token']);
                $session->getUser()->personalSettings('twitter_token_secret', $access_token['oauth_token_secret']);
            } else {
                $this->view->error = @$twUser->errors[0]->message;
            }
        }
    }

}
