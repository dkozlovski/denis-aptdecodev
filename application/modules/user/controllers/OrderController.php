<?php

class User_OrderController extends Ikantam_Controller_Front
{

    const MESSAGE_ITEM_NOT_AVAILABLE = 'Order cancelled. We have notified buyer that the product is not available.';
    const MESSAGE_REQUEST_ERROR      = '';

    protected $_filters = array();
    protected $_user    = null;
    protected $_searchValue;
    protected $_filterParams;

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {

            // moved in plugin "GuestPresignInHandler" $this->_helper->redirectAfter(); //save the request params and use it on next initRedirect() call
            $this->_redirect('user/login/index');
        }
        $this->_filters[]   = new Zend_Filter_StringTrim();
        $this->_filters[]   = new Zend_Filter_StringToLower();
        $this->_searchValue = $this->getRequest()->getParam('search', false);
        $this->filter($this->_searchValue);
        $this->_user        = $this->getSession()->getUser();
    }

    public function indexAction()
    {
        $this->_forward('list');
    }

    public function listAction()
    {
        $products = $this->_user->getOrderList(
                $this->prepareFilters(), $this->getRequest()->getParam('page', 1), 10, $this->_searchValue
        );

        $this->view->paginator        = $products->getPaginator()->setPageRange(5);
        $this->view->products         = $products->getItems();
        $this->view->userObj          = new Application_Model_User();
        $this->view->status           = ($this->_filterParams['status']) ? $this->_filterParams['status'] : 'all';
        $this->view->date             = ($this->_filterParams['date']) ? $this->_filterParams['date'] : 'all';
        $from                         = ($this->_filterParams['from']) ? $this->_filterParams['from'] : false;
        $till                         = ($this->_filterParams['till']) ? $this->_filterParams['till'] : false;
        $this->view->from             = $from;
        $this->view->till             = $till;
        $this->view->paginationParams = $this->getPaginationParams();
        if ($from || $till) {
            $this->view->date = 'between';
        }
        $this->view->activeMenu = 'listings';
        $this->view->headTitle('My Sales');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

    public function orderAction()
    {
        $this->view->headScript()
            ->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/aptdeco/confirmation.js'));

        $orderId = $this->getRequest()->getParam('id', 0);

        $item = new Order_Model_Item($orderId);
        if (!$item->getId()) {
            throw new Exception('This product does not exist');
        }

        $product             = $item->getProduct();
        $this->view->product = $product;
        $this->view->orderId = $item->getOrder()->getId();

        $order = $item->getOrder();

        $this->view->shipping = ($item->getShippingMethodId() == 'delivery') ? 'Delivery' : 'Pick up by customer';
        $this->view->buyer    = $order->getUser();

        if ($product->getSeller()->getId() != $this->getSession()->getUserId()) {
            throw new Exception('This product does not exist');
        }

        if ($item->getShippingMethodId() == 'delivery' && $item->getBuyerDates() && !$item->getSellerDates()) {
            $dates                    = $item->getBuyerDatesForSeller();
            $this->view->orderedDates = $dates;
        }
        $this->view->item         = $item;
        $this->view->order        = $order;
        $this->view->activeMenu   = 'listings';
        $this->view->loggedInUser = $this->getSession()->getUser();
        $this->view->session      = $this->getSession();
    }

    public function suggestionsAction()// AJAX action
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        if (!$this->getRequest()->isPost())
            return;
        $result = array();
        if (is_string($this->_searchValue) && strlen($this->_searchValue) > 0) {
            $productsByTitle = $this->_user->getOrderList(
                            array('title' => $this->_searchValue, 'groupByProduct' => true), 1, -1)->getItems();

            foreach ($productsByTitle as $product) {
                $result['products'][] = array('id'      => $product->getId(),
                    'title'   => $product->getTitle(),
                    'encoded' => urlencode($product->getTitle()),
                    'url'     => $product->getProductUrl());
            }
        }

        echo Zend_Json::encode($result);
    }

    protected function filter(&$val)
    {
        if (!$val)
            return false;
        foreach ($this->_filters as $filter) {
            if ($filter instanceof Zend_Filter_Interface) {
                $filter->filter($val);
            }
        }
    }

    /**
     * Retrieves field value from objects and pass it to result array.         
     * @param array $objArray - array of Ikantam_Objects
     * @param string $method - method name to retrieve value
     * @param string $keyMethod - method name to retrieve key
     * @return array
     */
    private function convertToSimpleArray($objArray, $method, $keyMethod)
    {
        $result = array();
        foreach ($objArray as $obj) {
            $result[$obj->$keyMethod()] = $obj->$method();
        }
        return $result;
    }

    private function getFilterParams()
    {
        $params = &$this->_filterParams;
        if (is_null($params)) {
            $rqst = $this->getRequest();

            $params['brand']   = $rqst->getParam('brand', false);
            $params['user']    = $rqst->getParam('user', false);
            $params['date']    = $rqst->getParam('date', false);
            $params['from']    = $rqst->getParam('from', false);
            $params['till']    = $rqst->getParam('till', false);
            $params['product'] = $rqst->getParam('product', false);
            $params['status']  = $rqst->getParam('status', false);
            $params['order']   = $rqst->getParam('order', false);
            $params['product'] = $rqst->getParam('product', false);

            if (!$params['from']) {
                $params['from'] = $rqst->getParam('cfrom', false);
            }

            if (!$params['till']) {
                $params['till'] = $rqst->getParam('ctill', false);
            }

            if (!$params['date']) {
                $params['date'] = $rqst->getParam('cdate', false); // cdate = 'all' by default. (current date)
            } else {
                $params['from'] = false;
                $params['till'] = false;
            }
        }
        return $params;
    }

    private function getPaginationParams()
    {
        $params = array_filter($this->getFilterParams());
        if (isset($params['till'])) {
            $params['till'] = str_replace(' ', '', str_replace('/', '-', $params['till']));
        }
        if (isset($params['from'])) {
            $params['from'] = str_replace(' ', '', str_replace('/', '-', $params['from']));
        }
        return $params;
    }

    private function prepareFilters()
    {
        $filter     = array();
        $collection = 'Product_Model_Product_Collection';
        $vDate      = new Ikantam_Validate_Date('mdy');
        foreach (array_filter($this->getFilterParams()) as $key => $value) {
            if ($key == 'status') {
                if ($value == 'received') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_RECEIVED;
                } elseif ($value == 'in-progress') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_IN_DELIVERY;
                } elseif ($value == 'sold') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_SOLD;
                } elseif ($value == 'not-sold') {
                    $filter[$collection::FILTER_TYPE_STATUS] = $collection::FILTER_NOT_SOLD;
                }
            }//status

            if ($key == 'product') {
                $filter[$collection::FILTER_TYPE_TITLE] = $value;
            }

            if ($key == 'brand') {
                $filter[$collection::FILTER_TYPE_BRAND] = $value;
            }//brand

            if ($key == 'date') {
                if ($value == 'month')
                    $filter[$collection::FILTER_TYPE_DATE] = $collection::FILTER_MONTH;
                if ($value == 'year')
                    $filter[$collection::FILTER_TYPE_DATE] = $collection::FILTER_YEAR;
            }//month-year            

            if ($key == 'from') {
                if ($vDate->isValid($value)) {
                    $date                                              = $vDate->getParsedDate();
                    $filter[$collection::FILTER_TYPE_BETWEEN_DATES][0] = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
                }
            }

            if ($key == 'till') {
                if ($vDate->isValid($value)) {
                    $date                                              = $vDate->getParsedDate();
                    $filter[$collection::FILTER_TYPE_BETWEEN_DATES][1] = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
                }
            }//between dates

            if ($key == 'user') {
                $filter[$collection::FILTER_TYPE_USER] = $value;
            }

            if ($key == 'order') {
                $filter[$collection::FILTER_TYPE_ORDER] = $value;
            }
        }
        return $filter;
    }

    public function setAvailableAction()
    {
        $dbh = Application_Model_DbFactory::getFactory()->getConnection();
        $dbh->beginTransaction();

        try {

            $itemId      = $this->getRequest()->getParam('id');
            $isAvailable = (bool) $this->getRequest()->getParam('availabitily');

            $item = new Order_Model_Item($itemId);
            if (!$item->getId()) {
                throw new Exception('This order does not exist');
            }

            if ($item->getStatus() != Order_Model_Order::ORDER_STATUS_ON_HOLD) {
                $this->_helper->redirector('order', 'order', 'user', array('id' => $item->getId()));
            }

            $product = $item->getProduct();

            if ($product->getSeller()->getId() != $this->getSession()->getUserId()) {
                throw new Exception('This product does not exist');
            }

            $sellerDate = null;
            $buyerDate = null;
            if ($isAvailable) {
                $error           = false;
                $deliveryOptions = array();

                if ($item->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
                    /*  date param example: 2014-06-08 8-12|2014-06-08 8-12  */
                    $dates = explode('|', $this->getRequest()->getParam('date'));
                    $buyerDate  = isset($dates[0]) ? $dates[0] : null;
                    $sellerDate = isset($dates[1]) ? $dates[1] : null;
                    $phoneNumber  = $this->getRequest()->getPost('phone_number');
                    $buildingType = $this->getRequest()->getPost('building_type');
                    $fos          = (int) $this->getRequest()->getPost('flights_of_stairs');
                    $paperInfo    = trim(strip_tags($this->getRequest()->getPost('paperwork_info')));

                    if (empty($sellerDate) || !in_array($sellerDate, $item->getBuyerDatesForSeller())) {
                        $error = true;
                    }

                    if (empty($buyerDate) || !in_array($buyerDate, $item->getBuyerDates())) {
                        $error = true;
                    }

                    $deliveryOptions = array(
                        'building_type'     => $buildingType,
                        'flights_of_stairs' => $fos,
                        'paperwork_info'    => $paperInfo,
                        'phone_number'      => $phoneNumber
                    );

                    $validator = new User_Form_Order_Availability();
                    $error     = $error || !$validator->isValid($deliveryOptions);
                }

                if ($error) {
                    $this->getSession()->addMessage('error', 'Opps, looks like some of the fields have errors or were left blank.');
                    $this->getSession()->addFormErrors($validator->getFlatMessages());
                } else {

                    Httpful\Bootstrap::init();
                    RESTful\Bootstrap::init();
                    Balanced\Bootstrap::init();
                    Balanced\Settings::init();

                    $hold  = Balanced\Hold::get($item->getHoldUri());
                    $debit = $hold->capture();

                    $additionalDebitUri = null;
                    if ($item->getAdditionalHoldUri()) {
                        $additionalHold  = Balanced\Hold::get($item->getAdditionalHoldUri());
                        $additionalDebit = $additionalHold->capture();
                        $additionalDebitUri = $additionalDebit->uri;
                    }

                    if ($debit) {
                        $buyer = $item->getOrder()->getUser();
                        if (!$buyer->getId()) {//guest order
                            $item->setAccessCode(Ikantam_Math_Rand::getString(64, Ikantam_Math_Rand::ALPHA_NUMERIC));
                        }

                        $item->setSellerDates($sellerDate)
                                ->setBuyerDate($buyerDate)
                                ->setSellerDeliveryOptions($deliveryOptions)
                                ->setIsAvailable(1)
                                ->setIsProductAvailable(1)
                                ->setStatus(Order_Model_Order::ORDER_STATUS_PENDING)
                                ->setDebitsUri($debit->uri)
                                ->setAdditionalDebitsUri($additionalDebitUri)
                                ->save();

                        $couponId = $item->getOrder()->getCouponId();
                        if ($couponId) {
                            $coupon = new Cart_Model_Coupon();
                            $coupon->getById($couponId);
                            $coupon->addUse($item->getOrder()->getUserId(), $item->getOrder()->getId());
                        }

                        //also send receipt email
                        $mailer = new Notification_Model_Order_Confirmed();
                        $mailer->sendEmails($item);
                    } else {
                        //@TODO: error text required
                        $this->getSession()->addMessage('error', self::MESSAGE_REQUEST_ERROR);
                    }
                }
            } else {
                $payout = new Order_Model_Payout();
                $payout->getByItemId($item->getId());

                Httpful\Bootstrap::init();
                RESTful\Bootstrap::init();
                Balanced\Bootstrap::init();
                Balanced\Settings::init();

                $hold    = Balanced\Hold::get($item->getHoldUri());
                $success = $hold->void();

                if ($item->getAdditionalHoldUri()) {
                    $additionalHold = Balanced\Hold::get($item->getAdditionalHoldUri());
                    $additionalHold->void();
                }

                if ($success && $payout->getId()) {
                    $item->setIsAvailable(0)
                            ->setIsProductAvailable(0)
                            ->setStatus(Order_Model_Order::ORDER_STATUS_CANCELED)
                            ->save();

                    if ($item->getProduct()->getIsSold()) {
                        $item->getProduct()->setIsPublished(0)->save();
                    }

                    $payout->setIsActive(0)->save();

                    $mailer = new Notification_Model_Order_Invalidated();
                    $mailer->sendEmails($item);

                    $this->getSession()->addMessage('success', self::MESSAGE_ITEM_NOT_AVAILABLE);

                    /* Recalculate payout with new shipping fee */

                    $allItems = new Order_Model_Item_Collection();
                    $payouts  = new Order_Model_Payout_Collection();

                    $allItems->getByOrderId($item->getOrderId());

                    $totalShippingPrice    = 0.0;
                    $numberOfDeliveryItems = 0;
                    $options               = new Application_Model_Options();

                    foreach ($allItems as $dItem) {
                        $dItem->getProduct()->setQty(0)
                                ->save();
                        if ($dItem->getStatus() != Order_Model_Order::ORDER_STATUS_CANCELED && $dItem->getId() != $item->getId()) {
                            if ($dItem->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
                                $totalShippingPrice += $dItem->getSummaryPrice();
                                $numberOfDeliveryItems += $dItem->getQty(1);
                                $oldPayout = new Order_Model_Payout();   //disactive old payout
                                $oldPayout->getByItemId($dItem->getId())
                                        ->setIsActive(0)
                                        ->save();

                                $newPayout = $oldPayout->unsId(); //new payout same as old but shipping_price
                                $newPayout->setIsActive(1)
                                        ->setShippingPrice(($options->getFee($newPayout->getProductPrice() * $dItem->getQty(1), $options::BUYER) * $newPayout->getProductPrice() * $dItem->getQty(1)) / 100);


                                $payouts->addItem($newPayout); //store in collection for future updates    
                                Cart_Model_Item_Collection::removeNotAvailable(); //clear carts
                            }
                        }
                    }


                    $SFCollection = new Application_Model_ShippingFeeRule_Collection();
                    $shippingFee  = $SFCollection->getShippingFee($totalShippingPrice, $numberOfDeliveryItems); //new shipping price

                    if ($shippingFee > 0 && $payouts->getSize() > 0) {

                        $shippingFee = $shippingFee / $payouts->getSize();

                        foreach ($payouts as $newPayout) {
                            $newPayout->setShippingPrice($newPayout->getShippingPrice() + $shippingFee)
                                    ->save();
                        }
                    }

                    //*****************END*RECALCULATION************************ 
                } else {
                    //@TODO: error text required
                    $this->getSession()->addMessage('error', self::MESSAGE_REQUEST_ERROR);
                }
            }

            if (!$isAvailable) {
                $item->getEventDispatcher()->trigger('transaction.canceled.byUser', $item);
            }else{
                $item->getEventDispatcher()->trigger('transaction.approved.byUserr', $item);
            }
            $dbh->commit();
        } catch (Exception $ex) {
            $dbh->rollBack();
            $this->_logException($ex);
            $this->getSession()->addMessage('error', 'Cannot save pick up details. Please try again later.');
            //throw $ex;
        }
        $this->_helper->redirector('order', 'order', 'user', array('id' => $item->getId()));
    }

    protected function _logException($exception)
    {
        $fileName = APPLICATION_PATH . '/log/exception.log';

        $data = $this->prepareException($exception) . $this->prepareRequest() . $this->prepareUserInfo() . "\n";

        file_put_contents($fileName, $data, FILE_APPEND);
    }

    protected function prepareException($exception)
    {
        return date('Y-m-d H:i:s') . ': Unable to confirm item availability:' . "\n" . $exception->getMessage() . "\n" . $exception->getTraceAsString() . "\n";
    }

    protected function prepareRequest()
    {
        $str = "Request Parameters:\n";
        foreach ($this->getRequest()->getParams() as $param => $value) {
            $str .= $param . ': ' . $value . "\n";
        }
        return $str;
    }

    protected function prepareUserInfo()
    {
        $str = "User info:\n";
        $str .= "IP: " . $this->getRequest()->getClientIp() . "\n";
        $str .= "User agent: " . $_SERVER['HTTP_USER_AGENT'] . "\n\n";
        $str .= '*************************************************************' . "\n";
        return $str;
    }

    public function changePickupAddressAction()
    {
        $addressData                 = $this->getRequest()->getPost('address');
        $addressData['country_code'] = 'US';
        $itemId                      = $this->getRequest()->getParam('id');

        $item    = new Order_Model_Item($itemId);
        $product = $item->getProduct();

        $addressForm = new User_Form_Address();

        if (!$product->getId() || $product->getUserId() != $this->getSession()->getUserId()) {
            throw new Exception('This product does not exist.');
        }

        if ($addressForm->isValid($addressData)) {
            $address = new User_Model_Address();

            $address->addData($addressForm->getValues())
                    ->setUserId($this->getSession()->getUserId())
                    ->setIsPrimary(0)
                    ->save();

            $product->setPickUpAddressId($address->getId())
                    ->setPickupFullName($addressForm->getValue('full_name'))
                    ->setPickupAddressLine1($addressForm->getValue('address_line1'))
                    ->setPickupAddressLine2($addressForm->getValue('address_line2'))
                    ->setPickupCity($addressForm->getValue('city'))
                    ->setPickupState($addressForm->getValue('state'))
                    ->setPickupPostcode($addressForm->getValue('postcode'))
                    ->setPickupPhone($addressForm->getValue('phone_number'))
                    ->setPickupCountryCode($addressForm->getValue('country_code'))
                    ->save();

            if ($item->getShippingMethodId() == Order_Model_Order::ORDER_SHIPPING_METHOD_DELIVERY) {
                $buildingType    = isset($addressData['building_type']) ? $addressData['building_type'] : null;
                $flightsOfStairs = isset($addressData['flights_of_stairs']) ? $addressData['flights_of_stairs'] : null;

                $error = false;

                if (!in_array($buildingType, array('elevator', 'walkup'))) {
                    $error = true;
                }

                if ($buildingType == 'walkup' && ($flightsOfStairs < 1 || $flightsOfStairs > 10)) {
                    $error = true;
                }

                if (!$error) {
                    $sdo = $item->getSellerDeliveryOptions();

                    $sdo['building_type']     = $buildingType;
                    $sdo['flights_of_stairs'] = $flightsOfStairs;

                    $item->setSellerDeliveryOptions($sdo)->save();

                    $confirmed = new Notification_Model_Order_Confirmed();
                    $confirmed->sendEmailToCarrier($item);
                } else {
                    $this->getSession()->addMessage('error', 'Opps, looks like some of the fields have errors or were left blank.');
                }
            }

            $this->getSession()->addMessage('success', 'Pick up address saved.');
        } else {
            $this->getSession()->addMessage('error', 'Opps, looks like some of the fields have errors or were left blank.');
        }

        $this->_helper->redirector('order', 'order', 'user', array('id' => $item->getId()));
    }

//prepare filters    
}