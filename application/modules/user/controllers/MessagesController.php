<?php

class User_MessagesController extends Ikantam_Controller_Front
{
    protected $_messagesPerPage = 10;
    
    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }
    }
    
    public function indexAction()
    {
        $this->_forward('inbox', 'messages', 'user');
    }

    public function inboxAction()
    {
        $userId = $this->getSession()->getUserId();
        
        $status = $this->getRequest()->getParam('status');
        $messages = new Application_Model_Message_Collection();

        if ($status == 'new') {
            $totalMessages = $messages->getNewByRecipientId($userId)->getSize();
        } else {
            $totalMessages = $messages->getByRecipientId($userId)->getSize();
        }

        $limit = $this->_messagesPerPage;

        $totalPages = (int) ceil($totalMessages / $limit);

        $page = (int) $this->getRequest()->getParam('page');

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $limit;

        if ($status == 'new') {
            $this->view->messages = $messages->clear()->getNewByRecipientId($userId, $offset, $limit);
            $this->view->status = 'new';
        } else {
            $this->view->messages = $messages->clear()->getByRecipientId($userId, $offset, $limit);
            $this->view->status = 'all';
        }
        $this->view->totalPages = $totalPages;
        $this->view->currentPage = $page;
        $this->view->activeMenu = 'inbox_index';
    }

    public function sentAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $totalMessages = $this->getSession()->getUser()->countOutboxMessages();

        $limit = $this->_messagesPerPage;

        $totalPages = (int) ceil($totalMessages / $limit);

        $page = (int) $this->getRequest()->getParam('page');

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $limit;

        $this->view->messages = $this->getSession()->getUser()->getOutboxMessages($offset, $limit);

        $this->view->totalPages = $totalPages;
        $this->view->currentPage = $page;
        $this->view->activeMenu = 'inbox_sent';
    }

    public function notAllowedAction()
    {
        $this->view->activeMenu = 'inbox_sent';
    }

    public function viewAction()
    {
        $this->view->activeMenu = 'inbox_sent';
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $id = $this->getRequest()->getParam('id');

        $message = new Conversation_Model_Conversation_Message($id);

        $conversation = new Conversation_Model_Conversation($this->getRequest()->getParam('conversation_id'));

        if (!$message->getId() || !$message->getConversation()->getId()) {
            if (!$conversation->getId()) {
                $this->_redirect('user/inbox/index');
            } else {
                $this->view->conversation = $conversation;
            }
        } else {
            $message->setAsReadedFor($this->getSession()->getUser());
            $this->view->conversation = $message->getConversation();
        }

        if (false) {
            //@TODO: check participants
        }
        $this->view->session = $this->getSession();
    }

    public function createAction()
    {
        $this->view->activeMenu = 'inbox_sent';
        $userId = $this->getRequest()->getParam('id');
        if (!$this->getSession()->isLoggedIn()) {

            $this->_helper->redirector('index', 'login', 'user', array('redirect-success' => urlencode('user/inbox/create/id/' . $userId)));
            //$this->_redirect('user/login/index', );
        }



        $user = new Application_Model_User($userId);

        if (!$this->getSession()->getUser()->canSendMessagesTo($user)) {
            $this->_forward('not-allowed');
        }

        $this->view->user = $user;
        $this->view->session = $this->getSession();
    }

    public function conversationPostAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $id = $this->getRequest()->getParam('id');
        $conversation = new Conversation_Model_Conversation($id);

        //@TODO:check permissions to post to the conversation
        //if (!$this->getSession()->getUser()->canSendMessagesTo($user)) {
        //$this->_helper->redirector('create', 'inbox', 'user', array('id' => $userId));
        //}

        $data = $this->getRequest()->getPost();

        $form = new User_Form_Conversation_Existing();

        if ($form->isValid($data)) {
            $author = $this->getSession()->getUser();

            $message = new Conversation_Model_Conversation_Message();
            $message->setText($form->getValue('text'))->setAuthor($author);

            $conversation->addMessage($message)->save();
            
            foreach ($conversation->getUsers() as $user) {
                if ($user->getId() != $author->getId())
                $this->sendMessageToRecipient($message, $user);
            }
            
            
            $this->_helper->redirector('view', 'inbox', 'user', array('id' => $message->getId()));
        } else {
            $this->getSession()->addFormErrors($form->getFlatMessages());
        }
        $this->_helper->redirector('view', 'inbox', 'user', array('conversation_id' => $conversation->getId()));
    }

    public function createPostAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $author = $this->getSession()->getUser();

        $userId = $this->getRequest()->getParam('id');
        $user = new User_Model_User($userId);

        if (!$this->getSession()->getUser()->canSendMessagesTo($user)) {
            $this->_helper->redirector('create', 'inbox', 'user', array('id' => $userId));
        }

        $data = $this->getRequest()->getPost();

        try {
            $form = new User_Form_Message();

            if ($form->isValid($data)) {
                $conversation = new Conversation_Model_Conversation();
                $message = new Conversation_Model_Conversation_Message();
                $message->setText($form->getValue('text'))->setAuthor($author);
                $conversation->setSubject($form->getValue('subject'))
                        ->addMessage($message)
                        ->addUser($user)
                        ->addUser($author)
                        ->save();

                $this->getSession()->addMessage('success', sprintf('Message has been sent. <a href="%s">See Message</a>', Ikantam_Url::getUrl('user/inbox/view', array('conversation_id' => $conversation->getId()))));

                $this->sendMessageToRecipient($message, $user);

                $this->successRedirect($user);
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to send message. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->getSession()->addFormData($data);

        //die('error');
        $this->failureRedirect($user);
    }

    public function sendMessageToRecipient($message, $user)
    {
        if ($user->getMainEmail()) {
            $this->view->messageId = $message->getId();
        $output = $this->view->render('email_message_pending.phtml');


        $toSend = array(
            'email' => $user->getMainEmail(),
            'subject' => 'You have new message',
            'body' => $output);

        $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
        $mail->send();
        }
        
        
    }

    protected function failureRedirect($user)
    {
        $redirect = urldecode($this->getRequest()->getParam('redirect-failure'));

        if (empty($redirect)) {
            $this->_helper->redirector('create', 'inbox', 'user', array('id' => $user->getId()));
        } else {
            $this->_redirect($redirect);
        }
    }

    protected function successRedirect($user)
    {
        $redirect = urldecode($this->getRequest()->getParam('redirect-success'));

        if (empty($redirect)) {
            $this->_helper->redirector('create', 'inbox', 'user', array('id' => $user->getId()));
        } else {
            $this->_redirect($redirect);
        }
    }

    public function deleteAction()
    {
        $userId = $this->getSession()->getUserId();
        $ids = $this->getRequest()->getPost('messages');

        foreach ($ids as $id) {
            $message = new Application_Model_Message($id);

            if (!$message->getId()) {
                continue;
            }
            
            if ($message->getRecipientId() == $userId) {
                $message->setIsVisibleToRecipient(0)->save();
            } elseif ($message->getAuthorId() == $userId) {
                $message->setIsVisibleToAuthor(0)->save();
            }

        }
        if ($this->getRequest()->getParam('redirect')) {
            $this->_helper->redirector('sent', 'messages', 'user');
        }
        $this->_helper->redirector('inbox', 'messages', 'user');
    }

    public function readAction()
    {
        $userId = $this->getSession()->getUserId();
        $ids = $this->getRequest()->getPost('messages');

        foreach ($ids as $id) {
            $message = new Application_Model_Message($id);

            if (!$message->getId()) {
                continue;
            }

            if ($message->getRecipientId() == $userId) {
                $message->setIsNew(0)->save();
            }
        }
        if ($this->getRequest()->getParam('redirect')) {
            $this->_helper->redirector('sent', 'messages', 'user');
        }
        $this->_helper->redirector('inbox', 'messages', 'user');
    }

}