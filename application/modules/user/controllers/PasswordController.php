<?php

class User_PasswordController extends Ikantam_Controller_Front
{

    private $_forgotPasswordForm;
    private $_resetPasswordForm;

    public function forgotAction()
    {
        $this->view->session = $this->getSession();

        $this->view->headTitle('Password Recovery');
        $this->view->headMeta()->appendName('description', 'Recover your password on AptDeco furniture marketplace.');

    }

    public function forgotPostAction()
    {
        $forgotPasswordData = $this->getRequest()->getPost();

        try {
            $form = $this->getForgotPasswordForm();

            if ($form->isValid($forgotPasswordData)) {
                if ($this->getSession()->getUser()->forgotPassword($form->getValues())) {
                    $this->getSession()->addMessage('success', 'Reset link has been sent to your email address.');
                } else {
                    $this->getSession()->addFormErrors(array('email' => 'This email address is not valid. Please check your email address and try again.'));
                }

                $this->_redirect('user/password/forgot');
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Ikantam_Exception_Password $exception) {
            $this->getSession()->addMessage('error', $exception->getMessage());
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to reset password. Please, try again later');
            $this->logException($exception);
        }

        $this->getSession()->addFormData($forgotPasswordData, 'email');
        $this->_redirect('user/password/forgot');
    }

    protected function getFailureRedirect()
    {
        $code = $this->getRequest()->getParam('code'); //@TODO:sanitize code
        return Ikantam_Url::getUrl('user/password/reset', array('code' => $code));
    }

    public function resetAction()
    {
        $code                = $this->getRequest()->getParam('code');
        $this->view->code    = $code;
        $this->view->session = $this->getSession();
    }

    public function resetPostAction()
    {
        $forgotPasswordData = $this->getRequest()->getPost();

        try {
            $form = $this->getResetPasswordForm();

            if ($form->isValid($forgotPasswordData)) {
                $success = $this->getSession()->getUser()->resetPassword($form->getValues());

                if ($success) {
                    $this->getSession()->addMessage('success', 'Password has been saved');

                    try {
                        $auth['password'] = $form->getValue('password');
                        $auth['email']    = $success;
                        $this->getSession()->authenticate($auth);
                        $this->_redirect('user/profile/account');
                    } catch (Ikantam_Exception_Auth $exception) {
                        $this->getSession()->addMessage('error', $exception->getMessage());
                    } catch (Exception $exception) {
                        $this->getSession()->addMessage('error', 'Unable to log in. Please, try again later');
                        //$this->logException($exception, $logParams = false);
                    }
                } else {
                    $this->getSession()->addMessage('error', 'Code is invalid or has expired');
                }
                $this->_redirect('user/password/reset');
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Ikantam_Exception_Password $exception) {
            $this->getSession()->addMessage('error', $exception->getMessage());
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to reset password. Please, try again later');
            $this->logException($exception);
        }

        $this->_redirect($this->getFailureRedirect());
    }

    protected function getForgotPasswordForm()
    {
        if (!$this->_forgotPasswordForm) {
            $this->_forgotPasswordForm = new User_Form_ForgotPassword();
        }

        return $this->_forgotPasswordForm;
    }

    protected function getResetPasswordForm()
    {
        if (!$this->_resetPasswordForm) {
            $this->_resetPasswordForm = new User_Form_ResetPassword();
        }

        return $this->_resetPasswordForm;
    }

}