<?php

class User_SalesController extends Ikantam_Controller_Front
{

	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->redirect('user/login/index');
	}
	}

	public function indexAction()
	{

	}
	
	public function itemsAction()
	{
		$user = $this->getSession()->getUser();
		
		$this->view->items = new Product_Model_Product2_Collection();
		$this->view->items->getByUser($user->getId());
	}
	
	public function deleteItemAction()
	{
		$user = $this->getSession()->getUser();
		$id = $this->getRequest()->getParam('id');
		
		$product = new Product_Model_Product($id);
		
		if ($product->getId() && $product->getUserId() == $user->getId()) {
			$product->delete();
		}
		
		$this->redirect('user/sale/list');
	}
	
	public function publishItemAction()
	{
		$user = $this->getSession()->getUser();
		$id = $this->getRequest()->getParam('id');
		
		$product = new Product_Model_Product($id);
		
		if ($product->getId() && $product->getUserId() == $user->getId()) {
			$product->setIsPublished(1)->save();
		}
		
		$this->redirect('user/sale/list');
	}
	
        public function republishItemAction(){
            
                
            
        }
        
	public function unpublishItemAction()
	{
		$user = $this->getSession()->getUser();
		$id = $this->getRequest()->getParam('id');
		
		$product = new Product_Model_Product($id);
		
		if ($product->getId() && $product->getUserId() == $user->getId()) {
			$product->setIsPublished(0)->save();
		}
		
		$this->redirect('user/sale/list');
	}

    public function changePriceAction()
    {
        $price = (float)$this->getParam('price');
        $product = new Product_Model_Product($this->getParam('product_id'));
        $errors = array();

        if (!($product->isBelongsToUser($this->getSession()->getUser()) && $this->getSession()->getUser()->getId())) {
            $errors[] = 'Undefined product';
        }

        $chosenCategory = $product->getCategory()->getParent();
        if (in_array(strtolower($chosenCategory->getTitle()), array('décor', 'decor'))) {
            if ($price <= 1) {
                $errors[] = 'Price must be greater than $1';
            }
        } elseif ($price < 50) {
            $errors[] = 'Price must be greater than $50';
        }
        if (($origPrice = $product->getOriginalPrice()) && $price > $origPrice) {
            $errors[] = 'Price must be less than original price $'.$origPrice.'.';
        }


        if (count($errors) == 0) {

            $product->setPrice($price)
                ->save();

            $this->getSession()->addMessage('success', 'Price for your item has been changed.');
        } else {
            foreach ($errors as $_message) {
                $this->getSession()->addMessage('error', $_message);
            }
        }
        $this->redirect('user/sale/list');
    }

}
