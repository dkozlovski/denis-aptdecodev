<?php

class User_IndexController extends Ikantam_Controller_Front
{

	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
	}

	public function indexAction()
	{
		
	}

}
