<?php

class User_ResetPasswordController extends Ikantam_Controller_Front
{

	protected $_form;

	public function init()
	{
		$this->_form = $form = new User_Form_NewPassword();
	}

	public function indexAction()
	{
		$code = $this->getRequest()->getParam('code');
		$resetPasswordObj = new User_Model_PasswordReset();
		$resetPasswordObj->getByCode($code);
		
		if ($resetPasswordObj->getId()) {

				if ($resetPasswordObj->getIsActive() && $resetPasswordObj->IsExpiryValid()) {
					$this->_form->setAction(Ikantam_Url::getUrl('user/reset-password/post'));
					$this->_form->code->setValue($code);
					$this->view->form = $this->_form;
				} else {
					throw new Exception('Code not active or expired.');
				}
				//$this->redirect(array('controller'=>'index','action'=>'index'));
			}
		
	}

	public function postAction()
	{
		$formData = $this->getRequest()->getPost();
		if ($this->_form->isValid($formData)) {
			$resetPasswordObj = new User_Model_PasswordReset();
			$user = new Application_Model_User();
			$resetPasswordObj->getByCode($this->_form->getValue('code'));

			$user->getByEmail($resetPasswordObj->getEmail());

			$user->setPassword($user->hash($this->_form->getValue('password')));
			$user->save();

			$resetPasswordObj->setIsActive(false)->save();

			$this->_helper->redirector(array('module' => 'user', 'controller' => 'reset-password', 'action' => 'index'));
		}
	}

}
