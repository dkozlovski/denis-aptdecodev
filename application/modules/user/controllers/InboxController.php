<?php

class User_InboxController extends Ikantam_Controller_Front
{

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->getSession()->setRefererRedirectUrl(Ikantam_Url::getUrl('user/inbox'));
            $this->_redirect('user/login/index');
        }
    }

    public function indexAction()
    {

        $status = $this->getRequest()->getParam('status');

        if ($status == 'new') {
            $totalMessages = $this->getSession()->getUser()->countNewInboxMessages();
        } else {
            $totalMessages = $this->getSession()->getUser()->countInboxMessages();
        }

        $limit = 10;

        $totalPages = (int) ceil($totalMessages / $limit);

        $page = (int) $this->getRequest()->getParam('page');

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $limit;

        if ($status == 'new') {
            $this->view->messages = $this->getSession()->getUser()->getNewInboxMessages($offset, $limit);
            $this->view->status = 'new';
        } else {
            $this->view->messages = $this->getSession()->getUser()->getInboxMessages($offset, $limit);
            $this->view->status = 'all';
        }
        $this->view->totalPages = $totalPages;
        $this->view->currentPage = $page;
        $this->view->activeMenu = 'inbox_index';

        $this->view->headTitle('Inbox');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

    public function sentAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $totalMessages = $this->getSession()->getUser()->countOutboxMessages();

        $limit = 10;

        $totalPages = (int) ceil($totalMessages / $limit);

        $page = (int) $this->getRequest()->getParam('page');

        if ($page > $totalPages) {
            $page = $totalPages;
        }
        if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $limit;

        $this->view->messages = $this->getSession()->getUser()->getOutboxMessages($offset, $limit);

        $this->view->totalPages = $totalPages;
        $this->view->currentPage = $page;
        $this->view->activeMenu = 'inbox_sent';

        $this->view->headTitle('Sent Messages');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

    public function notAllowedAction()
    {
        $this->view->activeMenu = 'inbox_sent';
    }

    public function viewAction()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/aptdeco/user.inbox.js'));
        $this->view->activeMenu = 'inbox_sent';
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $id = $this->getRequest()->getParam('id');

        $message = new Conversation_Model_Conversation_Message($id);

        $conversation = new Conversation_Model_Conversation($this->getRequest()->getParam('conversation_id'));

        if (!$message->getId() || !$message->getConversation()->getId()) {
            if (!$conversation->getId()) {
                $this->_redirect('user/inbox/index');
            } else {
                $this->view->conversation = $conversation;
            }
        } else {
            $message->setAsReadedFor($this->getSession()->getUser());
            $this->view->conversation = $message->getConversation();
        }

        if (false) {
            //@TODO: check participants
        }
        $this->view->session = $this->getSession();
    }

    public function createAction()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/aptdeco/user.inbox.js'));
        $this->view->activeMenu = 'inbox_sent';
        $userId = $this->getRequest()->getParam('id');
        if (!$this->getSession()->isLoggedIn()) {

            $this->_helper->redirector('index', 'login', 'user', array('redirect-success' => urlencode('user/inbox/create/id/' . $userId)));
            //$this->_redirect('user/login/index', );
        }

        $product = new Product_Model_Product;
        if($slug = $this->getParam('product')){
            $product->getBy_page_url($slug);
        }

        $user = new Application_Model_User($userId);
        $currentUser = $this->getSession()->getUser();

        if (!$currentUser->canSendMessages() || !$currentUser->canSendMessagesTo($user)) {
            $this->_forward('not-allowed');
        }
        
        $subject = $this->getSession()->getFormData('subject');
        if(null === $subject && $product->isExists()){
            $subject = $product->getTitle();
        } 
       
        $this->view->subject = $subject;
        $this->view->user = $user;
        $this->view->session = $this->getSession();

    }

    public function conversationPostAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $id = $this->getRequest()->getParam('id');
        $conversation = new Conversation_Model_Conversation($id);

        //@TODO:check permissions to post to the conversation
        //if (!$this->getSession()->getUser()->canSendMessagesTo($user)) {
        //$this->_helper->redirector('create', 'inbox', 'user', array('id' => $userId));
        //}



        $data = $this->getRequest()->getPost();

        $form = new User_Form_Conversation_Existing();

        if (($canSend = $this->getSession()->getUser()->canSendMessages()) && $form->isValid($data)) {
            $author = $this->getSession()->getUser();

            $message = new Conversation_Model_Conversation_Message();
            $message->setText($form->getValue('text'))->setAuthor($author);

            $conversation->addMessage($message)->save();

            if($errors = $conversation->getErrors()) {
               $this->getSession()->addFormData($data);
               $this->getSession()->addFormErrors($errors);
               $this->_helper->redirector('view', 'inbox', 'user', array('conversation_id' => $conversation->getId()));
            }

            foreach ($conversation->getUsers() as $user) {
                if ($user->getId() != $author->getId())
                    $this->sendMessageToRecipient($message, $user);
            }
            
            $message->getEventDispatcher()->trigger('response.message.seller', $message);

            $this->_helper->redirector('view', 'inbox', 'user', array('id' => $message->getId()));
        } else {
            $errors = $canSend ? $form->getFlatMessages() : array("Our spam filters have detected suspicious activity.
                If you think this is an error please contact us and we'll investigate immediately.");
            $this->getSession()->addFormErrors($errors);
        }
        $this->_helper->redirector('view', 'inbox', 'user', array('conversation_id' => $conversation->getId()));
    }

    public function createPostAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $author = $this->getSession()->getUser();

        $userId = $this->getRequest()->getParam('id');
        $user = new User_Model_User($userId);

        if (!$this->getSession()->getUser()->canSendMessagesTo($user)) {
            $this->_helper->redirector('create', 'inbox', 'user', array('id' => $userId));
        }

        $data = $this->getRequest()->getPost();

        try {
            $form = new User_Form_Message();

            if ($form->isValid($data)) {
                $conversation = new Conversation_Model_Conversation();
                $message = new Conversation_Model_Conversation_Message();
                $message->setText($form->getValue('text'))->setAuthor($author);
                                
                $conversation->setSubject($form->getValue('subject'))
                        ->addMessage($message)
                        ->addUser($user)
                        ->addUser($author);
                                                                
                if($conversation->save()) {
                    $this->getSession()->addMessage('success', sprintf('Message has been sent. <a href="%s">See Message</a>', Ikantam_Url::getUrl('user/inbox/view', array('conversation_id' => $conversation->getId()))));
                    $this->sendMessageToRecipient($message, $user);
                    $this->successRedirect($user);
                } else { 
                    $this->getSession()->addFormErrors($conversation->getErrors());
                                    
                }
                                        
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Exception $exception) {  throw $exception;
            $this->getSession()->addMessage('error', 'Unable to send message. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->getSession()->addFormData($data);

        //die('error');
        $this->failureRedirect($user);
    }

    public function sendMessageToRecipient($message, $user)
    {        
        if ($user->getMainEmail()) {
            $this->view->messageId = $message->getId();
            $this->view->message = $message;
            $this->view->author = $message->getAuthor();
        
        $output = $this->view->render('email_message_pending.phtml');

        $toSend = array(
            'email' => $user->getMainEmail(),
            'subject' => 'You have a new message: ' . $message->getConversation()->getSubject(),
            'body' => $output);

        $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
        $mail->send(null, true);
        }
        
        
    }

    protected function failureRedirect($user)
    {
        $redirect = urldecode($this->getRequest()->getParam('redirect-failure'));

        if (empty($redirect)) {
            $this->_helper->redirector('create', 'inbox', 'user', array('id' => $user->getId()));
        } else {
            $this->_redirect($redirect);
        }
    }

    protected function successRedirect($user)
    {
        $redirect = urldecode($this->getRequest()->getParam('redirect-success'));

        if (empty($redirect)) {
            $this->_helper->redirector('create', 'inbox', 'user', array('id' => $user->getId()));
        } else {
            $this->_redirect($redirect);
        }
    }

    public function deleteMessageAction()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $id = $this->getRequest()->getParam('id');

        $message = new Conversation_Model_Conversation_Message($id);

        if (!$message->getId() || $message->getRecipientId() !== $this->getSession()->getUserId()) {
            //@TODO:check user permissions
        }

        $message->setAsDeletedFor($this->getSession()->getUser());

        $this->_helper->redirector('view', 'inbox', 'user', array('conversation_id' => $message->getConversationId()));
    }

    public function deleteAction()
    {

        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $ids = (array) $this->getRequest()->getPost('messages');

        foreach ($ids as $id) {
            $message = new Conversation_Model_Conversation_Message($id);

            if (!$message->getId()/* || $message->getRecipientId() !== $this->getSession()->getUserId() */) {
                continue;
            }

            $message->setAsDeletedFor($this->getSession()->getUser());
        }
        if ($this->getRequest()->getParam('redirect')) {
            $this->_helper->redirector('sent', 'inbox', 'user');
        }
        $this->_helper->redirector('index', 'inbox', 'user');
    }

    public function readAction()
    {

        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $ids = $this->getRequest()->getPost('messages');

        foreach ($ids as $id) {
            $message = new Conversation_Model_Conversation_Message($id);

            if (!$message->getId()/* || $message->getRecipientId() !== $this->getSession()->getUserId() */) {
                continue;
            }

            $message->setAsReadedFor($this->getSession()->getUser());
        }
        if ($this->getRequest()->getParam('redirect')) {
            $this->_helper->redirector('sent', 'inbox', 'user');
        }
        $this->_helper->redirector('index', 'inbox', 'user');
    }

}
