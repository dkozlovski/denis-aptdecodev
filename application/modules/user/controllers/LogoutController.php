<?php

class User_LogoutController extends Ikantam_Controller_Front
{

	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
	}

	public function indexAction()
	{
		$this->getSession()->setUserId(null);
		$this->_redirect('user/login/index');
	}

}
