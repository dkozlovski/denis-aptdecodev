<?php

class User_ProfileController extends Ikantam_Controller_Front
{

    protected $_user    = null;
    private $_profile = array(
        'module'     => 'user',
        'controller' => 'profile',
        'action'     => 'account',
    );

    public function init()
    {
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }
        $this->view->headLink()->prependStylesheet(Ikantam_Url::getPublicUrl('css/jquery.Jcrop.css'));
        $this->view->session = $this->getSession();
        $this->_user         = $this->getSession()->getUser();
    }

//-----------------------------------------------------------------------------------------    
    private function isValidPostData($form)
    {
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                return true;
            }
        }

        return false;
    }

//-----------------------------------------------------------------------------------------    
    private function redirectToProfile()
    {
        $this->_helper->redirector($this->_profile['action'], $this->_profile['controller'], $this->_profile['module']);
    }

//-----------------------------------------------------------------------------------------    
    public function accountAction()
    {
        $this->view->user       = $this->_user;
        $this->view->session    = $this->getSession();
        $this->view->activeMenu = 'profile';
        $this->view->month      = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

        $user    = $this->getSession()->getUser();

        $addressForm = $this->_getAddressForm();

        $rqst = $this->getRequest();
        if ($rqst->isPost()) {

            $addressData = $rqst->getPost('pickup_address');

            $addressData['postcode']     = $addressData['postal_code'];
            $addressData['phone_number'] = $addressData['telephone_number'];

            if (!empty($addressData)) {
                if ($addressForm->isValid($addressData)) {
                    $address = $user->getPrimaryAddress();
                    $address->addData($addressData)
                            ->setUserId($this->getSession()->getUserId())
                            ->setIsPrimary(1)
                            ->save();

                    if ($user->getFullName() !== $address->getFullName()) {
                        $user->setFullName($address->getFullName())
                                ->save();
                    }
                }
            }
        }

        $userAddresses = $user->getAddresses();
        $this->view->addressesForm = array();
        $this->view->newAddressForm = array();

        foreach ($userAddresses as $_address) {
            /* @var $_address User_Model_Address */
            $_form = new User_Form_ShippingAddress();

            $_form->populate($_address->getData());
            $this->view->addressesForm[$_address->getId()]  = $_form;
        }

        $newForm = new User_Form_ShippingAddress();
        $this->view->newAddressForm[0] = $newForm;
        $this->view->addressForm = $addressForm;
        $this->view->newAddress = new User_Model_Address();
        $this->view->headTitle('Profile');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

//-----------------------------------------------------------------------------------------    
    /* public function editnameAction()
      {
      $rqst = $this->getRequest();
      if ($rqst->isPost()) {

      $addressData = $rqst->getPost('pickup_address');

      $addressData['postcode']     = $addressData['postal_code'];
      $addressData['phone_number'] = $addressData['telephone_number'];
      $addressForm                 = $this->_getAddressForm();

      if ($addressForm->isValid($addressData)) {
      $user = $this->getSession()->getUser();

      $address = $user->getPrimaryAddress();

      $address->addData($addressData)
      ->setUserId($this->getSession()->getUserId())
      ->setIsPrimary(1)
      ->save();


      if ($user->getFullName() !== $address->getFullName()) {
      $user->setFullName($address->getFullName())
      ->save();
      }
      } else {
      //print_d($addressForm->getErrors()); exit;
      }
      }

      $this->redirectToProfile();
      } */

//-----------------------------------------------------------------------------------------    
    public function editdateofbirthAction()
    {
        $form = new User_Form_Birthday();

        if ($this->isValidPostData($form)) {


            $this->_user->setDateOfBirth(date('Y-m-d', $form->getTime()))
                    ->save();
        } else {
            $this->getSession()->addFormErrors($form->getFlatMessages());
        }
        $this->redirectToProfile();
    }

    public function editemailAction()
    {
        $form = new User_Form_Email();

        if ($this->isValidPostData($form)) {
            $user = $this->getSession()->getUser();
            $user->changePrimaryEmail($form->getValue('primary'));


            $newEmail = $form->getValue('email');
            if (!empty($newEmail)) {
                try {
                    if ($newEmail !== $user->getEmail()) {
                        $user->addEmail($newEmail);
                    }
                } catch (Exception $ex) {
                    if ($ex->getCode() == 23000) {
                        $this->getSession()->addFormErrors(array('email' => $newEmail . ' is already exist.'));
                    } else {
                        $this->getSession()->addFormErrors(array('email' => 'Unable to save email address, please try again later.'));
                    }
                }
            }
        } else { //form not valid
            $this->getSession()->addFormErrors($form->getFlatMessages());
        }

        $this->redirectToProfile();
    }

//-----------------------------------------------------------------------------------------
    public function editphoneAction()
    {
        if ($this->getRequest()->isPost()) {
            $phone   = $this->getRequest()->getParam('phone');
            $primary = $this->getRequest()->getParam('primary');

            if ($primary) {
                $this->_user->changePrimaryPhone($primary);
            }

            if (!empty($phone)) {
                try {
                    $this->_user->addPhone($phone);
                } catch (Exception $ex) {
                    if ($ex->getCode() == 23000) {
                        $this->getSession()->addFormErrors(array('phone' => 'Phone number \'' . $phone . '\' is already exist.'));
                    } else {
                        $this->getSession()->addFormErrors(array('phone' => 'Unable to save phone number, please try again later.'));
                    }
                }
            }
        }
        $this->redirectToProfile();
    }

//-----------------------------------------------------------------------------------------

    public function editAddressAction() // ajax
    {
        $response = array(
            'success'  => false,
            'message'  => '',
            'template' => ''
        );

        if ($this->getRequest()->isPost()) {
            $form = new User_Form_ShippingAddress();
            if ($form->isValid($this->getRequest()->getPost('shipping_address'))) {
                $address = new User_Model_Address($form->getValue('id'));

                if ($this->getSession()->getUserId() == $address->getUserId() || !$form->getValue('id')) {
                    $address->addData($form->getValues());

                    if ($address->getBuildingType() == 'elevator') {
                        $address->setNumberOfStairs(null);
                    }

                    if (!$form->getValue('id')) {
                        $address->setIsPrimary(0)
                            ->setUserId($this->getSession()->getUserId());
                    }

                    $address->setCountryCode('US')
                        ->save();

                    $form = new User_Form_ShippingAddress();
                    $form->populate($address->getData());

                    $this->view->addresses = array($address);
                    $this->view->addressesForm = array();
                    $this->view->addressesForm[$address->getId()] = $form;
                    $response['success'] = true;
                    $response['template']  = $this->view->render('profile/account/addresses_template.phtml');
                } else {
                    $response['message'] = 'Please check your login details';
                }

            } else {
                $response['message'] = 'Looks like some fields are missing or left blank';
            }
        }
        $this->responseJSON($response)
            ->sendResponse();
        exit;
    }

    public function setPrimaryAddressAction()
    {
        $address = new User_Model_Address($this->getParam('address_id'));
        $response = array('success' => false);

        if ($this->getSession()->getUserId() == $address->getUserId()) {
            $address->setPrimary();
            $response = array('success' => true);
        }

        $this->responseJSON($response)
            ->sendResponse();
        exit;
    }

    public function deleteAddressAction()
    {
        $address = new User_Model_Address($this->getParam('address_id'));
        $response = array('success' => false);

        if ($this->getRequest()->isXmlHttpRequest() && $this->getSession()->getUserId() == $address->getUserId()) {
            $isPrimary = $address->getIsPrimary();
            $address->delete();
            if ($isPrimary) {
                $addressCollection = new User_Model_Address_Collection();
                $addressCollection->getByUserId($this->getSession()->getUserId());

                $items = $addressCollection->getItems();
                if (count($items)) {
                    /* @var $firstAddress User_Model_Address */
                    $firstAddress = reset($items);
                    $firstAddress->setIsPrimary(1)
                        ->save();
                }

            }
            $response['success'] = true;
        } else {
            $response['message'] = 'Please check your login details';
        }

        $this->responseJSON($response)
            ->sendResponse();
        exit;
    }


    public function editdetailsAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->_user->setDetails($this->getRequest()->getParam('details'))
                    ->save();
        }

        $this->redirectToProfile();
    }

//-----------------------------------------------------------------------------------------

    public function changepasswordAction()
    {
        //user registered via facebook does not have a password
        if (!$this->_user->getEmail()) {
            $this->redirectToProfile();
        }

        $post = $this->getRequest()->getPost();

        $form = new User_Form_NewPassword();

        if ($form->isValid($post) && $this->isValidPostData($form)) {
            if ($this->_user->isPasswordValid($form->getValue('oldpassword'))) {
                $this->_user
                        ->setPassword($this->_user->hash($form->getValue('newpassword')))
                        ->save();
            } else {
                $this->getSession()->addFormErrors(array('oldpassword' => 'Wrong password!'));
            }
        } else {
            $this->getSession()->addFormErrors($form->getFlatMessages());
        }



        $this->redirectToProfile();
    }

    protected function _getAddressForm()
    {
        $form = new Product_Form_AddressSubform();
        $form->removeElement('amount');
        $form->removeElement('is_active');
        $form->setAction(Ikantam_Url::getUrl('user/profile/account'));


        $form->addDecorator('Form')
                ->setAttrib('class', 'apt-form')
                ->addElement(new Zend_Form_Element_Button('Save', array(
                    'class' => 'save-basic-info m-0',
                    'type'  => 'submit',
        )));

        $form->getElement('Save')->removeDecorator('DtDdWrapper');

        $user                  = $this->getSession()->getUser();
        $address               = $user->getPrimaryAddress();
        $formData              = $address->getData();
        $formData['full_name'] = $user->getFullName();

        $formData['telephone_number'] = @$formData['phone_number'];
        $formData['postal_code']      = @$formData['postcode'];

        $form->populate($formData);


        return $form;
    }

}