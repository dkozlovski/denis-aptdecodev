<?php

/**
 * Class User_EmailsAndAlertsController
 * @method User_Model_Session getSession()
 */
class User_EmailsAndAlertsController extends Ikantam_Controller_Front
{

    public function init()
    {

        if (!$this->getSession()->isLoggedIn()) {
            $this->getSession()->setRefererRedirectUrl(Ikantam_Url::getUrl('user/inbox'));
            $this->_redirect('user/login/index');
        }
        $this->view->session = $this->getSession();

    }

    public function indexAction()
    {
        //$this->_helper->_layout->setLayout('layout3');
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('css/additional.css'));
        $user = $this->getSession()->getUser();
        $form = new User_Form_EmailsAndAlerts();


        $personalSettings = array(
            'notification_newsletter',
            'notification_product_alerts',
            'notification_cart_and_wishlist',
            'notification_promotions'
        );

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                foreach ($personalSettings as $_option) {
                    $user->personalSettings($_option, $form->getValue($_option));
                }
            }
        } else {
            $values = array();
            foreach ($personalSettings as $_option) {
                $_value = $user->personalSettings($_option);
                if ($_value !== null) {
                    $values[$_option] = $_value;
                }
            }

            $form->populate($values);
        }

        $this->view->form = $form;
        $this->view->activeMenu   = 'profile';
        $this->view->headTitle('Emails and Alerts');
        $this->view->headMeta()->appendName('description', 'AptDeco user dashboard. Manage your used furniture listings and purchases from any device.');
    }

}
