<?php

class User_LoginController extends Ikantam_Controller_Front
{

    //From config| see init()
    protected $_appId;
    protected $_appSecret;
    //-----------------------
    protected $_defaultSuccessRedirect         = 'home';
    protected $_defaultFailureRedirect         = 'user/login/index';
    protected $_defaultSuccessCheckoutRedirect = 'checkout/onestep/index';
    protected $_scope                          = 'email,user_birthday';

    public function init()
    {
        if ($this->getSession()->isLoggedIn()) {
            $this->_redirect($this->getSuccessRedirect());
        }

        //$cfg              = $this->getXmlConfig('additional_settings', 'facebook');
        $cfg              = Zend_Registry::get('config')->facebook;
        $this->_appId     = $cfg->appId;
        $this->_appSecret = $cfg->appSecret;
    }

    public function indexAction()
    {
        $this->view->redirect    = $this->getRequest()->getParam('redirect-success');
        $this->view->session     = $this->getSession();
        $this->view->appId       = $this->_appId;
        $this->view->redirectUrl = $this->getRedirectUrl();
        $this->view->scope       = $this->_scope;

        $this->view->headTitle('Sign in to your account');
        $this->view->headMeta()->appendName('description', 'Sign in to your AptDeco account, your destination marketplace for buying and selling used furniture in NYC.');
    }

    /**
     * Used to check if a logged user has parameters in his session which need to be moved to DB
     * (personal settings)
     */
    public function __destruct()
    {
        $user = new \User_Model_User($this->getSession()->getUserId()); //!
        if ($user->isExists() && $this->getSession()->hasUserSettings()) {
            foreach ($this->getSession()->getUserSettings() as $name => $value) {
                $user->personalSettings($name, $value);
            }
            $this->getSession()->unsUserSettings();
        }
    }

    /**
     * Process login form
     */
    public function postAction()
    {
        $loginData = $this->getRequest()->getPost();

        try {
            $form = new User_Form_Login();

            if ($form->isValid($loginData)) {
                $this->getSession()->authenticate($form->getValues());
                $isPublishProductRedirect = $this->getSession()->activeData('publishProductRedirect', null, true);

                $eventDispatcher = new Application_Model_Event_Dispatcher();
                $eventDispatcher->trigger('assign.unassigned.created.product.after.user.authorize', $this, array());

                if ($isPublishProductRedirect === 'Save') {
                    $this->_helper->redirector('list', 'sale', 'user');
                } elseif ($isPublishProductRedirect === true) {
                    $this->getSession()->addMessage('success', $this->getSession()->activeData('_success_product_publish_message'));
                    $this->_helper->redirector('success', 'add', 'product');
                }

                $this->_redirect($this->getSuccessRedirect());
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
            }
        } catch (Ikantam_Exception_Auth $exception) {
            $this->getSession()->addMessage('error', $exception->getMessage());
        } catch (Exception $exception) {
            $this->getSession()->addMessage('error', 'Unable to log in. Please, try again later');
            $this->logException($exception, $logParams = false);
        }

        $this->getSession()->addFormData($loginData, 'email');
        $this->getSession()->addMessage('signin', 'fail sign in');
        $redirectUrl = Ikantam_Url::getUrl($this->getFailureRedirect(), array('redirect-success' => $this->getParam('redirect-success')));
        $this->_redirect($redirectUrl);
    }

    public function asyncAction()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            // Login via Facebook
            if ($this->getParam('accessToken')) {
                // get facebook user model or create if not exist
               $facebookUser = $this->facebookUser($this->getParam('accessToken'));
                $user = new \User_Model_User();
                $user->getActiveById($facebookUser->getUserId());
                if ($user->isExists()) {
                    $this->getSession()->setUserId($user->getId());

                    $eventDispatcher = new Application_Model_Event_Dispatcher();
                    $eventDispatcher->trigger('assign.unassigned.created.product.after.user.authorize', $this, array());

                    $this->getSession()->getCart()->checkItems($user->getId()); //clear cart from own products
                    $result['success'] = true;
                }

            } else { // Login via Aptdeco
                $form = new User_Form_Login();
                $loginData = $this->getRequest()->getPost();
                $result = array(
                    'success' => false,
                );
                if ($form->isValid($loginData)) {
                    try{
                        $this->getSession()->authenticate($form->getValues());
                        if ($this->getSession()->isLoggedIn()) {
                            $result['success'] = true;
                        }
                    } catch(Ikantam_Exception_Auth $exception) {
                        $result['message'] = $exception->getMessage();
                    }
                } else {
                    $result['message'] = implode(' ', $form->getFlatMessages());
                }
            }

            if ($result['success']) {
                if (!isset($user)) {
                    $user = new \User_Model_User($this->getSession()->getUserId());
                }
                $result['user_name'] = $user->getFirstName();
                $result['header_html'] = $this->view->partial('header.phtml', 'default');
            }

            $this->_helper->json($result);
        }

        $this->_helper->show404();
    }

    protected function getSuccessRedirect()
    {
        $this->_helper->redirectAfter->initRedirect(); //redirect to page where we called $this->_helper->redirectAfter()
        //or do nothing if we didn't call it 

        if ($url = $this->getSession()->getRefererRedirectUrl()) {
            $this->getSession()->unsRefererRedirectUrl();
            return $url;
        }
        $url = $this->getRequest()->getParam('redirect-success');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultSuccessRedirect;
    }

    protected function getFailureRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-failure');
        if (!empty($url)) {
            return urldecode($url);
        }
        return $this->_defaultFailureRedirect;
    }

    protected function getRedirectUrl()
    {
        return urlencode(Ikantam_Url::getUrl('user/login/facebook', array('redirect-success' => $this->getParam('redirect-success'))));
    }

    public function facebookAction()
    {
        $code             = $this->getRequest()->getParam('code');
        $isCheckoutSignIn = $this->getRequest()->getParam('checkout_signin', false);
        $redirectSuccess  = $this->getRequest()->getParam('redirect-success');

        $rUrl = $this->getRedirectUrl();

        if ($isCheckoutSignIn) {
            $this->getSession()->setFlashData('checkout_redirect', true);
            $rUrl = urlencode(Ikantam_Url::getUrl('user/login/facebook', array('checkout_signin' => 1)));
        }

        if ($this->getRequest()->getParam('publish_product', false)) {
            $this->getSession()->setFlashData('publish_product_redirect', true);
            $this->getSession()->setFlashData('publish_product_mode', $this->getParam('publish_mode'));
        }

        if ($redirectSuccess) {
            $this->getSession()->setFlashData('redirect-success', $redirectSuccess);
        }

        if (empty($code)) {
            $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
            $dialog_url        = "https://www.facebook.com/dialog/oauth?client_id="
                    . $this->_appId . "&redirect_uri=" . $rUrl . "&state="
                    . $_SESSION['state'];

            $this->_redirect($dialog_url);
        } else {
            $token_url = "https://graph.facebook.com/oauth/access_token?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s";
            $tokenUrl  = sprintf($token_url, $this->_appId, $rUrl, $this->_appSecret, $code);

            $response = file_get_contents($tokenUrl);
            $params   = null;
            parse_str($response, $params);

            if (isset($params['access_token'])) {
                $accessToken = $params['access_token'];

                $graph_url = "https://graph.facebook.com/me?access_token=" . $accessToken;

                $user = json_decode(file_get_contents($graph_url));


                $facebookUser = new User_Model_FacebookUser();
                $facebookUser->getByFacebookUserId($user->id);

                if (!$facebookUser->getUserId()) {

                    if (isset($user->name, $user->birthday, $user->id, $user->email)) {

                        $facebookUser = $this->facebookUser($accessToken, $user);

                        $this->getSession()->getCart()->checkItems($facebookUser->getUserId()); //clear cart from own products

                        $eventDispatcher = new Application_Model_Event_Dispatcher();
                        $eventDispatcher->trigger('assign.unassigned.created.product.after.user.authorize', $this, array());

                        if ($this->getSession()->getFlashData('checkout_redirect')) {

                            $this->redirect($this->_defaultSuccessCheckoutRedirect);
                        } elseif ($this->getSession()->getFlashData('publish_product_redirect')) {
                            $_redirect = ($this->getSession()->getFlashData('publish_product_mode') === 'save') ? 'user/sale/list' : 'product/add/success';
                            $this->redirect($_redirect);
                        } elseif ($_redirectSuccess = $this->getSession()->getFlashData('redirect-success')) {
                            $this->redirect(urldecode($_redirectSuccess));
                        } else {
                            $this->_redirect($this->getSuccessRedirect());
                        }
                    }
                } else {
                    $user2 = new User_Model_User($facebookUser->getUserId());

                    if ($user2->getId()) {

                        $this->getSession()->getUser()->getActiveById($user2->getId());

                        $this->getSession()->setUserId($user2->getId());

                        $this->getSession()->getCart()->checkItems($facebookUser->getUserId()); //clear cart from own products

                        $eventDispatcher = new Application_Model_Event_Dispatcher();
                        $eventDispatcher->trigger('assign.unassigned.created.product.after.user.authorize', $this, array());

                        if ($this->getSession()->getFlashData('checkout_redirect')) {
                            $this->redirect($this->_defaultSuccessCheckoutRedirect);
                        } elseif ($this->getSession()->getFlashData('publish_product_redirect')) {
                            $_redirect = ($this->getSession()->getFlashData('publish_product_mode') === 'save') ? 'user/sale/list' : 'product/add/success';
                            $this->redirect($_redirect);
                        } else {
                            $this->_redirect($this->getSuccessRedirect());
                        }
                    }
                }
            }
        }

        $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
        $dialog_url        = "https://www.facebook.com/dialog/oauth?client_id="
                . $this->_appId . "&redirect_uri=" . $rUrl . "&state="
                . $_SESSION['state'];

        $this->_redirect($dialog_url);
    }

    protected function facebookUser($accessToken, $user = null)
    {
        if (!$user) {
            $graph_url = "https://graph.facebook.com/me?access_token=" . $accessToken;
            $user = json_decode(file_get_contents($graph_url));
        }

        $facebookUser = new User_Model_FacebookUser();
        $facebookUser->getByFacebookUserId($user->id);

        if (!$facebookUser->getUserId()) {
            $fullName = $user->name;

            $this->getSession()->registerFacebook($fullName, $user->birthday);
            $this->getSession()->getUser()->addEmail($user->email, true);

            $src = "https://graph.facebook.com/me/picture?access_token=" . $accessToken . '&width=300&height=300';

            $upl      = new Ikantam_Image();
            $filename = $upl->downloadAvatar($src, $this->getSession()->getUserId());

            $avatar = new Application_Model_Avatar();

            $avatar->setUserId($this->getSession()->getUserId())
                ->setTitle($filename)
                ->setS3Path($filename)
                ->save();

            $avatar->setIsAvailable(1)->save();

            $fql_query_url    = 'https://graph.facebook.com/fql?q=SELECT+current_location+FROM+user+WHERE+uid=me()&access_token=' . $accessToken;
            $fql_query_result = file_get_contents($fql_query_url);
            $fql_query_obj    = json_decode($fql_query_result, true);

            if (is_array($fql_query_obj) && isset($fql_query_obj['data'][0]['current_location'])) {
                $loc = $fql_query_obj['data'][0]['current_location'];

                $countryCode = null;
                $stateCode   = null;
                if (isset($loc['country']) && isset($loc['country']) == 'United States') {
                    $countryCode = 'US';

                    if (isset($loc['state'])) {
                        $stateTitle = trim($loc['state']);

                        $state = new Application_Model_State();
                        $state->getByTitle($stateTitle);
                        if ($state->getStateCode()) {
                            $stateCode = $state->getStateCode();
                        }
                    }
                }

                $city = null;
                if (isset($loc['city'])) {
                    $city = $loc['city'];
                }

                $userAddress = new User_Model_Address();
                $userAddress->setFullName($fullName)
                    ->setUserId($this->getSession()->getUserId())
                    ->setCountryCode($countryCode)
                    ->setCity($city)
                    ->setState($stateCode)
                    ->setIsPrimary(1)
                    ->save();
            }

            $facebookUser->setUserId($this->getSession()->getUserId())
                ->setFacebookId($user->id)
                ->save();
        }

        return $facebookUser;
    }

}
