<?php

class Cart_Model_Cart2 extends Application_Model_Abstract
{

    protected $_entityClass     = 'Core_Model_Entity_Cart';
    protected $_entity          = null;
    protected $_items           = array();
    protected $_totalCollectors = array();
    protected $_user            = null;

    public function save()
    {
        $this->_beforeSave();

        $this->getEntity()->save();

        $this->_afterSave();

        return $this;
    }

    protected function _beforeSave()
    {
        
    }

    protected function _afterSave()
    {
        foreach ($this->getItems() as $item) {
            $item->save();
        }
    }

    /**
     * 
     * @param User_Model_User $user
     * @return \Cart_Model_Cart2
     */
    public function setUser(User_Model_User $user)
    {
        $this->_user = $user;
        return $this;
    }

    /**
     * 
     * @return User_Model_User
     */
    public function getUser()
    {
        if (!$this->_user) {
            $user = new User_Model_User($this->getUserId());
            $this->setUser($user);
        }
        return $this->_user;
    }

    protected function _getConfig()
    {
        return Zend_Registry::get('config')->cart->totals->collectors;
    }

    public function __construct($id = null)
    {
        $this->setTotalCollectors($this->_getConfig());

        if ($id) {
            $this->getById($id);
        }

        return $this;
    }

    /**
     * 
     * @return \Core_Model_Entity_Abstract
     */
    public function getEntity()
    {
        if (!$this->_entity) {
            $this->_entity = new $this->getEntityClass();
        }
        return $this->_entity;
    }

    /**
     * 
     * @param Core_Model_Entity_Abstract $entity
     * @return \Cart_Model_Cart2
     */
    public function setEntity(Core_Model_Entity_Abstract $entity)
    {
        $this->_entity = $entity;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getEntityClass()
    {
        return $this->_entityClass;
    }

    /**
     * 
     * @param string $entityClass
     * @return \Cart_Model_Cart2
     */
    public function setEntityClass($entityClass)
    {
        $this->_entityClass = $entityClass;
        return $this;
    }

    /**
     * 
     * @param type $type
     * @return mixed
     * @throws Exception
     */
    public function getTotalCollectors($type = null)
    {
        if (is_null($type)) {
            return $this->_totalCollectors;
        }

        if (!isset($this->_totalCollectors[$type])) {
            throw new Exception(sprintf('Totals collector ot type %s does not exist', $type));
        }

        return $this->_totalCollectors[$type];
    }

    /**
     * 
     * @param array $totalCollectors
     * @return \Cart_Model_Cart2
     */
    public function setTotalCollectors(array $totalCollectors)
    {
        foreach ($totalCollectors as $type => $totalCollector) {
            $this->addTotalCollector($type, $totalCollector);
        }
        return $this;
    }

    /**
     * 
     * @param string $type
     * @param type $totalCollector
     * @return Cart_Model_Cart2
     */
    public function addTotalCollector($type, $totalCollector)
    {
        $this->_totalCollectors[$type] = $totalCollector;
        return $this;
    }

    /**
     * 
     * @return Cart_Model_Cart2
     */
    public function collectTotals2()
    {
        foreach ($this->getTotalCollectors() as $dbField => $totalCollector) {
            $amount = $totalCollector->setCart($this)->collectTotals();
            $this->setData($dbField, $amount);
        }

        return $this;
    }

    public function addCoupon($code)
    {
        $coupon = new Cart_Model_Coupon();
        $coupon->getByCode($code);

        if (!$coupon->getId() || !$coupon->isValid($this->getUserId())) {
            return false;
        }

        $this->setCouponId($coupon->getId());
        $this->collectTotals2()->save();

        return true;
    }

    public function deleteCoupon()
    {
        $this->setCouponId(null);
        $this->collectTotals2()->save();
        return $this;
    }

    public function addItem2($item)
    {
        $this->_items[$item->getId()] = $item;
        $this->collectTotals2()->save();
        return $this;
    }

    public function deleteItem($item)
    {
        $item->setIsDeleted();
        $this->unset($this->_items[$item->getId()]);
        $this->collectTotals2()->save();
        return $this;
    }

    public function setFreeShipping()
    {
        //...
        $this->collectTotals2()->save();
        return $this;
    }

    public function getItems()
    {
        if (!$this->_items) {
            foreach ($this->getEntity()->getItems() as $itemEntity) {
                $item = new Cart_Model_Cart_Item();
                $this->addItem($item->setEntity($itemEntity));
            }
        }

        return $this->_items;
    }

    /*     * ***************************** */

    protected $_itemCollection = null;

    /**
     * Retrieve cart items array
     *
     * @return array
     */
    public function getAllItems()
    {
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->getIsDeleted()) {
                $items[] = $item;
            }
        }
        return $items;
    }

    public function getItemsCount()
    {
        return count($this->getAllItems());
    }

    /**
     * Retrieve cart items array for checkout
     *
     * @return array
     */
    public function getAllCheckoutItems()
    {
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->getIsDeleted() && $item->getIsUsedOnCheckout()) {
                $items[] = $item;
            }
        }
        return $items;
    }

    /**
     * Retrieve cart items array for checkout
     *
     * @return array
     */
    public function getAllDeliveredItems()
    {
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->getIsDeleted() && $item->getIsUsedOnCheckout() && $item->getShipping()) {
                $items[] = $item;
            }
        }
        return $items;
    }

    /**
     * Retrieve cart items collection
     *
     * @return Cart_Model_Item_Collection
     */
    public function getItemsCollection()
    {
        if (!$this->_items) {
            $this->_items = new Cart_Model_Item_Collection();
            $this->_items->getByCartId($this->getId());
        }

        return $this->_items;
    }

    /**
     * Collect totals
     *
     * @return Cart_Model_Cart
     */
    public function collectTotals()
    {
        $this->setSubtotal(0);
        $this->setTotal(0);

        foreach ($this->getAllItems() as $item) {

            $this->setSubtotal($this->getSubtotal() + ($item->getProduct()->getPrice() * $item->getQty(1)));
            $this->setTotal($this->getTotal() + ($item->getProduct()->getPrice() * $item->getQty(1)));
        }

        $this->save();

        return $this;
    }

    public function getGrandTotal()
    {
        return $this->getData('total_amount');
    }

    public function getSubtotal()
    {
        return $this->getData('subtotal_amount');
    }

    /* $items: array(id => amount...) */

    public function prepareItemsForCheckout($items)
    {
        $items = (array) $items;

        foreach ($this->getAllItems() as $item) {
            if (key_exists($item->getId(), $items)) {

                $item->setIsUsedOnCheckout(1)
                        ->setQty($items[$item->getId()])
                        ->save();
            } else {
                $item->setIsUsedOnCheckout(0)
                        ->setQty($items[$item->getId()])
                        ->save();
            }
        }
    }

    public function prepareItemsDelivery($items)
    {
        $items = (array) $items;

        foreach ($this->getAllCheckoutItems() as $item) {
            $product = $item->getProduct();

            if ($product->getIsDeliveryAvailable() && $product->getIsPickUpAvailable()) {
                $shippingMethod = isset($items[$item->getId()]) ? $items[$item->getId()] : '';

                if ($shippingMethod == 'delivery' || $shippingMethod == 'pick-up') {
                    $item->setShipping($items[$item->getId()])->save();
                } else {
                    $item->setShipping(null)->save();
                }
            } elseif ($product->getIsDeliveryAvailable()) {
                $item->setShipping('delivery')->save();
            } elseif ($product->getIsPickUpAvailable()) {
                $item->setShipping('pick-up')->save();
            }
        }
    }

    protected function _loadItems()
    {
        /*
          if (!$this->getId()) {
          throw new Exception('Can\'t load collection before saving.');
          }

          $items = new Cart_Model_Item_Collection();
          $this->_itemCollection = $items->getByCartId($this->getId());
         */

        $items = new Cart_Model_Item_Collection();

        if ($this->getId()) {
            $items->getByCartId($this->getId());
        }

        $this->_itemCollection = $items;
    }

    private function calculateTotal()
    {
        $products = new Product_Model_Product_Collection();
        $products->getProductsInCart($this); //$this
        $total    = 0;

        foreach ($products->getItems() as $product) {
            $total += $product->getPrice();
        }

        $this->setTotal($total)->save();

        return $this;
    }

    public function getById($id)
    {
        parent::getById($id);
        $this->_loadItems();
    }

    /**
     * @return Cart_Model_Item_Collection 
     */
    public function getItemCollection()
    {
        return $this->_itemCollection;
    }

    /**
     *  Add new item to cart
     * @param  mixed $param;
     * @example
     *      $param = array('id'=>$product_id, 'shipping'=>'pick-up');
     *      $cart->addItem($param);
     * @example
     *      $item = new Item_Class;
     *      $cart->addItem($item);
     * 
     */
    public function addItem($param)
    {
        if (!$this->_itemCollection)
            $this->_loadItems();
        if ($param instanceof Cart_Model_Item) {
            $param->save();
            $this->_itemCollection->addItem($param);
        } elseif (isset($param['id']) && key_exists('shipping', $param)) {

            $item = new Cart_Model_Item();

            $product = new Product_Model_Product((int) $param['id']);


            if (!$product->getIsPublished())
                return $this;
            if (key_exists($product->getId(), $this->getProducts()->getItems()))
                return $this;

            $shipping = '';
            if ($product->getShipping() == $product::SHIPPING_DELIVERY) {
                switch ($param['shipping']) {
                    case '1':
                        $shipping = $product::SHIPPING_PICK_UP;
                        break;

                    case '2':
                        $shipping = $product::SHIPPING_DELIVERY;
                        break;

                    case $product::SHIPPING_DELIVERY:
                        $shipping = $product::SHIPPING_DELIVERY;
                        break;

                    case $product::SHIPPING_PICK_UP:
                        $shipping = $product::SHIPPING_PICK_UP;
                        break;

                    default:
                        $shipping = $product::SHIPPING_PICK_UP;
                        break;
                }
            } else {
                $shipping = $product::SHIPPING_PICK_UP;
            }

            $item->setProductId($param['id'])->setCartId($this->getId())->setShipping($shipping)->save();
            $this->_itemCollection->addItem($item);
        }

        $this->calculateTotal();
        return $this;
    }

    /**
     * Add product to cart
     * @param mixed  $param 
     * @param string $shipping
     * @param int $qty
     * 
     * @example
     *      $product = new Product_Class;
     *      $cart->addProduct($product, $product::SHIPPING_DELIVERY);
     * @example
     *      $param = $product_id;
     *      $cart->addProduct($param);
     */
    protected function isProductInCart($product)
    {
        return $this->_getbackend()->isProductInCart($this, $product->getId());
    }

    public function addProduct($product, $qty = 1)
    {
        if (!$this->isProductInCart($product)) {
            $item = new Cart_Model_Item();

            if ($qty > $product->getQty(1)) {
                $qty = $product->getQty(1);
            } elseif ($qty < 1) {
                $qty = 1;
            }

            $item->setCartId($this->getId())
                    ->setProductId($product->getId())
                    ->setQty($qty)
                    ->save();
            return $item;
        } else {
            throw new Exception(sprintf('<i class="apt-i-warning orange"></i><p><a href="%s">%s</a> is already in cart.</p>', $product->getProductUrl(), $product->getTitle()));
        }


        return $this;



        /* if ($param instanceof Product_Model_Product) {
          if (key_exists($param->getId(), $this->getProducts()->getItems()))
          return $this;
          $item = new Cart_Model_Item();

          if ($param->getShipping() == $param::SHIPPING_PICK_UP) {
          $shipping = $param::SHIPPING_PICK_UP;
          } else {
          $shipping = ($shipping) ? $shipping : $param::SHIPPING_PICK_UP;
          }

          $item->setCartId($this->getId())->setProductId($param->getId())->setShipping($shipping);
          $this->addItem($item);
          return $this;
          }

          if (!is_array($param) && (int) $param > 0) {
          $this->addItem(array('id' => $param, 'shipping' => $shipping));
          return $this;
          }

          return $this; */
    }

    /**
     * @return Product_Model_Product_Collection
     */
    public function getProducts()
    {
        $products = new Product_Model_Product_Collection();
        return $products->getProductsInCart($this); //$this   
    }

    protected function removeItemFromCollection($key)
    {
        $this->_itemCollection->removeItem($key);
        $item = new Cart_Model_Item($key);
        $item->delete();
        $this->calculateTotal();
        return $this;
    }

    public function removeItemByProduct($product_id)
    {

        foreach ($this->getItemCollection()->getItems() as $key => $item) {
            if ($item->getProductId() == $product_id) {
                $this->removeItemFromCollection($key);
                return $this;
            }
        }
        return $this;
    }

    public function getItemByProductId($productId)
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProductId() == $productId) {
                return $item;
            }
        }

        return new Cart_Model_Item();
    }

    public function count()
    {
        return count($this->getAllItems());
    }

    public function getAllItemsQtySum()
    {
        $qty = $this->getItemsCollection()->getColumn('qty');
        return array_sum($qty);
    }

    public function checkItems($userId)
    {
        $this->_getbackend()->checkItems($userId, $this->getId());
        $this->setUserId($userId)->save();
        return $this;
    }

}
