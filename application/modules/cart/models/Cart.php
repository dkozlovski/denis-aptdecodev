<?php
/**
 * Class Cart_Model_Cart
 * @method int getIsNotified()
 * @method int getIsDeleted()
 * @method getCouponId()
 *
 * @method Cart_Model_Cart setFreeShipping($freeShipping)
 * @method Cart_Model_Cart setUserId(int $userId)
 * @method Cart_Model_Cart setCouponId($couponId)
 * @method Cart_Model_Cart setTotalAmount($totalAmount)
 */
class Cart_Model_Cart extends Application_Model_Abstract
{

    protected $_backendClass     = 'Cart_Model_Cart_Backend';
    protected $_itemCollection   = null;
    protected $_items;
    protected $_user;
    protected $_totalsCollectors = array();
    protected $_shippingAddress;
    protected $_billingAddress;
    protected $_itemsForCheckout;
    protected $_itemsForDelivery;
    protected $_itemsForPickup;

    protected function _beforeSave()
    {
        if (!$this->getData('no_reset_notified')) {
            $this->setData('is_notified', 0);
        }

        $this->setTotalAmount($this->getSubtotalAmount() + $this->getShippingAmount() - $this->getDiscountAmount() + $this->getTransactionAmount());
    }

    public function getItems()
    {
        $items = new Cart_Model_Item_Collection();
        $items->getCartItems($this->getId());
        return $items;
    }

    public function reload()
    {
        $this->_itemsForCheckout = null;
        $this->_itemsForDelivery = null;
        $this->_itemsForPickup   = null;
        
        return $this;
    }

    public function getItemsForCheckout()
    {
        if (!$this->_itemsForCheckout) {
            $this->_itemsForCheckout = new Cart_Model_Item_Collection();
            $this->_itemsForCheckout->getItemsForCheckout($this->getId());
        }

        return $this->_itemsForCheckout;
    }

    public function getItemsForDelivery()
    {
        if (!$this->_itemsForDelivery) {
            $this->_itemsForDelivery = new Cart_Model_Item_Collection();
            $this->_itemsForDelivery->getItemsForDelivery($this->getId());
        }

        return $this->_itemsForDelivery;
    }

    public function getItemsForPickup()
    {
        if (!$this->_itemsForPickup) {
            $this->_itemsForPickup = new Cart_Model_Item_Collection();
            $this->_itemsForPickup->getItemsForPickup($this->getId());
        }

        return $this->_itemsForPickup;
    }

    public function hasItems()
    {
        return ($this->getItems()->getSize() > 0);
    }

    public function hasItemsForCheckout()
    {
        return ($this->getItemsForCheckout()->getSize() > 0);
    }

    public function hasItemsForDelivery()
    {
        return ($this->getItemsForDelivery()->getSize() > 0);
    }

    public function hasItemsForPickup()
    {
        return ($this->getItemsForPickup()->getSize() > 0);
    }

    public function getUser()
    {
        if (!$this->_user) {
            $this->_user = new User_Model_User($this->getUserId());
        }
        return $this->_user;
    }

    public function __construct($id = null)
    {
        $totalsCollectors = \Zend_Registry::get('config')->cart->totals->collectors;

        foreach ($totalsCollectors as $type => $totalsCollector) {
            $this->_totalsCollectors[$type] = new $totalsCollector;
        }

        if ($id) {
            $this->getById($id);
        }

        return $this;
    }

    public function getTotalsCollectors($type = null)
    {
        if (is_null($type)) {
            return $this->_totalsCollectors;
        }

        if (!isset($this->_totalsCollectors[$type])) {
            throw new Exception(sprintf('Totals collector ot type %s does not exist', $type));
        }

        return $this->_totalsCollectors[$type];
    }

    /**
     * Retrieve cart items array
     *
     * @return array
     */
    public function getAllItems()
    {
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->getIsDeleted()) {
                $items[] = $item;
            }
        }
        return $items;
    }

    public function getItemsCount()
    {
        return count($this->getAllItems());
    }

    /**
     * Retrieve cart items array for checkout
     *
     * @return array
     */
    public function getAllCheckoutItems()
    {
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->getIsDeleted() && $item->getIsUsedOnCheckout()) {
                $items[] = $item;
            }
        }
        return $items;
    }

    /**
     * Retrieve cart items array for checkout
     *
     * @return array
     */
    public function getAllDeliveredItems()
    {
        $items = array();
        foreach ($this->getItemsCollection() as $item) {
            if (!$item->getIsDeleted() && $item->getIsUsedOnCheckout() && $item->getShipping()) {
                $items[] = $item;
            }
        }
        return $items;
    }

    /**
     * Retrieve cart items collection
     *
     * @return  Cart_Model_Item_Collection
     */
    public function getItemsCollection()
    {
        if (!$this->_items) {
            $this->_items = new Cart_Model_Item_Collection();
            $this->_items->getByCartId($this->getId());
        }

        return $this->_items;
    }

    /**
     * 
     * @return \Cart_Model_Cart
     */
    public function collectTotals()
    {
        foreach ($this->getTotalsCollectors() as $dbField => $totalsCollector) {
            $this->setData($dbField, $totalsCollector->setCart($this)->collectTotals());
        }

        return $this;
    }

    public function collectShippingByAddress($address)
    {
        $shippingAmount = $this->getTotalsCollectors('shipping_amount')->setCart($this)->collectTotalsByAddress($address);
        $this->setData('shipping_amount', $shippingAmount);
        return $this;
    }

    public function getCoupon()
    {
        $coupon = new Cart_Model_Coupon();
        $coupon->getById($this->getCouponId());
        return $coupon;
    }

    public function addCoupon($code)
    {
        $coupon = new Cart_Model_Coupon();
        $coupon->getByCode($code);

        if (!$coupon->isValid($this->getUserId())) {
            $this->deleteCoupon();
            return $coupon;
        }

        $this->setCouponId($coupon->getId());

        if ($coupon->getType() == 'shipping') {
            if (!$coupon->getCategoryId()) {
                $this->setFreeShipping(true);
            }
        }

        $this->collectTotals()->save();

        return $coupon;
    }

    public function deleteCoupon()
    {
        $this->setFreeShipping(false)
            ->setCouponId(null)
            ->collectTotals()
            ->save();
    }

    public function getGrandTotal()
    {
        return $this->getData('total_amount');
    }

    public function getSubtotal()
    {
        return $this->getData('subtotal_amount');
    }
    
    public function checkItemAvailability()
    {
        foreach ($this->getItems() as $item) {
            if (!$item->getProduct()->getIsPublished() || $item->getProduct()->getIsSold() || $item->getProduct()->getQty() < $item->getQty()) {
                $item->delete();
            }
        }
        return $this;
    }

    /* $items: array(id => amount...) */

    public function prepareItemsForCheckout($items)
    {
        $items = (array) $items;

        foreach ($this->getAllItems() as $item) {
            if (key_exists($item->getId(), $items)) {

                $item->setIsUsedOnCheckout(1)
                        ->setQty($items[$item->getId()])
                        ->save();
            } else {
                $item->setIsUsedOnCheckout(0)
                        ->setQty($items[$item->getId()])
                        ->save();
            }
        }
    }

    public function prepareItemsDelivery($items)
    {
        $items = (array) $items;

        foreach ($this->getAllCheckoutItems() as $item) {
            $product = $item->getProduct();

            if ($product->getIsDeliveryAvailable() && $product->getIsPickUpAvailable()) {
                $shippingMethod = isset($items[$item->getId()]) ? $items[$item->getId()] : '';

                if ($shippingMethod == 'delivery' || $shippingMethod == 'pick-up') {
                    $item->setShipping($items[$item->getId()])->save();
                } else {
                    $item->setShipping(null)->save();
                }
            } elseif ($product->getIsDeliveryAvailable()) {
                $item->setShipping('delivery')->save();
            } elseif ($product->getIsPickUpAvailable()) {
                $item->setShipping('pick-up')->save();
            }
        }
    }

    protected function _loadItems()
    {
        /*
          if (!$this->getId()) {
          throw new Exception('Can\'t load collection before saving.');
          }

          $items = new Cart_Model_Item_Collection();
          $this->_itemCollection = $items->getByCartId($this->getId());
         */

        $items = new Cart_Model_Item_Collection();

        if ($this->getId()) {
            $items->getByCartId($this->getId());
        }

        $this->_itemCollection = $items;
    }

    private function calculateTotal()
    {
        $products = new Product_Model_Product_Collection();
        $products->getProductsInCart($this); //$this
        $total    = 0;

        foreach ($products->getItems() as $product) {
            $total += $product->getPrice();
        }

        $this->setTotal($total)->save();

        return $this;
    }

    public function getById($id)
    {
        parent::getById($id);
        $this->_loadItems();
    }

    /**
     * @return Cart_Model_Item_Collection 
     */
    public function getItemCollection()
    {
        return $this->_itemCollection;
    }

    /**
     *  Add new item to cart
     * @param  mixed $param;
     * @example
     *      $param = array('id'=>$product_id, 'shipping'=>'pick-up');
     *      $cart->addItem($param);
     * @example
     *      $item = new Item_Class;
     *      $cart->addItem($item);
     * 
     */
    public function addItem($param)
    {
        if (!$this->_itemCollection)
            $this->_loadItems();
        if ($param instanceof Cart_Model_Item) {
            $param->save();
            $this->_itemCollection->addItem($param);
        } elseif (isset($param['id']) && key_exists('shipping', $param)) {

            $item = new Cart_Model_Item();

            $product = new Product_Model_Product((int) $param['id']);


            if (!$product->getIsPublished())
                return $this;
            if (key_exists($product->getId(), $this->getProducts()->getItems()))
                return $this;

            $shipping = '';
            if ($product->getShipping() == $product::SHIPPING_DELIVERY) {
                switch ($param['shipping']) {
                    case '1':
                        $shipping = $product::SHIPPING_PICK_UP;
                        break;

                    case '2':
                        $shipping = $product::SHIPPING_DELIVERY;
                        break;

                    case $product::SHIPPING_DELIVERY:
                        $shipping = $product::SHIPPING_DELIVERY;
                        break;

                    case $product::SHIPPING_PICK_UP:
                        $shipping = $product::SHIPPING_PICK_UP;
                        break;

                    default:
                        $shipping = $product::SHIPPING_PICK_UP;
                        break;
                }
            } else {
                $shipping = $product::SHIPPING_PICK_UP;
            }

            $item->setProductId($param['id'])->setCartId($this->getId())->setShipping($shipping)->setPrice($product->getPrice())->save();
            $this->_itemCollection->addItem($item);
        }

        $this->calculateTotal();
        return $this;
    }

    /**
     * Add product to cart
     * @param mixed  $param 
     * @param string $shipping
     * @param int $qty
     * 
     * @example
     *      $product = new Product_Class;
     *      $cart->addProduct($product, $product::SHIPPING_DELIVERY);
     * @example
     *      $param = $product_id;
     *      $cart->addProduct($param);
     */
    protected function isProductInCart($product)
    {
        return $this->_getbackend()->isProductInCart($this, $product->getId());
    }

    public function addProduct($product, $qty = 1)
    {
        if (!$this->isProductInCart($product)) {
            $item = new Cart_Model_Item();

            if ($qty > $product->getQty(1)) {
                $qty = $product->getQty(1);
            } elseif ($qty < 1) {
                $qty = 1;
            }

            $item->setCartId($this->getId())
                    ->setProductId($product->getId())
                    ->setQty($qty)
                    ->setProductPrice($product->getPrice())
                    ->setIsActive(1)
                    ->setShippingMethodId('delivery')
                    ->save();

            $this->collectTotals()->save();

            $product->selfTriggerEvent('cart.add', array($qty)); // See Application_Model_Events_Listeners_Product (handlers)

            return $item;
        } else {
            throw new Exception(sprintf('<i class="apt-i-warning orange"></i><p><a href="%s">%s</a> is already in cart.</p>', $product->getProductUrl(), $product->getTitle()));
        }


        return $this;
    }

    /**
     * @return Product_Model_Product_Collection
     */
    public function getProducts()
    {
        $products = new Product_Model_Product_Collection();
        return $products->getProductsInCart($this); //$this   
    }

    protected function removeItemFromCollection($key)
    {
        $this->_itemCollection->removeItem($key);
        $item = new Cart_Model_Item($key);
        $item->delete();
        $this->calculateTotal();
        return $this;
    }

    public function removeItemByProduct($product_id)
    {

        foreach ($this->getItemCollection()->getItems() as $key => $item) {
            if ($item->getProductId() == $product_id) {
                $this->removeItemFromCollection($key);
                return $this;
            }
        }
        return $this;
    }

    public function getItemByProductId($productId)
    {
        foreach ($this->getAllItems() as $item) {
            if ($item->getProductId() == $productId) {
                return $item;
            }
        }

        return new Cart_Model_Item();
    }

    public function count()
    {
        return count($this->getAllItems());
    }

    public function getAllItemsQtySum()
    {
        $qty = $this->getItemsCollection()->getColumn('qty');
        return array_sum($qty);
    }

    public function checkItems($userId)
    {
        $this->_getbackend()->checkItems($userId, $this->getId());
        $this->setUserId($userId)->save();
        return $this;
    }

    /**
     * Call method with params and save result in cache or return result from cache
     * 
     * @param  string $method
     * @param  array $params (Optional)
     * @return mixed
     */
    protected function _throughCache($method, $params = array())
    {
        $key = $method . crc32(serialize($params));
        if (isset($this->_cache[$key])) {
            return $this->_cache[$key];
        }

        $this->_cache[$key] = call_user_func_array(array($this, $method), $params);
        return $this->_cache[$key];
    }

    /**
     * Return related shipping address
     *
     * @return object User_Model_Address
     */
    public function getShippingAddress()
    {
        //if (!$this->_shippingAddress) {//fix https://app.asana.com/0/5037368973334/15872609110354
            $this->_shippingAddress = new User_Model_Address($this->getShippingAddressId());
        //}
        return $this->_shippingAddress;
    }

    /**
     * Calculates transaction fee
     *  
     * @return float 
     */
    protected function _calculateTransactionFee()
    {
        $total          = $this->calculateSubTotal(true) + $this->calculateShippingPriceByZipCode(null, true);
        $transactionFee = ($total * $this->calculateTransactionFeePercent()) / 100;
        return $transactionFee;
    }

    /**
     * !Cash amount!
     * Calculates transaction fee
     *  
     * @return float 
     */
    public function calculateTransactionFee()
    {
        return $this->_throughCache('_calculateTransactionFee');
    }

    /**
     * Calculate transaction fee percent
     *
     * @return int 
     */
    public function calculateTransactionFeePercent()
    {
        $options = new Application_Model_Options();
        return $options->getFee($this->getSubtotalAmount() + $this->getShippingAmount() - $this->getDiscountAmount(), $options::BUYER);
    }

    public function setIsNotified($value)
    {
        $this->setData('is_notified', $value);
        $this->setData('no_reset_notified', true);
        return $this;
    }

    public function setShippingAddressId($shippingAddressId)
    {
        parent::setShippingAddressId($shippingAddressId);
        $this->_shippingAddress = null;
        return $this;
    }

}
