<?php

class Cart_Model_Item_Collection extends Application_Model_Abstract_Collection implements
    Ikantam_Filter_Flex_Interface
{

    protected $_backendClass = 'Cart_Model_Item_Backend';
    protected $_itemObjectClass = 'Cart_Model_Item';
    private $_loaded       = false;

    protected function _setIsLoaded($flag = true)
    {
        $this->_loaded = $flag;
    }

    protected function _getBackEnd()
    {
        return new $this->_backendClass();
    }
    
    /** 
     * 
     * @param int $cartId  
     * @return object self
     */
    public function getCartItems($cartId)
    {
        $this->_getBackEnd()->getByCartId($this, $cartId);
        return $this;
    }

    public function getItemsForCheckout($cartId)
    {
        $this->_getBackEnd()->getActibeByCartId($this, $cartId);
        return $this;
    }

    public function getItemsForDelivery($cartId)
    {
        $this->_getBackEnd()->getDeliveryByCartId($this, $cartId);
        return $this;
    }

    public function getItemsForPickup($cartId)
    {
        $this->_getBackEnd()->getPickupByCartId($this, $cartId);
        return $this;
    }

    public function getAll()
    {
        if (!$this->_loaded) {
            $this->clear();
            $this->_getBackEnd()->getAll($this);
            $this->_setIsLoaded();
        }
        return $this;
    }

    public function getByCartId($id)
    {
        $this->_getBackEnd()->getByCartId($this, $id);
        return $this;
    }

    public function removeItem($key)
    {
        unset($this->_items[$key]);
        return $this;
    }

    public function getTwoMethods($cartId)
    {
        return $this->_getBackEnd()->getTwoMethods($cartId);
    }

    public function getDeliverySelected($cartId)
    {
        return $this->_getBackEnd()->getDeliverySelected($cartId);
    }

    //Remove all items with not available products    
    public static function removeNotAvailable()
    {
        $me = new self();

        $me->_getbackend()->clear();
    }

    public function getAbandoned($time, $isNotified = 1)
    {
        $this->_getBackend()->getAbandoned($this, $time, $isNotified);
        return $this;
    }

    /**
     * Returns a filter related with collection.
     * @return object Ikantam_Filter_Flex
     */
    public function  getFlexFilter()
    {
        return new \Cart_Model_Item_FlexFilter($this);
    }
}