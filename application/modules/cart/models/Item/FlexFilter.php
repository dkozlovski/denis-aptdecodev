<?php
class Cart_Model_Item_FlexFilter extends Ikantam_Filter_Flex {
    
    protected $_acceptClass = 'Cart_Model_Item_Collection';
    
    protected $_joins = array(
        'products' => 'INNER JOIN `products` as `product` ON `product`.`id` = `cart_items`.`product_id`',
        'cart' => 'INNER JOIN `carts` ON `cart_items`.`cart_id` = `carts`.`id`',
        'notifications_unsubscribe' => array(
            'cart' => 'LEFT JOIN `notifications_unsubscribe` ON `notifications_unsubscribe`.`user_id` = `carts`.`user_id`',
        )
    );
    
    protected $_rules = array(
        'product_pickup_address_line1' => array('products' => '`product`.`pickup_address_line1`'),
        'product_pickup_postcode' => array('products' => '`product`.`pickup_postcode`'),
        'product_pickup_city' => array('products' => '`product`.`pickup_city`'),
        'product_title' => array('products' => '`product`.`title`'),
        'user_id' => array('cart' => '`carts`.`user_id`'),
        'user_unsubscribe_notifications' => array('notifications_unsubscribe' => '`notifications_unsubscribe`.`is_user_confirm`'),
    );

    protected $_select = array(
        'cart' => '`carts`.`user_id`'
    );
}