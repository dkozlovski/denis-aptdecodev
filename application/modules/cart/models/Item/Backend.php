<?php

class Cart_Model_Item_Backend extends Application_Model_Abstract_Backend
{
    protected $_table   = 'cart_items';
    protected $_primary = 'id';
    protected $_itemClass = 'Cart_Model_Item';
    protected $_fields  = array(
        'cart_id',
        'product_id',
        'shipping_method_id',
        'product_price',
        'qty',
        'row_total',
        'shipping_amount',
        'options',
        'created_at',
        'updated_at',
        'is_active',
        'seller_expenses'
    );

    public function getAll(\Application_Model_Abstract_Collection $collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Cart_Model_Item();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getByCartId(\Application_Model_Abstract_Collection $collection, $id)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `cart_id` = :id";

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Cart_Model_Item();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getActibeByCartId($collection, $cartId)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `cart_id` = :cart_id AND `is_active` = 1';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':cart_id', $cartId);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Cart_Model_Item();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    public function getDeliveryByCartId($collection, $cartId)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `cart_id` = :cart_id AND `shipping_method_id` = "delivery" AND `is_active` = 1';

        $dbh  = $this->_getConnection();
        $stmt = $dbh->prepare($sql);

        $stmt->bindParam(':cart_id', $cartId);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Cart_Model_Item();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    public function getPickupByCartId($collection, $cartId)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `cart_id` = :cart_id AND `shipping_method_id` = "pickup" AND `is_active` = 1';

        $dbh  = $this->_getConnection();
        $stmt = $dbh->prepare($sql);

        $stmt->bindParam(':cart_id', $cartId);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Cart_Model_Item();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

//-------------------------------------------------------------------------------
    protected function _update(\Application_Model_Abstract $object)
    {
        $bind = array();

        $template = 'UPDATE `%s` SET %s WHERE %s';

        $set = array();
        foreach ($this->_fields as $field) {
            $set[]              = sprintf('`%s` = :%s', $field, $field);
            $bind[':' . $field] = $object->getData($field);
        }

        $where                       = sprintf('`%s` = :%s', $this->_primary, $this->_primary);
        $bind[':' . $this->_primary] = $object->getData($this->_primary);
        $sql                         = sprintf($template, $this->_getTable(), implode(', ', $set), $where);

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bind);
    }

    protected function _beforeInsert($object)
    {
        $object->setCreatedAt(time());
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $this->_beforeInsert($object);

        $template = 'INSERT INTO `%s` (%s) VALUES (%s)';
        $bind     = array();
        $fields   = '`' . implode('`, `', $this->_fields) . '`';
        $values   = ':' . implode(', :', $this->_fields);

        foreach ($this->_fields as $field) {
            $bind[':' . $field] = $object->getData($field);
        }

        $sql = sprintf($template, $this->_getTable(), $fields, $values);

        $dbh  = $this->_getConnection();
        $stmt = $dbh->prepare($sql);

        $stmt->execute($bind);
        $object->getById($dbh->lastInsertId());
    }

    public function getTwoMethods($cartId)
    {
        $sql = "SELECT * FROM `cart_items`
			JOIN `products` on `cart_items`.`product_id` = `products`.`id`
			WHERE `cart_items`.`cart_id` = :cart_id
			AND `products`.`is_delivery_available` = 1
			AND `products`.`is_pick_up_available` = 1";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':cart_id', $cartId);
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            return count($rows) > 0;
        }
        return false;
    }

    public function getAbandoned($collection, $time, $notified = 1)
    {

        $sql = "SELECT `c`.*
            FROM `carts` AS `c`
            JOIN `cart_items` AS `ci` ON `c`.`id` = `ci`.`cart_id`
            LEFT JOIN `cart_user_notified` AS `cun`
                ON `c`.`user_id` = `cun`.`user_id`
            LEFT JOIN `coupons_users` AS `cpu` 
                ON `c`.`user_id` = `cpu`.`user_id`
            LEFT JOIN `app_settings` AS `a_s`
                ON  `a_s`.`name` = 'user_notification_cart_and_wishlist'
            LEFT JOIN `users_app_settings` AS `uas`
                ON `c`.`user_id` = `uas`.`user_id` AND `uas`.`app_setting_id` = `a_s`.`id`

            WHERE `ci`.`created_at` < :time
                AND `c`.`user_id` IS NOT NULL
                AND `c`.`is_notified` < :is_notified
                AND `cpu`.`user_id` IS NULL
                AND GREATEST(IFNULL(`cun`.`last_send_date1`, 0), IFNULL(`cun`.`last_send_date2`, 0), IFNULL(`cun`.`last_send_date3`, 0 )) + IFNULL(`uas`.`value`, 86400) < :now
                AND `c`.`subtotal_amount` > 100  AND `cun`.`last_send_date1` IS NULL

            GROUP BY `c`.`id`
            LIMIT 0 , 10";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':time', $time);
        $stmt->bindParam(':is_notified', $notified);
        $stmt->bindParam(':now', time());


        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Cart_Model_Cart();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getDeliverySelected($cartId)
    {
        $sql = "SELECT * FROM `cart_items` WHERE `cart_id` = :cart_id AND `shipping` = 'delivery'";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':cart_id', $cartId);
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            return count($rows) > 0;
        }
        return false;
    }

    public function getItemsForDelivery($collection, $cartId)
    {
        $sql = "SELECT * FROM `cart_items` WHERE `cart_id` = :cart_id AND `shipping` = 'delivery'";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':cart_id', $cartId);
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Cart_Model_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    /**
     * Clear all carts items: remove items with product which is not saleable.
     */
    public function clear()
    {
        $sql = "DELETE `items` FROM `" . $this->_getTable() . "` AS `items`
                INNER JOIN `products` ON `products`.`id` = `items`.`product_id` AND `products`.`qty` = 0
                WHERE 1";

        $this->_getConnection()->prepare($sql)->execute();
    }

}