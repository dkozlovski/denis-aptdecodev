<?php

class Cart_Model_Totals
{

    protected $_totals = array();

    public function setTotals($totals)
    {
        $this->_totals = $totals;
        return $this;
    }

    public function addTotals($totals)
    {
        $this->_totals[] = $totals;
        return $this;
    }

    public function getTotals()
    {
        return $this->_totals;
    }

    public function asArray()
    {
        return $this->getTotals();
    }

    public function asFloat()
    {
        return array_sum($this->getTotals());
    }

}