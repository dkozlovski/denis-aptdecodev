<?php

/**
 * Class Cart_Model_Item
 * @method null|float getSellerExpenses();
 * @method setSellerExpenses(float $value);
 * ...
 */
class Cart_Model_Item extends Application_Model_Abstract
{

    protected $_backendClass = 'Cart_Model_Item_Backend';
    protected $_shippingMethod;
    protected $_product      = null;

    public function __construct($id = null)
    {
        if ($id) {
            $this->getById($id);
        }

        return $this;
    }

    protected function _beforeSave()
    {
        $rowTotal = $this->getData('qty') * $this->getData('product_price');

        $this->setData('row_total', $rowTotal)
                ->setData('updated_at', time());
    }

    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = new Product_Model_Product();
            $this->_product->getById($this->getProductId());
        }

        return $this->_product;
    }

    public function getShippingMethod()
    {
        if (!$this->_shippingMethod) {
            $this->_shippingMethod = new Checkout_Model_ShippingMethod($this->getShippingMethodId());
        }
        return $this->_shippingMethod;
    }

    public function getDeliveryOptions()
    {
        $options = unserialize($this->getOptions());
        return isset($options['delivery_options']) ? $options['delivery_options'] : null;
    }

    public function setDeliveryOptions($deliveryOptions)
    {
        $options                     = unserialize($this->getOptions());
        $options['delivery_options'] = $deliveryOptions;
        $this->setOptions(serialize($options));
        return $this;
    }

    public function getBuyerDates()
    {
        $options = unserialize($this->getOptions());
        return isset($options['buyer_dates']) ? $options['buyer_dates'] : null;
    }

    public function setBuyerDates($dates)
    {
        $options                = unserialize($this->getOptions());
        $options['buyer_dates'] = $dates;
        $this->setOptions(serialize($options));
        return $this;
    }

}
