<?php

/**
 * Class Cart_Model_Coupon
 * @method Cart_Model_Coupon_Backend _getBackend()
 *
 * @method string getCode()
 * @method string getType()
 * @method float getAmount()
 * @method int getCreatedAt()
 * @method int getValidTill()
 * @method int getCouponLimit()
 * @method int getUserLimit()
 * @method float|null getMinPrice()
 * @method float|null getMaxPrice()
 * @method int|null getCategoryId()
 * @method string|null getPostcode()
 * @method int getIsActive()
 *
 * @method Cart_Model_Coupon setCode(string $value)
 * @method Cart_Model_Coupon setType(string $value)
 * @method Cart_Model_Coupon setAmount(float $value)
 * @method Cart_Model_Coupon setCreatedAt(int $value)
 * @method Cart_Model_Coupon setValidTill(int $value)
 * @method Cart_Model_Coupon setCouponLimit(int $value)
 * @method Cart_Model_Coupon setUserLimit(int $value)
 * @method Cart_Model_Coupon setMinPrice(float $value)
 * @method Cart_Model_Coupon setMaxPrice(float $value)
 * @method Cart_Model_Coupon setCategoryId(int $value)
 * @method Cart_Model_Coupon setPostcode(string $value)
 * @method Cart_Model_Coupon setIsActive(int $value)
 */
class Cart_Model_Coupon extends Application_Model_Abstract
{
    protected $_backendClass   = 'Cart_Model_Coupon_Backend';
    protected $_itemCollection = null;
    protected $_items;
    protected $_errorMessages = array();

    const TYPE_FIXED    = 'fixed';
    const TYPE_PERCENT  = 'percent';
    const TYPE_SHIPPING = 'shipping';

    const ERROR_MESSAGE_CODE      = 'Coupon code error';
    const ERROR_MESSAGE_MIN_PRICE = 'Coupon is only valid for purchases over %s';
    const ERROR_MESSAGE_MAX_PRICE = 'Coupon is only valid for purchases under %s';
    const ERROR_MESSAGE_CATEGORY  = 'Coupon is only valid for %s';
    const ERROR_MESSAGE_POSTCODE  = 'Coupon is only valid for %s zipcode only';

    public function __construct($code = null)
    {
        if ($code) {
            $this->getByCode($code);
        }

        return $this;
    }

    public function getById($code)
    {
        $this->_getBackend()->getById($this, $code);
        return $this;
    }

    public function getByCode($code)
    {
        $this->_getBackend()->getByCode($code, $this);
        return $this;
    }

    public function isValid($userId = null)
    {
        $this->setErrorMessages(array());

        if (!$this->getId()) {
            $this->addErrorMessage(self::ERROR_MESSAGE_CODE);
            return false;
        }

        if (!$this->getIsActive()) {
            $this->addErrorMessage(self::ERROR_MESSAGE_CODE);
            return false;
        }

        if ($this->getValidTill() < time()) {
            $this->addErrorMessage(self::ERROR_MESSAGE_CODE);
            return false;
        }

        if ($this->getCouponLimit() <= $this->countUses()) {
            $this->addErrorMessage(self::ERROR_MESSAGE_CODE);
            return false;
        }

        if ($userId && $this->getUserLimit() <= $this->countUsesByUser($userId)) {
            $this->addErrorMessage(self::ERROR_MESSAGE_CODE);
            return false;
        }

        $session = User_Model_Session::instance();
        $cart = $session->getCart();

        if ($this->getMinPrice() !== null) {
            if ($this->getMinPrice() > $cart->getSubtotal()) {
                $this->addErrorMessage(sprintf(self::ERROR_MESSAGE_MIN_PRICE, '$' . $this->getMinPrice()));
                return false;
            }
        }

        if ($this->getMaxPrice() !== null) {
            if ($this->getMaxPrice() < $cart->getSubtotal()) {
                $this->addErrorMessage(sprintf(self::ERROR_MESSAGE_MAX_PRICE, '$' . $this->getMaxPrice()));
                return false;
            }
        }

        if ($this->getCategoryId() !== null) {
            if (!count($this->getAppliedItems())) {
                $this->addErrorMessage(sprintf(self::ERROR_MESSAGE_CATEGORY, $this->getCategory()->getTitle()));
                return false;
            }
        }

        if ($this->getPostcode() !== null) {
            $postcode = $cart->getShippingAddress()->getPostcode();
            if (!$postcode) {
                $postcode = Zend_Controller_Front::getInstance()->getRequest()->getParam('postal_code');
                if (!$postcode) {
                    $shippingAddressParam =  Zend_Controller_Front::getInstance()->getRequest()->getParam('shipping_address');
                    if (isset($shippingAddressParam['postal_code'])) {
                        $postcode = $shippingAddressParam['postal_code'];
                    }
                }
            }

            if (!preg_match('/^' . $this->getPostcode() . '$/', $postcode)) {
                $this->addErrorMessage(sprintf(self::ERROR_MESSAGE_POSTCODE, $this->getPostcode()));
                return false;
            }
        }

        return true;
    }

    public function countUsesByUser($userId)
    {
        return $this->_getBackend()->countUsesByUser($this->getId(), $userId);
    }

    public function addUse($userId, $orderId)
    {
        if (!$this->_hasUsedForOrder($orderId)) {
            $this->_getBackend()->addUse($this->getId(), $userId, $orderId);
        }
        return $this;
    }

    public function countUses()
    {
        return $this->_getBackend()->countUses($this->getId());
    }

    /*
     * return items for which there is a discount
     */
    public function getAppliedItems()
    {
        $cart = $this->getCart();
        if ($this->getCategoryId() !== null) {
            $subcategoriesIds = $this->getSubcategoriesIds();
            $collection = new Cart_Model_Item_Collection();
            foreach ($cart->getItemsForCheckout() as $_item) {
                /* @var $_item Cart_Model_Item */
                if (in_array($_item->getProduct()->getCategoryId(), $subcategoriesIds)) {
                    $collection->addItem($_item);
                }
            }
            return $collection;
        } else {
            return $cart->getItemsForCheckout();
        }
    }

    public function getSubcategoriesIds()
    {
        if ($this->getCategoryId()) {
            $category = new Category_Model_Category();
            $category->setId($this->getCategoryId());
            return  $category->getSubCategoriesId();
        }

        $allCategories = new Category_Model_Category_Collection();
        $ids = array();
        foreach ($allCategories->getAllSubcategories() as $_category) {
            /* @var $_category Category_Model_Category */
            $ids[] = $_category->getId();
        }
        return $ids;
    }

    public function getCart()
    {
        $session = User_Model_Session::instance();
        $cart = $session->getCart();
        return $cart;
    }

    public function getCategory()
    {
        $category = new Category_Model_Category($this->getCategoryId());
        return $category;
    }

    public function addErrorMessage($message)
    {
        return $this->_errorMessages[] = $message;
    }

    public function setErrorMessages(array $messages)
    {
        return $this->_errorMessages = $messages;
    }

    public function getErrorMessages()
    {
        return (array) $this->_errorMessages;
    }

    protected  function _hasUsedForOrder($orderId)
    {
        return $this->_getBackend()->hasUsedForOrder($this, $orderId);
    }
}
