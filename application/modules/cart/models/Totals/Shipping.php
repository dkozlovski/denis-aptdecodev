<?php

use Shipping_Model_RoutePlanner as RoutePlanner;

class Cart_Model_Totals_Shipping extends Cart_Model_Totals_Abstract
{
    /* public function __construct()
      {
      $this->addRulesModel(new Cart_Model_Totals_Rule_Shipping());
      } */

    public function collectTotals(\User_Model_Address $address = null)
    {

        //pick-up selected for all items
        if (!$this->getCart()->hasItemsForDelivery()) {
            return 0;
        }

        $totals = 0;

        // If address isn't specified then use related with cart address
        $address = $address ? : $this->getCart()->getShippingAddress();

        if (!$address->getPostalCode($address->getPostcode())) {
            return null;
        }

        $items        = $this->getCart()->getItemsForDelivery();
        $routePlanner = new RoutePlanner();
        $addresses    = array();

        $freeShippingForCategoryItemsIds = $this->_getFreeShippingForCategoryItemsIds();
        foreach ($items as $item) {
            /* @var $item Cart_Model_Item */
            if (in_array($item->getId(), $freeShippingForCategoryItemsIds)) {
                $item->setShippingAmount(0)
                    ->save();
                continue;
            }

            $product      = $item->getProduct();
            //check if item have same address as already used (same pick up address)
            $addressLine1 = $product->getPickupAddressLine1();
            $addressLine1 = preg_replace("[\,|\.|\'|-|\"|\\|\/i]", "", $addressLine1);
            $addressLine1 = preg_replace("/[\n|\t]/i", " ", $addressLine1);
            $addressLine1 = strtoupper(preg_replace('/\s+/', '', trim($addressLine1)));

            $key             = crc32($addressLine1 . $product->getPickupPostcode());
            $addresses[$key] = isset($addresses[$key]) + 1;
            // same as one of previous
            $isSameAddress   = $addresses[$key] > 1;
            $shippingAmount = $this->getShippingPriceForItem(
                $routePlanner, $address, $item, $isSameAddress
            );
            $item->setShippingAmount($shippingAmount)
                ->save();
            $totals += $shippingAmount;
        }

        return $totals;
    }

    public function collectTotalsByAddress(\User_Model_Address $address)
    {
        return $this->collectTotals($address);
    }

    /**
     * Calculates shipping price for cart item
     *
     * @param Shipping_Model_RoutePlanner $planner - uses to obtain default prices
     * @param User_Model_Address $address - destination address
     * @param Cart_Model_Item $item
     * @param bool $isSameAddress
     * @return float
     */
    protected function getShippingPriceForItem(
        RoutePlanner $planner,
        User_Model_Address $address,
        Cart_Model_Item $item,
        $isSameAddress
    )
    {
        $product                     = $item->getProduct();
        $qty                         = $item->getQty();
        $isUnder15Pounds             = $product->getData('is_under_15_pounds');
        $productZip                  = $product->getPickupPostalCode($product->getPickupPostcode());
        $destinationZip              = $address->getPostalCode($address->getPostcode());
        $additionalItemShippingPrice = 0;
        $total                       = 0;

        // Find price for first item
        $qty -= 1; //!
        $defaultShippingPrice = $planner->getShippingPriceByPostalCodes(
            $productZip, $destinationZip, $isUnder15Pounds, $isSameAddress
        );

        //If we have another items(i.e. qty > 0), then lets find price for "same address"
        if ($qty > 0) {
            $additionalItemShippingPrice = $planner->getShippingPriceByPostalCodes(
                $productZip, $destinationZip, $isUnder15Pounds, true //It is same address ;)
            );
        }

        // Sellers can absorb part of shipping fee shipping
        if ($absorbingBySellerPercent = $product->getShippingCover()) {
            if ($absorbingBySellerPercent < 0 || $absorbingBySellerPercent > 100) {
                $absorbingBySellerPercent = 0;
            }
            $absorbForDefault              = $defaultShippingPrice / 100 * $absorbingBySellerPercent;
            $absorbForAdditionalSingleItem = $additionalItemShippingPrice / 100 * $absorbingBySellerPercent;


            $absorbLimit = Shipping_Model_Rate::getMaximumValueSellerAbsorb($isUnder15Pounds) / 100 * $absorbingBySellerPercent;

            if ($absorbForDefault < 0) {
                $absorbForDefault = 0;
            } elseif ($absorbForDefault > $absorbLimit) {
                $absorbForDefault = $absorbLimit;
            }

            // price for 1 additional item
            if ($absorbForAdditionalSingleItem < 0) {
                $absorbForAdditionalSingleItem = 0;
            } elseif ($absorbForAdditionalSingleItem > $absorbLimit) {
                $absorbForAdditionalSingleItem = $absorbLimit;
            }
            // price for all additional items
            $absorbForAdditionalAllItems = $qty * $absorbForAdditionalSingleItem;

            $defaultShippingPrice -= $absorbForDefault;
            $additionalItemShippingPrice -= $absorbForAdditionalSingleItem;

            if ($item->getShippingMethodId() == 'delivery') {
                $item->setSellerExpenses($absorbForDefault + $absorbForAdditionalAllItems)
                    ->save();
            } else {
                $item->setSellerExpenses(null)
                    ->save();
            }
        }

        $total = max(0, $defaultShippingPrice) + max(0, $additionalItemShippingPrice * $qty);

        /*
          take into total price for flights of stairs:
          If a buyer indicates that their building is 4 or more flights during
          the check out process the delivery fee should increase
          by $10 for each additional flight
         */
        $additionalFlightsOfStairs = max(0, $address->getNumberOfStairs() - 3);
        $total += $additionalFlightsOfStairs * Shipping_Model_Rate::getRateForAdditionalFlights();

        // And also we need calculate additional "Flights" cost for items in set
        $total += ($additionalFlightsOfStairs * Shipping_Model_Rate::getRateForAdditionalFlights()) * max(0, $qty);

        return $total;
    }

    /**
    The total delivery cost of this job excluding any discounts / offers.
    This will help us determine how much to pay each delivery crew.
     */
    public function collectTotalsForOrderItem($order)
    {
        $totals     = 0;
        $itemsCount = 0;
        // If address isn't specified then use related with cart address
        $address    = $order->getShippingAddress();

        $items        = $order->getItemsForDelivery();
        $routePlanner = new RoutePlanner();
        $addresses    = array();

        foreach ($items as $item) {
            $product      = $item->getProduct();
            //check if item have same address as already used (same pick up address)
            $addressLine1 = $product->getPickupAddressLine1();
            $addressLine1 = preg_replace("[\,|\.|\'|-|\"|\\|\/i]", "", $addressLine1);
            $addressLine1 = preg_replace("/[\n|\t]/i", " ", $addressLine1);
            $addressLine1 = strtoupper(preg_replace('/\s+/', '', trim($addressLine1)));

            $key             = crc32($addressLine1 . $product->getPickupPostcode());
            $addresses[$key] = isset($addresses[$key]) + 1;
            // same as one of previous
            $isSameAddress   = $addresses[$key] > 1;

            $totals += $this->getShippingPriceForOrderItem(
                $routePlanner, $address, $item, $isSameAddress
            );
            $itemsCount++;
        }

        if ($itemsCount > 0) {
            return $totals / $itemsCount;
        }
        return 0;
    }

    public  function getShippingPriceForOrderItem(RoutePlanner $planner, $address, $item, $isSameAddress)
    {
        $product                     = $item->getProduct();
        $qty                         = $item->getQty();
        $isUnder15Pounds             = $product->getData('is_under_15_pounds');
        $productZip                  = $product->getPickupPostalCode($product->getPickupPostcode());
        $destinationZip              = $address->getPostalCode($address->getPostcode());
        $additionalItemShippingPrice = 0;
        $total                       = 0;

        // Find price for first item
        $qty -= 1; //!
        $defaultShippingPrice = $planner->getShippingPriceByPostalCodes(
            $productZip, $destinationZip, $isUnder15Pounds, $isSameAddress
        );

        //If we have another items(i.e. qty > 0), then lets find price for "same address"
        if ($qty > 0) {
            $additionalItemShippingPrice = $planner->getShippingPriceByPostalCodes(
                $productZip, $destinationZip, $isUnder15Pounds, true //It is same address ;)
            );
        }

        $total = max(0, $defaultShippingPrice) + max(0, $additionalItemShippingPrice * $qty);

        /*
          take into total price for flights of stairs:
          If a buyer indicates that their building is 4 or more flights during
          the check out process the delivery fee should increase
          by $10 for each additional flight
         */
        $additionalFlightsOfStairs = max(0, $address->getNumberOfStairs() - 3);
        $total += $additionalFlightsOfStairs * Shipping_Model_Rate::getRateForAdditionalFlights();

        // And also we need calculate additional "Flights" cost for items in set
        $total += ($additionalFlightsOfStairs * Shipping_Model_Rate::getRateForAdditionalFlights()) * max(0, $qty);

        return $total;
    }


    protected function _getFreeShippingForCategoryItemsIds()
    {
        $coupon = $this->getCart()->getCoupon();
        $ids = array();

        if ($coupon->getId() && $coupon->getType() == Cart_Model_Coupon::TYPE_SHIPPING) {
            foreach ($coupon->getAppliedItems() as $_item) {
                /* @var $_item Cart_Model_Item */
                $ids[] = $_item->getId();
            }
        }
        return $ids;
    }

}