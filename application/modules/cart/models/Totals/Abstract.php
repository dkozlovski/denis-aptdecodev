<?php

abstract class Cart_Model_Totals_Abstract
{

    protected $_cart;
    protected $_rulesModels;

    abstract public function collectTotals();

    public function setCart($cart)
    {
        $this->_cart = $cart;
        return $this;
    }

    /**
     * @return Cart_Model_Cart
     */
    public function getCart()
    {
        return $this->_cart;
    }

    public function getRulesModels()
    {
        return $this->_rulesModels;
    }

    public function setRulesModels(array $rulesModels)
    {
        $this->_rulesModels = $rulesModels;
        return $this;
    }

    public function addRulesModel($rulesModel)
    {
        $this->_rulesModels[] = $rulesModel;
        return $this;
    }

}