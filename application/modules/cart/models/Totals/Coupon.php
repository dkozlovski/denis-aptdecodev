<?php

class Cart_Model_Totals_Coupon extends Cart_Model_Totals_Abstract
{
    public function collectTotals()
    {
        $coupon = $this->getCart()->getCoupon();

        $total = 0;
        if (!$coupon->getId()) {
            return $total;
        }

        if ($coupon->isValid()){
            if ($coupon->getType() == 'shipping') {
                //nothing to do here
            } elseif ($coupon->getType() == 'percent') {
                foreach ($coupon->getAppliedItems() as $item) {
                    $total += $item->getRowTotal() * $coupon->getAmount() / 100;
                }
            } elseif ($coupon->getType() == 'fixed') {
                $total = $coupon->getAmount();
            }
        } else {
            $this->getCart()
                ->setFreeShipping(false)
                ->setCouponId(null)
                ->save();
        }

        return $total;
    }
}