<?php

class Cart_Model_Totals_Transaction extends Cart_Model_Totals_Abstract
{

    public function __construct()
    {
        $this->addRulesModel(new Cart_Model_Totals_Rule_Transaction());
    }

    public function collectTotals()
    {
        $cart      = $this->getCart();
        $totals    = 0;
        $cartTotal = $cart->getSubtotalAmount() + $cart->getShippingAmount() - $cart->getDiscountAmount();

        foreach ($this->getRulesModels() as $rule) {
            $totals += $cartTotal * $rule->getFees($cartTotal) / 100;
        }
        
        return $totals;
    }

}