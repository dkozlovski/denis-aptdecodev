<?php

class Cart_Model_Totals_Rule_Transaction
{

    public function getFees($total)
    {
        $options = new Application_Model_Options();
        return $options->getFee($total, Application_Model_Options::BUYER);
    }

}