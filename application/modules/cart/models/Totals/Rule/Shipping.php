<?php

class Cart_Model_Totals_Rule_Shipping
{

    protected $_dbTable;

    public function getDbTable()
    {
        if (!$this->_dbTable) {
            $this->_dbTable = new Application_Model_ShippingFee_Table();
        }
        return $this->_dbTable;
    }

    public function getFees($items)
    {
        $totalPrice = 0;

        foreach ($items as $item) {
            $totalPrice += $item->getQty() * $item->getPrice();
        }

        $fees = $this->getDbTable()->getFees($totalPrice);
        
        return array_pad($fees, count($items), end($fees));
    }

}