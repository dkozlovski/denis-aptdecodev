<?php

class Cart_Model_Totals_Subtotal extends Cart_Model_Totals_Abstract
{

    public function collectTotals()
    {
        $totals = 0;

        foreach ($this->getCart()->getItemsForCheckout() as $item) {
            $totals += $item->getRowTotal();
        }

        return $totals;
    }

}