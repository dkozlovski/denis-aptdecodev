<?php

/**
 * Class Cart_Model_Cart_UserNotified
 * @method int getUserId()
 * @method int getLastSendDate1()
 * @method int getLastSendDate2()
 * @method int getLastSendDate3()
 * @method Cart_Model_Cart_UserNotified_Backend _getbackend()
 *
 * @method Cart_Model_Cart_UserNotified setUserId(int $userId)
 * @method Cart_Model_Cart_UserNotified setLastSendDate1(int $time)
 * @method Cart_Model_Cart_UserNotified setLastSendDate2(int $time)
 * @method Cart_Model_Cart_UserNotified setLastSendDate3(int $time)
 */
class Cart_Model_Cart_UserNotified extends  Application_Model_Abstract
{

    public function getByUserId($userId)
    {
        $this->_getbackend()->getByUserId($this, $userId);
        return $this;
    }
}