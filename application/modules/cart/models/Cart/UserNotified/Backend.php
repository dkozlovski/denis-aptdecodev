<?php

class Cart_Model_Cart_UserNotified_Backend extends Application_Model_Abstract_Backend
{
    protected $_table   = 'cart_user_notified';

    public function getByUserId(\Cart_Model_Cart_UserNotified $object, $userId)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `user_id` = :user_id';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }
}