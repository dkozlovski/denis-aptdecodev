<?php
class Cart_Model_Cart_Collection extends Application_Model_Abstract_Collection
{
	protected $_backendClass = 'Cart_Model_Cart_Backend';
    private   $_loaded = false;
    
    protected function _setIsLoaded($flag = true)
    {
        $this->_loaded = $flag;
    }
    
    protected function _getBackEnd()
    {
        return new $this->_backendClass();
    }
    
    public function getAll ()
    {
        if(!$this->_loaded)
        {
    $this->clear();
    $this->_getBackEnd()->getAll($this);
    $this->_setIsLoaded();
    }
    return $this;
    }
    
 }