<?php

class Cart_Model_Cart_Backend extends Application_Model_Abstract_Backend
{

    protected $_table   = 'carts';
    protected $_primary = 'id';
    protected $_fields  = array(
        'user_id',
        'coupon_id',
        'billing_address_id',
        'shipping_address_id',
        'free_shipping',
        'subtotal_amount',
        'shipping_amount',
        'discount_amount',
        'transaction_amount',
        'total_amount',
        'is_notified',
    );

    public function getAll($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Cart_Model_Cart();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function isProductInCart($cart, $product_id)
    {
        $sql = 'SELECT * FROM `cart_items` WHERE `cart_id` = :cart_id AND `product_id` = :product_id;';

        $stmt = $this->_getConnection()->prepare($sql);

        $cart_id = $cart->getId();

        $stmt->bindParam(':cart_id', $cart_id);
        $stmt->bindParam(':product_id', $product_id);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (is_array($result) && count($result)) {
            return true;
        }
        return false;
    }

    public function checkItems($userId, $cartId)
    {
        $sql = "DELETE `cart_items` FROM  `cart_items` 
            INNER JOIN `products` AS `p` ON `p`.`id` = `cart_items`.`product_id` AND `p`.`user_id` = :uid
            WHERE `cart_items`.`cart_id` = :cid";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':uid', $userId);
        $stmt->bindParam(':cid', $cartId);

        $stmt->execute();
    }

//-------------------------------------------------------------------------------
    protected function _update(\Application_Model_Abstract $object)
    {
        $bind = array();

        $template = 'UPDATE `%s` SET %s WHERE %s';

        $set = array();
        foreach ($this->_fields as $field) {
            $set[]              = sprintf('`%s` = :%s', $field, $field);
            $bind[':' . $field] = $object->getData($field);
        }

        $where                       = sprintf('`%s` = :%s', $this->_primary, $this->_primary);
        $bind[':' . $this->_primary] = $object->getData($this->_primary);
        $sql                         = sprintf($template, $this->_getTable(), implode(', ', $set), $where);

        $stmt = $this->_getConnection()->prepare($sql);

        // https://app.asana.com/0/12118343550314/15072379986423
        // Urgent Bug: Customer recieved application error when trying to place order
        try {
            $stmt->execute($bind);
        } catch (\PDOException $PDOException) {
            $errorInfo = $PDOException->errorInfo;
            if ($errorInfo[0] != 23000 || $errorInfo[1] != 1452) {
                throw $PDOException;
            }
        }
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $dbh  = $this->_getConnection();
        $sql  = "INSERT INTO `" . $this->_getTable() . "` (`id`) VALUES (NULL)";
        $stmt = $dbh->prepare($sql);
        $stmt->execute();
        $object->getById($dbh->lastInsertId());
    }

}
