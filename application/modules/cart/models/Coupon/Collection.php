<?php

class Cart_Model_Coupon_Collection extends Application_Model_Abstract_Collection
{

    protected $_backendClass = 'Cart_Model_Coupon_Backend';

    protected function _getBackend()
    {
        return new $this->_backendClass();
    }

    public function getAll($params = array())
    {
        $this->_getBackend()->getAll($this, $params);
        return $this;
    }
    
    public function getAllPaginated($params, $offset, $limit)
    {
        $this->_getBackend()->getAllPaginated($this, $params, $offset, $limit);
        return $this;
    }

}