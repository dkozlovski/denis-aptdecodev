<?php

class Cart_Model_Coupon_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'coupons';

    public function getAll($collection, $params)
    {
        $status = $params['status'];
        $type   = $params['type'];
        $sortBy = $params['sortBy'];
        $code   = $params['code'];

        $bind    = array();
        $where   = array('1=1');
        $sort_by = '';

        if ($status == 'active') {
            $where[]       = '`is_active` = 1 AND `coupon_limit` > 0 AND `valid_till` > :time';
            $bind[':time'] = time();
        } elseif ($status == 'used') {
            $where[] = '`is_active` = 0 AND `coupon_limit` < 1';
        } elseif ($status == 'expired') {
            $where[]       = '`is_active` = 0 AND `valid_till` < :time';
            $bind[':time'] = time();
        }

        if ($type == 'percent') {
            $where[] = '`type` = "percent"';
        } elseif ($type == 'shipping') {
            $where[] = '`type` = "shipping"';
        } elseif ($type == 'fixed') {
            $where[] = '`type` = "fixed"';
        }

        if ($sortBy == 'expiry_date') {
            $sort_by = ' ORDER BY `valid_till` DESC';
        } elseif ($sortBy == 'active') {
            $sort_by = ' ORDER BY `is_active` DESC';
        } else {
            $sort_by = ' ORDER BY `id` DESC';
        }

        if (!empty($code)) {
            $where[]       = '`code` LIKE :code';
            $bind[':code'] = '%' . $code . '%';
        }


        ////////

        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE ' . implode(' AND ', $where) . $sort_by;

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Cart_Model_Coupon();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getAllPaginated($collection, $params, $offset, $limit)
    {
        $status = $params['status'];
        $type   = $params['type'];
        $sortBy = $params['sortBy'];
        $code   = $params['code'];

        $bind    = array();
        $where   = array('1=1');
        $sort_by = '';

        if ($status == 'active') {
            $where[]       = '`is_active` = 1 AND `coupon_limit` > 0 AND `valid_till` > :time';
            $bind[':time'] = time();
        } elseif ($status == 'used') {
            $where[] = '`is_active` = 0 AND `coupon_limit` < 1';
        } elseif ($status == 'expired') {
            $where[]       = '`is_active` = 0 AND `valid_till` < :time';
            $bind[':time'] = time();
        }

        if ($type == 'percent') {
            $where[] = '`type` = "percent"';
        } elseif ($type == 'shipping') {
            $where[] = '`type` = "shipping"';
        } elseif ($type == 'fixed') {
            $where[] = '`type` = "fixed"';
        }

        if ($sortBy == 'expiry_date') {
            $sort_by = ' ORDER BY `valid_till` DESC';
        } elseif ($sortBy == 'active') {
            $sort_by = ' ORDER BY `is_active` DESC';
        } else {
            $sort_by = ' ORDER BY `id` DESC';
        }

        if (!empty($code)) {
            $where[]       = '`code` LIKE :code';
            $bind[':code'] = '%' . $code . '%';
        }


        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE ' . implode(' AND ', $where) . $sort_by . ' LIMIT :offset, :limit';

        $stmt = $this->_getConnection()->prepare($sql);

        $bind[':offset'] = $offset;
        $bind[':limit']  = $limit;

        $stmt->execute($bind);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Cart_Model_Coupon();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getById(Application_Model_Abstract $object, $id)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `id` = :code';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':code', $id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function getByCode($code, $object)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `code` = :code';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':code', $code);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function countUsesByUser($couponId, $userId)
    {
        $sql = 'SELECT COUNT(*) as `count`
            FROM `coupons`
            JOIN `coupons_users` ON `coupons`.`id` = `coupons_users`.`coupon_id`
            WHERE `coupons_users`.`coupon_id` = :coupon_id
            AND `coupons_users`.`user_id` = :user_id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':coupon_id', $couponId);
        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row && isset($row['count'])) {
            return (int) $row['count'];
        }
        return 0;
    }

    public function countUses($couponId)
    {
        $sql = 'SELECT COUNT(*) as `count`
            FROM `coupons`
            JOIN `coupons_users` ON `coupons`.`id` = `coupons_users`.`coupon_id`
            WHERE `coupons_users`.`coupon_id` = :coupon_id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':coupon_id', $couponId);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row && isset($row['count'])) {
            return (int) $row['count'];
        }
        return 0;
    }

    public function addUse($couponId, $userId, $oderId)
    {
        $sql = 'INSERT INTO `coupons_users` (`coupon_id`, `user_id`, `order_id`, `used_at`)
            VALUES (:coupon_id, :user_id, :order_id, :used_at)';

        $stmt = $this->_getConnection()->prepare($sql);

        $time = time();

        $stmt->bindParam(':coupon_id', $couponId);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':order_id', $oderId);
        $stmt->bindParam(':used_at', $time);

        $stmt->execute();
    }

    public function hasUsedForOrder(Cart_Model_Coupon $coupon, $orderId)
    {
        $sql = 'SELECT `id`
            FROM  `coupons_users`
            WHERE `coupon_id` = :coupon_id AND `order_id` = :order_id
            LIMIT 1';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':coupon_id', $coupon->getId());
        $stmt->bindParam(':order_id', $orderId);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_COLUMN);
        return (bool) $row;
    }
}
