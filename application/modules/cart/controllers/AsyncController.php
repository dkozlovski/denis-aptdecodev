<?php

class Cart_AsyncController extends Ikantam_Controller_Front
{
    
    protected $vDigits = null; //Validator

    public function init()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->vDigits = new Ikantam_Validate_Digits();
    }

    public function removeAction ()
    {
        $id = $this->getRequest()->getParam('id');//product id
        if($this->vDigits->isValid($id))
        {
            $cart = $this->getSession()->getCart();
            $cart->removeItemByProduct($id);
            $product = new Product_Model_Product((int)$id);
            
           echo Zend_Json::encode(array(
           'url'=>$product->getProductUrl(),
           'title'=>$product->getTitle(),
           'count'=>$cart->count(),
           ));    
        }
              
    }
    
    public function restoreAction ()
    {
        $id = $this->getRequest()->getParam('id');//product id
        if($this->vDigits->isValid($id))
        {
            $cart = $this->getSession()->getCart();
            $cart->addProduct($id);
            echo Zend_Json::encode(array('count'=>$cart->count()));   
        }
    
    }
    


}

