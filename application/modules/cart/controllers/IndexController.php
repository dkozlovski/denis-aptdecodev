<?php

class Cart_IndexController extends Ikantam_Controller_Front
{

    protected function getCart()
    {
        return $this->getSession()->getCart();
    }

    public function indexAction()
    {
        if (!$this->getCart()->hasItems()) {
            $this->_helper->viewRenderer->setRender('empty');
            $this->view->messages = $this->getSession()->getMessages('error');
        } else {
            $this->view->session = $this->getSession();
            $this->view->cart    = $this->getCart()->collectTotals();
        }

        $this->view->headTitle('Your Shopping Cart');
        $this->view->headMeta()->appendName('description', 'View used furniture currently in your AptDeco shopping cart. Delivery is available for just $65.');

        $existingItems = array();

        foreach ($this->getCart()->getItems() as $cartItem) {
            $existingItems[$cartItem->getProduct()->getUserId()][] = $cartItem->getProduct()->getId();
        }

        $suggestions             = new Product_Model_Product_Collection();
        $this->view->suggestions = $suggestions->getCartSuggestions($existingItems);

        /*
         * Cart suggestions to be implemented later
         * 
         * $SFCollection                    = new Application_Model_ShippingFeeRule_Collection();
          $this->view->shippingFeeSentence = $SFCollection->getSentenceForFreeDelivery();
          $latest                          = new Product_Model_Product_Collection();
          $this->view->latest              = $latest->getSuggestionProducts();
          $deliverySum                     = 0;

          foreach ($this->getCart()->getAllItems() as $cartItem) {
          $product = $cartItem->getProduct();
          if ($product->getIsDeliveryAvailable()) {
          $deliverySum += $product->getPrice() * $cartItem->getQty(1);
          }
          }
          $this->deliverySum       = $deliverySum;
          $this->view->shippingFee = $SFCollection->getShippingFee($deliverySum);
         */

        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/input.spin.js'));
    }

    public function addAction()
    {
        $request = $this->getRequest();

        $productId = $request->getParam('id');
        $qty       = $request->getParam('qty', 1);
        $session   = $this->getSession();
        $product   = new Product_Model_Product($productId);

        if ($request->isXmlHttpRequest()) {
            $response = array(
                'success'  => false,
                'error'    => 'Cannot add item to cart',
                'message'  => '',
                'subtotal' => ''
            );

            if (!$product->getId() || !$product->isSaleable()) {
                $response['error'] = "This product does not exist.";
            }

            if ($product->getUserId() == $session->getUser()->getId()) {
                $response['error'] = "This product does not exist.";
            }

            if ($product->getQty() < $qty) {
                $response['error'] = "The requested quantity for this product is not available.";
            }

            foreach ($session->getUser()->getWishlist()->getAllItems() as $item) {
                if ($item->getProductId() == $product->getId()) {
                    $item->delete();
                }
            }

            try {
                $item = $this->getCart()->addProduct($product, $qty);

                $response['message'] = sprintf("<i class=\"apt-i-warning orange\"></i><p><a href='%s'>%s</a> has been added to cart.</p>", $product->getProductUrl(), $product->getTitle());
                $response['success'] = true;
                $response['error']   = '';
                $response['item_id'] = $item->getId();
            } catch (Exception $exc) {
                $response['error'] = $exc->getMessage();
            }

            if ($this->getCart()->getItems()->getSize() == 1) {
                $this->view->session  = $this->getSession();
                $this->view->cart     = $this->getCart();
                $response['template'] = $this->view->render('index/index.phtml');
            }

            $this->_helper->json($response);
        }


        if ($product->getUserId() == $this->getSession()->getUser()->getId()) {
            $this->_redirect('catalog');
        }

        if (!$product->getId() || !$product->getIsPublished()) {
            $session->addMessage('error', "This product does not exist.");
            $this->_redirect('cart/index');
        }

        if (!$product->isSaleable()) {
            $session->addMessage('error', sprintf("<a href='%s'>%s</a> is not saleable.", $product->getProductUrl(), $product->getTitle()));
            $this->_redirect('cart/index');
        }

        try {
            foreach ($this->getSession()->getUser()->getWishlist()->getAllItems() as $item) {
                if ($item->getProductId() == $product->getId()) {
                    $item->delete();
                }
            }
            $this->getCart()->addProduct($product, $this->getRequest()->getParam('num', 1));

            $session->addMessage('success', sprintf("<a href='%s'>%s</a> has been added to cart.", $product->getProductUrl(), $product->getTitle()));
        } catch (Exception $exc) {
            //echo $exc->getTraceAsString();
            $session->addMessage('error', $exc->getMessage());
        }

        if ($this->getRequest()->getParam('redirect') == 'checkout') {
            $this->_redirect('checkout/onestep');
        }

        $this->_redirect('cart/index');
    }

    public function getFreeShippingConditionAction()
    {
        $rqst = $this->getRequest();
        if (!$rqst->isXmlHttpRequest())
            throw new Zend_Controller_Action_Exception('Page not found', 404);

        $SFCollection = new Application_Model_ShippingFeeRule_Collection();

        $condition = $SFCollection->findBestCondition();
        $response  = array('success' => 'false');
        if ($condition) {
            $response['success']   = true;
            $response['condition'] = $condition->toArray(array('operator', 'sum'));
        }
        exit(Zend_Json::encode($response));
    }

    public function updateItemsAction()
    {
        $cart     = $this->getCart();
        $request  = $this->getRequest();
        $response = array(
            'success'  => false,
            'error'    => true,
            'subtotal' => '$0.00',
        );

        if ($request->isXmlHttpRequest() && $request->isPost()) {

            foreach ((array) $request->getPost('items') as $itemId => $params) {
                $item = new Cart_Model_Item($itemId);

                if ($item->getId() && $item->getCartId() == $cart->getId()) {
                    $qty      = isset($params['qty']) ? (int) $params['qty'] : 1;
                    $isActive = isset($params['is_active']) ? (bool) $params['is_active'] : false;
                    $item->setQty($qty)->setIsActive($isActive)->save();
                }
            }
            $cart->collectTotals()->save();

            $response['success'] = true;
            $response['error']   = false;


            $existingItems = array();

            foreach ($cart->getItems() as $cartItem) {
                $existingItems[$cartItem->getProduct()->getUserId()][] = $cartItem->getProduct()->getId();
            }

            $suggestions             = new Product_Model_Product_Collection();
            $this->view->suggestions = $suggestions->getCartSuggestions($existingItems);

            $response['sidebar_template'] = $this->view->render('index/index/suggestions.phtml');
        }

        $response['subtotal'] = (string) $this->view->currency($cart->getSubtotal());

        $this->_helper->json($response);
    }

    public function removeItemAction()
    {
        $cart     = $this->getCart();
        $request  = $this->getRequest();
        $response = array(
            'success'  => false,
            'error'    => true,
            'message'  => 'Cannot delete the item from cart',
            'template' => null,
        );

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $itemId = $request->getPost('id');
            $item   = new Cart_Model_Item($itemId);

            if ($item->getId() && $item->getCartId() == $cart->getId()) {
                $product = $item->getProduct();

                $message = '<i class="apt-i-warning orange"></i><p><a href="%s">%s</a> has been deleted from your shopping cart. <a class="restore" href="%s">Restore</a></p>';
                $url1    = $this->view->escape($product->getProductUrl());
                $title   = $this->view->escape($product->getTitle());
                $url2    = $this->view->escape($product->getAddToCartUrlAjax($item->getQty()));

                $response['success'] = true;
                $response['error']   = false;
                $response['message'] = sprintf($message, $url1, $title, $url2);

                $item->delete();
            }

            if (!$cart->getItemsCollection()->getSize()) {
                $this->view->messages = $this->getSession()->getMessages('error');
                $response['template'] = $this->view->render('index/empty.phtml');
            }
        }
        $this->_helper->json($response);
    }

}