<?php

abstract class Core_Model_Abstract extends Ikantam_Object
{

    protected $_backendClass;
    protected $_backend;
    protected $_table;
    protected $_fields = array();

    public function __construct()
    {
        if (!$this->_backendClass) {
            $this->_backendClass = get_class($this) . '_Backend';
        }
        //parent::__construct();
    }

    public function getIsLoaded()
    {
        return $this->getData('is_loaded');
    }

    public function setIsLoaded($isLoaded)
    {
        return $this->setData('is_loaded', $isLoaded);
    }

    public function getIsDeleted()
    {
        return $this->getData('is_deleted');
    }

    public function setIsDeleted($isDeleted)
    {
        return $this->setData('is_deleted', $isDeleted);
    }

    public function getFields()
    {
        return $this->_fields;
    }

    protected function _getBackend()
    {
        if (!$this->_backend) {
            $this->_backend = new $this->_backendClass();
        }
        return $this->_backend;
    }

    protected function getBind()
    {
        $fields = $this->getFields();
        $bind = array();

        foreach ($this->getData() as $field => $value) {
            if (in_array($field, $fields)) {
                $bind[$field] = $value;
            }
        }
        return $bind;
    }

    public function load($var)
    {
        $params = array();
        
        if (is_integer($var) || is_string($var)) {
            $params = array('id' => $var);
        } elseif (is_array($var)) {
            $params = $var;
        }
        
        $row = $this->_getBackend()->select($this->_table, $params);
        if ($row) {
            $this->addData($row);
        }
        return $this;
    }

    public function save()
    {
        if ($this->getIsDeleted() && $this->getId()) {
            $this->_delete();
        } elseif (!$this->getIsDeleted() && $this->getId()) {
            $this->_update();
        } elseif (!$this->getIsDeleted() && !$this->getId()) {
            $this->_insert();
        }

        return $this;
    }

    public function delete()
    {
        $this->setIsDeleted(1)->save();//@TODO: do we really need this?
    }

    protected function _insert()
    {
        $rows = $this->_getBackend()
                ->insert($this->_table, $this->getBind());

        if ($rows) {
            $this->setId($this->_getBackend()->getLastInsertId());
        } else {
            die('Unable to insert row');
        }
    }

    protected function _update()
    {
        $rows = $this->_getBackend()
                ->update($this->_table, $this->getBind(), array('id' => $this->getId()));
    }

    protected function _delete()
    {
        $rows = $this->_getBackend()
                ->delete($this->_table, array('id' => $this->getId()));
    }

}