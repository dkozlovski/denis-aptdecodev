<?php

abstract class Core_Model_Abstract_Backend
{

    protected $_autoQuoteIdentifiers = true;
    protected $_orderField;

    protected function where($where)
    {
        if (!empty($where)) {
            return ' WHERE ' . $where;
        }
        return '';
    }
    
    protected function from($tableName)
    {
        return ' FROM ' . $this->quote($tableName);
    }

    protected function limit($limit)
    {
        if (!empty($limit)) {
            return ' LIMIT ' . $limit;
        }
        return '';
    }
    
    protected function modifiers($modifiers)
    {
        if (count($modifiers) > 0) {
            return implode(' ', $modifiers);
        }
        return '';
    }
    
    protected function partitions($partitions)
    {
        if (count($partitions) > 0) {
            return ' PARTITION (' . implode(', ', $partitions) . ')';
        }
        return '';
    }
    
    protected function orderBy($orderBy)
    {
        if (count($orderBy) > 0) {
            return ' ORDER BY ' . implode(', ', $orderBy);
        }
        return '';
    }
    
    public function getLastInsertId()
    {
        return $this->getConnection()->lastInsertId();
    }

    /**
     * DELETE Syntax
     * 
     * http://dev.mysql.com/doc/refman/5.6/en/delete.html
     * DELETE [LOW_PRIORITY] [QUICK] [IGNORE] FROM tbl_name
     * [PARTITION (partition_name,...)]
     * [WHERE where_condition]
     * [ORDER BY ...]
     * [LIMIT row_count]
     * 
     * @param string $tableName
     * @param array $whereCondition
     * @return int
     *
     * public function delete($modifiers, $tableName, $partitions, $where, $orderBy, $limit)
     */
    public function delete($tableName, $whereCondition = array(), $orderBy = array(), $limit = null)
    {
        $bind = array();
        $where = $this->_whereExpr($whereCondition, $bind);

        $sql = 'DELETE'
                //. $this->modifiers($modifiers)
                . $this->from($tableName)
                //. $this->partitions($partitions)
                . $this->where($where)
                . $this->orderBy($orderBy)
                . $this->limit($limit);

        $stmt = $this->query($sql, $bind);
        $result = $stmt->rowCount();
        return $result;
    }

    public function select($tableName, array $whereCondition)
    {
        $bind = array();
        $where = $this->_whereExpr($whereCondition, $bind);

        $sql = 'SELECT * FROM ' . $this->quote($tableName)
                . ' WHERE ' . $where;

        $stmt = $this->query($sql, $bind);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function selectC($tableName, $whereCondition, array $bind, $order, $limit)
    {
        //$bind = array();
        //$where = $this->_whereExpr($whereCondition, $bind);
        if (!$whereCondition) {
            $whereCondition = array('1 = 1');
        }

        $sql = 'SELECT * FROM ' . $this->quote($tableName)
                . ' WHERE ' . implode(' AND ', $whereCondition);

        if ($order) {
            $sql .= $order;
        }

        if ($limit) {
            $sql .= $limit;
        }

        $stmt = $this->query($sql, $bind);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * http://dev.mysql.com/doc/refman/5.6/en/update.html
     * UPDATE Syntax
     * 
     * UPDATE [LOW_PRIORITY] [IGNORE] table_reference
     * SET col_name1={expr1|DEFAULT} [, col_name2={expr2|DEFAULT}] ...
     * [WHERE where_condition]
     * [ORDER BY ...]
     * [LIMIT row_count]
     * 
     * @param type $tableName
     * @param array $bind
     * @param array $whereCondition
     * @return type
     */
    public function update($tableName, array $bind, array $whereCondition)
    {
        $set = array();

        foreach ($bind as $field => $value) {
            unset($bind[$field]);
            $bind[':' . $field] = $value;
            $set[] = $this->quote($field) . ' = ' . ':' . $field;
        }

        $where = $this->_whereExpr($whereCondition, $bind);

        $sql = 'UPDATE ' . $this->quote($tableName)
                . ' SET ' . implode(', ', $set)
                . ' WHERE ' . $where;

        $stmt = $this->query($sql, $bind);
        $result = $stmt->rowCount();
        return $result;
    }

    /**
     * http://dev.mysql.com/doc/refman/5.6/en/insert.html
     * INSERT Syntax
     * 
     * INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
     * [INTO] tbl_name
     * [PARTITION (partition_name,...)] 
     * [(col_name,...)]
     * {VALUES | VALUE} ({expr | DEFAULT},...),(...),...
     * [ ON DUPLICATE KEY UPDATE
     *   col_name=expr
     *     [, col_name=expr] ... ]
     * 
     * Or:
     * 
     * INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
     * [INTO] tbl_name
     * [PARTITION (partition_name,...)]
     * SET col_name={expr | DEFAULT}, ...
     * [ ON DUPLICATE KEY UPDATE
     *   col_name=expr
     *     [, col_name=expr] ... ]
     *
     * 
     * @param type $tableName
     * @param array $bind
     * @return int
     */
    public function insert($tableName, array $bind)
    {
        $cols = array();
        $vals = array();

        foreach ($bind as $field => $value) {
            $cols[] = $this->quote($field, true);
            unset($bind[$field]);
            $bind[':' . $field] = $value;
            $vals[] = ':' . $field;
        }

        $sql = 'INSERT INTO ' . $this->quote($tableName)
                . ' (' . implode(', ', $cols) . ') '
                . 'VALUES (' . implode(', ', $vals) . ')';

        try {
            $stmt = $this->query($sql, $bind);
            $result = $stmt->rowCount();
            return $result;
        } catch (PDOException $e) {
            //($e->getMessage());
        }
        
        return 0;
    }

    /**
     * 
     * @param type $sql
     * @param type $bind
     * @return PDOStatement
     */
    public function query($sql, $bind)
    {
        $stmt = $this->getConnection()->prepare($sql);
        $stmt->execute($bind);
        return $stmt;
    }

    /**
     * 
     * @return PDO
     */
    protected function getConnection()
    {
        return Application_Model_DbFactory::getFactory()->getConnection();
    }

    protected function _whereExpr($where, &$bind)
    {
        $wh = array();
        foreach ($where as $field => $value) {
            $bind[':where_' . $field] = $value;
            $wh[] = $this->quote($field) . ' = ' . ':where_' . $field;
        }

        return implode(' AND ', $wh);
    }

    public function quote($value)
    {
        $q = $this->getQuoteIdentifierSymbol();
        return ($q . str_replace("$q", "$q$q", $value) . $q);
    }

    public function getQuoteIdentifierSymbol()
    {
        return '`';
    }

}