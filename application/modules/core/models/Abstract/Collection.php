<?php

abstract class Core_Model_Abstract_Collection extends Ikantam_Collection
{

    const MAX_LIMIT = 2147483647;//Usual value of PHP_INT_MAX for 32-bit systems

    protected $_backendClass;
    protected $_table;
    protected $_filters = array();
    protected $_bind = array();
    protected $_backend;
    protected $_items = array();
    protected $_orders = array();
    protected $_offset;
    protected $_limit;
    protected $_itemClass;
    protected $_isLoaded;

    public function __construct()
    {
        if (!$this->_backendClass) {
            $this->_backendClass = str_replace('_Collection', '_Backend', get_class($this));
        }
        if (!$this->_itemClass) {
            $this->_itemClass = str_replace('_Collection', '', get_class($this));
        }
        parent::__construct();
    }

    public function getTable()
    {
        return $this->_table;
    }

    protected function _getBackend()
    {
        if (!$this->_backend) {
            $this->_backend = new $this->_backendClass();
        }
        return $this->_backend;
    }

    public function setTable($table)
    {
        $this->_table = $table;
        return $this;
    }

    abstract public function getFields();

    protected function getOperator($type)
    {
        $operators = array(
            'eq' => ' = ',
            'neq' => ' != ',
            'lt' => ' < ',
            'gt' => ' > ',
            'gteq' => ' >= ',
            'lteq' => ' <= ',
            'like' => ' LIKE ',
            'nlike' => ' NOT LIKE ',
            'isnull' => ' IS NULL',
            'nisnull' => ' NOT IS NULL',
            'in' => ' IN ',
            'nin' => ' NOT IN ',
        );

        return $operators[$type];
    }

    /*
      http://dev.mysql.com/doc/refman/5.6/en/comparison-operators.html
     */

    public function addFilter($field, $value, $type = 'eq')
    {
        if (!in_array($field, $this->getFields())) {
            return $this;
        }

        $type = strtolower($type);

        switch ($type) {
            case 'eq' :
            case 'neq' :
            case 'lt' :
            case 'gt' :
            case 'gteq' :
            case 'lteq' :
            case 'like' :
            case 'nlike' :
                if (!isset($this->_bind[':where_' . $field])) {
                    $this->_filters[] = $this->quote($field) . $this->getOperator($type) . ':where_' . $field;
                    $this->_bind[':where_' . $field] = $value;
                } else {
                    $this->_filters[] = $this->quote($field) . $this->getOperator($type) . ':where_' . $field . '0';
                    $this->_bind[':where_' . $field . '0'] = $value;
                }

                break;

            case 'isnull' :
            case 'nisnull' :
                $this->_filters[] = $this->quote($field) . $this->getOperator($type);
                break;

            case 'in' :
            case 'nin' :
                foreach ($value as $key => $val) {
                    $ins[] = ':where_' . $field . $key;
                    $this->_bind[':where_' . $field . $key] = $val;
                }

                $this->_filters[] = $this->quote($field) . $this->getOperator($type) . '(' . implode(', ', $ins) . ')';
                break;

            case 'between' :
            case 'nbetween' :
                break;
        }

        return $this;
    }

    public function load($printQuery = false, $logQuery = false)
    {
        if ($this->_isLoaded) {
            return $this;
        }

        $rows = $this->_getBackend()->selectC($this->_table, $this->_filters, $this->_bind, $this->getOrder(), $this->getOffsetLimit());

        if ($rows) {
            foreach ($rows as $row) {
                $item = new $this->_itemClass();
                $item->addData($row);
                $this->addItem($item);
            }
        }
        
        $this->_isLoaded = true;
        return $this;
    }

    public function addOrder($field, $direction)
    {
        $direction = (strtolower($direction) == 'desc') ? $direction : 'asc';
        $this->_orders[$field] = $direction;
        return $this;
    }

    public function setOrder(array $order)
    {
        foreach ($order as $field => $direction) {
            $this->addOrder($field, $direction);
        }
        return $this;
    }

    public function setOrderDir($order)
    {
        $this->_orderDir = $order;
        return $this;
    }

    public function setOffset($offset)
    {
        $this->_offset = $offset;
        return $this;
    }

    public function setLimit($limit)
    {
        $this->_limit = $limit;
        return $this;
    }

    protected function getOrder()
    {
        if (count($this->_orders)) {
            $order = array();
            foreach ($this->_orders as $field => $dir) {
                $order[] = '`' . $field . '` ' . $dir;
            }
            return ' ORDER BY ' . implode(', ', $order);
        }
        return null;
    }

    public function getLimit()
    {
        return isset($this->_limit) ? $this->_limit : self::MAX_LIMIT;
    }

    public function getOffset()
    {
        return isset($this->_offset) ? $this->_offset : 0;
    }

    protected function getOffsetLimit()
    {
        return sprintf(' LIMIT %d, %d ', $this->getOffset(), $this->getLimit());
    }

    public function quote($value)
    {
        $q = $this->getQuoteIdentifierSymbol();
        return ($q . str_replace("$q", "$q$q", $value) . $q);
    }

    public function getQuoteIdentifierSymbol()
    {
        return '`';
    }

}