<?php

class Core_Model_Session_Backend extends Core_Model_Abstract_Backend
{

	protected $_table = 'sessions';

	protected function insert(\Application_Model_Abstract $session)
	{
		return $session;
	}

	protected function update(\Application_Model_Abstract $session)
	{
		return $session;
	}

	public function read($id, $name)
	{
		$sql = "SELECT * FROM `" . $this->getTableName() . "` WHERE `id` = :id AND `name` = :name LIMIT 1;";

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':name', $name);

		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		return $row;
	}

	public function write($data, $where = null)
	{
		if ($where === null) {
			$sql = 'INSERT INTO `' . $this->getTableName() . '` (`id`, `name`, `modified`, `lifetime`, `data`) values (:id, :name, :modified, :lifetime, :data);';

			$stmt = $this->_getConnection()->prepare($sql);

			$id = $data[3];
			$name = $data[4];
			$modified = $data[0];
			$lifetime = $data[2];
			$data2 = $data[1];

			$stmt->bindParam(':modified', $modified);
			$stmt->bindParam(':data', $data2);
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':name', $name);
			$stmt->bindParam(':lifetime', $lifetime);

			$stmt->execute();
		} else {
			$sql = 'UPDATE `' . $this->getTableName() . '` set `modified` = :modified, `data` = :data WHERE `id` = :id AND `name` = :name;';

			$stmt = $this->_getConnection()->prepare($sql);

			$modified = $data[0];
			$data2 = $data[1];
			$id = $where[0];
			$name = $where[1];

			$stmt->bindParam(':modified', $modified);
			$stmt->bindParam(':data', $data2);
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':name', $name);

			$stmt->execute();
		}

		return true;
	}

	public function destroy($id, $name)
	{
		$sql = 'DELETE DROM `' . $this->getTableName() . '` WHERE `id` = :id AND `name` = :name;';

		$stmt = $this->_getConnection()->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':name', $name);

		$stmt->execute();

		return true;
	}

	public function gc()
	{
		$sql = 'DELETE FROM `' . $this->getTableName() . '` WHERE `modified` + `lifetime` < :time';

		$stmt = $this->_getConnection()->prepare($sql);

		$time = time();
		$stmt->bindParam(':time', $time);
		$stmt->execute();

		return true;
	}

}
