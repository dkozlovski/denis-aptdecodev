<?php

class Core_Model_Session_Handler implements Zend_Session_SaveHandler_Interface
{

	/**
	 * Session Save Path
	 *
	 * @var string
	 */
	protected $sessionSavePath;

	/**
	 * Session Name
	 *
	 * @var string
	 */
	protected $sessionName;

	/**
	 * Lifetime
	 * @var int
	 */
	protected $lifetime;

	/**
	 * Zend Db Table Gateway
	 * @var TableGateway
	 */
	protected $tableGateway;

	/**
	 * DbTableGateway Options
	 * @var DbTableGatewayOptions
	 */
	protected $options;

	/**
	 * Constructor
	 *
	 * @param TableGateway          $tableGateway
	 * @param DbTableGatewayOptions $options
	 */
	public function __construct($options)
	{
		$this->tableGateway = new Core_Model_Session_Backend();
		$this->options = $options;
	}

	/**
	 * Open Session
	 *
	 * @param  string  $savePath
	 * @param  string  $name
	 * @return boolean
	 */
	public function open($savePath, $name)
	{
		$this->sessionSavePath = $savePath;
		$this->sessionName = $name;
		$this->lifetime = ini_get('session.gc_maxlifetime');

		return true;
	}

	/**
	 * Close session
	 *
	 * @return boolean
	 */
	public function close()
	{
		return true;
	}

	/**
	 * Read session data
	 *
	 * @param  string $id
	 * @return string
	 */
	public function read($id)
	{
		$row = $this->tableGateway->read($id, $this->sessionName);

		if (is_array($row)) {
			$modified = $row['modified'];
			$lifetime = $row['lifetime'];

			if ($modified + $lifetime > time()) {
				return $row['data'];
			}
			$this->destroy($id);
		}

		return '';
	}

	/**
	 * Write session data
	 *
	 * @param  string  $id
	 * @param  string  $data
	 * @return boolean
	 */
	public function write($id, $data)
	{
		$data = array(time(), (string) $data);

		$row = $this->tableGateway->read($id, $this->sessionName);

		if (is_array($row)) {
			return (bool) $this->tableGateway->write($data, array($id, $this->sessionName));
		}

		$data[] = $this->lifetime;
		$data[] = $id;
		$data[] = $this->sessionName;

		return (bool) $this->tableGateway->write($data);
	}

	/**
	 * Destroy session
	 *
	 * @param  string  $id
	 * @return boolean
	 */
	public function destroy($id)
	{
		return (bool) $this->tableGateway->destroy($id, $this->sessionName);
	}

	/**
	 * Garbage Collection
	 *
	 * @param  int  $maxlifetime
	 * @return true
	 */
	public function gc($maxlifetime)
	{
		return (bool) $this->tableGateway->gc();
	}

}
