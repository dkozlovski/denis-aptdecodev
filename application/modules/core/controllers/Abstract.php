<?php

abstract class Core_AbstractController extends Zend_Controller_Action
{

	protected $_sessionModel;
	protected $_session;

	public function __construct(\Zend_Controller_Request_Abstract $request, \Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
	{
		$this->setSession(new $this->_sessionModel());
		parent::__construct($request, $response, $invokeArgs);
	}

	/**
	 * 
	 * @param Ikantam_Session $session
	 * @return Ikantam_Controller_Abstract
	 */
	protected function setSession(Ikantam_Session $session)
	{
		$this->_session = $session;
		return $this;
	}

	/**
	 * 
	 * @return Ikantam_Session
	 */
	protected function getSession()
	{
		return $this->_session;
	}

	protected function addError($errorMessage)
	{
		$this->getSession()->addError($errorMessage);
	}

	protected function addErrors(array $errorMessages)
	{
		$this->getSession()->addErrors($errorMessages);
	}

	/**
	 * Log Exception to file
	 * 
	 * @param Exception $exception
	 * @param boolean $logParams
	 */
	protected function logException(Exception $exception, $logParams = true)
	{
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/log/exception.log');
		$logger->addWriter($writer);
		$logger->log($exception, Zend_Log::INFO); //@TODO: priority?
		if ($logParams) {
			$logger->log(var_export($this->getRequest()->getParams(), true), Zend_Log::INFO);
		}
	}

}
