<?php

class Checkout_PaymentController extends Ikantam_Controller_Front
{

    protected $_comission         = 15;
    protected $_shippingComission = 0;
    
    protected function getShippingComission($sum)
    {
        $options   = new Application_Model_Options();
        $comission = $options->getFee($sum, $options::BUYER);
        return ($comission) ? $comission : $this->_shippingComission;
    }

    public function init()
    {
        $this->_helper->redirector('index', 'onestep', 'checkout');
        
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }
    }

    public function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }

    public function indexAction()
    {
        $this->view->session = $this->getSession();

        $this->view->addresses  = new User_Model_Address_Collection();
        $this->view->addresses->getByUserId($this->getSession()->getUserId());
        $this->view->activeMenu = 'payment';

        $couponCode         = $this->getSession()->getCoupon();
        $this->view->coupon = new Cart_Model_Coupon($couponCode);
    }

    public function postAction()
    { 
        $user        = $this->getSession()->getUser();
        $paymentData = $this->getRequest()->getPost();

        $form         = new Checkout_Form_Payment();
        //$SFCollection = new Application_Model_ShippingFeeRule_Collection();

        if ($form->isValid($paymentData)) {
            $dbh = Application_Model_DbFactory::getFactory()->getConnection();
            $dbh->beginTransaction();

            $cart = $this->getSession()->getCart();

            $cartTotal             = $cart->calculateSubTotal(); // subtotal
            $shipping              = $cart->calculateShippingPriceByZipCode(null, true);
            //$deliveryPrice         = 0;
            //$numberOfDeliveryItems = 0;
            $comission             = $cart->calculateTransactionFeePercent();

            /*foreach ($cart->getAllDeliveredItems() as $item) {
                $productPrice = $item->getProduct()->getPrice(); 
                $comission    = $this->getShippingComission($productPrice);
                $cartTotal += $productPrice * $item->getQty(1);
                if ($item->getShipping() == 'delivery') {
                    $deliveryPrice += $productPrice * $item->getQty(1);
                    $numberOfDeliveryItems += $item->getQty(1);
                }
            }

            $shipping = $SFCollection->getShippingFee($deliveryPrice, $numberOfDeliveryItems);*/
            $order    = new Order_Model_Order();
            $order->setUserId($user->getId())
                    ->setCreatedAt(time())
                    ->setTotal($cartTotal + $shipping)
                    ->setPaymentId(null)
                    ->save();

            $userAddress = new User_Model_Address();

            $userAddress->setUserId($user->getId())
                    ->setFullName($form->getValue('ba_full_name'))
                    ->setCountryCode($form->getValue('ba_country'))
                    ->setCity($form->getValue('ba_city'))
                    ->setState($form->getValue('ba_region'))
                    ->setAddressLine1($form->getValue('ba_line1'))
                    ->setAddressLine2($form->getValue('ba_line2'))
                    ->setPostcode($form->getValue('ba_postcode'))
                    ->setIsPrimary(0)
                    ->save();

            $billingAddress = new Order_Model_Order_Address();
            $billingAddress->setOrderId($order->getId())
                    ->setUserId($user->getId())
                    ->setUserAddressId($userAddress->getId())
                    ->setCountryCode($form->getValue('ba_country'))
                    ->setState($form->getValue('ba_region'))
                    ->setCity($form->getValue('ba_city'))
                    ->setPostcode($form->getValue('ba_postcode'))
                    ->setFullName($form->getValue('ba_full_name'))
                    ->setAddressLine1($form->getValue('ba_line1'))
                    ->setAddressLine2($form->getValue('ba_line2'))
                    ->setEmail(null)
                    ->setTelephone(null)
                    ->setAddressType('billing')
                    ->save();

            try {
                $couponCode = $this->getSession()->getCoupon();
                $coupon     = new Cart_Model_Coupon($couponCode);

                $payment = new Order_Model_Payment();
                $payment->createPayment($form->getValues(), $cart, $user, $comission, $order, $shipping, $coupon);

                $order->setPaymentId($payment->getId())->save();


                if ($coupon->getId() && $coupon->isValid($this->getSession()->getUserId())) {
                    $coupon->addUse($this->getSession()->getUserId());
                }

                $dbh->commit();

                $this->getSession()->setCoupon(null);
            } catch (Ikantam_Balanced_Exception $exception) {
                $this->getSession()->addMessage('error', $exception->getMessage());
                $this->getSession()->addFormData($paymentData, null, array('cc_number', 'cc_code', 'cc_expiry_month', 'cc_expiry_year'));
                $dbh->rollBack();
                $this->_redirect('checkout/payment');
            }

            $this->sendEmails($order);

            $this->_helper->redirector('index', 'success', 'checkout', $params = array('id' => $order->getId()));
        } else {
            $this->getSession()->addFormErrors($form->getFlatMessages());
            $this->getSession()->addMessage('error', 'Opps, looks like one or more of the fields was left blank. Please ensure all fields marked with an asterisk (*) are filled out');
        }


        $this->getSession()->addFormData($paymentData, null, array('cc_number', 'cc_code', 'cc_expiry_month', 'cc_expiry_year'));
        $this->_redirect('checkout/payment');
    }

    protected function sendEmails($order)
    {
        foreach ($order->getItemsForPickUp() as $item) {
            if ($item->getIsReceived() || $item->getIsDelivered()) {
                continue;
            }

            $seller = $item->getProduct()->getSeller();
            $buyer  = $this->getSession()->getUser();

            $this->view->buyer   = $buyer;
            $this->view->product = $item->getProduct();
            $this->view->seller  = $seller;
            $this->view->item    = $item;
            $this->view->order   = $order;

            if ($seller->getMainEmail()) {
                $output = $this->view->render('order_notification_pickup_to_seller.phtml');

                $toSend = array(
                    'email'   => $seller->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                $mail->sendCopy($output);
                $mail->send();
            }

            if ($buyer->getMainEmail()) {
                $output2 = $this->view->render('order_notification_pickup_to_buyer.phtml');

                $toSend2 = array(
                    'email'   => $buyer->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output2);

                $mail2 = new Ikantam_Mail($toSend2, 'utf-8', 'hello@aptdeco.com');
                $mail2->sendCopy($output2);
                $mail2->send();
            }
        }

        foreach ($order->getItemsForDelivery() as $item) {
            if ($item->getIsReceived() || $item->getIsDelivered()) {
                continue;
            }

            $seller = $item->getProduct()->getSeller();
            $buyer  = $this->getSession()->getUser();

            $this->view->buyer   = $buyer;
            $this->view->product = $item->getProduct();
            $this->view->item    = $item;
            $this->view->seller  = $seller;
            $this->view->order   = $order;

            if ($seller->getMainEmail()) {
                $output = $this->view->render('order_notification_delivery_to_seller.phtml');

                $toSend = array(
                    'email'   => $seller->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                $mail->sendCopy($output);
                $mail->send();
            }

            if ($buyer->getMainEmail()) {
                $output2 = $this->view->render('order_notification_delivery_to_buyer.phtml');

                $toSend2 = array(
                    'email'   => $buyer->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output2);

                $mail2 = new Ikantam_Mail($toSend2, 'utf-8', 'hello@aptdeco.com');
                $mail2->sendCopy($output2);
                $mail2->send();
            }
        }
    }

    public function validateAddressAction()
    {
        $data = $this->getRequest()->getPost();

        $form = new Checkout_Form_BillingAddress();


        if ($form->isValid($data)) {
            $response['success'] = true;
        } else {
            $response['success'] = false;
            $response['error']   = $form->getFlatMessages();
        }
        echo Zend_Json::encode($response);
        die();
    }

    public function setCouponAction()
    {
        $redirect = $this->getRequest()->getPost('action');

        switch ($redirect) {
            case 'auth' : $redTo = 'checkout/guest';
                break;
            case 's-method' : $redTo = 'checkout/shipping-method';
                break;
            case 'delivery' : $redTo = 'checkout/delivery';
                break;
            case 's-address' : $redTo = 'checkout/shipping-address';
                break;
            case 'payment' : $redTo = 'checkout/payment';
                break;
            default: $redTo = 'checkout/guest';
                break;
        }

        $couponCode = $this->getRequest()->getPost('coupon');

        $coupon = new Cart_Model_Coupon($couponCode);

        if (!$coupon->getId() || !$coupon->isValid($this->getSession()->getUserId())) {
            //@TODO: return error
        } else {
            $this->getSession()->setCoupon($coupon->getCode());
        }

        $this->_redirect($redTo);
    }

}
