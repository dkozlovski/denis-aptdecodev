<?php

class Checkout_SuccessController extends Ikantam_Controller_Front
{

    protected $_range;
    protected $_collection;

    public function init()
    {
        /* if (!$this->getSession()->isLoggedIn()) {
          $this->_redirect('user/login/index');
          } */

        $this->_range      = $this->getRnge();
        $this->_collection = new Admin_Model_DeliveryDate_Collection();
        $this->_collection->getByDateRange($this->_range);
    }

    public function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }

    /**
     * @param int $days
     * @param int $offset - from current date in days
     * @return array
     */
    protected function getRnge($days = 70, $offset = 2)
    {
        $current = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        if ($offset)
            $current += $offset * 86400;
        return array($current, $current + (86400 * $days));
    }

    protected function getTableDates($range, $startDate, $endDate, $format = 'd M Y D')
    {
        $aux    = array();
        $aux2   = array();
        $result = array();

        for ($i = $range[0]; $i < $range[1]; $i += 86400) {
            if ($startDate && $i < $startDate) {
                continue;
            }
            if ($endDate && $i > $endDate) {
                continue;
            }
            $aux2[date($format, $i)] = array(0, 0, 0);
        }

        foreach ($this->_collection->getItems() as $date) {
            if (key_exists($date->getDateString(), $aux2)) {
                //$aux[$date->getDateString()] = array($date->getIsFirstPeriodAvailable(), $date->getIsSecondPeriodAvailable(), $date->getIsThirdPeriodAvailable());

                $datePeriods                 = array($date->getIsFirstPeriodAvailable(), $date->getIsSecondPeriodAvailable(), $date->getIsThirdPeriodAvailable());
                if ($datePeriods[0] || $datePeriods[1] || $datePeriods[2])
                    $aux[$date->getDateString()] = $datePeriods; //if at least one period available, get this date, else ignore it.
            }
        }

        foreach (array_keys($aux) as $stringDate) {
            $result[] = array_merge($aux[$stringDate], array('stringDate' => $stringDate));
        }

        return $result;
    }

    public function indexAction()
    {
        $orderId = $this->getRequest()->getParam('id');
        $order   = new Order_Model_Order($orderId);

        if ($this->getSession()->getLastOrderId() != $orderId) {
            $this->_redirect('cart/index');
        }

        if (!$order->getId()) {
            $this->_redirect('cart/index');
        }

        $this->getSession()->setLastOrderId(null);

        $this->view->order   = $order;
        $this->view->session = $this->getSession();
        $this->view->allset  = false;


        if (APPLICATION_ENV === 'development') {
            $referer = new Application_Model_Referer();
            if ($referer->findReferer($order)) {
                $referer->setOrderId($order->getId())
                        ->save();
            }
        }
        $startDate = null;
        $endDate   = null;

        foreach ($this->view->order->getItemsForDelivery() as $item) {
            if ($item->getBuyerDates()) {
                $this->view->allset = true;
            }
            $product = $item->getProduct();

            if ($product->getAvailableFrom()) {
                if (!$startDate) {
                    $startDate = $product->getAvailableFrom();
                } elseif ($product->getAvailableFrom() > $startDate) {
                    $startDate = $product->getAvailableFrom();
                }
            }

            if ($product->getAvailableTill()) {
                if (!$endDate) {
                    $endDate = $product->getAvailableTill();
                } elseif ($product->getAvailableTill() < $endDate) {
                    $endDate = $product->getAvailableTill();
                }
            }
        }

        $tableDates                = $this->getTableDates($this->_range, $startDate, $endDate);
        $this->view->tableDates    = $tableDates;
        $this->view->selectedDates = $this->getSession()->getFormData('delivery_dates');
        $this->view->addressDecorator = new Ikantam_Form_Decorator_BbCode(array('tag' => 'p'));
    }

}