<?php

class Checkout_IndexController extends Ikantam_Controller_Front
{

	public function init()
	{
        $this->_helper->redirector('index', 'onestep', 'checkout');
        
		/*if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}*/
	}
    
    public function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }    

    public function indexAction()
    {     
		$this->_forward('index', 'guest', 'checkout');
    }


}