<?php

class Checkout_ShippingAddressController extends Ikantam_Controller_Front
{

    public function init()
    {
        $this->_helper->redirector('index', 'onestep', 'checkout');
        
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }
    }

    public function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }

    public function indexAction()
    {
        if (count($this->getSession()->getCart()->getAllDeliveredItems()) == 0) {
            $this->getSession()->addMessage('error', 'Select Shipping Methods!');
            $this->_redirect('checkout/shipping-method');
        }

        $collection = new Cart_Model_Item_Collection();
        if (!$collection->getDeliverySelected($this->getSession()->getCart()->getId())) {
            $this->_redirect('checkout/payment');
        }

        $this->view->session    = $this->getSession();
        $this->view->addresses  = new User_Model_Address_Collection();
        $this->view->addresses->getByUserId($this->getSession()->getUserId());
        $this->view->activeMenu = 's-address';

        $couponCode         = $this->getSession()->getCoupon();
        $this->view->coupon = new Cart_Model_Coupon($couponCode);
    }

    public function setAddressAction()
    {
        $addressId = $this->getRequest()->getParam('id');

        $address = new User_Model_Address();
        $address->getById($addressId);

        if ($address->getId() && $address->getUserId() == $this->getSession()->getUserId()) {
            $this->getSession()
                    ->getCart()
                    ->setShippingAddressId($address->getId())
                    ->save();
            $this->_redirect('checkout/payment');
        }
        $this->getSession()->addMessage('error', 'Select Shipping Address!');
        $this->_redirect('checkout/shipping-address');
    }

    public function postAction()
    {
        $post = $this->getRequest()->getPost();

        $phone             = $this->getRequest()->getParam('phone', false);
        $this->view->phone = $phone;

        $form = new User_Form_Address();

        try {
            if ($form->isValid($post) && $phone) {
                $address = new User_Model_Address();

                $address->addData($form->getValues())
                        ->setPhoneNumber($phone)
                        ->setUserId($this->getSession()->getUserId())
                        ->setIsPrimary(0)
                        ->save();

                $this->getSession()
                        ->getCart()
                        ->setShippingAddressId($address->getId())
                        ->save();
                $this->getSession()->activeData('_checkout_phone', $phone);
                $this->_redirect('checkout/payment');
            } else {
                $this->getSession()->addFormErrors($form->getFlatMessages());
                if (!$phone) {
                    $this->getSession()->setFormErrors('phone', 'Phone is required.');
                }
            }
        } catch (Exception $e) {
            //	var_dump($e);
            die();
        }

        $this->getSession()->addMessage('error', 'Select Shipping Address!');
        $this->getSession()->addFormData($post);
        $this->_redirect('checkout/shipping-address');
    }
    
    public function priceByCodeAction ()
    {
      /*  if(!$this->isAjax()) {
            $this->_helper->show404();
        } elseif(($code = $this->getParam('code')) && preg_match('/^\d{5}$/', $code)){
            $routePlanner = new Shipping_Model_RoutePlanner;
            $cart = $this->getSession()->getCart();
            foreach($cart->getDElive)
               
        }
        */
    }

}
