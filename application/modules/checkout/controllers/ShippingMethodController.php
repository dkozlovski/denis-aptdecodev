<?php

class Checkout_ShippingMethodController extends Ikantam_Controller_Front
{

    protected $_range;
    protected $_collection;


    public function init()
    {
        $this->_helper->redirector('index', 'onestep', 'checkout');
        
        if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
        
        $this->_range = $this->getRnge();
        $this->_collection = new Admin_Model_DeliveryDate_Collection();
        $this->_collection->getByDateRange($this->_range);        
    }
    
    public function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }    
    
    
    /**
     * @param int $days
     * @param int $offset - from current date in days
     * @return array
     */
    protected function getRnge($days = 70, $offset = 2)
    {
        $current = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        if ($offset)
            $current += $offset * 86400;
        return array($current, $current + (86400 * $days));
    }

    protected function getTableDates($range, $startDate, $endDate, $format = 'd M Y D')
    {
        $aux = array();
        $aux2 = array();
        $result = array();

        for ($i = $range[0]; $i < $range[1]; $i += 86400) {
            if ($startDate && $i < $startDate) {
                //continue;
            }
            if ($endDate && $i > $endDate) {
                //continue;
            }
            $aux2[date($format, $i)] = array(0, 0, 0);
        }

        foreach ($this->_collection->getItems() as $date) {
            if (key_exists($date->getDateString(), $aux2)) {
                //$aux[$date->getDateString()] = array($date->getIsFirstPeriodAvailable(), $date->getIsSecondPeriodAvailable(), $date->getIsThirdPeriodAvailable());

                $datePeriods = array($date->getIsFirstPeriodAvailable(), $date->getIsSecondPeriodAvailable(), $date->getIsThirdPeriodAvailable());
                if ($datePeriods[0] || $datePeriods[1] || $datePeriods[2])
                    $aux[$date->getDateString()] = $datePeriods; //if at least one period available, get this date, else ignore it.
            }
        }

        foreach (array_keys($aux) as $stringDate) {
            $result[] = array_merge($aux[$stringDate], array('stringDate' => $stringDate));
        }

        return $result;
    }

    

	public function indexAction()
	{
	   
       //$this->view->isDeliveryWindowAvailable = (bool)$this->getTableDates($this->_range, null, null);
       
		foreach ($this->getSession()->getCart()->getAllItems() as $item) {
			$product = new Product_Model_Product($item->getProductId());
			if ($product->getUserId() == $this->getSession()->getUserId()) {
				$item->delete();
			}
		}

		if (count($this->getSession()->getCart()->getAllItems()) == 0) {
			$this->_redirect('cart/index');
		}
		
		$this->getSession()->getCart()->prepareItemsDelivery(array());

		$collection = new Cart_Model_Item_Collection();
		if (!$collection->getTwoMethods($this->getSession()->getCart()->getId())) {
			$this->_redirect('checkout/delivery');
		}
		$this->view->session = $this->getSession();
		$this->view->activeMenu = 's-method';
        
        $couponCode = $this->getSession()->getCoupon();
        $this->view->coupon = new Cart_Model_Coupon($couponCode);
	}

	public function postAction()
	{
		$post = $this->getRequest()->getPost('delivery');

		$this->getSession()->getCart()->prepareItemsDelivery($post);

		if (count($this->getSession()->getCart()->getAllDeliveredItems()) == 0) {
			$this->getSession()->addMessage('error', 'Select Shipping Methods!'); 
            $this->getSession()->addFormErrors(array('delivery_method' => 'Select shipping method!'));
			$this->_redirect('checkout/shipping-method');
		}

		if (count($this->getSession()->getCart()->getAllDeliveredItems()) != count($this->getSession()->getCart()->getAllCheckoutItems())) {
			$this->getSession()->addMessage('error', 'Select Shipping Methods!');
			$this->_redirect('checkout/shipping-method');
		}

		$this->_redirect('checkout/delivery');
	}

}

