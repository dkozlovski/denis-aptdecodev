<?php

class Checkout_GuestController extends Ikantam_Controller_Front
{

	public function init()
	{
		$this->_helper->redirector('index', 'onestep', 'checkout');
	}
    
    public function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }    

    public function indexAction()
    {     
		if ($this->getSession()->isLoggedIn()) {
			$this->_forward('index', 'shipping-method', 'checkout');
		}
		$this->view->session = $this->getSession();
		$this->view->activeMenu = 'auth';
        
        $this->view->isSignIn = (bool)$this->getSession()->getMessages('signin');
        
         
        
        //$facebookCfg = $this->getXmlConfig('additional_settings', 'facebook');    
        $facebookCfg = Zend_Registry::get('config')->facebook;

        $fUrl = sprintf(  'http://www.facebook.com/dialog/oauth/?client_id=%s&redirect_uri=%s&state=%s&scope=%s',
                          $facebookCfg->appId,
                          Ikantam_Url::getUrl('user/login/facebook', array('checkout_signin' => 1)),
                          '', 
                          'email,user_birthday');
       
       $this->view->facebookUrl = $fUrl;                            
    }

	public function signUpAction()
	{
		$this->_redirect('checkout/shipping-method');
	}
	
	public function signUpSuccessAction()
	{
		
	}
	
}