<?php

class Checkout_DeliveryController extends Ikantam_Controller_Front
{

    protected $_range;
    protected $_collection;

    public function init()
    {
        $this->_helper->redirector('index', 'onestep', 'checkout');
        
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }

        $this->_range      = $this->getRnge();
        $this->_collection = new Admin_Model_DeliveryDate_Collection();
        $this->_collection->getByDateRange($this->_range);
    }

    public function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }

    /**
     * @param int $days
     * @param int $offset - from current date in days
     * @return array
     */
    protected function getRnge($days = 70, $offset = 2)
    {
        $current = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        if ($offset)
            $current += $offset * 86400;
        return array($current, $current + (86400 * $days));
    }

    protected function getTableDates($range, $startDate, $endDate, $format = 'd M Y D')
    {
        $aux    = array();
        $aux2   = array();
        $result = array();

        for ($i = $range[0]; $i < $range[1]; $i += 86400) {
            if ($startDate && $i < $startDate) {
                continue;
            }
            if ($endDate && $i > $endDate) {
                continue;
            }
            $aux2[date($format, $i)] = array(0, 0, 0);
        }

        foreach ($this->_collection->getItems() as $date) {
            if (key_exists($date->getDateString(), $aux2)) {

                $datePeriods = array($date->getIsFirstPeriodAvailable(), $date->getIsSecondPeriodAvailable(), $date->getIsThirdPeriodAvailable());
                
                if ($datePeriods[0] || $datePeriods[1] || $datePeriods[2]) {
                    $aux[$date->getDateString()] = $datePeriods; //if at least one period available, get this date, else ignore it.
                }
            }
        }

        foreach (array_keys($aux) as $stringDate) {
            $result[] = array_merge($aux[$stringDate], array('stringDate' => $stringDate));
        }

        return $result;
    }

    public function indexAction()
    {
        $couponCode         = $this->getSession()->getCoupon();
        $this->view->coupon = new Cart_Model_Coupon($couponCode);

        $collection = new Cart_Model_Item_Collection();

        if (!$collection->getDeliverySelected($this->getSession()->getCart()->getId())) {
            $this->_redirect('checkout/shipping-address');
        }

        $collection2 = new Cart_Model_Item_Collection();
        $items       = $collection2->getItemsForDelivery($this->getSession()->getCart()->getId());

        $this->view->items      = $items;
        $this->view->session    = $this->getSession();
        $this->view->activeMenu = 'delivery';

        $startDate = null;
        $endDate   = null;

        foreach ($items as $item) {
            if ($item->getBuyerDates()) {
                $this->_redirect('checkout/shipping-address');
            }
            $product = $item->getProduct();

            if ($product->getAvailableFrom()) {
                if (!$startDate) {
                    $startDate = $product->getAvailableFrom();
                } elseif ($product->getAvailableFrom() > $startDate) {
                    $startDate = $product->getAvailableFrom();
                }
            }

            if ($product->getAvailableTill()) {
                if (!$endDate) {
                    $endDate = $product->getAvailableTill();
                } elseif ($product->getAvailableTill() < $endDate) {
                    $endDate = $product->getAvailableTill();
                }
            }
        }

        $tableDates                = $this->getTableDates($this->_range, $startDate, $endDate);
        $this->view->tableDates    = $tableDates;
        $this->view->selectedDates = $this->getSession()->getFormData('delivery_dates');
    }

    public function postAction()
    {
        $dateFormat = 'Y-m-d';

        $deliveryDates = $this->getRequest()->getPost('delivery_dates');
        $this->getSession()->addFormData(array('delivery_dates' => $deliveryDates));

        $out = array();

        if (isset($deliveryDates[1]) && is_array($deliveryDates[1])) {
            foreach ($deliveryDates[1] as $deliveryDate) {
                $date = strtotime($deliveryDate);
                if ($date) {
                    $out[] = date($dateFormat, $date) . ' 8AM - 12PM';
                }
            }
        }

        if (isset($deliveryDates[2]) && is_array($deliveryDates[2])) {
            foreach ($deliveryDates[2] as $deliveryDate) {
                $date = strtotime($deliveryDate);
                if ($date) {
                    $out[] = date($dateFormat, $date) . ' 12PM - 4PM';
                }
            }
        }

        if (isset($deliveryDates[3]) && is_array($deliveryDates[3])) {
            foreach ($deliveryDates[3] as $deliveryDate) {
                $date = strtotime($deliveryDate);
                if ($date) {
                    $out[] = date($dateFormat, $date) . ' 4PM - 8PM';
                }
            }
        }

        if (count($out) >= 1 && count($out) <= 3) {

            $error = false;

            $phoneNumber   = $this->getRequest()->getPost('phone_number');
            $fos           = (int) $this->getRequest()->getPost('flights_of_stairs');
            $building_type = $this->getRequest()->getPost('building_type');
            $paperInfo     = $this->getRequest()->getPost('paperwork_info');

            if (empty($phoneNumber)) {
                $error = true;
                $this->getSession()->addMessage('error', 'Please, enter your phone number.');
            }

            if ($building_type != 'walkup') {
                $building_type = 'elevator';
            } elseif ($fos < 1 || $fos > 10) {
                $error = true;
                $this->getSession()->addMessage('error', 'Please, select number of flights of stairs.');
            }

            if (!$error) {
                $deliveryOptions = array(
                    'building_type' => $building_type,
                    'flights_of_stairs' => $fos,
                    'paperwork_info' => $paperInfo,
                    'phone_number' => $phoneNumber);

                $collection = new Cart_Model_Item_Collection();
                $items      = $collection->getItemsForDelivery($this->getSession()->getCart()->getId());

                foreach ($items as $item) {
                    $item->setBuyerDates($out)
                            ->setDeliveryOptions($deliveryOptions)
                            ->save();
                }

                $this->getSession()->addMessage('allset', 'Your item is reserved. We are confirming product availability with the seller and coordinating with our moving company. We will follow up shortly updates on your order.');
                $this->_helper->redirector('index', 'shipping-address', 'checkout');
            }
        } elseif (count($out) > 3) {
            $this->getSession()->addMessage('error', 'Please, select at most 3 delivery dates.');
        } elseif (count($out) < 1) {
            $this->getSession()->addMessage('error', 'Please, select at least 1 delivery date.');
        }

        $this->_helper->redirector('index', 'delivery', 'checkout');
    }

}