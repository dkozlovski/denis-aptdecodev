<?php

class Checkout_PaymentController2 extends Ikantam_Controller_Front
{

    protected $_comission         = 15;
    protected $_shippingComission = 0;

    /* Старое
      protected function getComission()
      {
      $options = new Application_Model_Options();
      return $options->getOption('comission_from_seller', $this->_comission);
      } */

    protected function getShippingComission($sum)
    {
        $options   = new Application_Model_Options();
        $comission = $options->getFee($sum, $options::BUYER);
        return ($comission) ? $comission : $this->_shippingComission;
    }

    public function init()
    {
        $this->_helper->redirector('index', 'onestep', 'checkout');
        
        if (!$this->getSession()->isLoggedIn()) {
            $this->_redirect('user/login/index');
        }
    }

    public function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }

    public function indexAction()
    {
        $this->view->session = $this->getSession();

        $this->view->addresses  = new User_Model_Address_Collection();
        $this->view->addresses->getByUserId($this->getSession()->getUserId());
        $this->view->activeMenu = 'payment';

        $couponCode         = $this->getSession()->getCoupon();
        $this->view->coupon = new Cart_Model_Coupon($couponCode);
    }

    protected function _saveAddresses($user, $form, $order)
    {
        $fullName    = $form->getValue('ba_full_name');
        $country     = $form->getValue('ba_country');
        $city        = $form->getValue('ba_city');
        $state       = $form->getValue('ba_region');
        $streetLine1 = $form->getValue('ba_line1');
        $streetLine2 = $form->getValue('ba_line2');
        $postcode    = $form->getValue('ba_postcode');

        $userAddress = new User_Model_Address();

        $userAddress->setUserId($user->getId())
                ->setFullName($fullName)
                ->setCountryCode($country)
                ->setCity($city)
                ->setState($state)
                ->setAddressLine1($streetLine1)
                ->setAddressLine2($streetLine2)
                ->setPostcode($postcode)
                ->setIsPrimary(0)
                ->save();

        $billingAddress = new Order_Model_Order_Address();
        $billingAddress->setUserId($user->getId())
                ->setFullName($fullName)
                ->setCountryCode($country)
                ->setCity($city)
                ->setState($state)
                ->setPostcode($postcode)
                ->setAddressLine1($streetLine1)
                ->setAddressLine2($streetLine2)
                ->setEmail(null)
                ->setTelephone(null)
                ->setAddressType('billing')
                ->setUserAddressId($userAddress->getId())
                ->setOrderId($order->getId())
                ->save();
    }

    protected function getDatabase()
    {
        return Application_Model_DbFactory::getFactory()->getConnection();
    }

    public function postAction()
    {
        $user        = $this->getSession()->getUser();
        $cart        = $this->getSession()->getCart();
        $paymentData = $this->getRequest()->getPost();
        $form        = new Checkout_Form_Payment();

        if ($form->isValid($paymentData)) {

            $this->getDatabase()->beginTransaction();

            try {
                $order = new Order_Model_Order();

                $order->createFromCart($cart)
                        ->setUserId($user->getId())
                        ->setCreatedAt(time())
                        ->save();
                
                $order->createHolds($user);

                $this->_saveAddresses($user, $form, $order);

                $this->getDatabase()->commit();
            } catch (Ikantam_Balanced_Exception $exception) {
                $this->getSession()->addMessage('error', $exception->getMessage());
                $this->getSession()->addFormData($paymentData, null, array('cc_number', 'cc_code', 'cc_expiry_month', 'cc_expiry_year'));
                $this->getDatabase()->rollBack();
                $this->_redirect('checkout/payment');
            }

            $this->sendEmails($order);

            $this->_helper->redirector('index', 'success', 'checkout', $params = array('id' => $order->getId()));
        } else {
            $this->getSession()->addFormErrors($form->getFlatMessages());
            $this->getSession()->addMessage('error', 'Opps, looks like one or more of the fields was left blank. Please ensure all fields marked with an asterisk (*) are filled out');
        }

        $this->getSession()->addFormData($paymentData, null, array('cc_number', 'cc_code', 'cc_expiry_month', 'cc_expiry_year'));
        $this->_redirect('checkout/payment');
    }

    protected function sendEmails($order)
    {
        foreach ($order->getItemsForPickUp() as $item) {
            if ($item->getIsReceived() || $item->getIsDelivered()) {
                continue;
            }

            $seller = $item->getProduct()->getSeller();
            $buyer  = $this->getSession()->getUser();

            $this->view->buyer   = $buyer;
            $this->view->product = $item->getProduct();
            $this->view->seller  = $seller;
            $this->view->item    = $item;
            $this->view->order   = $order;

            if ($seller->getMainEmail()) {
                $output = $this->view->render('order_notification_pickup_to_seller.phtml');

                $toSend = array(
                    'email'   => $seller->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                $mail->sendCopy($output);
                $mail->send();
            }

            if ($buyer->getMainEmail()) {
                $output2 = $this->view->render('order_notification_pickup_to_buyer.phtml');

                $toSend2 = array(
                    'email'   => $buyer->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output2);

                $mail2 = new Ikantam_Mail($toSend2, 'utf-8', 'hello@aptdeco.com');
                $mail2->sendCopy($output2);
                $mail2->send();
            }
        }

        foreach ($order->getItemsForDelivery() as $item) {
            if ($item->getIsReceived() || $item->getIsDelivered()) {
                continue;
            }

            $seller = $item->getProduct()->getSeller();
            $buyer  = $this->getSession()->getUser();

            $this->view->buyer   = $buyer;
            $this->view->product = $item->getProduct();
            $this->view->item    = $item;
            $this->view->seller  = $seller;
            $this->view->order   = $order;

            if ($seller->getMainEmail()) {
                $output = $this->view->render('order_notification_delivery_to_seller.phtml');

                $toSend = array(
                    'email'   => $seller->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output);

                $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
                $mail->sendCopy($output);
                $mail->send();
            }

            if ($buyer->getMainEmail()) {
                $output2 = $this->view->render('order_notification_delivery_to_buyer.phtml');

                $toSend2 = array(
                    'email'   => $buyer->getMainEmail(),
                    'subject' => 'Purchase Request - ' . $item->getProduct()->getTitle(),
                    'body'    => $output2);

                $mail2 = new Ikantam_Mail($toSend2, 'utf-8', 'hello@aptdeco.com');
                $mail2->sendCopy($output2);
                $mail2->send();
            }
        }
    }

    public function validateAddressAction()
    {
        $data = $this->getRequest()->getPost();

        $form = new Checkout_Form_BillingAddress();


        if ($form->isValid($data)) {
            $response['success'] = true;
        } else {
            $response['success'] = false;
            $response['error']   = $form->getFlatMessages();
        }
        echo Zend_Json::encode($response);
        die();
    }

    public function setCouponAction()
    {
        $redirect = $this->getRequest()->getPost('action');

        switch ($redirect) {
            case 'auth' : $redTo = 'checkout/guest';
                break;
            case 's-method' : $redTo = 'checkout/shipping-method';
                break;
            case 'delivery' : $redTo = 'checkout/delivery';
                break;
            case 's-address' : $redTo = 'checkout/shipping-address';
                break;
            case 'payment' : $redTo = 'checkout/payment';
                break;
            default: $redTo = 'checkout/guest';
                break;
        }

        $couponCode = $this->getRequest()->getPost('coupon');

        $coupon = new Cart_Model_Coupon($couponCode);

        if (!$coupon->getId() || !$coupon->isValid($this->getSession()->getUserId())) {
            //@TODO: return error
        } else {
            $this->getSession()->setCoupon($coupon->getCode());
        }

        $this->_redirect($redTo);
    }

}
