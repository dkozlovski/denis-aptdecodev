<?php

class Checkout_OnestepController extends Ikantam_Controller_Front
{

    const ERROR_CARD_DECLINED   = 'It looks like your card has been declined by your bank. Please try a different card or contact your bank for more details';
    const ERROR_CARD_NUMBER     = 'Looks like your credit card was declined. Please check the information and try again.';
    const ERROR_MISSING_FIELDS  = 'Looks like some fields are missing or left blank. Please complete all the fields highlighted in red and try again.';
    const ERROR_MISSING_ADDRESS = 'Please select shipping address.';

    //From config| see init()
    protected $_appId;
    protected $_appSecret;
    //-----------------------
    protected $_defaultSuccessRedirect         = 'home';
    protected $_defaultFailureRedirect         = 'user/login/';
    protected $_defaultSuccessCheckoutRedirect = 'checkout/onepage';
    protected $_scope                          = 'email,user_birthday';
    protected $_range;

    /**
     * @Inject("core.service.deliveryDateHelper")
     * @var \Core\Service\DeliveryDateHelper
     */
    protected $dateHelper;

    public function init()
    {
        //$cfg              = $this->getXmlConfig('additional_settings', 'facebook');
        $cfg              = Zend_Registry::get('config')->facebook;
        $this->_appId     = $cfg->appId;
        $this->_appSecret = $cfg->appSecret;
    }

    public function _initJsCss()
    {
        //$this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/ga.checkout.events.js'));
    }

    protected function _getUserAddresses()
    {
        $addresses = new User_Model_Address_Collection();
        $addresses->getByUserId($this->getSession()->getUserId());

        $options = array();
        foreach ($addresses as $address) {
            $options[] = $address->getId();
        }
        return $options;
    }

    protected function _validateShippingMethods()
    {
        $cart = $this->getSession()->getCart();

        if ($cart->getItemsForCheckout()->getSize() != $cart->getItemsForDelivery()->getSize() + $cart->getItemsForPickup()->getSize()) {
            return false;
        }

        return true;
    }

    protected function _saveUserData($form)
    {
        if ($this->getSession()->isLoggedIn()) {
            return;
        }

        $guestForm      = $form->getSubform('guest');
        $checkoutMethod = $this->getRequest()->getPost('checkout_method');
        $user           = $this->getSession()->getUser();

        if ($checkoutMethod != 'guest' && $checkoutMethod != 'signup') {
            throw new Exception(self::ERROR_MISSING_FIELDS);
        }

        if ($checkoutMethod == 'guest') {
            $user->setEmail($guestForm->getValue('email'))
                    ->setFullName($guestForm->getValue('full_name'));
        } else {
            try {
                $data = $guestForm->getValues();
                $this->getSession()->register($data['guest']);
            } catch (Ikantam_Exception_Signup $exception) {
                throw new Exception($exception->getMessage());
            } catch (Exception $exception) {
                $this->logException($exception, $logParams = false);
                throw new Exception('Unable to sign up. Please, try again later');
            }
        }
    }

    protected function _saveBillingAddress($form)
    {
        $cart    = $this->getSession()->getCart();
        $subform = $form->getSubform('billing_address');

        if ($cart->hasItemsForDelivery() && $subform->getValue('same_as_shipping')) {
            $cart->setBillingAddressId($cart->getShippingAddressId())->save();
        } else {
            $data           = $subform->getValues();
            $billingAddress = new User_Model_Address();

            if (!empty($data['billing_address']['state_name']) && empty($data['billing_address']['state_code'])) {
                $state                                 = $data['billing_address']['state_code'] = $data['billing_address']['state_name'];
            }

            if (!empty($data['billing_address']['state_code']) && empty($data['billing_address']['state_name'])) {
                $state                                 = $data['billing_address']['state_name'] = $data['billing_address']['state_code'];
            }

            $billingAddress->addData($data['billing_address'])
                    ->setState($state)
                    ->setPostcode($data['billing_address']['postal_code'])
                    ->setIsPrimary(0)
                    ->save();

            $cart->setBillingAddressId($billingAddress->getId())->save();
        }
    }

    protected function _saveShippingAddress($form)
    {
        $cart = $this->getSession()->getCart();

        if ($cart->hasItemsForDelivery()) {
            $subform = $form->getSubform('shipping_address');

            if ($this->getSession()->isLoggedIn() && count($this->_getUserAddresses()) > 0 && $subform->getValue('id')) {
                $cart->setShippingAddressId($subform->getValue('id'))->save();
            } else {
                $address = new User_Model_Address();
                $states  = new Application_Model_State();
                $state   = $states->getByCode($subform->getValue('state_code'));

                $address->addData($subform->getValues())
                        ->setState($state->getName())
                        ->setCountryCode('US')
                        ->setPhoneNumber($subform->getValue('telephone_number'))
                        ->setPostcode($subform->getValue('postal_code'))
                        ->setIsPrimary(0)
                        ->save();

                $cart->setShippingAddressId($address->getId())->save();
            }
        } else {
            $cart->setShippingAddressId(null)->save();
        }
    }

    protected function _saveDeliveryDates($form)
    {
        $cart = $this->getSession()->getCart();

        if ($cart->hasItemsForDelivery()) {
            $subform = $form->getSubform('delivery_date');
            $options = array('buyer_dates' => array());

            foreach ($subform->getValues() as $dates) {
                foreach ($dates as $date) {
                    foreach ($date as $values) {
                        if ($values['date'] && $values['time']) {
                            $options['buyer_dates'][] = date('Y-m-d', strtotime($values['date'])) . ' ' . $values['time'];
                        }
                    }
                }
            }

            foreach ($cart->getItemsForDelivery() as $item) {
                $item->setOptions(serialize($options))->save();
            }

            $cart->reload();
        }
    }

    protected function _getHowYouHeard($form)
    {
        $howYouHeard = $form->getSubform('how_did_you_hear_about_us')->getValues();
        $how         = '';

        if (isset($howYouHeard['how_did_you_hear_about_us_select'])) {
            $heards = Checkout_Form_HowYouHeard::getHeards();

            if (isset($heards[$howYouHeard['how_did_you_hear_about_us_select']])) {
                $how = $heards[$howYouHeard['how_did_you_hear_about_us_select']];

                if ($how == 'Other' && !empty($howYouHeard['how_did_you_hear_about_us_text'])) {
                    $how = $howYouHeard['how_did_you_hear_about_us_text'];
                }
            }
        }
        return $how;
    }

    protected function _saveOrder($form)
    {
        $cart        = $this->getSession()->getCart();
        $user        = $this->getSession()->getUser();
        $paymentForm = $form->getSubform('payment');

        $this->_saveUserData($form);
        $this->_saveShippingAddress($form);
        $this->_saveDeliveryDates($form);
        $this->_saveBillingAddress($form);

        $order = new Order_Model_Order();

        $userId     = $user->getId();
        $bpCustomer = $user->getBpCustomer();

        if (!$userId) {
            $searchUser = new User_Model_User();
            $searchUser->getByEmail($user->getMainEmail());
            $userId     = $searchUser->getId();
            $bpCustomer = $searchUser->getBpCustomer();
        }

        $order->createFromCart($cart)
                ->setUserId($userId)
                ->setUserFullName($user->getFullName())
                ->setUserEmail($user->getMainEmail())
                ->setHowYouHeard($this->_getHowYouHeard($form))
                ->save();

        if (!$cart->checkItemAvailability()->reload()->collectTotals()->hasItemsForCheckout()) {
            $dbh = Application_Model_DbFactory::getFactory()->getConnection();
            $dbh->rollBack();
            $cart->checkItemAvailability()->reload()->collectTotals();
            $this->getSession()->addMessage('error', 'Some of your products are no longer available for purchase.');
            $this->_redirect('cart');
        }

        $cart->collectTotals();

        $comission = $cart->calculateTransactionFeePercent();
        $shipping  = $cart->getShippingAmount();
        $coupon    = $cart->getCoupon();

        $payment = new Order_Model_Payment();
        $payment->createPayment2($paymentForm->getValues(), $cart, $user, $comission, $order, $shipping, $coupon, $bpCustomer, $userId);
        //$order->createHolds($user);//@TODO: to be done later

        $order->setPaymentId($payment->getId())->save();

        $cart->deleteCoupon();

        $this->getSession()->setCoupon(null);
        return $order;
    }

    protected function _prepareForm($form)
    {
        if (!$this->getSession()->isLoggedIn()) {
            $signup = ($this->getRequest()->getPost('checkout_method') == 'signup');
            $form->setPersonalInfoRequired($signup);
        }

        $cart = $this->getSession()->getCart();

        $billingData = $this->getRequest()->getPost('billing_address');

        if ($cart->hasItemsForDelivery()) {
            if ($this->getSession()->isLoggedIn()) {
                if (count($this->_getUserAddresses()) > 0) {
                    $form->setShippingAddressIdRequired($this->_getUserAddresses());
                } else {
                    $form->setShippingAddressRequired();
                }
            } else {
                $form->setShippingAddressRequired();
            }
            //Most minimal lifetime from all delivered products
            $minLifeTime = min(array_map(function($item) {
                        return $item->getProduct()->getTimeToLife();
                    }, $cart->getItemsForDelivery()->getItems()));

            $fallWithinDelivery = count($this->_getDeliveryDates());
            if ($minLifeTime > (strtotime('+3 days') - time())) {
                // Set required 3 dates only if a sale window wider than 3 days
                // https://app.asana.com/0/12118343550314/15174981957817
                if ($fallWithinDelivery >= 3) {
                    $form->setDeliveryDateRequired();
                } elseif ($fallWithinDelivery > 0) {
                    // set number of required dates equal to days that fall within our delivery dates
                    $form->setDeliveryDateRequired($fallWithinDelivery);
                } // otherwise dates are not required
            } else {
                $requiredDatesNumber = $fallWithinDelivery > 3 ? 3 : $fallWithinDelivery;
                $form->setDeliveryDateRequired($requiredDatesNumber ? : 1);
            }


            $ba = $this->getRequest()->getPost('billing_address');
            $sa = (isset($ba['same_as_shipping'])) ? (bool) $ba['same_as_shipping'] : false;
            if (!$sa) {
                if (!empty($billingData['state_code'])) {
                    $form->setBillingAddressRequired($codeRequired = true);
                } else {
                    $form->setBillingAddressRequired($codeRequired = false);
                }
            }
        } else {
            //always require billing address for pickup orders
            if (!empty($billingData['state_code'])) {
                $form->setBillingAddressRequired($codeRequired = true);
            } else {
                $form->setBillingAddressRequired($codeRequired = false);
            }
        }

        //always require card info
        $form->setCardRequired();
    }

    public function indexAction()
    {
        $this->view->appId       = $this->_appId;
        $this->view->redirectUrl = urlencode(Ikantam_Url::getUrl('user/login/facebook', array('checkout_signin' => 1)));
        $this->view->scope       = $this->_scope;

        $this->view->message = '';

        $this->view->headTitle('Checkout ');
        $this->view->headMeta()->appendName('description', 'Buy used furniture online with your credit card or Paypal account.');

        $form = new Checkout_Form_Onestep();

        $request = $this->getRequest();
        $cart    = $this->getSession()->getCart();

        if (!$request->isPost()) {
            $coupon = $cart->getCoupon();

            if (!$coupon->isValid($this->getSession()->getUserId())) {
                $cart->deleteCoupon();
            }
        }

        //prevent multiple form submissions
        if ($this->getSession()->getLastOrderId()) {
            $this->_helper->redirector('index', 'success', 'checkout', array('id' => $this->getSession()->getLastOrderId()));
        }

        if (!$cart->hasItems()) {
            $this->_redirect('cart');
        }

        if (!$cart->hasItemsForCheckout()) {
            $this->getSession()->addMessage('error', 'Select Products!');
            $this->_redirect('cart');
        }

        if ($request->isPost()) {

            $shipping_param = $request->getParam('shipping_address');
            if ($this->getSession()->isLoggedIn() && @empty($shipping_param['id'])) {
                //if no address id
                $this->_tryToSaveShippingAddress($request->getParam('shipping_address'));
            }

            $this->_prepareForm($form);

            if ($form->isValid($request->getParams()) && $this->_validateShippingMethods()) {

                $dbh = Application_Model_DbFactory::getFactory()->getConnection();
                $dbh->beginTransaction();

                try {
                    $order = $this->_saveOrder($form);                    
                    $cart->setShippingAddressId(null)->setBillingAddressId(null)->save();

                    //prevent multiple form submissions
                    if ($this->getSession()->getLastOrderId()) {
                        $dbh->rollBack();
                        $this->_helper->redirector('index', 'success', 'checkout', array('id' => $this->getSession()->getLastOrderId()));
                    }

                    $dbh->commit();

                    //die('success');
                    $this->getSession()->setLastOrderId($order->getId());

                    try {                       
                        $order->getEventDispatcher()->trigger('transaction.user.checkout', $order);
                    } catch (Exception $exception) {
                        $this->_logException($exception);
                    }
                    $this->_helper->redirector('index', 'success', 'checkout', array('id' => $order->getId()));
                } catch (Ikantam_Balanced_Exception $e) {
                    $dbh->rollBack();
                    $this->view->message = self::ERROR_CARD_DECLINED;
                    //var_dump($e->getMessage());
                    //die();
                } catch (Exception $exception) {
                    $dbh->rollBack();
                    $this->view->message = $exception->getMessage();
                    //var_dump($exception->getMessage());
                    //var_dump($exception->getTraceAsString());
                    //die();
                }
            } else {
                $form->populate($request->getPost());

                $cardNumber = $form->getSubform('payment')->getElement('card_number');
                $expMonth   = $form->getSubform('payment')->getElement('exp_month');
                $expYear    = $form->getSubform('payment')->getElement('exp_year');
                $cvv        = $form->getSubform('payment')->getElement('cvv');

                $error = false;

                if ($cardNumber->hasErrors() && !$expMonth->hasErrors() && !$expYear->hasErrors() && !$cvv->hasErrors()) {
                    $errors = $cardNumber->getErrors();

                    $checksum = in_array('creditcardChecksum', $errors);
                    $content  = in_array('creditcardContent', $errors);
                    $invalid  = in_array('creditcardInvalid', $errors);
                    $length   = in_array('creditcardLength', $errors);
                    $prefix   = in_array('creditcardPrefix', $errors);

                    if ($checksum || $content || $invalid || $length || $prefix) {
                        $error = true;
                    }
                }

                foreach ($form->getSubforms() as $subform) {
                    foreach ($subform->getElements() as $element) {
                        if ($element->hasErrors()) {
                            $element->setAttrib('class', $element->getAttrib('class') . ' error-field');
                        }
                    }
                }

                foreach ($form->getSubform('delivery_date')->getSubforms() as $subform) {
                    foreach ($subform->getElements() as $element) {
                        if ($element->hasErrors()) {
                            $element->setAttrib('class', $element->getAttrib('class') . ' error-field');
                        }
                    }
                }

                if ($error) {
                    $this->view->message = self::ERROR_CARD_NUMBER;
                } else {
                    if ($form->getSubform('shipping_address')->getElement('id')->hasErrors()) {
                        $this->view->message = self::ERROR_MISSING_ADDRESS;
                    } else {
                        $this->view->message = self::ERROR_MISSING_FIELDS;
                    }
                }

                //var_dump($form->getMessages());
                //die();
            }
        } else {
            $cart->setShippingAddressId(null)->setBillingAddressId(null)->collectTotals()->save();
        }

        if (!$cart->checkItemAvailability()->reload()->collectTotals()->hasItemsForCheckout()) {
            $this->getSession()->addMessage('error', 'Some of your products are no longer available for purchase.');
            $this->_redirect('cart');
        }

        $this->view->stepCounter = 1;

        $form->populate(array('shipping_address' => array('id' => $cart->getShippingAddressId())));

        $states             = new Application_Model_State_Collection();
        $this->view->states = Zend_Json_Encoder::encode($states->getJsonConfig());
        $this->view->cart   = $this->getSession()->getCart();
        $this->view->form   = $form;

        $addresses                 = new User_Model_Address_Collection();
        $addresses->getByUserId($this->getSession()->getUserId());
        $this->view->userAddresses = $addresses;

        $deliveryDates                             = $this->_getDeliveryDates();
        $numberOfRequiredDeliveryDates             = count($deliveryDates);
        $this->view->numberOfRequiredDeliveryDates = $numberOfRequiredDeliveryDates > 3 ? 3 : $numberOfRequiredDeliveryDates;
        $this->view->tableDates                    = Zend_Json_Encoder::encode($deliveryDates);
    }

    /**
     * @param int $days
     * @param int $offset - from current date in days
     * @return array
     */
    protected function getRnge($days = 70, $offset = 2)
    {
        $current = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        if ($offset)
            $current += $offset * 86400;
        return array($current, $current + (86400 * $days));
    }

    protected function getTableDates($range, $startDate, $endDate, $format = 'd M Y D')
    {
        $dateHelper    = $this->dateHelper;
        /* @var $cart Cart_Model_Cart */
        $cart          = $this->getSession()->getCart();
        $smallDelivery = false;

        if (count($cart->getItemsForCheckout()) == 1) {
            /* @var $singleItem Cart_Model_Item */
            $_items     = $cart->getItemsForCheckout()->getItems();
            $singleItem = array_pop($_items);

            if ($singleItem->getQty() == 1 && $singleItem->getProduct()->getData('is_under_15_pounds')) {
                $smallDelivery = true;
            }
        }

        $dateHelper->setFrom($range[0])
                ->setTill($range[1])
                ->setIncludeDefault(!$smallDelivery)
                ->setIncludeSmall($smallDelivery);

        $this->_collection = clone $dateHelper->dateIntersect();


        $aux    = array();
        $aux2   = array();
        $result = array();

        for ($i = $range[0]; $i < $range[1]; $i += 86400) {
            if ($startDate && $i < $startDate) {
                continue;
            }
            if ($endDate && $i > $endDate) {
                continue;
            }
            $aux2[date($format, $i)] = array(0, 0, 0);
        }

        $fillPeriod = function(\Admin_Model_DeliveryDate $date) use ($smallDelivery) {
            $fieldsForDefault = array(
                'is_first_period_available',
                'is_second_period_available',
                'is_third_period_available'
            );
            $fieldsForSmall   = array(
                'is_first_period_for_small_available',
                'is_second_period_for_small_available',
                'is_third_period_for_small_available'
            );

            if ($smallDelivery) {
                $datePeriods = array_values($date->toArray($fieldsForSmall));
            } else {
                $datePeriods = array_values($date->toArray($fieldsForDefault));
            }

            return array_sum($datePeriods) ? $datePeriods : null;
        };

        foreach ($this->_collection->getItems() as $date) {
            /* @var $date Admin_Model_DeliveryDate */
            if (array_key_exists($date->getDateString(), $aux2)) {

                if ($datePeriods = $fillPeriod($date)) {
                    $aux[$date->getDateString()] = $datePeriods; //if at least one period available, get this date, else ignore it.
                }
            }
        }

        // if dates were not found then try to find closest available date
        if (empty($aux)) {
            $closestDate = $dateHelper->setTill(null)
                    ->setLimit(1)
                    ->dateIntersect();

            if ($closestDate->getSize()) {
                $closestDate                        = $closestDate->getFirstItem();
                $aux[$closestDate->getDateString()] = $fillPeriod($closestDate);
            }
        }

        foreach (array_keys($aux) as $stringDate) {
            $result[] = array_merge($aux[$stringDate], array('stringDate' => $stringDate));
        }

        return $result;
    }

    public function _getDeliveryDates()
    {
        $this->_range    = $this->getRnge();
        $startDate       = null;
        $endDate         = null;
        /* @var $cart Cart_Model_Cart */
        $cart            = $this->getSession()->getCart();
        $sellerPostcodes = array();
        foreach ($cart->getItemsForCheckout() as $item) {
            /* @var $product Product_Model_Product */
            $product = $item->getProduct();
            if ($item->getShippingMethodId() == 'delivery') {
                $sellerPostcodes[] = $product->getPickupPostcode();
            }
            if ($product->getAvailableFrom()) {
                if (!$startDate) {
                    $startDate = $product->getAvailableFrom();
                } elseif ($product->getAvailableFrom() > $startDate) {
                    $startDate = $product->getAvailableFrom();
                }
            }

            if ($product->getAvailableTill()) {
                if (!$endDate) {
                    $endDate = $product->getAvailableTill();
                } elseif ($product->getAvailableTill() < $endDate) {
                    $endDate = $product->getAvailableTill();
                }
            }
        }

        $dates        = array();
        $tableDates   = $this->getTableDates($this->_range, $startDate, $endDate);
        $deliveryDate = new Checkout_Model_DeliveryDate();


        $postcode = $cart->getShippingAddress()->getPostcode();
        // checkout as guest
        if (!$postcode) {
            $postcode = $cart->getPostalCode();
            if (!$postcode) {
                $shippingAddress = $this->getRequest()->getParam('shipping_address');
                if (isset($shippingAddress['postal_code'])) {
                    $postcode = $shippingAddress['postal_code'];
                }
            }
        }

        $buyerDates = $deliveryDate->getDeliveryTimesForBuyer($sellerPostcodes, $postcode);

        foreach ($tableDates as $date) {
            if (isset($date['stringDate'])) {
                $stringDate     = $date['stringDate'];
                unset($date['stringDate']);
                $fdate['7-12']  = $date[0] && isset($buyerDates['7-12']);
                $fdate['12-16'] = $date[1] && isset($buyerDates['12-16']);
                $fdate['16-20'] = $date[2] && isset($buyerDates['16-20']);
//                $fdate['17-21'] = $date[3] && isset($buyerDates['17-21']);
                if ($fdate['7-12'] || $fdate['12-16'] || $fdate['16-20'] /* || $fdate['17-21'] */) {
                    $dates[date('Y-m-d', strtotime($stringDate))] = $fdate;
                }
            }
        }

        return $dates;
    }

    public function setMethodAction()
    {
        $request = $this->getRequest();
        $cart    = $this->getSession()->getCart();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $response = array(
                'success' => false,
                'error'   => true,
                'message' => '',
                'update'  => array(
                    'sidebar'  => '',
                    'shipping' => true,
                    'billing'  => false,
                ),
            );

            $cartItems = (array) $this->getRequest()->getPost('cart_items');

            foreach ($cartItems as $itemId => $params) {
                $item = new Cart_Model_Item($itemId);

                if (!$item->getId() || $item->getCartId() !== $cart->getId()) {
                    continue;
                    //@TODO: return error
                }

                if (isset($params['shipping_method']) && in_array($params['shipping_method'], array('pickup', 'delivery'))) {
                    $item->setShippingMethodId($params['shipping_method'])->save();
                }
            }

            if (!$this->getSession()->isLoggedIn()) {
                $shippingAddress = $this->getRequest()->getPost('shipping_address');
                $address         = new User_Model_Address($shippingAddress);
                $cart->collectShippingByAddress($address)->save();
            } else {
                $cart->collectTotals()->save();
            }

            $this->view->cart = $cart;

            $response['success']                  = true;
            $response['error']                    = false;
            $response['update']['sidebar']        = $this->view->render('onestep/sidebar.phtml');
            $response['update']['shipping']       = $cart->hasItemsForDelivery();
            $response['update']['billing']        = !$cart->hasItemsForDelivery();
            $response['update']['delivery_dates'] = $this->_getDeliveryDates();

            $this->_helper->json($response);
        }

        throw new Exception('Cannot set shipping method.');
    }

    public function saveAddressAction()
    {
        $request = $this->getRequest();
        $cart    = $this->getSession()->getCart();
        $form    = new Checkout_Form_ShippingAddress();
        $form->_setDataRequired();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $response = array(
                'success'  => false,
                'error'    => true,
                'message'  => '',
                'template' => '',
            );

            $exists = false;

            if ($form->isValid($request->getPost('shipping_address'))) {
                //@TODO save address
                $address = new \User_Model_Address();

                if ($form->getValue('id')) {
                    $address = $address->getById($form->getValue('id'));

                    if (!$address->getId() || $address->getUserId() !== $this->getSession()->getUserId()) {
                        $this->_helper->json($response);
                    }

                    $exists = true;
                }

                $states = new Application_Model_State();
                $state  = $states->getByCode($form->getValue('state_code'));

                $address->addData($form->getValues())
                        ->setUserId($this->getSession()->getUserId())
                        ->setIsPrimary(false)
                        ->setCountryCode('US')
                        ->setState($state->getName())
                        ->setPostcode($form->getValue('postal_code'))
                        ->setPhoneNumber($form->getValue('telephone_number'))
                        ->save();

                $this->view->cart          = $cart;
                $this->view->userAddresses = array($address);

                if (!$exists) {
                    $response['template'] = $this->view->render('onestep/shipping/address-template.phtml');
                }

                $response['success'] = true;
                $response['error']   = false;
                $response['id']      = $address->getId();
            } else {
                foreach ($form->getElements() as $element) {
                    if ($element->hasErrors()) {
                        $response['errors'][$element->getName()] = true;
                    }
                }
                //var_dump($form->getMessages());
            }

            $this->_helper->json($response);
        }

        throw new Exception('Cannot save the address.');
    }

    public function setAddressAction()
    {
        $request     = $this->getRequest();
        $cart        = $this->getSession()->getCart();
        $form        = new Checkout_Form_ShippingAddress();
        $oldCouponId = $cart->getCouponId();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $response = array(
                'success' => false,
                'error'   => true,
                'message' => ''
            );

            $form->_setIdRequired($this->_getUserAddresses());

            if ($form->isValid($request->getPost('shipping_address'))) {

                $cart->setShippingAddressId($form->getValue('id'))
                        ->save()
                        ->collectTotals()
                        ->save();

                $response['success'] = true;
                $response['error']   = false;
            }

            $this->view->cart = $cart;

            if ($oldCouponId && !$cart->getCouponId()) {
                $response['message'] = 'The coupon has been cancelled';
            }

            $response['update']['sidebar']        = $this->view->render('onestep/sidebar.phtml');
            $response['update']['shipping']       = $cart->hasItemsForDelivery();
            $response['update']['billing']        = !$cart->hasItemsForDelivery();
            $response['update']['delivery_dates'] = $this->_getDeliveryDates();
            $this->_helper->json($response);
        }

        throw new Exception('Cannot save the address.');
    }

    public function setAddressInfoAction()
    {
        $request     = $this->getRequest();
        $cart        = $this->getSession()->getCart();
        $data        = $request->getPost('shipping_address');
        $oldCouponId = $cart->getCouponId();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $response = array(
                'success' => false,
                'error'   => true,
                'message' => ''
            );

            $address = new User_Model_Address();
            $address->setData($data);


            $cart->collectTotals()->collectShippingByAddress($address)->save();
            $cart->setPostalCode($data['postal_code']);
            $response['success'] = true;
            $response['error']   = false;


            $this->view->cart = $cart;

            if ($oldCouponId && !$cart->getCouponId()) {
                $response['message'] = 'The coupon has been canceled';
            }
            $response['update']['sidebar']        = $this->view->render('onestep/sidebar.phtml');
            $response['update']['shipping']       = $cart->hasItemsForDelivery();
            $response['update']['billing']        = !$cart->hasItemsForDelivery();
            $response['update']['delivery_dates'] = $this->_getDeliveryDates();

            $this->_helper->json($response);
        }

        throw new Exception('Cannot save the address.');
    }

    public function loginAction()
    {
        $request = $this->getRequest();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $response = array(
                'success' => false,
                'error'   => true,
                'message' => ''
            );

            $form = new User_Form_Login();

            try {
                if ($form->isValid($request->getPost('login'))) {
                    $this->getSession()->authenticate($form->getValues());
                    $response['success'] = true;
                    $response['error']   = false;
                } else {
                    if ($form->getElement('email')->hasErrors()) {
                        $response['message']['email'] = current($form->getElement('email')->getMessages());
                    }
                    if ($form->getElement('password')->hasErrors()) {
                        $response['message']['password'] = current($form->getElement('password')->getMessages());
                    }
                }
            } catch (Ikantam_Exception_Auth $exception) {
                $response['message']['login'] = $exception->getMessage();
            } catch (Exception $exception) {
                $response['message']['login'] = 'Unable to log in. Please, try again later.';
                $this->logException($exception, $logParams                    = false);
            }

            $this->_helper->json($response);
        }
    }

    public function setCouponAction()
    {
        $request = $this->getRequest();
        $cart    = $this->getSession()->getCart();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $response = array(
                'success' => false,
                'error'   => true,
                'message' => ''
            );

            $couponCode = $request->getPost('coupon_code');
            if ($couponCode !== null) {
                $coupon = $cart->addCoupon($couponCode);
                if ($cart->getCouponId()) {
                    $response['success'] = true;
                    $response['error']   = false;

                    $this->view->cart = $cart;

                    $response['update']['sidebar']  = $this->view->render('onestep/sidebar.phtml');
                    $response['update']['shipping'] = $cart->hasItemsForDelivery();
                    $response['update']['billing']  = !$cart->hasItemsForDelivery();
                } else {
                    $response['message'] = implode('<br />', $coupon->getErrorMessages());
                }
            } elseif ($request->getPost('cancel_coupon')) {
                $cart->deleteCoupon();
                $response['success'] = true;
                $response['error']   = false;

                $this->view->cart = $cart;

                $response['update']['sidebar']  = $this->view->render('onestep/sidebar.phtml');
                $response['update']['shipping'] = $cart->hasItemsForDelivery();
                $response['update']['billing']  = !$cart->hasItemsForDelivery();
            }

            $this->_helper->json($response);
        }

        throw new Exception('Cannot apply this promotion code');
    }

    /**
     * Tries to save the address if user filled of address
     * form and forgot to click on save button
     *
     * @param  array $data - fields
     * @return self
     */
    protected function _tryToSaveShippingAddress($data)
    {
        $form = new Checkout_Form_ShippingAddress();
        $form->_setDataRequired();

        if ($form->isValid((array) $data)) {
            $cart    = $this->getSession()->getCart();
            $address = new \User_Model_Address();
            $address->addData($data)
                    ->setUserId($this->getSession()->getUserId())
                    ->setIsPrimary(false)
                    ->setCountryCode("US")
                    ->setState($address->getStateCode())
                    ->setPostcode($address->getPostalCode())
                    ->save();

            $cart->setShippingAddressId($address->getId())
                    ->collectTotals()
                    ->save();

            $this->getRequest()->setParam('shipping_address', array_replace_recursive($data, $address->getData()));
        }
    }

    protected function _logException($exception)
    {
        $fileName = APPLICATION_PATH . '/log/exception.log';

        $data = $this->prepareException($exception) . $this->prepareRequest() . $this->prepareUserInfo() . "\n";

        file_put_contents($fileName, $data, FILE_APPEND);
    }

    protected function prepareException($exception)
    {
        return date('Y-m-d H:i:s') . ': Unable to send emails:' . "\n" . $exception->getMessage() . "\n" . $exception->getTraceAsString() . "\n";
    }

    protected function prepareRequest()
    {
        $str = "Request Parameters:\n";
        foreach ($this->getRequest()->getParams() as $param => $value) {
            $str .= $param . ': ' . $value . "\n";
        }
        return $str;
    }

    protected function prepareUserInfo()
    {
        $str = "User info:\n";
        $str .= "IP: " . $this->getRequest()->getClientIp() . "\n";
        $str .= "User agent: " . $_SERVER['HTTP_USER_AGENT'] . "\n\n";
        $str .= '*************************************************************' . "\n";
        return $str;
    }

}