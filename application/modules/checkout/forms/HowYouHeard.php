<?php

class Checkout_Form_HowYouHeard extends Zend_Form
{

    public function init()
    {
        $howDidYouHearAboutUsElement = new Zend_Form_Element_Select('how_did_you_hear_about_us_select');

        $options = static::getHeards();
        $select  = array('' => 'Select');

        $howDidYouHearAboutUsElement
                ->addFilters(array('Int', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field')
                ->setMultiOptions($select + $options)
                ->setLabel('How did you hear about us?');

        $howDidYouHearAboutUsElement->setDecorators(array(
            'ViewHelper',
            array('Label', array('placement' => 'prepend', 'class' => 'text-mute sub-title show')),
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'eq-fields m-t30 select-el', 'openOnly' => true)),
        ));

        $otherElement = new Zend_Form_Element_Text('how_did_you_hear_about_us_text');

        $otherElement
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field select-field hidden')
                ->setAttrib('data-option', '7');

        $otherElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'eq-fields m-t30 select-el', 'closeOnly' => true)),
        ));

        $this->addElement($howDidYouHearAboutUsElement);
        $this->addElement($otherElement);
    }

    public static function getHeards()
    {
        return array(
            1 => 'Craigslist',
            8 => 'Google / Search',
            2 => 'Friend / Family',
            3 => 'Facebook',
            4 => 'Krrb / Apartment Therapy',
            5 => 'Twitter',
            6 => 'Flyer',
            9 => 'Subway',
            10 => 'Pinterest',
            11 => 'Houzz',
            7 => 'Other',
        );
    }

}