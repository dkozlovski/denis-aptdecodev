<?php

class Checkout_Form_BillingAddress extends Zend_Form
{

    public function init()
    {
        $this->addElement($this->_getSameAsShippingElement())
                ->addElement($this->_getFullNameElement())
                ->addElement($this->_getAddressLine1Element())
                ->addElement($this->_getAddressLine2Element())
                ->addElement($this->_getCityElement())
                ->addElement($this->_getStateNameElement())
                ->addElement($this->_getStateCodeElement())
                ->addElement($this->_getPostalCodeElement())
                ->addElement($this->_getCountryCodeElement());
    }
    
    public function _setDataRequired($stateCodeRequired)
    {
        foreach ($this->getElements() as $element) {
            $element->setRequired(true);
        }

        $this->getElement('same_as_shipping')->setRequired(false);
        $this->getElement('address_line2')->setRequired(false);
        
        if ($stateCodeRequired) {
            $this->getElement('state_name')->setRequired(false);
        } else {
            $this->getElement('state_code')->setRequired(false);
        }
    }

    protected function _getSameAsShippingElement()
    {
        $sameAsShippingElement = new Zend_Form_Element_Checkbox('same_as_shipping');

        $sameAsShippingElement->setBelongsTo('billing_address')
                ->setRequired(false)
                ->setAttrib('checked', 'checked')
                ->setValue(1)
                ->setLabel('Same as Shipping Address')
                ->setDescription('<strong>Billing address</strong>');

        $sameAsShippingElement->setDecorators(array(
            'ViewHelper',
            array('Label', array('placement' => 'append')),
            array(array('addLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('addUl' => 'HtmlTag'), array('tag'   => 'ul', 'class' => 'v-list')),
            array('Description', array('tag'       => 'label', 'class'     => 'block-lable title', 'placement' => 'prepend', 'escape'    => false))
        ));

        return $sameAsShippingElement;
    }

    protected function _getFullNameElement()
    {
        $fullNameElement = new Zend_Form_Element_Text('full_name');

        $fullNameElement->setBelongsTo('billing_address')
                ->setRequired(false)
                ->setAttrib('class', 'form-field field-i')
                ->setAttrib('placeholder', 'Full Name');

        $fullNameElement->setDecorators(array(
            'ViewHelper',
            array(array('div' => 'HtmlTag'), array('tag'      => 'div', 'id'       => 'billing_address', 'openOnly' => true))
        ));

        return $fullNameElement;
    }

    protected function _getAddressLine1Element()
    {
        $addressLine1Element = new Zend_Form_Element_Text('address_line1');

        $addressLine1Element->setBelongsTo('billing_address')
                ->setRequired(false)
                ->setAttrib('class', 'form-field field-i key right')
                ->setAttrib('placeholder', 'Building & Street Name');

        $addressLine1Element->setDecorators(array('ViewHelper'));

        return $addressLine1Element;
    }

    protected function _getAddressLine2Element()
    {
        $addressLine2Element = new Zend_Form_Element_Text('address_line2');

        $addressLine2Element->setBelongsTo('billing_address')
                ->setRequired(false)
                ->setAttrib('class', 'form-field field-i key right')
                ->setAttrib('placeholder', 'Apt #');

        $addressLine2Element->setDecorators(array('ViewHelper'));

        return $addressLine2Element;
    }

    protected function _getCityElement()
    {
        $cityElement = new Zend_Form_Element_Text('city');

        $cityElement->setBelongsTo('billing_address')
                ->setRequired(false)
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', 'City');

        $cityElement->setDecorators(array('ViewHelper'));

        return $cityElement;
    }

    protected function _getStateNameElement()
    {
        $cityElement = new Zend_Form_Element_Text('state_name');

        $cityElement->setBelongsTo('billing_address')
                ->setRequired(false)
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', 'State');

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'eq-fields', 'openOnly' => true)),
        ));

        return $cityElement;
    }

    protected function _getStateCodeElement()
    {
        $options = $this->_getStates();
        $select  = array('' => '-- State --');

        $cityElement = new Zend_Form_Element_Select('state_code');

        $cityElement->setBelongsTo('billing_address')
                ->setRequired(false)
                ->addValidator('InArray', false, array(array_keys($options)))
                ->setMultiOptions($select + $options)
                ->setAttrib('class', 'form-field')
                ->setAttrib('style', 'display: none;')
                ->setValue('NY');

        $cityElement->setDecorators(array('ViewHelper'));

        return $cityElement;
    }

    protected function _getPostalCodeElement()
    {
        $cityElement = new Zend_Form_Element_Text('postal_code');

        $cityElement->setBelongsTo('billing_address')
                ->setRequired(false)
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', 'ZIP');

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'       => 'div', 'closeOnly' => true)),
        ));

        return $cityElement;
    }

    protected function _getCountryCodeElement()
    {
        $cityElement = new Zend_Form_Element_Select('country_code');

        $options = $this->_getCountries();
        $select  = array('' => '-- Country --');

        $cityElement->setBelongsTo('billing_address')
                ->setRequired(false)
                ->setAttrib('class', 'form-field')
                ->setMultiOptions($select + $options)
                ->setValue('US');

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('div' => 'HtmlTag'), array('tag'       => 'div', 'closeOnly' => true))
        ));

        return $cityElement;
    }

    public function isValid($data)
    {
        $isValid = parent::isValid($data);

        /* if ($this->getElement('full_name')->hasErrors()) {
          $this->getElement('full_name')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('address_line1')->hasErrors()) {
          $this->getElement('address_line1')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('address_line2')->hasErrors()) {
          $this->getElement('address_line2')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('city')->hasErrors()) {
          $this->getElement('city')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('state_name')->hasErrors()) {
          $this->getElement('state_name')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('state_code')->hasErrors()) {
          $this->getElement('state_code')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('postal_code')->hasErrors()) {
          $this->getElement('postal_code')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('country_code')->hasErrors()) {
          $this->getElement('country_code')->setAttrib('class', 'form-field error');
          } */

        return $isValid;
    }

    protected function _getStates()
    {
        $states = new Application_Model_State_Collection();
        return $states->getOptions();
    }

    protected function _getCountries()
    {
        $countries = new Application_Model_Country_Collection();
        return $countries->getOptions();
    }

}
