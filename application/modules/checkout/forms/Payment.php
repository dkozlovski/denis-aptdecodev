<?php

class Checkout_Form_Payment extends Zend_Form
{

    public function init()
    {
        $this->addElement($this->_getCardNumberElement());
        $this->addElement($this->_getExpMonthElement());
        $this->addElement($this->_getExpYearElement());
        $this->addElement($this->_getCvvNumberElement());
    }

    public function _setDataRequired()
    {
        foreach ($this->getElements() as $element) {
            $element->setRequired(true);
        }
    }

    protected function _getCardNumberElement()
    {
        $fullNameElement = new Zend_Form_Element_Text('card_number');

        $creditCard = new Zend_Validate_CreditCard(array(
            Zend_Validate_CreditCard::AMERICAN_EXPRESS,
            Zend_Validate_CreditCard::VISA,
            Zend_Validate_CreditCard::DISCOVER,
            Zend_Validate_CreditCard::MASTERCARD
        ));

        $fullNameElement->setBelongsTo('credit_card')
                ->setRequired(true)
                ->addFilters(array('StringTrim', 'Digits'))
                ->addValidator($creditCard)
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', '0000-0000-0000-0000')
                ->setDescription('Credit Card #')
                ->setAttrib('autocomplete', 'off');

        $fullNameElement->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag'       => 'p', 'placement' => 'prepend')),
        ));

        return $fullNameElement;
    }

    protected function _getExpMonthElement()
    {
        $options = $this->_getMonths();
        $select  = array('' => '-- Month --');

        $expMonthElement = new Zend_Form_Element_Select('exp_month');

        $expMonthElement->setBelongsTo('credit_card')
                ->setRequired(true)
                ->addValidator('InArray', false, array(array_keys($options)))
                ->setAttrib('class', 'form-field')
                ->setDescription('Expiration Date')
                ->setMultiOptions($select + $options);

        $expMonthElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'eq-fields', 'openOnly' => true)),
            array('Description', array('tag'       => 'p', 'placement' => 'prepend')),
        ));

        return $expMonthElement;
    }

    protected function _getExpYearElement()
    {
        $options = $this->_getYears();
        $select  = array('' => '-- Year --');

        $expYearElement = new Zend_Form_Element_Select('exp_year');

        $expYearElement->setBelongsTo('credit_card')
                ->setRequired(true)
                ->addValidator('InArray', false, array(array_keys($options)))
                ->setAttrib('class', 'form-field')
                ->setMultiOptions($select + $options);

        $expYearElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'       => 'div', 'class'     => 'eq-fields', 'closeOnly' => true))
        ));

        return $expYearElement;
    }

    protected function _getCvvNumberElement()
    {
        $fullNameElement = new Zend_Form_Element_Text('cvv');

        $fullNameElement->setBelongsTo('credit_card')
                ->setRequired(true)
                ->addFilters(array('StringTrim', 'Digits'))
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', 'CSV')
                ->setDescription('Security Code')
                ->setAttrib('autocomplete', 'off');

        $fullNameElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'eq-fields')),
            array('Description', array('tag'       => 'p', 'placement' => 'prepend')),
        ));

        return $fullNameElement;
    }

    public function isValid($data)
    {
        $isValid = parent::isValid($data);

        /* if ($this->getElement('card_number')->hasErrors()) {
          $this->getElement('card_number')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('exp_month')->hasErrors()) {
          $this->getElement('exp_month')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('exp_year')->hasErrors()) {
          $this->getElement('exp_year')->setAttrib('class', 'form-field error');
          }

          if ($this->getElement('cvv')->hasErrors()) {
          $this->getElement('cvv')->setAttrib('class', 'form-field error');
          } */

        return $isValid;
    }

    protected function _getMonths()
    {
        return array(
            '01' => '01 - January',
            '02' => '02 - February',
            '03' => '03 - March',
            '04' => '04 - April',
            '05' => '05 - May',
            '06' => '06 - June',
            '07' => '07 - July',
            '08' => '08 - August',
            '09' => '09 - September',
            '10' => '10 - October',
            '11' => '11 - November',
            '12' => '12 - December',
        );
    }

    protected function _getYears()
    {
        $years       = array();
        $currentYear = (int) date('Y');
        $maxYear     = $currentYear + 17;

        for ($i = $currentYear; $i < $maxYear; $i++) {
            $years[$i] = $i;
        }

        return $years;
    }

}
