<?php

class Checkout_Form_Guest extends Zend_Form
{

    public function init()
    {
        $this->addElement($this->_getFullNameElement())
                ->addElement($this->_getEmailElement())
                ->addElement($this->_getPasswordElement())
                ->addElement($this->_getConfirmationElement());
    }
    
    public function _setEmailRequired()
    {
        $this->getElement('email')->setRequired(true);
        $this->getElement('full_name')->setRequired(true);
    }
    
    public function _setPasswordRequired()
    {
        $this->getElement('password')->setRequired(true);
        $this->getElement('confirmation')->setRequired(true);
    }
    
    protected function _getFullNameElement()
    {
        $fullNameElement = new Zend_Form_Element_Text('full_name');

        $fullNameElement->setBelongsTo('guest')
                ->setRequired(false)
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', 'Full Name');

        $fullNameElement->setDecorators(array('ViewHelper'));

        return $fullNameElement;
    }

    protected function _getEmailElement()
    {
        $addressLine1Element = new Ikantam_Form_Element_Email('email');

        $addressLine1Element->setBelongsTo('guest')
                ->setRequired(false)
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->addValidator('EmailAddress', false)
                ->setAttrib('class', 'form-field field-i email left')
                ->setAttrib('placeholder', 'Your Email Address')
                ->setDescription('We\'ll send your confirmation here');

        $addressLine1Element->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag'       => 'small', 'class'     => 'field-helper text-mute', 'placement' => 'append'))
        ));

        return $addressLine1Element;
    }

    protected function _getPasswordElement()
    {
        $fullNameElement = new Zend_Form_Element_Password('password');

        $fullNameElement->setBelongsTo('guest')
                ->setRequired(false)
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', 'Password');

        $fullNameElement->setDecorators(array(
            'ViewHelper',
            array('HtmlTag', array('tag'      => 'div', 'class'    => 'to-register m-t5 hidden', 'openOnly' => true))
        ));

        return $fullNameElement;
    }

    protected function _getConfirmationElement()
    {
        $fullNameElement = new Zend_Form_Element_Password('confirmation');

        $fullNameElement->setBelongsTo('guest')
                ->setRequired(false)
                ->addValidator('Identical', false, array('token' => 'password'))
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', 'Confirm password');

        $fullNameElement->setDecorators(array(
            'ViewHelper',
            array('HtmlTag', array('tag'       => 'div', 'closeOnly' => true))
        ));

        return $fullNameElement;
    }

}
