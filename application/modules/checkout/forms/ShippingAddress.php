<?php

class Checkout_Form_ShippingAddress extends Zend_Form
{

    public function init()
    {
        $this->addElement($this->_getIdElement())
                ->addElement($this->_getFullNameElement())
                ->addElement($this->_getAddressLine1Element())
                ->addElement($this->_getAddressLine2Element())
                ->addElement($this->_getCityElement())
                //->addElement($this->_getStateNameElement())// for international addresses only
                ->addElement($this->_getStateCodeElement())
                ->addElement($this->_getPostalCodeElement())
                ->addElement($this->_getBuildingTypeElement())
                ->addElement($this->_getNumberOfStairsElement())
                ->addElement($this->_getPhoneNumberElement())
                ->setMethod('post');
    }

    public function isValid($data)
    {
        if (isset($data['building_type']) && $data['building_type'] == 'elevator') {
            $this->getElement('number_of_stairs')->setRequired(false);
        }

        return parent::isValid($data);
    }

    public function _setIdRequired($options)
    {
        $this->getElement('id')->setRequired(true);
        $this->getElement('id')->addValidator('InArray', false, array($options));
    }

    public function _setDataRequired()
    {
        foreach ($this->getElements() as $element) {
            $element->setRequired(true);
        }

        $this->getElement('id')->setRequired(false)->setIgnore(true);
        $this->getElement('address_line2')->setRequired(false);
    }

    protected function _getIdElement()
    {
        $fullNameElement = new Zend_Form_Element_Hidden('id');

        $fullNameElement->setAttrib('name', 'shipping_address[id]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Int', 'Null'))
                ->setRequired(false);

        $fullNameElement->setDecorators(array('ViewHelper'));

        return $fullNameElement;
    }

    protected function _getFullNameElement()
    {
        $fullNameElement = new Zend_Form_Element_Text('full_name');

        $fullNameElement->setAttrib('name', 'shipping_address[full_name]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field')
                ->setAttrib('placeholder', 'Full Name');

        $fullNameElement->setDecorators(array('ViewHelper'));

        return $fullNameElement;
    }

    protected function _getAddressLine1Element()
    {
        $addressLine1Element = new Zend_Form_Element_Text('address_line1');

        $addressLine1Element->setAttrib('name', 'shipping_address[address_line1]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field field-i key right')
                ->setAttrib('placeholder', 'Building & Street Name');

        $addressLine1Element->setDecorators(array('ViewHelper'));

        return $addressLine1Element;
    }

    protected function _getAddressLine2Element()
    {
        $addressLine2Element = new Zend_Form_Element_Text('address_line2');

        $addressLine2Element->setAttrib('name', 'shipping_address[address_line2]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field field-i key right')
                ->setAttrib('placeholder', 'Apt #');

        $addressLine2Element->setDecorators(array('ViewHelper'));

        return $addressLine2Element;
    }

    protected function _getCityElement()
    {
        $cityElement = new Zend_Form_Element_Text('city');

        $cityElement->setAttrib('name', 'shipping_address[city]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setRequired(false)
                //->addValidator('InArray', false, array(array('New York')))
                ->setAttrib('class', 'form-field m-r5')
                ->setValue('New York')
                ->setAttrib('placeholder', 'City');
                //->setAttrib('readonly', true);

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'eq-fields', 'openOnly' => true)),
        ));

        return $cityElement;
    }

    protected function _getStateNameElement()
    {
        $cityElement = new Zend_Form_Element_Text('state_name');

        $cityElement->setAttrib('name', 'shipping_address[state_name]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field m-r0')
                ->setAttrib('placeholder', 'State');

        $cityElement->setDecorators(array(
            'ViewHelper',
        ));

        return $cityElement;
    }

    protected function _getStates()
    {
        $states = new Application_Model_State_Collection();
        $states->getByCountryCode("US");

        $options = array();

        foreach ($states as $state) {
            $options[$state->getStateCode()] = $state->getName();
        }

        return $options;
    }

    protected function _getStateCodeElement()
    {
        $cityElement = new Zend_Form_Element_Select('state_code');
        //$cityElement = new Zend_Form_Element_Text('state_code');

        $options = $this->_getStates();
        $select = array('' => '-- State --');

        $cityElement->setAttrib('name', 'shipping_address[state_code]')
                ->setRequired(false)
                ->addValidator('InArray', false, array(array_keys($options)))
                ->setMultiOptions($select + $options)
                ->setAttrib('class', 'form-field m-r0')
                ->setAttrib('placeholder', 'State')
                ->setValue('NY');
                //->setAttrib('readonly', true);

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'       => 'div', 'closeOnly' => true)),
        ));

        return $cityElement;
    }

    protected function _getPostalCodeElement()
    {
        $cityElement = new Zend_Form_Element_Text('postal_code');

        $cityElement->setAttrib('name', 'shipping_address[postal_code]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field m-r5')
                ->setAttrib('placeholder', 'ZIP')
                ->setAttrib('maxlength', '5');

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'      => 'div', 'class'    => 'eq-fields', 'openOnly' => true)),
        ));

        return $cityElement;
    }

    protected function _getBuildingTypeElement()
    {
        $cityElement = new Zend_Form_Element_Select('building_type');

        $options = array('elevator' => 'Elevator', 'walkup'   => 'Walk-up');
        $select  = array('' => '-- Building Type --');

        $cityElement->setAttrib('name', 'shipping_address[building_type]')
                ->setRequired(false)
                ->setAttrib('class', 'form-field m-r0')
                ->setMultiOptions($select + $options)
                ->setDescription('&nbsp;');

        $cityElement->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag'   => 'div', 'class' => 'clear')),
        ));

        return $cityElement;
    }

    protected function _getNumberOfStairsElement()
    {
        $cityElement = new Zend_Form_Element_Select('number_of_stairs');

        $options = array(1  => 1, 2  => 2, 3  => 3, 4  => 4, 5  => 5, 6  => 6, 7  => 7, 8  => 8, 9  => 9, 10 => 10);
        $select  = array('' => 'Number of flights');

        $cityElement->setAttrib('name', 'shipping_address[number_of_stairs]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Int', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field pull-right m-r0 hidden')
                ->setMultiOptions($select + $options);

        $cityElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'       => 'div', 'closeOnly' => true)),
        ));

        return $cityElement;
    }

    protected function _getPhoneNumberElement()
    {
        $cityElement = new Zend_Form_Element_Text('telephone_number');

        $cityElement->setAttrib('name', 'shipping_address[telephone_number]')
                ->addFilters(array('StringTrim', 'StripNewlines', 'StripTags', 'Null'))
                ->setRequired(false)
                ->setAttrib('class', 'form-field field-i tel left numb-format')
                ->setAttrib('placeholder', 'Your Phone');

        $cityElement->setDecorators(array(
            'ViewHelper',
        ));

        return $cityElement;
    }

}
