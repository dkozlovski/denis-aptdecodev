<?php

class Checkout_Form_Onestep extends Zend_Form
{

    public function init()
    {
        $this->addSubform(new Checkout_Form_BillingAddress(), 'billing_address');
        $this->addSubform(new Checkout_Form_DeliveryDate(), 'delivery_date');
        $this->addSubform(new Checkout_Form_Guest(), 'guest');
        $this->addSubform(new Checkout_Form_Payment(), 'payment');
        $this->addSubform(new Checkout_Form_ShippingAddress(), 'shipping_address');
        $this->addSubform(new Checkout_Form_HowYouHeard(), 'how_did_you_hear_about_us');
        $this->addSubform(new Checkout_Form_Terms(), 'terms');
    }

    public function setPersonalInfoRequired($requirePassword = false)
    {
	    $this->getSubform('guest')->_setEmailRequired();
	    
	    if ($requirePassword) {
	        $this->getSubform('guest')->_setPasswordRequired();
	    }
    }
    
    public function setShippingAddressIdRequired($options)//options ids for inArray validator
    {
        $this->getSubform('shipping_address')->_setIdRequired($options);
    }
    
    public function setShippingAddressRequired()
    {
        $this->getSubform('shipping_address')->_setDataRequired();
    }

    /**
     * @param bool|int $limit - to limit number of required dates
     *  in some cases form must be limited for only 1 required date
     * https://app.asana.com/0/12118343550314/15174981957817
     */
    public function setDeliveryDateRequired($limit = false)
    {
        $this->getSubform('delivery_date')->_setDataRequired($limit);
    }
    
    public function setBillingAddressRequired($stateCodeRequired = false)
    {
        $this->getSubform('billing_address')->_setDataRequired($stateCodeRequired);
    }
    
    public function setCardRequired()
    {
        $this->getSubform('payment')->_setDataRequired();
    }
    
    
}
