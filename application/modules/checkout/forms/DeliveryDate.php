<?php

class Checkout_Form_DeliveryDate extends Zend_Form
{

    public function init()
    {
        $subForm1 = new Zend_Form_SubForm(array('disableLoadDefaultDecorators' => true));
        $subForm1->setDecorators(array('FormElements',));
        $subForm1->addElement($this->_getDeliveryDateElement(1));
        $subForm1->addElement($this->_getDeliveryTimeElement(1));
        $subForm1->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'delivery-date'));
        $subForm1->addDecorator(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'eq-fields'));

        $subForm2 = new Zend_Form_SubForm(array('disableLoadDefaultDecorators' => true));
        $subForm2->setDecorators(array('FormElements',));
        $subForm2->addElement($this->_getDeliveryDateElement(2));
        $subForm2->addElement($this->_getDeliveryTimeElement(2));
        $subForm2->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'delivery-date'));
        $subForm2->addDecorator(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'eq-fields'));

        $subForm3 = new Zend_Form_SubForm(array('disableLoadDefaultDecorators' => true));
        $subForm3->setDecorators(array('FormElements',));
        $subForm3->addElement($this->_getDeliveryDateElement(3));
        $subForm3->addElement($this->_getDeliveryTimeElement(3));
        $subForm3->addDecorator(array('div' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'delivery-date'));
        $subForm3->addDecorator(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'eq-fields'));


        $this->addSubForm($subForm1, '1');
        $this->addSubForm($subForm2, '2');
        $this->addSubForm($subForm3, '3');
    }

    /**
     * @param bool|int $limit - to limit number of required dates
     *  in some cases form must be limited for only 1 required date
     * https://app.asana.com/0/12118343550314/15174981957817
     */
    public function _setDataRequired($limit = false)
    {
        foreach ($this->getSubforms() as $subform) {
            if (is_int($limit) && !$limit--) {
                break;
            }
            foreach ($subform->getElements() as $element) {
                $element->setRequired(true);
            }
        }
    }

    protected function _getDatepickerHtml($id)
    {
        return '<div id="dp-' . $id . '"></div>
                <div class="dp-time">
                    <ul class="inline-list">
                        <li data-time="8-12"><i class="apt-i-clock"></i> 8AM &ndash; 12PM</li>
                        <li data-time="12-16"><i class="apt-i-clock"></i> 12PM &ndash; 4PM</li>
                        <li data-time="16-20"><i class="apt-i-clock"></i> 4PM &ndash; 8PM</li>
                    </ul>
                </div>';
    }

    protected function _getDeliveryDateElement($id)
    {
        $deliveryDateeElement = new Zend_Form_Element_Text('date');

        $deliveryDateeElement->setBelongsTo('delivery[' . $id . ']')
            ->setRequired(false)
            ->addFilters(array('StripTags', 'StripNewlines', 'StringTrim'))
            ->setAttrib('name', 'delivery[' . $id . '][date]')
            ->setAttrib('class', 'form-field apt-dp field-i right')
            //->setDescription($this->_getDatepickerHtml($id))
            ->setAttrib('readonly', 'readonly');

        $deliveryDateeElement->setDecorators(array(
            'ViewHelper',
            //array('Description', array('tag'       => 'div', 'class'     => 'apt-datepick', 'placement' => 'append', 'escape'    => false)),
            array(array('holder' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'dp-holder to-eq pull-left')),
        ));

        return $deliveryDateeElement;
    }

    protected function _getDeliveryTimeElement($id)
    {
        $deliveryTimeElement = new Zend_Form_Element_Select('time');

        $options = array('7-12'  => '7AM – 12PM', '12-16' => '12PM – 4PM', '16-20' => '4PM – 8PM'/*, '17-21' => '5PM - 9PM'*/);
        $select  = array('' => 'Select a time window');

        $deliveryTimeElement->setBelongsTo('delivery[' . $id . ']')
            ->setRequired(false)
            ->addValidator('InArray', false, array(array_keys($options)))
            ->setAttrib('name', 'delivery[' . $id . '][time]')
            ->setAttrib('class', 'form-field text-small p-6')
            ->setMultiOptions($select + $options);

        $deliveryTimeElement->setDecorators(array(
            'ViewHelper',
            array(array('wrapper' => 'HtmlTag'), array('tag'   => 'div', 'class' => 'dp-holder to-eq pull-left')),
        ));

        return $deliveryTimeElement;
    }

    /**
     * Return only selected (filled) values
     * @return array
     */
    public function getSelectedDates()
    {
        $values = array_filter($this->getValues(), function($item){
            $notEmpty = false;
            array_walk_recursive($item, function($el) use (&$notEmpty){
                $notEmpty = (bool) $el;
            });

            return $notEmpty;
        });

        return $values;
    }

}
