<?php

class Checkout_Form_Terms extends Zend_Form
{

    public function init()
    {
        if (User_Model_Session::instance()->isLoggedIn()) {
            return;
        }

        $terms = new Zend_Form_Element_Checkbox('accept_agreement');
        $terms
            ->setRequired(true)
            ->addErrorMessage('You should accept Terms of Use an Privacy Policy.')
            ->setLabel('I agree to the <a href="' . Ikantam_Url::getUrl('terms/index') . '" class="text-primary dark" target="_blank">Terms of use</a> and <a href="' . Ikantam_Url::getUrl('privacy/index') . '" class="text-primary dark" target="_blank">Privacy Policy</a>')
            ->setUncheckedValue(null)
            ->setDecorators(array(
            'ViewHelper',
            array('Label', array('placement' => 'append', 'class' => 'inline')),
            array(array('addBlockFields' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields')),
            array(array('wrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'm-t20 text-mute')),
            array('Description', array('tag' => 'label', 'class' => 'block-lable title', 'placement' => 'prepend', 'escape'    => false)),


            ));

        $terms->getDecorator('label')->setOption('escape', false);
        $this->addElement($terms);
    }

}