<?php

class Checkout_Model_DeliveryDate extends Application_Model_Abstract
{

    protected $_borough = array();
    /**
     * @see https://docs.google.com/spreadsheets/d/1X6WAHGWVECBrjAMqrDdcjGSy_d8G-vgLRkokEybpxLk/edit#gid=738547738
     *
     * $dates = array(
     *  'seller neighborhood' => array(
     *      'buyer neighborhood' => array(
     *          'buyer selected date' => array('available pick up day', 'available pick up day', ...)
     *       )
     *  )
     * )
     */
    protected function _getTimes()
    {
        $dates = array(
            'Manhattan' => array(
                'Manhattan' => array(
                    '7-12'  => array(1 => '7-12'),
                    '16-20' => array(1 => '7-12', 3 => '16-20')
                ),
                'Brooklyn'  => array(
                    '12-16' => array(1 => '7-12'),
                    '16-20' => array(1 => '7-12', 3 => '16-20')
                ),
                'Queens'    => array(
                    '12-16' => array(1 => '7-12'),
                    '16-20' => array(1 => '7-12', 3 => '16-20')
                ),
                'Other'     => array(
                    '12-16' => array(1 => '7-12'),
                    '16-20' => array(1 => '7-12', 3 => '16-20')
                )
            ),
            'Brooklyn' => array(
                'Brooklyn'  => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Manhattan' => array(
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Queens'    => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Other'     => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                )
            ),
            'Queens'   => array(
                'Queens'    => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Brooklyn'  => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Manhattan' => array(
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Other'     => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                )
            ),
            'Other' => array(
                'Manhattan' => array(
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Brooklyn'  => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Queens'    => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                ),
                'Other'     => array(
                    '12-16' => array(2 => '12-16'),
                    '16-20' => array(2 => '12-16', 3 => '16-20')
                )
            )
        );

        return $dates;
    }

    public function getDeliveryTimesForBuyer($sellerPostcodes, $buyerPostcode)
    {

        // for multiple items
        if (count($sellerPostcodes) > 1)  {
            return array(
                '12-16' => array(),
                '16-20' => array()
            );
        }
        // for once item
        $sellerBoroughs = array();

        if (!is_array($sellerPostcodes)) {
            $sellerPostcodes = array($sellerPostcodes);
        }

        foreach ($sellerPostcodes as $_sellerPostcode) {
            $sellerBoroughs[] = $this->getBoroughNameByPostcode($_sellerPostcode);
        }

        $sellerBoroughs = array_unique($sellerBoroughs);
        $buyerBorough  = $this->getBoroughNameByPostcode($buyerPostcode);

        $dates = self::_getTimes();
        $variants = array();

        foreach ($sellerBoroughs as $_sellerBorough) {
            if (isset($dates[$_sellerBorough]) && isset($dates[$_sellerBorough][$buyerBorough])) {
                $variants += $dates[$_sellerBorough][$buyerBorough];
            }
        }

        return $variants;
    }

    public function getDeliveryTimesForSellerForOnceItem($sellerPostcode, $buyerPostcode, $buyerTime)
    {
        $sellerBorough = $this->getBoroughNameByPostcode($sellerPostcode);
        $buyerBorough  = $this->getBoroughNameByPostcode($buyerPostcode);

        $dates = self::_getTimes();
        if (empty($dates[$sellerBorough]) || empty($dates[$sellerBorough][$buyerBorough])) {
            return array();
        }
        // when order have removed time window, get earliest time window
        if (empty($dates[$sellerBorough][$buyerBorough][$buyerTime])) {
            $earlierSellerDates = reset($dates[$sellerBorough][$buyerBorough]);
            reset($earlierSellerDates);
            // return earlier date for seller, if date is not exist
            return array(key($earlierSellerDates) => current($earlierSellerDates));
        }

        return $dates[$sellerBorough][$buyerBorough][$buyerTime];
    }

    public function getDeliveryTimesForSellerForMultipleItems($sellerPostcode, $buyerPostcode, $buyerTime)
    {

        if ($buyerTime == '12-16') {
            return array(1 => '7-12', 2 => '12-16');
        }

        return $this->getDeliveryTimesForSellerForOnceItem($sellerPostcode, $buyerPostcode, $buyerTime);
    }

    public function getBoroughNameByPostcode($postcode)
    {
        if (empty($this->_borough[$postcode])) {
            $filter     = new Shipping_Model_Borough_FlexFilter(new Shipping_Model_Borough_Collection);
            $filter->postal_code('=', $postcode);
            $col = $filter->apply();
            if (count($col->getItems())) {
                $items = $col->getItems(); //!Strict standards: only variables should be passed by reference
                $item = array_pop($items);
                $boroughName = $item->getName();
            } else {
                $boroughName = 'Other';
            }

            $dates = self::_getTimes();

            if (!isset($dates[$boroughName])) {
                $boroughName = 'Other';
            }
            $this->_borough[$postcode] =  $boroughName;
        }
        return  $this->_borough[$postcode];
    }
}