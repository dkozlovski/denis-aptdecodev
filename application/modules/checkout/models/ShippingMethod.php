<?php

/**
 * @method string getId() get method's ID
 * @method Checkout_Model_ShippingMethod setId(string|int $id) set method's ID
 * @method string getTitle() get method's title
 * @method Checkout_Model_ShippingMethod setTitle(string $title) set method's titles
 * @method string getPrice() get method's price
 * @method Checkout_Model_ShippingMethod setPrice(float|int $price) set method's price
 * @method string getPriceType() get method's price type, may be "fixed" or "percent"
 * @method Checkout_Model_ShippingMethod setPriceType(string $priceType) set method's price type
 * @method string getIsActive() get method's active flag
 * @method Checkout_Model_ShippingMethod setIsActive(string $priceType) set method's active flag
 */
class Checkout_Model_ShippingMethod extends Application_Model_Abstract
{

    public function getShippingPrice(Product_Model_Product $product)
    {
        if ($this->getPriceType() == 'fixed') {
            return $this->getPrice();
        } elseif ($this->getPriceType() == 'percent') {
            return $product->getPrice() * $this->getPrice() / 100;
        }
    }

}