<?php

class Wishlist_AsyncController extends Ikantam_Controller_Front
{

	public function init()
	{
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

	}


    public function indexAction()
    {   
     

    }
    
    public function removeAction ()
    { 
    $id = $this->getRequest()->getParam('id');
        if((int)$id)
         {
           $wishlist = $this->getSession()->getUser()->getWishlist();
           $item = $wishlist->checkItemInWishlist($id);
           
           if($item)
                {
                    $product = new Product_Model_Product((int)$item->getProductId());
                    $item->delete();
                    echo Zend_Json::encode(array('url'=>$product->getProductUrl(),
                                                 'id'=>$product->getId(),
                                                 'title'=>$product->getTitle(),
                                                 'count'=>$wishlist->count(),
                                           )
                                                 );
                }  
         }     

    }
    
    public function restoreAction ()
    {
        $id = $this->getRequest()->getParam('id');
        if((int)$id)
            {
              $wishlist = $this->getSession()->getUser()->getWishlist();
              $wishlist->addProduct($id);
              $item = $wishlist->getItemByProductId($id,true);
              echo Zend_Json::encode(array(
              'itemId'=>$item->getId(),
              'count'=>$wishlist->count(),
              )); 
            }
    
    }
    


}