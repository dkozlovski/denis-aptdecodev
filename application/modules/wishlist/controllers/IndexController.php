<?php

class Wishlist_IndexController extends Ikantam_Controller_Front
{

	public function init()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
	}

	public function indexAction()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
		
		if ($this->getSession()->getUser()->getWishlist()->getAllItems()->getSize() < 1) {
			$this->_forward('empty', 'index', 'wishlist');
		}
		
		$this->view->wishlist = $this->getSession()->getUser()->getWishlist();
		$this->view->session = $this->getSession();
		
	}
	
	public function emptyAction()
	{
		$this->view->wishlist = $this->getSession()->getUser()->getWishlist();
		$this->view->session = $this->getSession();
	}

	public function addAction()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
		
		$productId = $this->getRequest()->getParam('id');
		$product = new Product_Model_Product($productId);

		$session = $this->getSession();

		if (!$product->getId() || !$product->getIsPublished()) {
			$session->addMessage('error', "This product does not exist.");
			$this->_redirect('wishlist/index');
		}
		
		if ($product->getUserId() == $this->getSession()->getUserId()) {
			$session->addMessage('error', "You cannot add this product to wishlist.");
			$this->_redirect('wishlist/index');
		}

		try {
			$this->getSession()->getUser()->getWishlist()->addProduct($product);

			$session->addMessage('success', sprintf("<i class=\"notificon\"></i><a href='%s'>%s</a> has been added to wishlist.", $product->getProductUrl(), $product->getTitle()));
		} catch (Exception $exc) {
			//echo $exc->getTraceAsString();
			$session->addMessage('error', $exc->getMessage());
		}

		$this->_redirect('wishlist/index');
	}

	public function removeItemAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();

		$response = array(
			'success' => false,
			'error' => 'Cannot delete item from wishlist',
			'message' => '',
		);

		$itemId = $this->getRequest()->getPost('id');
		$item = new Wishlist_Model_Wishlist_Item($itemId);

		if ($item->getId()) {
			if ($item->getUserId() == $this->getSession()->getUserId()) {
				$item->delete();

				$message = '<i class="notificon"></i><a href="%s">%s</a> has been deleted. <a class="restore" href="%s">Restore</a>';
				$url1 = $item->getProduct()->getProductUrl();
				$title = $item->getProduct()->getTitle();
				$url2 = $item->getProduct()->getAddToWishlistUrl();

				$response['success'] = true;
				$response['message'] = sprintf($message, $url1, $title, $url2);
				$response['error'] = '';
			}
		}

		$this->getResponse()
				->setHeader('Content-type', 'text/plain')
				->setBody(json_encode($response));
	}

}
