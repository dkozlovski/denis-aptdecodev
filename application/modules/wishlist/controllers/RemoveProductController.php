<?php

class Wishlist_RemoveProductController extends Ikantam_Controller_Front
{

	public function init()
	{
		/* Initialize action controller here */
	}

	public function postAction()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
		
		$items = $this->getRequest()->getPost('items');

		foreach ($items as $itemId) {
			$item = new Wishlist_Model_Wishlist_Item($itemId);

			if ($item->getId() && $item->getUserId() == $this->getSession()->getUserId()) {
				$item->delete();
			}
		}
		
		$this->_redirect('wishlist/index');
	}
	
	public function addAction()
	{
		if (!$this->getSession()->isLoggedIn()) {
			$this->_redirect('user/login/index');
		}
		
		$items = $this->getRequest()->getPost('items');
		$session = $this->getSession();
		foreach ($items as $itemId) {
			$item = new Wishlist_Model_Wishlist_Item($itemId);

			if ($item->getId() && $item->getUserId() == $this->getSession()->getUserId()) {
				$product = new Product_Model_Product($item->getProductId());
				
				if (!$product->getId() || !$product->getIsPublished()) {
					$session->addMessage('error', "This product does not exist.");
				} else {
				try {
					
						if ($item->getProductId() == $product->getId()) {
							$item->delete();
						}
					
						$session->getCart()->addProduct($product);

						$session->addMessage('success', sprintf("<i class=\"notificon\"></i><a href='%s'>%s</a> has been added to cart.", $product->getProductUrl(), $product->getTitle()));
					} catch (Exception $exc) {
						//echo $exc->getTraceAsString();
						$session->addMessage('error', $exc->getMessage());
					}
				}
			}
		}

		$this->_redirect('cart/index');
		
		
		
	}

}
