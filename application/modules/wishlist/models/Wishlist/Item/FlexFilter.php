<?php
/**
 * Author: Alex P.
 * Date: 17.06.14
 * Time: 19:21
 */

class Wishlist_Model_Wishlist_Item_FlexFilter extends Ikantam_Filter_Flex
{
    protected $_acceptClass= 'Wishlist_Model_Wishlist_Item_Collection';

    protected $_joins = array(
        'notifications_unsubscribe' => 'LEFT JOIN `notifications_unsubscribe` ON `notifications_unsubscribe`.`user_id` = `wishlist_items`.`user_id`',
    );

    protected $_rules = array(
        'user_unsubscribe_notifications' => array('notifications_unsubscribe' => '`notifications_unsubscribe`.`is_user_confirm`'),
    );
} 