<?php

class Wishlist_Model_Wishlist_Item_Collection extends Application_Model_Abstract_Collection implements
    Ikantam_Filter_Flex_Interface
{

	protected function _getBackend()
	{
		return new Wishlist_Model_Wishlist_Backend();
	}
	
	public function getByUserId($id)
	{
		$this->_getBackend()->getByUserId($this, $id);
		return $this;
	}
    public function getAbandoned($time, $isNotified)
    {
        return $this->_getBackend()->getAbandoned($this, $time, $isNotified);
    }

    /**
     * Returns a filter related with collection.
     * @return object Ikantam_Filter_Flex
     */
    public function  getFlexFilter()
    {
       return new Wishlist_Model_Wishlist_Item_FlexFilter($this);
    }
}