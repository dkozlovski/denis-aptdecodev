<?php

/**
 * Class Wishlist_Model_Wishlist_UserNotified
 * @method int getUserId()
 * @method int getLastSendDate1()
 * @method int getLastSendDate2()
 * @method int getLastSendDate3()
 * @method Wishlist_Model_Wishlist_UserNotified_Backend _getbackend()
 *
 * @method Wishlist_Model_Wishlist_UserNotified setUserId(int $userId)
 * @method Wishlist_Model_Wishlist_UserNotified setLastSendDate1(int $time)
 * @method Wishlist_Model_Wishlist_UserNotified setLastSendDate2(int $time)
 * @method Wishlist_Model_Wishlist_UserNotified setLastSendDate3(int $time)
 */
class Wishlist_Model_Wishlist_UserNotified extends  Application_Model_Abstract
{

    public function getByUserId($userId)
    {
        $this->_getbackend()->getByUserId($this, $userId);
        return $this;
    }
}