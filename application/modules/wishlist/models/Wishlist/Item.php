<?php

/**
 * Class Wishlist_Model_Wishlist_Item
 * @method Wishlist_Model_Wishlist_Backend _getbackend()
 * @method int getUserId()
 * @method int getProductId()
 * @method int getCreatedAt()
 * @method int getIsNotified()
 * @method Wishlist_Model_Wishlist setProductId(int $productId)
 * @method Wishlist_Model_Wishlist setCreatedAt(int $time)
 * @method Wishlist_Model_Wishlist setIsNotified(int $isNotified)
 */
class Wishlist_Model_Wishlist_Item extends Application_Model_Abstract
{
    protected $_user;
    protected $_backendClass = 'Wishlist_Model_Wishlist_Backend';


	public function getProduct()
	{
		$product = new Product_Model_Product();
		$product->getById($this->getProductId());
		return $product;
	}

    protected function _beforeSave()
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
    }

    /**
     * @param $isNotified
     * @param $userId
     * @return $this
     */
    public function setIsNotifiedForUserId($isNotified, $userId)
    {
        $this->_getbackend()->setIsNotifiedForUserId($isNotified, $userId);
        return $this;
    }

    /**
     * @return User_Model_User
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = new User_Model_User($this->getUserId());
        }
        return  $this->_user;
    }


}
