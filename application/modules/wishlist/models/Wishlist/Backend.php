<?php

class Wishlist_Model_Wishlist_Backend extends Application_Model_Abstract_Backend
{

    protected $_table     = 'wishlist_items';
    protected $_itemClass = 'Wishlist_Model_Wishlist_Item';

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql  = 'INSERT INTO `wishlist_items` (`user_id`, `product_id`, `created_at`) VALUES (:user_id, :product_id, :created_at)';
        $stmt = $this->_getConnection()->prepare($sql);

        $user_id    = $object->getUserId();
        $product_id = $object->getProductId();
        $created_at = $object->getCreatedAt();

        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':product_id', $product_id);
        $stmt->bindParam(':created_at', $created_at);

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        
    }

    public function isProductInWishlist($user_id, $product_id)
    {
        $sql = 'SELECT * FROM `wishlist_items` WHERE `user_id` = :user_id AND `product_id` = :product_id;';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':product_id', $product_id);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (is_array($result) && count($result)) {
            return true;
        }
        return false;
    }

    public function getByUserId($collection, $id)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `user_id` = :user_id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $id);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Wishlist_Model_Wishlist_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function setIsNotifiedForUserId($isNotified, $userId)
    {
        $sql = 'UPDATE  `' . $this->_getTable() . '` SET `is_notified` = ' . (int) $isNotified . '  WHERE `user_id` = :user_id';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();
        return $this;
    }

    /**
     * @param $collection
     * @param $time
     * @param int $isNotified
     * @return Wishlist_Model_Wishlist_Item_Collection
     */
    public function getAbandoned(Wishlist_Model_Wishlist_Item_Collection $collection, $time, $notified = 1)
    {
        $sql = "SELECT *
            FROM `" . $this->_getTable() . "` AS `main_table`
            LEFT JOIN `wishlist_user_notified` AS `wun`
                ON `main_table`.`user_id` = `wun`.`user_id`
            LEFT JOIN `app_settings` AS `a_s`
                ON  `a_s`.`name` = 'user_notification_cart_and_wishlist'
            LEFT JOIN `users_app_settings` AS `uas`
                ON `main_table`.`user_id` = `uas`.`user_id` AND `uas`.`app_setting_id` = `a_s`.`id`

            WHERE `created_at` < :time  AND `is_notified` < :is_notified
                AND GREATEST(IFNULL(`wun`.`last_send_date1`, 0), IFNULL(`wun`.`last_send_date2`, 0), IFNULL(`wun`.`last_send_date3`, 0)) + IFNULL(`uas`.`value`, 86400) < :now
            GROUP BY `main_table`.`user_id`
            LIMIT 0 , 10";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':time', $time);
        $stmt->bindParam(':is_notified', $notified);
        $stmt->bindParam(':now', time());

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Wishlist_Model_Wishlist_Item();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
        return $collection;
    }

}