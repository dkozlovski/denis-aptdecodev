<?php

/**
 * Class Wishlist_Model_Wishlist
 * @method Wishlist_Model_Wishlist_Backend _getbackend()
 * @method int getUserId()
 * @method int getProductId()
 * @method int getCreatedAt()
 * @method int getIsNotified()
 * @method Wishlist_Model_Wishlist setProductId(int $productId)
 * @method Wishlist_Model_Wishlist setCreatedAt(int $time)
 * @method Wishlist_Model_Wishlist setIsNotified(int $isNotified)
 *
 */
class Wishlist_Model_Wishlist extends Application_Model_Abstract
{

    protected $_userId;
    protected $_items;
    protected $_user;

    public function setUserId($userId)
    {
        $this->_userId = $userId;
        $this->setData('user_id', $userId);
        return $this;
    }

    public function getAllItems()
    {
        if (!$this->_items) {
            $this->_items = new Wishlist_Model_Wishlist_Item_Collection();
            $this->_items->getByUserId($this->_userId);
        }
        return $this->_items;
    }

    protected function isProductInWishlist($product)
    {
        return $this->_getbackend()->isProductInWishlist($this->_userId, $product->getId());
    }

    public function addProduct($product/* $param, $shipping = null */)
    {
        if (!$this->isProductInWishlist($product)) {
            $item = new Wishlist_Model_Wishlist();

            $product->selfTriggerEvent('wishlist.add'); // See Application_Model_Events_Listeners_Product (handlers)

            $item->setUserId($this->_userId)
                    ->setProductId($product->getId())
                    ->save();
        } else {
            throw new Exception(sprintf('<i class="notificon"></i><a href="%s">%s</a> is already in wishlist.', $product->getProductUrl(), $product->getTitle()));
        }

        return $this;
    }

    public function count()
    {
        return count($this->getAllItems());
    }

    protected function _beforeSave()
    {
        if (!$this->getCreatedAt()) {
            $this->setCreatedAt(time());
        }
    }

}