<?php

class Category_AsyncController extends Ikantam_Controller_Front
{

    public function init()
    {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function indexAction()
    {
        $data = $this->getRequest()->getParam('data');


        $products = new Product_Model_Product_Collection();
        try {

//------------FILTERS
            $filters = array();

            if (isset($data['brand']) && is_array($data['brand'])) {
                $filters[] = new Product_Model_Filter(Product_Model_Filter::FILTER_BY_BRAND, array('brands' => $data['brand']));
            }

            if (isset($data['condition']) && is_array($data['condition'])) {
                $filters[] = new Product_Model_Filter(Product_Model_Filter::FILTER_BY_CONDITION, array('conditions' => $data['condition']));
            }

            if (isset($data['color']) && is_array($data['color'])) {
                $filters[] = new Product_Model_Filter(Product_Model_Filter::FILTER_BY_COLOR, array('colors' => $data['color']));
            }

            if (isset($data['price']) && is_array($data['price'])) {
                $filters[] = new Product_Model_Filter(Product_Model_Filter::FILTER_BY_PRICE, $data['price']);
            }

//*******************
//------------Sorting params
            $sortBy = $products::SORT_BY_DATE;
            if ($data['sort'] == $products::SORT_BY_PRICE) {
                $sortBy = $products::SORT_BY_PRICE;
            }
            $direction = $products::SORT_DESC;
            if ($data['direction'] == $products::SORT_ASC) {
                $direction = $products::SORT_ASC;
            }
//*******************

            $products->loadSortedBy($sortBy, $direction, $data['category'], $data['page'], 6, $filters);

            /* $products->filterBy($products::FILTER_BY_CONDITION,$data)
              ->filterBy($products::FILTER_BY_BRAND,array('id'=>$data['brand']))
              ->filterBy($products::FILTER_BY_COLOR,array('id'=>$data['color']))
              ->filterBy($products::FILTER_BY_PRICE,$data['price']); */

            if ($products->getPagesCount() < $data['page']) {
                echo Zend_Json::encode(array());
                return;
            }

            $answer = array();
            foreach ($products->toArray() as $product) {
                $_product = new Product_Model_Product($product['id']);
                $answer[] = array(
                    'title'        => $product['title'],
                    'price'        => $this->view->currency($product['price'])->toCurrency(),
                    'imagePath'    => Ikantam_Url::getPublicUrl('upload/products/' . $_product->getMainImage()->getPath()),
                    'sellerAvatar' => Ikantam_Url::getPublicUrl('upload/avatars/' . $_product->getSeller()->getAvatar()->getPath())
                );
            }
            echo Zend_Json::encode($answer);
        } catch (Exception $ex) {
            throw $ex;
            echo Zend_Json::encode(array());
        }
    }

    public function subcategoriesAction()
    {
        $id            = (int) $this->getRequest()->getParam('categoryId');
        $category      = new Category_Model_Category($id);
        $subCategories = $category->getSubCategories();
        echo Zend_Json::encode($subCategories->getOptions());
    }

}
