<?php

class Category_ViewController extends Ikantam_Controller_Front
{

    public function init()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/catalog_filters.js'));
    }

    public function indexAction()
    {

        $id = $this->getRequest()->getParam('id');

        $category      = new Category_Model_Category((int) $id);
        $products      = new Product_Model_Product_Collection();
        $colors        = new Application_Model_Color_Collection();
        $manufacturers = new Application_Model_Manufacturer_Collection();
        try {
            if ($category->isParent()) {
                $products->loadSortedBy($products::SORT_BY_DATE, $products::SORT_DESC, $category->getSubCategoriesId(), 1, 6);
            } else {
                $products->loadSortedBy($products::SORT_BY_DATE, $products::SORT_DESC, $id);
            }
        } catch (Exception $ex) {
            
        }

        $this->view->products      = $products->getItems();
        $this->view->subCategories = $category->getSubCategories()->getItems();
        $this->view->brands        = $manufacturers->getByCategories($products->getCategoriesId(), true)->getItems();
        $this->view->colors        = $colors->getAll()->getItems();
        $this->view->id            = $id; //Category id 
        // print_r($products->getCategoriesId());
        // print_r($category->getSubCategoriesId());           
    }

}