<?php

class Category_IndexController extends Ikantam_Controller_Front
{

    protected $_itemsCountPerPage = 42;
    protected $_maxCountPage;
    protected $_maxLinkPage = 6;

    protected function _initJsCss()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/iKantam/modules/hash.uri.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery.autocomplete.js'));
    }

    public function indexAction()
    {
        $this->view->headLink()->appendStylesheet(Ikantam_Url::getPublicUrl('js/plugins/iCheck/skins/flat/aptdeco.css'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/plugins/iCheck/icheck.js'));
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery.mousewheel.min.js'));

        $form = new Category_Form_ProductFilter();

        $this->view->form = $form;


        $itemsPerPage = $this->_itemsCountPerPage;
        $page         = 1;
        $data         = $this->getRequest()->getParams();


        $this->view->category = new Category_Model_Category($this->getRequest()->getParam('id'));
        $this->view->headTitle(sprintf(' Used %s for sale in NYC',strtolower($this->view->category->getTitle())));

        //Meta
        $category_meta = $this->view->category->getTitle();
        $single        = (strtolower(substr($category_meta, -1)) == 's') ? substr($category_meta, 0, strlen($category_meta) - 1) : $category_meta;
        $msg           = sprintf("Used %s for sale in NYC available for delivery through AptDeco. Browse gently used furniture from a trusted community of sellers and arrange for affordable delivery on AptDeco.", strtolower($category_meta), $single);
        $this->view->headMeta()->appendName('description', $msg);

        $this->view->session = $this->getSession();

        $this->view->appendHeader = false;
        $query                    = $this->getRequest()->getParam('id');
        $data['query']            = $query;

        if ($form->isValid($data)) {

            $offset = ($page - 1) * $itemsPerPage;

            $data = $form->getValues();
            unset($data['query']);

            if (empty($data['min_price'])) {
                unset($data['min_price']);
            }
            if (empty($data['max_price'])) {
                unset($data['max_price']);
            }
            if (empty($data['colors']) || !is_array($data['colors']) || !count($data['colors'])) {
                unset($data['colors']);
            }
            if (empty($data['brands']) || !is_array($data['brands']) || !count($data['brands'])) {
                unset($data['brands']);
            }
            if (empty($data['categories']) || !is_array($data['categories']) || !count($data['categories'])) {
                unset($data['categories']);
            }
            if (empty($data['conditions']) || !is_array($data['conditions']) || !count($data['conditions'])) {
                unset($data['conditions']);
            }
            //$data['user_id'] = $this->getSession()->getUserId();

            $categoryId = $this->getRequest()->getParam('id');
            $category   = new Category_Model_Category($categoryId);

            if ($category->getParentId()) {
                $data['categories'] = array($category->getId());
            } else {
                $data['parent_category_id'] = $category->getId();
            }

            $data['is_approved'] = 1;

            $c = new Product_Model_Product2_Collection();
            $maxCountElement = clone $c;
            $maxCountElement = count($maxCountElement->setFilters($data)->load());
            $c->setFilters($data)
                    ->setOrder(array(
                        'is_product_sold'    => 'asc',
                        'is_expired'         => 'asc',
                        'approved_at'        => 'desc',
                        'seller_popularity'  => 'desc',
                        'product_popularity' => 'desc',
                        'updated_at'         => 'desc',
                    ))
                    ->setOffset($offset)
                    ->setLimit($itemsPerPage)
                    ->load();

            $this->_maxCountPage = ceil($maxCountElement/$itemsPerPage);
            $products = $c;

            // actual products
            $this->view->products = $products;
            $this->view->currentPage = 1;
            $this->view->maxCountPage = $this->_maxCountPage;
            if($this->_maxCountPage < $this->_maxLinkPage){
                $this->view->maxLinkPage = $this->_maxCountPage;
            }else{
                $this->view->maxLinkPage = $this->_maxLinkPage;
            }
            $alertParams            = Product_Model_Alert::getParamsFromRequest($this->getRequest());
            $alertParams['is_ajax'] = 1;

            if ($products->getSize() == 0) {
                $this->view->appendHeader = true;
                $this->view->params       = $alertParams;
            }

            $this->view->query = $query;

            $categoryCollection  = new Category_Model_Category_Collection();
            $availableCategories = $categoryCollection->getAllSubcategoriesByCategory($this->view->category->getId());

            $colorCollection = new Application_Model_Color_Collection();
            $availableColors = $colorCollection->getByCategory($query);

            $brandCollection = new Application_Model_Manufacturer_Collection();
            $availableBrands = $brandCollection->getByCategory($query);

            if (count($availableCategories) > 0) {
                $this->view->form->getElement('categories')->setMultiOptions($availableCategories);
            } else {
                $this->view->form->removeElement('category_slider');
                $this->view->form->removeElement('categories');
            }

            $this->view->form->getElement('colors')->setMultiOptions($availableColors);
            $this->view->form->getElement('brands')->setMultiOptions($availableBrands);

            $productsCollection = new Product_Model_Product_Collection();

            $this->view->minPrice = (int) floor($productsCollection->getMinPriceByCategory($query) / 10) * 10;
            $this->view->maxPrice = (int) ceil($productsCollection->getMaxPriceByCategory($query) / 10 * 10);

            $this->view->selectedMinPrice = (int) $this->getRequest()->getParam('min_price', $this->view->minPrice);
            $this->view->selectedMaxPrice = (int) $this->getRequest()->getParam('max_price', $this->view->maxPrice);

            $this->view->form->populate(array(
                'min_price'  => $this->view->selectedMinPrice,
                'max_price'  => $this->view->selectedMaxPrice,
                'categories' => (array) $this->getRequest()->getParam('categories'),
                'brands'     => (array) $this->getRequest()->getParam('brands'),
                'colors'     => (array) $this->getRequest()->getParam('colors'),
                'conditions' => (array) $this->getRequest()->getParam('conditions')
            ));
        }
    }

    public function ajaxAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $session  = $this->getSession();
        $pageHash = $session->getPageHash();

        $response                 = array(
            'success'  => false,
            'error'    => 'Cannot add item to cart',
            'message'  => '',
            'subtotal' => ''
        );
        $this->view->appendHeader = false;
        $hp                       = $this->getRequest()->getParam('history_page', false);

        $itemsPerPage = $this->_itemsCountPerPage;

        $page = (int) $this->getRequest()->getParam('page');

        $data = $this->getRequest()->getParams();

        $query         = $this->getRequest()->getParam('id');
        $data['query'] = $query;

        $form = new Category_Form_ProductFilter();

        if ($form->isValid($data)) {


            $offset = ($page - 1) * $itemsPerPage;

            $data = $form->getValues();
            unset($data['query']);

            if (empty($data['min_price'])) {
                unset($data['min_price']);
            }
            if (empty($data['max_price'])) {
                unset($data['max_price']);
            }
            if (empty($data['colors']) || !is_array($data['colors']) || !count($data['colors'])) {
                unset($data['colors']);
            }
            if (empty($data['brands']) || !is_array($data['brands']) || !count($data['brands'])) {
                unset($data['brands']);
            }
            if (empty($data['categories']) || !is_array($data['categories']) || !count($data['categories'])) {
                unset($data['categories']);
            }
            if (empty($data['conditions']) || !is_array($data['conditions']) || !count($data['conditions'])) {
                unset($data['conditions']);
            }
            //$data['user_id'] = $this->getSession()->getUserId();

            $categoryId = $this->getRequest()->getParam('id');
            $category   = new Category_Model_Category($categoryId);

            if ($category->getParentId()) {
                $data['categories'] = array($category->getId());
            } else {
                $data['parent_category_id'] = $category->getId();
            }

            $data['is_approved'] = 1;

            $c = new Product_Model_Product2_Collection();
            $maxCountElement = clone $c;
            $maxCountElement = count($maxCountElement->setFilters($data)->load());


            if ($hp) {
                //$offset       = 0;
                //$itemsPerPage = $hp * $this->_itemsCountPerPage - $this->_itemsCountPerPage;
            }

            
            $c->setFilters($data)
                    ->setOffset($offset)
                    ->setLimit($itemsPerPage)
                    ->setOrder(array(
                        'is_product_sold' => 'asc',
                        'is_expired'      => 'asc',
                            // 'seller_popularity' => 'desc',
                            // 'product_popularity'=>'desc',
            ));

            $orderPrice  = $this->getRequest()->getParam('order_price');
            $orderRating = $this->getRequest()->getParam('order_rating');
            $orderDate   = $this->getRequest()->getParam('order_date');

            if ($orderPrice == 'asc' || $orderPrice == 'desc') {
                $c->addOrder('price', $orderPrice);
            } elseif ($orderRating == 'asc' || $orderRating == 'desc') {
                $c->addOrder('seller_rating', $orderRating);
            } elseif ($orderDate == 'asc' || $orderDate == 'desc') {
                $c->addOrder('approved_at', $orderDate);
            } else {

                $c->addOrder('approved_at', 'desc')
                        ->addOrder('seller_popularity', 'desc')
                        ->addOrder('product_popularity', 'desc')
                        ->addOrder('updated_at', 'desc');
            }

            $c->load();
            $products = $c;
            $this->view->category = $category;
            $this->_maxCountPage = ceil($maxCountElement/$itemsPerPage);
            $this->view->currentPage = (int)$this->getRequest()->getParam('page');
            $this->view->maxCountPage = $this->_maxCountPage;
            if($this->_maxCountPage < $this->_maxLinkPage){
                $this->view->maxLinkPage = $this->_maxCountPage;
            }else{
                $this->view->maxLinkPage = $this->_maxLinkPage;
            }
//			$products = new Product_Model_Product_Collection();
//			$offset = ($page - 1) * $itemsPerPage;
//			$products->filterAndFilter($form->getValues(), $offset, $itemsPerPage);

            $alertParams            = Product_Model_Alert::getParamsFromRequest($this->getRequest());
            $alertParams['is_ajax'] = 1;

            if ($products->getSize() == 0) {
                $this->view->appendHeader = true;
                $this->view->params       = $alertParams;
            }

            $this->view->products  = $products;
            $this->view->setScriptPath(APPLICATION_PATH . '/views/scripts');
            $response['template']  = $this->view->render('search/search_items.phtml');
            $response['alert_url'] = Ikantam_Url::getUrl('product/alert/popup', $alertParams);
            $response['success']   = true;
        } else {
            
        }

        //$session->setPage($page);

        $this->getResponse()
                ->setHeader('Content-type', 'application/json')
                ->setBody(json_encode($response));
    }

    public function checkIfBackAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        if (User_Model_Session::instance()->getPageHash() === $this->getRequest()->getParam('pageHash')) {
            exit(Zend_Json::encode(array('status' => true, 'page' => User_Model_Session::instance()->getPage())));
        }

        User_Model_Session::instance()->setPageHash($this->getRequest()->getParam('pageHash'));

        exit(Zend_Json::encode(array('status' => false)));
    }

}