<?php

class Category_Form_ProductFilter extends Zend_Form
{

    const ERROR_CATEGORY_VALUE     = 'Please, select a category';
    const ERROR_SUBCATEGORY_VALUE  = 'Please, select a subcategory';
    const ERROR_MIN_PRICE_VALUE    = 'Please, enter correct min price.';
    const ERROR_MAX_PRICE_VALUE    = 'Please, enter correct max price.';
    const ERROR_CONDITION_VALUE    = 'Please, select condition.';
    const ERROR_COLOR_VALUE        = 'Please, select a color';
    const ERROR_MANUFACTURER_VALUE = 'Manufacturer is required.';

    public function init()
    {
        $this->addDecorator('FormElements')
                ->addDecorator('Form');

        $this->setAttrib('id', 'search-filter');
        $this->setAttrib('class', 'apt-form');


        $this->addElements($this->getHiddenElements());

        $this->addElement($this->getClearAllElement())
                ->addElement($this->getSubCategoryElement())
                ->addElement($this->getSubCategoryScrollElement())
                ->addElement($this->getPricesNoteElement())
                ->addElement($this->getMinPriceElement())
                ->addElement($this->getMaxPriceElement())
                ->addElement($this->getConditionElement())
                ->addElement($this->getColorNoteElement())
                ->addElement($this->getColorElement())
                ->addElement($this->getColorClearElement())
                ->addElement($this->getManufacturerNoteElement())
                ->addElement($this->getManufacturerElement())
                ->addElement($this->getManufacturerScrollElement())
                ->addElement($this->getOnlyAvailableElement());
    }

    protected function getHiddenElements()
    {
        $query       = new Zend_Form_Element_Hidden('query');
        $orderPrice  = new Zend_Form_Element_Hidden('order_price');
        $orderRating = new Zend_Form_Element_Hidden('order_rating');
        $orderDate   = new Zend_Form_Element_Hidden('order_date');

        $query->setDecorators(array('ViewHelper'));
        $orderPrice->setDecorators(array('ViewHelper'));
        $orderRating->setDecorators(array('ViewHelper'));
        $orderDate->setDecorators(array('ViewHelper'));

        return array($query, $orderPrice, $orderRating, $orderDate);
    }

    protected function getClearAllElement()
    {
        $note = new Ikantam_Form_Element_Note('clear_all');

        $note->setRequired(false)
                ->setDescription('<a href="javascript:void(0);" onclick="clearAll();" class="clearall">Clear all</a>');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'div', 'class' => 'widget-title', 'escape' => false)),
            array(array('widget' => 'HtmlTag'), array('tag' => 'div', 'class' => 'apt-widget')),
        ));

        return $note;
    }

    protected function getSubCategoryScrollElement()
    {
        $note = new Ikantam_Form_Element_Note('category_slider');

        $note->setRequired(false)
                ->setDescription('<span></span>');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'div', 'class' => 'scroll', 'escape' => false)),
            array(array('scrollable' => 'Htmltag'), array('tag' => 'div', 'closeOnly' => true)),
            array(array('scrollableholder' => 'Htmltag'), array('tag' => 'div', 'closeOnly' => true)),
            array(array('widget' => 'Htmltag'), array('tag' => 'div', 'closeOnly' => true)),
        ));

        return $note;
    }

    protected function getSubCategoryElement()
    {
        $subCategoryElement = new Zend_Form_Element_MultiCheckbox('categories');

        $collection = new Category_Model_Category_Collection();
        $values     = $collection->getOptions2();

        $subCategoryElement->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim', 'Null'))
                ->addValidator('InArray', true, array(array_keys($values)))
                ->addErrorMessage(self::ERROR_SUBCATEGORY_VALUE)
                ->setMultiOptions($values)
                ->setAttrib('class', 'aptdeco-styled-checkbox')
                ->setAttrib('label_style', 'font-size: 12px;margin-bottom: 0;')
                ->setDescription('<h4>Styles</h4><a href="javascript:void(0);" onclick="clearStyle();" id="clear-styles" style="visibility:hidden">Clear</a>');

        $subCategoryElement->setDecorators(array(
            'ViewHelper',
            array(array('addLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('addUl' => 'HtmlTag'), array('tag' => 'ul')),
            array(array('content' => 'HtmlTag'), array('tag' => 'div', 'class' => 'content categories')),
            array(array('scrollable' => 'Htmltag'), array('tag' => 'div', 'class' => 'scrollable', 'openOnly' => true)),
            array(array('scrollableholder' => 'Htmltag'), array('tag' => 'div', 'class' => 'scrollable-holder', 'openOnly' => true)),
            array('Description', array('placement' => 'prepend', 'tag' => 'div', 'class' => 'widget-title', 'escape' => false)),
            array(array('widget' => 'Htmltag'), array('tag' => 'div', 'class' => 'apt-widget', 'openOnly' => true)),
        ));

        $subCategoryElement->setSeparator('</li><li>');

        return $subCategoryElement;
    }

    protected function getPricesNoteElement()
    {
        $note = new Ikantam_Form_Element_Note('prices_note');

        $note->setRequired(false)
                ->setDescription('<h4>Price</h4><a href="javascript:void(0);" onclick="clearPrice();" id="clear-prices" style="visibility:hidden">Clear</a>');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'div', 'class' => 'widget-title', 'escape' => false)),
            array(array('widget' => 'HtmlTag'), array('tag' => 'div', 'class' => 'apt-widget', 'openOnly' => true)),
        ));

        return $note;
    }

    protected function getMinPriceElement()
    {
        $minPriceElement = new Zend_Form_Element_Text('min_price');

        $minPriceElement->setRequired(false)
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->addValidator('Float', true)
                ->addErrorMessage(self::ERROR_MIN_PRICE_VALUE)
                ->setLabel('From')
                ->setDescription('$')
                ->setAttrib('class', 'form-field m-0');

        $minPriceElement->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag' => 'span', 'placement' => 'prepend', 'class' => 'add-on')),
            array(array('inputprepend' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-prepend pull-left')),
            //<div class="clear"></div>
            array('Label', array('class' => 'inner-label text-mute', 'placement' => 'prepend')),
            array(array('blockfields' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields pull-left')),
            array(array('controlgroup' => 'HtmlTag'), array('tag' => 'div', 'class' => 'control-group m-0', 'openOnly' => true)),
        ));

        return $minPriceElement;
    }

    protected function getMaxPriceElement()
    {
        $maxPriceElement = new Zend_Form_Element_Text('max_price');

        $maxPriceElement->setRequired(false)
                ->addFilters(array('StringTrim', 'StripTags', 'StripNewlines', 'Null'))
                ->addValidator('Float', true)
                ->addErrorMessage(self::ERROR_MAX_PRICE_VALUE)
                ->setDescription('$')
                ->setLabel('To')
                ->setAttrib('class', 'form-field m-0');

        $maxPriceElement->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag' => 'span', 'placement' => 'prepend', 'class' => 'add-on')),
            array(array('inputprepend' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-prepend pull-left')),
            //<div class="clear"></div>
            array('Label', array('class' => 'inner-label text-mute', 'placement' => 'prepend')),
            array(array('blockfields' => 'HtmlTag'), array('tag' => 'div', 'class' => 'block-fields pull-right')),
            array(array('controlgroup' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true)),
            array(array('widget' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true)),
        ));

        return $maxPriceElement;
    }

    protected function getConditionElement()
    {
        $currentCondition = new Zend_Form_Element_MultiCheckbox('conditions');

        $values = array(
            'new'          => 'New',
            //'like-new'     => 'Like New',
            'good'         => 'Good',
            'satisfactory' => 'Satisfactory',
            'age-worn'     => 'Age-worn');

        $currentCondition->setRequired(false)
                ->addValidator('InArray', true, array(array_keys($values)))
                ->setMultiOptions($values)
                ->setAttrib('class', 'aptdeco-styled-checkbox')
                ->setAttrib('label_style', 'font-size: 12px;margin-bottom: 0;')
                ->addErrorMessage(self::ERROR_CONDITION_VALUE)
                ->setDescription('<h4>Conditions</h4><a href="javascript:void(0);" onclick="clearCondition();" id="clear-conditions" style="visibility:hidden">Clear</a>');

        $currentCondition->setDecorators(array(
            'ViewHelper',
            array(array('addLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('addUl' => 'HtmlTag'), array('tag' => 'ul')),
            array(array('content' => 'HtmlTag'), array('tag' => 'div', 'class' => 'content categories')),
            array('Description', array('tag' => 'div', 'class' => 'widget-title', 'placement' => 'prepend', 'escape' => false)),
            array(array('widget' => 'HtmlTag'), array('tag' => 'div', 'class' => 'apt-widget')),
        ));

        $currentCondition->setSeparator('</li><li>');

        return $currentCondition;
    }

    protected function getColorNoteElement()
    {
        $note = new Ikantam_Form_Element_Note('color_note');

        $note->setRequired(false)
                ->setDescription('<h4>Color</h4><a href="javascript:void(0);" onclick="clearColor();" id="clear-colors" style="visibility:hidden">Clear</a>');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag' => 'div', 'class' => 'widget-title', 'placement' => 'prepend', 'escape' => false)),
            array(array('widget' => 'HtmlTag'), array('tag' => 'div', 'class' => 'apt-widget', 'openOnly' => true)),
        ));

        return $note;
    }

    protected function getColorElement()
    {
        $color = new Ikantam_Form_Element_Colors('colors');

        $collection = new Application_Model_Color_Collection();
        $values     = $collection->getOptions2();

        $color->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim', 'Int', 'Null'))
                ->addValidator('InArray', true, array(array_keys($values)))
                ->setMultiOptions($collection->getAsOptions())
                ->addErrorMessage(self::ERROR_COLOR_VALUE);

        $color->setDecorators(array(
            'ViewHelper',
            array(array('AddTheLi' => 'HtmlTag'), array('tag' => 'div', 'class' => 'color addItemcolor ttip', 'style' => 'cursor:pointer')),
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class' => 'content categories')),
        ));

        $color->setSeparator('</div><div style="cursor:pointer" class="color addItemcolor ttip">');

        return $color;
    }

    protected function getColorClearElement()
    {
        $note = new Ikantam_Form_Element_Note('color_clear');

        $note->setRequired(false)
                ->setDescription('&nbsp;');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'div', 'class' => 'clear m--b5', 'escape' => false)),
            array(array('widget' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true)),
        ));

        return $note;
    }

    protected function getManufacturerNoteElement()
    {
        $note = new Ikantam_Form_Element_Note('manufacturer_note');

        $note->setRequired(false)
                ->setDescription('<h4>Brands</h4><a href="javascript:void(0);" onclick="clearBrand();" id="clear-brands" style="visibility:hidden">Clear</a>');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('tag' => 'div', 'class' => 'widget-title', 'placement' => 'prepend', 'escape' => false)),
            array(array('widget' => 'HtmlTag'), array('tag' => 'div', 'class' => 'apt-widget', 'openOnly' => true)),
        ));

        return $note;
    }

    protected function getManufacturerScrollElement()
    {
        $note = new Ikantam_Form_Element_Note('manufacturer_slider');

        $note->setRequired(false)
                ->setDescription('<span></span>');

        $note->setDecorators(array(
            'ViewHelper',
            array('Description', array('placement' => 'prepend', 'tag' => 'div', 'class' => 'scroll', 'escape' => false)),
            array(array('scrollable' => 'Htmltag'), array('tag' => 'div', 'closeOnly' => true)),
            array(array('scrollableholder' => 'Htmltag'), array('tag' => 'div', 'closeOnly' => true)),
            array(array('widget' => 'HtmlTag'), array('tag' => 'div', 'closeOnly' => true)),
        ));

        return $note;
    }

    protected function getManufacturerElement()
    {
        $manufacturer = new Zend_Form_Element_MultiCheckbox('brands');

        $collection = new Application_Model_Manufacturer_Collection();
        $values     = $collection->getOptions();

        $manufacturer->setRequired(false)
                ->addFilters(array('StripTags', 'StringTrim', 'Null'))
                ->setAttrib('class', 'aptdeco-styled-checkbox')
                ->setAttrib('label_style', 'font-size: 12px;margin-bottom: 0;')
                ->addValidator('InArray', true, array(array_keys($values)))
                ->setMultioptions($values);

        $manufacturer->setDecorators(array(
            'ViewHelper',
            array(array('addLi' => 'HtmlTag'), array('tag' => 'li')),
            array(array('addUl' => 'HtmlTag'), array('tag' => 'ul')),
            array(array('content' => 'HtmlTag'), array('tag' => 'div', 'class' => 'content categories')),
            array(array('scrollable' => 'Htmltag'), array('tag' => 'div', 'class' => 'scrollable', 'openOnly' => true)),
            array(array('scrollableholder' => 'Htmltag'), array('tag' => 'div', 'class' => 'scrollable-holder', 'openOnly' => true)),
        ));

        $manufacturer->setSeparator('</li><li>');

        return $manufacturer;
    }

    protected function getOnlyAvailableElement()
    {
        $onlyAvailable = new Zend_Form_Element_Hidden('only_available');

        $onlyAvailable->setRequired(false)
                ->addFilters(array('Boolean'));

        $onlyAvailable->setDecorators(array(
            'ViewHelper',
        ));


        return $onlyAvailable;
    }

}