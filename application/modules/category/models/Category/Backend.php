<?php

class Category_Model_Category_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'categories';

    public function getAll($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1 ORDER BY `parent_id` ASC, `sort_order` ASC';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getAllSortedByTitle($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` ORDER BY `title` ASC';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);

            $collection->addItem($object);
        }
    }


    public function getAllUsed(\Application_Model_Abstract_Collection $collection)
    {
        $sql  = "SELECT c.* FROM `" . $this->_getTable() . "` as c INNER JOIN `products` as p ON p.category_id = c.id GROUP BY c.id";
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (is_array($result)) {
            foreach ($result as $row) {
                $item = new Category_Model_Category();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getByUserSaleList($collection, $userId)
    {
        /* $sql = "SELECT `c`.* FROM `".$this->_getTable()."` as `c` 
          INNER JOIN `products` as `p` ON `p`.`category_id` = `c`.`id`
          INNER JOIN `users` as `u` ON `u`.`id` = `p`.`user_id`
          INNER JOIN `orders` as `o` ON `o`.`user_id` != `u`.`id`
          INNER JOIN `order_items` as `oi` ON `oi`.`order_id` = `o`.`id`
          WHERE `u`.`id` = :u_id GROUP BY `c`.`id"; */

        $sql = "SELECT `c`.* FROM `" . $this->_getTable() . "` as `c` 
                INNER JOIN `products` as `p` ON `p`.`category_id` = `c`.`id` 
                INNER JOIN `users` as `u` ON `u`.`id` = `p`.`user_id` 
                
                WHERE `u`.`id` = :u_id GROUP BY `c`.`id`";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':u_id', $userId);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    public function getParents(\Application_Model_Abstract_Collection $collection)
    {
        $sql    = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `parent_id` IS NULL AND `is_active` = 1 ORDER BY `sort_order` ASC';
        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getSubCategories(\Application_Model_Abstract_Collection $collection, $id)
    {
        $sql    = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `parent_id` = :id AND `is_active` = 1 ORDER BY `title` ASC';
        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getAllSubcategories(\Application_Model_Abstract_Collection $collection)
    {
        $sql    = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `parent_id` IS NOT NULL';
        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);
            $collection->addItem($object);
        }
    }
    
    
    protected function _getPartialMatch($query)
    {
        $q = explode(' ', trim($query));

        foreach ($q as $word) {
            $query_like[] = '%' . str_replace('%', '', strtolower(trim($word))) . '%';
        }

        $sql = 'SELECT DISTINCT (`categories`.`id`), `categories`.`title`
            FROM `products`
            JOIN `categories` ON `products`.`category_id` = `categories`.`id`
            JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
            JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
            JOIN `colors` ON `products`.`color_id` = `colors`.`id`
            JOIN `materials` ON `products`.`material_id` = `materials`.`id`
            WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1 AND `categories`.`parent_id` IS NOT NULL';

        $c    = 1;
        $bind = array();

        foreach ($query_like as $word) {
            $sql .= "\n" . 'AND 
            (`products`.`title` LIKE :query' . $c++ . /*'
            OR `products`.`description` LIKE :query' . $c++ .*/ '
            OR `categories`.`title` LIKE :query' . $c++ . '
            OR `conditions`.`short_title` LIKE :query' . $c++ . '
            OR `manufacturers`.`title` LIKE :query' . $c++ . '
            OR `colors`.`title` LIKE :query' . $c++ . '
            OR `materials`.`title` LIKE :query' . $c++ . ')';

            //$bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bind);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllSubcategoriesByProductQuery(Application_Model_Abstract_Collection $collection, $query)
    {
        /*$sql = 'SELECT DISTINCT (`id`), `title` FROM `view_product_categories`
			WHERE (`product_title` LIKE :product_title OR `product_description` LIKE :product_description)
			AND `parent_id` IS NOT NULL';

        $stmt = $this->_getConnection()->prepare($sql);

        $query = '%' . str_replace('%', '', strtolower($query)) . '%';

        $stmt->bindParam(':product_title', $query);
        $stmt->bindParam(':product_description', $query);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);*/
        
        $result = $this->_getPartialMatch($query);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    public function getAllSubcategoriesByCategory(Application_Model_Abstract_Collection $collection, $category)
    {
        $sql = 'SELECT DISTINCT (`id`), `title` FROM `view_product_categories`
			WHERE `parent_id` = :category_id ORDER BY `title` ASC';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':category_id', $category);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    public function getAllSubcategoriesByManufacturer(Application_Model_Abstract_Collection $collection, $category)
    {
        $sql = 'SELECT DISTINCT (`id`), `title` FROM `view_product_categories`
			WHERE `manufacturer_id` = :category_id ORDER BY `title` ASC';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':category_id', $category);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        
    }

    public function getByUser($collection, $userId)
    {
        $sql = 'SELECT * FROM `view_users_parent_categories`
			WHERE `user_id` = :user_id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $userId);

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    public function getFixedAtHome(\Application_Model_Abstract_Collection $collection)
    {
        $sql    = "SELECT `c`.*, `ca`.`alt` FROM `" . $this->_getTable() . "` as `c` INNER JOIN `categories_home` as `ca` ON 
                `ca`.`category_id` = `c`.`id`";
        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    public function getCategoriesForManagePriceFilter(\Application_Model_Abstract_Collection $collection)
    {
        $db     = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from(array('main_table' => $this->getTable()))
            ->join(
                array('p' => 'products'),
                'main_table.id = p.category_id',
                array()
            )
            ->where('p.qty > ?', 0)
            ->where('p.is_approved = ?', 1)
            ->where('p.is_published = ?', 1)
            ->where('p.is_manage_price_allowed = ?', 1)
            ->where('p.admin_edited_price IS NULL OR p.admin_edited_price <> ?', 1)
            ->where('p.available_till >= ?', time())
            ->where('p.available_till <= ?', strtotime('+1 week'))
            ->group('main_table.id')
            ->order('main_table.title', 'ASC');

        $res = $db->fetchAll($select);

        foreach ($res as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);
            $collection->addItem($object);
        }
        return $collection;
    }}
