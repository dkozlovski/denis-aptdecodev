<?php

class Category_Model_Category_Collection extends Application_Model_Abstract_Collection
{

    protected $_backendClass     = 'Category_Model_Category_Backend';
    private $_loaded           = false;
    protected $_homeBackendClass = 'Category_Model_Home_Backend';
    protected $_homeBackend      = null;

    protected function _setIsLoaded($flag = true)
    {
        $this->_loaded = $flag;
    }

    /**
     * Retrieves all used by products categories 
     * @return self
     */
    public function getAllUsed()
    {
        $this->_getBackEnd()->getAllUsed($this);
        return $this;
    }

    /**
     * @return Category_Model_Category_Backend
     */
    protected function _getBackEnd()
    {
        return new $this->_backendClass();
    }

    public function getByUserSaleList($userId)
    {
        $this->_getBackend()->getByUserSaleList($this, $userId);
        return $this;
    }

    public function getJsonConfig()
    {
        $cache   = Zend_Registry::get('core_cache');
        $cacheId = 'categories_json_config';

        if (!($categories = $cache->load($cacheId))) {
            $categories    = array();
            $this->_loaded = false;
            $this->getAll();

            foreach ($this->_items as $category) {
                if (!$category->getIsActive()) {
                    continue;
                }

                if (!$category->getParentId()) {
                    $categories[$category->getId()] = array('title'         => $category->getTitle(), 'subcategories' => array());
                } else {
                    $categories[$category->getParentId()]['subcategories'][$category->getId()] = $category->getTitle();
                }
            }
            $cache->save($categories);
        }

        return $categories;
    }

    public function getJsonConfig2()
    {
        $cache   = Zend_Registry::get('core_cache');
        $cacheId = 'categories_json_config';

        if (!($categories = $cache->load($cacheId))) {
            $categories    = array();
            $this->_loaded = false;
            $this->getAllSortedByTitle();

            foreach ($this->_items as $category) {
                if (!$category->getIsActive()) {
                    continue;
                }
                
                if (!$category->getParentId()) {
                    $categories[$category->getId()]['title']  = $category->getTitle();
                } else {
                    $categories[$category->getParentId()]['subcategories'][$category->getTitle()] = $category->getId() ;
                }
            }

            $cache->save($categories);
        }

        return $categories;
    }

    public function getSubOptions($id)
    {
        $result = array();

        $this->getSubCategories($id);
        $result[0] = '-- Select --';
        foreach ($this->_items as $category) {
            $result[$category->getId()] = $category->getTitle();
        }

        return $result;
    }

    public function getAll()
    {
        if (!$this->_loaded) {
            $this->clear();
            $this->_getBackEnd()->getAll($this);
            $this->_setIsLoaded();
        }
        return $this;
    }

    public function getAllSortedByTitle()
    {
        if (!$this->_loaded) {
            $this->clear();
            $this->_getBackEnd()->getAllSortedByTitle($this);
            $this->_setIsLoaded();
        }
        return $this;
    }


    /**
     * Return only parent categories
     *   
     * @return Category_Model_Category_Collection
     */
    public function getParents()
    {
        $this->clear();
        $this->_getBackEnd()->getParents($this);

        return $this;
    }

    public function getOptions2()
    {
        $result = array();

        foreach ($this->getAll() as $category) {
            $result[$category->getId()] = $category->getTitle();
        }

        return $result;
    }

    public function getOptions3()
    {
        $result = array();

        foreach ($this->getAll() as $category) {
            if (!$category->getParentId() && $category->getIsActive())
                $result[$category->getId()] = $category->getTitle();
        }

        return $result;
    }

    public function getOptions4()
    {
        $result = array();

        foreach ($this->getAll() as $category) {
            if ($category->getParentId())
                $result[$category->getId()] = $category->getTitle();
        }

        return $result;
    }

    public function getOptions()
    {
        $result = array();

        foreach ($this->getItems() as $category) {
            $result[$category->getId()] = $category->getTitle();
        }

        return $result;
    }

    public function getSubCategories($id)
    {
        $this->clear();
        $this->_getBackEnd()->getSubCategories($this, $id);

        return $this;
    }

    /**
     * @param  integer id = category id         * 
     * @return array
     */
    public function getSubCategoriesId($id)
    {
        $this->getSubCategories($id);
        $result = array();
        foreach ($this->getItems() as $category) {
            $result[] = $category->getId();
        }

        return $result;
    }

    public function getAllSubcategories()
    {
        $this->clear();
        $this->_getBackEnd()->getAllSubcategories($this);
        return $this;
    }

    public function getSubcategoriesOptions()
    {
        $this->getAllSubcategories();

        $result = array();

        foreach ($this->getItems() as $category) {
            $result[$category->getId()] = $category->getId();
        }

        return $result;
    }

    public function getAllSubcategoriesByProductQuery($query)
    {
        $this->clear();
        $this->_getBackEnd()->getAllSubcategoriesByProductQuery($this, $query);
        return $this;
    }

    public function getAllSubcategoriesByCategory($category)
    {
        $this->clear();
        $this->_getBackEnd()->getAllSubcategoriesByCategory($this, $category);

        $cats = array();

        foreach ($this as $category) {
            $cats[$category->getId()] = $category->getTitle();
        }

        return $cats;
    }

    public function getAllSubcategoriesByManufacturer($category)
    {
        $this->clear();
        $this->_getBackEnd()->getAllSubcategoriesByManufacturer($this, $category);
        return $this;
    }

    public function getByUser($userId)
    {
        $this->clear();
        $this->_getBackEnd()->getByUser($this, $userId);
        return $this;
    }

    public function getCategoriesFixedAtHomePage()
    {
        $this->clear();
        $this->_getBackEnd()->getFixedAtHome($this);
        return $this;
    }

    public function getCategoriesForManagePriceFilter()
    {
        $this->clear();
        return $this->_getBackEnd()->getCategoriesForManagePriceFilter($this);
    }

    public function getTitles()
    {
        $result = array();
        foreach ($this->getItems() as $item) {
            $result[] = $item->getTitle();
        }
        return $result;
    }

    protected function _getHomeBackend()
    {
        if (is_null($this->_homeBackend)) {
            return $this->_homeBackend = new $this->_homeBackendClass();
        } elseif ($this->_homeBackend instanceof $this->_homeBackendClass) {
            return $this->_homeBackend;
        }
    }

    public function releaseAllFixedCategories()
    {
        $this->_getHomeBackend()->deleteAll();
        return $this;
    }

    /**
     * @param array $ids - optional
     * @return self
     */
    public function fixAtHomepage($ids = null)
    {
        if (is_null($ids))
            $ids = $this->getColumn('id');
        $this->_getHomeBackend()->fixMultiple($ids);
        return $this;
    }

    public function getFlexFilter()
    {
        return new Category_Model_Category_FlexFilter($this);
    }

}
