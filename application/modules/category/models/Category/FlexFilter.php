<?php

/**
 * @method Category_Model_Category_FlexFilter id_of_set (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_id_of_set (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter products_published (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_products_published (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter products_approved (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_products_approved (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter products_visible (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_products_visible (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter google_category_name (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_google_category_name (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter id (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_id (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter parent_id (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_parent_id (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter title (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_title (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter sort_order (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_sort_order (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter page_title (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_page_title (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter page_url (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_page_url (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter is_active (string $operator, mixed $value)
 * @method Category_Model_Category_FlexFilter or_is_active (string $operator, mixed $value)
 * @method Category_Model_Category_Collection apply(int $limit = null, int $offset = null)
 */

class Category_Model_Category_FlexFilter extends Ikantam_Filter_Flex {
    
    protected $_acceptClass = 'Category_Model_Category_Collection';
    
    protected $_joins = array (
        'products' => 'INNER JOIN `products` ON `categories`.`id` = `products`.`category_id`',
        'collections_products' => array('products' => 'INNER JOIN `collections_products` AS `c_p` ON `c_p`.`product_id` = `products`.`id`'),
        'set' => array('collections_products' => 'INNER JOIN `collections` AS `collection` ON `collection`.`id` = `c_p`.`collection_id`'),
        'google_related' => "INNER JOIN `categories_google_merchant_categories` AS `google_relation` ON `google_relation`.`category_id`
            = `categories`.`id`",
        'google_merchant_category' => array('google_related' => 'INNER JOIN `google_merchant_categories` AS `google_category` ON 
            `google_relation`.`google_merchant_category_id` = `google_category`.`id`'),
    );
    
    protected $_select = array (
        'google_category_name' => '`google_category`.`name` AS `google_category_name`',
    );
    
    protected $_rules = array(
        'id_of_set' => array('set' => '`collection`.`id`'),
        'products_published' => array('products' => '`products`.`is_published`'),
        'products_approved' => array('products' => '`products`.`is_approved`'),
        'products_visible' => array('products' => '`products`.`is_visible`'),
        'google_category_name' => array('google_merchant_category' => '`google_category`.`name`')
    );       
}