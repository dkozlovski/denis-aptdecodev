<?php

class Category_Model_Home_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'categories_home';


    
    public function addCategory (\Category_Model_Category $object, $imagePath = null)
    {
        $this->_insert($object->setImagePath($imagePath));
    }
    
    public function getImagePath (\Category_Model_Category $object)
    {
        $sql = "SELECT `image_path` FROM `".$this->_getTable()."` WHERE `category_id` = :cid LIMIT 1";        
        $stmt = $this->_getConnection()->prepare($sql);
        
        $id = $object->getId();
        
        $stmt->bindParam(':cid', $id);
        $stmt->execute();
        
        return $stmt->fetch(PDO::FETCH_COLUMN);
         
    }
    
    public function deleteAll ()
    {
        $sql = "DELETE FROM `".$this->_getTable()."` WHERE 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
    }
    
    	/**
         * Fix multiple categories
         * @param  array $ids
         */
    public function fixMultiple ($ids)
    {       

        $parts = array();
        $binds = array(); 

        foreach($ids as $id)
        {
            $parts[] = '(NULL, :id'.$id.')';
            $binds[':id'.$id]=$id;
        }
     $sql = "INSERT IGNORE INTO `".$this->_getTable()."` (`id`, `category_id`) VALUES ".implode(',', $parts);
     $stmt = $this->_getConnection()->prepare($sql);     

     $stmt->execute($binds);
    }
    
    public function delete (\Category_Model_Category $object)
    {
        $sql = "DELETE FROM `".$this->_getTable()."` WHERE `category_id` = :cid LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $id = $object->getId();
        $stmt->bindParam(':cid', $id);
        $stmt->execute();
    }



    	/**
         * !IMPORTANT!
         * @param  Category_Model_Category $object
         */
	protected function _update(\Application_Model_Abstract $object)
	{
   
	}

    	/**
         * !IMPORTANT!
         * @param  Category_Model_Category $object
         */
	protected function _insert(\Application_Model_Abstract $object)
	{

	    $dbh = $this->_getConnection();
        $sql = "INSERT INTO `".$this->_getTable()."` (`id`, `category_id`, `image_path`) VALUES (NULL, :cid, :img_path)";
        
        $stmt = $dbh->prepare($sql);
        
        $id = $object->getId();
        $imagePath = $object->getImagePath();
                
        $stmt->bindParam(':cid', $id);
        $stmt->bindParam(':img_path', $imagePath);        
        
        $stmt->execute();
        
        $object->setId($dbh->lastInsertId());       
        
	}
    



}
