<?php

class Category_Model_Category extends Application_Model_Abstract
{

    protected $_backendClass     = 'Category_Model_Category_Backend';
    protected $_homeBackendClass = 'Category_Model_Home_Backend';
    protected $_homeBackend      = null;
    protected $_homeImageDir     = 'upload/homepage/';
    protected static $_google_names;

    public function __construct($id = null)
    {
        if ($id) {
            $this->getById($id);
        }
        return $this;
    }

    public function save()
    {
        $cache   = Zend_Registry::get('output_cache');
        $cacheId = 'primary_menu';
        $cache->clean('matchingTag', array($cacheId));

        $cache2   = Zend_Registry::get('core_cache');
        $cacheId2 = 'categories_json_config';
        $cache2->clean('matchingTag', array($cacheId2));

        return parent::save();
    }

    /**
     *
     * @return Category_Model_Category_Collection 
     */
    public function getSubCategories()
    {
        if ($this->getParentId()) {
            return new Category_Model_Category_Collection();
        }
        $categories = new Category_Model_Category_Collection();
        return $categories->getSubCategories($this->getId());
    }

    public function getSubCategoriesAsArray()
    {
        $ret = array();

        if (!$this->getParentId()) {
            $categories = new Category_Model_Category_Collection();

            foreach ($categories->getSubCategories($this->getId()) as $category) {
                $ret[$category->getId()] = $category->getTitle();
            }
        }

        return $ret;
    }

    /**
     * 
     * @return array
     */
    public function getSubCategoriesId()
    {
        if ($this->getParentId()) {
            return array($this->getId());
        }
        $categories = new Category_Model_Category_Collection();
        return $categories->getSubCategoriesId($this->getId());
    }

    /**
     * @return bool
     */
    public function isParent()
    {
        if (!$this->getParentId())
            return true;
        return false;
    }

    /**
     * 
     * @return Application_Model_Manufacturer_Collection (for all subcategories)
     */
    public function getAllManufacturers()
    {
        $subCategories = $this->getSubCategoriesId();
        $manufacturers = new Application_Model_Manufacturer_Collection();

        foreach ($subCategories as $category) {
            $products = new Product_Model_Product_Collection();
            $products->getByCategory($category);
            foreach ($products->getItems() as $product) {
                $manufacturer = new Application_Model_Manufacturer($product->getManufacturerId());
                $manufacturers->addItem($manufacturer);
            }
        }
        return $manufacturers;
    }

    public function getCategoryUrl()
    {
        if ($this->getPageUrl()) {
            return Ikantam_Url::getUrl('catalog/' . $this->getPageUrl());
        }
        return Ikantam_Url::getUrl('category/index/index', array('id' => $this->getId()));
    }

    public function getAllProducts()
    {
        //@TODO: return Product_Model_Product_Collection|+
        $products = new Product_Model_Product_Collection();
        return $products->getByCategory($this->getId());
    }

    protected function _getHomeBackend()
    {
        if (is_null($this->_homeBackend)) {
            return $this->_homeBackend = new $this->_homeBackendClass();
        } elseif ($this->_homeBackend instanceof $this->_homeBackendClass) {
            return $this->_homeBackend;
        }
    }

    public function getParent()
    {
        return new Category_Model_Category($this->getParentId());
    }

    /**
     * Fix category at home page
     * @param  string $imagePath 
     * @return self
     */
    public function fixAtHomepage($imagePath = null)
    {
        $hbe       = $this->_getHomeBackend();
        if (is_null($imagePath))
            $imagePath = $this->getTmpFileName();

        if ($this->getId()) {
            try {
                $hbe->addCategory($this, $imagePath);
            } catch (Exception $e) {
                if ($e->getCode() != 23000)
                    throw $e; //SQLSTATE[23000]: Duplicate entry                                       
            }
        }

        return $this;
    }

    public function removeFromHomePage()
    {
        if ($this->getId()) {
            $this->_getHomeBackend()->delete($this);
        }

        return $this;
    }

    /**
     * Return path to image. 
     * @return mixed - string if there was provided| null if path was not provided| false if category not fixed at homepage
     */
    public function getHomeImageName()
    {
        $path = false;
        if ($this->getId()) {
            $path = $this->_getHomeBackend()->getImagePath($this);
        }

        return $path;
    }

    protected function getHomeImageDirPath()
    {
        return realpath(APPLICATION_PATH . '/../public/' . $this->_homeImageDir . '/');
    }

    public function getHomeImageRealPath()
    {
        return realpath($this->getHomeImageDirPath() . '/' . $this->getHomeImageName());
    }

    public function getHomeImageUrl()
    {
        if ($path = $this->getHomeImageName()) {
            $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
            $path          = $cloudFrontUrl . '/homepage/' . $path;
        }
        return $path;
    }

}