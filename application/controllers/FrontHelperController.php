<?php
/**
 * Author: Alex P.
 * Date: 07.07.14
 * Time: 13:43
 */

class FrontHelperController extends Ikantam_Controller_Front
{

    /**
     * Gives html which is using for modal window
     */
    public function modalHtmlAction()
    {
        if (!$this->isAjax()) {
            $this->_helper->show404();
        }

        $template = $this->getParam('template');

        switch($template) {
            case 'price-reduction-popup':
                $template = 'front-helper/price-reduction-confirmation-popup-template';
                break;

            case 'login':
                $template = 'front-helper/login';
                break;

            case 'confirmation':
                $template = 'front-helper/confirmation';
                break;

            default:
                $template = 'front-helper/modal';
                break;
        }

        $this->_helper->_layout->disableLayout();

        $this->_helper
            ->viewRenderer
            ->setNeverController(true) // do not render into controller subdirectories
            ->setRender($template);       // set render script to modal
    }

    public function isLoggedAction()
    {
        $this->_helper->json(array(
                'is_logged' => $this->getSession()->isLoggedIn(),
            ));
    }

    public function javaScriptErrorAction()
    {
        if (!$this->isAjax()) {
            exit;
        }
        $logger = new Zend_Log(new Zend_Log_Writer_Stream(APPLICATION_PATH . '/log/javascript.log'));

        $message = "\nMESSAGE: " . $this->getParam('message') . "\n";
        $message .= "URL: " . $this->getParam('url') . "\n";
        $message .= "LINE: " . $this->getParam('line') . "\n";
        $message .= "LOCATION: " . $this->getParam('location') . "\n";
        $message .= "USERAGENT: " . $this->getParam('useragent') . "\n";

        $logger->log($message, Zend_Log::ERR);

        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $this->getResponse()->setHttpResponseCode(204);
    }

    /*public function jsConfigAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->getResponse()
            ->setHeader('Content-Type', 'text/javascript');
    }*/

}