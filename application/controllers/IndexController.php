<?php

class IndexController extends Ikantam_Controller_Front
{

    const META_DESCRIPTION = 'New and used furniture online marketplace in New York. Sell used furniture online or purchase quality pre-owned furnishing and get free delivery. Safe and secure online payments, hassle free experience.';

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('layout2');

        $product     = new Product_Model_Product();
        $categories  = new Category_Model_Category_Collection();
        $brands      = new Application_Model_Manufacturer_Collection();
        $wpConnector = new Application_Model_Static();
        $products    = new Product_Model_Product_Collection();

        $blogSlug     = $wpConnector->getFixedAtHomeBlogPost();
        $featuredBlog = $wpConnector->getBlogPosts(null, $blogSlug);

        $this->view->session            = $this->getSession();
        $this->view->background         = Product_Model_HomeBackground::url();
        $this->view->categories         = $categories->getCategoriesFixedAtHomePage();
        $this->view->brands             = $brands->getManufacturersFixedAtHomePage();
        $this->view->product            = $product->getFixedAtHomeProduct();
        $this->view->seller             = $product->getSeller();
        $this->view->featuredBlogPost   = current($featuredBlog);
        $this->view->featuredBlogPostBg = Application_Model_BlogBackground::url();
        $this->view->featuredProducts   = $products->getFeatured();


        $this->view->headTitle()->setSeparator(' | ');
        $this->view->headTitle('AptDeco');

        $settings = new Admin_Model_Settings();
        $this->view->headTitle($settings->getValue('home_page_title', 'home_page_title'));

        $this->view->headMeta()->appendName('description', self::META_DESCRIPTION);

        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/testimonials.js'));

        $items                = new Widget_Model_Homepage_AsSeenIn_Collection();
        $items->getAll();
        $this->view->asSeenIn = $items;

        $slides             = new Widget_Model_Homepage_Slider_Collection();
        $slides->getAll();
        $sl = array();
        foreach ($slides as $slide) { 
            $sl[$slide->getSortOrder()][] = $slide;
        }
        $this->view->slides = $sl;


    }

}
