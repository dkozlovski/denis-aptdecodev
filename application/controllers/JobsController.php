<?php
class JobsController extends Ikantam_Controller_Front {
 
    protected $wp_db_connector ;
    
    public function init ()
    {
        $this->wp_db_connector = new Application_Model_Static();   
    }   
    
    public function indexAction ()
    {		 
		$data_from_wp = $this->wp_db_connector->getJobsPosts();
		$this->view->posts = $data_from_wp;
        $this->view->headTitle('Join the AptDeco team');
        $this->view->headMeta()->appendName('description', "Work at AptDeco - NYC's destination marketplace for buying and selling used furniture.");
    }
    
    public function viewAction ()
    {
       $post = $this->wp_db_connector->getJobsPosts($this->getRequest()->getParam('id'));
              
       if(!isset($post[0])) {
        throw new Zend_Controller_Action_Exception('Page not found', 404);
       }       
       $this->view->headTitle($post[0]['post_title']);
       $this->view->postTitle = $post[0]['post_title'];
       $this->view->post = $post[0]['post_content'];
            
    }
}