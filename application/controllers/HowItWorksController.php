<?php

/**
*    HowitworksController class
*   
*    @author Alex Polevoy
*    @created 31.01.2013 
*/
class HowItWorksController extends Ikantam_Controller_Front
{

	public function indexAction()
	{
	    $wp_db_connector = new Application_Model_Static();
		$data_from_wp = $wp_db_connector->getContent('how-it-works');
		$this->view->content = $data_from_wp['post_content'];
		$this->view->title = $data_from_wp['post_title'];
        $this->view->headTitle('How to buy and sell used furniture');
        $this->view->headMeta()->appendName('description', 'Get more information on how to sell your used furniture and how to buy used furniture in NYC.');
    }

}
