<?php

class GetImageController extends Ikantam_Controller_Front
{

    public function indexAction()
    {
        $key = $this->getRequest()->getParam('key');
        
        if ($key) {
            $viewedEmail = new Application_Model_ViewedEmail();
            $viewedEmail->getByCode($key);
            if ($viewedEmail->getId() && !$viewedEmail->getReadAt()) {
                $viewedEmail->setReadAt(time())->save();
            }
        }
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $image = file_get_contents(APPLICATION_PATH . '/../public/images/new-logo.png');

        $this->getResponse()->clearBody();
        $this->getResponse()->setHeader('Content-Type', 'image/png');
        $this->getResponse()->setBody($image);
    }

}
