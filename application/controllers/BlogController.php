<?php

/**
 *    AboutController class
 *    
 *    @author Alex Polevoy
 *    @created 31.01.2013 
 */
class BlogController extends Ikantam_Controller_Front
{

    protected $_postsPerPage = 4;

    public function indexAction()
    {
        $page            = (int) $this->getRequest()->getParam('page');
        $categorySlug    = $this->getParam('slug');
        $wp_db_connector = new Application_Model_Static();
        $categoryInfo    = $wp_db_connector->getCategoryInfo($categorySlug);

        if (!$categoryInfo) {
            throw new Zend_Controller_Action_Exception('Page not found', 404);
        }

        $totalPosts = (int) $categoryInfo['count'];
        $totalPages = (int) ceil($totalPosts / $this->_postsPerPage);

        if ($page > $totalPages) {
            $page = $totalPages;
        }

        if ($page < 1) {
            $page = 1;
        }

        $offset = ($page - 1) * $this->_postsPerPage;

        $data_from_wp = $wp_db_connector->getBlogPosts($categoryInfo['slug'], null, $offset, $this->_postsPerPage);

        $this->view->posts      = $data_from_wp;
        $this->view->page       = $page;
        $this->view->totalPages = $totalPages;
        $this->view->categories = $wp_db_connector->getCategories();
        $this->view->slug       = $categoryInfo['slug'];

        $this->view->headTitle('Home Interior Design Ideas');
        $this->view->headMeta()->appendName('description', 'Fresh interior design ideas and furniture pairings on the AptDeco blog. Subscribe for updates.');

        if ($categoryInfo['term_id'] != Zend_Registry::get('config')->blog->defaultCategory) {
            $this->view->headTitle($categoryInfo['name']);
        }
    }

    public function viewAction()
    {
        $path = $this->getRequest()->getParam('path');

        $wp_db_connector   = new Application_Model_Static();
        $data_from_wp      = $wp_db_connector->getBlogPosts(null, $path);
        $this->view->posts = $data_from_wp;

        if (!empty($data_from_wp[0]) && !empty($data_from_wp[0]['post_title'])) {
            $this->view->headTitle($data_from_wp[0]['post_title']);
        }

        $this->view->categories = $wp_db_connector->getCategories();

        if (!empty($data_from_wp[0]) && !empty($data_from_wp[0]['post_title'])) {
            $this->view->headMeta()->appendName('og:title', 'AptDeco | Blog | ' . htmlentities(strip_tags($data_from_wp[0]['post_title'])));
        }

        if (!empty($data_from_wp[0]) && !empty($data_from_wp[0]['post_content'])) {
            $this->view->headMeta()->appendName('og:description', htmlentities(strip_tags($data_from_wp[0]['post_content'])));
        }
    }

    public function pageAction()
    {
        $postName = $this->getRequest()->getParam('post_name');
        $postName = substr($postName, 0, strripos($postName, '.html'));

        $wpDbConnector   = new Application_Model_Static();
        $post = $wpDbConnector->getPage($postName);

        if ($post) {
            $this->view->post = $post;
            $this->view->headTitle($post['post_title']);
            $this->view->headMeta()->appendName('og:title', 'AptDeco | Blog | ' . htmlentities(strip_tags($post['post_title'])));
            $this->view->headMeta()->appendName('og:description', htmlentities(strip_tags($post['post_content'])));

            $this->view->categories = $wpDbConnector->getCategories();

        } else {
            throw new Zend_Controller_Action_Exception('', 404);
        }
    }

}