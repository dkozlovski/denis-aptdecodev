<?php

class PickupDeliveryController extends Ikantam_Controller_Front
{

	public function indexAction()
	{
		$wp_db_connector = new Application_Model_Static();
		$data_from_wp = $wp_db_connector->getContent('pick-up-delivery');
		$this->view->content = $data_from_wp['post_content'];
		$this->view->title = $data_from_wp['post_title'];
	}

}
