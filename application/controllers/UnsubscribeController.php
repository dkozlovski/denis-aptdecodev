<?php
/**
 * Author: Alex P.
 * Date: 19.06.14
 * Time: 14:06
 */

class UnsubscribeController extends Ikantam_Controller_Front
{
    /**
     * Saves user decision
     */
    public function notificationsAction()
    {
        $unsubscribe = $this->getUnsubscribeModel($this->getParam('code'));
        if ($unsubscribe->isExists() || $unsubscribe->getIsUserConfirm() == 1) {
            $unsubscribe->setIsUserConfirm(1)->save();
            $this->getSession()->setFlashData('just_unsubscribed', true);
            $this->redirect('/');
        }
        $this->_helper->show404();
    }

    /**
     * Retrieves model by code
     * @param $code
     * @return Notification_Model_Unsubscribe
     */
    protected function getUnsubscribeModel($code)
    {
        $unsubscribe = new \Notification_Model_Unsubscribe;
        $unsubscribe->getBy_code($code);
        return $unsubscribe;
    }
} 