<?php

class ErrorController extends Ikantam_Controller_Front
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority            = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
               $this->_helper->_layout->setLayout('layout3');
                $this->renderScript('error/error_404.phtml');
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority            = Zend_Log::CRIT;
                $this->view->message = 'Application error';
                break;
        }

        // Log exception, if logger available
        if ($log = $this->getLog()) {
            //$log->log($this->view->message, $priority, $errors->exception);
            //$log->log('Request Parameters', $priority, $errors->request->getParams());
            $log->log($this->prepareException($this->view->message, $errors->exception), $priority);
            $log->log($this->prepareRequest($errors->request), $priority);
            $log->log($this->prepareUserInfo(), $priority);
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request = $errors->request;
    }



    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');

        return $log;
    }

    protected function prepareException($message, $exception)
    {
        return $message . "\n" . $exception->getMessage() . "\n" . $exception->getTraceAsString() . "\n";
    }

    protected function prepareRequest($request)
    {
        $str = "Request Parameters:\n";
        foreach ($request->getParams() as $param => $value) {
            $str .= $param . ': ' . $value . "\n";
        }
        return $str;
    }

    protected function prepareUserInfo()
    {
        $str = "User info:\n";
        $str .= "IP: " . $this->getRequest()->getClientIp() . "\n";
        $str .= "User agent: " . $_SERVER['HTTP_USER_AGENT'] . "\n\n";
        $str .= '*************************************************************' . "\n";
        return $str;
    }

}
