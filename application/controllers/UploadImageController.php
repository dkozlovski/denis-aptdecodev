<?php

class UploadImageController extends Ikantam_Controller_Front
{

    protected $_uploadDir;
    protected $_avatarUploadDir;

    public function init()
    {
        $this->_avatarUploadDir = APPLICATION_PATH . '/../public/upload/avatars';
        $this->_uploadDir       = APPLICATION_PATH . '/../product-images';

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function indexAction()
    {
        $response = array();

        $productId = $this->getRequest()->getParam('product_id');
        $product   = new Product_Model_Product($productId);

        if (!$product->getId() || $product->getUserId() != $this->getSession()->getUserId()) {
            //@TODO:return error message
            die();
        }
        //////
        $basePath = Product_Model_Image::getBaseTmpDir();
        $path     = Product_Model_Image::getTmpUploadDir($product);


        $uploader = new Ikantam_File_Uploader();

        $uploader->setAdapter(new Zend_File_Transfer_Adapter_Http)
                ->setUploadPath($basePath . DIRECTORY_SEPARATOR . $path);

        if (!$uploader->upload()) {
            $errors = $uploader->getErrorMessages();

            $response['success'] = false;
            $response['error'] =  $response['message'] = implode("\n", $errors);
            $this->getResponse()
                    ->setHeader('Content-type', 'text/plain')
                    ->setBody(json_encode($response));
        } else {
            $fileName = $uploader->getFileName();

            $iConverter = new Ikantam_File_ImageConverter();

            $newFileName = $basePath . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . 'original-0.jpg';
            $iConverter->convert($fileName, $newFileName, 90);

            $res = $iConverter->createScaledImages($newFileName, $basePath . DIRECTORY_SEPARATOR . $path);

            if ($res) {
                $fileNames   = $iConverter->getFileName();
                $fileNames[] = $newFileName;
            } else {
                //@TODO: return error;
            }

            foreach ($fileNames as $file) {
                $e = Product_Model_Image::getTmpS3Path($file);
                $uploader->uploadToS3($file, $e);
                unlink($file);
            }

            unlink($fileName);

            $productImage = new Product_Model_Image();

            $productImage->setProductId($product->getId())
                    ->setPath('original-0.jpg')
                    ->setS3Path($path)
                    ->setIsVisible(0)
                    ->setIsMain(0)
                    ->setIsLocked(0)
                    ->setIsUploaded(0)
                    ->save();


            $response['success']     = true;
            $response['file']['id']  = $productImage->getId();
            //$response['file']['name'] = $path . '/original-0.jpg';
            $response['file']['url'] = $productImage->getS3Url(500, 500, 'crop');
        }



        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(json_encode($response));
    }

    public function indexOldAction()
    {
        /* if (!$this->getSession()->isLoggedIn()) {
          //@TODO:return error message
          die();
          } */

        $contentLength = $this->getRequest()->getHeader('Content-Length');

        if (($contentLength / 1024 / 1024) > 14) {
            exit('Sorry. File size is too big. Only accepts files less than 14MB size.');
        }

        $productId = $this->getRequest()->getParam('product_id');
        $product   = new Product_Model_Product($productId);

        if (!$product->getId() || $product->getUserId() != $this->getSession()->getUserId()) {
            //@TODO:return error message
            die();
        }

        $img       = new Ikantam_Image();
        $imagePath = $img->uploadImage($this->_uploadDir, $product->getId());
        $img->createScaledImages($this->_uploadDir, $imagePath, $product->getId());

        $productImage = new Product_Model_Image();

        $productImage->setProductId($product->getId())
                ->setPath('original-0.jpg')
                ->setS3Path($imagePath)
                ->setIsVisible(0)
                ->setIsMain(0)
                ->setIsLocked(0)
                ->setIsUploaded(0)
                ->save();

        $response                 = array();
        $response['success']      = true;
        $response['file']['id']   = $productImage->getId();
        $response['file']['name'] = $imagePath . '/original-0.jpg';
        $response['file']['url']  = $productImage->getS3Url(500, 500, 'crop');

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(json_encode($response));
    }

    public function avatarAction()
    {
        $user = $this->getSession()->getUser();

        try {
            $img       = new Ikantam_Image();
            $imagePath = $img->uploadAvatar($this->_avatarUploadDir, $user->getId());

            if ($imagePath) {
                $file       = new stdClass();
                $file->name = $this->_avatarUploadDir . DIRECTORY_SEPARATOR . $imagePath;

                $avatar = $user->getAvatar(true);

                $avatar->setTitle($file->name)
                        ->setUserId($user->getId())
                        ->setIsAvailable(false)
                        ->setS3Path($imagePath)
                        ->save();

                $file->src = $avatar->getUrl();

                $srcImg    = @imagecreatefromjpeg($avatar->getUrl());
                $imgWidth  = imagesx($srcImg);
                $imgHeight = imagesy($srcImg);

                $file->width  = $imgWidth;
                $file->height = $imgHeight;
            }
            $this->getResponse()
                    ->setHeader('Content-type', 'text/plain')
                    ->setBody(Zend_Json::encode($file));
        } catch (Exception $e) {
            $response['success'] = false;
            $response['error']   = true;
            $response['err_msg'] = $e->getMessage();

            $this->getResponse()
                    ->setHeader('Content-type', 'text/plain')
                    ->setBody(Zend_Json::encode($response));
        }
    }

    public function cropAction()
    {
        $params = $this->getRequest()->getParam('cropParams', false);
        if (!$params)
            return;

        $user = $this->getSession()->getUser();

        $cropper = new Ikantam_Crop(array(
            'src_dir' => $this->_avatarUploadDir,
            'square' => true, //Width and height must be equal.
            'save_original' => true, //original image will be delete
            'output_format' => IMG_JPG,
            'quality' => 100,
            'interlace' => false, //will be saved like progressive JPEG
            'prefix' => $user->getId() . '_avtr',
            'random_name' => true,
            'create_thumbnail' => false,
            'thumbnail' => array('width' => 200, 'height' => 200, 'dir' => $this->_avatarUploadDir . '/thumbnail'),
            'width' => $params['w'], //selected area width
            'height' => $params['h'], //selected area height
        ));

        $cropper->setImage(@imagecreatefromjpeg($user->getAvatar(true)->getUrl()));

        $response = (object) array('error' => false);


        if (!$cropper->getErrorMessages()) {



            $file_info = $cropper->getInfo();

            $receivedHeight = $params['Height'];
            $originalHeight = $file_info[1];

            $R = $originalHeight / $receivedHeight;

            $x = round($params['x'] * $R);
            $y = round($params['y'] * $R);
            $h = round($params['h'] * $R);
            $w = round($params['w'] * $R);

            $cropper->crop($x, $y, $w, $h);

            $img = new Ikantam_Image();


            $avatar = $user->getAvatar(true);

            $newPath = str_replace(basename($avatar->getS3Path()), '', $avatar->getS3Path())
                    . $img->_getRandomDirName() . '.jpg';


            $avatar->setTitle($cropper->getName())
                    ->setIsAvailable(true)
                    ->setUserId($user->getId())
                    ->setS3Path($newPath)
                    ->save();

            $response->src = $user->getAvatarUrl();


            $img->uploadAvatarToS3($this->_avatarUploadDir . '/' . $cropper->getName(), $newPath);
            unlink($this->_avatarUploadDir . '/' . $cropper->getName());
        } else {
            $errors            = $cropper->getErrorMessages();
            $err               = array_shift($errors);
            $response->error   = true;
            $response->message = $err['message'];
        }

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(Zend_Json::encode($response));
    }

}
