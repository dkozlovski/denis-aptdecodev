<?php

class SearchController extends Ikantam_Controller_Front
{

    protected $_itemsCountPerPage = 12;

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile(Ikantam_Url::getPublicUrl('js/jquery/jquery.autocomplete.js'));
        $itemsPerPage = $this->_itemsCountPerPage;
        $page         = 1;
        $data         = $this->getRequest()->getParams();

        $form = new Product_Form_Filter();

        $this->view->appendHeader = false;

        if (true || $form->isValid($data)) {

            $offset = ($page - 1) * $itemsPerPage;

            $data = $form->getValues();

            if (empty($data['min_price'])) {
                unset($data['min_price']);
            }
            if (empty($data['max_price'])) {
                unset($data['max_price']);
            }
            if (empty($data['colors']) || !is_array($data['colors']) || !count($data['colors'])) {
                unset($data['colors']);
            }
            if (empty($data['brands']) || !is_array($data['brands']) || !count($data['brands'])) {
                unset($data['brands']);
            }
            if (empty($data['categories']) || !is_array($data['categories']) || !count($data['categories'])) {
                unset($data['categories']);
            }
            if (empty($data['conditions']) || !is_array($data['conditions']) || !count($data['conditions'])) {
                unset($data['conditions']);
            }

            //$data['is_approved']  = 1;
            //$data['is_published'] = 1;
            //$data['is_visible']   = 1;
            //$data['user_id'] = $this->getSession()->getUserId();
            $query = $this->getRequest()->getParam('query');

            $product_object = new Product_Model_Product_Collection();
            $products       = $product_object->searchProductPagesHeader($query, $offset, $this->_itemsCountPerPage, $data);


            /* $products_count = $products->count();
              if (empty($products_count)) {
              $array_query = explode(' ', $query);
              $products    = $product_object->searchProductPagesHeader($array_query, $this->_itemsCountPerPage);
              } */

            $this->view->products = $products;

            if ($products->getSize() == 0) {
                $this->view->appendHeader = true;
            }

            $this->view->query = $query;


            $categories             = new Category_Model_Category_Collection();
            $this->view->categories = $categories->getAllSubcategoriesByProductQuery($query);

            $this->view->colors = new Application_Model_Color_Collection();
            $this->view->colors->getByProductQuery($query);

            $this->view->brands = new Application_Model_Manufacturer_Collection();
            $this->view->brands->getByProductQuery($query);

            $productsCollection = new Product_Model_Product_Collection();

            $this->view->minPrice = (int) floor($productsCollection->getMinPriceByQuery($query) / 10) * 10;
            $this->view->maxPrice = (int) ceil($productsCollection->getMaxPriceByQuery($query) / 10 * 10);

            $this->view->selectedMinPrice = (int) $this->getRequest()->getParam('min_price', $this->view->minPrice);
            $this->view->selectedMaxPrice = (int) $this->getRequest()->getParam('max_price', $this->view->maxPrice);

            $this->view->selectedConditions = (array) $this->getRequest()->getParam('condition');
            $this->view->selectedColors     = (array) $this->getRequest()->getParam('color');
            $this->view->selectedBrands     = (array) $this->getRequest()->getParam('brand');
            $this->view->selectedStyles     = (array) $this->getRequest()->getParam('style');
        } else {
            
        }
    }

    public function ajaxAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $response = array(
            'success'  => false,
            'error'    => 'Cannot add item to cart',
            'message'  => '',
            'subtotal' => ''
        );

        $this->view->appendHeader = false;

        $itemsPerPage = $this->_itemsCountPerPage;

        $page = (int) $this->getRequest()->getParam('page');
        $data = $this->getRequest()->getParams();

        $form = new Product_Form_Filter();

        if ($form->isValid($data)) {

            //$products = new Product_Model_Product_Collection();
            //$offset = ($page - 1) * $itemsPerPage;
            //$products->searchAndFilter($form->getValues(), $offset, $itemsPerPage);

            $offset = ($page - 1) * $itemsPerPage;

            $data = $form->getValues();

            if (empty($data['min_price'])) {
                unset($data['min_price']);
            }
            if (empty($data['max_price'])) {
                unset($data['max_price']);
            }
            if (empty($data['colors']) || !is_array($data['colors']) || !count($data['colors'])) {
                unset($data['colors']);
            }
            if (empty($data['brands']) || !is_array($data['brands']) || !count($data['brands'])) {
                unset($data['brands']);
            }
            if (empty($data['categories']) || !is_array($data['categories']) || !count($data['categories'])) {
                unset($data['categories']);
            }
            if (empty($data['conditions']) || !is_array($data['conditions']) || !count($data['conditions'])) {
                unset($data['conditions']);
            }
            // $data['is_approved'] = 1;
            //$data['user_id'] = $this->getSession()->getUserId();

            /* $c = new Product_Model_Product2_Collection();

              $c->setFilters($data)
              ->setOffset($offset)
              ->setLimit($itemsPerPage)
              ->setOrder(array('is_product_sold' => 'asc')); */
            
            $order = array();

            if ($this->_getParam('order_price')) {
                $order = array(
                    'field'     => 'price',
                    'direction' => ($this->_getParam('order_price') == 'asc') ? 'asc' : 'desc'
                );
            } elseif ($this->_getParam('order_rating')) {
                $order = array(
                    'field'     => 'rating',
                    'direction' => ($this->_getParam('order_rating') == 'asc') ? 'asc' : 'desc'
                );
            } elseif ($this->_getParam('order_date')) {
                $order = array(
                    'field'     => 'created_at',
                    'direction' => ($this->_getParam('order_date') == 'asc') ? 'asc' : 'desc'
                );
            }

            $query = $this->getRequest()->getParam('query');

            $product_object = new Product_Model_Product_Collection();
            $products       = $product_object->searchProductPagesHeader($query, $offset, $this->_itemsCountPerPage, $data, $order);


            /* $orderPrice  = $this->getRequest()->getParam('order_price');
              $orderRating = $this->getRequest()->getParam('order_rating');

              if ($orderPrice == 'asc' || $orderPrice == 'desc') {
              $c->addOrder('price', $orderPrice);
              } elseif ($orderRating == 'asc' || $orderRating == 'desc') {
              $c->addOrder('seller_rating', $orderRating);
              } else {
              $c->addOrder('created_at', 'desc');
              }


              $c->load();
              $products = $c; */

            // $params = $this->getRequest()->getParam('query');
            // $page = $this->getRequest()->getParam('page');
            // $products->searchProducts($params, $page);

            $this->view->products = $products;

            if ($products->getSize() == 0 && $page == 1) {
                $this->view->appendHeader = true;
            }

            $this->view->query = $query;


            $categories             = new Category_Model_Category_Collection();
            $this->view->categories = $categories->getAllSubcategoriesByProductQuery($query);

            $this->view->colors = new Application_Model_Color_Collection();
            $this->view->colors->getByProductQuery($query);
            //$this->view->colors->getAll();

            $this->view->brands = new Application_Model_Manufacturer_Collection();
            $this->view->brands->getByProductQuery($query);
            //$this->view->brands->getAll();


            $productsCollection = new Product_Model_Product_Collection();

            $this->view->minPrice = $productsCollection->getMinPriceByQuery($query);
            $this->view->maxPrice = $productsCollection->getMaxPriceByQuery($query);

            $response['template'] = $this->view->render('search/search_items.phtml');
            $response['success']  = true;
        } else {
            
        }

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(json_encode($response));
    }

    public function autocompleteAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $response = array(
            'success'  => false,
            'template' => ''
        );

        $query = $this->getRequest()->getParam('query');

        if (!empty($query)) {

            $products = new Product_Model_Product_Collection();

            $params = array(
                'query' => $query,
                    //'user_id' => $this->getSession()->getUserId()
            );

            $products->searchAndFilter($params, 0, 5);

            $this->view->products = $products;

            $response['template'] = $this->view->render('search/autocomplete_items.phtml');
            $response['success']  = true;
        }

        $this->getResponse()
                ->setHeader('Content-type', 'text/plain')
                ->setBody(json_encode($response));
    }

}
