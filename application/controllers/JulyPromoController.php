<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 24.07.14
 * Time: 0:10
 */

class JulyPromoController extends Ikantam_Controller_Front
{

    public function indexAction()
    {
        $this->_helper
            ->viewRenderer
            ->setRender('wp_page', null, true);  // set render script to modal

        $wp_db_connector = new Application_Model_Static();
        $data_from_wp = $wp_db_connector->getContent('july-promo');
        $this->view->content = $data_from_wp['post_content'];
        $this->view->title = $data_from_wp['post_title'];
    }
} 