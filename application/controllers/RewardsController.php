<?php

class RewardsController extends Ikantam_Controller_Front
{

    protected $_apiKey = 'e013dde301ca0e980897c1f620f92c71';
    
    protected $_apiKey2 = 'tricOltThIYZZKvM8N2x';
    
    protected $_apiUrl = 'https://www.curebit.com/api/v1/sites/:site_slug/customers/:customer_email/stats.json?api_key=:api_key';
    
    protected $_siteSlug = 'aptdeco-3c47343a-ba21-4494-a85e-4931b0961070';

    public function getApiKey()
    {
        return $this->_apiKey;
    }

    public function init()
    {
        
    }

    public function indexAction()
    {
        $search = array(':site_slug', ':customer_email', ':api_key');
        $replace = array($this->_siteSlug, 'v.o.v.a@tut.by', $this->_apiKey2);
        
        $url = str_replace($search, $replace, $this->_apiUrl);

      //  var_dump(file_get_contents($url));

        /*
          if (!$this->getSession()->isLoggedIn()) {
          $this->_helper->redirector('login', 'user', 'default');
          }
         */
/*
        $this->view->session = $this->getSession();
        $this->view->layout()->disableLayout();
        $categories          = new Category_Model_Category_Collection();
        $brands              = new Application_Model_Manufacturer_Collection();
        $product             = new Product_Model_Product();

        $this->view->background = Product_Model_HomeBackground::url();
        $this->view->categories = $categories->getCategoriesFixedAtHomePage();
        $this->view->brands     = $brands->getManufacturersFixedAtHomePage();
        $this->view->product    = $product->getFixedAtHomeProduct();
        $this->view->seller     = $product->getSeller();*/
    }

    public function createAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $request = $this->getRequest();

        if ($request->getPost('key') !== $this->getApiKey() || !$request->getPost('payload')) {
            return;
        }

        $payload = Zend_Json_Decoder::decode($request->getPost('payload'));

        if (isset($payload['referrer'])) {
            $referrer = $payload['referrer'];

            if (isset($referrer['id'], $referrer['email'], $referrer['amount'], $referrer['incentive'])) {
                $user = new User_Model_User($referrer['id']);

                if ($user->getId() && $user->getEmail() == $referrer['email']) {
                    if ($referrer['incentive'] == 'rebate') {
                        $user->addReward($referrer['amount']);
                    }
                }
            }
        }

        if (isset($payload['referred'])) {
            $referred = $payload['referred'];

            if (isset($referred['id'], $referred['email'], $referred['amount'], $referred['incentive'])) {
                $user = new User_Model_User($referred['id']);

                if ($user->getId() && $user->getEmail() == $referred['email']) {
                    if ($referred['incentive'] == 'rebate') {
                        $user->addReward($referred['amount']);
                    }
                }
            }
        }
    }

}