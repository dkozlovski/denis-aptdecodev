<?php

class RegistrationController extends Ikantam_Controller_Front
{

    public function init()
    {
        if ($this->getSession()->isLoggedIn()) {
            $this->redirect('index');
        }
    }

    public function indexAction()
    {
        $isPublishRedirect = $this->getSession()->activeData('publishProductRedirect', null, true);
        $mode              = ($isPublishRedirect === 'Save') ? 'saveValue' : 'publish';

        $this->view->session = $this->getSession();
        //$cfg                 = $this->getXmlConfig('additional_settings', 'facebook');
        $cfg                 = Zend_Registry::get('config')->facebook;
        $this->view->fbUrl   = "http://www.facebook.com/dialog/oauth/?client_id={$cfg->appId}&redirect_uri=" .
                urlencode(Ikantam_Url::getUrl('user/login/facebook', array('publish_product' => 1, 'publish_mode' => $mode))) . "&state=&scope=email,user_birthday";

        $this->view->headTitle('Sign up for an account');
        $this->view->headMeta()->appendName('description', 'Create an account on AptDeco for free and start buying or selling used furniture online.');
    }

    public function postAction()
    {
        $registrationForm         = new User_Form_Registration();
        $data                     = $this->getRequest()->getPost();
        $isCheckoutSignUp         = $this->getRequest()->getParam('checkout_sign_up', false);
        $isPublishProductRedirect = $this->getSession()->activeData('publishProductRedirect', null, true);

        if ($registrationForm->isValid($data)) {
            $user = new User_Model_User();

            $result = $user->registerUser($registrationForm->getValues());

            if ($result === true) {
                $this->getSession()->authenticate($registrationForm->getValues());
                $this->_sendEmail($user);
                if ($isCheckoutSignUp) {
                    $this->redirect('checkout/index');
                } elseif ($isPublishProductRedirect) {
                    $eventDispatcher = new Application_Model_Event_Dispatcher();
                    $eventDispatcher->trigger('assign.unassigned.created.product.after.user.authorize', $this, array());

                    if ($isPublishProductRedirect === 'Save') {
                        $this->_helper->redirector('list', 'sale', 'user');
                    } elseif ($isPublishProductRedirect === true) {
                        $this->getSession()->addMessage('success', $this->getSession()->activeData('_success_product_publish_message'));
                        $this->_helper->redirector('success', 'add', 'product');
                    }
                } else {
                    $this->redirect('');
                }
            } else {
                $this->getSession()->addMessage('error-signup', $result);
                $redirectUrl = Ikantam_Url::getUrl($this->getFailureRedirect());
                $this->_redirect($redirectUrl);
            }
        }
        $this->getSession()->addFormData($data)
                ->addFormErrors($registrationForm->getFlatMessages())
                ->addMessage('page', 'registr');

        if ($isCheckoutSignUp) {
            $this->redirect('checkout/index');
        } else {
            $redirectUrl = Ikantam_Url::getUrl($this->getFailureRedirect());
            $this->_redirect($redirectUrl);
        }
    }

    protected function getFailureRedirect()
    {
        $url = $this->getRequest()->getParam('redirect-failure');
        if (!empty($url)) {
            return urldecode($url);
        }
        return 'registration/index';
    }

    protected function _sendEmail($user)
    {
        if (!$user->getMainEmail()) {
            return;
        }

        $this->view = new Zend_View();
        $this->view->setBasePath(APPLICATION_PATH . '/views/');

        $this->view->user = $user;

        $output = $this->view->render('email_notification_sign_up.phtml');

        $toSend = array(
            'email'   => $user->getMainEmail(),
            'subject' => 'Welcome to AptDeco!',
            'body'    => $output);

        $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');

        try {
            //$mail->sendCopy($output);
            $mail->send(null, true);
        } catch (Exception $e) {
            
        }
    }

    //When guest try to publish product he will be redirected here.  
    public function lastStepAction()
    {
        $messages          = $this->getSession()->getMessages('page');
        $this->view->isReg = (isset($messages[0]) && $messages[0] == 'registr') ? true : false;

        //$cfg                 = $this->getXmlConfig('additional_settings', 'facebook');
        $cfg                 = Zend_Registry::get('config')->facebook;
        $this->view->session = $this->getSession();
        $isPublishRedirect   = $this->getSession()->activeData('publishProductRedirect', null, true);
        //$this->getSession()->setFlashData('publishProductRedirect', $isPublishRedirect);
        $mode                = ($isPublishRedirect === 'Save') ? 'saveValue' : 'publish';

        $this->view->fbUrl = "http://www.facebook.com/dialog/oauth/?client_id={$cfg->appId}&redirect_uri=" .
                urlencode(Ikantam_Url::getUrl('user/login/facebook', array('publish_product' => 1, 'publish_mode' => $mode))) . "&state=&scope=email,user_birthday";
    }

}