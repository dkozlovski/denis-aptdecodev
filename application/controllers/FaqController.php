<?php

/**
*    FaqController class
*    
*    @author Alex Polevoy
*    @created 31.01.2013 
*/
class FaqController extends Ikantam_Controller_Front
{
    protected $markers = array(
        'What are the fees for selling on AptDeco?' => array(
            'title' => 'fees-title',
            'body' => 'fees-body'
        ),
        'How can I make sure my furniture fits before buying?' => array(
            'title' => 'fit-title',
            'body' => 'fit-body'
        ),
        'Can I pick up an item vs. having it delivered?' => array(
            'title' => 'pickup-title',
            'body' => 'pickup-body'
        )
    );

    protected $allowed = array('fees', 'fit', 'fits', 'pickup');


	public function indexAction()
	{
	    $wp_db_connector = new Application_Model_Static();
		$data_from_wp = $wp_db_connector->getFaqPosts();
		$this->view->questions = $data_from_wp;
        $this->view->headTitle('FAQ');
        $this->view->query = $this->getQuery();
        $this->view->markers = $this->markers;
    }

    protected function getQuery()
    {
        $query = $this->view->escape($this->getParam('q', false));
        return in_array($query, $this->allowed) ? $query : false;
    }

}
