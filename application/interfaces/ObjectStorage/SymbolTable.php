<?php
/**
 * Author: Alex P.
 * Date: 16.06.14
 * Time: 18:30
 */

interface Application_Interface_ObjectStorage_SymbolTable
{
    /**
     * Sets name that will be used for current symbol table
     * @param string $name
     * @return mixed
     */
    public function setSymbolTableName($name);

    /**
     * Retrieve name to current symbol table
     * @return mixed
     */
    public function getSymbolTableName();
} 