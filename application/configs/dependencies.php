<?php
/**
 * Author: Alex P.
 * Date: 04.08.14
 * Time: 19:25
 */

return array(

    'deliveryDateFilter' => \DI\object('Admin_Model_DeliveryDate_FlexFilter')
            ->scope(\DI\Scope::PROTOTYPE())
            ->constructor(\DI\link('Admin_Model_DeliveryDate_Collection')),

    'core.service.deliveryDateHelper' => \DI\object('\Core\Service\DeliveryDateHelper')
            ->constructor(\DI\link('deliveryDateFilter')),

    'core.service.JSConfig' => \DI\object('\Core\Service\JSConfig')
            ->constructor(\DI\link('\Zend_Json')),

    'facebook.sdk' => \DI\factory(function($c){
            return new \Facebook($c->get('facebook.keys'));
        }),

    'ikantam.ZendFormToJqueryValidation' => \DI\object('\Ikantam\ZendFormToJqueryValidation\Form')
            ->constructor(\DI\link('ikantam.ZendFormToJqueryValidation.formElement')),

    'ikantam.ZendFormToJqueryValidation.formElement' => \DI\object('\Ikantam\ZendFormToJqueryValidation\FormElement')
            ->constructor(\DI\link('ikantam.ZendFormToJqueryValidation.validatorsMap')),

    'ikantam.ZendFormToJqueryValidation.validatorsMap' => \DI\object(
        '\Ikantam\ZendFormToJqueryValidation\ValidatorsMap'
    ),

    'categoryFilter' => \DI\object('Category_Model_Category_FlexFilter')
            ->scope(\DI\Scope::PROTOTYPE())
            ->constructor(\DI\link('Category_Model_Category_Collection')),

    'productFilter' => \DI\object('Product_Model_Product_FlexFilter')
            ->scope(\DI\Scope::PROTOTYPE())
            ->constructor(\DI\link('Product_Model_Product_Collection')),

    'core.GoogleShoppingContent.ProductConverter' => \DI\object('\Core\GoogleShoppingContent\ProductConverter')
            ->constructor(
                \DI\link('google.shopping.content.categories.map'),
                \DI\link('core.GoogleShoppingContent.ValidateConvertable')
            ),

    'core.GoogleShoppingContent.ValidateConvertable' => \DI\object('\Core\GoogleShoppingContent\ValidateConvertable')
            ->constructor(\DI\link('google.shopping.content.categories.map')),

    'core.GoogleShoppingContent.TokenStorage' => \DI\object('\Core\GoogleShoppingContent\TokenStorage'),


    'google.service.shoppingContent' => \DI\factory(function($c){
            $googleConfig = $c->get('google.shopping.content');
            $tokenStorage = $c->get('core.GoogleShoppingContent.TokenStorage');
            $client = new \Google_Client();
            $client->setApplicationName($googleConfig['applicationName']);
            $service = new \Google_Service_ShoppingContent($client);

            if ($serviceToken = $tokenStorage->get()) {
                $client->setAccessToken($serviceToken);
            }

            $key = file_get_contents($googleConfig['keyFileLocation']);

            $cred = new Google_Auth_AssertionCredentials(
                $googleConfig['serviceAccountName'],
                array('https://www.googleapis.com/auth/content'),
                $key
            );

            $client->setAssertionCredentials($cred);
            if($client->getAuth()->isAccessTokenExpired()) {
                $client->getAuth()->refreshTokenWithAssertion($cred);
            }

            $tokenStorage->set($client->getAccessToken());

            return $service;

        }),

    'core.service.GoogleShoppingContent' => \DI\object('\Core\Service\GoogleShoppingContent')
            ->constructor(
                \DI\link('core.GoogleShoppingContent.ProductConverter'),
                \DI\link('google.service.shoppingContent'),
                \DI\link('google.shopping.content'),
                \DI\link('\Core\GoogleShoppingContent\ValidateAvailable')
            ),

    'core.GoogleShoppingContent.Delayed' => \DI\object('\Core\GoogleShoppingContent\Delayed')
            ->constructor(\DI\link('productFilter')),

    'currentUser' => User_Model_Session::user(),

    'eav.storage' => \DI\object('\Application_Model_EAV_DB_MySql'),

    'core.service.thirdPartyWidget' => \DI\object('\Core\Service\ThirdPartyWidget')
            ->constructor(
                \DI\link('third_party.widget.config'),
                \DI\link('\Zend_View'),
                \DI\link('currentUser'),
                \DI\link('eav.storage')
            )

);
