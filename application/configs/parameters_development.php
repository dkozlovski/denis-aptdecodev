<?php
/**
 * Author: Alex P.
 * Date: 04.08.14
 * Time: 19:25
 */

return array(

    'facebook.keys' => array(
        'appId' => '514871408572909',
        'secret' => 'ee726d26a23e7eee16d93d56040177a3',
    ),

    'google.shopping.content' => array(
        'applicationName' => 'Aptdeco Shopping Content',
        'merchantId' => '11370803',
        'clientId' => '890962010999-77e75j2eaqd88ordrh29270oqiavqkpu.apps.googleusercontent.com',
        'serviceAccountName' => '890962010999-77e75j2eaqd88ordrh29270oqiavqkpu@developer.gserviceaccount.com',
        'publicKey' => 'daeff031ed803f8e1e0c02e877c3086b8a1ec68e',
        'keyFileLocation' => realpath(APPLICATION_PATH . "/configs/google/Aptdeco Shopping Content-72e87cf46084.p12"),
        //set to false in production
        'dryRun' => APPLICATION_ENV !== 'live',
    ),
     // array: [ '{$category_id}' => [ 'google_category_name' => '{$name}', 'id' => '{$category_id}'] ]
    'google.shopping.content.categories.map' => \DI\factory(function($c){
            /** @var \Category_Model_Category_FlexFilter $filter */
            $filter = $c->get('categoryFilter');

            return $filter->alwaysJoin('google_category_name')
                ->apply()
                ->getColumns(array('id', 'google_category_name'), true);
        }),
);