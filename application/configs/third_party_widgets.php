<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 09.09.14
 * Time: 17:49
 */

return array(
    'third_party.widget.config' => array(
        // global config
        'ENV' => APPLICATION_ENV,
        'enabled' => true, // enable/disable all widget output
        'view.basePath' => realpath(APPLICATION_PATH . '/views'),
        'view.scriptPath' => APPLICATION_PATH . '/views/scripts/third_party_widgets/',
        'allowTest' => true, // if set to false all widgets will output only in live env

        // widgets config
        /*'vendor.example' => array (
            // enable/disable showing
            'enabled' => true,

            // widget code. relative to script path
            'view_script' => 'example/template.phtml',

            // set special mark to determine whether the widget is already been shown
            // bool isFirstHit will be assigned to view if set to true
            'markShow' => true,

            // track hits (number of times when widget was shown)
            // int hits will be assigned to view
            'trackHits' => true,

            // maximum shows (widget will not be showed once it reach this number, trackHits must be set to true)
            'limitHits' => 1,
        ),*/

        // global config for all frienbuy's widgets (assigns to view:... echo $this->site; ...)
        'friendbuy.config' => array(
            'site' => 'site-984672a3-www.aptdeco.com',
        ),

        'friendbuy.header' => array(
            'enabled' => true,
            'view_script' => 'friendbuy/header.phtml',
        ),

        'friendbuy.refer_friend' =>array (
            'enabled' => true,
            'view_script' => 'friendbuy/refer_friend.phtml',
            'markShow' => true,
        ),

        'friendbuy.track_purchase' => array(
            'enabled' => true,
            'view_script' => 'friendbuy/track_purchase.phtml',
        )

    ));