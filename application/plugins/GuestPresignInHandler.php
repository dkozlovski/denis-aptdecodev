<?php

/**
 * Plugin save the request parameters for future redirect 
 * see Ikantam_Controller_Action_Helper_RedirectAfter
 */
class Application_Plugin_GuestPresignInHandler extends Zend_Controller_Plugin_Abstract
{

    /**
     * @type array
     * structure:
     * 'moduleName' - save params in module i.e. in all controllers and actions in this, module 
     * 'moduleName2' => array ('controllerName', 'controllerNameX') - save params just in specified controllers (in all actions)
     * 'moduleName3'  => array ('controllerName2' => array ('actionName')) - save params just in specified action
     */
    protected $_saveIn = array(
        'user',
        'checkout'
    );

    /**
     * Exclude parameters saving from...
     * Same as $_saveIn
     */
    protected $_exclude = array(
        'user'     => array('login'),
        'checkout' => array(
            'guest',
            'onestep' => array(
                'set-postal-code',
                'set-address',
                'set-method',
                'set-address-info'
            )
        )
    );

    public function preDispatch(Zend_Controller_Request_Abstract $rqst)
    {
        if (!User_Model_Session::instance()->isLoggedIn()) {
            $this->_saveRequest($rqst);
        }
    }

    protected function _saveRequest($rqst)
    {
        $module     = $rqst->getModuleName();
        $controller = $rqst->getControllerName();
        $action     = $rqst->getActionName();

        if ($this->_match($this->_exclude, $module, $controller, $action)) {
            return $this;
        }

        if ($this->_match($this->_saveIn, $module, $controller, $action)) {
            Zend_Controller_Action_HelperBroker::getStaticHelper('redirectAfter')->saveRedirectParams();
        }

        return $this;
    }

    /**
     * Find matches in list of rules
     * @param array $haystack - list rules
     * @param string $module
     * @param string $controller
     * @param string $action 
     * @return bool
     */
    protected function _match($haystack, $module, $controller, $action)
    {
        if (in_array($module, $haystack)) {
            return true;
        }

        if (isset($haystack[$module])) {
            foreach ($haystack[$module] as $key => $value) {

                if ($value === $controller) {
                    return true;
                }

                if ($key === $controller && is_array($value)) {
                    if (in_array($action, $value)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

}