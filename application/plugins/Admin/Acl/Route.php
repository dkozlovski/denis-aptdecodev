<?php

class Application_Plugin_Admin_Acl_Route extends Zend_Controller_Plugin_Abstract
{

    protected $_acl;
    protected $_role;

    public function preDispatch(Zend_Controller_Request_Abstract $rqst)
    {

        $this->_acl  = Application_Model_Acl::getInstance();
        $role        = new Admin_Model_Roles_Backend();
        $this->_role = $role->getRole(Admin_Model_Session::instance()->getAdmin()->getData('role_id'));

        if ($rqst->module == 'admin' && !$this->_isAllowedRoute($rqst)) {
            //throw new Application_Model_Acl_Exception_Route('ACCESS DENIED');
            $this->getResponse()->setRedirect(Ikantam_Url::getUrl('admin/error/access'))->sendResponse();
        }
    }

    protected function _isAllowedRoute($rqst)
    {
        $action = $rqst->action; //privilege

        $moduleLevel     = 'mvc:' . $rqst->module;
        $controllerLevel = $moduleLevel . '.' . $rqst->controller;

        $access = $this->_acl->isAllowed($this->_role, $controllerLevel, $action); //allowed to controller action

        return $access;
    }

}