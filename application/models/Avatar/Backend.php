<?php

class Application_Model_Avatar_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'avatars';

	public function getByUserId($object, $userId, $notCropped)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `user_id` = :user_id';

		if (!$notCropped) {
			$sql.=' AND `is_available` = 1';
		}

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':user_id', $userId);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row) {
			$object->addData($row);
		}
	}

	public function getAll($collection)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($result as $row) {
			$object = new $this->_itemClass();
			$object->addData($row);

			$collection->addItem($object);
		}
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		$sql = "UPDATE `" . $this->_getTable() . "` set `title` = :title, 
            `is_available` = :available, `s3_path` = :s3_path
            WHERE `id` = :id";
        
		$stmt = $this->_getConnection()->prepare($sql);
		$title = $object->getTitle();
		$id = $object->getId();
        $s3Path = $object->getS3Path();
		$available = $object->getIsAvailable();

		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':id', $id);
        $stmt->bindParam(':s3_path', $s3Path);
		$stmt->bindParam(':available', $available);
		$stmt->execute();
	}

	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = "INSERT INTO `" . $this->_getTable() . "` (`title`, `user_id`, `s3_path`) 
            VALUES (:title, :u_id, :s3_path)";
		$stmt = $this->_getConnection()->prepare($sql);
		$title = $object->getTitle();
		$userId = $object->getUserId();
        $s3Path = $object->getS3Path();

		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':u_id', $userId);
        $stmt->bindParam(':s3_path', $s3Path);
		$stmt->execute();
		$object->setId($this->_getConnection()->lastInsertId());
	}

}
