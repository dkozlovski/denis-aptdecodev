<?php

class Application_Model_ShippingFeeRule extends Application_Model_Abstract
{

    protected $_saveFlag = true;

    public function getSumOrLabel($default = null)
    {
        $sum = isset($this->_data['sum']) ? $this->_data['sum'] : $default;
        if ($sum == -1) {
            return 'any';
        }

        return $sum;
    }

    protected function prepareShippingFee($val)
    {

        if (!preg_match('/^((\d*),?\s?)*$/u', $val)) {
            return false;
        }

        $val   = str_replace(' ', '', $val);
        $items = explode(',', $val);

        return array_values(array_filter($items, function($a) {
                            if ($a !== '') {
                                return true;
                            }
                        }));
    }

    public function getArrayOptions()
    {
        $options = $this->getOptions('a:0:{}');
        return unserialize($options);
    }

    public function setOptions(array $options)
    {
        parent::setOptions(serialize($options));
        return $this;
    }

    public function getByPair($operator, $value)
    {
        if ($value == 'any') {
            $value = -1;
        }
        $this->_getbackend()->getByPair($this, $operator, $value);
        return $this;
    }

    public function getFee($numberOfItems = 1)
    {
        $sum           = parent::getFee();
        $options       = $this->getArrayOptions();
        $optionsLength = count($options);


        if ($numberOfItems == 1 || !$options) {
            return $sum;
        }

        $lastOption = $options[$optionsLength - 1];

        //$i = 1 : exclude first item        
        for ($i = 1; $i < $numberOfItems; $i++) {

            if ($i - 1 < $optionsLength) {
                $sum += $options[$i - 1];
            } else {
                $sum += $lastOption * ($numberOfItems - $i);
                return $sum;
            }
        }

        return $sum;
    }

    public function getFullFeeString()
    {
        $options = $this->getArrayOptions();

        array_unshift($options, $this->getFee());
        return implode(', ', $options);
    }

    public function setFee($val)
    {
        $this->unsOptions();

        if (is_int($val)) {
            parent::setFee($val);
            return $this;
        } elseif (is_string($val)) {
            $data = $this->prepareShippingFee($val);
            if ($data) {
                $this->_saveFlag = true;
                parent::setFee($data[0]);
                unset($data[0]);
                $data            = array_values($data);

                if ($data) {
                    $this->setOptions($data);
                }
            } else {
                $this->_saveFlag = false;
            }
        }

        return $this;
    }

    public function save()
    {
        if ($this->_saveFlag) {
            parent::save();
        }

        return $this;
    }

    public function isExists()
    {
        if ($this->_saveFlag) {
            return parent::isExists();
        }

        return false;
    }

}