<?php

class Application_Model_HtmlBuilder
{

    protected $_html = null;

    public function addAttributes(&$element, $attributes)
    {
        foreach ($attributes as $attr => $value) {
            $element.=' ' . $attr . ' = "' . $value . '"';
        }
        return $this;
    }

    public function wrap(&$text, $tag, $attributes = null)
    {
        if (!$tag)
            return $this;
        $begin = '<' . $tag;
        if ($attributes)
            $this->addAttributes($begin, $attributes);
        $begin.='>';

        $begin.= $text;

        $text = $begin . '</' . $tag . '>';
        return $this;
    }

    public function getUrl($params, $params2 = null, $reset = false)
    {
        $params = array_reverse(explode('/', trim($params, '/')));

        $action     = !empty($params[0]) ? $params[0] : null;
        $controller = !empty($params[1]) ? $params[1] : null;
        $module     = !empty($params[2]) ? $params[2] : null;
        if (!$params2)
            $params2    = array();
        $view       = new Zend_View();
        $d          = array('module'     => $module, 'controller' => $controller, 'action'     => $action);

        return $view->serverUrl() . $view->url(array_merge($d, $params2), null, $reset);
    }

}