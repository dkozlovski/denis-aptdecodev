<?php

class Application_Model_Options extends Application_Model_Abstract
{
    /* This codes must be stored in DB (`option` table) */

    const INDIVIDUAL = 'comission_individual';
    const SELLER     = 'comission_from_seller';
    const BUYER      = 'comission_from_buyer';

    /**
     * Contains conditions and values
     * @type Array
     */
    protected $_conditions = array();

    /**
     * Flag - to check if loaded
     * @type bool 
     */
    protected $_loaded = false;
    
    /**
     * Stores additional condition properties for fee calculating
     * for example: 
     *  'shipping_method_id' => 'delivery', 'some_prop', 'prop2' => 'value'
     * and then before return value checks for consistent method which hanlde value...
     * 
     * @type array
     */
    protected $_additionalConditions = array();
    
    /**
     * Add additional condition which will be considered when fee will calculate
     * 
     * @param  $name - condition name (method name for conditition = _ACHanler.$name)
     * @param  $params (Optional) - parameters that will be passed to the method(handler)
     * @return object self
     */
    public function registerAdditionalCondition ($name, $params = null)
    {   
        if(null === $params){
            $this->_additionalConditions[] = $name;
        } else {
            $this->_additionalConditions[$name] = $params;   
        }        
        return $this;
    }

    /* This is compare function for uasort. !DO NOT CHANGE THIS FUNCTION! */

    protected final function _sorter_($a, $b)
    {
        if ($a['value'] == $b['value']) {
            return 0;
        }
        return ($a['value'] < $b['value']) ? 1 : -1;
    }

    /* This is compare function for uasort. !DO NOT CHANGE THIS FUNCTION! */

    protected final function _sorter_for_less_($a, $b)
    {
        if ($a['value'] == $b['value']) {
            return 0;
        }
        return ($a['value'] < $b['value']) ? -1 : 1;
    }

    /** Load option values by code
     * @param string $code - see consts 
     * @return self
     */
    public function loadOption($code)
    {
        $this->_loaded     = true;
        $this->_conditions = array();
        $result            = $this->_getbackend()->loadOption($code);
        if (is_array($result) && !empty($result)) {
            $auxConditions = array();
            $this->setId($result[0]['option_id']);
            $this->setCode($code);
            foreach ($result as $row) {
                $condition        = unserialize($row['condition']);
                $condition['fee'] = $row['value'];
                if ($condition['operator'] == '<') {
                    $this->_conditions[] = $condition;
                } else {
                    $auxConditions[] = $condition;
                }
            }
            /* ! THIS SORT VERY IMPORTANT. getFee method depend on it. */
            uasort($auxConditions, array(get_class($this), '_sorter_'));
            uasort($this->_conditions, array(get_class($this), '_sorter_for_less_'));
            $this->_conditions = array_merge($this->_conditions, $auxConditions);
        }
        return $this;
    }

    /** Adds new fee value depends on condition
     * @param int $feeValue - formal | may be attached to the object ($options->setValue(100))
     * @param array $condition - formal | may be attached to the object ($options->setCondition($condition))
     * @return mixed - int (id of new condition) or null if save fail
     * Condition structure:
     * $condition  = array ('operator' => '<', 'value' => '100'); | See Application_Model_Options_Backend::_availableOperators
     */
    public function addNewConditionValue($feeValue = null, $condition = null)
    {
        $ex_conflict = new Exception(__METHOD__ . ' conditions conflict.', 500);
        if (!is_null($feeValue))
            $this->setValue($feeValue);

        if (!is_null($condition))
            $this->setCondition($condition);

        $condition = $this->getCondition();


        $myConditions = $this->getConditions($this->getCode());
        foreach ($myConditions as $myCondition) {
            if ($myCondition['operator'] == '<' && $condition['operator'] == '>') {
                if ($condition['value'] < $myCondition['value']) {
                    throw $ex_conflict;
                }
            }

            if ($myCondition['operator'] == '>' && $condition['operator'] == '<') {
                if ($condition['value'] > $myCondition['value']) {
                    throw $ex_conflict;
                }
            }
        }

        return $this->_getbackend()->addNewConditionValue($this);
    }

    /** 
     * !PERCENT!
     * Get the fee for sum
     * 
     * @param float  $sum - to get fee depend on it
     * @param string $code - if option not loaded use code to define it
     * @return float
     */
     public function getFee ($sum, $code = null)
     {
        $fee = $this->_getFee($sum, $code);
        foreach($this->_additionalConditions as $conditionKey => $condition) {
            if(is_array($condition)) {               
                $handler = '_ACHandler'.$conditionKey; 
                if(method_exists($this, $handler)) {
                    $params = array_merge(array($code, $sum, &$fee), (array)$condition);
                    $fee = call_user_func_array(array($this, $handler), $params);
                } 
            } elseif(is_string($condition)) {
                $handler = '_ACHandler'.$condition;
                if(method_exists($this, $handler)) {
                    $fee = $this->{$handler}($code, $sum, $fee);
                }   
            } 
        }
        return $fee;
     }
     
     /**
      * Calculate original fee (without additional condition handlers)
      * 
      * @param  float $sum
      * @param  string $code 
      * @return float
      */
    protected function _getFee($sum, $code = null)
    {
        if (!is_numeric($sum)) {
            throw new Exception(__METHOD__ . ' sum must be a number.', 7);
        }
        if (!$this->_loaded) {

            if (!$code) {
                $code = $this->getCode();
                if (!$code)
                    throw new Exception('Can not load options. Code is undefined.');
            }
            $this->loadOption($code);
        }

        foreach ($this->_conditions as $condition) {
            if ($condition['operator'] === 'true') {
                return $condition['fee'];
            }
        }

        foreach ($this->_conditions as $condition) {
            if ($condition['operator'] === '=' && $this->_compare($sum, '=', $condition['value'])) {
                return $condition['fee'];
            }
        }

        $_maxLessThan = null;
        foreach ($this->_conditions as $condition) {
            if (is_null($_maxLessThan) && $condition['operator'] == '<') {
                $_maxLessThan = $condition;
            } elseif ($_maxLessThan['value'] < $condition && $condition['operator'] == '<') {
                $_maxLessThan = $condition;
            }

            if ($this->_compare($sum, $condition['operator'], $condition['value'])) {
                return $condition['fee'];
            }
        }



        return $_maxLessThan['fee'];
    }

    protected function _compare($value1, $operator, $value2)
    {

        switch ($operator) {

            case '<':
                return $value1 < $value2;
                break;

            case '>':
                return $value1 > $value2;
                break;

            case '=':
                return $value1 === $value2;
                break;

            case 'true':
                return true;
                break;

            default:
                return false;
                break;
        }
    }

    public function addIndividualFee($userId, $feeValue, $code = null)
    {
        if (!$code) {
            $this->getBy_code(self::INDIVIDUAL);
        } else {
            $this->getBy_code($code);
        }

        if (!$userId || !is_numeric($feeValue))
            throw new Exception(__METHOD__ . " Invalid parameters");
        $BE = $this->_getbackend();
        $BE->startTransaction();
        try {
            $option_value_id = $this->addNewConditionValue($feeValue, array('operator' => 'true', 'user'     => $userId));
            $BE->addIndividualFee($userId, $option_value_id);
        } catch (Exception $ex) {
            $BE->rollBack();
            throw $ex;
        }
        $BE->endTransaction();
        return $this;
    }

    public function getIndividualFee($userId, $code = self::INDIVIDUAL)
    {
        $this->getBy_code($code);
        if (!$id = $this->getId(false))
            return null;

        return $this->_getbackend()->getIndividualFee($id, $userId);
    }

    public function getIndividualFeeOrDefault($userId, $code, $sum)
    {

        $fee = $this->getIndividualFee($userId);
        if (!$fee) {
            $fee = $this->getFee($sum, $code);
        }
        return $fee;
    }

    /** Updates condition
     * @param string $code
     * @param array $oldCondition - Structure: array('operator' => '<', 'value' => '300')
     * @param array $newCondition - same structure as $oldCondition + new fee value: array (...., 'fee' => '10')
     * @return bool - result
     */
    public function editCondition($code, $oldCondition, $newCondition)
    {
        return $this->_getbackend()->updateCondition($code, $oldCondition, $newCondition);
    }

    public function editIndividualFee($userId, $newFee)
    {
        return $this->_getbackend()->updateIndividualCondition($userId, $newFee);
    }

    public function getConditions($code = null)
    {
        if (!$this->_loaded)
            $this->loadOption($code);
        return $this->_conditions;
    }

    public function deleteCondition($code, $condition)
    {
        return $this->_getbackend()->deleteCondition($code, $condition);
    }

    public function deleteIndividualFee($userId, $code = self::INDIVIDUAL)
    {
        return $this->_getbackend()->deleteIndividualFee($userId, $code);
    }

    public function getAllIndividualOptions()
    {
        return $this->_getbackend()->getAllIndividualOptions();
    }
    
    //additional conditions handlers
    
    /**
     * Handle fee value if additional condition registred for shipping method
     * 
     * @param  string $code - see self constants
     * @param  string $sum - original sum passed to the "getFee" method
     * @param  float  $fee -  calculated fee(returned value from "getFee")
     * @param  string $method - shipping method
     * @return float new fee value
     */
    protected function _ACHandlerShippingMethod($code, $sum, &$fee, $method)
    {
        if($code == self::SELLER && $method == 'pick-up') {
            return 0;
        }
        
        return $sum;
    }

}
