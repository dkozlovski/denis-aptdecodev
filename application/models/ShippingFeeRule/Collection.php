<?php

class Application_Model_ShippingFeeRule_Collection extends Application_Model_Abstract_Collection
{

    protected $_loaded       = false;
    protected $_replacements = array(
        '<' => 'less than',
        '>' => 'over',
        '=' => 'equals to'
    );

    public function getShippingFee($sum, $numberOfItems = 1)
    {
        $this->_load();

        $default = null;
        foreach ($this->getItems() as $rule) {
            $operator = $rule->getOperator();
            if ($operator == 'always') {
                return $rule->getFee();
            }
            if ($operator == '=' && $sum == $rule->getSum()) {
                return $rule->getFee();
            }
            if ($operator == 'default') {
                $default = $rule->getFee();
            }
        }
        $this->sortByColumn('sum', 'ASC', false);

        $c = count($this->getItems());

        for ($i = 0, $j = $c; $j--; $i++) {
            $itemI = $this->_items[$i];
            $itemJ = $this->_items[$j];

            if ($itemI->getOperator() == '<' && $sum < $itemI->getSum()) {
                return $itemI->getFee($numberOfItems);
            }

            if ($itemJ->getOperator() == '>' && $sum > $itemJ->getSum()) {
                return $itemJ->getFee($numberOfItems);
            }
        }

        return $default;
    }

    /**
     * @param string $operator
     * @param float $sum 
     * @return self
     */
    public function getByCondition($operator, $sum)
    {
        $this->_getbackend()->getByCondition($this, $operator, $sum);
        return $this;
    }

    protected function _load()
    {
        if (!$this->_loaded) {
            $this->getAll_();
            $this->_loaded = true;
        }
    }

    public function findBestCondition()
    {
        $this->_load();
        $best = null;
        foreach ($this as $rule) {
            if (is_null($best) && $rule->getOperator() != 'default') {
                $best = $rule;
            } else {
                if (!is_null($best) && $rule->getFee() < $best->getFee() && $rule->getOperator() != 'default') {
                    $best = $rule;
                }
            }
        }//foreach

        return $best;
    }

    public function getSentenceForFreeDelivery()
    {
        $best     = $this->findBestCondition();
        $sentence = null;
        if ($best) {
            if ($best->getFee() != 0 || $best->getSum() < 1) {
                return null;
            }
            $sentence .= 'Qualify for FREE delivery in New York for orders ';
            $sentence .= isset($this->_replacements[$best->getOperator()]) ? $this->_replacements[$best->getOperator()] : '';
            $sentence .= ' <strong>$' . $best->getSum() . '</strong>.';
        }


        return $sentence;
    }

}
