<?php
class Application_Model_ShippingFeeRule_Backend extends Application_Model_Abstract_Backend
{
    
    protected $_table = 'shipping_fee_rules';
    protected $_operators = array('<', '>', '=', 'default', 'always');
    protected $_fields_ = array('id', 'operator', 'sum', 'fee', 'options');  

    
	protected function _update(Application_Model_Abstract $object)
	{
        $this->runStandartUpdate(array('operator', 'sum', 'fee', 'options'), $object);    
	}

	protected function _insert(Application_Model_Abstract $object)
	{
        $this->runStandartInsert(array('operator', 'sum', 'fee', 'options'), $object); 
	}
    
    public function save(Application_Model_Abstract $object)
    {
        if(!in_array($object->getOperator(), $this->_operators)) {
            throw new Exception('Undefined operator "'. $object->getOperator() .'".');
        }
        if($object->getSum() == 'any') {
            $object->setSum(-1);
        }
        parent::save($object);
    }
    
    public function getByPair (Application_Model_Abstract $object, $operator, $value)
    {
        $sql = "SELECT * FROM `". $this->_getTable() ."` WHERE `operator` = :operator AND `sum` = :value LIMIT 1";
        
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':operator', $operator);
        $stmt->bindParam(':value', $value);
        
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if(is_array($result)) {
            $object->setData($result);
        }
    }
    
    public function getByCondition (Application_Model_Abstract_Collection $collection, $operator, $sum)
    {
        $sql = "SELECT * FROM `". $this->_getTable() ."` WHERE `operator` = :operator ";
        $binds = array(':operator' => $operator);
        
        switch($operator) {
            
            case '<':
                $sql .= 'AND `sum` < :sum';
                $binds[':sum'] = $sum;                
                break;
            
            case '>':
                $sql .= 'AND `sum` > :sum';
                $binds[':sum'] = $sum;
                break;
            
            case '=':
                $sql .= 'AND `sum` = :sum';
                $binds[':sum'] = $sum;
                break;
                
            default:                
                break;                                    
                
        }        

        $stmt = $this->_getConnection()->prepare($sql);

        foreach($binds as $bind => $value) {
            $stmt->bindValue($bind, $value);
        }

        $this->fillCollection($stmt, $collection);        
        
    }
    
   
}