<?php

class Application_Model_SmsNotification extends Application_Model_Abstract
{
    protected $_backendClass = 'Application_Model_SmsNotification_Backend';
    
    public function getByType($type)
    {
        $this->_getbackend()->getByType($this, $type);
        return $this;
    }
    
    public function bindParam($template, $param)
    {
        $this->setData('text', str_replace($template, $param, $this->getData('text')));
        return $this;
    }
}

