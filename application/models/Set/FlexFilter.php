<?php

/**
 * @method Application_Model_Set_FlexFilter menu_name (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter or_menu_name (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter id (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter or_id (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter name (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter or_name (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter description (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter or_description (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter image_path (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter or_image_path (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter page_url (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter or_page_url (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter is_active (string $operator, mixed $value)
 * @method Application_Model_Set_FlexFilter or_is_active (string $operator, mixed $value)
 * @method Application_Model_Set_Collection apply(int $limit = null, int $offset = null)
 */
class Application_Model_Set_FlexFilter extends Ikantam_Filter_Flex
{

    protected $_acceptClass = 'Application_Model_Set_Collection';
    protected $_joins       = array(
        'collections_menus' => 'INNER JOIN `collections_menus` AS `c_m` ON `c_m`.`collection_id` = `collections`.`id`',
        'menus' => array('collections_menus' => 'INNER JOIN `menus` ON `c_m`.`menu_id` = `menus`.`id`'),
    );
    protected $_select      = array();
    protected $_rules       = array(
        'menu_name' => array('menus' => '`menus`.`name`'),
    );

}