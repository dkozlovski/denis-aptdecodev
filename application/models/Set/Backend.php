<?php

class Application_Model_Set_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'collections';
    protected $_related_products = 'collections_products';
    protected $_fields_ = array('id', 'name', 'image_path', 'description', 'page_url', 'is_active');
    
    public function addProducts (\Application_Model_Set $object, $products)
    {
        $ids = $this->_productId($products);
        while($id = array_shift($ids)) {
            $this->_saveRelatedProduct($object, $id);
        }
        
        return $object;
    }
    
    public function removeProducts (\Application_Model_Set $object, $products)
    {
        $ids = $this->_productId($products);
        while($id = array_shift($ids)) { 
            $this->_removeRelatedProduct($object, $id);
        }
        
        return $object;    
    }
    
    public function removeAllProducts (\Application_Model_Set $object)
    {
        $this->_removeRelatedProduct($object, null);
        return $object;
    }
    
    protected function _removeRelatedProduct(\Application_Model_Set $object, $productId)
    {
        $sql = "DELETE FROM `".$this->_related_products."` WHERE `collection_id` = :collection_id";
        $sql .= !is_null($productId) ? " AND `product_id` = :product_id" : '';
        $stmt = $this->_prepareSql($sql);
        $stmt->bindValue(':collection_id', $object->getId(), PDO::PARAM_INT);
        (!is_null($productId)) && $stmt->bindValue(':product_id', $productId, PDO::PARAM_INT); 
        $stmt->execute();   
    }
    
    protected function _productId($products)
    {
        $ids = array();
        if(is_array($products)) {
            $ids = $products;
        }elseif(!is_object($products)) {
            $ids = (array) $products;
        }elseif($products instanceof Product_Model_Product_Collection) {
            $ids = $products->getColumn('id');   
        } elseif($products instanceof Product_Model_Product) {
            $ids[] = $products->getId();
        }

        return $ids;    
    }
    
    protected function _saveRelatedProduct(\Application_Model_Set $object, $productId) 
    {        
        $stmt = $this->_prepareSql(
            "INSERT IGNORE INTO `".
            $this->_related_products.
            "` (`collection_id`, `product_id`) VALUES(:collection_id, :product_id)"
        );
        $stmt->bindValue(':collection_id', $object->getId(), PDO::PARAM_INT);
        $stmt->bindValue(':product_id', $productId, PDO::PARAM_INT);
        
        try{ 
            $stmt->execute();
         } catch(PDOException $E) { 
              if($E->getCode() != 23000) {
                  throw $E;
              }
         }
    }
    
    protected function _getPageUrl($name)
    {
        $url     = strtolower($name);
        $url     = preg_replace('/[\s]/', '-', $url);
        $url     = preg_replace('/[^-a-z0-9]/', '', $url);
        $url     = preg_replace('/[-]+/', '-', $url);
        $i       = 2;
        $baseUrl = $url;
        do {
            $count = 0;
            $sql1     = 'SELECT count(`page_url`) as `count` FROM `'.$this->_getTable().'` WHERE `page_url` = :page_url';
            $stmt1    = $this->_getConnection()->prepare($sql1);
            $stmt1->bindParam(':page_url', $url);
            $stmt1->execute();
            $result1  = $stmt1->fetch(PDO::FETCH_ASSOC);
            if ($result1 && isset($result1['count'])) {
                $count = (int) $result1['count'];
            }
            ///////
            $prodCount = 0;
            $sql2      = 'SELECT count(`page_url`) as `count` FROM `products` WHERE `page_url` = :page_url';
            $stmt2     = $this->_getConnection()->prepare($sql2);
            $stmt2->bindParam(':page_url', $url);

            $stmt2->execute();
            $result2   = $stmt2->fetch(PDO::FETCH_ASSOC);
            if ($result2 && isset($result2['count'])) {
                $prodCount = (int) $result2['count'];
            }
            if ($count !== 0 || $prodCount !== 0) {
                $url = $baseUrl . '-' . $i++;
            }
        } while ($count != 0 || $prodCount != 0);

        return $url;
    }    
    
	protected function _update(Application_Model_Abstract $object)
	{
            if($object->getStored('page_url') != $object->getPageUrl()){
                if($object->getPageUrl() == null){
                    $object->setPageUrl($this->_getPageUrl($object->getName()));
                }else{               
                    $object->setPageUrl($this->_getPageUrl($object->getPageUrl()));
                }
            }
        $this->runStandartUpdate(array('name', 'description', 'image_path', 'page_url', 'is_active'), $object);    
	}

	protected function _insert(Application_Model_Abstract $object)
	{
	    if($object->getPageUrl() == null){
                $object->setPageUrl($this->_getPageUrl($object->getName()));
            }else{
                $object->setPageUrl($this->_getPageUrl($object->getPageUrl()));
            }
        $this->runStandartInsert(array('name', 'description', 'image_path', 'page_url', 'is_active'), $object); 
	}        
}