<?php

class Application_Model_Set_Collection extends Application_Model_Abstract_Collection
{
    protected $_itemObjectClass = 'Application_Model_Set';
    public function getFlexFilter ()
    {
        return new Application_Model_Set_FlexFilter($this);
    }    
}