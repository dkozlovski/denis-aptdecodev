<?php

class Application_Model_Referer_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'referers';

    public function findRefererPrevValue($order)
    {
        $sql = 'SELECT `referer` FROM `' . $this->_getTable() . '` as `main_table`
            JOIN `orders` as `o` ON `main_table`.`order_id` = `o`.`id`';

        $bind = array();

        if ($order->getUserId()) {
            $sql .= ' WHERE `o`.`user_id` = :user_id';
            $bind[] = $order->getUserId();
        } else {
            $sql .= ' WHERE `o`.`user_email` = :user_email';
            $bind[] = $order->getUserEmail();
        }

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
        $res  = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($res && isset($res['referer'])) {
            return $res['referer'];
        }

        return false;
    }

    public function getForAdmin($collection, $filters, $offset, $limit)
    {
        $sql = 'SELECT `referer`, COUNT(*) as `orders_count` FROM `' . $this->_getTable() . '` as `main_table`
            JOIN `orders` as `o` ON `main_table`.`order_id` = `o`.`id`';

        $bind = array();
        $where = array();

        if (isset($filters['start_date'])) {
            $where[] = '`o`.`created_at` >= :start_date';
            $bind[] = $filters['start_date'];
        }

        if (isset($filters['end_date'])) {
            $where[] = '`o`.`created_at` <= :end_date';
            $bind[] = $filters['end_date'];
        }
        
        if (count($where)) {
            $sql .= ' WHERE ' . implode(' AND ', $where);
        }
        
        $sql .= ' GROUP BY `referer` ORDER BY `orders_count` DESC';

        if (!is_null($offset) && !is_null($limit)) {
            $sql .= ' LIMIT ' . $offset . ', ' . $limit;
        }

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
        $res  = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($res) {
            foreach ($res as $row) {
                $item = new Application_Model_Referer();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function countForAdmin($filters)
    {
        $sql0 = 'SELECT `referer`, COUNT(*) as `orders_count` FROM `' . $this->_getTable() . '` as `main_table`
                JOIN `orders` as `o` ON `main_table`.`order_id` = `o`.`id`';

        $bind  = array();
        $where = array();

        if (isset($filters['start_date'])) {
            $where[] = '`o`.`created_at` >= :start_date';
            $bind[]  = $filters['start_date'];
        }

        if (isset($filters['end_date'])) {
            $where[] = '`o`.`created_at` <= :end_date';
            $bind[]  = $filters['end_date'];
        }

        if (count($where)) {
            $sql0 .= ' WHERE ' . implode(' AND ', $where);
        }

        $sql0 .= ' GROUP BY `referer`';

        $sql = 'SELECT count(*) as `count` FROM (' . $sql0 . ') as `c`';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute($bind);
        $res  = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($res && isset($res['count'])) {
            return (int) $res['count'];
        }

        return 0;
    }

}
