<?php

class Application_Model_Referer_Collection extends Application_Model_Abstract_Collection
{

    public function getForAdmin($filters = array(), $offset = null, $limit = null)
    {
        $this->_getBackend()->getForAdmin($this, $filters, $offset, $limit);
        return $this;
    }

    public function countForAdmin($filters)
    {
        return $this->_getBackend()->countForAdmin($filters);
    }

}
