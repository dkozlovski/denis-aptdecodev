<?php

class Application_Model_Color_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'colors';

    public function getAll($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `is_active` = 1 ORDER BY `sort_order` ASC';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $object = new Application_Model_Color();
                $object->addData($row);
                $collection->addItem($object);
            }
        }
    }

    public function getByTitle($object, $title)
    {
        $sql  = "SELECT * FROM `" . $this->_getTable() . "` WHERE `title` = :title";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':title', $title);
        $stmt->execute();
        $row  = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = "UPDATE `" . $this->_getTable() . "` 
		   SET `title` = :title, `rgb_hex` = :rgb_hex 
		   WHERE `id` = :id";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':title', $object->getTitle());
        $stmt->bindParam(':rgb_hex', $object->getRgbHex());
        $stmt->bindParam(':id', $object->getId());

        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = "INSERT INTO `" . $this->_getTable() . "` (`title`, `rgb_hex`) 
		   VALUES (:title, :rgb_hex)";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':title', $object->getTitle());
        $stmt->bindParam(':rgb_hex', $object->getRgbHex());

        $stmt->execute();
        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _getPartialMatch($query)
    {
        $q = explode(' ', trim($query));

        foreach ($q as $word) {
            $query_like[] = '%' . str_replace('%', '', strtolower(trim($word))) . '%';
        }

        $sql = 'SELECT DISTINCT(`colors`.`id`), `colors`.`title`, `colors`.`hex`
            FROM `products`
            JOIN `categories` ON `products`.`category_id` = `categories`.`id`
            JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
            JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
            JOIN `colors` ON `products`.`color_id` = `colors`.`id`
            JOIN `materials` ON `products`.`material_id` = `materials`.`id`
            WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1';

        $c    = 1;
        $bind = array();

        foreach ($query_like as $word) {
            $sql .= "\n" . 'AND 
            (`products`.`title` LIKE :query' . $c++ . /* '
                      OR `products`.`description` LIKE :query' . $c++ . */ '
            OR `categories`.`title` LIKE :query' . $c++ . '
            OR `conditions`.`short_title` LIKE :query' . $c++ . '
            OR `manufacturers`.`title` LIKE :query' . $c++ . '
            OR `colors`.`title` LIKE :query' . $c++ . '
            OR `materials`.`title` LIKE :query' . $c++ . ')';

            //$bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bind);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getByProductQuery(Application_Model_Abstract_Collection $collection, $query)
    {
        /* $sql = 'SELECT DISTINCT(`id`), `title`, `hex` FROM `view_product_colors` 
          WHERE `product_title` LIKE :query1 OR `product_description` LIKE :query2';

          $stmt = $this->_getConnection()->prepare($sql);

          $query = '%' . str_replace('%', '', strtolower($query)) . '%';

          $stmt->bindParam(':query1', $query);
          $stmt->bindParam(':query2', $query);

          $stmt->execute();
          $rows = $stmt->fetchAll(PDO::FETCH_ASSOC); */

        $rows = $this->_getPartialMatch($query);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Application_Model_Color();
                $item->setData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getByCategory(Application_Model_Abstract_Collection $collection, $category)
    {
        $sql = 'SELECT DISTINCT(`id`), `title`, `hex`, `element_id`, `is_active` FROM `view_product_colors` 
			WHERE `category_id` = :category_id1 OR `parent_id` = :category_id2';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':category_id1', $category);
        $stmt->bindParam(':category_id2', $category);

        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Application_Model_Color();
                $item->setData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getByManufacturer(Application_Model_Abstract_Collection $collection, $manufacturerId)
    {
        $sql = 'SELECT DISTINCT(`id`), `title`, `hex`, `element_id`, `is_active` FROM `view_product_colors` 
			WHERE `manufacturer_id` = :manufacturer_id';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':manufacturer_id', $manufacturerId);

        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Application_Model_Color();
                $item->setData($row);
                $collection->addItem($item);
            }
        }
    }

}
