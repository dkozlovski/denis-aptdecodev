<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 03.07.14
 * Time: 0:40
 */

class Application_Model_EAV_DB_MySql extends Application_Model_Abstract
    implements  Ikantam_EAV_Interface_DB
{

    /**
     * @var string
     */
    protected $_backendClass = 'Application_Model_EAV_DB_MySql_Backend';

    /**
     * @var mixed
     */
    protected static  $values = array();

    /**
     * Saves value
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @param $value
     * @return mixed
     */
    public function saveValue
    (
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute,
        $value
    ) {
        $this->_getbackend()->saveValue($entity, $attribute, $value);
    }

    /**
     * Alias for saveValue
     * @return mixed
     */
    public function save()
    {
        return call_user_func_array(array($this, 'save_value'), func_get_args());
    }

    /**
     * Retrieves value
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @return mixed
     */
    public function getValue
    (
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute
    ) {
        $key = crc32($entity->getEntityId() . $attribute->getName());
        if (!isset(self::$values[$key])) {
            self::$values[$key] = $this->_getbackend()->getValue($entity, $attribute);
        }

        return self::$values[$key];
    }

    /**
     * Delete value
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @return mixed
     */
    public function deleteValue
    (
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute
    ) {
        return  $this->_getbackend()->deleteValue($entity, $attribute);
    }
}