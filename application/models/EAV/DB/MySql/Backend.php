<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 03.07.14
 * Time: 0:52
 */

class Application_Model_EAV_DB_MySql_Backend extends Application_Model_Abstract_Backend
{
    /**
     * Retrieves attribute value for entity
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @return mixed
     */
    public function getValue(
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute
    ) {
        $this->initTable($entity, $attribute);
        $sql = "SELECT `value` FROM `" . $this->_getTable() . "` WHERE `entity_id` = :entity_id
                AND `attribute_id` = :attribute_id";

        $stmt = $this->_prepareSql($sql);

        $stmt->bindValue(':entity_id', $entity->getEntityId(), PDO::PARAM_INT);
        $stmt->bindValue(':attribute_id', $attribute->getAttributeId());

        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_COLUMN);

        return $result === false ? null : $result;
    }

    /**
     * Saves EAV value
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @param $value
     */
    public function saveValue (
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute,
        $value
    ) {
        $this->initTable($entity, $attribute);
        $sql = "INSERT INTO `" . $this->_getTable() . "` (`entity_id`, `attribute_id`, `value`)
                VALUES(:entity_id, :attribute_id, :value)
                ON DUPLICATE KEY UPDATE `value` = VALUES(`value`)";

        $stmt = $this->_prepareSql($sql);

        $stmt->bindValue(':entity_id', $entity->getEntityId(), PDO::PARAM_INT);
        $stmt->bindValue(':attribute_id', $attribute->getAttributeId(), PDO::PARAM_INT);
        $stmt->bindParam(':value', $value);

        $stmt->execute();
    }

    /**
     * Delete value
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @return mixed
     */
    public function deleteValue
    (
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute
    ) {
        $this->initTable($entity, $attribute);
        $sql = "DELETE FROM `" . $this->_getTable() . "` WHERE `entity_id` = :entity_id
                AND `attribute_id` = :attribute_id LIMIT 1";

        $stmt = $this->_prepareSql($sql);

        $stmt->bindValue(':entity_id', $entity->getEntityId(), PDO::PARAM_INT);
        $stmt->bindValue(':attribute_id', $attribute->getAttributeId(), PDO::PARAM_INT);

        $stmt->execute();
    }

    /**
     * Init with necessary table
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     */
    protected function initTable (
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute
    ) {
      $this->_table = $this->getTableByEntityAttribute($entity, $attribute);
    }

    /**
     * Return table name based on attribute-entity
     * @param Ikantam_EAV_Interface_Entity $entity
     * @param Ikantam_EAV_Interface_Attribute $attribute
     * @return string
     */
    protected function getTableByEntityAttribute (
        \Ikantam_EAV_Interface_Entity $entity,
        \Ikantam_EAV_Interface_Attribute $attribute
    ) {
       return 'eav_' . strtolower($entity->getEntityFamily() . '_' . $attribute->getType());
    }
} 