<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 03.07.14
 * Time: 2:40
 */


class Application_Model_EAV_Attribute extends Application_Model_EAV_Attribute_Abstract
{
    /**
     * Specifies INT attributes
     */
    const ATTRIBUTE_TYPE_INT = 'int';

    /**
     * Specifies TEXT attributes
     */
    const ATTRIBUTE_TYPE_TEXT = 'text';

    /**
     * Specifies VARCHAR attributes
     */
    const ATTRIBUTE_TYPE_VARCHAR = 'varchar';

    /**
     * Specifies SMALLINT attributes
     */
    const ATTRIBUTE_TYPE_SMALLINT = 'smallint';

    /**
     * Specifies DECIMAL attributes
     */
    const ATTRIBUTE_TYPE_DECIMAL = 'decimal';

    /**
     * @var string
     */
    protected $type;

    /**
     * @param string $type
     * @param string $name
     * @throws InvalidArgumentException
     */
    public function __construct($type, $name = null)
    {
        $reflection = new \ReflectionClass($this);
        $types = $reflection->getConstants();
        if (!in_array($type, $types)) {
            throw new \InvalidArgumentException('Unknown attribute type "' . $type . '"');
        }
        $this->type = $type;
        if ($name) {
            $this->setName($name);
        }
    }

    /**
     * Gets attribute type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}