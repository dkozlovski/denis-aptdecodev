<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 03.07.14
 * Time: 2:18
 */

abstract class Application_Model_EAV_Attribute_Abstract extends Application_Model_Abstract
    implements Ikantam_EAV_Interface_Attribute
{
    protected $_backendClass = 'Application_Model_EAV_Attribute_Backend';

    /**
     * Gets attribute type
     * @return string
     */
    abstract public function getType();

    /**
     * Sets attribute name
     * @param string $name
     * @return mixed
     */
    public function setName($name)
    {
        $this->getBy_name($name);
        if (!$this->isExists()) {
            parent::setName($name);
            parent::setType($this->getType());
            $this->save();
        }
        return $this;
    }

    /**
     * Gets attribute name
     * @return string
     */
    public function getName()
    {
        return parent::getName();
    }

    /**
     * Retrieve attribute unique identifier
     * @return mixed
     */
    public function getAttributeId()
    {
        return parent::getId();
    }
}