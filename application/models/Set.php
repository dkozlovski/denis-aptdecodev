<?php

class Application_Model_Set extends Application_Model_Abstract
{

    protected $_productsFilter;

    /**
     * Add products to set 
     * @param mixed $products (array, id, object, collection) 
     * @return object self
     */
    public function addProducts($products)
    {
        return $this->_getbackend()->addProducts($this, $products);
    }

    /**
     * Remove products from set
     * @param mixed $products (array, id, object, collection) 
     * @return object self
     */
    public function removeProducts($products)
    {
        return $this->_getbackend()->removeProducts($this, $products);
    }

    public function removeAllProducts()
    {
        return $this->_getbackend()->removeAllProducts($this);
    }

    /**
     * Checks if product belongs to this set
     * 
     * @param  Product_Model_Product $product
     * @return 
     */
    public function hasProduct(\Product_Model_Product $product)
    {
        $filter = $this->_productsCollection()->getFlexFilter()->id_of_set('=', $this->getId());

        return (bool) $filter->id('=', $product->getId())->count();
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param object $request - to attach additional filter conditions
     * @return object Product_Model_Product_Collection
     */
    public function getProducts($limit = null, $offset = null, \Zend_Controller_Request_Http $request = null)
    {
        return $this->productsFilter($request)
                        ->apply($limit, $offset);
    }

    /**
     * Count all products in set
     * @return int 
     */
    public function getProductsCount(\Zend_Controller_Request_Http $request = null)
    {
        return $this->productsFilter($request)
                        ->count();
    }

    /**
     * @param object $request - to attach additional filter conditions
     * @param bool $new - new instance of filter 
     * @return object - filter
     */
    public function productsFilter(\Zend_Controller_Request_Http $request = null, $new = false)
    {
        if (!$this->_productsFilter || $new) {
            $this->_productsFilter = $this->_productsCollection()->getFlexFilter()
                    ->id_of_set('=', $this->getId())
                    ->is_visible('=', 1)
                    ->is_published('=', 1)
                    ->is_approved('=', 1)
                    ->save('frontend'); // save filter state         
        }
        if ($request) {
            $this->_productsFilter->restore();
            $this->_filterByRequest($this->_productsFilter, $request);
        }
        return $this->_productsFilter;
    }

    /**
     * Use request object to set filter parameters
     * 
     * @param  object Ikantam_Filter_Flex
     * @param  object Zend_Controller_Request_Http
     * @return void
     */
    protected function _filterByRequest($filter, $request)
    {
        $filter->handleRequest($request, array(
            'min_price'  => array('price' => '>=', 'and'),
            'max_price'  => array('price' => '<=', 'and'),
            'colors'     => array('color_id' => 'in'),
            'conditions' => array('condition' => 'in'),
            'categories' => array('category_id' => 'in'),
            'brands'     => array('manufacturer_id' => 'in'),
            'q'          => array('title' => function($value) {
            return array('like', '%' . str_replace('%', '', $value) . '%');
        }),
            'only_available' => array('qty' => function($value) {
            return array('>', 0);
        }),
        ));

        if ($request->getParam('only_available')) {
            $filter->available_till('=', 0)
                    ->available_till('IS_NULL')
                    ->available_till('>=', time())
                    ->expire_at('=', 0)
                    ->expire_at('>=', time());
        }

        if ($order_direction = $request->getParam('order_price')) {
            $filter->order('price', $order_direction);
        }

        if ($order_direction = $request->getParam('order_rating')) {
            $filter->order('user_rating', $order_direction);
        }

        if ($order_direction = $request->getParam('order_date')) {
            $filter->order('created_at', $order_direction);
        }

        $filter->order('qty desc');

        if (!$order_direction) {
            $filter->order('created_at desc');
        }
    }

    public function getImageUrlIfExists()
    {
        //@TODO: retrieve image url(full)

        return $this->getImagePath() ? Ikantam_Url::getPublicUrl('collections/' . $this->getImagePath()) : false;
    }

    protected function _productsCollection()
    {
        return new Product_Model_Product_Collection();
    }

    public function getProductCategories()
    {
        $categoriesC = new Category_Model_Category_Collection();

        $categories = $categoriesC->getFlexFilter()
                ->id_of_set('=', $this->getId())
                ->products_approved('=', 1)
                ->products_visible('=', 1)
                ->products_published('=', 1)
                ->group('id')
                ->apply(8);

        return $categories;
    }

    public function getCollectionUrl()
    {
        return Ikantam_Url::getUrl('catalog/collection/' . $this->getPageUrl());
    }

}