<?php

class Application_Model_Color_Collection extends Application_Model_Abstract_Collection
{

	protected $_backendClass = 'Application_Model_Color_Backend';
	private $_loaded = false;

	protected function _getBackEnd()
	{
		return new $this->_backendClass();
	}

	public function getAll()
	{
		if (!$this->_loaded) {
			$this->_getBackEnd()->getAll($this);
			$this->_loaded = true;
		}

		return $this;
	}

	public function getOptions()
	{
		$result = array();

		foreach ($this->getAll() as $color) {
			$result[$color->getId()] = $color->getTitle();
		}

		return $result;
	}
	
	public function getOptions3()
	{
		$result = array();

		foreach ($this->getAll() as $color) {
			$result[$color->getId()] = $color->getHex();
		}

		return $result;
	}
	
	
	public function getOptions2()
	{
		$result = array();

		foreach ($this->getAll() as $color) {
			$result[$color->getId()] = $color->getId();
		}

		return $result;
	}
	
	
	public function getByProductQuery($query)
	{
		$this->_getBackEnd()->getByProductQuery($this, $query);
		return $this;
	}
	
	public function getByCategory($category)
	{
		$this->_getBackEnd()->getByCategory($this, $category);
		return $this;
	}
	
	
	

}
