<?php

class Application_Model_ViewedEmail_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'viewed_emails';

    public function getAll($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $object = new Application_Model_ViewedEmail();
                $object->addData($row);
                $collection->addItem($object);
            }
        }
    }

    public function getByCode($object, $title)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `code` = :title";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':title', $title);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql = "UPDATE `" . $this->_getTable() . "` 
		   SET `code` = :code, `email` = :email, `read_at` = :read_at
		   WHERE `id` = :id";

        $stmt = $this->_getConnection()->prepare($sql);
        
        $code = $object->getCode();
        $email = $object->getEmail();
        $readAt = $object->getReadAt();
        $id = $object->getId();

        $stmt->bindParam(':code', $code);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':read_at', $readAt);
        
        
        $stmt->bindParam(':id', $id);

        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql = "INSERT INTO `" . $this->_getTable() . "` (`code`, `email`, `read_at`) 
		   VALUES (:code, :email, :read_at)";

        $stmt = $this->_getConnection()->prepare($sql);
        
        $code = $object->getCode();
        $email = $object->getEmail();
        $readAt = $object->getReadAt();

        $stmt->bindParam(':code', $code);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':read_at', $readAt);

        $stmt->execute();
        $object->setId($this->_getConnection()->lastInsertId());
    }

}
