<?php

class Application_Model_Manufacturer_FlexFilter extends Ikantam_Filter_Flex {
    
    protected $_acceptClass = 'Application_Model_Manufacturer_Collection';
    
    protected $_joins = array (
        'products' => 'INNER JOIN `products` ON `manufacturers`.`id` = `products`.`manufacturer_id`',
        'collections_products' => array('products' => 'INNER JOIN `collections_products` AS `c_p` ON `c_p`.`product_id` = `products`.`id`'),
        'set' => array('collections_products' => 'INNER JOIN `collections` AS `collection` ON `collection`.`id` = `c_p`.`collection_id`'),
    );
    
    protected $_select = array  (
    );
    
    protected $_rules = array(
        'id_of_set' => array('set' => '`collection`.`id`'),
        'products_published' => array('products' => '`products`.`is_published`'),
        'products_approved' => array('products' => '`products`.`is_approved`'),
        'products_visible' => array('products' => '`products`.`is_visible`'),
    );     
}