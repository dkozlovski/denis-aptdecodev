<?php

class Application_Model_Manufacturer_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'manufacturers';

    public function getAll($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Category_Model_Category();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getUsed(\Application_Model_Abstract_Collection $collection, $search = null, $limit = null)
    {
        $sql = "SELECT m.* FROM `" . $this->_getTable() . "` as m INNER JOIN `products` as p ON p.manufacturer_id = m.id ";
        if (is_string($search) && !empty($search)) {
            $bind = true;
            $sql.="WHERE m.title LIKE :search";
            $search.='%';
        }
        $sql.=" GROUP BY m.id";

        if ((int) $limit > 0) {
            $sql.=" LIMIT " . $limit;
        }


        $stmt = $this->_getConnection()->prepare($sql);

        if ($bind)
            $stmt->bindParam(':search', $search);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (is_array($result)) {
            foreach ($result as $row) {
                $item = new Application_Model_Manufacturer();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function findByValue($collection, $value, $limit = 5)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `title` LIKE :value LIMIT :limit";

        $stmt   = $this->_getConnection()->prepare($sql);
        $value .= '%';
        $stmt->bindParam(':value', $value);
        $stmt->bindParam(':limit', $limit);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Application_Model_Manufacturer();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function findByIds($collection, $value)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `id` in (" . implode(',', $value) . ")";

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Application_Model_Manufacturer();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getByTitle($object, $title)
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `title` = :title LIMIT 1";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':title', $title);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function getByCategories($collection, $categoriesId)
    {
        $in = '';
        foreach ($categoriesId as $id) {
            if ((int) $id > 0) {
                $in.=$id;
                if (end($categoriesId) != $id) {
                    $in.=',';
                }
            }
        }
        if ($in == '')
            $in     = '0';
        $sql    = "SELECT * FROM `" . $this->_getTable() . "` WHERE `id` IN({$in})";
        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Application_Model_Manufacturer();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    //----------------------------

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql  = 'UPDATE `' . $this->_getTable() . '` set `title` = :title WHERE `id` = :id';
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':title', $object->getTitle());
        $stmt->bindParam(':id', $object->getId());
        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql  = 'INSERT INTO `' . $this->_getTable() . '` (`id`, `title`) VALUES (NULL, :title)';
        $stmt = $this->_getConnection()->prepare($sql);

        $title = $object->getTitle();

        $al = new Manufacturer_Model_Association(trim($title));

        if ($al->isExists()) {
            $object->setData($al->getManufacturer()->getData());
            return;
        }

        $stmt->bindParam(':title', $title);
        $stmt->execute();
        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _getPartialMatch($query)
    {
        $q = explode(' ', trim($query));

        foreach ($q as $word) {
            $query_like[] = '%' . str_replace('%', '', strtolower(trim($word))) . '%';
        }

        $sql = 'SELECT DISTINCT(`manufacturers`.`id`), `manufacturers`.`title`
            FROM `products`
            JOIN `categories` ON `products`.`category_id` = `categories`.`id`
            JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
            JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
            JOIN `colors` ON `products`.`color_id` = `colors`.`id`
            JOIN `materials` ON `products`.`material_id` = `materials`.`id`
            WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1';

        $c    = 1;
        $bind = array();

        foreach ($query_like as $word) {
            $sql .= "\n" . 'AND 
            (`products`.`title` LIKE :query' . $c++ . /* '
                      OR `products`.`description` LIKE :query' . $c++ . */ '
            OR `categories`.`title` LIKE :query' . $c++ . '
            OR `conditions`.`short_title` LIKE :query' . $c++ . '
            OR `manufacturers`.`title` LIKE :query' . $c++ . '
            OR `colors`.`title` LIKE :query' . $c++ . '
            OR `materials`.`title` LIKE :query' . $c++ . ')';

            //$bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
            $bind[] = $word;
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute($bind);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getByProductQuery(Application_Model_Abstract_Collection $collection, $query)
    {
        /* $sql = 'SELECT distinct(`id`), `title` FROM `view_product_manufacturers`
          WHERE `product_title` LIKE :product_title OR `product_description` LIKE :product_description
          ORDER BY `title` ASC';

          $stmt = $this->_getConnection()->prepare($sql);

          $query = '%' . str_replace('%', '', strtolower($query)) . '%';

          $stmt->bindParam(':product_title', $query);
          $stmt->bindParam(':product_description', $query);

          $stmt->execute();
          $rows = $stmt->fetchAll(PDO::FETCH_ASSOC); */

        $rows = $this->_getPartialMatch($query);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Application_Model_Color();
                $item->setData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getByCategory(Application_Model_Abstract_Collection $collection, $category)
    {
        $sql = 'SELECT distinct(`id`), `title` FROM `view_product_manufacturers`
			WHERE `category_id` = :category_id OR `parent_id` = :parent_id
            ORDER BY `title` ASC';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':category_id', $category);
        $stmt->bindParam(':parent_id', $category);

        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Application_Model_Color();
                $item->setData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getFixedAtHome(\Application_Model_Abstract_Collection $collection)
    {
        $sql    = "SELECT `m`.*, `ma`.`alt` FROM `" . $this->_getTable() . "` as `m` INNER JOIN `manufacturers_home` as `ma` ON 
                `ma`.`manufacturer_id` = `m`.`id`";
        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new Application_Model_Manufacturer();
            $object->addData($row);
            $collection->addItem($object);
        }
    }

    public function getFirst(\Application_Model_Abstract $object)
    {
        $sql    = "SELECT * FROM `" . $this->_getTable() . "` WHERE 1 ORDER BY `title` ASC LIMIT 1";
        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (is_array($result)) {
            $object->addData($result);
        }
    }

}
