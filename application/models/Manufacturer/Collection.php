<?php

class Application_Model_Manufacturer_Collection extends Application_Model_Abstract_Collection
{

    private $_loaded           = false;
    protected $_homeBackendClass = 'Manufacturer_Model_Home_Backend';
    protected $_homeBackend      = null;

    public function getAll()
    {
        if (!$this->_loaded) {
            $this->_getBackend()->getAll($this);
        }
        return $this;
    }

    public function findByIds($value)
    {
        $this->_getBackEnd()->findByIds($this, $value);
        return $this;
    }

    public function getOptions()
    {
        $result = array();

        foreach ($this->getAll() as $manufacturer) {
            $result[$manufacturer->getId()] = $manufacturer->getTitle();
        }
        return $result;
    }

    public function getOptions2()
    {
        $result = array();

        foreach ($this->getAll() as $manufacturer) {
            $result[] = $manufacturer->getTitle();
        }
        return $result;
    }

    public function findSimilar($value, $limit = 5)
    {
        $this->_getBackEnd()->findByValue($this, $value, $limit);
        return $this;
    }

    public function getForAutocomplete($value, $limit = 10)
    {
        $this->_getBackEnd()->findByValue($this, $value, $limit);
        $result = array();
        
        foreach ($this->getItems() as $item) {
            $d        = new stdClass();
            $d->id    = $item->getId();
            $d->label = $item->getTitle();
            $d->value = $item->getTitle();
            $result[] = $d; //$item->getTitle(); 
        }

        return $result;
    }

    public function getTitles()
    {
        $result = array();
        foreach ($this->getItems() as $item) {
            $result[] = $item->getTitle();
        }
        return $result;
    }

    /**
     * 
     * @param  array categoriesId
     * @param  bool $onlyPublished  
     * @return Application_Model_Manufacturer_Collection
     *  
     */
    public function getByCategories($categoriesId, $onlyPublished = false)
    {
        if (!is_array($categoriesId))
            throw new Exception(__METHOD__ . ' Invalid parameter $categoriesId.');
        $manufacturerId = array();

        foreach ($categoriesId as $id) {
            $products = new Product_Model_Product_Collection();
            $products->getByCategory((int) $id, $onlyPublished);

            foreach ($products->getItems() as $product) {
                $manufacturerId[] = $product->getManufacturerId();
            }
        }

        $this->_getBackend()->getByCategories($this, $manufacturerId);

        return $this;
    }

    public function getByProductQuery($query)
    {
        $this->_getBackEnd()->getByProductQuery($this, $query);
        return $this;
    }

    public function getByCategory($category)
    {
        $this->_getBackEnd()->getByCategory($this, $category);
        
        $brands = array();

        foreach ($this as $brand) {
            $brands[$brand->getId()] = $brand->getTitle();
        }
        
        return $brands;
    }

    /**
     * @param integer $userId 
     * @return Application_Model_Manufacturer_Collection
     */
    public function getByUserPurchaseHistory($userId)
    {
        $user = new Application_Model_User($userId);
        $mId  = array();
        foreach ($user->getPurchaseHistory(null, 1, $user->getPurchaseCount())->getItems() as $product) {
            $mId[] = $product->getManufacturerId();
        }

        return $this->findByIds($mId);
    }

    /**
     * Retrieves all used by products brands  
     * @return self
     */
    public function getAllUsed()
    {
        $this->_getBackend()->getUsed($this);
        return $this;
    }

    /**
     * 
     * @param  string value
     * @return 
     */
    public function searchInUsedByTitle($value, $limit = null)
    {
        $this->_getBackend()->getUsed($this, $value, $limit);
        return $this;
    }

    public function getManufacturersFixedAtHomePage()
    {
        $this->clear();
        $this->_getBackend()->getFixedAtHome($this);
        return $this;
    }

    protected function _getHomeBackend()
    {
        if (is_null($this->_homeBackend)) {
            return $this->_homeBackend = new $this->_homeBackendClass();
        } elseif ($this->_homeBackend instanceof $this->_homeBackendClass) {
            return $this->_homeBackend;
        }
    }

    public function releaseAllFixedBrands()
    {
        $this->_getHomeBackend()->deleteAll();
    }

    /**
     * @param array $ids - optional
     * @return self
     */
    public function fixAtHomepage($ids = null)
    {
        if (is_null($ids))
            $ids = $this->getColumn('id');
        $this->_getHomeBackend()->fixMultiple($ids);
        return $this;
    }

    public function getFlexFilter()
    {
        return new Application_Model_Manufacturer_FlexFilter($this);
    }

}
