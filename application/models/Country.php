<?php

/**
 * @method string getName()
 * @method string getCode()
 * @method int getSortOrder()
 */
class Application_Model_Country extends Application_Model_Abstract
{
	public function getByCode($countryCode)
	{
		$this->_getBackend()->getByCode($this, $countryCode);
		return $this;
	}
    
    public function getByName($countryName)
	{
		$this->_getBackend()->getByName($this, $countryName);
		return $this;
	}
	
	
}
