<?php

class Application_Model_Manufacturer extends Application_Model_Abstract
{
	protected $_backendClass = 'Application_Model_Manufacturer_Backend';
    protected $_homeBackendClass = 'Manufacturer_Model_Home_Backend';
    protected $_homeBackend = null;
    protected $_homeImageDir = 'upload/homepage/';    
    
        public function __construct($id = null) 
    {        

        if($id)
        {
            $this->getById($id);
        }
        
        return $this;
    }
    
    public function getByTitle ($title)
    {
        $this->_getbackend()->getByTitle($this,$title); 
        return $this;    
    }
    
    protected function _getHomeBackend()
    {
        if(is_null($this->_homeBackend))
        {
           return $this->_homeBackend = new $this->_homeBackendClass();
        } elseif($this->_homeBackend instanceof $this->_homeBackendClass)
            {
                return  $this->_homeBackend;
            }        
    }    
    
    	/**
         * Fix category at home page
         * @param  string $imagePath 
         * @return self
         */
    public function fixAtHomepage ($imageName = null)
    { 
        $hbe = $this->_getHomeBackend();
        if(is_null($imageName)) $imageName = $this->getTmpFileName();
        
        if($this->getId())
        { 
            try
            {            
            $hbe->addManufacturer($this, $imageName);
            }catch(Exception $e)
                {
                    if($e->getCode() != 23000 ) throw $e;//SQLSTATE[23000]: Duplicate entry                                       
                    
                }
        }
        
        return $this;
    }
    
    public function removeFromHomePage ()
    {
       if($this->getId())
       {
            $this->_getHomeBackend()->delete($this);
       }
       
       return $this; 
    } 
    
    	/**
         * Return path to image. 
         * @return mixed - string if there was provided| null if path was not provided| false if category not fixed at homepage
         */
    public function getHomeImageName()
    {
       $path = false;
       if($this->getId())
       {
       $path = $this->_getHomeBackend()->getImagePath($this);
       }
       
       return $path; 
    }
    
    
    protected function getHomeImageDirPath()
    {
    return realpath(APPLICATION_PATH . '/../public/'.$this->_homeImageDir.'/');
    }
    public function getHomeImageRealPath ()
    {
    return realpath($this->getHomeImageDirPath().'/'.$this->getHomeImageName());
    }
    
    
    public function getHomeImageUrl ()
    {
        if($path = $this->getHomeImageName()) {
            $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;
            $path = $cloudFrontUrl . '/homepage/' . $path;
        }
        return $path;
    }       
    
    public function getManufacturerUrl()
    {
        return Ikantam_Url::getUrl('catalog/brand/index', array('id' => $this->getId()));
    }
    
    public static function getFirst ()
    {
        $obj = new self();
        $obj->_getbackend()->getFirst($obj);
        return $obj;
    }
    
    
}
