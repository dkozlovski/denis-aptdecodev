<?php

class Application_Model_Blog_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'blog_home';

    public function addBlogPost($slug)
    {
        $object = new Application_Model_Abstract();
        $object->setBlogPostSlug($slug);

        if ($this->isExists()) {
            $this->_update($object);
        } else {
            $this->_insert($object);
        }
    }

    public function delete($blogPostSlug)
    {
        $sql = "DELETE FROM `" . $this->_getTable() . "` WHERE `blog_post_slug` = :blog_post_slug LIMIT 1";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':blog_post_slug', $blogPostSlug);

        $stmt->execute();
    }

    public function removeBackground()
    {
        $sql = "UPDATE `" . $this->_getTable() . "` SET `image_path` = NULL";
        $this->_getConnection()->prepare($sql)->execute();
    }

    /**
     * @param string $imageName
     * @param Application_Model_BlogBackground $object - optional   
     */
    public function changeBackground($imageName, $object = null)
    {
        $update = "UPDATE `" . $this->_getTable() . "` SET `image_path` = :img";
        $insert = "INSERT INTO `" . $this->_getTable() . "` (`blog_post_slug`, `image_path`) VALUES (NULL, :img)";
        $sql    = ($this->isExists()) ? $update : $insert;
        $stmt   = $this->_getConnection()->prepare($sql);



        $stmt->bindParam(':img', $imageName);

        $execute_result = $stmt->execute();

        if ($object instanceof Application_Model_BlogBackground) {
            $object->setInsertedImage($imageName);
        }
    }

    public function getBackgroundName()
    {
        $sql  = "SELECT `image_path` FROM `" . $this->_getTable() . "` WHERE 1 LIMIT 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_COLUMN);
    }

    protected function isExists()
    {
        $sql  = "SELECT `id` FROM `" . $this->_getTable() . "` WHERE 1";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $dbh = $this->_getConnection();

        $sql = "UPDATE `" . $this->_getTable() . "` SET `blog_post_slug` = :blog_post_slug";

        $stmt = $dbh->prepare($sql);
        
        $blogPostSlug = $object->getBlogPostSlug();

        $stmt->bindParam(':blog_post_slug', $blogPostSlug);

        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $dbh = $this->_getConnection();

        $sql = "INSERT INTO `" . $this->_getTable() . "` (`id`, `blog_post_slug`) VALUES (NULL, :blog_post_slug)";

        $stmt = $dbh->prepare($sql);
        
        $blogPostSlug = $object->getBlogPostSlug();
        
        $stmt->bindParam(':blog_post_slug', $blogPostSlug);

        $stmt->execute();
    }
    
    public function getFixedAtHomeBlogPost()
    {
        $sql = "SELECT * FROM `" . $this->_getTable() . "`";

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            return $result['blog_post_slug'];
        }
    }

}