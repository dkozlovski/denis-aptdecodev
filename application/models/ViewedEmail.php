<?php

class Application_Model_ViewedEmail extends Application_Model_Abstract
{

    protected $_backendClass = 'Application_Model_ViewedEmail_Backend';

    public function __construct($id = null)
    {
        if ($id) {
            $this->getById($id);
        }

        return $this;
    }

    public function getByCode($title)
    {
        $this->_getbackend()->getByCode($this, $title);
        return $this;
    }

}
