<?php

/**
 * @method Application_Model_AppSettings_FlexFilter id (string $operator, mixed $value)
 * @method Application_Model_AppSettings_FlexFilter or_id (string $operator, mixed $value)
 * @method Application_Model_AppSettings_FlexFilter name (string $operator, mixed $value)
 * @method Application_Model_AppSettings_FlexFilter or_name (string $operator, mixed $value)
 * @method Application_Model_AppSettings_FlexFilter description (string $operator, mixed $value)
 * @method Application_Model_AppSettings_FlexFilter or_description (string $operator, mixed $value)
 * @method Application_Model_AppSettings_Collection apply(int $limit = null, int $offset = null)
 */
class Application_Model_AppSettings_FlexFilter extends Ikantam_Filter_Flex
{

    protected $_acceptClass = 'Application_Model_AppSettings_Collection';

}