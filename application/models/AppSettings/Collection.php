<?php

class Application_Model_AppSettings_Collection extends Application_Model_Abstract_Collection implements Ikantam_Filter_Flex_Interface
{

    protected $_itemObjectClass = 'Application_Model_AppSettings';

    public function getFlexFilter()
    {
        return new Application_Model_AppSettings_FlexFilter($this);
    }

}