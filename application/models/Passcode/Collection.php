<?php

class Application_Model_Passcode_Collection extends Application_Model_Abstract_Collection
{

	private $_loaded = false;

	public function getAll()
	{
		if (!$this->_loaded) {
			$this->_getBackend()->getAll($this);
		}

		return $this;
	}

	public function getOptions()
	{
		$result = array();

		foreach ($this->getAll() as $material) {
			$result[$material->getId()] = $material->getId();
		}

		return $result;
	}

}
