<?php

class Application_Model_Passcode_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'passcodes';

	public function getById(Application_Model_Abstract $object, $id)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `id` = :id';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($row) {
			$object->addData($row);
		}
	}
    
    public function getAll($object)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '`';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if ($rows) {
            foreach ($rows as $row) {
                $item = new Application_Model_Passcode();
                $item->addData($row);
                $object->addItem($item);
            }
		}
	}
    
    
	protected function _update(\Application_Model_Abstract $object)
	{
		
	}

	protected function _insert(\Application_Model_Abstract $object)
	{
		
	}

}
