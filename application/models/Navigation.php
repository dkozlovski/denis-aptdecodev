<?php

class Application_Model_Navigation extends Application_Model_HtmlBuilder
{

    protected $_controllerFront = null;
    protected $_request         = null;

    public function __construct()
    {

        $this->_controllerFront = Zend_Controller_Front::getInstance();
        $this->_request         = $this->_controllerFront->getRequest();
    }

    /**
     * Create html code
     * 
     * @param array  $menuData 
     * @param array  $options            
     * @param string $tagWrapper
     * @param string $tagItem
     * @param string $tagLink 
     * 
     * @return string
     * 
     * @example
     * $menuData = array(array('module'=>'M','controller'=>'C','action'=>'A','params'=>array('id'=>1)));
     * $wrapperAttributes  = array('id'=>'ID');
     * $itemAttributes = array('class'=>'className');
     * $activeLinkAttributes = array('class'=>'curent');
     * $navigation->createMenu($menuData,
     * array('wrapper'=>$wrapperAttributes,'item'=>$itemAttributes,'active'=>$activeLinkAttributes);
     */
    public function createMenu($menuData, $options = null, $tagWrapper = 'ul', $tagItem = 'li', $tagLink = 'a')
    {
        $wrapperAttr = array();
        $itemAttr    = array();
        $html        = '';
        //-------------------------------------
        if (is_array($options)) {

            if (isset($options['wrapper']) && is_array($options['wrapper'])) {
                foreach ($options['wrapper'] as $key => $value) {
                    $wrapperAttr[$key] = $value;
                }
            }

            if (isset($options['item']) && is_array($options['item'])) {
                foreach ($options['item'] as $key => $value) {
                    $itemAttr[$key] = $value;
                }
            }
        }

//----------------------<<<Options 
        /* $currentPage = $this->_request->getModuleName().'/'.$this->_request->getControllerName().'/'.$this->_request->getActionName();
          $currentPage = Ikantam_Url::getUrl($currentPage,$this->_request->getParams()); */
        $currentPage = Ikantam_Url::getUrl($_SERVER['REQUEST_URI']);


        foreach ($menuData as $menuItem) {
            $module     = isset($menuItem['module']) ? $menuItem['module'] . '/' : '';
            $controller = isset($menuItem['controller']) ? $menuItem['controller'] . '/' : '';
            $action     = isset($menuItem['action']) ? $menuItem['action'] . '/' : '';
            $params     = isset($menuItem['params']) ? $menuItem['params'] : null;

            $uri = $module . $controller . $action;
            $url = Ikantam_Url::getUrl($uri, $params);


            if ($url == $currentPage) {
                if (is_array($options['active'])) {
                    foreach ($options['active'] as $attr => $value) {
                        $menuItem['attributes'][$attr] = $value;
                    }
                }
            }

            $url = Ikantam_Url::getUrl($uri, $params, true);


            $itemHtml                       = $menuItem['label'];
            $tagLink                        = isset($menuItem['tag']) ? $menuItem['tag'] : $tagLink;
            if ($tagLink === 'a')
                $menuItem['attributes']['href'] = $url;

            /* 		  // $attributes = isset($menuItem['attributes']) ? : array();

              $this->wrap($itemHtml,$tagLink,$menuItem['attributes'])
              ->wrap($itemHtml,$tagItem,$itemAttr); */
            $attributes = isset($menuItem['attributes']) ? $menuItem['attributes'] : NULL;
            $this->wrap($itemHtml, $tagLink, $attributes)
                    ->wrap($itemHtml, $tagItem, $itemAttr);
            $html.=$itemHtml;
        }


        $this->wrap($html, $tagWrapper, $wrapperAttr);

        return $html;
    }

    public function getRequest()
    {
        return $this->_request;
    }

}
