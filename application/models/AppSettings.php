<?php
class Application_Model_AppSettings extends Application_Model_Abstract
{
    public function getCollection ()
    {
        return new Application_Model_AppSettings_Collection();
    } 
        
    /**
     * Get item
     * 
     * @param array $conditions  
     * @return mixed
     */
    public function getByCondition (array $conditions)
    {
        $filter = $this->getCollection()->getFlexFilter();
        foreach ($conditions as $method => $args) {
            call_user_func_array(array($filter, $method), $args);
        }
        
        $this->setData($filter->order('id asc')->apply(1)->getFirstItem()->getData());        
        return $this;
    }    
}