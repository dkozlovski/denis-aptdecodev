<?php

/**
 *    Application_Model_Static class
 *    Used to get content from WordPress for static pages 
 *    (ex. About Us, Blog, FAQ, How It Works and etc.)
 *    @method Application_Model_Static_Backend _getBackend()
 *    @author Alex Polevoy
 *    @created 31.01.2013 
 */
class Application_Model_Static extends Application_Model_Abstract
{

    protected $_backendClass = 'Application_Model_Static_Backend';

    /**
     * Used to call getContent from backend model
     * 
     * @access public
     * @return array
     */
    public function getContent($post_name)
    {
        $content = $this->_getBackend()->getContent($post_name);
        return $content;
    }

    public function getPostTags($postId)
    {
        return $this->_getBackend()->getPostTags($postId);
    }

    /**
     * Used to call getContent from backend model
     * Called by Blog controller (show blog posts)
     * 
     * @access public
     * @return array
     */
    public function getBlogPosts($categorySlug = null, $postName = null, $offset = null, $limit = null)
    {
        $posts = $this->_getBackend()->getBlogPosts($categorySlug, $postName, $offset, $limit);
        return $posts;
    }

    public function getCategoryInfo($categorySlug = null)
    {
        return $this->_getBackend()->getCategoryInfo($categorySlug);
    }
    
    public function countPosts()
    {
        return $this->_getBackend()->countPosts();
    }

    /**
     * Used to call getFaqPosts from backend model
     * Called by FAQ controller show list of FAQ 
     * As Question Title ---- Answer 
     * 
     * @access public
     * @return array
     */
    public function getFaqPosts()
    {
        return $this->_getBackend()->getFaqPosts();
    }

    public function getFaqSidebarPosts()
    {
        return $this->_getBackend()->getFaqSidebarPosts();
    }

    public function getJobsPosts($postId = null)
    {
        return $this->_getBackend()->getJobsPosts($postId);
    }

    public function getCategories()
    {
        return $this->_getBackend()->getCategories();
    }
    
    public function getLatestPost()
    {
        return $this->_getBackend()->getLatestPost();
    }
    
    public function getFixedAtHomeBlogPost()
    {
        $b = new Application_Model_Blog_Backend();
        
        return $b->getFixedAtHomeBlogPost();
    }

    public function getPage($postName)
    {
        return $this->_getBackend()->getPage($postName);
    }

}