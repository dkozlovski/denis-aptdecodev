<?php

class Application_Model_Invitation_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'invitations';

	public function getByCode($invitation, $code)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `code` = :code LIMIT 1';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':code', $code);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			$invitation->addData($row);
		}
	}
    
    public function getByEmail($invitation, $code)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `email` = :email AND `is_used` = 0 LIMIT 1';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':email', $code);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row) {
			$invitation->addData($row);
		}
	}
    
    public function getForRemind (Application_Model_Abstract_Collection $collection, $limit = 10)
    {
        $fiveDaysAgoTS = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 86400 * 5;
        $sql = 'SELECT * FROM `'. $this->_getTable() .'` WHERE `created_at` < '.$fiveDaysAgoTS.' AND `is_used` = 0 AND `is_reminded` = 0 LIMIT '.$limit;
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        if(is_array($result))
        {
            foreach($result as $data)
            {
                $object = new Application_Model_Invitation($data);
                $collection->addItem($object);
            }
        }
        
    }
    
    public function markAsReminded (Application_Model_Abstract_Collection $collection)
    {
        $ids = $collection->getColumn('id');
        if (!count($ids)) {
            return;
        }
        $in = implode(',', $ids);
        
        $sql = "UPDATE `" . $this->_getTable() . "` set `is_reminded` = 1 WHERE `id` IN({$in})";
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();
                
       /* $fiveDaysAgoTS = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 86400 * 5;
        $sql = 'UPDATE `'. $this->_getTable() .'` SET `is_reminded` = 1 WHERE `created_at` < '.$fiveDaysAgoTS.' AND `is_used` = 0 AND `is_reminded` = 0';
        $stmt = $this->_getConnection()->prepare($sql);
        
        $stmt->execute(); */
    }

	protected function _update(Application_Model_Abstract $object)
	{
		$sql = "UPDATE `" . $this->_getTable() . "` set `code` = :code, `email` = :email, `created_at` = :created_at, `is_used` = :is_used, `is_reminded` = :is_reminded WHERE `id` = :id;";

		$stmt = $this->_getConnection()->prepare($sql);

		$id = $object->getId();
		$code = $object->getCode();
		$email = $object->getEmail();
		$createdAt = $object->getCreatedAt();
		$isUsed = $object->getIsUsed();
        $isReminded = $object->getIsReminded(0);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':code', $code);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':created_at', $createdAt);
		$stmt->bindParam(':is_used', $isUsed);
        $stmt->bindParam(':is_reminded', $isReminded);

		$stmt->execute();
	}

	protected function _insert(Application_Model_Abstract $object)
	{
		$sql = "INSERT INTO `" . $this->_getTable() . "` (`code`, `email`, `created_at`, `is_used`) VALUES (:code, :email, :created_at, :is_used);";

		$stmt = $this->_getConnection()->prepare($sql);

		$code = $object->getCode();
		$email = $object->getEmail();
		$createdAt = $object->getCreatedAt();
		$isUsed = $object->getIsUsed();

		$stmt->bindParam(':code', $code);
		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':created_at', $createdAt);
		$stmt->bindParam(':is_used', $isUsed);

		$stmt->execute();
	}
        
        public function createInvitationAdmin($data){

            $sql = 'INSERT INTO `invitation_admins` (`admin_id`, `email`, `code`, `is_used`)'.
                    ' values (:admin, :email, :code, :is_used)';
            $stmt = $this->_getConnection()->prepare($sql);
            $stmt->bindParam(':admin', $data['admin_id']);
            $stmt->bindParam(':email', $data['email']);
            $stmt->bindParam(':code', $data['code']);
            $stmt->bindParam(':is_used', $data['is_used']);
            $stmt->execute();
        }
        
        public function getInvitationAdmin($code){
            
            $sql = 'SELECT * FROM `invitation_admins` WHERE `code` = :code';
            
            $stmt = $this->_getConnection()->prepare($sql);
            $stmt->bindParam(':code', $code);
            $stmt->execute();
            
            return $stmt->fetch();
            
        }
        
        public function confirmInvitationAdmin($code){

            $sql = 'UPDATE `invitation_admins` SET `is_used` = 1 WHERE `code` = :code';
            
            $stmt = $this->_getConnection()->prepare($sql);
            $stmt->bindParam(':code', $code);
            $stmt->execute();
            
        }

}
