<?php

class Application_Model_Invitation_Request_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'invitation_requests';


	protected function _update(Application_Model_Abstract $object)
	{
		
	}

	protected function _insert(Application_Model_Abstract $object)
	{
		$sql = "INSERT INTO `" . $this->_getTable() . "` (`email`, `created_at`) 
			VALUES (:email, :created_at)";

		$stmt = $this->_getConnection()->prepare($sql);

		$email = $object->getEmail();
		$createdAt = $object->getCreatedAt();

		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':created_at', $createdAt);

		$stmt->execute();
	}

}
