<?php

class Application_Model_Invitation_Collection extends Application_Model_Abstract_Collection
{
    
    public function getForRemind ($limit = 10)
    {        
        $this->_getBackend()->getForRemind($this, $limit);
        return $this;
    }
    
    public function markAsReminded ()
    {
        $this->_getBackend()->markAsReminded($this);
        $this->fillColumn('is_reminded', 1);
        return $this;
    }


}
