<?php
/**
 * Created by PhpStorm.
 * User: FleX
 * Date: 18.06.14
 * Time: 17:51
 */

class Application_Model_ValueExtractor implements Application_Interface_ObjectStorage_SymbolTable
{
    /**
     * Method use to extract value
     * @var mixed
     */
    protected $method;

    /**
     * Arguments that will be passed to specified method
     * @var array
     */
    protected $arguments = array();

    /**
     * Implementation of \Application_Interface_ObjectStorage_SymbolTable
     * @var
     */
    protected $_symbolTableName;

    /**
     * Cache for expensive calls
     * @var array
     */
    protected static $cache = array();

    /**
     * Creates rules which will retrieve the value.
     * @param string $params - sting that will be exploded to particles where first part is method name
     *  and other next is arguments which will be passed to method. Delimiter is |
     * @throws InvalidArgumentException
     *
     * for instance to run method "foo" with params (10, 'bar') you must pass string like foo|10|bar
     */
    public function __construct($params)
    {
        if (!is_string($params)) {
            throw new \InvalidArgumentException('Invalid parameter passed into constructor');
        }

        $data = explode('|' ,$params);
        $this->method = array_shift($data);
        if (!method_exists($this, $this->method)) {
            throw new \InvalidArgumentException('Method that you specified is not exists.');
        }
        $this->arguments = $data;
    }

    /**
     * Identifies extractor
     * Used in \Application_Model_ObjectStorage
     * @return string
     */
    public function getId()
    {
        return $this->method . '|' . implode('|', $this->arguments);
    }

    /**
     * Retrieves value
     * @return mixed
     */
    public function extract()
    {
        return call_user_func_array(array($this, $this->method), $this->arguments);
    }

    /**
     * Sets name that will be used for current symbol table
     * @param string $name
     * @throws InvalidArgumentException
     * @return mixed
     */
    public function setSymbolTableName($name)
    {
        //http://www.php.net/manual/en/language.variables.basics.php
        if (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $name)) {
            throw new InvalidArgumentException('Invalid variable name.');
        }
        $this->_symbolTableName = $name;
    }

    /**
     * Retrieve name to current symbol table
     * @return mixed
     */
    public function getSymbolTableName()
    {
        return $this->_symbolTableName;
    }

    /**
     * Retrieves value from \Admin_Model_Settings
     * @param $settingName
     * @param $parameterName
     * @return mixed|null
     */
    protected function adminEAVSetting($settingName, $parameterName)
    {
        $settings = new Admin_Model_Settings;
        $key = crc32($settingName . $parameterName . __METHOD__);
        if (!array_key_exists($key, self::$cache)) {
            self::$cache[$key] = $settings->getValue($settingName, $parameterName);

        }

        return self::$cache[$key];
    }

    /**
     * Return passed value (It's wrapper for ObjectStorage)
     * @param mixed $value
     * @return mixed
     */
    protected function simpleValue($value)
    {
        return $value;
    }
}