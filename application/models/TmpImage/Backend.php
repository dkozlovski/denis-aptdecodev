<?php

class Application_Model_TmpImage_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'tmp_images';

	public function getAll($collection)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($result as $row) {
			$object = new Application_Model_Color();
			$object->addData($row);

			$collection->addItem($object);
		}
	}

	public function getByTitle($object, $title)
	{
		$sql = "SELECT * FROM `" . $this->_getTable() . "` WHERE `title` = :title LIMIT 1";
		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->bindParam(':title', $title);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if ($result[0]) {
			$object->addData($result[0]);
		} else {
			$object->setTitle($title);
		}
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		$sql = "UPDATE `" . $this->_getTable() . "` set `user_id` = :user_id, `path` = :path, `created_at` = :created_at WHERE `id` = :id";

		$stmt = $this->_getConnection()->prepare($sql);
		
		$stmt->bindParam(':user_id', $object->getUserId());
		$stmt->bindParam(':path', $object->getPath());
		$stmt->bindParam(':created_at', $object->getCreatedAt());
		$stmt->bindParam(':id', $object->getId());

		$stmt->execute();
	}

	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = "INSERT INTO `" . $this->_getTable() . "` (`user_id`, `path`, `created_at`) VALUES (:user_id, :path, :created_at)";

		$stmt = $this->_getConnection()->prepare($sql);
		
		$userId = $object->getUserId();
		$path = $object->getPath();
		$createdAt = $object->getCreatedAt();

		
		$stmt->bindParam(':user_id', $userId);
		$stmt->bindParam(':path', $path);
		$stmt->bindParam(':created_at', $createdAt);

		$stmt->execute();
		
		$object->setId($this->_getConnection()->lastInsertId());
	}

}
