<?php

/**
 * Class Application_Model_Referer
 * @method Application_Model_Referer_Backend _getBackend()
 * @method int getOrderId()
 * @method Application_Model_Referer setOrderId(int $orderId)
 */
class Application_Model_Referer extends Application_Model_Abstract
{

    public function findReferer($order)
    {
        $referer = $this->_findRefererPrevValue($order);
        
        if (!$referer) {
            $referer = $this->_findRefererFromCookie();
        }
        
        $this->setReferer($referer);
        return $referer;
    }

    public function getReferersSelect($filters = array())
    {
        return $this->_getBackend()->getReferersSelect($filters);
    }

    private function _findRefererFromCookie()
    {
        if (isset($_COOKIE['first_referer'])) {
            return ($_COOKIE['first_referer'] != '(direct)') ? $_COOKIE['first_referer'] : false;
        }
        /*
          // $_COOKIE['__utmz'] - google analytics cookie
          if (isset($_COOKIE['__utmz'])) {
          list($domainHash, $timestamp, $sessionNumber, $campaignNumer, $campaignData) = preg_split('[\.]', $_COOKIE['__utmz'], 5);
          parse_str(strtr($campaignData, '|', '&'));
          return (isset($utmcsr) && $utmcsr != '(direct)') ? $utmcsr : false;
          }
         */
        return false;
    }

    private function _findRefererPrevValue($order)
    {
        return $this->_getBackend()->findRefererPrevValue($order);
    }

}
