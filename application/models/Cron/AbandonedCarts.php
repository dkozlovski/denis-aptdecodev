<?php

class Application_Model_Cron_AbandonedCarts
{

    public function doCron()
    {
        $settings = new Admin_Model_Settings();

        for ($i = 1; $i <= 3; $i++) {
            $configTime = $settings->getValue('abandoned_cart' . $i, 'hours');
            $time    = time() - $configTime  * 3600;
            $text    = $settings->getValue('abandoned_cart' . $i, 'text');
            $subject = $settings->getValue('abandoned_cart' . $i, 'subject');

            if (empty($configTime) || empty($subject) || empty($text)) {
                continue;
            }
            
            $collection = new Cart_Model_Item_Collection();
            $collection->getAbandoned($time, $i);

            foreach ($collection as $cart) {
                if (!$cart->getUser()->getMainEmail()) {
                    $cart->setIsNotified(3)->save();
                    continue;
                }

                $this->sendEmails($cart, $text, $subject);
                $cart->setIsNotified($i)->save();

                $userNotified = new Cart_Model_Cart_UserNotified();
                $userNotified->getByUserId($cart->getUserId());
                if (!$userNotified->getUserId()) {
                    $userNotified->setUserId($cart->getUserId());
                }
                $userNotified->setData('last_send_date' . $i, time())
                    ->save();
            }
        }
    }

    protected function sendEmails($cart, $text, $subject)
    {
        $buyer = $cart->getUser();

        if (!$buyer->getMainEmail()) {
            return;
        }

        $this->view = new Zend_View();
        $this->view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $this->view->setScriptPath(realpath(APPLICATION_PATH . '/views/scripts'));

        if ($buyer->getMainEmail()) {
            $replaces     = array('%first_name%');
            $replacements = array($buyer->getFirstName());

            $this->view->content = str_replace($replaces, $replacements, $text);

            $output = $this->view->render('email_notification_abandoned_cart.phtml');

            $toSend = array(
                'email'   => $buyer->getMainEmail(),
                'subject' => $subject,
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');

            try {
                $mail->sendCopy($output);
                $mail->send();
            } catch (Exception $e) {
                
            }
        }
    }

}