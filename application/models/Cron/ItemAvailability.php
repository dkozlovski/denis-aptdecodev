<?php

class Application_Model_Cron_ItemAvailability
{

    public function doCron()
    {
        $collection = new Order_Model_Item_Collection();
        $collection->getForAvailabilityCron();

        foreach ($collection as $item) {
            $item->setIsAvailable(0)
                    ->setIsProductAvailable(0)
                    ->setStatus(Order_Model_Order::ORDER_STATUS_CANCELED)->save();

            
            if ($item->getProduct()->getIsSold()) {
                $item->getProduct()->setIsPublished(0)->save();
            }
            
            $payout = new Order_Model_Payout();
            $payout->getByItemId($item->getId());

            $mp      = new Ikantam_Balanced_Marketplace();
            $success = $mp->voidHold($item->getHoldUri());

            if ($success && $payout->getId()) {
                $payout->setIsActive(0)->save();
                $this->sendNotAvailEmail($item);
            } else {
                //@TODO: error text required
            }
        }
    }

    protected function sendNotAvailEmail($item)
    {
        $buyer = $item->getOrder()->getUser();

        if (!$buyer->getMainEmail()) {
            return;
        }

        $this->view = new Zend_View();
        $this->view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $this->view->setScriptPath(realpath(APPLICATION_PATH . '/modules/user/views/scripts'));

        $this->view->product = $item->getProduct();
        $this->view->item    = $item;
        $this->view->buyer   = $buyer;

        $output = $this->view->render('order_notification_seller_does_not_respond_to_buyer.phtml');

        $toSend = array(
            'email'   => $buyer->getMainEmail(),
            'subject' => 'Purchase Request Cancelled - ' . $item->getProduct()->getTitle(),
            'body'    => $output);

        $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
        $mail->sendCopy($output);
        $mail->send();
    }

}