<?php

class Application_Model_Cron_AbandonedWishlist extends Application_Model_Cron_AbandonedCarts
{

    public function doCron()
    {
        $settings = new Admin_Model_Settings();

        for ($i = 1; $i <= 3; $i++) {
            $configTime =  $settings->getValue('abandoned_wishlist' . $i, 'hours');
            $time    = time() - $configTime * 3600;
            $text    = $settings->getValue('abandoned_wishlist' . $i, 'text');
            $subject = $settings->getValue('abandoned_wishlist' . $i, 'subject');

            if (empty($configTime) || empty($subject) || empty($text)) {
                continue;
            }

            $collection = new Wishlist_Model_Wishlist_Item_Collection();
            $collection->getAbandoned($time, $i);

            foreach ($collection as $_item) {
                /* @var $_item Wishlist_Model_Wishlist_Item */
                $_item->setIsNotifiedForUserId($i, $_item->getUserId());
                if ($_item->getUser()->getMainEmail()) {
                    $this->sendEmails($_item, $text, $subject);
                }

                $userNotified = new Wishlist_Model_Wishlist_UserNotified();
                $userNotified->getByUserId($_item->getUserId());
                if (!$userNotified->getUserId()) {
                    $userNotified->setUserId($_item->getUserId());
                }
                $userNotified->setData('last_send_date' . $i, time())
                    ->save();
            }
        }
    }

}