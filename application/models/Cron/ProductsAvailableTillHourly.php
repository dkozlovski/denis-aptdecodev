<?php

class Application_Model_Cron_ProductsAvailableTillHourly
{

    protected $tomorrow       = null;
    protected $afterThreeDays = null;

    public function doCron()
    {
        $dbh = Application_Model_DbFactory::getFactory()->getConnection();

        $sql1  = 'SELECT * FROM `products_available_till` WHERE is_active = 1 LIMIT 10';
        $stmt1 = $dbh->prepare($sql1);
        $stmt1->execute();
        $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);

        if ($rows1) {
            foreach ($rows1 as $row) {
                $productId = $row['product_id'];

                $sql2  = 'UPDATE `products_available_till` SET `is_active` = 0 WHERE `id` = :id';
                $stmt2 = $dbh->prepare($sql2);
                $stmt2->bindParam(':id', $row['id']);
                $stmt2->execute();

                $product = new Product_Model_Product($productId);
                if ($product->getId()) {
                    $daysLeft = (int) $row['days_left'];

                    $date = strtotime(date('Y-m-d')) + (($daysLeft + 1) * 24 * 60 * 60);

                    if ((int) $product->getAvailableTill() > $date) {
                        continue;
                    }

                    $this->sendEmail($product, $daysLeft);
                }
            }
        }
    }

    protected function sendEmail($product, $daysLeft)
    {
        $view = new Zend_View();
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/views/scripts'));

        $user            = $product->getSeller();
        $view->user      = $user;
        $view->product   = $product;
        $view->startDate = $product->getAvailableFrom() ? $product->getAvailableFrom() : $product->getCreatedAt();
        $view->endDate   = $product->getAvailableTill();

        if ($daysLeft == 1) {
            $template = 'email_reminder_about_available_till_1_day_before.phtml';
            $subject  = 'Final Reminder: Your Sale Window is Expiring in 1 day - ' . $product->getTitle();
        } else {
            $template = 'email_reminder_about_available_till_3_days_before.phtml';
            $subject  = 'Your Furniture Listing on AptDeco.com is About to Expire';
        }


        $body = $view->render($template);

        $toSend = array(
            'email'   => $user->getMainEmail(),
            'subject' => $subject,
            'body'    => $body
        );

        $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
        try {
            $mail->sendCopy($body);
            $mail->send();
        } catch (Exception $exc) {
            var_dump($exc->getMessage());
            var_dump($user->getMainEmail());
        }
    }

}