<?php

class Application_Model_Cron_AfterDelivery
{

    public function doCron()
    {
        $collection = new Order_Model_Item_Collection();
        $collection->getForAfterDeliveryCron();

        $count = 0;
        foreach ($collection as $item) {
            $sellerDate = $item->getSellerDates();

            if (!$sellerDate) {
                continue;
            }

            $timeOfDelivery = strtotime(substr($sellerDate, 0, 10));

            if (!$timeOfDelivery) {
                continue;
            }

            if (($timeOfDelivery + (24 * 3600)) != strtotime(date('Y-m-d'))) {
                continue;
            }

            $count++;

            if ($count > 10) {
                break;
            }
            $item->setIsNotified(2)->save();
            $this->sendEmails($item);
        }
    }

    protected function sendEmails($item)
    {
        $buyer  = $item->getOrder()->getUser();

        if (!$buyer->getMainEmail()) {
            return;
        }

        $this->view = new Zend_View();
        $this->view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $this->view->setScriptPath(realpath(APPLICATION_PATH . '/views/scripts'));

        $this->view->product = $item->getProduct();
        $this->view->item    = $item;

        $prodTitle = $item->getProduct()->getTitle();

        if ($buyer->getMainEmail()) {
            $this->view->user      = $buyer;

            $output = $this->view->render('email_notification_after_delivery.phtml');

            $toSend = array(
                'email'   => $buyer->getMainEmail(),
                'subject' => sprintf("Subject: Confirm Delivery of Your %s", $prodTitle),
                'body'    => $output);

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');

            try {
                $mail->sendCopy($output);
                $mail->send();
            } catch (Exception $e) {
                
            }
        }
    }

}