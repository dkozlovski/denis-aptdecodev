<?php

class Application_Model_Cron_BeforeDelivery
{

    public function doCron()
    {
        $collection = new Order_Model_Item_Collection();
        $collection->getForBeforeDeliveryCron();

        $count = 0;
        foreach ($collection as $item) {
            $sellerDate = $item->getSellerDates();

            if (!$sellerDate) {
                continue;
            }

            $timeOfDelivery = strtotime(substr($sellerDate, 0, 10));

            if (!$timeOfDelivery) {
                continue;
            }

            if (($timeOfDelivery - (24 * 3600)) != strtotime(date('Y-m-d'))) {
                continue;
            }

            $count++;

            if ($count > 10) {
                break;
            }
            $item->setIsNotified(1)->save();

            $mailer = new Notification_Model_Order_BeforeDelivery();
            $mailer->sendEmails($item);
        }
    }

}