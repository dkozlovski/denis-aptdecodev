<?php
/**
 * Author: Alex P.
 * Date: 02.09.14
 * Time: 16:19
 */


use Core\GoogleShoppingContent\Delayed;
use Core\Service\GoogleShoppingContent;

class Application_Model_Cron_GoogleShoppingContent
{
    /**
     * @var Delayed
     */
    private $delayed;

    /**
     * @var GoogleShoppingContent
     */
    private $gsc;

    /**
     * Init
     */
    public function __construct()
    {
        $c = $this->getContainer();
        $this->delayed = $c->get('core.GoogleShoppingContent.Delayed');
        $this->gsc = $c->get('core.service.GoogleShoppingContent');
    }

    public function doCron()
    {
        $updateProducts = $this->delayed->getUpdateProducts();

        // Update modifies collection (filter). In order to clear all "delayed update" markers - clone it.
        $this->gsc->updateProductBatch(clone $updateProducts);

        $this->delayed->clear($updateProducts, Delayed::DELAYED_UPDATE_MARKER_ATTRIBUTE);
    }

    /**
     * @return \DI\ContainerInterface
     */
    private function getContainer()
    {
        return Ikantam_Controller_Front::getContainer();
    }
} 