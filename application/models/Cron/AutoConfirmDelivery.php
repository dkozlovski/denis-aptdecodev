<?php

class Application_Model_Cron_AutoConfirmDelivery
{

    public function doCron()
    {
        $items = $this->_initItems();
        
        foreach ($items as $item) {
            $item->confirmDelivery();
        }
    }

    /**
     * Get recently delivered items
     * 
     * @return Order_Model_Item[]
     */
    protected function _initItems()
    {
        $out = array();

        $collection = new Order_Model_Item_Collection();
        $collection->getForAfterDeliveryCron();

        $count = 0;

        foreach ($collection as $item) {
            $sellerDate = $item->getSellerDates();

            if (!$sellerDate) {
                continue;
            }

            $dateOfDelivery = substr($sellerDate, 0, 10);
            $timeOfDelivery = substr($sellerDate, 11);

            if (!$dateOfDelivery || !$timeOfDelivery) {
                continue;
            }

            switch ($timeOfDelivery) {
                case '7-12':
                case '8-12':
                    $timeOfDelivery = '12:00:00';
                    break;

                case '12-16':
                    $timeOfDelivery = '16:00:00';
                    break;

                case '16-20':
                    $timeOfDelivery = '20:00:00';
                    break;

                case '9-13':
                    $timeOfDelivery = '13:00:00';
                    break;

                case '17-21':
                    $timeOfDelivery = '21:00:00';
                    break;

                default:
                    $timeOfDelivery = '21:00:00';
                    break;
            }

            $dateTimeOfDelivery = strtotime($dateOfDelivery . ' ' . $timeOfDelivery);

            if (!$dateTimeOfDelivery || (($dateTimeOfDelivery + (8 * 3600)) >= time())) {//at least 8 hours after scheduled delivery date & time
                continue;
            }

            $out[] = $item;

            $count++;

            if ($count >= 10) {
                break;
            }
        }

        return $out;
    }

}