<?php

class Application_Model_Cron_ReindexProducts
{

    protected $_connection = null;

    public function doCron()
    {
        $dbh = Application_Model_DbFactory::getFactory()->getConnection();

        $sql = 'SELECT
            `products`.`id` as `id`,
            `products`.`title` as `title`,
            `products`.`description` as `description`,
            `products`.`price` as `price`,
            `products`.`color_id` as `color_id`,
            `products`.`manufacturer_id` as `manufacturer_id`,
            `products`.`category_id` as `category_id`,
            `products`.`condition` as `condition`,  
            `products`.`created_at` as `created_at`,
            `products`.`updated_at` as `updated_at`,
            `products`.`qty` as `qty`,
            `products`.`page_url` as `page_url`,
            `products`.`available_till` as `available_till`,
            `products`.`expire_at` as `expire_at`,
            `categories`.`title` as `category_title`,
            `conditions`.`short_title` as `condition_title`,
            `manufacturers`.`title` as `manufacturer_title`,
            `colors`.`title` as `color_title`,
            `materials`.`title` as `material_title`,
            `users`.`rating` as `seller_rating`

            FROM `products`
            LEFT JOIN `categories` ON `products`.`category_id` = `categories`.`id`
            LEFT JOIN `conditions` ON `products`.`condition` = `conditions`.`code`
            LEFT JOIN `manufacturers` ON `products`.`manufacturer_id` = `manufacturers`.`id`
            LEFT JOIN `colors` ON `products`.`color_id` = `colors`.`id`
            LEFT JOIN `materials` ON `products`.`material_id` = `materials`.`id`
            LEFT JOIN `users` ON `products`.`user_id` = `users`.`id`

            WHERE `products`.`is_published` = 1 AND `products`.`is_approved` = 1 AND `products`.`is_visible` = 1
            ORDER BY `id` ASC;';

        $stmt = $dbh->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $catalogSearch = new Catalog_Model_CatalogSearch();
        $catalogSearch->createIndex($result);
    }

}