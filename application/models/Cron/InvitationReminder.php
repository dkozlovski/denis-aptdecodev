<?php
class Application_Model_Cron_InvitationReminder
{
    protected $invitaions = null;
    protected $sendLimit = 10;
    
    public function doCron()
    {
        $this->remind();
        $this->invitaions->markAsReminded();
    }   
    
    public function __construct()
    {
        $this->invitaions = new Application_Model_Invitation_Collection();
    }    
    
    
    protected function getInvToRimind()
    {
        $inv = $this->invitaions;
        $inv->getForRemind($this->sendLimit);

        return $inv->getColumns(array('code', 'email')); //struct: array( 0 => array('|code|', '|email|')...);
    }
    
    protected function remind()
    {   
        $view = new Zend_View();
        $view->setScriptPath(realpath(APPLICATION_PATH.'/modules/admin/views/scripts'));
        
        $invitations = $this->getInvToRimind();

        foreach ($invitations as $inv)
        {
            $view->code = $inv['code'];
            $view->imageKey = Ikantam_Math_Rand::get62String(16);
            
            $body = $view->render('invite_reminder_letter.phtml');

            $toSend = array(
                'email' => $inv['email'],
                'subject' => 'Reminder - Your Exclusive AptDeco Invitation Is Waiting For You',
                'body' => $body);
            

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            try {
                $mail->send();
            } catch (Exception $e) {
                var_dump($inv['email']);
                var_dump($e->getMessage());
            }
        }
    }     
  
}