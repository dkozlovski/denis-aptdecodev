<?php

class Application_Model_Cron_UnpublishSoldProducts
{

    public function doCron()
    {
        $products = new Product_Model_Product_Collection();
        $products->getPublishedSoldProduct();

        foreach ($products as $product) {
            $item = new Order_Model_Item();
            $item->getLastWeekByProductId($product->getId());

            if ($item->getId()) {//product sold less than 2 weeks ago
                continue;
            }

            $str = date('Y-m-d H:i:s') . sprintf(" Product #%d unpublished.\n", $product->getId());
            file_put_contents(APPLICATION_PATH . '/log/cron11.log', $str, FILE_APPEND);

            $product->setIsPublished(0)->save();
        }
    }

}