<?php

class Application_Model_Cron_PayoutStatus
{
    
    protected function initPayouts()
    {
        $payouts = new Order_Model_Payout_Collection();
        return $payouts->getForStatusCron();
    }
    
    public function doCron()
    {
        $payouts = $this->initPayouts();
        
        foreach ($payouts as $payout) {
            if ($payout->getId()) {
                $marketplace = new Ikantam_Balanced_Marketplace();
                
                $credit = $marketplace->getCredit($payout->getCreditUri());
                
                if ($credit->getStatus()) {
                    $payout->setStatus($credit->getStatus())->save();
                    
                    if ($credit->getStatus() == 'failed') {
                        //$this->sendEmails($payout);
                    }
                }
            }
        }
    }

}