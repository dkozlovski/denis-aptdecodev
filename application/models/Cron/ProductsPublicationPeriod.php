<?php 
class Application_Model_Cron_ProductsPublicationPeriod
{
   protected $tomorrow = null;
   protected $afterThreeDays = null; 

    public function doCron ()
    {
        $this->sendReminderEmails();
        $this->markAsReminded();
        $this->unpublishExpiredProducts();
    }


    protected function sendReminderEmails()
    {
        $products = $this->getAboutExpireProducts();
        $view = new Zend_View();
      
        $view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $view->setScriptPath(realpath(APPLICATION_PATH . '/views/scripts'));

        foreach($products as $product)
        {
            $user = $product->getSeller();
            $view->user = $user;
            $view->product = $product;
            $body = $view->render('email_reminder_about_expire.phtml');

            $toSend = array(
                'email' => $user->getMainEmail(),
                'subject' => 'Your Furniture Listing on AptDeco.com is About to Expire',
                'body' => $body
            );

            $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
            try {
                $mail->sendCopy($body);
                $mail->send();     
            } catch (Exception $exc) {
                var_dump($exc->getMessage());
                var_dump($user->getMainEmail());
            }

                               
        }
    }
    
    
    protected function getAboutExpireProducts()
    {
        $tomorow = new Product_Model_Product_Collection();
        $tomorow->getAboutExpireTomorrow();
        
        $afterThreeDays = new Product_Model_Product_Collection();
        $afterThreeDays->getAboutExpireThreeDays();
        
        $this->tomorrow = $tomorow;
        $this->afterThreeDays = $afterThreeDays;
        
        return $tomorow->merge($afterThreeDays);
    }
    
    protected function markAsReminded()
    {
        $this->afterThreeDays->markAsReminded(1);
        $this->tomorrow->markAsReminded(2);
    }
    
    protected function unpublishExpiredProducts()
    {
        $products = new Product_Model_Product_Collection();
        $products->unpublishExpired();
    }
}
        
