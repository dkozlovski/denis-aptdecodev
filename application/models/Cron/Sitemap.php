<?php

class Application_Model_Cron_Sitemap
{

    public function doCron()
    {

        $site = new Application_Model_Sitemap();

        $modelsite = $site->setCategories()
                ->setProducts()
                ->setCollections()
                ->setThemesBlog()
                ->setPostsBlog()
                ->setJobs()
                ->setStaticsPages("blog")
                ->setStaticsPages("jobs")
                ->setAllStaticsPages()
                ->getSitemodel();

        $sitemap = new Ikantam_Sitemap(Ikantam_Url::getUrl(''));

        $sitemap->addUrls($modelsite);

        $sitemap->createSitemap();
    }

}