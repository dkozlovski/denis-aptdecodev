<?php

class Application_Model_Cron_ProductsAvailableTillDaily
{

    protected $_connection = null;

    public function doCron()
    {
        $this->clearList();

        $this->prepareList($daysLeft = 1);
        $this->prepareList($daysLeft = 3);

        $this->unpublishExpired();
    }

    protected function clearList()
    {
        $dbh = $this->getConnection();

        $sql = 'UPDATE `products_available_till` SET is_active = 0';

        $stmt = $dbh->prepare($sql);

        $stmt->execute();
    }

    protected function prepareList($daysLeft)
    {
        $dbh = $this->getConnection();

        $sql = 'SELECT * FROM `products` 
            WHERE `available_till` IS NOT NULL 
            AND `available_till` < :time AND `available_till` >= :now
            AND `qty` > 0
            AND `is_visible` = 1
            AND `is_published` = 1
            AND `is_approved` = 1';

        //product will expire in $daysLeft days
        $time = strtotime(date('Y-m-d')) + (($daysLeft + 1) * 24 * 60 * 60);
        $now  = strtotime(date('Y-m-d')) + ($daysLeft * 24 * 60 * 60);

        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':time', $time);
        $stmt->bindParam(':now', $now);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $this->populateTable($row, $daysLeft);
            }
        }
    }

    protected function populateTable($row, $daysLeft)
    {
        $dbh = $this->getConnection();

        if (isset($row['id'])) {
            //$daysLeft days left before product will be upublished

            $sql = 'INSERT INTO `products_available_till` 
                    (`product_id`, `is_active`, `days_left`) 
                    VALUES (:product_id, 1, :days_left)';

            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(':product_id', $row['id']);
            $stmt->bindParam(':days_left', $daysLeft);
            $stmt->execute();
        }
    }

    protected function unpublishExpired()
    {
        $this->_deleteSearchIndex();

        $dbh = $this->getConnection();

        $sql = 'UPDATE `products` SET `is_published` = 0
            WHERE `available_till` IS NOT NULL
            AND `available_till` != 0
            AND `available_till` < :time
            AND `qty` > 0
            AND `is_visible` = 1
            AND `is_published` = 1
            AND `is_approved` = 1';

        $time = strtotime(date('Y-m-d'));

        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':time', $time);
        $stmt->execute();
    }

    protected function _deleteSearchIndex()
    {
        $dbh = $this->getConnection();

        $select = 'SELECT `id` FROM `products` WHERE `available_till` IS NOT NULL
            AND `available_till` != 0
            AND `available_till` < :time
            AND `qty` > 0
            AND `is_visible` = 1
            AND `is_published` = 1
            AND `is_approved` = 1';

        $stmt0 = $dbh->prepare($select);
        $time0 = strtotime(date('Y-m-d'));
        $stmt0->bindParam(':time', $time0);
        $stmt0->execute();
        $rows  = $stmt0->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            $catalogSearch = new Catalog_Model_CatalogSearch();
            foreach ($rows as $row) {
                if (isset($row['id'])) {
                    $catalogSearch->deleteProductIndexById($row['id']);
                }
            }
        }
    }

    protected function getConnection()
    {
        if (!$this->_connection) {
            $this->_connection = Application_Model_DbFactory::getFactory()->getConnection();
        }

        return $this->_connection;
    }

}