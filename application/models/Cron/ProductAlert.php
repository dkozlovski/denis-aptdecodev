<?php

class Application_Model_Cron_ProductAlert
{

    public function doCron()
    {
        $collection = new Product_Model_Alert_Collection();
        $collection->check();
        foreach ($collection->getItems() as $_alert) {
            /* @var $_alert Product_Model_Alert */
            $_alert->sendEmails();
        }
    }

}