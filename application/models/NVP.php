<?php
/**
 * NVP(abbreviation) - Name Value Pair
 * This class use for manage addtitional application parameters (like options in WP),
 * To store some value to get it in future
 */
class Application_Model_NVP extends Application_Model_Abstract
{
    /** 
     * @type array
     */
    protected $_cache = array();
    
    /**
     * Stores the object instance
     * @type object Application_NVP
     */
    protected static $_instance ;
    
    /**
     * Getter|Setter method
     * Return value if name passed without second argument
     * Or save value for passed name
     * 
     * @param string $name
     * @param mixed $value 
     * @return mixed
     */
    public static function option ($name, $value = null)
    {
        $inst = self::_getInstance();
        if(null === $value)  {
            return $inst->_get($name);
        }         
        return $inst->_set($name, $value);        
    }
    
    /**
     * Delete option by name
     * 
     * @param  string $name
     * @return object self
     */
    public static function deleteOption ($name)
    {
        $inst = self::_getInstance();
        unset($inst->_cache[$name]);
        $inst->getBy_name($name);
        if($inst->isExists()) {
            $inst->delete();
        }
        
        return $inst;        
    }
    
    /**
     * Return instance
     *
     * @return object Application_Model_NVP
     */
    protected static function _getInstance ()
    {
        if(!$inst = self::$_instance) {
            $inst = self::$_instance = new self;
        }        
        return $inst;        
    }
    
    /**
     * Return value for passed name
     * 
     * @param  string $name
     * @return mixed
     */
    protected function _get($name)
    {
        $value = null;
        if(isset($this->_cache[$name])) {
            $value = $this->_cache[$name];
        } else {
            $this->getBy_name($name);
            $value = $this->getValue();
            $value = $this->_unserializeValue($value);
            $this->_cache[$name] = $value;
        }
        
        return $value;
    }
    
    /**
     * Write/Rewrite value for passed name
     * 
     * @param  string $name
     * @param  mixed $value
     * @return object self
     */
    protected function _set($name, $value)
    {
        $this->_cache[$name] = $value;
        $value = $this->_serializeValue($value);
        $this->setName($name)
             ->setValue($value)
             ->save();
        return $this;
    }
    
    /**
     * Serialize value if it is object or array
     * 
     * @param mixed $value  
     * @return mixed
     */
    protected function _serializeValue ($value)
    {
        if(is_array($value) || is_object($value)) {
            $value = serialize($value);
        }
        return $value;
    }
    
    /**
     * Unserialize value if it is possible
     * 
     * @param  mixed $value
     * @return mixed
     */
    protected function _unserializeValue ($value)
    {
        $data = @unserialize($value); 
            if($data !== false) {
                $value = $data;
            }        
        
        return $value;
    } 
    
    // redeclare standart methods to protect it
    protected function setName($name)
    {
        return parent::setName($name);
    }
    
    protected function setValue($value) 
    {
        return parent::setValue($value);
    }    
}