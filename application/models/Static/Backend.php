<?php

/**
 *    Application_Model_Static_Backend class
 *    Contains functions for getting data from WordPress database 
 *    (ex. About Us, Blog, FAQ, How It Works and etc.) pages content
 *    
 *    @author Alex Polevoy
 *    @created 31.01.2013 
 */
class Application_Model_Static_Backend extends Application_Model_Abstract_Backend
{

    /**
     * @var private $_wp_previx - prefix of tables in WordPress database
     */
    private $_wp_prefix = 'wp_';

    /**
     * @var private $_wpPostType - 'post_type' in wp_posts according 'Opened Post (not wp specific records)'
     */
    private $_wpPostType = 'post';

    /**
     * @var private $_wpPostStatus - 'post_status' in wp_posts according 'Opened Post (not wp specific records)'
     */
    private $_wpPostStatus = 'publish';

    /**
     * Used to fetch data frow WP database 
     * get data for static pages
     * 
     * @access public
     * @return array
     */
    public function getContent($post_name)
    {
        $wp_connection = new Application_Model_DbFactory();

        $sql = "SELECT * FROM `" . $this->_wp_prefix . "posts` WHERE `post_type` = :post_type";
        $sql .= " AND `post_name` = :post_name LIMIT 1;";



        $stmt = $wp_connection->getConnection('_wp')->prepare($sql);

        $post_type = 'page';
        $stmt->bindParam(':post_type', $post_type);
        $stmt->bindParam(':post_name', $post_name);


        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row;
    }

    public function getPostTags($postId)
    {
        $wp_connection = new Application_Model_DbFactory();

        $sql = 'SELECT `wp_terms`.*
                FROM `wp_terms`
                JOIN `wp_term_taxonomy` ON `wp_terms`.`term_id` = `wp_term_taxonomy`.`term_id`
                JOIN `wp_term_relationships` ON `wp_term_taxonomy`.`term_taxonomy_id` = `wp_term_relationships`.`term_taxonomy_id`
                WHERE `wp_term_taxonomy`.`taxonomy` = "post_tag"
                AND `wp_term_relationships`.`object_id` = :post_id
                ORDER BY `wp_terms`.`name` ASC';

        $stmt = $wp_connection->getConnection('_wp')->prepare($sql);

        $stmt->bindParam(':post_id', $postId);

        $stmt->execute();

        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $row;
    }

    /**
     * Used to fetch data frow WP database 
     * get posts for 'blog' page
     * 
     * @access public
     * @return array
     */
    public function getBlogPosts($categorySlug, $postName = null, $offset = null, $limit = null)
    {
        $wp_connection = new Application_Model_DbFactory();

        $query = 'SELECT `p`.`post_date`, `p`.`post_name`, `p`.`post_title`, `p`.`post_content`, `p`.`ID` as `post_id`'
                . ' FROM `' . $this->_wp_prefix . 'posts` as `p`'
                . ' JOIN `' . $this->_wp_prefix . 'term_relationships` as `tr` ON  `tr`.`object_id` = `p`.`ID`'
                . ' JOIN `' . $this->_wp_prefix . 'term_taxonomy` AS `tt` ON `tt`.`term_taxonomy_id` = `tr`.`term_taxonomy_id`'
                . ' JOIN `' . $this->_wp_prefix . 'terms` AS `t` ON `t`.`term_id` = `tt`.`term_id`'
                . ' WHERE `p`.`post_status` = :blogPostStatus'
                . ' AND `p`.`post_type` = :blogPostType';

        if ($postName) {
            $query .= ' AND `p`.post_name = :post_name';
            $query .= ' GROUP BY `p`.`ID`';
        } else {
            $query .= ' AND `t`.`slug` = :slug';
        }

        $query .= ' ORDER BY `p`.`post_date` DESC';

        if ($offset !== null && $limit !== null) {
            $query .= sprintf(' LIMIT %d, %d', (int) $offset, (int) $limit);
        }

        $stmt = $wp_connection->getConnection('_wp')->prepare($query);

        $stmt->bindParam(':blogPostType', $this->_wpPostType);
        $stmt->bindParam(':blogPostStatus', $this->_wpPostStatus);

        if ($postName) {
            $stmt->bindParam(':post_name', $postName);
        } else {
            $stmt->bindParam(':slug', $categorySlug);
        }

        $stmt->execute();

        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $res;
    }

    //get current category by slug or "Blog" category if slug not provided
    public function getCategoryInfo($categorySlug = null)
    {
        $wp_connection = new Application_Model_DbFactory();

        $query = "SELECT DISTINCT *  FROM  `" . $this->_wp_prefix . "term_taxonomy` AS `tt`
            JOIN  `" . $this->_wp_prefix . "terms` AS `t` ON `tt`.`term_id` = `t`.`term_id` ";

        if ($categorySlug) {
            $query .= "  WHERE `t`.`slug` = :slug";
        } else {
            $query .= "  WHERE `tt`.`term_id` = :termId";
        }

        $stmt = $wp_connection->getConnection('_wp')->prepare($query);

        if ($categorySlug) {
            $stmt->bindParam(':slug', $categorySlug);
        } else {
            $defaultCategoryId = (int) Zend_Registry::get('config')->blog->defaultCategory;
            $stmt->bindParam(':termId', $defaultCategoryId);
        }

        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        return $res;
    }

    public function countPosts()
    {
        $wp_connection = new Application_Model_DbFactory();

        $query = 'SELECT COUNT(*) as `count` FROM `' . $this->_wp_prefix . 'posts` as posts';
        $query .= ' JOIN wp_term_relationships as term_relation ON  posts.ID = term_relation.object_id';
        $query .= ' WHERE term_relation.term_taxonomy_id = 2 ';
        $query .= ' AND posts.post_status = :blogPostStatus';
        $query .= ' AND posts.post_type = :blogPostType';

        $stmt = $wp_connection->getConnection('_wp')->prepare($query);

        $stmt->bindParam(':blogPostType', $this->_wpPostType);
        $stmt->bindParam(':blogPostStatus', $this->_wpPostStatus);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row && isset($row['count'])) {
            return (int) $row['count'];
        }

        return 0;
    }

    /**
     * Used to get FAQ posts from WP database
     * Need to get 'faq' category id from wp_terms 
     * After - get post ids  with this CATEGORY ID  (term_id)  from wp_term_relationships
     * And get posts with selected OBJECT ID 
     * 
     * @access public
     * @return array
     */
    public function getFaqPosts()
    {

        $wp_connection = new Application_Model_DbFactory();

        $query = 'SELECT `post_title`, `post_content` FROM `' . $this->_wp_prefix . 'posts` as post ';
        $query .= ' JOIN ' . $this->_wp_prefix . 'terms as term';
        $query .= ' JOIN ' . $this->_wp_prefix . 'term_relationships as term_relation ON post.id = term_relation.object_id';
        $query .= ' WHERE term.slug = :faqTermSlug';
        $query .= ' AND term_relation.term_taxonomy_id = term.term_id ';
        $query .= ' AND post.ID = term_relation.object_id';
        $query .= ' AND post.post_status = :faqPostStatus';
        $query .= ' AND post.post_type = :faqPostType';

        $stmt = $wp_connection->getConnection('_wp')->prepare($query);

        $faqSlug = 'faq';
        $stmt->bindParam(':faqTermSlug', $faqSlug);
        $stmt->bindParam(':faqPostType', $this->_wpPostType);
        $stmt->bindParam(':faqPostStatus', $this->_wpPostStatus);

        $stmt->execute();

        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $row;
    }

    public function getFaqSidebarPosts()
    {
        return array();
        $sql = 'SELECT `posts`.`post_title`, `posts`.`post_content` FROM `' . $this->_wp_prefix . 'posts` as `posts`
			JOIN `' . $this->_wp_prefix . 'term_relationships` as `term_relationships` ON `posts`.`ID` = `term_relationships`.`object_id`
			JOIN `' . $this->_wp_prefix . 'term_taxonomy` as `term_taxonomy` ON `term_relationships`.`term_taxonomy_id` = `term_taxonomy`.`term_taxonomy_id`
			JOIN `' . $this->_wp_prefix . 'terms` as `terms` ON `term_taxonomy`.`term_id` = `terms`.`term_id`
			WHERE `terms`.`slug` = :slug AND `posts`.`post_status` = :post_status AND `posts`.`post_type` = :post_type';

        $wpConnection = new Application_Model_DbFactory();
        $stmt         = $wpConnection->getConnection('_wp')->prepare($sql);

        $slug       = 'faq-sidebar';
        $postStatus = $this->_wpPostStatus;
        $postType   = $this->_wpPostType;

        $stmt->bindParam(':slug', $slug);
        $stmt->bindParam(':post_status', $postStatus);
        $stmt->bindParam(':post_type', $postType);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            return $rows;
        }
        return array();
    }

    public function getJobsPosts($postId = null)
    {

        $wp_connection = new Application_Model_DbFactory();

        $query = 'SELECT `id`, `post_title`, `post_content` FROM `' . $this->_wp_prefix . 'posts` as post ';
        $query .= ' JOIN ' . $this->_wp_prefix . 'terms as term';
        $query .= ' JOIN ' . $this->_wp_prefix . 'term_relationships as term_relation ON post.id = term_relation.object_id';
        $query .= " WHERE term.slug = 'jobs' ";
        $query .= ' AND term_relation.term_taxonomy_id = term.term_id ';
        $query .= ' AND post.ID = term_relation.object_id';
        $query .= ' AND post.post_status = :jobsPostStatus';
        $query .= ' AND post.post_type = :jobsPostType';

        if (!is_null($postId)) {
            $query .= ' AND post.id = :postId';
        }

        $stmt = $wp_connection->getConnection('_wp')->prepare($query);

        $stmt->bindParam(':jobsPostType', $this->_wpPostType);
        $stmt->bindParam(':jobsPostStatus', $this->_wpPostStatus);

        if (!is_null($postId)) {
            $stmt->bindParam(':postId', $postId);
        }

        $stmt->execute();

        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $res;
    }

    public function getCategories()
    {

        $wp_connection = new Application_Model_DbFactory();

        $query = "SELECT `tt`.`count`, `tt`.`term_id`, `t`.`name`, `t`.`slug` FROM  `wp_term_taxonomy` AS `tt`
            JOIN `wp_terms` AS `t` ON  `t`.`term_id` = `tt`.`term_id`
            WHERE `tt`.`parent` = :termId1  OR `tt`.`term_id` = :termId2
            ORDER BY `tt`.`parent` ASC, `t`.`name` ASC
            ";


        $stmt = $wp_connection->getConnection('_wp')->prepare($query);

        $defaultCategoryId = (int) Zend_Registry::get('config')->blog->defaultCategory;

        $stmt->bindParam(':termId1', $defaultCategoryId);
        $stmt->bindParam(':termId2', $defaultCategoryId);


        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $res;
    }

    protected function _insert(\Application_Model_Abstract $session)
    {
        
    }

    protected function _update(\Application_Model_Abstract $session)
    {
        
    }

    public function getLatestPost()
    {
        $wp_connection = new Application_Model_DbFactory();

        $query = 'SELECT `p`.`post_date`, `p`.`post_name`, `p`.`post_title`, `p`.`post_content`, `p`.`ID` as `post_id`'
                . ' FROM `' . $this->_wp_prefix . 'posts` as `p`'
                . ' JOIN `' . $this->_wp_prefix . 'term_relationships` as `tr` ON  `tr`.`object_id` = `p`.`ID`'
                . ' JOIN `' . $this->_wp_prefix . 'term_taxonomy` AS `tt` ON `tt`.`term_taxonomy_id` = `tr`.`term_taxonomy_id`'
                . ' JOIN `' . $this->_wp_prefix . 'terms` AS `t` ON `t`.`term_id` = `tt`.`term_id`'
                . ' WHERE `p`.`post_status` = :blogPostStatus'
                . ' AND `p`.`post_type` = :blogPostType'
                . ' ORDER BY `p`.`post_date` DESC'
                . ' LIMIT 1';

        $stmt = $wp_connection->getConnection('_wp')->prepare($query);
        
        $stmt->bindParam(':blogPostStatus', $this->_wpPostStatus);
        $stmt->bindParam(':blogPostType', $this->_wpPostType);

        $stmt->execute();

        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        return $res;
    }

    public function getPage($postName)
    {
        $wp_connection = new Application_Model_DbFactory();

        $query = "SELECT DISTINCT `p`.`post_date`, `p`.`post_name`, `p`.`post_title`, `p`.`post_content`, `p`.`ID` as `post_id`
            FROM `" . $this->_wp_prefix . "posts` as `p`
            WHERE `p`.`post_status` = :blogPostStatus
                AND `p`.`post_type` = 'page'
                AND `p`.post_name = :post_name";


        $stmt = $wp_connection->getConnection('_wp')->prepare($query);

        $stmt->bindParam(':blogPostStatus', $this->_wpPostStatus);
        $stmt->bindParam(':post_name', $postName);

        $stmt->execute();

        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        return $res;
    }

}