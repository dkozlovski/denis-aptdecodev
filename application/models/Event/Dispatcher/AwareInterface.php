<?php

interface Application_Model_Event_Dispatcher_AwareInterface
{

    /**
     * Get event dispatcher instance
     */
    public function getEventDispatcher();

    /**
     * Set event dispatcher instance
     * 
     * @param Zend_EventManager_EventCollection $eventDispatcher
     */
    public function setEventDispatcher(Zend_EventManager_EventCollection $eventDispatcher);
}