<?php

class Application_Model_Event_Listener_AssignProductToUser
{
    public function assign()
    {
        if ($isPublishProductRedirect = User_Model_Session::instance()->activeData('publishProductRedirect')) {

            $productData = User_Model_Session::instance()->activeData('_product_data');
            $product     = new Product_Model_Product($productData['id']);

            $userId = User_Model_Session::instance()->getUserId();

            $product->setUserId($userId)
                ->setIsPublished($productData['is_published'])
                ->setCreatedAt(time());

            $address = User_Model_Session::instance()->activeData('_guest_shipping_address');

            if ($address) {
                $address = unserialize($address);
                $address->setUserId($userId)
                    ->save();

                $product->setPickUpAddressId($address->getId())
                    ->save();
            }
            $product->save();

        }
    }
}