<?php

class Application_Model_Event_Listener_Message
{
    public static function responseMessageSeller($event)
    {
        self::_timelyResponseSellerRatingUp($event);
    }
    
    protected static function _timelyResponseSellerRatingUp($event)
    {
        $message = $event->getTarget()->getData();
        $AuthorID = $message['author_id'];
        $ConversationID = $message['conversation_id'];
        $TimeResponse = $message['created_at'];
        $conversation = new Conversation_Model_Conversation($ConversationID);
        $config = Zend_Registry::get('config');
        foreach ($conversation->getUsers() as $user) {
             if ($user->getId() != $AuthorID){
                 $messageC = new Conversation_Model_Conversation_Message_Collection();
                 $LastMessage = $messageC->getTimeLastMessage($user->getId(), $ConversationID);                
             }               
        }
        
        if($LastMessage){
            if(1 == $messageC->countResponseMessage($AuthorID, $ConversationID, $LastMessage['created_at'])){
                if(($TimeResponse - $LastMessage['created_at']) <= $config->rating->seller->intervalResponse){
                    $proc =  ($TimeResponse - $LastMessage['created_at'])*100/$config->rating->seller->intervalResponse;
                    $proc = round($proc);
                    $userC = new User_Model_User_Collection();
                    $user = $userC->getUser($AuthorID)->getData();
                    $rating = $user['seller_popularity']+($config->rating->seller->timelyResponse-($config->rating->seller->timelyResponse*$proc/100));
                    $userC->userRatingUp($AuthorID, $rating);

                }
            }

        }
    }
}