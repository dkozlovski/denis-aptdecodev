<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 15.09.14
 * Time: 17:45
 */

class Application_Model_Event_Listener_Transactions
{
    public  static function transactionCanceledByUser($e)
    {
        /** @var \Order_Model_Item $item */
        $item = $e->getTarget();
        $product = $item->getProduct();
        /*$order = $item->getOrder();
        $ShippingAddress = new Order_Model_Address($order->getData('shipping_address_id'));
        $phone = $ShippingAddress->getData('telephone');
        $msg = new Application_Model_SmsNotification();
        $msg->getByType('user_seller_canceled')
            ->bindParam('{:titleProduct}', $product->getData('title'));
        self::_sendMessage($phone,$msg->getData('text'));*/
        self::_CanceledSoldSellerRatingDown($product);

    }
    
    public static function transactionApprovedByUser($e)
    {
        /*$item = $e->getTarget();
        $product = $item->getProduct();
        $order = $item->getOrder();
        $ShippingAddress = new Order_Model_Address($order->getData('shipping_address_id'));
        $phone = $ShippingAddress->getData('telephone');
        $msg = new Application_Model_SmsNotification();
        $msg->getByType('user_seller_approved')
            ->bindParam('{:titleProduct}', $product->getData('title'));
        self::_sendMessage($phone, $msg->getData('text')); */
    }
    
    public static  function transactionUserCheckout($e)
    {
        $order = $e->getTarget();
        //send delivery
        foreach($order->getItemsForDelivery() as $item){
            $product = $item->getProduct();
            $phone = $product->getPickupPhone();
            $seller = $product->getSeller();
            $msg = new Application_Model_SmsNotification();            
            $msg->getByType('sale_confirmation_delivery')
                ->bindParam('[item name]', $product->getData('title'))
                ->bindParam('[first name]', $seller->getFirstName());
            self::_sendMessage($phone,$msg->getData('text'));
        }
        
        //send email
        $mailer = new Notification_Model_Order_Created();
        $mailer->sendEmails($order);
    }


    protected function _CanceledSoldSellerRatingDown($product)
    {
        $config = Zend_Registry::get('config');
        $userC = new User_Model_User_Collection();
        $user = $product->getSeller()->getData();
        $rating = $user['seller_popularity']+$config->rating->seller->CanceledSoldProduct;
        $userC->userRatingUp($user['id'], $rating);
    }
    
    protected function _sendMessage($number, $msg)
    {
        $config = Zend_Registry::get('config');
        try{
        $client = new Services_Twilio($config->twilio->accountSid, $config->twilio->authToken);      
        $client->account->messages->sendMessage($config->tvilio->number, $number, $msg);
        }  catch (Exception $e){
            
        }
    }
} 