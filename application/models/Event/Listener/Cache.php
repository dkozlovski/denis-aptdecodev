<?php

class Application_Model_Event_Listener_Cache
{

    public function product($e)
    {
        $product = $e->getTarget();
        
        if (!$product instanceof Product_Model_Product || !$product->getId()) {
            return;
        }
        
        if ($product->isDeleted()) {
            self::_deleteCache($product->getId());
        } elseif ($product->hasDataChanges()) {
            self::_updateCache($product->getId());
        }
    }
    
    protected function _deleteCache($productId)
    {
        //@TODO: to be implemented later
    }
    
    protected function _updateCache($productId)
    {
        //@TODO: to be implemented later
    }

}