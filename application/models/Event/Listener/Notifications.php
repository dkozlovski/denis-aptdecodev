<?php
/**
 * Author: Alex P.
 * Date: 17.06.14
 * Time: 19:11
 */

class Application_Model_Event_Listener_Notifications
{
    /**
     * Calls when price for product lowered
     * @param $e
     * @return bool
     */
    public static function productPriceReduced($e)
    {
        $product = $e->getTarget();

        $userCollection = self::getWishlistCartUsersCollection($product->getId());

        // no need to create task for nobody
        if (!$userCollection->getFirstItem()->isExists()) {
            return false;
        }

        // this string will be exploded using delimiter |. The first part is a method name. Next is arguments.
        // i.e. adminEAVSetting method in Application_Model_ValueExtractor will be used with parameter
        // "whitelist_notification_price_reduced" and the second parameter defined below
        $methodSetting ='adminEAVSetting|wishlist_notification_price_reduced|';


        $storage = \Application_Model_ObjectStorage::factory(
            array(
                'product' => $product,
                // these objects(extractors) will be used to retrieve appropriate values via \Admin_Model_Settings
                'subject' => new \Application_Model_ValueExtractor($methodSetting . 'subject'),
                'text' => new \Application_Model_ValueExtractor($methodSetting . 'text'),
                'oldPrice' => $product->getStored('price'),
            )
        );

        \Notification_Model_Dispatcher::newTask($userCollection, 'wishlist/price_reduced.phtml', $storage);
        return true;
    }

    /**
     * Calls when sale window for product changed
     * @param $e
     * @return bool
     */
    public static function productSaleWindowChanged($e)
    {
        $product = $e->getTarget();

        $userCollection = self::getWishlistCartUsersCollection($product->getId());

        if (!$userCollection->getFirstItem()->isExists()) {
            return false;
        }

        $methodSetting ='adminEAVSetting|wishlist_notification_sale_window_changed|';

        $storage = \Application_Model_ObjectStorage::factory(
            array(
                'product' => $product,
                'subject' => new \Application_Model_ValueExtractor($methodSetting . 'subject'),
                'text' => new \Application_Model_ValueExtractor($methodSetting . 'text')
            )
        );

        \Notification_Model_Dispatcher::newTask($userCollection, 'wishlist/sale_window_changed.phtml', $storage);
        return true;
    }

    /**
     * Sends notification to seller
     * @param $e
     */
    public static function productManagePricingLoweredByAdmin($e)
    {
        $product = $e->getTarget();
        $dispatcher = self::getNotificationDispatcher();

        // immediately sends notification
        $dispatcher->sendNotificationByTask(
            // creates task without saving
            \Notification_Model_NotificationTask::instantTask(
                // send to
                $product->getSeller(),
                // use view template (notification module)
                'to_seller/manage_pricing_price_lowered_by_admin.phtml',
                // parameters
                \Application_Model_ObjectStorage::factory(
                    array(
                        'user' => $product->getSeller(),
                        'product' => $product,
                        'oldPrice' => $product->getStored('price'),
                        'subject' => new Application_Model_ValueExtractor(
                                'adminEAVSetting|email_notification_to_seller_price_lowered|subject'
                            ),
                        'title' => 'Price Drop Alert',
                        'text' => new Application_Model_ValueExtractor(
                                'adminEAVSetting|email_notification_to_seller_price_lowered|text'
                            )
                    )
                )
            )
        );
    }

    /**
     * Retrieves users which added product to cart or wishlist
     * @param $productId
     * @return Application_Model_User_Collection
     */
    protected static function getWishlistCartUsersCollection($productId)
    {
        $wishListItemsCollection = new \Wishlist_Model_Wishlist_Item_Collection;
        $wishListItemsCollection->getFlexFilter()
            ->user_unsubscribe_notifications('=', 0)
            ->user_unsubscribe_notifications('IS_NULL')
            ->product_id('=', $productId)
            ->apply();

        $userIds = $wishListItemsCollection->getColumn('user_id');

        $cartItemsCollection = new \Cart_Model_Item_Collection;

        // merge wishlist users with cart users
        $userIds = array_unique(array_merge(
                $userIds,
                $cartItemsCollection->getFlexFilter()
                    ->alwaysJoin('cart')
                    ->product_id('=', $productId)
                    ->user_unsubscribe_notifications('=', 0)
                    ->user_unsubscribe_notifications('IS_NULL')
                    ->user_id('>', 0) // instead of NOT IS NULL (not implemented yet)
                    ->apply()
                    ->getColumn('user_id')
            ));

        $cartItemsCollection->getFlexFilter()->product_id('=', $productId);

        $userCollection = new \Application_Model_User_Collection();
        $userCollection->getFlexFilter()->id('in', $userIds)->apply();

        return $userCollection;
    }

    /**
     * @return Notification_Model_Dispatcher
     */
    protected static function getNotificationDispatcher()
    {
        return new \Notification_Model_Dispatcher;
    }
} 