<?php

class Application_Model_Event_Listener_Indexer
{

    public static function product($e)
    {
        $product = $e->getTarget();

        if (!$product instanceof Product_Model_Product || !$product->getId()) {
            return;
        }

        if ($product->isDeleted() || !$product->getIsPublished() || !$product->getIsApproved() || !$product->getIsVisible()) {
            self::_deleteIndex($product);
        } elseif ($product->hasDataChanges()) {
            self::_updateIndex($product);
        }
    }

    protected static function _deleteIndex($product)
    {
        $catalogSearch = new Catalog_Model_CatalogSearch();

        if ($product->getId()) {
            $catalogSearch->deleteProductIndex($product);
        }
    }

    protected static function _updateIndex($product)
    {
        $catalogSearch = new Catalog_Model_CatalogSearch();

        if ($product->getId()) {
            $catalogSearch->updateProductIndex($product);
        }
    }

}