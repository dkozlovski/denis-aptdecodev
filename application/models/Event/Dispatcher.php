<?php

class Application_Model_Event_Dispatcher extends Zend_EventManager_EventManager
{

    public function __construct($identifiers = null)
    {
        $this->attach('product.save.after', array('Application_Model_Event_Listener_Indexer', 'product'));
        $this->attach(
            'product.price.reduced',
            array('Application_Model_Event_Listener_Notifications', 'productPriceReduced')
        );

        $this->attach(
            'product.sale_window.change',
            array('Application_Model_Event_Listener_Notifications', 'productSaleWindowChanged')
        );
        //$this->attach('product.saveValue.after', array('Application_Model_Event_Listener_Cache', 'product'));

        $this->attach(
            'product.price.manage_pricing.lowered_by_admin',
            array('Application_Model_Event_Listener_Notifications', 'productManagePricingLoweredByAdmin')
        );

        $this->attach(
            'assign.unassigned.created.product.after.user.authorize',
            array('Application_Model_Event_Listener_AssignProductToUser', 'assign')
        );

        $this->attach(
            'transaction.canceled.byUser',
            array('Application_Model_Event_Listener_Transactions', 'transactionCanceledByUser')
        );
        
        $this->attach(
            'transaction.approved.byUser',
            array('Application_Model_Event_Listener_Transactions', 'transactionApprovedByUser')
        );
        
        $this->attach(
            'transaction.user.checkout',
            array('Application_Model_Event_Listener_Transactions', 'transactionUserCheckout')
        );
        
        $this->attach(
            'response.message.seller',
            array('Application_Model_Event_Listener_Message', 'responseMessageSeller')
        );
        parent::__construct($identifiers);
    }

}