<?php

/**
 * Singleton
 */
class Application_Model_Acl extends Application_Model_Acl_Abstract
{

    protected function _getRolesList()
    {
        return array('Super Admin', 'Admin', 'Product Manager', 'Guest', 'Accounting',);
    }

    protected function _getResourcesList()
    {
        return array(
            'mvc:admin',
            'mvc:admin.product',
            'mvc:admin.products',
            'mvc:admin.login',
            'mvc:admin.users',
            'mvc:admin.index',
            'mvc:admin.transactions',
            'mvc:admin.pickup-and-delivery-date',
            'mvc:admin.invite',
            'mvc:admin.homepage',
            'mvc:admin.association',
            'mvc:admin.coupons',
            'mvc:admin.abandoned',
            'mvc:admin.upload-image',
            'mvc:admin.view-product',
            'mvc:admin.order',
            'mvc:admin.logout',
            'mvc:admin.items',
            'mvc:admin.shipping',
            'mvc:admin.rejected-messages',
            'mvc:admin.edit-product',
            'mvc:admin.referers',
            'mvc:admin.notifications',
            'mvc:admin.cron-emulator',
            'mvc:admin.text',
            'mvc:admin.aptdeco-pics',
            'mvc:admin.widgets',
            'mvc:admin.error',
            'mvc:admin.manager-admins',
            'mvc:admin.optimization',
            'mvc:admin.seller-verification',
            'mvc:admin.coll',
            'mvc:admin.invite-admin',
            'mvc:admin.collections',
            'mvc:admin.sms-notification'
        );
    }

    protected function _getRulesList()
    {
        return array(
            'allow' => array(
                array('Product Manager', array(
                        'mvc:admin.product', // full access to this controllers
                        'mvc:admin.products',
                        'mvc:admin.view-product',
                        'mvc:admin.edit-product',
                        'mvc:admin.cron-emulator',
                    )),
                array('Product Manager', 'mvc:admin.upload-image', 'index'), //only index action allowed to Product Manager
                array('Product Manager', 'mvc:admin.users', 'send-email'), //only send-email action allowed to Product Manager
                array('Accounting', 'mvc:admin.transactions', 'index'),
                array('Accounting', 'mvc:admin.users', 'order'),
                /*array('Accounting', 'mvc:admin.users', 'confirmdelivery'),
                array('Accounting', 'mvc:admin.users', 'cancel-order'),
                array('Accounting', 'mvc:admin.users', 'change-notes'),
                array('Accounting', 'mvc:admin.users', 'change-shipping-info'),
                array('Accounting', 'mvc:admin.users', 'set-check-payout'),
                array('Accounting', 'mvc:admin.users', 'change-delivery-address'),
                array('Accounting', 'mvc:admin.users', 'change-pickup-address'),
                array('Accounting', 'mvc:admin.users', 'edit-seller-date'),
                array('Accounting', 'mvc:admin.users', 'edit-buyer-dates'),
                array('Accounting', 'mvc:admin.users', 'create-dispute'),
                array('Accounting', 'mvc:admin.users', 'change-dispute-seller'),
                array('Accounting', 'mvc:admin.users', 'change-dispute-buyer'),*/
                array(null, 'mvc:admin.index', 'index'),
                array(null, 'mvc:admin.login'), // take a chance)
                array(null, 'mvc:admin.logout'),
                array(null, 'mvc:admin.error'),
                array(null, 'mvc:admin.invite-admin'),
                array('Admin'), // allow all (except following in deny)
                array('Super Admin'), // god mode ;)
            ),
            'deny'  => array(
                array('Admin', 'mvc:admin.manager-admins') // deny this controller
            )
        );
    }

    public function isAllowed($role = null, $resource = null, $privilege = null)
    {
        if (null !== $resource) { //input - module/controller/action
            if ($resource instanceof Zend_Acl_Resource_Interface) {
                $resource = $resource->getResourceId();
            }

            if (is_string($resource) && strpos($resource, '/') !== false) {
                $parts    = explode('/', $resource);
                $resource = 'mvc:' . array_shift($parts);
                $ctrl     = array_shift($parts);
                $resource .= $ctrl ? '.' . $ctrl : '';
                if (!$privilege) {
                    $privilege = array_shift($parts) ? : null;
                }
            } //output - mvc:module.controller and $privilege = action
        }
        //_log($role); _log($resource); _log($privilege);

        try {
            return parent::isAllowed($role, $resource, $privilege);
        } catch (Exception $e) {
            return false;
        }
    }

}

/* code snippet to test permissions:
        $resources = array(
            'mvc:admin',
            'mvc:admin.product',
            'mvc:admin.products',
            'mvc:admin.login',
            'mvc:admin.users',
            'mvc:admin.index',
            'mvc:admin.transactions',
            'mvc:admin.pickup-and-delivery-date',
            'mvc:admin.invite',
            'mvc:admin.homepage',
            'mvc:admin.association',
            'mvc:admin.coupons',
            'mvc:admin.abandoned',
            'mvc:admin.upload-image',
            'mvc:admin.view-product',
            'mvc:admin.order',
            'mvc:admin.logout',
            'mvc:admin.items',
            'mvc:admin.shipping',
            'mvc:admin.rejected-messages',
            'mvc:admin.edit-product',
            'mvc:admin.referers',
            'mvc:admin.notifications',
            'mvc:admin.cron-emulator',
            'mvc:admin.text',
            'mvc:admin.aptdeco-pics',
            'mvc:admin.widgets',
            'mvc:admin.error',
            'mvc:admin.manager-admins',
            'mvc:admin.optimization',
        );

$acl = Application_Model_Acl::getInstance();
echo '<pre>';
foreach ($resources as $resource) {
    $allowed = $acl->isAllowed('Product Manager', $resource) ? '<span style="color: green">GRANTED</span>' : '<span style="color: red">DENY</span>';
    echo $resource . "  $allowed\n";
}

exit;

*/