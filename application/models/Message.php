<?php

class Application_Model_Message extends Application_Model_Abstract
{
	protected $_author;
    protected $_recipient;

    public function getAuthor()
	{
        if (!$this->_author) {
            $this->_author = new Application_Model_User($this->getAuthorId());
        }
		return $this->_author;
	}
    
    public function getRecipient()
	{
        if (!$this->_recipient) {
            $this->_recipient = new Application_Model_User($this->getRecipientId());
        }
		return $this->_recipient;
	}
	
	public function setAsReaded()
	{
        $this->setIsNew(0)->save();
		return $this;
	}
	
	public function setAsDeletedByAuthor()
	{
		$this->setIsVisibleToAuthor(0)->save();
		return $this;
	}
    
    public function setAsDeletedByRecipient()
	{
		$this->setIsVisibleToRecipient(0)->save();
		return $this;
	}
	
	
	
}