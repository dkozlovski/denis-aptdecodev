<?php
class Application_Model_NVP_Backend extends Application_Model_Abstract_Backend
{
    protected $_table = 'nvp_options';
    protected $_fields_ = array('id', 'name', 'value');
    
    protected function _insert(\Application_Model_Abstract $object)
    {
        $nvp = new Application_Model_NVP;
        $nvp->getBy_name($object->getName());
        if($nvp->isExists()) {
            $object->setId($nvp->getId());
            return $this->_update($object);
        }
        return $this->runStandartInsert(array('name', 'value'), $object);
    }
    
    protected function _update(\Application_Model_Abstract $object)
    {
        return $this->runStandartUpdate(array('name', 'value'), $object);
    }    
}