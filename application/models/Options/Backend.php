<?php

class Application_Model_Options_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'options';
    protected $_relatedTable = 'options_values';
    protected $_individualFeeTable = 'individual_options';
    protected $_availableOperators = array('<', '>', '=', 'true'); // use in conditions

	public function loadOption($code)
	{
	   //! VERY IMPORTANT TO ORDER `options_values` BY `value` DESC! (Application_Model_Options::getFee depends on it)
        $sql = "SELECT * FROM `". $this->_getTable() ."` as `option` 
                INNER JOIN `". $this->_relatedTable ."` as `oi` ON `oi`.`option_id` = `option`.`id`
                WHERE `option`.`code` = :code ORDER BY `oi`.`value` DESC ";//DO NOT CHANGE ORDER DIRECTION
                
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':code', $code);
        $stmt->execute();
        
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $res;                
        
	}
    
    private function __checkCondition($condition, $feeValue) {


        if(!is_array($condition)) {
            throw new Exception(__METHOD__.' Condition must be an array.', 405);
        }
        
        if(!in_array($condition['operator'], $this->_availableOperators)) {
            throw new Exception(__METHOD__.' Operator "'.$condition['operator'].'" is not defined.', 406);
        }
        if(!isset($condition['value']) && $condition['operator'] === 'true') {
            $condition['value'] = 'any';
        }
        if(!is_numeric($condition['value']) && $condition['value'] !=  'any') {
            throw new Exception(__METHOD__.' Invalid condition value "'.$condition['value'].'"', 407);
        }
        
       
        if(!is_numeric($feeValue)) {
            throw new Exception(__METHOD__.' Invalid fee value "'.$feeValue.'"', 408);
        }         
    }

	/** Adds new fee value depends on condition
     * @param Application_Model_Abstract $object - must contain condition and value fields
     * 
     * condition example:
     * $condition = array( 'operator' => '<', value = '100' );
     * operators: <, >, =, true - for individual. Always valid.
     */    
    public function addNewConditionValue (\Application_Model_Abstract $object)
    {
        if(!$id = $object->getId(false))
            throw new Exception(__METHOD__.' Can not relate condition with empty object.', 403);
        if(!$object->getCondition(false) || $object->getValue(false) === false) {
            throw new Exception(__METHOD__.' Object must contain condition and value fields.', 404);
        }
        
        $condition = $object->getCondition();            
        $feeValue = $object->getValue(); 
           
        $this->__checkCondition($condition, $feeValue);
        
        $condition = serialize($condition);

        
        $sql = "INSERT INTO `". $this->_relatedTable ."` (`option_id`, `condition`, `value`) VALUES (:option_id, :condition, :value)";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':option_id', $id);
        $stmt->bindParam(':condition', $condition);
        $stmt->bindParam(':value', $feeValue);
        
        $stmt->execute(); 
        return $this->_getConnection()->lastInsertId();            
    }
    
    public function addIndividualFee ($userId, $valueId)
    {
        $sql = "INSERT INTO `" .$this->_individualFeeTable. "` (`options_values_id`, `user_id`) VALUES(:options_values_id, :user_id)";
        $stmt = $this->_getConnection()->prepare($sql);        
            
        
        $stmt->bindParam(':options_values_id', $valueId);
        $stmt->bindParam(':user_id', $userId);        
        $stmt->execute();         
    }
    
    public function updateIndividualCondition ($userId, $fee)
    {
        if(!is_numeric($fee))
            throw new Exception(__METHOD__.' Invalid fee value "'.$feeValue.'"', 408);
            
        //$condition = array('operator' => 'true', 'user' => $userId);    
        
        $sql = "UPDATE `". $this->_relatedTable ."` 
                INNER JOIN `" . $this->_individualFeeTable . "` as `i` ON `i`.`user_id` = :user_id
                AND `i`.`options_values_id` = `options_values`.`id`
                SET `value` = :fee               
                WHERE 1";
                
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':fee', $fee);
        
        return $stmt->execute();        
    }
    
    
// TRANSACTIONS   
    public function startTransaction ()
    {
        $this->_getConnection()->beginTransaction();
    }
    
    public function endTransaction ()
    {
         $this->_getConnection()->commit();
    }
    
        public function rollBack ()
    {
         $this->_getConnection()->rollBack();
    }
//**************************************** 



    public function getIndividualFee ($optionId, $userId)
    {   
        $sql = "SELECT `ov`.`value` FROM `". $this->_relatedTable . "` as `ov` INNER JOIN `". $this->_individualFeeTable ."`
                as `if` ON `if`.`options_values_id` = `ov`.`id` AND `if`.`user_id` = :user_id
                WHERE `ov`.`option_id` = :option_id";
        
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':option_id', $optionId);
        
        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        if($res)
            return $res['value'];
        
        return null;
    }
    
    public function getAllIndividualOptions ()
    {
        $sql = "SELECT `user`.`id` AS 'user_id', `user`.`full_name`, `user`.`email`, `o`.`value` AS 'fee'
                FROM `users` AS `user` INNER JOIN `". $this->_individualFeeTable ."` AS `if` 
                ON `if`.`user_id` = `user`.`id` INNER JOIN `". $this->_relatedTable ."` AS `o`
                ON `o`.`id` = `if`.`options_values_id` WHERE 1";
        
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(is_array($result)) {
            foreach($result as $key => $item) {
                $result[$key]['profile_url'] = Ikantam_Url::getUrl('user/profile/view', array('id' => $item['user_id']));
            }
        }

        return (is_array($result)) ? $result : array();
        
    }     
    
    
	/** Updates condition
     * @param string $code
     * @param array $old - old condition. Structure: array('operator' => '<', 'value' => '300', 'fee' =>10)
     * @param array @new - same structure as $old
     * @return bool - result
     */    
    public function updateCondition ($code, array $old, array $new)
    {
        $this->__checkCondition($new, $new['fee']);
        $sql = "UPDATE `". $this->_relatedTable ."` 
                INNER JOIN `" .$this->_getTable(). "` as `o` ON `o`.`code` = :code
                SET `condition` = :condition, `value` = :value               
                WHERE `option_id` = `o`.`id` AND `condition` = :old_condition";
               
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':value', $new['fee']);
        $stmt->bindParam(':code', $code);
        
        unset($old['fee']); unset($new['fee']);
        $old = serialize($old);
        $new = serialize($new);
        
        $stmt->bindParam(':condition', $new);
        $stmt->bindParam(':old_condition', $old);
        
        return $stmt->execute();       
    }
    
    public function deleteCondition ($code, $condition)
    {
        $condition = serialize($condition);
        $sql = "DELETE `oi` FROM `". $this->_relatedTable ."` AS `oi` INNER JOIN `". $this->_getTable() ."` AS `o`
                ON `oi`.`option_id` = `o`.`id`  AND `o`.`code` = :code WHERE  `condition` = :condition";
      //  exit($sql); 
        $stmt = $this->_getConnection()->prepare($sql);
        
        $stmt->bindParam(':code', $code);
        $stmt->bindParam(':condition', $condition);
        
        return $stmt->execute();
    }
    
    public function deleteIndividualFee ($userId, $code)
    {
        $sql = "DELETE `ov` FROM `". $this->_relatedTable ."` AS `ov` INNER JOIN `". $this->_individualFeeTable ."` AS `if` ON
                `if`.`options_values_id` = `ov`.`id` AND `if`.`user_id` = :user_id  INNER JOIN `". $this->_getTable() ."` AS `o`
                ON `o`.`id` = `ov`.`option_id` AND `o`.`code` = :code WHERE 1";

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':code', $code);
        
        return $stmt->execute();
    }
    
    
	protected function _update(\Application_Model_Abstract $object)
	{
	   $sql = "UPDATE `". $this->_getTable() ."` SET `code` = :code WHERE `id` = :id";
       
       $stmt = $this->_getConnection()->prepare($sql);       
       $code = $object->getCode();
       $id = $object->getId();
       $stmt->bindParam(':code', $code);
       $stmt->bindParam(':id', $id);
       
       
       $stmt->execute();
	
	}

	protected function _insert(\Application_Model_Abstract $object)
	{
	   $sql = "INSERT INTO `". $this->_getTable() ."` (`code`) VALUES (:code)";
       
       $stmt = $this->_getConnection()->prepare($sql);       
       $code = $object->getCode();
       $stmt->bindParam(':code', $code);
       
       $stmt->execute();
       $object->setId($stmt->lastInsertId());       	
	}

}
