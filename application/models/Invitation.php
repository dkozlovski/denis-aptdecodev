<?php

class Application_Model_Invitation extends Application_Model_Abstract
{

	protected $_backendClass = 'Application_Model_Invitation_Backend';

	public function getByCode($code)
	{
		$this->_getBackend()->getByCode($this, $code);

		return $this;
	}
    
    public function getByEmail($code)
	{
		$this->_getBackend()->getByEmail($this, $code);

		return $this;
	}
    
    public function generateCode ()
    {
        return substr(md5(Ikantam_Math_Rand::get62String(16)), 16);
    }

	public function isValid($data)
	{
		if (is_array($data)) {
			if (!isset($data['code'], $data['email'])) {
				return false;
			}

			$this->getByCode($data['code']);

			if (!$this->getId() || $this->getEmail() !== $data['email'] || $this->getIsUsed()) {
				return false;
			}
		} else {
			$this->getByCode($data);
			if (!$this->getId() || $this->getIsUsed()) {
				return false;
			}
		}

		return true;
	}
        
        public function createInvitationAdmin($data){
            
            $this->_getbackend()->createInvitationAdmin($data);
            
        }
        
        public function getInvitationAdmin($code){
            
           return $this->_getbackend()->getInvitationAdmin($code);
            
        }
        
        public function confirmInvitationAdmin($code){
            
            $this->_getbackend()->confirmInvitationAdmin($code);
            
        }

}
