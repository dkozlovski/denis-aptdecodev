<?php

class Application_Model_Avatar extends Application_Model_Abstract
{

    public function getByUserId($userId, $notCropped = false)
    {
        $this->_getBackend()->getByUserId($this, $userId, $notCropped);
        return $this;
    }

    public function getUrl()
    {
        $title = $this->getTitle();

        if (empty($title) || !$this->getS3Path()) {
            return Ikantam_Url::getPublicUrl('images/avatar.png');
        }

        $cloudFrontUrl = Zend_Registry::get('config')->amazon_cloudFront->url;

        return $cloudFrontUrl . '/user-images/avatars/' . $this->getS3Path();
    }

    public function getThumbnailUrl()
    {
        $title = $this->getTitle();

        if (empty($title)) {
            return false;
        }

        return Ikantam_Url::getPublicUrl('upload/avatars/thumbnail/' . $title);
    }

    public function save()
    {
        $cache = Zend_Registry::get('output_cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('search_items'));
        parent::save();
    }

}
