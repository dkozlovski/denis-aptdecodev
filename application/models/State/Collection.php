<?php

class Application_Model_State_Collection extends Application_Model_Abstract_Collection
{

	private $_loaded = false;

	public function getAll()
	{
		if (!$this->_loaded) {
			$this->_getBackend()->getAll($this);
		}

		return $this;
	}
    
    public function getJsonConfig()
	{
		$cache = Zend_Registry::get('core_cache');
		$cacheId = 'states_json_config';

		if (!($states = $cache->load($cacheId))) {
			$states = array();
			$this->_loaded = false;
			$this->getAll();

			foreach ($this->_items as $state) {
                $states[$state->getCountryCode()][$state->getStateCode()] = $state->getName();
			}
			$cache->save($states);
		}

		return $states;
	}
	
	public function getByCountryCode($countryCode)
	{
		$this->_getBackend()->getByCountryCode($this, $countryCode);
		return $this;
	}

	public function getOptions()
	{
		$result = array();

		foreach ($this->getAll() as $material) {
			$result[$material->getStateCode()] = $material->getName();
		}

		return $result;
	}


}
