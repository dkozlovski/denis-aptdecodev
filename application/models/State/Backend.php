<?php

class Application_Model_State_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'states';

    public function getByCountryCode($collection, $countryCode)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `country_code` = :country_code AND `is_active` = 1 ORDER BY `sort_order` ASC';

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':country_code', $countryCode);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new Application_Model_State();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    public function getByCode(\Application_Model_Abstract $object, $id)
    {
        $sql  = "SELECT * FROM `" . $this->_getTable() . "` WHERE `state_code` = :id";
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function getByTitle(\Application_Model_Abstract $object, $id)
    {
        $sql  = "SELECT * FROM `" . $this->_getTable() . "` WHERE `name` = :id AND `country_code` = 'US'";
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function getAll($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `is_active` = 1 ORDER BY `sort_order` ASC';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new $this->_itemClass();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    protected function _update(\Application_Model_Abstract $object)
    {
        $sql    = "UPDATE `" . $this->_getTable() . "` set `path` = :path, `user_id` = :user_id WHERE `id` = :id";
        $stmt   = $this->_getConnection()->prepare($sql);
        $path   = $object->getPath();
        $userId = $object->getUserId();
        $id     = $object->getId();

        $stmt->bindParam(':path', $path);
        $stmt->bindParam(':user_id', $userId);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $object)
    {
        $sql    = "INSERT INTO `" . $this->_getTable() . "` (`path`, `user_id`) VALUES (:path, :user_id)";
        $stmt   = $this->_getConnection()->prepare($sql);
        $path   = $object->getPath();
        $userId = $object->getUserId();

        $stmt->bindParam(':path', $path);
        $stmt->bindParam(':user_id', $userId);

        $stmt->execute();
        $object->setId($this->_getConnection()->lastInsertId());
    }

}
