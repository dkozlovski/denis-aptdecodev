<?php

class Application_Model_CronJob extends Application_Model_Abstract
{

    public function doCron()
    {
        $payout = $this->initPayout();

        if ($payout->getId()) {
            $user = new User_Model_User($payout->getUserId());
            if ($user->getMerchant()->getBankAccountUri() && $user->getMerchant()->getAccountUri()) {
                //$bankAccountUri = $user->getMerchant()->getBankAccountUri();
                $bankAccountUri = $user->getMerchant()->getAccountUri();

                $amount = $payout->getTotalPaid() * 100; //amount in cents

                $marketplace = new Ikantam_Balanced_Marketplace();

                try {
                    if ($amount > 50) {
                        $item = new Order_Model_Item($payout->getItemId());

                        $credit = $marketplace->createCredit($bankAccountUri, $amount);

                        $payout->setStatus($credit->getStatus())
                                ->setCreatedAt(strtotime($credit->getCreatedAt()))
                                ->setCreditUri($credit->getUri())
                                ->setIsProcessed(1)
                                ->setTotalPaid($credit->getAmount() / 100)
                                ->save();

                        $mailer = new Notification_Model_Order_PaymentSent();
                        $mailer->sendEmails($item);
                    } else {
                        $payout->setStatus('pending')
                                ->setCreatedAt(time())
                                ->setCreditUri('')
                                ->setIsProcessed(1)
                                ->setTotalPaid(0)
                                ->save();
                        
                        $this->_logError($payout);
                    }
                } catch (Ikantam_Balanced_Exception $e) {
                    //$this->sendFailureEmails($item);
                    die();
                }

                if (Ikantam_Balanced_Settings::getOwnerUri()) {
                    $amount = ($payout->getSummaryPrice() - $payout->getTotalPaid() + $payout->getShippingPrice()) * 100; //amount in cents

                    if ($amount > 0) {
                        $marketplace = new Ikantam_Balanced_Marketplace();
                        $credit      = $marketplace->createCredit(Ikantam_Balanced_Settings::getOwnerUri(), $amount);
                    }
                }
            }
        }
    }
    
    protected function _logError($payout)
    {
       $fileName = APPLICATION_PATH . '/log/payout.log';
       
       $data = date('Y-m-d H:i:s') . ': Invalid amount for payout #' . $payout->getId() . "\n";
       
       file_put_contents($fileName, $data, FILE_APPEND);
    }

    protected function initPayout()
    {
        $payout = new Order_Model_Payout();
        return $payout->getOneForCronJob();
    }

    protected function sendFailureEmails($item)
    {
        $seller = $item->getProduct()->getSeller();

        if (!$seller->getMainEmail()) {
            return;
        }
        $this->view          = new Zend_View();
        $this->view->setBasePath(realpath(APPLICATION_PATH . '/views'));
        $this->view->setScriptPath(realpath(APPLICATION_PATH . '/views/scripts'));
        
        $this->view->product = $item->getProduct();
        $this->view->item    = $item;
        $this->view->seller  = $seller;

        $output = $this->view->render('email_notification_funds_release_failed.phtml');

        $toSend = array(
            'email'   => $seller->getMainEmail(),
            'subject' => 'Payment sent - ' . $item->getProduct()->getTitle(),
            'body'    => $output);

        $mail = new Ikantam_Mail($toSend, 'utf-8', 'hello@aptdeco.com');
        $mail->sendCopy($output);
        $mail->send();
    }

}
