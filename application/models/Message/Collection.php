<?php

class Application_Model_Message_Collection extends Application_Model_Abstract_Collection
{

	public function getByRecipientId($id, $offset = null, $limit = null)
	{
		$this->_getBackend()->getByRecipientId($this, $id, $offset, $limit);
		return $this;
	}

	public function getNewByRecipientId($id, $offset = null, $limit = null)
	{
		$this->_getBackend()->getByRecipientId($this, $id, $offset, $limit, true);
		return $this;
	}

	public function getByAuthorId($id, $offset, $limit)
	{
		$this->_getBackend()->getByAuthorId($this, $id, $offset, $limit);
		return $this;
	}
	
	public function getConversationByUserIds($recipientId, $authorId)
	{
		$this->_getBackend()->getConversationByUserIds($this, $recipientId, $authorId);
		return $this;
	}

}