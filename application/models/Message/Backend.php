<?php

class Application_Model_Message_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'messages';
    protected $_itemClass = 'Application_Model_Message';

    public function getByAuthorId($collection, $id, $offset, $limit)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` 
            WHERE `author_id` = :author_id AND `is_visible_to_author` = 1
            ORDER BY `created_at` DESC';
        
        if (!is_null($offset) && !is_null($limit)) {
            $sql .= ' LIMIT :offset, :limit';
        }

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':author_id', $id);
        
        if (!is_null($offset) && !is_null($limit)) {
            $stmt->bindParam(':offset', $offset);
            $stmt->bindParam(':limit', $limit);
        }
        
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new $this->_itemClass();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }
    
    public function getByRecipientId($collection, $id, $offset, $limit, $isNew = false)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` 
			WHERE `recipient_id` = :user_id AND `is_visible_to_recipient` = 1';
        
        if ($isNew) {
            $sql .= ' AND `is_new` = 1';
        }
        
        $sql .= ' ORDER BY `created_at` DESC';
        
        if (!is_null($offset) && !is_null($limit)) {
            $sql .= ' LIMIT :offset, :limit';
        }
     
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':user_id', $id);
        
        if (!is_null($offset) && !is_null($limit)) {
            $stmt->bindParam(':offset', $offset);
            $stmt->bindParam(':limit', $limit);
        }

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new $this->_itemClass();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

    protected function _insert(Application_Model_Abstract $object)
    {
        $sql = 'INSERT INTO `' . $this->_getTable() . '` 
			(`id`, `parent_id`, `author_id`, `recipient_id`, `subject`,
                `text`, `created_at`, `is_new`, `is_visible_to_author`, `is_visible_to_recipient`) 
			VALUES (NULL, :parent_id, :author_id, :recipient_id, :subject,
                :text, :created_at, :is_new, :is_visible_to_author, :is_visible_to_recipient)';

        $stmt = $this->_getConnection()->prepare($sql);

        $parentId = $object->getParentId();
        $authorId = $object->getAuthorId();
        $recipientId = $object->getRecipientId();
        $subject = $object->getSubject();
        $text = $object->getText();
        $createdAt = $object->getCreatedAt();
        $isNew = $object->getIsNew();
        $isVisibleToAuthor = $object->getIsVisibleToAuthor();
        $isVisibleToRecipient = $object->getIsVisibleToRecipient();

        $stmt->bindParam(':parent_id', $parentId);
        $stmt->bindParam(':author_id', $authorId);
        $stmt->bindParam(':recipient_id', $recipientId);
        $stmt->bindParam(':subject', $subject);
        $stmt->bindParam(':text', $text);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':is_new', $isNew);
        $stmt->bindParam(':is_visible_to_author', $isVisibleToAuthor);
        $stmt->bindParam(':is_visible_to_recipient', $isVisibleToRecipient);

        $stmt->execute();

        $object->setId($this->_getConnection()->lastInsertId());
    }

    protected function _update(Application_Model_Abstract $object)
    {
        $sql = 'UPDATE `' . $this->_getTable() . '` SET
            
			`parent_id` = :parent_id, `author_id` = :author_id,
                `recipient_id` = :recipient_id, `subject` = :subject, `text` = :text, 
                `created_at` = :created_at, `is_new` = :is_new,
                `is_visible_to_author` = :is_visible_to_author, `is_visible_to_recipient` = :is_visible_to_recipient
            WHERE `id` = :id';

        $stmt = $this->_getConnection()->prepare($sql);

        $id = $object->getId();
        $parentId = $object->getParentId();
        $authorId = $object->getAuthorId();
        $recipientId = $object->getRecipientId();
        $subject = $object->getSubject();
        $text = $object->getText();
        $createdAt = $object->getCreatedAt();
        $isNew = $object->getIsNew();
        $isVisibleToAuthor = $object->getIsVisibleToAuthor();
        $isVisibleToRecipient = $object->getIsVisibleToRecipient();

        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':parent_id', $parentId);
        $stmt->bindParam(':author_id', $authorId);
        $stmt->bindParam(':recipient_id', $recipientId);
        $stmt->bindParam(':subject', $subject);
        $stmt->bindParam(':text', $text);
        $stmt->bindParam(':created_at', $createdAt);
        $stmt->bindParam(':is_new', $isNew);
        $stmt->bindParam(':is_visible_to_author', $isVisibleToAuthor);
        $stmt->bindParam(':is_visible_to_recipient', $isVisibleToRecipient);

        $stmt->execute();
    }

    /**
     * 
     * @param Application_Model_Abstract_Collection $collection
     * @param string|numeric $userId
     * @param string|numeric $messageId
     */
    public function getConversationByUserIds($collection, $recipientId, $authorId)
    {
        $sql = 'SELECT distinct(`messages`.`id`) as `id`, `messages2`.`subject`, `messages2`.`text`, `messages2`.`created_at`
FROM `messages`
JOIN `messages_users` ON `messages`.`id` = `messages_users`.`message_id`

JOIN `messages` as `messages2` ON `messages`.`id` = `messages2`.`id`


WHERE (
`messages`.`author_id` = :author_id1
AND `messages`.`recipient_id` = :recipient_id1
)
OR (
`messages`.`author_id` = :author_id2
AND `messages`.`recipient_id` = :recipient_id2
)
AND `messages_users`.`is_visible` =1
ORDER BY `messages`.`created_at` DESC';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':author_id1', $authorId);
        $stmt->bindParam(':recipient_id1', $recipientId);

        $stmt->bindParam(':author_id2', $recipientId);
        $stmt->bindParam(':recipient_id2', $authorId);

        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows) {
            foreach ($rows as $row) {
                $item = new User_Model_Message();
                $item->addData($row);
                $collection->addItem($item);
            }
        }
    }

}