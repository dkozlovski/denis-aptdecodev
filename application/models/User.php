<?php
/**
 * Class Application_Model_User
 * @method Application_Model_User_Backend _getbackend()
 *
 * @method string|null getEmail()
 * @method string getFullName()
 * @method string getPassword()
 * @method string|null getSex()
 * @method |null getDateOfBirth()
 * @method float|null getRating()
 * @method string|null getDetails()
 * @method int getIsActive()
 * @method int getIsUserDeleted()
 * @method int|null getMessagesBlockedUp()
 * @method string|null getBpCustomerUri()
 * @method int|null getCreatedAt()
 * @method int|null getSellerPopularity()
 *
 * @method Application_Model_User setEmail(string $value)
 * @method Application_Model_User setFullName(string $value)
 * @method Application_Model_User setPassword(string $value)
 * @method Application_Model_User setSex(string $value)
 * @method Application_Model_User setDateOfBirth( $value)
 * @method Application_Model_User setRating(float $value)
 * @method Application_Model_User setDetails(string $value)
 * @method Application_Model_User setIsActive(int $value)
 * @method Application_Model_User setIsUserDeleted(int $value)
 * @method Application_Model_User setMessagesBlockedUp(int $value)
 * @method Application_Model_User setBpCustomerUri(string $value)
 * @method Application_Model_User setCreatedAt(int $value)
 * @method Application_Model_User setSellerPopularity(int $value)
 */
class Application_Model_User extends Application_Model_Abstract implements
Zend_Acl_Role_Interface, Application_Interface_ObjectStorage_SymbolTable, Ikantam_EAV_Interface_Entity
{

    //See app_settings
    /**
     * When user has refused verification
     */
    const VERIFICATION_USER_REJECTED = '-1';

    /**
     * When the user has not been proposed verification
     */
    const VERIFICATION_NOT_REQUESTED = '0';

    /**
     * When the user has requested verification
     */
    const VERIFICATION_REQUESTED = '1';

    /**
     * When admin has approved verification
     */
    const VERIFICATION_VERIFIED = '2';

    /**
     *  When admin has reject verification
     */
    const VERIFICATION_REJECTED = '3';

    protected $_backendClass        = 'Application_Model_User_Backend';
    protected $_invitation;
    protected $_wishlist;
    protected $_messagebox;
    protected $_emailCollection;
    protected $_primaryEmail;
    protected $_phoneCollection;
    protected $_merchant;
    protected $_balancedAccount;
    protected $_symbolTableName;
    protected static $_locatedInNYC = array();

    public function save()
    {
        $cache = Zend_Registry::get('output_cache');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array('search_items'));
        parent::save();
    }

    public function getActiveById($id)
    {
        $this->_getBackend()->getActiveById($this, $id);

        return $this;
    }

    public function getFirstName()
    {
        $names = explode(' ', $this->getFullName());
        if (isset($names[0])) {
            return trim($names[0]);
        }
        return $this->getFullName();
    }

    public function __construct($id = null)
    {
        if ($id) {
            $this->getById($id);
        }
    }

    public function getAvatar($notCropped = false)
    {
        $avatar = new Application_Model_Avatar();
        $avatar->getByUserId($this->getId(), $notCropped);
        return $avatar;
    }

    public function getAvatarUrl()
    {
        return $this->getAvatar()->getUrl();
    }

    public function getAvatarThumbnailUrl()
    {
        return $this->getAvatar()->getThumbnailUrl();
    }

    public function authenticate($loginData)
    {
        if (!isset($loginData['email'], $loginData['password'])) {
            throw new Ikantam_Exception_Auth('Email and Password Required');
        }

        $this->getByEmail($loginData['email']);

        if (!$this->isPasswordValid($loginData['password']) || !$this->getIsActive()) {
            throw new Ikantam_Exception_Auth('The email address or the password that you entered does not match our records.');
        }

        return $this;
    }

    public function register($signupData)
    {
        try {
            /* if (!$this->getInvitation()->isValid($signupData)) {
              throw new Ikantam_Exception_Signup('Invalid Invitation Code');
              } */

            $this->setData($signupData)
                    ->setPassword($this->hash($this->getPassword()))
                    ->save();

            $this->addEmail($this->getEmail(), true); //add and set email as primary
            //$this->getInvitation()->setIsUsed(true)->save();

            return $this;
        } catch (PDOException $exc) {
            if ($exc->getCode() == "23000") {
                throw new Ikantam_Exception_Signup('This email is already taken');
            }
        }

        return $this;
    }

    public function registerFacebook($fullName, $dateofbirth)
    {
        $this->setFullName($fullName)
                ->setPassword($this->hash(Ikantam_Math_Rand::get62String(128)));

        if (strtotime($dateofbirth)) {
            $this->setDateOfBirth(date('Y-m-d', strtotime($dateofbirth)));
        }

        $this->save();

        return $this;
    }

    public function getInvitation()
    {
        if (!$this->_invitation) {
            $this->_invitation = new Application_Model_Invitation();
        }

        return $this->_invitation;
    }

    //@TODO move somewhere
    public function isPasswordValid($password)
    {
        $bcrypt = new Ikantam_Crypt_Password_Bcrypt();

        return $bcrypt->verify($password, $this->getPassword());
    }

    //@TODO move somewhere
    public function hash($password)
    {
        $bcrypt = new Ikantam_Crypt_Password_Bcrypt();

        return $bcrypt->create($password);
    }

    public function getByEmail($email)
    {
        $this->_getBackend()->getByEmail($this, $email);

        return $this;
    }

    public function forgotPassword($data)
    {
        $this->getByEmail($data['email']);

        if ($this->getId() && $this->getEmail()) {
            $passwordReset = new User_Model_PasswordReset();
            $code          = Ikantam_Math_Rand::get62String(32);
            $passwordReset->setUserEmail($this->getEmail())
                    ->setExpiryTime(time() + 3600)
                    ->setCode($code)
                    ->setIsActive(true)
                    ->save();


            $toSend = array(
                'email'   => $this->getEmail(),
                'subject' => 'Password recovery at AptDeco',
                'body'    => '<html>
				<body>
				<p><img src="' . Ikantam_Url::getPublicUrl('images/invite-logo.png') . '" /></p>
				<p>Hello,</p>

<p>Please use the link below to reset your password at AptDeco:</p>

<p><a href="' . Ikantam_Url::getUrlWithGaParams(Ikantam_Url::getUrl('user/password/reset/', array('code' => $code))) . '">Reset Password</a></p>

<p>Thanks,</p>
<p>AptDeco team</p>
</body>
</html>');

            try {
                $mail = new Ikantam_Mail($toSend);
                $mail->send(null, true);
                return true;
            } catch (Exception $e) {
                
            }
        }

        return false;
    }

    public function getUrl($params, $params2 = null, $reset = false)
    {
        $params = array_reverse(explode('/', trim($params, '/')));

        $action     = !empty($params[0]) ? $params[0] : null;
        $controller = !empty($params[1]) ? $params[1] : null;
        $module     = !empty($params[2]) ? $params[2] : null;
        if (!$params2)
            $params2    = array();
        $view       = new Zend_View();
        $d          = array('module' => $module, 'controller' => $controller, 'action' => $action);

        return $view->serverUrl() . $view->url(array_merge($d, $params2), null, $reset);
    }

    public function getViewProfileUrl()
    {
        return Ikantam_Url::getUrl('user/profile/view', array('id' => $this->getId()));
    }

    public function getProfileUrl()
    {
        return Ikantam_Url::getUrl('user/view/index', array('id' => $this->getId()));
    }

    public function getViewProfileLink()
    {
        return '<a href="' . $this->getViewProfileUrl() . '">' . $this->getFullName() . '</a>';
    }

    public function resetPassword($data)
    {
        $reset = new User_Model_PasswordReset();
        $reset->getByCode($data['code']);

        $this->getByEmail($reset->getUserEmail());

        if ($this->getId() && $reset->getExpiryTime() > time()) {
            $this->setPassword($this->hash($data['password']));
            $this->save();
            $reset->delete();

            return $this->getEmail();
        }
        return false;
    }

    public function getAddresses()
    {
        $addresses = new User_Model_Address_Collection();
        $addresses->getByUserId($this->getId());
        return $addresses;
    }

    /**
     * @return User_Model_Address
     */
    public function getPrimaryAddress()
    {
        $addresses = new User_Model_Address_Collection();
        return $addresses->getPrimaryByUserId($this->getId());
    }

    public function getWishlist()
    {
        if (!$this->_wishlist) {
            $this->_wishlist = new Wishlist_Model_Wishlist();
            $this->_wishlist->setUserId($this->getId());
        }
        return $this->_wishlist;
    }

    public function isRegistred()
    {
        if ($this->getId())
            return true;
        return false;
    }

    public function getMessagebox()
    {
        if (!$this->_messagebox) {
            //$this->setId(1);
            $this->_messagebox = Messagebox_Model_Messagebox::getInstance($this->getId());
        }
        return $this->_messagebox;
    }

    public function getOutboxMessages()
    {
        $collection = new User_Model_Message_Collection();
        $collection->getByAuthorId($this->getId());
        return $collection;
    }

    public function canSendMessagesTo(Application_Model_User $user)
    {
        if (!$user->getId() || $this->getId() == $user->getId()) {
            return false;
        }

        return true;
    }

    public function canReview(Application_Model_User $user)
    {
        if (!$user->getId() || $this->getId() == $user->getId()) {
            return false;
        }

        return ($this->_getBackend()->getUserBuyFromUser($this->getId(), $user->getId()) > 0);
    }

    public function recalculateRating()
    {
        $reviews = new User_Model_Review_Collection();
        $reviews->getByUserId($this->getId());

        $total = 0;
        $count = 0;
        foreach ($reviews as $review) {
            if ($review->getRating()) {
                $total += $review->getRating();
                $count++;
            }
        }

        if ($count > 0) {
            $this->setRating(round($total / $count))->save();
        }
    }

    //---------------EMAIL    
    public function getEmailCollection($reload = false)
    {
        if (!$this->_emailCollection || $reload) {
            $this->_emailCollection = new User_Model_Email_Collection();
            $this->_emailCollection->getByUserId($this->getId());
        }

        return $this->_emailCollection;
    }

    /**
     * @param string $email 
     */
    public function addEmail($email, $setPrimary = false)
    {
        $_email = new User_Model_Email();
        $_email->setEmail($email)
                ->setUserId($this->getId())
                ->save();

        $this->getEmailCollection(true);

        if ($setPrimary) {
            $this->changePrimaryEmail($_email->getId());
        }
        return $this;
    }

    public function getMainEmail()
    {
        $email = $this->getPrimaryEmail();
        if ($email->getId()) {
            return $email->getEmail();
        }
        return $this->getEmail();
    }

    public function getPrimaryEmail()
    {
        if (!$this->isExists()) {
            return new User_Model_Email();
        }

        if ($this->_primaryEmail) {
            return $this->_primaryEmail;
        }
        $email = $this->_getbackend()->getPrimaryEmail($this->getId());

        if (!$email->isExists()) {
            $email->setUserId($this->getId())
                    ->setIsPrimary(1)
                    ->setEmail($this->getEmail())
                    ->save();
        }


        return $this->_primaryEmail = $email;
    }

    public function changePrimaryEmail($id)
    {
        if ($id == 0) {
            foreach ($this->getEmailCollection()->getItems() as $email) {
                $email->setIsPrimary(0)->save();
            }
            return;
        }

        $email = new User_Model_Email($id);

        if ($email->getUserId() != $this->getId()) {
            return $this;
        }

        foreach ($this->getEmailCollection()->getItems() as $email) {
            if ($email->getIsPrimary()) {
                if ($email->getId() == $id)
                    return $this;
                $email->setIsPrimary(false)
                        ->save();
            }

            if ($email->getId() == $id) {
                $email->setIsPrimary(true)
                        ->save();
            }
        }
        return $this;
    }

//--------------PHONE

    public function getPhoneCollection($reload = false)
    {
        if (!$this->_phoneCollection || $reload) {
            $this->_phoneCollection = new User_Model_Phone_Collection();
            $this->_phoneCollection->getByUserId($this->getId());
        }

        return $this->_phoneCollection;
    }

    /**
     * @param string $phone 
     */
    public function addPhone($phone, $setPrimary = false)
    {
        $_phone = new User_Model_Phone();
        $_phone->setPhone($phone)
                ->setUserId($this->getId())
                ->save();

        $this->getPhoneCollection(true);

        if ($setPrimary) {
            $this->changePrimaryPhone($_phone->getId());
        }

        return $this;
    }

    public function getPrimaryPhone()
    {
        foreach ($this->getPhoneCollection()->getItems() as $phone) {
            if ($phone->getIsPrimary()) {
                return $phone;
            }
        }

        if (count($this->getPhoneCollection()->getItems()) > 0) {
            $phones = $this->getPhoneCollection()->getItems();
            $phone  = reset($phones);
            return $phone->setIsPrimary(1);
        }

        return new \User_Model_Phone();
    }

    public function changePrimaryPhone($id)
    {
        $phone = new User_Model_Phone();
        if ($phone->getById((int) $id)->getUserId() != $this->getId())
            return $this;
        foreach ($this->getPhoneCollection()->getItems() as $phone) {
            if ($phone->getIsPrimary()) {
                if ($phone->getId() == $id)
                    return $this;
                $phone->setIsPrimary(false)
                        ->save();
            }

            if ($phone->getId() == $id) {
                $phone->setIsPrimary(true)
                        ->save();
            }
        }
        return $this;
    }

    public function getReviews()
    {
        $review = new User_Model_Review_Collection();
        return $review->getByUserId($this->getId());
    }

    public function getCategories()
    {
        $categories = new Category_Model_Category_Collection();
        return $categories->getByUser($this->getId());
    }

    public function getReviewsUrl()
    {
        return Ikantam_Url::getUrl('user/view/reviews', array('id' => $this->getId()));
    }

    public function getCategoryUrl(Category_Model_Category $category)
    {
        return Ikantam_Url::getUrl('user/view/index', array('id' => $this->getId(), 'category' => $category->getId()));
    }

    public function getPurchaseHistory($options = null, $page = 1, $itemsPerPage = 10, $searchValue = null)
    {
        $products = new Product_Model_Product_Collection();
        $products->getPurchaseHistory($this->getId(), $options, $page, $itemsPerPage, $searchValue);
        return $products;
    }

    public function getSalesCount()
    {
        $pbe = new Product_Model_Product_Backend();
        return $pbe->getSalesCount($this->getId());
    }

    public function getPurchaseCount()
    {
        $pbe = new Product_Model_Product_Backend();
        return $pbe->getPurchaseCount($this->getId());
    }

    public function getOrderList($options = null, $page = 1, $itemsPerPage = 10, $searchValue = null)
    {
        $products = new Product_Model_Product_Collection();
        $products->getOrderList($this->getId(), $options, $page, $itemsPerPage, $searchValue);
        return $products;
    }

    public function getProductsForSale($options = null, $page = 1, $itemsPerPage = 10, $searchValue = null)
    {
        $products = new Product_Model_Product_Collection();
        $products->getProductsForSale($this->getId(), $options, $page, $itemsPerPage, $searchValue);
        return $products;
    }

    public function getSalableProducts()
    {
        $params     = array('user_id' => $this->getId(), 'is_published' => 1);
        $collection = new Product_Model_Product2_Collection();
        return $collection->getProducts($params);
    }

    public function getMerchant()
    {
        if (!$this->_merchant) {
            $this->_merchant = new User_Model_Merchant();
            $this->_merchant->getByUserId($this->getId());
        }
        return $this->_merchant;
    }

    public function getAccountBalance()
    {
        return $this->_getBackend()->getAccountBalance($this->getId());
    }

    public function getBalancedAccount()
    {
        if (!$this->_balancedAccount) {
            $balancedAccount = new User_Model_BalancedAccount();
            $balancedAccount->getByUserId($this->getId());

            if (!$balancedAccount->getId() && $this->getId()) {
                $marketplace = new Ikantam_Balanced_Marketplace();

                $data = array(
                    'meta'          => null,
                    'name'          => $this->getFullName(),
                    'email_address' => $this->getEmail(),
                );

                $buyer = $marketplace->createBuyer($data);

                $balancedAccount = new User_Model_BalancedAccount();
                $balancedAccount->setData($buyer->getData())
                        ->setId(null)
                        ->setRole(null)
                        ->setCreatedAt(time())
                        ->setEmailAddress($this->getEmail())
                        ->setUserId($this->getId())
                        ->setIsBuyer(0)
                        ->setIsMerchant(0)
                        ->save();
            }

            $this->_balancedAccount = $balancedAccount;
        }
        return $this->_balancedAccount;
    }

    public function getBpCustomer()
    {
        $bpCustomerUri = $this->getBpCustomerUri();

        if (!$bpCustomerUri) {
            return false;
        }

        Httpful\Bootstrap::init();
        RESTful\Bootstrap::init();
        Balanced\Bootstrap::init();
        Balanced\Settings::init();

        try {
            $bpCustomer = Balanced\Customer::get($bpCustomerUri);

            if ($bpCustomer->uri === $bpCustomerUri) {
                return $bpCustomer;
            }
        } catch (Exception $exc) {
            
        }

        return false;
    }

    /** When user registers himself 
     * @param  array data (fullname, email, password)
     * @return mixed - bool true on success or error string on failure
     */
    public function registerUser(array $data)
    {
        if (!$this->isExists()) {

            try {
                $this->setData($data)
                        ->setPassword($this->hash($this->getPassword()))
                        ->save();
            } catch (PDOException $e) {
                if ($e->errorInfo[1] == 1062) {
                    return 'The email address "' . $this->getEmail() . '" is already registered';
                }

                throw $e;
            }

            $this->addEmail($this->getEmail(), true);

            return true;
        }

        return 'User already registered.';
    }

    //Zend_Acl_Role_Interface
    public function getRoleId()
    {
        return 'User';
    }

    public function getRewards()
    {
        return $this->_getBackend()->getRewards($this);
    }

    public function addReward($amount)
    {
        $this->_getBackend()->addReward($this, $amount);
        return $this;
    }

    public function subReward($amount)
    {
        $this->_getBackend()->subtractReward($this, $amount);
        return $this;
    }

    /**
     * get settings model
     * 
     * @return object User_Model_Settings
     */
    protected function _getUserSettingsObject()
    {
        return new User_Model_Settings();
    }

    /**
     * Get/set personal settings for user
     * 
     * @param  string $name - setting name
     * @param  string $value (Optional) - set mode if passed
     * @return mixed (setting_object in set mode string in get mode)
     */
    public function personalSettings($name, $value = null)
    {
        $originalName = $name;
        $name         = 'user_' . $name;
        if (null === $value) {
            return $this->_getUserSettingsObject()->getValueByCondition(array(
                        'user_id'          => array('=', $this->getId()),
                        'app_setting_name' => array('=', $name),
            ));
        }
        $settingObject = $this->_getUserSettingsObject();
        if (!$this->isExists()) {
            $session = \User_Model_Session::instance();
            if ($session->hasUserSettings()) {
                $settings = $session->getUserSettings();
            } else {
                $settings = array();
            }
            $settings[$originalName] = $value;
            $session->setUserSettings($settings);
            return $settingObject;
        }
        return $settingObject->saveSetting($this, $name, $value); // save or overwrite
    }

    /**
     * Check if user is verified
     * @return bool
     */
    public function isVerified()
    {
        return $this->personalSettings('product_verification_available') == self::VERIFICATION_VERIFIED;
    }

    /**
     * Marks a user as a spammer
     */
    public function markAsSpammer()
    {
        $mark = (int) $this->personalSettings('spammer');

        if ($mark) {
            // if user already has been marked
            $this->setMessagesBlockedUp(strtotime('+10 years'))
                    ->save();
        } else {
            // if it is the first time mark
            $this->setMessagesBlockedUp(strtotime('+15 minutes'))
                    ->save();
        }
        $mark++;

        $this->personalSettings('spammer', $mark);
    }

    /**
     * Check if user allowed to send messages
     *
     * @return bool
     */
    public function canSendMessages()
    {
        return $this->getMessagesBlockedUp() < time();
    }

    /**
     * Sets name that will be used for current symbol table
     * @param string $name
     * @throws InvalidArgumentException
     * @return mixed
     */
    public function setSymbolTableName($name)
    {
        //http://www.php.net/manual/en/language.variables.basics.php
        if (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $name)) {
            throw new InvalidArgumentException('Invalid variable name.');
        }
        $this->_symbolTableName = $name;
    }

    /**
     * Retrieve name to current symbol table
     * @return mixed
     */
    public function getSymbolTableName()
    {
        return $this->_symbolTableName;
    }

    public function initFacebook()
    {
        include_once(APPLICATION_PATH . '/../library/Facebook/facebook.php'); //include facebook SDK
        //$config = $this->getXmlConfig('additional_settings', 'facebook');
        $config = Zend_Registry::get('config')->facebook;

        $facebook = new Facebook(array(
            'appId'  => $config->appId,
            'secret' => $config->appSecret
        ));
        return $facebook;
    }

    public function canFacebookPosting()
    {
        $facebook = $this->initFacebook();

        $facebookToken = $this->personalSettings('facebook_token');
        if (!$facebookToken) {
            return false;
        }
        $facebook->setAccessToken($facebookToken);
        $fbUser = $facebook->getUser();
        return (bool) $fbUser;
    }

    public function postingToFacebook($message, $object = null)
    {

        $facebook      = $this->initFacebook();
        $facebookToken = $this->personalSettings('facebook_token');
        $facebook->setAccessToken($facebookToken);

        try {
            $params['message'] = $message;
            if ($object instanceof Application_Model_Abstract) {
                if ($object instanceof Product_Model_Product) {
                    $params['source'] = $object->getMainImageUrl();
                    $params['link']   = $object->getProductUrl();
                } elseif ($object instanceof User_Model_User) {
                    $params['source'] = $object->getAvatarUrl();
                    $params['link']   = $object->getProfileUrl();
                }
            }

            $postResult = $facebook->api('/me/feed', 'post', $params);
            return $postResult;
        } catch (FacebookApiException $e) {
            return false;
        }
    }

    public function canTwitterPosting()
    {
        require_once(APPLICATION_PATH . '/../library/twitteroauth/twitteroauth.php');

        $config = $this->getXmlConfig('additional_settings', 'twitter');

        $token       = $this->personalSettings('twitter_token');
        $tokenSecret = $this->personalSettings('twitter_token_secret');

        if (empty($token) || empty($tokenSecret)) {
            return false;
        }

        $connection = new TwitterOAuth($config->appId, $config->appSecret, $token, $tokenSecret);

        /* If method is set change API call made. Test is called by default. */
        $twUser = $connection->get('account/verify_credentials');
        return isset($twUser->id);
    }

    public function postingToTwitter($message)
    {
        require_once(APPLICATION_PATH . '/../library/twitteroauth/twitteroauth.php');

        $config = $this->getXmlConfig('additional_settings', 'twitter');

        $token       = $this->personalSettings('twitter_token');
        $tokenSecret = $this->personalSettings('twitter_token_secret');

        if (empty($token) || empty($tokenSecret)) {
            return false;
        }

        $connection = new TwitterOAuth($config->appId, $config->appSecret, $token, $tokenSecret);
        $postResult = $connection->post('statuses/update', array('status' => $message));

        return isset($postResult->id);
    }

    /**
     * Determine whether user located in NYC
     * @return bool
     */
    public function isUserLocatedInNYC()
    {
        if (!$this->isExists()) {
            return false;
        }

        // This is expensive operation, so we save the result in static array (common for all)
        if (!isset(self::$_locatedInNYC[$id = $this->getId()])) {
            $address = $this->getPrimaryAddress();
            $filter  = new Shipping_Model_Borough_FlexFilter(new Shipping_Model_Borough_Collection);
            $filter->name('in', Shipping_Model_Rate::getFlatRateBoroughs())
                    ->postal_code('=', $address->getPostalCode($address->getPostcode()));

            self::$_locatedInNYC[$id] = in_array(
                            trim(preg_replace('/[\s,-]+/', ' ', strtoupper($address->getCity()))), array('NEW YORK', 'NY', 'NYC', 'NEW YORK CITY')
                    ) && $filter->count();
        }

        return self::$_locatedInNYC[$id];
    }

    /**
     * Return unique identifier for entity
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->getId();
    }

    /**
     * Return entity family (common for group)
     * @return string
     */
    public function getEntityFamily()
    {
        return 'users';
    }

    protected function getXmlConfig($config, $section = null, $options = false)
    {
        if (substr($config, -4) != '.xml') {
            $config .= '.xml';
        }
        $cfg = new Zend_Config_Xml(APPLICATION_PATH . '/configs/' . $config, $section, $options);
        return $cfg;
    }

}