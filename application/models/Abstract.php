<?php

/**
 * Abstract model
 * 
 * @method int getId()
 * 
 */
class Application_Model_Abstract extends Ikantam_Object
{

    protected $_backendClass;
    protected $_stored;

    public function __construct($id = null)
    {
        if (!$this->_backendClass) {
            $this->_backendClass = get_class($this) . '_Backend';
        }

        if (is_numeric($id)) {
            $this->getById($id);
        }

        if (is_array($id)) {
            $this->addData($id);
        }

        if ($this->isExists()) {
            $this->_stored = $this->getData();
        }
    }

    protected function _getbackend()
    {
        return new $this->_backendClass();
    }

    public function getById($id)
    {
        $this->_getBackend()->getById($this, $id);

        return $this;
    }

    protected function _beforeSave()
    {
        
    }

    public function save()
    {
        $this->_beforeSave();
        $this->_getbackend()->save($this);
        $this->_afterSave();
        return $this;
    }

    protected function _afterSave()
    {
        
    }

    public function delete()
    {
        $this->isDeleted(true);
        $this->setIsDeleted(true)->save()->unsId();
        //@TODO unset data?
    }

    /** Retreives first matched data from DB where field name is part after getBy_ and condition value is first argument.
     * getBy_table_field_name
     *
     * @param   string $method
     * @param   array $args
     * @return  mixed
     */
    public function __call($method, $args)
    {
        if (substr($method, 0, 6) == 'getBy_') {
            $field = substr($method, 6, strlen($method));
            $this->_getbackend()->getByFieldValue__($this, $field, $args[0]);
            return $this;
        } elseif (substr($method, 0, 3) == 'set' && $this->isExists()) {
            //store old data
            if (empty($this->_stored)) {
                $this->_stored = $this->getData();
            }
        }

        return parent::__call($method, $args);
    }

    public function addData(array $data)
    {
        if (empty($this->_stored)) {
            $this->_stored = $this->getData();
        }
        return parent::addData($data);
    }

    /**
     * Return stored data (old object data wich was saved when setter method was called)
     * 
     * @param string|array $key (Optional) - leave empty if need to return all data(array)
     * @return mixed
     */
    public function getStored($key = null)
    {
        if (null === $key) {
            return $this->_stored;
        }
        return @$this->_stored[$key] ? : null;
    }

    /**
     * Check if object was changed i.e. object exist and one of its properties was changed
     *
     * @return bool 
     */
    public function isChanged()
    {
        if (!$this->isExists()) {
            return false;
        } elseif (empty($this->_stored)) {
            return false;
        } else {
            $diff = array_diff_assoc($this->getData(), $this->getStored());
            return !empty($diff);
        }
        return true;
    }

    /**
     * Get list  of changes
     *
     * @return array 
     */
    public function getChanges()
    {
        if (!$stored = $this->getStored()) {
            return array();
        }
        return array_diff_assoc($this->getData(), $this->getStored());
    }

    /**
     * Get list of changes except newly added fields 
     *
     * @return array 
     */
    public function getExistChanges()
    {
        $changes = array();
        if (!$stored  = $this->getStored()) {
            return $changes;
        }
        foreach ($this->_stored as $storedField => $storedValue) {
            if (isset($this->_data[$storedField]) && $this->_data[$storedField] != $storedValue) {
                $changes[$storedField] = $this->_data[$storedField];
            }
        }
        return $changes;
    }

    /** Check if object record exists     
     * @return bool
     */
    public function isExists()
    {
        return (bool) $this->getId();
    }

}
