<?php

class Application_Model_Material_Backend extends Application_Model_Abstract_Backend
{

	protected $_table = 'materials';

	public function getAll($collection)
	{
		$sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1 ORDER BY `title` ASC';

		$stmt = $this->_getConnection()->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($result as $row) {
			$object = new $this->_itemClass();
			$object->addData($row);

			$collection->addItem($object);
		}
	}

	protected function _update(\Application_Model_Abstract $object)
	{
		$sql = "UPDATE `" . $this->_getTable() . "` set `title` = :title WHERE `id` = :id";
		$stmt = $this->_getConnection()->prepare($sql);
		$title = $object->getTitle();
		$id = $object->getId();

		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}

	protected function _insert(\Application_Model_Abstract $object)
	{
		$sql = "INSERT INTO `" . $this->_getTable() . "` (`title`) VALUES (:title)";
		$stmt = $this->_getConnection()->prepare($sql);
		$title = $object->getTitle();

		$stmt->bindParam(':title', $title);
		$stmt->execute();
		$object->setId($this->_getConnection()->lastInsertId());
	}

}
