<?php

class Application_Model_Color extends Application_Model_Abstract
{

    protected $_backendClass = 'Application_Model_Color_Backend';

    public function __construct($id = null)
    {
        if ($id) {
            $this->getById($id);
        }

        return $this;
    }

    public function getByTitle($title)
    {
        $this->_getbackend()->getByTitle($this, $title);
        return $this;
    }

}
