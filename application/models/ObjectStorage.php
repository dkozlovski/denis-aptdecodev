<?php
/**
 * Author: Alex P.
 * Date: 16.06.14
 * Time: 17:11
 */

class Application_Model_ObjectStorage extends SplObjectStorage
{
    /**
     * Relate objects classes and short names
     * @var array
     */
    protected $map = array(
        'Product_Model_Product' => 'P',
        'Application_Model_User'=> 'U',
        'Cart_Model_Cart' => 'C',
        'Application_Model_ValueExtractor' => 'VE',
    );

    public function __construct()
    {   //checks whether map contains duplicates
        if (count($this->map) !== count(array_unique($this->map))) {
            throw new Exception('Duplicate map value. Please check the objects map.');
        }
    }

    /**
     * @param object $object
     * @throws InvalidArgumentException
     */
    public function attach($object)
    {
        if (!$this->getObjectKey($object)) {
            throw new InvalidArgumentException('Unable to store passed object.');
        }
        parent::attach($object);
    }

    /**
     * Serialize attached objects
     * @return string
     */
    public function serialize()
    {
        $result = '';
        foreach ($this as $item) {
            $result .= $this->getObjectKey($item) . ':' . $item->getId();
            if ($item instanceof \Application_Interface_ObjectStorage_SymbolTable) {
                if ($name = $item->getSymbolTableName()) {
                    $result .= ':' . $name;
                }
            }
            $result .= '/';
        }
        return $result;
    }

    /**
     * Unserialize string to objects
     * @param string $serialized
     * @throws InvalidArgumentException
     */
    public function unserialize($serialized)
    {
        $itemsEncoded = explode('/', rtrim($serialized, '/'));
        if (!$itemsEncoded) {
            throw new InvalidArgumentException('Can not unserialize string.');
        }

        $map = array_combine(array_values($this->map), array_keys($this->map));

        foreach ($itemsEncoded as $itemEncoded) {
            $details = explode(':', $itemEncoded);
            $className = $map[$details[0]];
            $id = $details[1];
            $object = new $className($id);
            if ($object instanceof Application_Interface_ObjectStorage_SymbolTable) {
                if (isset($details[2])) {
                    $object->setSymbolTableName($details[2]);
                }
            }
            $this->attach($object);
        }
    }

    /**
     * Fills storage with objects.
     * @param array $data
     * @return static
     */
    public static function factory(array $data)
    {
        $storage = new static;
        foreach($data as $name => $value) {
            if ($value instanceof \Application_Interface_ObjectStorage_SymbolTable) {
                $value->setSymbolTableName($name);
            } elseif (!is_object($value) && !is_array($value) && !is_resource($value)) {
                $value = new \Application_Model_ValueExtractor('simpleValue|' . $value);
                $value->setSymbolTableName($name);
            }
            $storage->attach($value);
        }

        return $storage;
    }

    /**
     * Retrieve key that used for "serailization". Return false if nothing found.
     * @param $object
     * @return bool
     */
    protected function getObjectKey($object)
    {
        foreach ($this->map as $class => $key) {
            if ($object instanceof $class) {
                return $key;
            }
        }
        return false;
    }
} 