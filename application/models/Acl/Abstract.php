<?php

/**
 * @author Aleksei Poliushkin
 * @copyright iKantam 2013/12/20
 */
abstract class Application_Model_Acl_Abstract extends Zend_Acl
{

    protected static $_instance;

    protected function __construct()
    {
        $this->_bootstrap();
    }

    public static function getInstance()
    {
        if (!static::$_instance) {
            static::$_instance = new static();
        }
        return static::$_instance;
    }

    protected function _bootstrap()
    {
        $this->_initResources()
                ->_initRoles()
                ->_initRules();

        return $this;
    }

    protected function _initRoles()
    {
        foreach ($this->_getRolesList() as $item) {
            $params = $this->_parseItem($item);
            $this->addRole($params[0], $params[1]);
        }

        return $this;
    }

    protected function _initResources()
    {
        foreach ($this->_getResourcesList() as $item) {
            $params = $this->_parseItem($item);
            $this->addResource($params[0], $params[1]);
        }

        return $this;
    }

    protected function _initRules()
    {
        foreach ($this->_getRulesList() as $action => $list) {
            if (in_array($action = strtolower($action), array('allow', 'deny'))) {
                foreach ($list as $item) {
                    $params = $this->_parseItem($item, 4);
                    $assert = $params[3] ? new $params[3]() : null;
                    $this->{$action}($params[0], $params[1], $params[2], $assert);
                }
            }
        }

        return $this;
    }

    private function _parseItem($item, $fill_count = 2)
    {
        $item = (array) $item;

        if (!count($item)) {
            throw new Aplication_Model_Acl_Exception('Empty item given.');
        }

        $fill_count = abs($fill_count);
        while ($fill_count--) {
            $result[] = array_shift($item);
        }

        return $result;
    }

    /**
     * Source for roles 
     * @return array
     * structure:
     * array(
     *       'Guest' or array('any_key|role' => 'Guest'),       
     *       array('any_key|role' => 'General_User', 'any_key|parent' => 'Guest'|array("Guest", "Something")),
     * )
     * ...
     * similar as above
     * ...
     * array(
     *       'Guest' or array('Guest'),
     *       array('General_User', 'Guest'|array("Guest", "Something")),
     * )
     */
    protected abstract function _getRolesList();

    /**
     * Source for resources 
     * @return array
     * structure:
     * array(
     *     'File' or array('any_key|resource' => 'File'),
     *     array('any_key|resource' => 'Image', 'any_key|parent' => 'File'|array("File", "Something")),
     * )
     * ...
     * similar as above
     * ...
     * array(
     *     'File' or array('File'),
     *     array('Image', 'File'|array("File", "Something")),
     * )
     */
    protected abstract function _getResourcesList();

    /**
     * Source for Rules 
     * @return array
     * structure:
     * array (
     *     'allow' => array(  
     *         array('role', 'resource', 'privelege', 'assert' => 'Assert_Class_Name'),
     *         array('Guest', 'Image', 'View', 'assert' => 'Assert_Class_Name'),
     *         array('General_User', 'Image', array('Upload', 'Edit'))
     *     ),
     *     ...
     *     'deny' => array(  
     *         array('role', 'resource', 'privelege', 'assert' => 'Assert_Class_Name'),
     *         array('Guest', 'Image', 'View'),
     *         array('General_User', 'Image', array('Upload', 'Edit'))
     *     ),    
     * )
     */
    protected abstract function _getRulesList();

    //singleton
    protected function __wakeup()
    {
        
    }

    protected function __clone()
    {
        
    }

}