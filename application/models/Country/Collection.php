<?php

class Application_Model_Country_Collection extends Application_Model_Abstract_Collection
{

	private $_loaded = false;

	public function getAll()
	{
		if (!$this->_loaded) {
			$this->_getBackend()->getAll($this);
		}

		return $this;
	}

	public function getOptions()
	{
		$result = array();

		foreach ($this->getAll() as $country) {
			$result[$country->getCode()] = $country->getName();
		}

		return $result;
	}

    /**
     * Converts all countries to JSON that could be used as jQuery Autocomplete source
     *
     * @return string
     */
    public static function getAsJQueryAutocompleteSource()
    {
        $self = new static;
        return Zend_Json::encode(array_map(function($item){
                    /** @var Application_Model_Country $item */
                    return array(
                        'id' => $item->getCode(),
                        'value' => $item->getName(),
                        'label' => $item->getName(),

                    );
                }, $self->getAll()->getItems()));
    }


}
