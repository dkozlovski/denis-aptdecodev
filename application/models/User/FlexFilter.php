<?php
/**
 * Author: Alex P.
 * Date: 09.06.14
 * Time: 17:15
 */

/**
 * @method Application_Model_User_FlexFilter setting_name (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_setting_name (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter setting_value (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_setting_value (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter id (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_id (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter email (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_email (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter full_name (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_full_name (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter password (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_password (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter sex (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_sex (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter date_of_birth (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_date_of_birth (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter rating (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_rating (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter details (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_details (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter is_active (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_is_active (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter is_user_deleted (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_is_user_deleted (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter messages_blocked_up (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_messages_blocked_up (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter created_at (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_created_at (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter seller_popularity (string $operator, mixed $value)
 * @method Application_Model_User_FlexFilter or_seller_popularity (string $operator, mixed $value)
 * @method Application_Model_User_Collection apply(int $limit = null, int $offset = null)
 */

class Application_Model_User_FlexFilter extends Ikantam_Filter_Flex
{
    protected $_acceptClass = 'Application_Model_User_Collection';

    protected $_joins = array (
        'users_app_settings' => 'INNER JOIN `users_app_settings` ON `users_app_settings`.`user_id` = `users`.`id`',
        'app_settings' => array('users_app_settings' => 'INNER JOIN `app_settings` ON `app_settings`.`id` = `users_app_settings`.`app_setting_id`'),
    );

    protected $_rules = array(
        'setting_name' => array('app_settings' => '`app_settings`.`name`'),
        'setting_value' => array('users_app_settings' => '`users_app_settings`.`value`'),
    );
} 