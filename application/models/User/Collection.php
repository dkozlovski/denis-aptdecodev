<?php

class Application_Model_User_Collection extends Application_Model_Abstract_Collection
    implements Ikantam_Filter_Flex_Interface
{

	private $_loaded = false;
    protected $_itemObjectClass = 'Application_Model_User';

	public function getAll()
	{
		if (!$this->_loaded) {
			$this->_getBackend()->getAll($this);
		}

		return $this;
	}

    /**
     * @return Application_Model_User_FlexFilter
     */
    public function getFlexFilter()
    {
        return new Application_Model_User_FlexFilter($this);
    }
    
    public function getAllByName($name)
	{
		if (!$this->_loaded) {
			$this->_getBackend()->getAllByName($this, $name);
		}

		return $this;
	}
    
    public function getAllPaginated($offset, $limit)
	{
		if (!$this->_loaded) {
			$this->_getBackend()->getAllPaginated($this, $offset, $limit);
		}

		return $this;
	}
    
    public function getAllPaginatedByName($offset, $limit, $name)
	{
		if (!$this->_loaded) {
			$this->_getBackend()->getAllPaginatedByName($this, $offset, $limit, $name);
		}

		return $this;
	}

	public function getOptions()
	{
		$result = array();

		foreach ($this->getAll() as $material) {
			$result[$material->getId()] = $material->getFullName();
		}

		return $result;
	}
	
	public function getOptions2()
	{
		$result = array();

		foreach ($this->getAll() as $material) {
			$result[$material->getId()] = $material->getId();
		}

		return $result;
	}
    
    public static function getCountWithoutDeleted ()
    {   
        $instance = new self();
        return $instance->_getbackend()->getCountWithoutDeleted();
    }
    
    public function getVerification($offset, $limit, $search){
        
        $this->_getBackend()->getVerification($this, $offset, $limit, $search);
        return $this;
        
    }
    
    public function countVerification($search = null){
        
        return  $this->_getBackend()->countVerification($search);
        
    }
}
