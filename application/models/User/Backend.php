<?php

class Application_Model_User_Backend extends Application_Model_Abstract_Backend
{

    protected $_table = 'users';

    public function getAccountBalance($userId)
    {
        $sql  = 'SELECT SUM(`total_paid`) as `total_paid` FROM `payouts` WHERE `user_id` = :user_id AND `is_processed` = 0';
        $dbh  = Application_Model_DbFactory::getFactory()->getConnection();
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row && isset($row['total_paid'])) {
            return (float) $row['total_paid'];
        }
        return 0;
    }

    public function getActiveById(\Application_Model_Abstract $object, $id)
    {
        $sql  = "SELECT * FROM `" . $this->_getTable() . "` WHERE `id` = :id and `is_active` = 1";
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }

    public function getUserBuyFromUser($buyerId, $sellerId)
    {
        $dbh = Application_Model_DbFactory::getFactory()->getConnection();

        $sql = 'SELECT COUNT(*) AS `count` FROM `view_orders` 
            WHERE `seller_id` = :seller_id AND `buyer_id` = :buyer_id';

        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':seller_id', $sellerId);
        $stmt->bindParam(':buyer_id', $buyerId);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return (int) $row['count'];
        }
        return 0;
    }

    public function getByEmail($user, $email)
    {
        $dbh = Application_Model_DbFactory::getFactory()->getConnection();

        $sql  = "SELECT * FROM `users` WHERE `email` = :email";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $user->setData($row);
        }
    }

    public function getAll($collection)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1';

        $stmt   = $this->_getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new $this->_itemClass();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getAllByName($collection, $name)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `full_name` LIKE :full_name';

        $stmt = $this->_getConnection()->prepare($sql);

        $fullName = '%' . str_replace('%s', '', $name) . '%';
        $stmt->bindParam(':full_name', $fullName);
        $stmt->execute();
        $result   = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new $this->_itemClass();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getAllPaginated($collection, $offset, $limit)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE 1 LIMIT :offset, :limit';

        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':offset', $offset);
        $stmt->bindParam(':limit', $limit);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new $this->_itemClass();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getAllPaginatedByName($collection, $offset, $limit, $name)
    {
        $sql = 'SELECT * FROM `' . $this->_getTable() . '` WHERE `full_name` LIKE :full_name AND  `is_user_deleted` != 1 LIMIT :offset, :limit';

        $stmt     = $this->_getConnection()->prepare($sql);
        $fullName = '%' . str_replace('%s', '', $name) . '%';
        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':offset', $offset);
        $stmt->bindParam(':limit', $limit);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new $this->_itemClass();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function getCountWithoutDeleted()
    {
        $sql  = "SELECT COUNT(*) AS 'count' FROM `" . $this->_getTable() . "` WHERE `is_user_deleted` != 1";
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        return $res['count'];
    }

    public function getPrimaryEmail($uid)
    {
        $sql = "SELECT * FROM `user_emails` WHERE `user_id` = :uid AND `is_primary` = 1 LIMIT 1";

        $stmt = $this->_prepareSql($sql);
        $stmt->bindParam(':uid', $uid);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return new User_Model_Email($result);
        //var_dump($result); exit;    
    }

    protected function _update(\Application_Model_Abstract $user)
    {
        $sql = "UPDATE `" . $this->_getTable() . "` SET
    `email` = :email,
    `full_name` = :full_name,
    `password` = :password,
    `sex` = :sex,
    `date_of_birth` = :date_of_birth,
	`rating` = :rating,
	`details` = :details,
    `is_active` = :is_active,
    `is_user_deleted` = :is_deleted,
    `messages_blocked_up` = :messages_blocked_up,
    `bp_customer_uri` = :bp_customer_uri
     WHERE `id` = :id";

        $id                = $user->getId();
        $email             = $user->getEmail();
        $fullName          = $user->getFullName();
        $password          = $user->getPassword();
        $sex               = $user->getSex();
        $dateOfBirth       = $user->getDateOfBirth();
        $rating            = $user->getRating();
        $details           = $user->getDetails();
        $isActive          = $user->getIsActive();
        $isDeleted         = $user->getIsUserDeleted(0);
        $messagesBlockedUp = $user->getMessagesBlockedUp(0);
        $bpCustomerUri     = $user->getBpCustomerUri();

        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':sex', $sex);
        $stmt->bindParam(':date_of_birth', $dateOfBirth);
        $stmt->bindParam(':rating', $rating);
        $stmt->bindParam(':details', $details);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':is_active', $isActive);
        $stmt->bindParam(':is_deleted', $isDeleted);
        $stmt->bindParam(':messages_blocked_up', $messagesBlockedUp);
        $stmt->bindParam(':bp_customer_uri', $bpCustomerUri);

        $stmt->execute();
    }

    protected function _insert(\Application_Model_Abstract $user)
    {
        $dbh = Application_Model_DbFactory::getFactory()->getConnection();

        $sql  = "INSERT INTO `users`(`full_name`, `email`, `password`, `date_of_birth`, `created_at`)
            values(:full_name, :email, :password, :date_of_birth, :created_at);";
        $stmt = $dbh->prepare($sql);

        $fullName      = $user->getFullName();
        $email         = $user->getEmail();
        $password      = $user->getPassword();
        $date_of_birth = $user->getDateOfBirth();
        $created_at    = time();

        $stmt->bindParam(':full_name', $fullName);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':date_of_birth', $date_of_birth);
        $stmt->bindParam(':created_at', $created_at);

        $stmt->execute();
        $user->setId($dbh->lastInsertId());
    }

    public function getRewards(\Application_Model_Abstract $user)
    {
        $sql = 'SELECT * FROM `rewards` WHERE `user_id` = :user_id';

        $stmt   = $this->getConnection()->prepare($sql);
        $userId = $user->getId();

        $stmt->bindParam(':user_id', $userId);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            return (float) $result['amount'];
        }

        return 0;
    }

    protected function _rewardsExist(\Application_Model_Abstract $user)
    {
        $sql = 'SELECT * FROM `rewards` WHERE `user_id` = :user_id';

        $stmt   = $this->getConnection()->prepare($sql);
        $userId = $user->getId();

        $stmt->bindParam(':user_id', $userId);

        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return (bool) $result;
    }

    public function addReward(\Application_Model_Abstract $user, $amount)
    {
        if ($this->_rewardsExist($user)) {
            $sql = 'UPDATE `rewards` SET `amount` = `amount` + :add_amount WHERE `user_id` = :user_id';
        } else {
            $sql = 'INSERT INTO `rewards` (`user_id`, `amount`) VALUES (:user_id, :add_amount)';
        }

        $stmt = $this->getConnection()->prepare($sql);

        $addAmount = ($amount > 0) ? $amount : 0;
        $userId    = $user->getId();

        $stmt->bindParam(':add_amount', $addAmount);
        $stmt->bindParam(':user_id', $userId);

        $stmt->execute();
    }

    public function subtractReward(\Application_Model_Abstract $user, $amount)
    {
        if ($this->_rewardsExist($user)) {
            $sql = 'UPDATE `rewards` SET `amount` = `amount` - :add_amount WHERE `user_id` = :user_id';

            $stmt = $this->getConnection()->prepare($sql);

            $addAmount = ($amount > 0) ? ((float) $amount) : 0;
            $userId    = $user->getId();

            $stmt->bindParam(':add_amount', $addAmount);
            $stmt->bindParam(':user_id', $userId);

            $stmt->execute();
        }
    }

    public function getVerification($collection, $offset, $limit, $search)
    {

        $sql = "select `users`.* , `users_app_settings`.`value` as `is_verification`, `uas`.`uas_value` as `date_verification`
        from " . $this->_getTable() . " inner join `users_app_settings` on `users`.`id` = `users_app_settings`.`user_id`
        inner join `app_settings` on `app_settings`.`id` = `users_app_settings`.`app_setting_id`
        left join
        (
            select `users_app_settings`.`user_id` as `uas_user_id`, `users_app_settings`.`value` as `uas_value`
            from `users_app_settings`inner join `app_settings` on `app_settings`.`id` = `users_app_settings`.`app_setting_id`
            where `app_settings`.`name` = 'user_verification_request'    
        ) as `uas` on `uas`.`uas_user_id` = `users_app_settings`.`user_id`
        where `app_settings`.`name` = 'user_product_verification_available' and `users_app_settings`.`value` = 1";

        if ($search) {
            $sql .= " and `users`.`full_name` like :search";
        }

        $sql .= " order by `uas`.`uas_value` desc";

        if ($offset !== null && $limit !== null) {
            $sql .= ' limit :offset, :limit';
        }

        $stmt = $this->_getConnection()->prepare($sql);

        if ($search) {
            $search = '%' . $search . '%';

            $stmt->bindParam(':search', $search);
        }

        if ($offset !== null && $limit !== null) {
            $stmt->bindParam(':offset', $offset);
            $stmt->bindParam(':limit', $limit);
        }

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $object = new $this->_itemClass();
            $object->addData($row);

            $collection->addItem($object);
        }
    }

    public function countVerification($search)
    {

        $sql = "select count(*) as `count`
        from " . $this->_getTable() . " inner join `users_app_settings` on `users`.`id` = `users_app_settings`.`user_id`
        inner join `app_settings` on `app_settings`.`id` = `users_app_settings`.`app_setting_id`
        where `app_settings`.`name` = 'user_product_verification_available' and `users_app_settings`.`value` = 1";
        if ($search) {
            $sql .= " and `users`.`full_name` like :search";
        }
        $stmt = $this->_getConnection()->prepare($sql);
        if ($search) {
            $search = '%' . $search . '%';

            $stmt->bindParam(':search', $search);
        }
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['count'];
    }

    public function userRatingUp($userID, $rating)
    {
        $sql  = "UPDATE `users` SET `seller_popularity` = :rating WHERE `id` = :userID";
        $stmt = $this->_getConnection()->prepare($sql);
        $stmt->bindParam(':rating', $rating);
        $stmt->bindParam(':userID', $userID);
        $stmt->execute();
    }

}