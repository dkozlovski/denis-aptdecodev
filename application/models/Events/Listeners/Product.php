<?php


use \Core\Service\GoogleShoppingContent;

class Application_Model_Events_Listeners_Product extends Application_Model_Events_Listeners_Abstract implements Zend_EventManager_ListenerAggregate
{

    protected $_handlers = array();
    protected $_options  = array();

    public function attach(Zend_EventManager_EventCollection $events)
    {
        $this->_handlers[] = $events->attach('catalog.view', array($this, 'catalogView'));
        $this->_handlers[] = $events->attach('cart.add', array($this, 'cartAdd'));
        $this->_handlers[] = $events->attach('wishlist.add', array($this, 'wishlistAdd'));

        $this->_handlers[] = $events->attach('approved.by.admin', array($this, 'approvedByAdmin'));
        $this->_handlers[] = $events->attach('sold', array($this, 'sold'));
        $this->_handlers[] = $events->attach('after.changes.save', array($this, 'afterChangesSave'));
    }

    public function detach(Zend_EventManager_EventCollection $events)
    {
        foreach ($this->_handlers as $key => $handler) {
            $events->detach($handler);
            unset($this->_handlers[$key]);
        }

        $this->_handlers = array();
    }

    //when product page loaded
    public function catalogView($event)
    {
        $this->_catalogViewRatingUp($event);
        $event->getTarget()->popularity(Product_Model_Popularity::TYPE_VIEW)->increase();
    }

    //when someone adds product to cart
    public function cartAdd($event)
    {   
        $this->_cartAddRatingUp($event);
        $event->getTarget()->popularity(Product_Model_Popularity::TYPE_CART)->increase();
    }

    //when someone adds product to wishlist
    public function wishlistAdd($event)
    {
        $event->getTarget()->popularity(Product_Model_Popularity::TYPE_WISHLIST)->increase();
    }


    // :) when admin approve product (привет от Кэпа)
    public function approvedByAdmin($event)
    {
        $this->googleShoppingContentInsertProduct($event->getTarget());
    }

    // checkout - sold
    public function sold($event)
    {
        $this->_soldSellerRatingUp($event);
        
        $product = $event->getTarget();
        
        if ($product->getIsSold()) {

        } else { // if more than one product (sold only 1 item)
            $this->_soldProductRatingUp($event);
        }

    }

    // when changes occurred and product saved
    public function afterChangesSave($event)
    {
        $product = $event->getTarget();

        // Add product to sale collection if price reduced or remove it otherwise
        $set = $this->getSaleCollection();
        if ($product->isReducedPrice()) {
            $set->addProducts($product);
        } else {
            $set->removeProducts($product);
        }

        if (!$product->isSaleable() || !$product->getIsApproved()) {
            $this->googleShoppingContentDeleteProduct($product);
        }

        $this->googleShoppingContentUpdateProduct($product);
    }

    /**
     * Get or set option
     * 
     * @param string $key
     * @param mixed $value(Optional) 
     * @return mixed
     */
    protected function _option($key, $value = null)
    {
        if (null === $value) {
            return @$this->_options[$key] ? : null;
        }
        $this->_options[$key] = $value;
        return $value;
    }

    /**
     * Retrieves "sale" collection
     * @return Application_Model_Set
     */
    protected function getSaleCollection()
    {
        $set = new Application_Model_Set;
        $set->getBy_name('sale');
        return $set;
    }

    protected function googleShoppingContentInsertProduct(\Product_Model_Product $product)
    {
        if (APPLICATION_ENV !== '_live') {
            return;
        }
        // dependency for Google Merchants is retrieved(not injected) for the performance reason
        $c = $this->getContainer();

        /** @var GoogleShoppingContent $gsc */
        $gsc = $c->get('core.service.GoogleShoppingContent');
        $gsc->insertProduct($product);
    }

    protected function googleShoppingContentUpdateProduct(\Product_Model_Product $product)
    {
        if (APPLICATION_ENV !== '_live') {
            return;
        }
        // if product does not have attribute it means that it has not been added to google
        // so skip any actions
        if (!$product->getEAVAttributeValue(GoogleShoppingContent::REST_ID_ATTRIBUTE)) {
            return;
        }
        /** @var Core\GoogleShoppingContent\Delayed $delayed */
        $delayed = $this->getContainer()->get('core.GoogleShoppingContent.Delayed');
        $delayed->update($product);
    }


    protected function googleShoppingContentDeleteProduct(\Product_Model_Product $product)
    {
        if (APPLICATION_ENV !== '_live') {
            return;
        }
        // if product does not have attribute it means that it has not been added to google
        // so skip any actions
        if (!$product->getEAVAttributeValue(GoogleShoppingContent::REST_ID_ATTRIBUTE)) {
            return;
        } 


    }

    private function getContainer()
    {
        return $c = Zend_Controller_Front::getInstance()
            ->getDispatcher()
            ->getContainer();
    }
    
    protected function _catalogViewRatingUp($event)
    {
        $item = $event->getTarget()->getData();
        $config = Zend_Registry::get('config');
        $productID = $item['id'];
        $rating = $item['product_popularity']+$config->rating->product->viewProduct;
        $product = new Product_Model_Product_Collection();
        $product->productRatingUP($productID, $rating);
    }
    
    protected function _cartAddRatingUp($event)
    {
        $item = $event->getTarget()->getData();
        $config = Zend_Registry::get('config');
        $productID = $item['id'];
        $rating = $item['product_popularity']+$config->rating->product->cartAdd;
        $product = new Product_Model_Product_Collection();
        $product->productRatingUP($productID, $rating);
    }
    
    protected function _soldProductRatingUp($event)
    {
        $item = $event->getTarget()->getData();
        $config = Zend_Registry::get('config');
        $productID = $item['id'];
        $rating = $item['product_popularity']+$config->rating->product->soldProduct;
        $product = new Product_Model_Product_Collection();
        $product->productRatingUP($productID, $rating);
    }
    
    protected function _soldSellerRatingUp($event)
    {
        $item = $event->getTarget()->getData();
        $config = Zend_Registry::get('config');
        $userID = $item['user_id'];
        $userC = new User_Model_User_Collection();
        $user = $userC->getUser($userID)->getData();
        $rating = $user['seller_popularity']+$config->rating->seller->soldProduct;
        $userC->userRatingUp($userID, $rating);
    }

}
