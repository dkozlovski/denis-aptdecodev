<?php

abstract class Application_Model_Events_Listeners_Abstract
{

    protected static $_instance;

    protected function __construct()
    {
        
    }

    protected function __wakeup()
    {
        
    }

    protected function __clone()
    {
        
    }

    public static function getInstance()
    {
        if (!static::$_instance) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }

}