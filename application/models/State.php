<?php

class Application_Model_State extends Application_Model_Abstract
{
	public function getByCode($id)
	{
		$this->_getBackend()->getByCode($this, $id);
		return $this;
	}
    
    public function getByTitle($id)
	{
		$this->_getBackend()->getByTitle($this, $id);
		return $this;
	}
	
	
}
