<?php

class Application_Model_SmsNotification_Backend extends Application_Model_Abstract_Backend
{
    protected $_table ='sms_notification';
    
    public function getByType(\Application_Model_Abstract $object, $type)
    {
        $sql  = "SELECT * FROM `" . $this->_getTable() . "` WHERE `type` = :type";
        $stmt = $this->_getConnection()->prepare($sql);

        $stmt->bindParam(':type', $type);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $object->addData($row);
        }
    }
}

