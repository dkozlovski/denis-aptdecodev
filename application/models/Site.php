<?php

class Application_Model_Site extends Zend_Db_Table_Abstract{
    
    protected $_data=array();

    
    public function getSitemodel(){
        return $this->_data;
    }
    
    public function setCategories($parent="catalog"){
       
        $select = $this->getAdapter()->select();
        $select->from("categories","page_url")
               ->where("is_active = 1");
        $stm = $this->getAdapter()->query($select);
        foreach($stm->fetchAll() as $row){
        
            $this->_data[] =$parent."/".$row["page_url"];
        
        }
        return $this;
    }
    
    public function setProducts($parent="catalog"){
        
        $select = $this->getAdapter()->select();
        $select->from("products","page_url")
               ->where("is_published = 1 and is_visible and is_approved = 1");
        $stm = $this->getAdapter()->query($select);
        
        foreach ($stm->fetchAll() as $row){
            
            $this->_data[] = $parent."/".$row["page_url"];
            
        }
        return $this;
    }
    
    public function setCollections($parent = "catalog/collection"){
        
        $select = $this->getAdapter()->select();
        $select->from("collections","page_url")
               ->where("is_active = 1");
        $stm = $this->getAdapter()->query($select);
        foreach($stm->fetchAll() as $row){
        
            $this->_data[] =$parent."/".$row["page_url"];
        
        }
        return $this;
        
    }
    public function setThemesBlog($parent = "blog/category") {
        
        $config = Zend_Registry::get("config");
        $db =  Zend_Db::factory($config->resources->db->adapter, array(
            "host"=>$config->resources->db->params_wp->host,
            "username"=>$config->resources->db->params_wp->username,
            "password"=>$config->resources->db->params_wp->password,
            "dbname"=>$config->resources->db->params_wp->dbname
        ));
        $subselect3 = $db->select();
        $subselect3->from("wp_terms","term_id")
                  ->where("slug = 'blog'");
        $subselect2 =$db->select();
        $subselect2->from("wp_term_taxonomy","term_id")
                  ->where("taxonomy = 'category'")
                  ->where("parent in ?",$subselect3);
        $select = $db->select();
        $select->from("wp_terms","slug")
               ->where("term_id in ?",$subselect2);
        $stm = $db->query($select);
        $this->setStaticsPages("blog/category/blog");
        foreach($stm->fetchAll() as $row){
        
            $this->_data[] =$parent."/".$row["slug"];
        
        }
        return $this;
    }
    
    public function setPostsBlog($parent = "blog"){
        
        $config = Zend_Registry::get("config");
        $db =  Zend_Db::factory($config->resources->db->adapter, array(
            "host"=>$config->resources->db->params_wp->host,
            "username"=>$config->resources->db->params_wp->username,
            "password"=>$config->resources->db->params_wp->password,
            "dbname"=>$config->resources->db->params_wp->dbname
        ));
        $subselect3 = $db->select();
        $subselect3->from("wp_terms","term_id")
                  ->where("slug = 'blog'");
        $subselect2 =$db->select();
        $subselect2->from("wp_term_taxonomy","term_taxonomy_id")
                  ->where("taxonomy = 'category'")
                  ->where("parent in ?",$subselect3);
        $subselect = $db->select();
        $subselect->from("wp_term_relationships", "object_id")
               ->where("term_taxonomy_id in ?",$subselect2)
               ->group("object_id");
        $select = $db->select();
        $select->from("wp_posts", "post_name")
               ->where("ID in ?", $subselect);
        $stm = $db->query($select);
        
        foreach($stm->fetchAll() as $row){
        
            $this->_data[] =$parent."/".$row["post_name"];
        
        }
        
        return $this;
        
    }
    
    public function setJobs($parent = "jobs/view/id"){
        
        $config = Zend_Registry::get("config");
        $db =  Zend_Db::factory($config->resources->db->adapter, array(
            "host"=>$config->resources->db->params_wp->host,
            "username"=>$config->resources->db->params_wp->username,
            "password"=>$config->resources->db->params_wp->password,
            "dbname"=>$config->resources->db->params_wp->dbname
        ));
        $subselect3 = $db->select();
        $subselect3->from("wp_terms","term_id")
                  ->where("slug = 'jobs'");
        $subselect2 =$db->select();
        $subselect2->from("wp_term_taxonomy","term_taxonomy_id")
                  ->where("taxonomy = 'category'")
                  ->where("parent in ?",$subselect3);
        $subselect = $db->select();
        $subselect->from("wp_term_relationships", "object_id")
               ->where("term_taxonomy_id in ?",$subselect2)
               ->group("object_id");
        $select = $db->select();
        $select->from("wp_posts", "post_name")
               ->where("ID in ?", $subselect);
        $stm = $db->query($select);
        
        foreach($stm->fetchAll() as $row){
        
            $this->_data[] =$parent."/".$row["post_name"];
        
        }
        
        return $this;
        
    }


    public function setStaticsPages($page){
        $this->_data[] = $page;
        return $this;
    }
}
