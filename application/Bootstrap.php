<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initCache()
    {
        $frontend = array(
            'lifetime'                => 86400, //24 hours
            'automatic_serialization' => true,
            'caching'                 => false,
        );

        /* 	
          $backend = array(
          'servers' => array( array(
          'host' => '127.0.0.1',
          'port' => '11211'
          ) ),
          'compression' => false
          ) ; */
        $backend = array('cache_dir' => '/tmp/');

        $cache = Zend_Cache::factory('Output', 'File', $frontend, $backend);

        Zend_Registry::set('output_cache', $cache);

        $cache2 = Zend_Cache::factory('Core', 'File', $frontend, $backend);

        Zend_Registry::set('core_cache', $cache2);
    }

    protected function _initConfig()
    {
        //Zend_Registry::set('config', $this->getOptions());

        $config = new Zend_Config($this->getOptions(), true);
        Zend_Registry::set('config', $config);

        return $config;
    }

    protected function _initActionHelpers()
    {
        Zend_Controller_Action_HelperBroker::addPath(
                'Ikantam/Controller/Action/Helper/', 'Ikantam_Controller_Action_Helper');
    }

    protected function _initCoreSession()
    {
        $this->bootstrap('db');
        $this->bootstrap('session');
        Zend_Session::start();
    }

    protected function _initRoutes()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $route = new Zend_Controller_Router_Route('/catalog/:path', array(
            'module'     => 'catalog',
            'controller' => 'index',
            'action'     => 'route',
            'path'       => ':path',
        ));
        $router->addRoute('catalog', $route)
                ->addRoute('home', new Zend_Controller_Router_Route('/home/', array(
                    'module'     => 'default',
                    'controller' => 'index',
                    'action'     => 'index'
        )));

        $router->addRoute('blog:path', new Zend_Controller_Router_Route('/blog/:path', array(
            'controller' => 'blog',
            'action'     => 'view',
            'path'       => ':path',
        )));

        $router->addRoute('blog', new Zend_Controller_Router_Route('/blog/', array(
            'controller' => 'blog',
            'action'     => 'index',
        )));

        $router->addRoute('blog/category', new Zend_Controller_Router_Route('/blog/category/:slug/*', array(
            'controller' => 'blog',
            'action'     => 'index',
            'slug'       => ':slug'
        )));

        $router->addRoute('collection', new Zend_Controller_Router_Route('/catalog/collection/:slug/*', array(
            'module'     => 'catalog',
            'controller' => 'collection',
            'action'     => 'index'
                ))
        );

        $router->addRoute('collection/ajax', new Zend_Controller_Router_Route('/catalog/collection/ajax/*', array(
            'module'     => 'catalog',
            'controller' => 'collection',
            'action'     => 'ajax'
                ))
        );

        $router->addRoute('product_edit', new Zend_Controller_Router_Route('/product/edit/:id', array(
            'module'     => 'product',
            'controller' => 'edit',
            'action'     => 'index',
            'id'         => ':id'
                ), array(
            'id' => '\d+'
                ))
        );

        $router->addRoute('wp_page:post_name', new Zend_Controller_Router_Route('/:post_name', array(
                'controller' => 'blog',
                'action'     => 'page',
                'post_name'       => ':post_name',
            ),
            array('post_name' => '.*\.html')
        ));

        $router->addRoute('home2', new Zend_Controller_Router_Route('/', array(
            'module'     => 'default',
            'controller' => 'index',
            'action'     => 'index'
        )));
    }

    protected function _initPlugins()
    {
        Zend_Controller_Front::getInstance()
                ->registerPlugin(new Application_Plugin_GuestPresignInHandler);
    }

    protected function _initEvents()
    {
        Zend_EventManager_StaticEventManager::getInstance()
                ->attach('Product_Model_Product', Application_Model_Events_Listeners_Product::getInstance(), null);
    }

    protected function _initFirstReferer()
    {
        if (APPLICATION_ENV === 'development') {
            if (!isset($_COOKIE['first_referer'])/* && !isset($_COOKIE['__utmz'] ) */) {
                $referer = '(direct)';
                if (isset($_SERVER['HTTP_REFERER']) && isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_REFERER']) {
                    if (strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) === false) {
                        $referer = $_SERVER['HTTP_REFERER'];
                    }
                }
                setcookie('first_referer', $referer, time() + 31556926); // year
            }
        }
    }

    protected function _initAutoload()
    {
        $resourceLoader = $this->getResourceLoader();
        $resourceLoader->addResourceType('interface', 'interfaces/', 'Interface');
    }

    protected function _initContainer()
    {
        Zend_Loader_Autoloader::getInstance()->suppressNotFoundWarnings(true);

        $builder = new \DI\ContainerBuilder();
        $builder->addDefinitions(APPLICATION_PATH . '/configs/dependencies.php');
        $builder->addDefinitions(APPLICATION_PATH . '/configs/third_party_widgets.php');

        $parametersConfigName = 'parameters';

        if (APPLICATION_ENV === 'development') {
            $parametersConfigName .= '_development';
        }

        $builder->addDefinitions(APPLICATION_PATH . '/configs/' . $parametersConfigName . '.php');

        //$builder->addDefinitions(APPLICATION_PATH . '/configs/config.' . APPLICATION_ENV . '.php');

        /* if (APPLICATION_ENV === 'production') {
          $cache = new MemcachedCache();
          $memcached = new Memcached();
          $memcached->addServer('localhost', 11211);
          $cache->setMemcached($memcached);
          } else {
          $cache = new ArrayCache();
          }
          $cache->setNamespace('MyApp');
          $builder->setDefinitionCache($cache); */

        $this->container = $builder->build();

        $dispatcher = new \Ikantam_Controller_Dispatcher();
        $dispatcher->setContainer($this->container);

        Zend_Controller_Front::getInstance()->setDispatcher($dispatcher);
    }

    /*
      public function _initLocale()
      {
      $localeValue = 'en_US';

      $locale = new Zend_Locale($localeValue);
      Zend_Registry::set('Zend_Locale', $locale);

      $translationFile = APPLICATION_PATH . DIRECTORY_SEPARATOR .
      'lang' . DIRECTORY_SEPARATOR . $localeValue . '.inc.php';

      $translate = new Zend_Translate('array', $translationFile, $localeValue);
      Zend_Registry::set('Zend_Translate', $translate);
      }
     */
}