<?php

$translation = array(
	'Brand:'          => 'Brand:',
	'Condition:'      => 'Condition:',
	'Color:'          => 'Color:',
	'Material:'       => 'Material:',
	'Dimensions:'     => 'Dimensions:',
	'Years Old:'      => 'Years Old:',
	'Delivery:'       => 'Delivery:',
	'Original Price:' => 'Original Price:',
	'Price:'          => 'Price:',
	'Add to Cart'     => 'Add to Cart',
	'Add to Wishlist' => 'Add to Wishlist',
	'Details:'        => 'Details:',
);

return $translation;