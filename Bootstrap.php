<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initCache()
    {
        $frontend = array(
            'lifetime' => 86400, //24 hours
            'automatic_serialization' => true,
            'caching' => false,
        );

        /* 	
          $backend = array(
          'servers' => array( array(
          'host' => '127.0.0.1',
          'port' => '11211'
          ) ),
          'compression' => false
          ) ; */
        $backend = array('cache_dir' => '/tmp/');

        $cache = Zend_Cache::factory('Output', 'File', $frontend, $backend);

        Zend_Registry::set('output_cache', $cache);

        $cache2 = Zend_Cache::factory('Core', 'File', $frontend, $backend);

        Zend_Registry::set('core_cache', $cache2);
    }

    protected function _initConfig()
    {
        //Zend_Registry::set('config', $this->getOptions());

        $config = new Zend_Config($this->getOptions(), true);
        Zend_Registry::set('config', $config);

        return $config;
    }
    
    protected function _initActionHelpers()
    { 
        Zend_Controller_Action_HelperBroker::addPath(
            'Ikantam/Controller/Action/Helper/',
            'Ikantam_Controller_Action_Helper');
    }

    protected function _initCoreSession()
    {
        $this->bootstrap('session');
        Zend_Session::start();
    }

    protected function _initRoutes()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $route = new Zend_Controller_Router_Route('/catalog/:path', array(
                    'module' => 'catalog',
                    'controller' => 'index',
                    'action' => 'route',
                    'path' => ':path',
                ));
        $router->addRoute('catalog', $route)
               ->addRoute('home', new Zend_Controller_Router_Route('/home/', array(
                   'module' => 'default',
                   'controller' => 'index',
                   'action' => 'index'                    
                    )));
    }
    
    protected function _initPlugins()
    {
        Zend_Controller_Front::getInstance()
                              ->registerPlugin( new Application_Plugin_GuestPresignInHandler () );  
    }

    /*
      public function _initLocale()
      {
      $localeValue = 'en_US';

      $locale = new Zend_Locale($localeValue);
      Zend_Registry::set('Zend_Locale', $locale);

      $translationFile = APPLICATION_PATH . DIRECTORY_SEPARATOR .
      'lang' . DIRECTORY_SEPARATOR . $localeValue . '.inc.php';

      $translate = new Zend_Translate('array', $translationFile, $localeValue);
      Zend_Registry::set('Zend_Translate', $translate);
      }
     */
}
