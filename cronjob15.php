<?php

//date_default_timezone_set('Europe/Minsk');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', 'live');

// Define application environment
defined('BASE_ORDER_URL')
    || define('BASE_ORDER_URL', 'https://aptdeco.com');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

// Init Composer autoload
require_once APPLICATION_PATH . "/../vendor/autoload.php";

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap();

$frontController = Zend_Controller_Front::getInstance();
$frontController->getRouter()->addDefaultRoutes();
$frontController->setBaseUrl(BASE_ORDER_URL);

$model = new Application_Model_Cron_ProductAlert();
$model->doCron();

$str = sprintf("Cron15 finished at %s (%s).\n", time(), date('r'));
file_put_contents (APPLICATION_PATH . '/../cron.log', $str, FILE_APPEND);
